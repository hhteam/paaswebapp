/**
 * Eliademy.com
 *
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

$(function ()
{
    var spinner = new Spinner({ lines: 13, length: 7, width: 4, radius: 10,
      corners: 1, rotate: 0, color: '#000', speed: 1, trail: 60, shadow: false,
      hwaccel: true, className: 'spinner', zIndex: 2e9, top: '50%', left: '50%' });
    var country = '';

    $('[data-magnific-iframe]').magnificPopup({ type: "iframe" });

    var resetLoginForm = function ()
    {
        $("#login_controls").removeClass("error");
        $("#login_controls .help-inline").hide();
    };

    var resetSignupForm = function ()
    {
        $("#signup_form .control-group").removeClass("error");
        $("#signup_form .control-group .help-inline").empty().hide();
    };

    var resetBusinessSignupForm = function ()
    {
        $("#business_signup_form .control-group").removeClass("error");
        $("#business_signup_form .control-group .help-inline").empty().hide();
    };

    var resetResetForm = function ()
    {
        $("#passwd_reset_form input[name=email]").val("").removeAttr("disabled");
        $("#reset_controls").removeClass("error");
    };

    if (_Header.info instanceof Object && parseInt(_Header.info.userid))
    {
        if (currentPage == "login" || currentPage == "signup" || currentPage == "business-signup")
        {
            window.location.href = paths.a;
        }
        else if (currentPage == "signup_redirect")
        {
            setTimeout(function ()
            {
                var wantsUrl = $.cookie("loginRedirect");

                if (wantsUrl)
                {
                    $.removeCookie('loginRedirect', { path: "/" });
                    window.location.href = wantsUrl;
                }
                else
                {
                    window.location.href = paths.a;
                }
            }, 400);
        }
        else if (currentPage == "business")
        {
            window.location.href = paths.a + "go-premium";
        }

        $(".cert-block-user-" + _Header.info.userid).show();
    }
    else
    {
        if (currentPage == "home" && !$.cookie("signup_nag_shown"))
        {
            var nagShown = 0;

            var showNag = function ()
            {
                if (!nagShown)
                {
                    nagShown = 1;
                    $.cookie("signup_nag_shown", "1", { expires: 36500, path: "/" });
                    $("#signup-reminder-modal").modal("show");
                }
            };

            setTimeout(showNag, 30000);
        }

        $(".cert-block-no-user").show();
    }

    $.ajax({ url: paths.r + "ipcheck/code.php", context: this, success: function (data)
    {
        this.country = data;
        if (data == "CN")
        {
            $("#follow-us-block").hide();
            $("#signup-social-block").hide();
            $("#login-social-block").hide();
            $("#social-buttons-block").hide();

            $("#login-email-block").removeClass("span6");
            $("#signup-email-block").removeClass("span6");
        }
    }});

    $("#login_form").submit(function ()
    {
        var thisThis = this;
        var handler = function ()
        {
            var error = function (ecode)
            {
                spinner.stop();
                var emsg = genericerror;
                if(typeof ecode != 'undefined') {
                    switch(ecode) {
                      case 1001:
                      emsg = userunknownerror;
                      break;
                      case 1002:
                      emsg = wrongautherror;
                      break;
                      default:
                    }
                }
                $("#login_controls .help-inline").text(emsg);
                $("#login_controls").addClass("error");
                $("#login_controls .help-inline").show();
            };

            $.ajax({ url: paths.m + "theme/monorail/ext/ajax_get_token.php", data: { username: thisThis.username.value }, dataType: "json", context: this,
                success: function (data)
                {
                    if (data instanceof Object && data.token)
                    {
                        spinner.stop();
                        localStorage.setItem('user_login','login_form');
                        try
                        {
                            var wantsUrl;
                            if( data.expired )
                                wantsUrl = paths.a + 'payment-method';
                            else {
                                wantsUrl = $.cookie('loginRedirect');
                                if( !wantsUrl )
                                    wantsUrl = paths.a;
                            }
                            $.removeCookie('loginRedirect', { path: '/' });
                            window.location.href = wantsUrl;
                        }
                        catch (err)
                        {
                            window.location.href = paths.a;
                        }
                    }
                    else if(data instanceof Object && data.error) {
                        error(data.error);
                    }
                    else
                    {

                        error();
                    }
                }, error: error});
        };

        $.ajax({ url: paths.m + "login/index.php", type: "POST", context: this,
            data: { username: this.username.value, password: this.password.value, rememberusername: 1 },
            success: handler,
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //If error while posting mostlikely user exceeded the number of retries his option is to restart
                // the browser
                spinner.stop();
                $("#login_controls .help-inline").text(loginretryerror);
                $("#login_controls").addClass("error");
                $("#login_controls .help-inline").show();
            } });
        spinner.spin(this);
        resetLoginForm();

        return false;
    });

    $("#signup_form").submit(function ()
    {
        var handler = function (data)
        {
            var err = false;

            $(data).find(".fitem").each(function ()
            {
                var field = $(this).attr("id").substring(9);

                $(this).find("span.error").each(function ()
                {
                    var errtxt = $(this).text();

                    if (errtxt == "This email address is already registered. New password?")
                    {
                        errtxt = "This email address is already in use. Please choose another email or login with your existing account.";
                    }

                    $("#signup_" + field + " .help-inline").show().html(errtxt);
                    $("#signup_" + field).addClass("error");

                    err = true;
                });
            });

            if (!err)
            {
                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", window.location.href);
                document.body.appendChild(form);
                form.submit();
            }
            else
            {
                spinner.stop();
            }
        };

        var countryCode = function(code) {
            this.country = code;
            $.ajax({ url: paths.m + "theme/monorail/ext/ajax_get_sesskey.php", type: "GET", context: this, success: function (sesskey)
            {
                var lang = (currentLang == "default") ? "en" : currentLang;
                $.ajax({ url: paths.m + "theme/monorail/signup.php", type: "POST", context: this,
                    data: { _qf__login_signup_form: "1", firstname: this.firstname.value, lastname: this.lastname.value,
                           email: this.email.value, password: this.password.value, password2: this.password.value,
                           lang: lang, country: this.country, sesskey: sesskey },
                success: handler, error: handler });
           } });
        }
        $.ajax({ url: paths.r + "ipcheck/code.php", context: this, success: countryCode, error: countryCode });
        spinner.spin(this);
        resetSignupForm();

        return false;
    });

    $("#business_signup_form").submit(function ()
    {
        var handler = function (data)
        {
            var err = false;

            $(data).find(".fitem").each(function ()
            {
                var field = $(this).attr("id").substring(9);

                $(this).find("span.error").each(function ()
                {
                    var errtxt = $(this).text();

                    if (errtxt == "This email address is already registered. New password?")
                    {
                        errtxt = "This email address is already in use. Please choose another email or login with your existing account.";
                    }

                    $("#business_signup_form ." + field + " .help-inline").show().html(errtxt);
                    $("#business_signup_form ." + field).addClass("error");

                    err = true;
                });
            });

            if (!err)
            {
                localStorage.setItem("e4b_orgname", this.orgname.value);

                setTimeout(function ()
                {
                    try
                    {
                        var wantsUrl = $.cookie("loginRedirect");

                        if (wantsUrl)
                        {
                            $.removeCookie("loginRedirect", { path: "/" });
                            window.location.href = wantsUrl;
                        }
                        else
                        {
                            window.location.href = paths.a;
                        }
                    }
                    catch (err)
                    {
                        window.location.href = paths.a;
                    }
                }, 3500);

                // Sign up tracking for Google.
                var img = document.createElement("img");
                img.style.visibility = "hidden";
                document.body.appendChild(img);
                img.setAttribute("src", "//www.googleadservices.com/pagead/conversion/999934223/?value=0&amp;label=gzdZCKHpkAUQj5Ln3AM&amp;guid=ON&amp;script=0");

                // Sign up tracking for face book.
                var imgfb = document.createElement("img");
                imgfb.style.visibility = "hidden";
                document.body.appendChild(imgfb);
                imgfb.setAttribute("src", "https://www.facebook.com/offsite_event.php?id=6006058576456&amp;value=0");
            }
            else
            {
                spinner.stop();
            }
        };

        var countryCode = function(code) {
            this.country = code;
            $.ajax({ url: paths.m + "theme/monorail/ext/ajax_get_sesskey.php", type: "GET", context: this, success: function (sesskey)
            {
                var lang = (currentLang == "default") ? "en" : currentLang;
                $.ajax({ url: paths.m + "theme/monorail/signup.php", type: "POST", context: this,
                    data: { _qf__login_signup_form: "1", firstname: this.firstname.value, lastname: this.lastname.value,
                           email: this.email.value, password: this.password.value, password2: this.password.value,
                           lang: lang, country: this.country, sesskey: sesskey, e4business: true },
                success: handler, error: handler });
           } });
        }
        $.ajax({ url: paths.r + "ipcheck/code.php", context: this, success: countryCode, error: countryCode });

        spinner.spin(this);
        resetBusinessSignupForm();

        return false;
    });

    $("#passwd_reset_form input[name=email]").change(function ()
    {
        $("#reset_controls").removeClass("error");
    });

    $("#passwd_reset_form").submit(function ()
    {
        if (!this.email.value)
        {
            $("#reset_controls").addClass("error");

            return false;
        }

        var handler = function ()
        {
            spinner.stop();

            resetResetForm();

            $('#passwd_reset_dialog').modal('hide');
            $('#passwd_reset_message_dialog').modal('show');
        };

        $.ajax({ url: paths.m + "theme/monorail/ext/ajax_get_sesskey.php", type: "GET", context: this, success: function (sesskey)
        {
            var data = { _qf__login_forgot_password_form: "1", sesskey: sesskey };

            data.email = this.email.value;

            $.ajax({ url: paths.m + "login/forgot_password.php", type: "POST", context: this,
                data: data, success: handler, error: handler });
        } });

        spinner.spin(this);

        return false;
    });

    $("#devselectlist li").click(function(ev)
    {
        $('#devselectlist li').each(function() {
            $(this).removeClass('active');
        });
        ($(ev.target).parent()).addClass("active");
    });

    $(window).resize(function() {
        if($("#header").css("position") == "fixed") {
            $("#devnav").css('position','fixed')
        } else {
            $("#devnav").css('position','relative')
        }
    });

    _Header.init($("#header-box").get(0), currentPage);

    if ($("#header").css("position") == "fixed")
    {
         // UserVoice Javascript SDK (only needed once on a page)
        (function(){
            var uv=document.createElement('script');
            uv.type='text/javascript';
            uv.async=true;
            uv.src='//widget.uservoice.com/eTEcdS7m6UK1wuohZOcX0Q.js';
            var s=document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(uv,s)
        })();
        UserVoice = window.UserVoice || [];

      // UserVoice Javascript SDK developer documentation:
      // https://www.uservoice.com/o/javascript-sdk

        if (_Header.info instanceof Object && parseInt(_Header.info.userid))
        {
            UserVoice.push(['identify', {
                name: _Header.info.username,
                id: _Header.info.userid
            }]);
        }
        else
        {
            UserVoice.push(['identify', { name: 'Unregistered user' }]);
        }

        // Add default trigger to the bottom-right corner of the window:
        UserVoice.push(['addTrigger', {}]);

        // Autoprompt users for Satisfaction and SmartVote
        // (only displayed when certain conditions are met)
        UserVoice.push(['autoprompt', {}]);

        UserVoice.push(['set', {
        // (include other options here)
            strings: {
            // Menu items
            contact_menu_label: 'Contact Eliademy team',
            // Contact form
            contact_title: 'Contact Eliademy team'
            // Etc.
            }
        }]);

    }
    else
    {
        setTimeout(function() { window.scrollTo(0, $("#header").height()); }, 0);
        $("#devnav").css('position','relative')
    }
});
