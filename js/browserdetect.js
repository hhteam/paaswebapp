/**
 * Eliademy.com
 *
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

$(function ()
{
    // The quick and dirty way to detect browser engine
    var browser = undefined;

    if (document.body.style.webkitTransform !== undefined)
    {
        browser = "webkit";
    }
    else if (document.body.mozRequestFullScreen !== undefined)
    {
        browser = "mozila";
    }
});
