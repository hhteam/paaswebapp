<?php

try
{
    if ($_SERVER["REQUEST_METHOD"] != "POST" || !isset($_POST["url"]) || !isset($_POST["name"]))
    {
        throw new Exception("Invalid request!");
    }

    $db = new PDO("sqlite:" . dirname(__FILE__) . "/inc/data/schools.db");

    $stmt1 = $db->prepare("SELECT id FROM schools WHERE ip_entered = ? AND time_entered > datetime('now', '-600 second')");
    $stmt1->execute(array($_SERVER["REMOTE_ADDR"]));

    if ($stmt1->fetch(PDO::FETCH_ASSOC) !== FALSE)
    {
        throw new Exception("Too many calls from this IP!");
    }

    $stmt2 = $db->prepare("INSERT INTO schools (name, url, ip_entered) VALUES (?, ?, ?)");
    $stmt2->execute(array($_POST["name"], $_POST["url"], $_SERVER["REMOTE_ADDR"]));
}
catch (Exception $e)
{
    header('HTTP/1.1 404 Not Found');
    echo "<h1>404 Not Found</h1>";
    echo "The page you requested could not be found.";
}
