<?php

require_once __DIR__ . "/inc/config.php";

$lang = @$_GET["lang"];

if ($lang == "en") {
    $lang = "default";
}

require_once __DIR__ . "/inc/i18n/default.php";

$langStr = $str;

if ($lang != "default" && array_key_exists($lang, $LANGS) && file_exists(__DIR__ . "/inc/i18n/" . $lang . ".php")) {
    require_once __DIR__ . "/inc/i18n/" . $lang . ".php";

    foreach ($str as $key => $val) {
        $langStr[$key] = $val;
    }
}

header("Content-Type: application/json");
echo json_encode($langStr);
