<?php
    ob_start();

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach (scandir(__DIR__ . "/../users/") as $file)
        if (substr($file, 0, 1) != ".") { ?>
    <url>
        <loc>https://eliademy.com/users/<?php echo $file; ?></loc>
        <lastmod><?php echo date("c", filemtime(__DIR__ . "/../users/" . $file)) ?></lastmod>
        <changefreq>yearly</changefreq>
        <priority>0.6</priority>
    </url>
<?php } ?>
</urlset>
<?php
    $text = ob_get_clean();
    file_put_contents(__DIR__ . "/../sitemap_users.xml", $text);
