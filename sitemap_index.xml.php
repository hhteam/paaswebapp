<?php
    header("Content-Type: application/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    $lastFriday = date("c", strtotime("last friday"));
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>https://eliademy.com/sitemap.xml</loc>
      <lastmod><?php echo $lastFriday ?></lastmod>
   </sitemap>
   <sitemap>
      <loc>https://eliademy.com/catalog/sitemap/sitemap.xml</loc>
      <lastmod><?php echo $lastFriday ?></lastmod>
   </sitemap>
   <sitemap>
      <loc>https://blog.eliademy.com/sitemap.xml</loc>
      <lastmod><?php echo $lastFriday ?></lastmod>
   </sitemap>
   <sitemap>
      <loc>https://eliademy.com/sitemap_users.xml</loc>
      <lastmod><?php echo $lastFriday ?></lastmod>
   </sitemap>
   <sitemap>
      <loc>https://eliademy.com/sitemap_org.xml</loc>
      <lastmod><?php echo $lastFriday ?></lastmod>
   </sitemap>
</sitemapindex>
