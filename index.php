<?php
/**
 * Eliademy.com
 *
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

$uri_parts = explode("/", @$_GET["q"]);
$uri_index = 0;

if (empty($uri_parts[$uri_index]) && count($uri_parts) > 1)
{
    $uri_index++;
}

if (file_exists(dirname(__FILE__) . "/inc/i18n/" . $uri_parts[$uri_index] . ".php"))
{
    $lang = $uri_parts[$uri_index];
    $uri_index++;
}
else
{
    $lang = "default";
}

// Check cookie for user seleced language (NOT USED ANYMORE).
if (isset($_COOKIE["preferred_language"]))
{
    $cookieLang = preg_replace("/[^a-z_]/", "", $_COOKIE["preferred_language"]);

    setcookie("preferred_language", NULL, 1);

    if ($lang == "default" && $cookieLang != "default" && file_exists(dirname(__FILE__) . "/inc/i18n/" . $cookieLang . ".php"))
    {
        header("Location: /" . $cookieLang . "/", 303);
        exit(0);
    }
}

if ($lang == "default")
{
    $browserLang = preg_replace("/[^a-z_]/", "", substr(@$_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));

    if (file_exists(dirname(__FILE__) . "/inc/i18n/" . $browserLang . ".php"))
    {
        $lang = $browserLang;
    }
}

if ($lang == "default")
{
    $lang = "en";
}

$page = @$uri_parts[$uri_index];

require_once "inc/config.php";

function get_string($word)
{
    global $str, $defalut_str;

    if (isset($str[$word]))
    {
        return $str[$word];
    }
    else
    {
        return $defalut_str[$word];
    }
}

require_once dirname(__FILE__) . "/inc/i18n/default.php";
$defalut_str = $str;

if ($lang != "en")
{
    require_once dirname(__FILE__) . "/inc/i18n/" . $lang . ".php";
}

$background_color = "#FFF";
$meta_publisher = "https://plus.google.com/+Eliademy";
$page_image = "https://eliademy.com/img/logo-eliademy-square.png";

if (isset($_GET["error"]) && !empty($_GET["error"]))
{
    // Error while logging in.
    $title = "Eliademy | " . get_string("dlg_log_in");
    $description = get_string("home_description");
    $pageName = "login";
    $content = "inc/content/login.php";
}
else
{
    // Normal page
    switch ($page)
    {
        case "":
            $title = "Eliademy | " . get_string("text_home_header");
            $description = get_string("home_description");
            $pageName = "home";
            $content = "inc/content/home.php";
            break;

        case "about":
            $title = "Eliademy | " . get_string("title_about_us");
            $description = get_string("home_description");
            $pageName = "about";
            $content = "inc/content/about.php";
            break;

        case "business-signup":
        case "premium-signup":
            if (isset($_GET["ref"]) && !empty($_GET["ref"]))
            {
                setcookie("referral", $_GET["ref"], time() + 432000, "/");
                header("Location: " . CFG_ROOT_PATH . "business-signup", 302);
                exit(0);
            }
            $title = "Eliademy | " . get_string("dlg_sign_up");
            $description = get_string("business_description");
            $pageName = "business-signup";
            $content = "inc/content/business_signup.php";
            break;

        case "business":
        case "premium":
            $title = "Eliademy | " . get_string("title_premium");
            $description = get_string("business_description");
            $pageName = "business";
            $content = "inc/content/business.php";
            break;

        case "e4b-error":
            $title = "Eliademy | " . get_string("title_premium");
            $description = "";
            $error_message = get_string("e4b_user_error");
            $pageName = "e4b-error";
            $content = "inc/content/error_page.php";
            break;

        case "login-ccig":
            $title = "Eliademy | CCIG";
            $description = "Custom login for CCIG";
            $pageName = "login";
            $content = "inc/content/login-ccig.php";
            break;

        case "login":
            $title = "Eliademy | " . get_string("dlg_log_in");
            $description = get_string("home_description");
            $pageName = "login";
            $content = "inc/content/login.php";
            break;

        case "love":
            $title = "Eliademy | " . get_string("title_love");
            $description = get_string("home_description");
            $pageName = "love";
            $content = "inc/content/love.php";
            break;

        case "lti":
            $title = "Eliademy | " . get_string("title_lti");
            $description = get_string("home_description");
            $pageName = "lti";
            $content = "inc/content/lti.php";
            break;

        // case "mobile":
        //     $title = get_string("mobile_title");
        //     $description = get_string("home_description");
        //     $pageName = "mobile";
        //     $content = "inc/content/mobile.php";
        //     break;

        case "opensource":
            $title = "Eliademy | " . get_string("title_open_source");
            $description = get_string("home_description");
            $pageName = "opensource";
            $content = "inc/content/opensource.php";
            break;

        case "privacy":
            $title = "Eliademy | " . get_string("title_privacy_policy");
            $description = get_string("home_description");
            $pageName = "privacy";
            $content = "inc/content/privacy.php";
            break;

        case "signup":
            $title = "Eliademy | " . get_string("dlg_sign_up");
            $description = get_string("home_description");

            if ($_SERVER["REQUEST_METHOD"] == "POST")
            {
                $content = "inc/content/signup_track.php";
                $pageName = "signup_redirect";
            }
            else
            {
                $content = "inc/content/signup.php";
                $pageName = "signup";
            }
            break;

        case "terms":
            $title = "Eliademy | " . get_string("title_terms_of_use");
            $description = get_string("home_description");
            $pageName = "terms";
            $content = "inc/content/tos.php";
            break;

        case "support":
            $title = get_string("text_support_hero_title");
            $description = get_string("home_description");
            $pageName = "support";
            $content = "inc/content/support.php";
            break;

        case "features":
            $title = "Eliademy | " . get_string("label_features");
            $description = get_string("home_description");
            $pageName = "features";
            $content = "inc/content/features.php";
            break;


        case "testimonials":
            $title = "Eliademy | " . get_string("title_testimonials");
            $description = get_string("home_description");
            $pageName = "testimonials";
            $content = "inc/content/testimonials.php";
            break;

        case "cert":
            $title = "Eliademy | " . get_string("title_certificate");
            $description = get_string("home_description");
            $pageName = "certificate";
            $content = "inc/content/certificate.php";
            break;

        default:
            // Checking if this is a organization public page.
            try
            {
                $db = new PDO("mysql:dbname=" . DB_NAME . ";host=" . DB_HOST, DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

                $q = $db->prepare("SELECT c.id AS id, c.name AS name, mci.organization_details AS details, mci.published_courses AS courses, mci.public_page_ann AS ann, " .
                    "mci.public_page_ann_info AS ann_info, mci.public_page_ad AS ad, mci.public_page_ad_info AS ad_info, mci.public_page_cats AS cats " .
                    "FROM " . DB_PREFIX . "monorail_cohort_info AS mci " .
                        "INNER JOIN " . DB_PREFIX . "cohort AS c ON c.id=mci.cohortid " .
                            "WHERE mci.public_page_path=? AND mci.public_page=1");

                $q->bindParam(1, $page);

                if ($q->execute()) {
                    $info = $q->fetch(PDO::FETCH_ASSOC);
                } else {
                    $info = null;
                }
            }
            catch (Exception $ex)
            {
                error_log(var_export($ex, true));
                $info = null;
            }

            if ($info) {
                $details = json_decode($info["details"]);

                if (!is_object($details)) {
                    $details = new stdClass();
                    $details->about = "";
                    $details->link_fb = "";
                    $details->link_li = "";
                    $details->link_tw = "";
                    $details->link_gp = "";
                    $details->link_web = "";
                    $details->location = "";
                    $details->country = "";
                }

                $title = $info["name"] . " | " . "Eliademy";
                $description = $details->about;
                $pageName = "organization_public_page";
                $content = "inc/content/org_page.php";
                $background_color = "#F5F5F5";
                $meta_publisher = $details->link_gp;

                if (file_exists(__DIR__ . "/app/public_images/org/logo" . $info["id"] . ".png")) {
                    $page_image = "https://eliademy.com" . CFG_MOODLE_PATH . "public_images/org/logo" . $info["id"] . ".png";
                }
                break;
            }

            // Must be a course public page (TODO: test, add 404 here?)
            header("Location: " . CFG_ROOT_PATH . "catalog/catalog/product/view/sku/" . $page, 303);
            exit(0);
    }
}

require_once "inc/base.php";
