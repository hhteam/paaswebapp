# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### QUICK INSTALL ###

PREREQUISITES

nginx
FastCGI Process Manager
Redis
MariaDB

1) Clone Eliademy WebApp repo to into your web directory (e.g. /var/www):
	$ git clone git@bitbucket.org:hhteam/webapp.git /var/www

2) Create a single database for Moodle to store all
   its tables in (or choose an existing database).
    $ mysql_secure_installation
    $ mysql -u root -p
    > CREATE DATABASE moodle DEFAULT CHARACTER SET UTF8 COLLATE utf8_unicode_ci;
    > CREATE USER 'moodle'@'localhost' IDENTIFIED BY 'moodle';
    > GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, CREATE TEMPORARY TABLES, DROP, \
		INDEX, ALTER ON moodle.* TO 'moodle'@'localhost';


3) Visit your Moodle site with a browser, you should
   be taken to the install.php script, which will lead
   you through creating a config.php file and then
   setting up Moodle, creating an admin account etc.

4) Set up a cron task to call the file admin/cron.php
   every five minutes or so.


For more information, see the Eliademy Project Wiki:

https://sites.google.com/a/cloudberrytec.com/hh/infrastructure/kaltura-setup

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact