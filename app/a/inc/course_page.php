<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title><?php echo @$courseInfo["fullname"] ?></title>
 <meta prefix="og: http://ogp.me/ns#" property="og:type" content="website">
 <meta prefix="og: http://ogp.me/ns#" property="og:title" content="<?php echo @$courseInfo["fullname"]; ?>">
 <meta prefix="og: http://ogp.me/ns#" property="og:url" content="<?php echo @$config->CatalogUrl; ?>/catalog/product/view/sku/<?php echo @$courseInfo["code"]; ?>">
 <meta prefix="og: http://ogp.me/ns#" property="og:image" content="<?php echo $config->EliademyUrl ?>/extimg/<?php echo @$courseInfo["code"] ?>_course_background.jpg">
 <meta prefix="og: http://ogp.me/ns#" property="og:description" content="<?php echo strip_tags(@$courseInfo["summary"]); ?>">
 <meta http-equiv="refresh" content="0;URL='<?php echo $config->CatalogUrl . "/catalog/product/view/sku/" . $courseInfo["code"]; ?>'">
</head>
<body>
<div style="display: none;">
 <h1><?php echo @$courseInfo["fullname"]; ?></h1>
 <p><?php echo strip_tags(@$courseInfo["summary"]); ?></p>
</div>
</body>
</html>
