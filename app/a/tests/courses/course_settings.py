#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import traceback
import shutil
import time
import sys
import os

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.course_settings'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'teacher@127.0.0.1'
        self.teacher2 = 'teacher2@127.0.0.1'
        
        self.coursename = 'Course Settings Course'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.teacher2)
        testdata.require(self.args, 'user', self.teacher2)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.teacher2, self.coursename)
       

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename;
        try:
            testutils.login(wd, args.target, username)
            testutils.go_to_course(wd, coursename)
            
            if args.j <= 1:
                # go to settings tab
                wd.find_element_by_id('course-button-settings').click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                # check we have buttons and fields
                if not testutils.is_element_visible(wd, By.ID, 'course-start-date-input') or not testutils.is_element_visible(wd, By.ID, 'course-end-date-input'):
                    raise Exception("Course start and end date input fields should be visible")
                if not testutils.is_element_visible(wd, By.ID, 'btn-end-course'):
                    raise Exception("Course end now button should be visible")
                # if not testutils.is_element_visible(wd, By.ID, 'btn-export-course'):
                #     raise Exception("Course export button should be visible")
                if not testutils.is_element_visible(wd, By.ID, 'btn-delete-course'):
                    raise Exception("Course delete button should be visible")
                    
            if args.j <= 2:
                # go to settings tab
                wd.find_element_by_id('course-button-settings').click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                # check that initially the start date is today and end date is empty
                if wd.find_element_by_id('course-start-date-input').get_attribute("value") != time.strftime('%d.%m.%Y'):
                    raise Exception("Start date should be today, eg: "+time.strftime('%d.%m.%Y')+', it is '+wd.find_element_by_id('course-start-date-input').get_attribute("value"))
                if wd.find_element_by_id('course-end-date-input').get_attribute("value") != '':
                    raise Exception("End date should be empty, it is "+wd.find_element_by_id('course-end-date-input').get_attribute("value"))
                # choose new dates
                nextmonth = time.localtime().tm_mon+1
                nextyear = time.localtime().tm_year
                if nextmonth == 13:
                    nextmonth = 1
                    nextyear += 1
                nextnextmonth = nextmonth+1
                nextnextyear = nextyear
                if nextnextmonth == 13:
                    nextnextmonth = 1
                    nextnextyear += 1
                wd.find_element_by_id('course-start-date-input').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"15\")').click();");
                if wd.find_element_by_id('course-start-date-input').get_attribute("value") != '15.'+'{num:02d}'.format(num=nextmonth)+'.'+str(nextyear):
                    raise Exception("Start date should be 15."+'{num:02d}'.format(num=nextmonth)+"."+str(nextyear)+", it is "+wd.find_element_by_id('course-start-date-input').get_attribute("value"))
                wd.find_element_by_id('course-end-date-input').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"16\")').click();");
                if wd.find_element_by_id('course-end-date-input').get_attribute("value") != '16.'+'{num:02d}'.format(num=nextnextmonth)+'.'+str(nextnextyear):
                    raise Exception("End date should be 16."+'{num:02d}'.format(num=nextnextmonth)+"."+str(nextnextyear)+", it is "+wd.find_element_by_id('course-end-date-input').get_attribute("value"))
                # save
                wd.find_element_by_id('btn-save-general-settings').click()
                time.sleep(1)
                # check
                if wd.find_element_by_id('course-start-date-input').get_attribute("value") != '15.'+'{num:02d}'.format(num=nextmonth)+'.'+str(nextyear):
                    raise Exception("Post-save: Start date should be 15."+'{num:02d}'.format(num=nextmonth)+"."+str(nextyear)+", it is "+wd.find_element_by_id('course-start-date-input').get_attribute("value"))
                if wd.find_element_by_id('course-end-date-input').get_attribute("value") != '16.'+'{num:02d}'.format(num=nextnextmonth)+'.'+str(nextnextyear):
                    raise Exception("Post-save: End date should be 16."+'{num:02d}'.format(num=nextnextmonth)+"."+str(nextnextyear)+", it is "+wd.find_element_by_id('course-end-date-input').get_attribute("value"))
            
            if args.j <= 3:
                # course end now and enable
                # end
                # go to settings tab
                wd.find_element_by_id('course-button-settings').click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                wd.find_element_by_id('btn-end-course').click()
                wd.find_element_by_css_selector('div.modal-footer a.btn-danger').click()
                # check course has ended label
                wd.find_element_by_id('course-button-overview').click()
                testutils.wait_for(wd, By.ID, 'course-ended-sticker')
                # check course is in the completed section
                wd.find_element_by_id('bt-course').click()
                testutils.wait_for(wd, By.CSS_SELECTOR, 'div.completed-course a[title="'+coursename+'"]')
                testutils.go_to_course(wd, coursename)
                # enable
                wd.find_element_by_id('course-button-settings').click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                wd.find_element_by_id('btn-enable-course').click()
                wd.find_element_by_css_selector('div.modal-footer a.btn-danger').click()
                testutils.wait_for(wd, By.ID, 'btn-end-course')
            
            if args.j <= 4:
                # change teacher dropdown is on page
                wd.find_element_by_id('course-button-settings').click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                wd.find_element_by_id('btn-change-teacher').click()
                wd.find_elements_by_css_selector('#change-teacher ul li')[1].click()
                
            if args.j <= 5:
                # skills
                testutils.wait_for(wd, By.ID, 'course-button-overview').click()
                testutils.wait_for(wd, By.CSS_SELECTOR, '.btn.btn-primary').click()
                # check skill label appears
                testutils.wait_for(wd, By.ID, 'skills-input').click()
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill")
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) == 0:
                    raise Exception("There should be one skill label")
                # check skill label is removed on click
                wd.find_elements_by_css_selector('.skill-label i')[0].click()
                if testutils.is_element_visible(wd, By.CSS_SELECTOR, '.skill-label'):
                    raise Exception("There should be no skill labels")
                # check skill is actually saved
                testutils.wait_for(wd, By.ID, 'skills-input').click()
                testutils.wait_for(wd, By.ID, 'skills-input').clear()
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill 2")
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                testutils.wait_for(wd, By.ID, 'btn_edit_save').click()
                testutils.wait_for(wd, By.CSS_SELECTOR, '.btn.btn-primary')
                wd.refresh()
                testutils.wait_for(wd, By.CSS_SELECTOR, '.btn.btn-primary')
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) == 0:
                    raise Exception("There should be one skill label")
                # add two more skills in one go
                testutils.wait_for(wd, By.CSS_SELECTOR, '.btn.btn-primary').click()
                testutils.wait_for(wd, By.ID, 'skills-input').click()
                testutils.wait_for(wd, By.ID, 'skills-input').clear()
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill 3,Test Skill 4")
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.skills-edit .skill-label'))))
                # remove one, add two more
                #wd.find_elements_by_css_selector('.skill-label i')[0].click()
                #time.sleep(1)
                #testutils.wait_for(wd, By.ID, 'skills-input').click()
                #testutils.wait_for(wd, By.ID, 'skills-input').clear()
                #testutils.wait_for(wd, By.ID, 'skills-input').send_keys("Tost, Tast")
                #testutils.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                #time.sleep(0.5)
                #if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) != 3:
                    #raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.skills-edit .skill-label'))))
                #testutils.wait_for(wd, By.CSS_SELECTOR, '#skill-edit-errors')  # error should be visible    
                #if len(wd.find_elements_by_css_selector('#skill-edit-errors span div')) != 2:
                    #raise Exception("There should be 2 error divs, there is "+str(len(wd.find_elements_by_css_selector('#skill-edit-errors span div'))))
                testutils.wait_for(wd, By.ID, 'btn_edit_save').click()
                testutils.wait_for(wd, By.CSS_SELECTOR, '.btn.btn-primary')
            
            if args.j <= 6:
                ####   Enrollment  #######
                wd.find_element_by_id('course-button-settings').click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                #check buttons
                if not testutils.is_element_visible(wd,By.ID,'button-unpublish-course') or not testutils.is_element_visible(wd,By.ID,'button-publish-course') or not testutils.is_element_visible(wd,By.ID,'button-close-enrollment'):
                    raise Exception("Invitations only,Public access and close enrollment buttons should be visible")
                   ##check with Invitations only
                if wd.find_element_by_id('button-unpublish-course').get_attribute('class') == 'btn.active':
                    if wd.find_element_by_css_selector(".block.showhidden").get_attribute('disabled') != "true":
                        raise Exception("Public catalog block should be disabled while course is shared by inbvitations only")
                              # checking: can not able to share through FB,Twitter
                    if not wd.find_element_by_css_selector('.btn.btn-primary.disabled').is_displayed():
                        raise Exception('Share button should be visible') 
                             #Email invitations 
                    self.email_invitations()
                else:
                    raise Exception("Invitations only button should be active by default")      #comment in local
                    ###Public Access####
                wd.find_element_by_id('button-publish-course').click()
                if testutils.is_element_visible(wd,By.CSS_SELECTOR,".block") != True:
                    raise Exception("Public catalog block should be enabled while course is shared by public access")
                if wd.find_element_by_id("course-free-checkbox").get_attribute('checked') != 'true':
                    raise Exception('Free of charge should be selected by default')              #comment in local
                 #Email Invitations
                self.email_invitations()
                 #Share    
                if not wd.find_element_by_css_selector('.btn-primary.dropdown-toggle').is_displayed():
                    raise Exception('Share button should be visible')
                ## update course enrollment fees
                wd.find_element_by_xpath("(//input[@name='course_pricing'])[2]").click()
                wd.find_element_by_id('course-price-input').click()
                wd.find_element_by_id('course-price-input').clear()
                wd.find_element_by_id('course-price-input').send_keys('5')
                wd.find_element_by_id('btn-update-course-price').click()
                wd.find_element_by_id('button-publish-in-catalog').click()
                wd.refresh()
                time.sleep(4)
                testutils.wait_for(wd,By.ID,"course-paid-checkbox")
                if testutils.wait_for(wd,By.ID,"course-paid-checkbox").get_attribute('checked') != 'true':
                    raise Exception("Charge should be seleccted")
                if wd.find_element_by_id('course-price-input').get_attribute('value') != '5':
                    raise Exception("Charge should be 5 but it is "+wd.find_element_by_id('course-price-input').get_attribute('value'))  
                 ##Free public course
                wd.find_element_by_xpath("(//input[@name='course_pricing'])[1]").click()
                wd.find_element_by_id('btn-update-course-price').click()
                wd.refresh()
                 #Check data
                testutils.wait_for(wd,By.ID,"course-paid-checkbox")
                if testutils.wait_for(wd,By.ID,"course-free-checkbox").get_attribute('checked') != 'true':
                    raise Exception("Course is free button should be selected")
                if wd.find_element_by_id('course-price-input').get_attribute('value') != '0':
                    raise Exception("Charge should be 0 but it is "+wd.find_element_by_id('course-price-input').get_attribute('value'))   
                 ##Close enrollment   
                testutils.wait_for(wd,By.ID,'button-close-enrollment').click()
                testutils.wait_for(wd,By.ID,'button-open-enrollment') 
                if  testutils.is_element_visible(wd,By.ID,'button-open-enrollment') and wd.find_element_by_xpath("//div[@id='invite-url-container']/p").text == '':
                    raise Exception("Could not see open enrollment button after closing enrollment")
            
            if args.j <= 7:
                # delete course
                wd.find_element_by_id('course-button-settings').click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                wd.find_element_by_id('btn-delete-course').click()
                wd.find_element_by_id('confirmation_input').click()
                wd.find_element_by_id('confirmation_input').click()
                wd.find_element_by_id('confirmation_input').send_keys("DELETE")
                time.sleep(0.1)
                wd.find_element_by_id('confirm-delete-button').click()
                wd.find_element_by_id('bt-course').click()
                testutils.wait_for(wd, By.ID, 'course-new-link')
                if testutils.is_element_present(wd, By.XPATH, "//div[@class='span9']//div[.='"+coursename+"']"):
                    raise Exception("Course "+coursename+" should not be in dashboard after delete!")
            
        except Exception, e:
            testutils.fail_test(self)
            raise
            
    def email_invitations(self):
        wd = self.wd;
        wd.find_element_by_id("button-invite-email").click() 
                    ##Check buttons and fields
        if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.close') or not testutils.is_element_visible(wd,By.CSS_SELECTOR,"a.btn.btn-primary"):
            raise Exception("Close and Send Invitations button should be visible")   
        if not testutils.is_element_present(wd,By.ID,"invite-emails-input") and not testutils.is_element_present(wd,By.ID,"invite-summary") == True:
            raise Exception("Invite email and invite summary fields are not present")
        wd.find_element_by_id("invite-emails-input").click()
        wd.find_element_by_id("invite-emails-input").clear() 
        wd.find_element_by_id("invite-emails-input").send_keys("aaaaa")
        wd.find_element_by_css_selector("a.btn.btn-primary").click()
        if not testutils.is_element_visible(wd,By.ID,"invite-error-type-box") and testutils.wait_for(wd,By.ID,"invite-error-type-box").text != "Some of the addresses are invalid, please check.":
            raise Exception("Entered invalid email address but it does not show any error message")
        wd.find_element_by_id("invite-emails-input").click()
        wd.find_element_by_id("invite-emails-input").clear() 
        wd.find_element_by_id("invite-emails-input").send_keys("shanthi.am@cloudberrytec.com")
        wd.find_element_by_id("send-invite").click()  
        return
            
    def tearDown(self):
        testutils.tear_down(self)
