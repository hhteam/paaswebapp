#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.participants'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'participants@127.0.0.1'
        self.studentname = 'student@127.0.0.1'
        self.coursename = 'participants - course'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.delete(self.args, 'user', self.studentname)
        testdata.require(self.args, 'user', self.studentname)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.studentname, self.coursename)
        
    def runTest(self):
        wd = self.wd;args=self.args;username=self.username;coursename = self.coursename;studentname = self.studentname 
        try:
            testutils.login(wd,args.target,username)
            testutils.go_to_course(wd, coursename)
            if args.j <= 1:
                testutils.wait_for(wd,By.ID,'course-button-participants').click()
                      ##check all buttons 
                if not testutils.is_element_present(wd,By.ID,'button-invite-email') and not testutils.is_element_present(wd,By.ID,'button-share-link') and not testutils.is_element_present(wd,By.ID,'btn_edit'):
                    raise Exception("Email Invitation,share,edit buttons are  not dispalyed")
                testutils.wait_for(wd,By.ID,'button-invite-email').click()
                time.sleep(1)
                testutils.wait_for(wd,By.ID,'invite-emails-input').click()
                wd.find_element_by_link_text('Send invitations').click()
                if wd.find_element_by_id('invite-error-type-box').text != "Some of the addresses are invalid, please check.":
                    raise Exception("email addresses are invalid, please check")
                wd.find_element_by_id('invite-emails-input').clear()
                wd.find_element_by_id('invite-emails-input').send_keys('shanthi.am@gmail.com')
                wd.find_element_by_id('invite-summary').click()
                wd.find_element_by_id('invite-summary').clear()
                wd.find_element_by_id('invite-summary').send_keys("Accept this invitation for access course")
                wd.find_element_by_link_text('Send invitations').click()
                time.sleep(0.5)
                msg = wd.find_element_by_id('page-warnings').text
                if msg.splitlines()[1] != "Invitation sent. Participants will be listed on this page after they have accepted invitations.":
                    raise Exception("Instructor can not able to send email invitations")
                       ##share --Invitations only [settings]
                wd.find_element_by_id('course-button-settings').click() 
                testutils.wait_for(wd,By.ID,'button-unpublish-course').click()
                if not wd.find_element_by_css_selector('.btn.btn-primary.disabled').is_displayed():
                        raise Exception('Share button should be visible') 
                        ####Testing in participants page   
                testutils.wait_for(wd,By.ID,'course-button-participants').click()
                if not wd.find_element_by_css_selector('.btn.btn-primary.disabled').is_displayed():
                        raise Exception('Share button should be visible') 
                   ##Go to settings tab
                wd.find_element_by_id('course-button-settings').click()
                wd.find_element_by_id('button-publish-course').click()
                wd.find_element_by_id('course-button-participants').click()
                        ##share --Public access [settings]
                if not wd.find_element_by_id('button-share-link').is_displayed():
                   raise Exception('Share button should be visible')
                #if not testutils.is_element_visible(wd,By.ID,'button-invite-facebook'):
                    #raise Exception("Facebook link should  be visible to share the courselink when it is 'Public access'")
                #if not testutils.is_element_visible(wd,By.ID,'button-invite-twitter'):
                    #raise Exception("Twitter link should  be visible to share the courselink when it is 'Public access'")
                #wd.find_element_by_css_selector('.close').click()   
                      ##check participants should not be removable [public access]
                wd.find_element_by_id('btn_edit').click()
                if  testutils.is_element_visible(wd,By.CSS_SELECTOR,'.small-remove-button'):
                    raise Exception("Public access course:Can remove participants but it can not be removable")       
                
            if args.j <= 2:
                
                ##promote student to instructor    
                wd.find_elements_by_css_selector('.btn-change-role')[1].click()
                wd.find_element_by_id('btn-save').click()
                 ##Check 
                if  wd.find_elements_by_css_selector('.teacher-label')[2].text != 'Instructor':
                    raise Exception("Not able to promote student to instructor")
                testutils.logout(wd)
                testutils.login(wd,args.target,studentname)
                time.sleep(2)
                testutils.wait_for(wd,By.CSS_SELECTOR,'DIV.subtitle-blue').click()
                time.sleep(2)
                if not testutils.is_element_visible(wd,By.ID,'course-button-settings'):
                    raise Exception('Student can not able to edit course details after promoted to instructor but student should be able to do')
                testutils.logout(wd)
                testutils.login(wd,args.target,username)
                time.sleep(1)
            
            if args.j <= 3:
                 ##Depromote to student
                testutils.go_to_course(wd, coursename)
                wd.find_element_by_id('course-button-participants').click()
                wd.find_element_by_id('btn_edit').click()
                wd.find_elements_by_css_selector('.btn-change-role')[1].click()
                wd.find_element_by_id('btn-save').click()
                ##Check 
                print wd.find_elements_by_css_selector('.btn-change-role')[1].text
                if  wd.find_elements_by_css_selector('.btn-change-role')[1].text != '':
                    raise Exception("Not able to depromote student from instructor")
                testutils.logout(wd)
                testutils.login(wd,args.target,studentname)
                time.sleep(2)
                testutils.wait_for(wd,By.CSS_SELECTOR,'DIV.subtitle-blue').click()
                time.sleep(2)
                if testutils.is_element_visible(wd,By.ID,'course-button-settings'):
                    raise Exception('Student should not able to edit course details after depromoted to student ')
                testutils.logout(wd)
                testutils.login(wd,args.target,username)
                testutils.go_to_course(wd, coursename)
                time.sleep(1)
                  
            if args.j <= 4:
               ##Delete participant --Invitations only
                wd.find_element_by_id('course-button-settings').click()
                wd.find_element_by_id('button-unpublish-course').click()
                wd.find_element_by_id('course-button-participants').click()
                wd.find_element_by_id('btn_edit').click()
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.small-remove-button'):
                    raise Exception("Can not edit participants")
                wd.find_elements_by_css_selector('.small-remove-button')[1].click()
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.removed-user'):
                    raise Exception("Can not able to remove participants")
                    ##Cancel button
                wd.find_element_by_id('btn-cancel').click()
                time.sleep(1)  
                elemsCancel = wd.find_elements_by_xpath("//span[contains(@class, 'thumbnail')]") 
                if len(elemsCancel) != 3:
                    raise Exception("Can not be cancel remove participant")
                   ##Save 
                wd.find_element_by_id('btn_edit').click()
                wd.find_elements_by_css_selector('.small-remove-button')[1].click()
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.removed-user'):
                    raise Exception("Can not able to remove participants")
                wd.find_element_by_id('btn-save').click()
                elems = wd.find_elements_by_xpath("//span[contains(@class, 'thumbnail')]") 
                if len(elems) > 2 or  len(elems) != 2:
                    raise Exception("Instructor can not able to remove the participant")
                time.sleep(1)
                
        except Exception, e:
            testutils.save_screenshot(self)
            raise
        
    def tearDown(self):
        testutils.tear_down(self)
