#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils as t
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.wizard.create_course'

    def setUp(self):
        t.set_up(self)
        
    def testdata_setup(self):
        self.username = 'teacher@127.0.0.1'
        self.coursename = 'Create Course Test Course'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename
        try:
            t.login(wd, args.target, username)
            
            if args.j <= 1:
                t.wait_for(wd, By.XPATH, '//*[@id="course-new-link"]').click()
                if t.is_element_present(wd, By.ID, 'course-section-add-new'):
                    raise Exception("Should not have new section link on wizard first step")
                t.wait_for(wd, By.ID, "input-course-fullname").click()
                t.wait_for(wd, By.ID, "input-course-fullname").clear()  
                t.wait_for(wd, By.ID, "input-course-fullname").send_keys(" ")
                if t.wait_for(wd,By.CSS_SELECTOR,"div.tooltip-inner").text != "This field can not be empty":
                    raise  Exception("Course name field can not be empty")    
                t.wait_for(wd, By.CSS_SELECTOR, '.full-name.editable').click()
                t.wait_for(wd, By.CSS_SELECTOR, '.full-name.editable').send_keys("Create Course Test Course Create Course Test CourseCreate Course Test CourseCreate Course Test CourseCreate Course Test Course")
                if t.wait_for(wd, By.CSS_SELECTOR,'.tooltip-inner').text != "Course name exceeds 120 characters limit":
                    raise Exception("Course name exceeds 120 characters limit")
                t.wait_for(wd, By.CSS_SELECTOR, '.full-name.editable').click()
                t.wait_for(wd, By.CSS_SELECTOR, '.full-name.editable').clear()  
                t.wait_for(wd, By.CSS_SELECTOR, '.full-name.editable').send_keys(coursename)
                #t.wait_for(wd, By.CSS_SELECTOR, '.short-name.editable').click()
                #shortname = 'CCTC#'+str(time.time())[0:5]
                #t.wait_for(wd, By.CSS_SELECTOR, '.short-name.editable').send_keys(shortname)
                t.wait_for(wd, By.ID, 'course-start-date-input').click()
                t.wait_for(wd, By.CSS_SELECTOR, '.course-content').click()
                t.wait_for(wd, By.ID, 'course-end-date-input').click()
                t.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                t.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"16\")').click();");
                wd.find_element_by_xpath("//select/option[4]").click()
                #t.wait_for(wd, By.ID, 'listCategory').click()
                #t.wait_for(wd, By.CSS_SELECTOR, 'option[value="3"]').click()    # business and law
                t.wait_for(wd, By.ID, 'btn_edit_save').click()
            if args.j <= 2:   
                # overview
                t.wait_for(wd, By.CSS_SELECTOR, '.summary .editable').click()
                t.wait_for(wd, By.CSS_SELECTOR, '.summary .editable').send_keys('Summary for the first section')
                #skills
                # check skill label appears
                if not t.is_element_visible(wd, By.ID, 'skills-input'):
                    raise Exception("Skill label should be visible")
                #add one skill    
                t.wait_for(wd, By.ID, 'skills-input').click()
                t.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill1")
                t.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) == 0:
                    raise Exception("There should be one skill label")
                # check skill label is removed on click
                wd.find_elements_by_css_selector('.skill-label i')[0].click()
                if t.is_element_visible(wd, By.CSS_SELECTOR, '.skill-label'):
                    raise Exception("There should be no skill labels")
                # check skill is actually saved
                t.wait_for(wd, By.ID, 'skills-input').click()
                t.wait_for(wd, By.ID, 'skills-input').clear()
                t.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill 2")
                t.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                t.wait_for(wd, By.ID, 'btn_edit_save').click()
                t.wait_for(wd, By.CSS_SELECTOR, '.btn-primary')
                wd.refresh()
                t.wait_for(wd, By.CSS_SELECTOR, '.btn-primary')
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) == 0:
                    raise Exception("There should be one skill label")
                # add two more skills in one go
                t.wait_for(wd, By.CSS_SELECTOR, '.btn-primary').click()
                t.wait_for(wd, By.ID, 'skills-input').click()
                t.wait_for(wd, By.ID, 'skills-input').clear()
                t.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill 3, Test Skill 4")
                t.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.skills-edit .skill-label'))))
                # remove one, add one
                wd.find_elements_by_css_selector('.skill-label i')[1].click()
                t.wait_for(wd, By.ID, 'skills-input').click()
                t.wait_for(wd, By.ID, 'skills-input').clear()
                t.wait_for(wd, By.ID, 'skills-input').send_keys("Test")
                t.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.skills-edit .skill-label'))))
                t.wait_for(wd, By.ID, 'btn_edit_save').click()
                t.wait_for(wd, By.CSS_SELECTOR, '.btn-primary')
                wd.refresh()
                t.wait_for(wd,By.ID,'bt-course').click()
                # check data
                if t.wait_for(wd, By.CSS_SELECTOR, '.subtitle-blue').text != coursename:
                    raise Exception("Full name is not correct, should be: "+coursename+", is "+t.wait_for(wd, By.CSS_SELECTOR, '.subtitle-blue').text)
                time.sleep(0.5)    
                wd.find_element_by_css_selector('.subtitle-blue').click() 
                t.wait_for(wd,By.CSS_SELECTOR,'.btn-primary')
                if len(wd.find_elements_by_css_selector('.skills-edit .skill-label')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.skills-edit .skill-label'))))
                t.wait_for(wd,By.ID,'course-button-settings').click()
                category = t.wait_for(wd,By.ID,'listCategory').get_attribute("value")
                if category != '3':
                    raise Exception('Category is not correct')
                
        except Exception, e:
            t.fail_test(self)
            raise
            
    def tearDown(self):
        t.tear_down(self)
