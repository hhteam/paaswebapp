#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys
import os


class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.wizard.import_course'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'teacher@127.0.0.1'
        self.coursename = 'Import Course'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'course', self.coursename)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename
        tdata = { 'userid': testdata.get(args, 'user', username, 'username', 'id') }
        env = testdata.get_env(args)
        try:
            testutils.login(wd, args.target, username)
            
            if args.j <= 1:
                testutils.wait_for(wd, By.XPATH, '//*[@id="course-new-link"]').click()
                testutils.wait_for(wd, By.ID, "btn-import").click()
                ## clicking back goes back
                testutils.wait_for(wd, By.ID, "course-import-back").click()
                testutils.wait_for(wd, By.ID, "btn-import").click()
                ## clicking cancel goes to dashboard
                testutils.wait_for(wd, By.XPATH, '//*[@id="btn_edit_cancel"]').click()
                testutils.wait_for(wd, By.XPATH, '//*[@id="course-new-link"]').click()
                testutils.wait_for(wd, By.ID, "btn-import").click()
                
                # Dummy file upload
                testutils.upload_file(args, wd, username, 'import_course.zip', By.ID, "course-import-upload-file")
                
                # Task selections
                testutils.wait_for(wd, By.ID, 'import-tasks-selections')
                # course name should be contained in modal dialog
                if testutils.wait_for(wd, By.CSS_SELECTOR, '.modal-body').text.find(coursename) == -1:
                    raise Exception("FAIL: Course name should be contained in modal dialog")
                # checkboxes for tasks
                if testutils.wait_for(wd, By.CSS_SELECTOR, '.modal-body').find_elements_by_css_selector('input[type="checkbox"]') == 0:
                    raise Exception("FAIL: Tasks should be contained in modal dialog as checkboxes")
                # task name in label
                if testutils.wait_for(wd, By.CSS_SELECTOR, '.modal-body').find_elements_by_css_selector('label')[0].text.find('Upload a single file') == -1:
                    raise Exception("FAIL: Task name should be contained in a label for checkbox")
                testutils.wait_for(wd, By.ID, 'import-course-tasks-next').click()
                
                # course menu
                #~ testutils.wait_for(wd, By.ID, "course-menu")
                ## Something weird - cannot wait_for since some modal dialog appears somewhere during the wait and hangs
                ## the test. Tried to patch wait_for to kill modal dialogs but it seems to hang in find_element().is_displayed
                ## so not possible to catch. Could think of only waiting for 2 seconds here for now.
                time.sleep(3)
                # course name
                if testutils.wait_for(wd, By.CSS_SELECTOR, ".full-name").text != coursename:
                    raise Exception("FAIL: Course full name 'Import Course' should be present")
                # edit mode
                if not testutils.wait_for(wd, By.XPATH, '//*[@id="wizard-step-two"]').is_displayed():
                    raise Exception("FAIL: Should be in wizard step two mode")
                # save course and check
                testutils.wait_for(wd, By.ID, 'btn_edit_save').click()
                # Edit button becomes visible
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn_edit")
                # Go to tasks
                testutils.wait_for(wd, By.XPATH, "//*[@id='course-button-tasks']").click()
                testutils.wait_for(wd, By.ID, "btNewTask")
                
                # Task should be here
                if wd.find_elements_by_css_selector("a.task-name span")[0].text.find("Upload a single file") == -1:
                    raise Exception("FAIL: Task 'Upload a single file' is missing on tasks page")
                    
                # Go to discussions and check a forum exists
                testutils.wait_for(wd, By.XPATH, "//*[@id='course-button-forum']").click()
                # General discussions forum should be here
                elems = wd.find_elements_by_css_selector('.forum-heading')
                if len(elems) == 0:
                    raise Exception("FAIL: There should be at least one forum")
                # New discussion button should exist
                testutils.wait_for(wd, By.CSS_SELECTOR, "td#forum-box button")
                testdata.delete(args, 'course', coursename)
            
            if args.j <= 2:
                # Lets do again, this time not importing the task
                wd.get(args.target+'/app/a/courses/import')
                
                # Upload
                testutils.wait_for(wd, By.ID, "course-import-upload-file")
                testutils.upload_file(args, wd, username, 'import_course.zip', By.ID, "course-import-upload-file")
                
                # Task selections
                testutils.wait_for(wd, By.ID, 'import-tasks-selections')
                
                wd.find_elements_by_css_selector('input[type="checkbox"]')[0].click()  ## deselect
                testutils.wait_for(wd, By.ID, 'import-course-tasks-next').click()
                
                time.sleep(3)
                # save course and check
                testutils.wait_for(wd, By.ID, 'btn_edit_save').click()
                # Go to tasks
                testutils.wait_for(wd, By.XPATH, "//*[@id='course-button-tasks']")
                testutils.wait_for(wd, By.XPATH, "//*[@id='course-button-tasks']").click()
                testutils.wait_for(wd, By.ID, "btNewTask")
                
                # Task should NOT be here
                if len(wd.find_elements_by_css_selector("a.task-name span")) > 0:
                    raise Exception("FAIL: There should be no tasks in task page")
                
            if args.j <= 3:
                # Lets try an invalid file
                wd.get(args.target+'/app/a/courses/import')
                testutils.wait_for(wd, By.ID, "course-import-upload-file")
                testutils.upload_file(args, wd, username, 'import_course.zip', By.ID, "course-import-upload-file", "import_course.rar")
                
                # Warning dialog should say about wrong type
                if wd.find_element_by_id("page-warnings").text.find('Only file types of zip, mbz are accepted') == -1:
                    raise Exception("FAIL: Warning should be shown about rar upload")
                    
                # Dismiss warning
                testutils.wait_for(wd, By.CSS_SELECTOR, "#page-warnings button").click()
            
            if args.j <= 4:
                # Do another import but this time skip the task selection dialog
                testdata.delete(args, 'course', coursename)
                wd.get(args.target+'/app/a/courses/import')
                
                # Upload
                testutils.wait_for(wd, By.ID, "course-import-upload-file")
                testutils.upload_file(args, wd, username, 'import_course.zip', By.ID, "course-import-upload-file")
                
                # Task selections
                testutils.wait_for(wd, By.ID, 'import-tasks-selections')
                
                # Skip box by clicking outside it
                testutils.wait_for(wd, By.CSS_SELECTOR, '.modal-backdrop').click()
                time.sleep(3)
                
                # We should be on course edit in next phase
                testutils.wait_for(wd, By.ID, 'btn_edit_save').click()
                # Go to tasks
                testutils.wait_for(wd, By.XPATH, "//*[@id='course-button-tasks']")
                testutils.wait_for(wd, By.XPATH, "//*[@id='course-button-tasks']").click()
                testutils.wait_for(wd, By.ID, "btNewTask")
                
                # Task should be here
                if len(wd.find_elements_by_css_selector("a.task-name span")) == 0:
                    raise Exception("FAIL: There should be tasks in task page")
                    
                # There should be no import_course.zip file in temp folder at this stage
                if os.access('/opt/'+env+'data/temp/files/'+str(tdata['userid'])+'/import_course.zip', os.F_OK):
                    raise Exception('FAIL: Course import file "/opt/'+env+'data/temp/files/'+str(tdata['userid'])+'/import_course.zip" should have been deleted by now')
                
            if args.j <= 5:
                # Upload an invalid zip file to generate a generic error in importing
                testutils.random_file('/opt/'+env+'data/temp/files/'+str(tdata['userid'])+'/import_course.zip')
                wd.get(args.target+'/app/a/courses/import')
                testutils.wait_for(wd, By.ID, "course-import-upload-file")
                wd.execute_script("$('#testdata-filename').val('import_course.zip');")
                testutils.wait_for(wd, By.ID, "course-import-upload-file").click()
                wd.execute_script("window.testFileUploadForm.fileupload('add', { fileInput: $('#testdata-filename') });")
                
                # Check for warnings
                if wd.find_element_by_id("page-warnings").text.find('There was a problem in importing the course backup') == -1:
                    raise Exception("FAIL: Warning should be shown about failed import")
                    
                # There should be no import_course.zip file in temp folder at this stage
                if os.access('/opt/'+env+'data/temp/files/'+str(tdata['userid'])+'/import_course.zip', os.F_OK):
                    raise Exception('FAIL: Course import file "/opt/'+env+'data/temp/files/'+str(tdata['userid'])+'/import_course.zip" should have been deleted by now')
                    
                # Dismiss warning
                testutils.wait_for(wd, By.CSS_SELECTOR, "#page-warnings button").click()
            
            if args.j <= 6:
                # Import course, then cancel it
                testdata.delete(args, 'course', coursename)
                wd.get(args.target+'/app/a/courses/import')
                
                # Upload
                testutils.wait_for(wd, By.ID, "course-import-upload-file")
                testutils.upload_file(args, wd, username, 'import_course.zip', By.ID, "course-import-upload-file")
                
                # Task selections
                testutils.wait_for(wd, By.ID, 'import-tasks-selections')    
                testutils.wait_for(wd, By.ID, 'import-course-tasks-next').click()
                
                time.sleep(3)
                # cancel edit mode
                testutils.wait_for(wd, By.ID, 'btn_edit_cancel').click()
                testutils.wait_for(wd, By.CSS_SELECTOR, '.modal-footer a.btn-danger')
                testutils.javascript(wd, "$('.modal-footer a.btn-danger').click();");
                
                # dashboard should be visible
                time.sleep(1)
                testutils.wait_for(wd, By.ID, "course-new-link")
                
                # course should not exist in moodle
                tdata['deletedcourse'] = testdata.get(args, 'course', coursename, 'fullname', 'id')
                if tdata['deletedcourse']:
                    raise Exception("FAIL: Imported course should have been deleted after cancel in import")
            
            if args.j <= 7:
                # Import course that has no tasks
                testdata.delete(args, 'course', 'AbraKab')
                wd.get(args.target+'/app/a/courses/import')
                
                # Upload
                testutils.wait_for(wd, By.ID, "course-import-upload-file")
                testutils.upload_file(args, wd, username, 'abrakab.mbz', By.ID, "course-import-upload-file")
                
                # Wait for edit page
                time.sleep(3)
                testutils.wait_for(wd, By.ID, 'btn_edit_cancel')
                testutils.wait_for(wd, By.ID, 'btn_edit_cancel').click()
                time.sleep(3)
                #FIXME: this is quite weird. Fails here only on kenny, for reasons really unknown. But clicking it *again* helps :P
                if testutils.is_element_visible(wd, By.ID, 'btn_edit_cancel'):
                    testutils.wait_for(wd, By.ID, 'btn_edit_cancel').click()
                testutils.wait_for(wd, By.CSS_SELECTOR, '.modal-footer a.btn-danger')
                testutils.javascript(wd, "$('.modal-footer a.btn-danger').click();");
            
            
        except Exception, e:
            testutils.save_screenshot(self)
            raise
            
    def tearDown(self):
        testutils.tear_down(self)

