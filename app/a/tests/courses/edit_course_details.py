#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import traceback
import time
import sys


class CBTestTestCase(unittest.TestCase):

    _testname = 'courses.edit_course_details'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'editcourse@127.0.0.1'
        self.coursename = 'Edit Course'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.delete(self.args, 'user_is_teacher',  self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        
        
    def runTest(self):
        wd = self.wd;args=self.args;username=self.username;coursename = self.coursename  
        try:
            testutils.login(wd,args.target,username)
            testutils.go_to_course(wd, coursename)
            if args.j <= 1:
                    #Check edit mode is working or not
                time.sleep(2)  
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                testutils.wait_for(wd,By.ID,'course-section-add-new')
                testutils.wait_for(wd, By.CSS_SELECTOR, ".no-overflow.editable.multiline").click()
                if not testutils.is_element_visible(wd, By.CSS_SELECTOR, '.no-overflow.editable.multiline'): 
                    raise Exception("Edit mode is not working,Could not edit")
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                    #Edit Contents  
                testutils.go_to_course(wd, coursename)
                time.sleep(0.5) 
                testutils.wait_for(wd, By.ID, "course-button-settings").click()
                testutils.wait_for(wd, By.ID, 'btn-save-general-settings')
                   #Edit Course Title
                testutils.wait_for(wd, By.ID, "input-course-fullname").click()
                testutils.wait_for(wd, By.ID, "input-course-fullname").clear()  
                testutils.wait_for(wd, By.ID, "input-course-fullname").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,"div.tooltip-inner").text != "This field can not be empty":
                    raise  Exception("Course name field can not be empty")
                testutils.wait_for(wd, By.ID, "input-course-fullname").clear()      
                testutils.wait_for(wd, By.ID, "input-course-fullname").send_keys("edit course title for test Course name exceeds 120 characters limitCourse name exceeds 120 characters limitCourse name exceeds 120 characters limitCourse name exceeds 120 characters limitafaddfsdsssssss") 
                if testutils.wait_for(wd,By.CSS_SELECTOR,"div.tooltip-inner").text != "Course name exceeds 120 characters limit":
                    raise  Exception("Course name exceeds 120 characters limit")
                Fullname = "Edit COURSE TITLE for test " +time.strftime("%x") 
                testutils.wait_for(wd, By.ID, "input-course-fullname").click()
                testutils.wait_for(wd, By.ID, "input-course-fullname").clear()      
                testutils.wait_for(wd, By.ID, "input-course-fullname").send_keys(Fullname)
                   #Edit Course Category
                wd.find_element_by_xpath("//select/option[10]").click()   
                #testutils.wait_for(wd, By.ID, "listCategory").click()  
                #testutils.wait_for(wd, By.CSS_SELECTOR, 'option[value="12"]').click()
                   #Edit startdate and End date of course
                start_date = testutils.wait_for(wd, By.ID, 'course-start-date-input').get_attribute("value")
                testutils.wait_for(wd, By.ID, 'course-end-date-input').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"14\")').click();");
                end_date = testutils.wait_for(wd, By.ID, 'course-end-date-input').get_attribute("value")
                    #save changes
                time.sleep(0.5) 
                testutils.wait_for(wd, By.ID, "btn-save-general-settings").click()
                time.sleep(2)
                count =0
                while  count < 10 and testutils.wait_for(wd,By.ID,"btn-save-general-settings").get_attribute("disabled") != 'true':
                    count = count+1
                    time.sleep(3)
                    if count == 10:
                        raise Exception()
                            #Check Changes
                if testutils.wait_for(wd, By.ID, "input-course-fullname").text != Fullname:
                    raise Exception("Course fullname is not correct")
                category = testutils.wait_for(wd, By.ID, "listCategory").get_attribute('value')
                if category != "9":
                    raise Exception("Category name is not correct, it is "+testutils.wait_for(wd, By.ID, "listCategory").get_attribute('value'))
                if testutils.wait_for(wd, By.ID, 'course-start-date-input').get_attribute("value") != start_date:
                    raise Exception("Start date is not correct")
                if testutils.wait_for(wd, By.ID, 'course-end-date-input').get_attribute("value") != end_date:
                    raise Exception("End date is not correct")
                    
            if args.j <= 2:
                  ##Editing Course overview
                coursename = Fullname
                testutils.go_to_course(wd, coursename)
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, ".no-overflow.editable.multiline").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, ".no-overflow.editable.multiline").send_keys("This Course has amazing features")
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                time.sleep(1)
                wd.refresh()
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary")
                if testutils.wait_for(wd, By.CSS_SELECTOR, ".no-overflow.editable.multiline").text != "This Course has amazing features":
                    raise Exception("Course overview is not correct")
               
            if args.j <= 3:
                        # Adding new topic 
                testutils.wait_for(wd,By.ID,"course-section-add-new").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").clear()
                wd.find_element_by_css_selector(".no-overflow.editable.multiline").click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,"div.tooltip-inner").text != "This field can not be empty":
                    raise Exception("Topic name should not be empty" )
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").send_keys("Character limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reachedCharacter limit is reached")       
                if testutils.wait_for(wd,By.CSS_SELECTOR,"div.tooltip-inner").text != "Character limit has been reached":
                    raise Exception("Topic name:Character limit has been reached")
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").clear()    
                newtopic = "Adding New topic for test"+" "+time.strftime("%b")
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").send_keys(newtopic)
                wd.find_element_by_css_selector(".no-overflow.editable.multiline").click()
                wd.find_element_by_css_selector(".no-overflow.editable.multiline").clear()
                wd.find_element_by_css_selector(".no-overflow.editable.multiline").send_keys("Adding Test Instructions")
                  #Adding regular LTI app.
                wd.find_element_by_css_selector('.btn-add').click()
                wd.find_elements_by_css_selector('.add-item')[1].click()
                testutils.wait_for(wd,By.ID,'radio-0').click()
                wd.find_element_by_id('ltisave').click()
                testutils.wait_for(wd,By.ID,'ctoolname').click()
                testutils.wait_for(wd,By.ID,'ctoolname').send_keys('Piazza')
                testutils.wait_for(wd,By.ID,'ctoolurl').click()
                testutils.wait_for(wd,By.ID,'ctoolurl').send_keys('https://piazza.com/basic_lti')
                testutils.wait_for(wd,By.ID,'ciconurl').click()
                testutils.wait_for(wd,By.ID,'ciconurl').send_keys('http://www.edu-apps.org/tools/piazza/icon.png')
                #testutils.wait_for(wd,By.ID,'ckey').click()
                #testutils.wait_for(wd,By.ID,'ckey').send_keys('P001')
                #testutils.wait_for(wd,By.ID,'csecret').click()
                #testutils.wait_for(wd,By.ID,'csecret').send_keys('P001002')
                testutils.wait_for(wd,By.ID,'cusername').click()
                testutils.wait_for(wd,By.ID,'cemail').click()
                testutils.wait_for(wd,By.ID,'ltisave').click()
                time.sleep(4)
                if  testutils.wait_for(wd,By.ID,'ltisave').text == 'Done':
                    testutils.wait_for(wd,By.ID,'ltisave').click()
                time.sleep(1)
                  #save changes 
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                time.sleep(2)
                        #Check added data
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary")        
                if testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").text != newtopic:
                    raise Exception("New topic name is not correct")
                if wd.find_element_by_css_selector(".no-overflow.editable.multiline").text != "Adding Test Instructions":
                    raise Exception("Could not add topic instructions") 
                         # LTI
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.small-attachment.block'):
                    raise Exception("Edu App 'Piazza' should be visible,but it is not visible")
                testutils.wait_for(wd,By.CSS_SELECTOR,'.small-attachment.block').click()
                    
            if args.j <= 4:
                      #Editing topic name and instructions
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").clear()    
                edittopic = "Editing  topic for test"+" "+time.strftime("%b")
                testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").send_keys(edittopic)
                wd.find_element_by_css_selector(".no-overflow.editable.multiline").click()
                wd.find_element_by_css_selector(".no-overflow.editable.multiline").clear()
                wd.find_element_by_css_selector(".no-overflow.editable.multiline").send_keys("Editing Test Instructions")
                 #Edit LTI app (name,url ...)
                wd.find_element_by_css_selector('A.btn.dropdown-toggle.ignored_link.btn-add').click()
                wd.find_elements_by_css_selector('.add-item')[1].click()
                wd.find_element_by_xpath("//div[@id='ltitoolsgrid']/ul/div/a/input").click()
                wd.find_element_by_id('ltisave').click()
                time.sleep(2)
                testutils.wait_for(wd,By.ID,'ltiedit').click()
                testutils.wait_for(wd,By.ID,'ctoolname').click()
                testutils.wait_for(wd,By.ID,'ctoolname').clear()
                testutils.wait_for(wd,By.ID,'ctoolname').send_keys('Prulu')
                testutils.wait_for(wd,By.ID,'ctoolurl').click()
                testutils.wait_for(wd,By.ID,'ctoolurl').clear()
                testutils.wait_for(wd,By.ID,'ctoolurl').send_keys('https://prulu.com/accounts/registration/lti/')
                testutils.wait_for(wd,By.ID,'ciconurl').click()
                testutils.wait_for(wd,By.ID,'ciconurl').clear()
                testutils.wait_for(wd,By.ID,'ciconurl').send_keys('http://www.edu-apps.org/tools/prulu/icon.png')
                testutils.wait_for(wd,By.ID,'ltiedit').click()
                time.sleep(1)
                testutils.wait_for(wd,By.ID,'ltisave').click()
                time.sleep(4)
                testutils.wait_for(wd,By.ID,'ltisave').click()
                time.sleep(1)
                  #save  
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                time.sleep(1)
                wd.refresh()
                #Check edited data
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary")    
                if testutils.wait_for(wd, By.CSS_SELECTOR, "span.editable.livevalidation").text != edittopic:
                    raise Exception("Could not update topic name to course")
                if wd.find_element_by_css_selector(".no-overflow.editable.multiline").text != "Editing Test Instructions":
                    raise Exception("Could not update topic instructions to course")
                  #LTI    
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                wd.find_element_by_css_selector('A.btn.dropdown-toggle.ignored_link.btn-add').click()
                wd.find_elements_by_css_selector('.add-item')[1].click()
                if wd.find_element_by_xpath("//div[@id='ltitoolsgrid']/ul/div/a/span").text != 'Prulu':
                    raise Exception('Custom Edu app name should be Prulu but it is ' +wd.find_element_by_xpath("//div[@id='ltitoolsgrid']/ul/div/a/span").text)
                wd.find_element_by_id('close').click()
                testutils.wait_for(wd, By.ID, "btn_edit_save").click() 
                time.sleep(1)
                            
            if args.j <= 5:
                   #Topic visiblity check(hide from students)
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                testutils.wait_for(wd, By.XPATH, "//*[contains(@id,'hidesection')]").click()
                   #check changes
                if testutils.wait_for(wd, By.XPATH, "//*[contains(@id,'hidesection')]").text != "Show":#"Not visible to students":
                    raise  Exception() 
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                if wd.find_element_by_xpath("//h3[@class='sectionname block-title']/span").get_attribute("class") != 'hide-section btn active':
                    raise Exception("Topic should be hidden from students but it is visible")
                    #Not visible to students
                testutils.wait_for(wd,By.CSS_SELECTOR,'.btn-primary').click()
                testutils.wait_for(wd, By.XPATH, "//*[contains(@id,'hidesection')]").click()  
                if testutils.wait_for(wd, By.XPATH, "//*[contains(@id,'hidesection')]").text != "Hide":# "Hide from students":
                    raise  Exception() 
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                if wd.find_element_by_xpath("//h3[@class='sectionname block-title']/span").get_attribute("class") != "hide-section btn":
                    raise Exception("Topic should be visible for students but it is hidden")
            
            if args.j <= 6:
                #Delete LTI app.
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                wd.find_element_by_css_selector('A.btn.dropdown-toggle.ignored_link.btn-add').click()
                wd.find_elements_by_css_selector('.add-item')[1].click()
                wd.find_element_by_xpath("//div[@id='ltitoolsgrid']/ul/div/a/input").click()
                wd.find_element_by_id('ltisave').click()
                time.sleep(1)
                testutils.wait_for(wd,By.ID,'ltidelete').click()
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                time.sleep(1)
                wd.refresh()
                #check changes
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                wd.find_element_by_css_selector('A.btn.dropdown-toggle.ignored_link.btn-add').click()
                wd.find_elements_by_css_selector('.add-item')[1].click()
                if wd.find_element_by_xpath("//div[@id='ltitoolsgrid']/ul/div/a/span").text != 'Add an edu app':
                    raise Exception('Could not delete custom Edu app')
                wd.find_element_by_id('close').click()
                testutils.wait_for(wd, By.ID, "btn_edit_save").click() 
                time.sleep(1)
                
            if args.j <= 7:
                ## add one more topic 
                self.add_topic()
                time.sleep(1)
            
            if args.j <= 8:
                ##Change topic position  and  checkong previous and next topic 
                wd.find_elements_by_css_selector('.dropdown-toggle.ignored_link')[2].click()
                wd.find_element_by_xpath("//a[@data-call='changeSectionIndex']").click()
                time.sleep(3)
                wd.find_elements_by_css_selector('.dropdown-toggle.ignored_link')[2].click()
                time.sleep(3)
                topic = wd.find_element_by_xpath("//a[@data-call='changeSectionIndex']").text
                topic =topic[:1]
                if topic != '2':
                    raise Exception('Error:Could not change the position')
                if wd.find_element_by_xpath("//div[@class='pagination pull-left']/ul/li/a").text !='Overview':
                    raise Exception("Previous topic should be Overview but it is "+wd.find_element_by_xpath("//div[@class='pagination pull-left']/ul/li/a").text) 
                if wd.find_element_by_xpath("//div[@class='pagination pull-right']/ul/li/a").text != "Editing  topic for test"+" "+time.strftime("%b"):
                    raise Exception("Next topic should be Editing  topic for test "+time.strftime("%b")+" "+"but it is "+wd.find_element_by_xpath("//div[@class='pagination pull-right']/ul/li/a").text)  
                                      
            if args.j <= 9:
                 #delete topic
                testutils.wait_for(wd,By.CSS_SELECTOR,".btn-primary").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, ".mui-remove-button").click()
                testutils.wait_for(wd, By.ID, "btn_edit_save").click()
                if testutils.is_element_visible(wd,edittopic):
                    raise Exception("Topic is not deleted")
                
        except Exception, e:
            testutils.fail_test(self)
            raise 
            
    def add_topic(self):
        wd = self.wd
        testutils.wait_for(wd,By.ID,'course-section-add-new').click()
        time.sleep(1)
        wd.find_element_by_css_selector("span.editable.livevalidation").click()
        wd.find_element_by_css_selector("span.editable.livevalidation").clear()    
        wd.find_element_by_css_selector("span.editable.livevalidation").send_keys('Lecture1')
        wd.find_element_by_css_selector(".no-overflow.editable.multiline").click()
        wd.find_element_by_css_selector(".no-overflow.editable.multiline").clear()
        wd.find_element_by_css_selector(".no-overflow.editable.multiline").send_keys("Instructions")
        testutils.wait_for(wd, By.ID, "btn_edit_save").click()
        time.sleep(2)
        return
            
    def edit_eduApp(self):
        wd = self.wd
        return
    
    def tearDown(self):
        testutils.tear_down(self)
