#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys
import datetime
from datetime import timedelta,date,datetime
import lxml
from selenium.webdriver.common.keys import Keys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.progress.py'
  
     
    def setUp(self):
        testutils.set_up(self)

    def testdata_setup(self):
        self.username = 'progress@127.0.0.1'
        self.studentname = 'student@127.0.0.1'
        self.studentname2 = 'student_2@127.0.0.1'
        self.coursename = 'progress and gradebook'
        self.taskname = 'Progress task'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.studentname)
        testdata.require(self.args, 'user', self.studentname)
        testdata.delete(self.args, 'user', self.studentname2)
        testdata.require(self.args, 'user', self.studentname2)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        #testdata.require(self.args, 'task_in_course', self.taskname, self.coursename)
        #testdata.delete(self.args, 'task_in_course',  self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        testdata.delete(self.args, 'user_is_teacher',  self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.studentname, self.coursename)
        testdata.delete(self.args, 'user_is_enrolled',  self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.studentname2, self.coursename)
        testdata.delete(self.args, 'user_is_enrolled',  self.coursename)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename; taskname = self.taskname; studentname = self.studentname;studentname2 = self.studentname2
        try:
            testutils.login(wd, args.target, username)
            wd.refresh()
            time.sleep(6)
            testutils.go_to_course(wd,coursename)
            #enable confgure certificate
            testutils.wait_for(wd,By.ID,'course-button-settings').click()
            testutils.wait_for(wd,By.ID,"course-cert_enabled-input").click()
            time.sleep(2)
            wd.find_element_by_id("btn-save-general-settings").click()
            time.sleep(35)
            testutils.wait_for(wd,By.ID,'course-button-tasks').click()
            time.sleep(5)
            testutils.wait_for(wd,By.ID,'btNewTask').click()
            #testutils.wait_for(wd,By.ID,'course-button-progress').click()
            
            if args.j <= 1:
               #create tasks
                testutils.wait_for(wd,By.ID,'btn-task-type-quiz').click()
                wd.find_element_by_id('full-name').click()
                wd.find_element_by_id('full-name').send_keys('Task 1')
                testutils.wait_for(wd,By.CSS_SELECTOR,'.richtext').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.richtext').send_keys('is it working?')
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[0].click()
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[0].send_keys('YES')
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[1].click()
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[1].send_keys('NO')
                wd.find_elements_by_css_selector('.checkbox')[1].click()
                wd.find_element_by_id('btn_edit_save').click()
                time.sleep(1)
                testutils.wait_for(wd,By.ID,'btNewTask').click()
                testutils.wait_for(wd,By.ID,'btn-task-type-quiz').click()
                wd.find_element_by_id('full-name').click()
                wd.find_element_by_id('full-name').send_keys('Task 2')
                testutils.wait_for(wd,By.CSS_SELECTOR,'.richtext').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.richtext').send_keys('is it necessary?')
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[0].click()
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[0].send_keys('YES')
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[1].click()
                wd.find_elements_by_css_selector('.editable.inputfield.livevalidation')[1].send_keys('NO')
                wd.find_elements_by_css_selector('.checkbox')[2].click()
                wd.find_element_by_id('btn_edit_save').click()
                time.sleep(2)
                testutils.logout(wd)
               
            if args.j <= 2:
                #Student login and submit task 
                testutils.login(wd, args.target, studentname)
                testutils.go_to_course(wd,coursename)
                ##check certificate icon on student page 
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.fa.fa-certificate'):
                    raise Exception('Certificate icon is not displayed in student page but it should be display')
                time.sleep(2)    
                testutils.wait_for(wd,By.ID,'course-button-tasks').click()
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click()
                testutils.wait_for(wd,By.ID,'button-quiz-start').click()
                wd.find_elements_by_css_selector('.checkbox')[1].click()
                wd.find_element_by_id('button-quiz-finish').click()
                wd.refresh()
                time.sleep(6)
                testutils.wait_for(wd,By.ID,'bt-course').click()
                testutils.go_to_course(wd,coursename)
                testutils.wait_for(wd,By.ID,'course-button-tasks').click()
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click()
                testutils.wait_for(wd,By.ID,'button-quiz-start').click()
                wd.find_elements_by_css_selector('.checkbox')[0].click()
                wd.find_element_by_id('button-quiz-finish').click()
                wd.refresh()
                time.sleep(6)
                testutils.wait_for(wd,By.ID,'bt-course').click()
                testutils.go_to_course(wd,coursename)
                time.sleep(2)
                testutils.logout(wd)
                #Student 2 login and submit task 
                testutils.login(wd, args.target, studentname2)
                testutils.go_to_course(wd,coursename)
                time.sleep(1)
                testutils.wait_for(wd,By.ID,'course-button-tasks').click()
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click()
                testutils.wait_for(wd,By.ID,'button-quiz-start').click()
                wd.find_elements_by_css_selector('.checkbox')[1].click()
                wd.find_element_by_id('button-quiz-finish').click()
                wd.refresh()
                time.sleep(6)
                testutils.wait_for(wd,By.ID,'bt-course').click()
                testutils.go_to_course(wd,coursename)
                testutils.wait_for(wd,By.ID,'course-button-tasks').click()
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click()
                testutils.wait_for(wd,By.ID,'button-quiz-start').click()
                wd.find_elements_by_css_selector('.checkbox')[1].click()
                wd.find_element_by_id('button-quiz-finish').click()
                wd.refresh()
                time.sleep(7)
                testutils.wait_for(wd,By.ID,'bt-course').click()
                testutils.go_to_course(wd,coursename)
                time.sleep(2)
                testutils.logout(wd)
                
            if args.j <= 3:
                testutils.login(wd, args.target, username)
                ##checking course completion feed
               
                if not wd.find_element_by_partial_link_text('just completed your course').is_dispalyed(): ## **comment in local
                    raise Exception('Course completion feed is not displayed in feed section')
                wd.find_element_by_partial_link_text('just completed your course').click()
                if testutils.wait_for(wd,By.ID,'course-button-progress').get_attribute('class') != '"btn setupdone active':
                    raise Exception('Course completion feed text is not redirecting to progress page')     ##**
                #testutils.go_to_course(wd,coursename)                                                  ##**comment in plato  ##uncomment in local
                #testutils.wait_for(wd,By.ID,'course-button-progress').click()                          ##**
                ##check the buttons 
                time.sleep(5)
                if not wd.find_element_by_xpath("//div[@class='inner']/div[1]/a[1]").is_displayed():
                    raise Exception('Grades button is not displayed')
                if not wd.find_element_by_xpath("//div[@class='inner']/div[1]/a[2]").is_displayed():
                    raise Exception('Export button is not displayed')
                if not wd.find_element_by_xpath("//div[@class='inner']/div[1]/a[3]").is_displayed():
                    raise Exception('Design button is not displayed')
                #Check tasks in grade book
                if not(testutils.wait_for(wd,By.LINK_TEXT,'Task 2').is_displayed() and  testutils.wait_for(wd,By.LINK_TEXT,'Task 1').is_displayed()):
                    raise Exception('All tasks are not displayed in gradebook')  
                count = 0
                for i in range(1,5):
                    value = wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div["+str(i)+"]").get_attribute('class')
                    if value == 'myrow':
                        count = count+1
                if count != 3:
                    raise Exception('3 rows should be displayed in gradebook but it is '+count) 
                #check individual scores and average
                time.sleep(4)
                if wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[2]/span[1]").text != '1/1' or  wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[2]/span[2]").text != '1/1' or  wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[2]/span[3]").text != '100':
                    raise Exception('Student1:Individual scores and average are not correct')
                if wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[3]/span[1]").text != '1/1' or  wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[3]/span[2]").text != '0/1' or  wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[3]/span[3]").text != '50':
                    raise Exception('Student2:Individual scores and average are not correct')  
                if wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[4]/span[1]").text != '100' or  wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[4]/span[2]").text != '50' or  wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[1]/div[4]/span[3]").text != '75':
                    raise Exception('Average percentage of tasks are not correct')
                ##Check certificate issue and revoke 
                if not  wd.find_element_by_xpath("//div[@id='progress-grade-cert']/div[2]/div[1]/button[1]").is_displayed():
                    raise Exception('Certificate issue button is not displayed')
                wd.find_element_by_xpath("//div[@id='progress-grade-cert']/div[2]/div[1]/button[2]").click()   
                wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[2]/div[2]/div[1]/ul/li/a").click()
                time.sleep(5)
                if not  wd.find_element_by_xpath("//div[@id='progress-grade-cert']/div[2]/div[1]/button[1]").get_attribute('disabled') == 'true':
                    raise Exception('Certificate Issue button should not be active once Issued')  
                wd.find_element_by_xpath("//div[@id='progress-grade-cert']/div[2]/div[1]/button[2]").click() 
                if wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[2]/div[2]/div[1]/ul[1]/li[1]/a").text != 'View' and wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[2]/div[2]/div[1]/ul[1]/li[1]/a").is_displayed() != True:
                    raise Exception('Certificate View  option is not visible after issued certificate')
                if wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[2]/div[2]/div[1]/ul[1]/li[2]/a").text != 'Revoke' and wd.find_element_by_xpath("//div[@id='progress-grade-grades']/div[2]/div[2]/div[1]/ul[1]/li[2]/a").is_displayed() != True:
                    raise Exception('Certificate Revoke option is not visible after issued certificate')    
                ##Export gradebook
                wd.find_element_by_xpath("//div[@class='inner']/div[1]/a[2]").click()
                if not (wd.find_element_by_xpath("//label[@class='radio']/input").is_selected() or wd.find_element_by_xpath("//label[@class='checkbox']/input").is_selected()):
                    raise Exception('CSV document and include feedback should be selected by default but it is not')
                if not testutils.wait_for(wd,By.ID,'grades-export-btn').is_displayed() == True:
                    raise Exception('Not able to download gradebook')
                
                
        except Exception, e:
            testutils.save_screenshot(self)
            testutils.save_browser_log(self)
            raise
            
    def tearDown(self):
        testutils.tear_down(self)            
