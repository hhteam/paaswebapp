#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys
import os

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.notifications'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'teacher@127.0.0.1'
        self.studentname = 'student@127.0.0.1'
        self.coursename = 'Notifications Unit Tests'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.studentname)
        testdata.require(self.args, 'user', self.studentname)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.studentname, self.coursename)
        testdata.mark_notifications_read(self.args, self.studentname)

    @unittest.skip("fails due to bug #1521")
    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename; studentname = self.studentname
        tdata = { 'userid': testdata.get(args, 'user', username, 'username', 'id') }
        try:
            testutils.login(wd, args.target, username)
            
            if args.j <= 1:
                # notification about course content change
                wd.find_element_by_xpath("//div[@class='span9']//div[.='"+coursename+"']").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn_edit")
                wd.find_elements_by_css_selector('.btn_edit')[0].click()
                time.sleep(2)
                
                # add section
                testutils.wait_for(wd, By.ID, "course-section-add-new").click();
                
                # Dummy file upload
                wd.find_element_by_css_selector('div#section-1 a.btn-add').click()
                testutils.upload_file(args, wd, username, 'upload_course_material.pdf', By.CSS_SELECTOR, "div#section-1 a.add-item")
                wd.find_element_by_id('btn_edit_save').click()
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn_edit")

                found = False;
                for element in wd.find_elements_by_css_selector('div#section-1 .attachmentlist .attachment-place span'):
                    if element.text == 'upload_course_material':
                        found = True
                if not found:
                    raise Exception("FAIL: Uploaded file not found")
                    
                testutils.logout(wd)
                    
                testutils.login(wd, args.target, studentname)
                time.sleep(3)
                # notification count
                notifs = wd.find_element_by_id("courses_ncount").text
                if notifs == '': notifs = 0
                if int(notifs) != 1:
                    raise Exception("Notification count should be 1, it is "+str(notifs))
                # feed notification count
                notifs = len(wd.find_elements_by_css_selector('.news-item-box'))
                if notifs != 1:
                    raise Exception("Notifications in feed count should be 1, it is "+str(notifs))
            
        except Exception, e:
            testutils.save_screenshot(self)
            raise
            
    def tearDown(self):
        testutils.tear_down(self)
