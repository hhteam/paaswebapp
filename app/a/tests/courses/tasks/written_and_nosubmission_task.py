#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys
import datetime
from datetime import timedelta,date,datetime
import lxml
from selenium.webdriver.common.keys import Keys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.tasks.written_and_nosubmission_task'
     
    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'teacher@127.0.0.1'
        self.studentname = 'student@127.0.0.1'
        self.coursename = 'Course for Written text task'
        self.taskname = 'Written text task'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.studentname)
        testdata.require(self.args, 'user', self.studentname)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        testdata.delete(self.args, 'user_is_teacher',  self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.studentname, self.coursename)
        testdata.delete(self.args, 'user_is_enrolled',  self.coursename)
        
        
    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename; taskname = self.taskname; studentname = self.studentname
        try:
            testutils.login(wd, args.target, username)
            testutils.go_to_course(wd,coursename)
            testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-primary').click()
            testutils.wait_for(wd, By.ID, 'skills-input').click()
            testutils.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill1,Skill2,Skill3")
            testutils.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
            time.sleep(0.5)
            wd.find_element_by_id('btn_edit_save').click()
            time.sleep(1)
            if args.j <= 1:
                          # go to task
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                time.sleep(1)
                wd.refresh()
                time.sleep(5)
                wd.find_element_by_id("btNewTask").click()
                         #checking the button is active or not 
                if wd.find_element_by_id("btn-task-type-upload").get_attribute("class") != "btn active":
                    raise Exception("Written text or file upload button should be active but it is not active ")
                  ##check default values
                self.displayValues()   
                
            if args.j <= 2:  
                    ##Written text or file upload task 
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()
                wd.find_element_by_id("full-name").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Task name shoul not be empty")
                wd.find_element_by_id("full-name").click()    
                wd.find_element_by_id("full-name").send_keys("Newly created task Newly created taskNewly created taskNewly created taskNewly created taskNewly created taskNewly created taskNewly created taskNewly created task")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "Character limit has been reached":
                    raise Exception("Task name character limit has been reached")
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()    
                wd.find_element_by_id("full-name").send_keys(taskname)
                wd.find_element_by_id('deadline-date').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"10\")').click();");
                #wd.find_element_by_xpath("//div[@id='deadline-time']/input").click()
                #wd.find_element_by_xpath("//div[@id='deadline-time']/input").clear()
                #wd.find_element_by_xpath("//div[@id='deadline-time']/input").send_keys('11:00')
                  #wd.find_element_by_css_selector(".icon.icon-time").click()
                  #testutils.javascript(wd, "$('div.timepicker-picker:visible tr a i').click();");
                wd.find_element_by_id("intro").click()
                wd.find_element_by_id("intro").clear()
                wd.find_element_by_id("full-name").click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Instructions shoul not be empty")
                wd.find_element_by_id("intro").send_keys("This is good task") 
                testutils.wait_for(wd,By.ID,"btn_edit_save").click()
                testutils.wait_for(wd,By.ID,"btNewTask") 
                wd.refresh()
                time.sleep(5)
                if wd.find_element_by_css_selector('.task-duedate.pull-left').text == " ":
                    raise Exception('Due date are not displaying')
                ##one more written task for student submit task as draft
                testutils.wait_for(wd,By.ID,"btNewTask").click()
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()    
                wd.find_element_by_id("full-name").send_keys('Submit as draft task')
                wd.find_element_by_id('deadline-date').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"18\")').click();");
                wd.find_element_by_id("intro").click()
                wd.find_element_by_id("intro").clear() 
                wd.find_element_by_id("intro").send_keys("This is for submit as drat task ") 
                testutils.wait_for(wd,By.ID,"btn_edit_save").click()
                testutils.wait_for(wd,By.ID,"btNewTask")   
            if args.j <= 3:
                    ##No submission required
                wd.find_element_by_id("btNewTask").click()    
                testutils.wait_for(wd,By.ID,'btn-task-type-nosubmissions').click()
                self.displayValues() 
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()
                wd.find_element_by_id("full-name").send_keys("No submission required task")
                wd.find_element_by_id('deadline-date').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"15\")').click();");
                time.sleep(1) 
                wd.find_element_by_id("intro").click()
                wd.find_element_by_id("intro").clear()
                wd.find_element_by_id("intro").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Instructions should not be empty")
                wd.find_element_by_id("intro").send_keys("No submission required for this task") 
                testutils.wait_for(wd,By.ID,"btn_edit_save").click()
                time.sleep(2)  
                 
            if args.j <= 4:
                 ### check saved task
                testutils.wait_for(wd,By.ID,'btNewTask')   
                time.sleep(4)
                if wd.find_element_by_xpath("//div[@class='task-block'][1]/a/span").text != taskname:
                    raise Exception('Task name should be '+taskname+' but it is '+ wd.find_element_by_xpath("//div[@class='task-block'][1]/a/span").text)  
                if wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text != "No submission required task":
                    raise Exception('Task name should be No submission required task but it is '+wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text)              
                if wd.find_element_by_xpath("//div[@class='task-block'][3]/a/span").text != "Submit as draft task":
                    raise Exception('Task name should be Submit as draft task but it is ' +wd.find_element_by_xpath("//div[@class='task-block'][3]/a/span").text )   
                    ##check saved task are displaying based on days 
                for i in range(1,4):
                    days_count = wd.find_element_by_xpath("//div[@class='task-block']["+str(i)+"]/span/strong").text
                    days_count,d = days_count.split()
                    days_count = days_count[1:]
                    if i == 1:
                        count1 = days_count
                    if i == 2:
                        count2 = days_count 
                    if i == 3:
                        count3 = days_count     
                if not int(count1) < int(count2) <  int(count3):
                    raise Exception("Tasks are not displayed based on due date") 
                testutils.logout(wd)
                
            if args.j <= 7:
                  ##  student login 
                testutils.login(wd, args.target, studentname) 
                time.sleep(1)
                   ##checking tasks are there 
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a/table/tbody/tr/td[1]").text != taskname:
                    raise Exception('Student:task is missing')
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[2]/a/table/tbody/tr/td[1]").text != "No submission required task":
                    raise Exception('Student:task is missing')
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[3]/a/table/tbody/tr/td[1]").text != "Submit as draft task":
                    raise Exception('Student:task is missing')
                      ###check can able to see task  and submit as draft
                self.ContentVisibleToStudents_and_submitDraft()
                      ##Submit task
                self.submit_textTask()
                 ##check able to give rating for course(can not able to see cause it is private course)
                wd.find_element_by_id('bt-course').click()  
                testutils.wait_for(wd,By.CSS_SELECTOR,'.subtitle-blue').click()
                if testutils.is_element_visible(wd,By.CSS_SELECTOR,'.fa.fa-star-o'):
                    raise Exception("Rating icon should not be visible to students but it is visible")
                  ##Progress bar status (should be 0%)
                time.sleep(2)  
                if wd.find_element_by_css_selector('.progressbar-back-text').text != '0%':
                    raise Exception('Progress bar status should be 0% but it is '+wd.find_element_by_css_selector('.progressbar-back-text').text)
                testutils.logout(wd)
                      
            if args.j <= 8:
                   ##Teacher: check tasks notification 
                testutils.login(wd, args.target, username)
                notification = testutils.wait_for(wd, By.ID, "courses_ncount").text  
                if len(notification) == 0 or int(notification) != 1: 
                    raise Exception("Notification count should be 1, it is "+str(notification))
                testdata.mark_notifications_read(args, username)
                time.sleep(1)
                   ##Check feed
                message = (wd.find_element_by_css_selector('.message').text).replace('\n', ' ')  
                #message = message.replace('\n', ' ')
                if message != 'Course for Written text task Foo Bar submitted an assignment':
                    raise Exception('Teacher:Feed Message is not displayed in feed section after student submit an assignment')
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a/table/tbody/tr/td[2]/strong").text != "(All returned)" and wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a/table/tbody/tr/td[1]").text != taskname:
                    raise Exception(taskname+":should be all returned but it is "+wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a/table/tbody/tr/td[2]/strong").text)
                    ##check no notification for task that is saved as draft by student
                wd.find_element_by_xpath("//div[@class='course-tasks']/div[3]/a").click()
                if wd.find_element_by_css_selector('.alert.alert-error').text != "Not returned":
                    raise Exception("Task returned status should be 'Not returned'")
                    #grade task
                self.grade_task()  
                time.sleep(1)  
                #check:is graded task  in graded task section ?
                wd.find_element_by_id('bt-course').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.subtitle-blue').click()
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                testutils.wait_for(wd,By.ID,'btNewTask')  
                if wd.find_element_by_xpath("//div[@id='tasks-col2']/div[1]/a/span").text != 'Written text task':
                    raise Exception('Graded task is not displayed in graded tasks section')
                #change course to public ,to check student able to give rating 
                testutils.wait_for(wd,By.ID,'course-button-settings').click()
                testutils.wait_for(wd,By.ID,'button-publish-course').click()
                testutils.logout(wd)
                
            if args.j <= 9:  
                #login as a student and 
                testutils.login(wd, args.target, studentname) 
                time.sleep(2)
                 ##check feed for graded task
                message = (wd.find_element_by_css_selector('.message').text).replace('\n', ' ')  
                if message != 'Course for Written text task Foo Bar graded a task':
                    raise Exception('Student:Feed Message is not displayed in feed section after teacher graded task') 
                #check ,able to give rating  
                testutils.wait_for(wd,By.CSS_SELECTOR,'.subtitle-blue').click()
                time.sleep(2)
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.fa.fa-star-o'):
                    raise Exception("Rating icon should be visible to students but it is not visible")
                ##Progress bar status (should not be 0%)
                if wd.find_element_by_css_selector('.bar').text == '0%':
                    raise Exception('Progress bar status should not be 0% but it is '+wd.find_element_by_css_selector('.bar').text)
                #check:is graded task  in completed task section ? 
                testutils.wait_for(wd,By.ID,"course-button-tasks").click() 
                if wd.find_element_by_xpath("//div[@id='tasks-col2']/div[1]/a/span[1]").text != 'Written text task':
                    raise Exception('Graded task is not displayed in completed tasks section') 
                grade = wd.find_element_by_xpath("//div[@id='tasks-col2']/div[1]/a/span[2]").text   
                if (wd.find_element_by_xpath("//div[@id='tasks-col2']/div[1]/a/span[2]").text).strip() != grade.strip():
                    raise Exception('Grade value is not correct') 
                testutils.logout(wd)  
            
            if args.j <= 9:  
                #login as a teacher 
                testutils.login(wd, args.target, username) 
                testutils.go_to_course(wd,coursename)
                time.sleep(1)  
                #wd.find_element_by_id('course-button-overview').click()
                #time.sleep(1)
                #testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-primary').click()
                #testutils.wait_for(wd, By.ID, 'skills-input').click()
                #testutils.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill1,Skill2,Skill3")
                #testutils.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                #time.sleep(0.5)
                #wd.find_element_by_id('btn_edit_save').click()
                ##Delete task (submit as draft task)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click() 
                time.sleep(1)
                for i in range(1,3):
                    i = 0
                    wd.find_elements_by_css_selector('.block.task-name')[i].click()
                    testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-primary.dropdown-toggle').click()
                    time.sleep(0.5)
                    testutils.wait_for(wd,By.ID,'btn_delete_task').click()
                    time.sleep(0.5)
                    testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-danger').click()
                    time.sleep(0.5)
                testutils.logout(wd)  
            
            if args.j <= 9:  
                #login as a student 
                testutils.login(wd, args.target, studentname) 
                testutils.go_to_course(wd,coursename)
                time.sleep(0.5)  
                if not wd.find_element_by_xpath("//div[@class='modal-header']/button").is_displayed():
                    raise Exception('Course completion modal popup should display')
                wd.find_element_by_xpath("//div[@class='modal-header']/button").click()
                ##Task progress should be 100%
                if wd.find_element_by_css_selector('.bar').text != '100%':
                    raise Exception('Progress bar status should  be 100% but it is '+wd.find_element_by_css_selector('.bar').text)
                ##Check skills 
                if len(wd.find_elements_by_css_selector('.skill-label')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.skill-label'))))
                time.sleep(1)
                ##Check skills in profile     
                wd.find_element_by_id('bt-settings').click()
                testutils.wait_for(wd,By.LINK_TEXT,'My Profile').click()
                time.sleep(0.5)
                if not testutils.is_element_visible(wd,By.ID,'skills-progress'):
                    raise Exception('Skill labels should be visible ')
                if len(wd.find_elements_by_css_selector('.label-success')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.label-success'))))   
                time.sleep(1)
                testutils.logout(wd)  
                
            if args.j <= 10:  
                    ##No due date
                testutils.login(wd, args.target, username)
                testutils.go_to_course(wd,coursename)    
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                time.sleep(1)
                wd.find_element_by_id("btNewTask").click()
                testutils.wait_for(wd,By.ID,'due-date-edit-checkbox').click()
                if testutils.wait_for(wd,By.ID,'deadline-date').is_displayed() != True and testutils.wait_for(wd,By.ID,'deadline-time-input').is_displayed() != True:
                    raise Exception('No due date: Date and time shoul be disabled')
                #create task to check no due date     
                testutils.wait_for(wd,By.ID,'full-name').click()
                testutils.wait_for(wd,By.ID,'full-name').send_keys('Test for due date') 
                testutils.wait_for(wd,By.ID,'intro').click()
                testutils.wait_for(wd,By.ID,'intro').send_keys('Test instructions')
                testutils.wait_for(wd,By.ID,'btn_edit_save').click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,'SPAN.task-duedate.pull-left').text != '(No due date)':
                    raise Exception("It should be 'No due date'")
                
              
        except Exception, e: 
            testutils.save_screenshot(self)
            testutils.save_browser_log(self)
            raise
    
    def displayValues(self):
        wd = self.wd; coursename = self.coursename
        if wd.find_element_by_id("full-name").find_element_by_tag_name("span").text != "New task":
            raise Exception("Task name default value should be 'New task'")
        if wd.find_element_by_id("task-view-course-name").text != coursename:
            raise Exception("Course name should be "+coursename+" " +"but it is" +" "+wd.find_element_by_id("task-view-course-name").text)
        d = date.today()+timedelta(days=7)
        due_date ="%s.%s.%s" % (d.day, '0'+str(d.month), d.year)
        if wd.find_element_by_id("deadline-date").get_attribute("value") != due_date:
            raise Exception("Default due date should be"+" "+due_date+" "+"but it is"+" "+wd.find_element_by_id("deadline-date").get_attribute("value"))
        current_time = time.strftime("%H:%M")
        if wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value") != current_time:
                raise Exception("Default due time should be"+" "+current_time+" "+"but it is"+" "+wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value"))            
        return  
        
    def ContentVisibleToStudents_and_submitDraft(self):
        wd = self.wd; args = self.args; username = self.username
        #check written text task contents 
        wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a").click()
        testutils.wait_for(wd,By.ID,'btn-submit-task')
        if not testutils.is_element_visible(wd,By.ID,'full-name') and wd.find_element_by_id('full-name').text != taskname:
            raise Exception("Task Title is not visible to students")
        if not testutils.is_element_visible(wd,By.ID,'due-date-view'):
            raise Exception("Task due date and time is not visible to students")   
        if not testutils.is_element_visible(wd,By.ID,'intro') and (wd.find_element_by_id('intro').text).strip() != "Amazing task":
            raise Exception("Task instruction is not visible to students") 
            ## student complete task and save as draft
        wd.find_element_by_id('bt-course').click() 
        wd.find_element_by_xpath("//div[@class='course-tasks']/div[3]/a").click()
        time.sleep(2)
        testutils.wait_for(wd,By.ID,'btn-submit-task').click()
        testutils.wait_for(wd,By.ID,'submission-text').click()
        wd.find_element_by_id('submission-text').clear() 
        wd.find_element_by_id('submission-text').send_keys("task is submitted as d")
        count =0
        while  count < 10 and testutils.wait_for(wd,By.ID,"btn-draft-submission").get_attribute("disabled") == 'true':
            count = count+1
            time.sleep(1)
            if count == 10:
                raise Exception() 
        wd.find_element_by_id('btn-draft-submission').click()
        time.sleep(1)
        if not testutils.is_element_visible(wd,By.ID,'no-submission-yet-text') and wd.find_element_by_id('draft-text').text != "Nothing submitted yet":
            raise Exception()
        wd.find_element_by_id('bt-course').click()    
        return     
        
    def submit_textTask(self):
        wd = self.wd;
        wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a").click()  
        time.sleep(2) 
        testutils.wait_for(wd,By.ID,'btn-submit-task').click()
        wd.find_element_by_id('submission-text').click()
        wd.find_element_by_id('submission-text').clear() 
        wd.find_element_by_id('submission-text').send_keys("Text task submitted")
        count =0
        while  count < 10 and testutils.wait_for(wd,By.ID,"btn-submit-submission").get_attribute("disabled") == 'true':
            count = count+1
            time.sleep(1)
            if count == 10:
                raise Exception() 
        wd.find_element_by_id('btn-submit-submission').click()
        time.sleep(1) 
        return   
        
    def grade_task(self):
        wd = self.wd; coursename = self.coursename
        testutils.go_to_course(wd,coursename)
        testutils.wait_for(wd,By.ID,"course-button-tasks").click()
        time.sleep(1)
        wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click() 
        time.sleep(2)
        testutils.wait_for(wd,By.CSS_SELECTOR,'a.task-submission-file.task-submission-text.btn').click()
        time.sleep(1)
        if wd.find_element_by_xpath("//div[@id='cboxLoadedContent']/div[2]/p").text != "Text task submitted":
            raise Exception("Submitted task is 'Text task submitted' but it is "+ wd.find_element_by_xpath("//div[@id='cboxLoadedContent']/div[2]/p").text)
        wd.find_element_by_id('cboxClose').click()    
        wd.find_element_by_css_selector('a.grade-item').click()
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").click()
            #without grade
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").send_keys(" ")
        wd.find_element_by_link_text('Save').click()
        time.sleep(1)
        if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.control-group.error'):
            raise Exception("Grade value should not be empty")
              #grade value above 100
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").clear()      
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").send_keys("103")
        time.sleep(1)
        wd.find_element_by_link_text('Save').click()
        time.sleep(1)
        if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.control-group.error'):
            raise Exception("Grade value should not be above 100")
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").click()
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").clear()
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").send_keys("100") 
        wd.find_element_by_xpath("//div[@class='modal-body']/textarea").click()
        wd.find_element_by_xpath("//div[@class='modal-body']/textarea").send_keys("Good")
        wd.find_element_by_link_text('Save').click()
             #check graded value is correct
        time.sleep(1) 
        if (wd.find_element_by_css_selector('A.grade-item.ignored_link.btn').text).strip() != 'Grade 100/100':
            raise Exception("Grade value should be Grade 90/100 but it is "+ wd.find_element_by_css_selector('a.grade-item').text)  
           
        return           
        
    def tearDown(self):
        testutils.tear_down(self)    
