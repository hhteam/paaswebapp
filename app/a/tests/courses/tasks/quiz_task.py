#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys
import datetime
from datetime import timedelta,date,datetime
import lxml
from selenium.webdriver.common.keys import Keys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.tasks.quiz_task'
   
    def setUp(self):
        testutils.set_up(self)
      
    def testdata_setup(self):
        self.username = 'teacher1@127.0.0.1'
        self.studentname = 'student1@127.0.0.1'
        self.coursename = 'Course for testing quiz task'
        self.taskname = 'Quiz task 1'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.studentname)
        testdata.require(self.args, 'user', self.studentname)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        testdata.delete(self.args, 'user_is_teacher',  self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.studentname, self.coursename)
        testdata.delete(self.args, 'user_is_enrolled',  self.coursename)
        
        
    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename; taskname = self.taskname; studentname = self.studentname
        try:
            testutils.login(wd, args.target, username)
            testutils.go_to_course(wd,coursename)
            if args.j <= 1:
                          # go to task
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                time.sleep(1)
                wd.refresh()
                time.sleep(7)
                wd.find_element_by_id("btNewTask").click()
                          
            if args.j <= 2:  
                 ##Quiz task 1
                testutils.wait_for(wd,By.ID,"btn-task-type-quiz").click()
                ##check default values  
                self.displayValues() 
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()
                wd.find_element_by_id("full-name").send_keys("Quiz task 1")
                wd.find_element_by_id('deadline-date').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"11\")').click();");
                time.sleep(1) 
                wd.find_element_by_css_selector('.richtext').click()
                wd.find_element_by_css_selector('.richtext').clear()
                wd.find_element_by_css_selector('.richtext').send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Question 1 shoul not be empty")
                wd.find_element_by_css_selector('.richtext').send_keys("Is quiz good?")
                 ###Add another option(2 more options) 
                for i in range(2):
                    wd.find_elements_by_css_selector('.btn-add-quiz-answer')[0].click()
                    #validating and send keys to answer option 
                checkbox_count =self.addAnswerOption_question1()
                    ##Check if it is added or not 
                if checkbox_count != 4:
                    raise Exception("Could not add another answer option")     
                    ##delete answer option  
                wd.find_elements_by_css_selector('.btn-remove-answer')[1].click()
                    ##check deletion
                checkbox_count -= 1
                if checkbox_count != 3:
                    raise Exception("could not remove answer option ")
                    ###select correct answer option 
                wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/label/input").click()
                ### Add new questions              
                wd.find_element_by_id("btn-add-new-question").click()
                wd.find_elements_by_css_selector('.btn-add-quiz-answer')[1].click()
                  ##add answer options
                self.addAnswerOption_question2()
                wd.find_element_by_xpath("//div[@class='block'][4]/div[6]/label/input").click()
                wd.find_element_by_id("btn_edit_save").click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Question 2 shoul not be empty")
                wd.find_elements_by_css_selector('.richtext')[1].click()
                wd.find_elements_by_css_selector('.richtext')[1].clear()
                wd.find_elements_by_css_selector('.richtext')[1].send_keys("Is task added?")
                 ##Add 3 more questions and answers
                self.add3more_questions()
                testutils.wait_for(wd,By.ID,'btNewTask')
                
            if args.j <= 3:
                ##Add 2 more quiz task  ##quiz task 2,quiz task 3
                for i in range(0,2):
                    testutils.wait_for(wd,By.ID,'btNewTask').click() 
                    testutils.wait_for(wd,By.ID,'btn-task-type-quiz').click()
                ##task name  
                    if i == 0:
                        wd.find_element_by_id('deadline-date').click()
                        testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                        time.sleep(1)
                        testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"13\")').click();"); 
                        time.sleep(1) 
                    if i == 1:
                        wd.find_element_by_id('deadline-date').click()
                        testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                        time.sleep(1)
                        testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"19\")').click();"); 
                        time.sleep(1)
                           
                    wd.find_element_by_id("full-name").click()
                    wd.find_element_by_id("full-name").clear()
                    if i == 0:
                        wd.find_element_by_id("full-name").send_keys("Quiz task 2")
                    if i == 1:
                        wd.find_element_by_id("full-name").send_keys("Quiz task 3")
                         
                    ##question
                    wd.find_element_by_css_selector('.richtext').click()
                    wd.find_element_by_css_selector('.richtext').clear()
                    if i == 0:
                        wd.find_element_by_css_selector('.richtext').send_keys("Is it additional quiz 1?")
                    if i == 1:
                        wd.find_element_by_css_selector('.richtext').send_keys("Is it additional quiz 2?")    
                    ##Quiz answers
                    wd.find_element_by_xpath("//div[@class='quiz_answer'][1]/span").click()
                    wd.find_element_by_xpath("//div[@class='quiz_answer'][1]/span").clear()
                    wd.find_element_by_xpath("//div[@class='quiz_answer'][1]/span").send_keys("a=1")
                    wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/span").click()
                    wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/span").clear()
                    wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/span").send_keys("a=2")
                    ##select correct answer
                    if i == 0:
                        wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/label/input").click()
                    if i == 1:
                        wd.find_element_by_xpath("//div[@class='quiz_answer'][1]/label/input").click()    
                    ##Save
                    wd.find_element_by_id("btn_edit_save").click()
                time.sleep(6.5)
                
                    
            if args.j <= 4:
                    ##Hide the quiz task and check 
                    testutils.wait_for(wd,By.ID,'btNewTask')
                    wd.find_elements_by_css_selector('.block.task-name.setupdone')[1].click()
                    #wd.find_element_by_link_text('Quiz task 2').click()
                    time.sleep(2)
                    testutils.wait_for(wd,By.ID,'btn_edit').click()
                    time.sleep(2)
                    #if not wd.find_element_by_id('btn-hide-task').is_displayed():
                        #raise Exception("Hide button should be display")
                    wd.find_element_by_id('btn-hide-task').click()
                    time.sleep(2)
                    wd.find_element_by_id("btn_edit_save").click()  
                    time.sleep(1)
                    if (wd.find_element_by_xpath("//div[@class='task-block'][2]/span[1]/strong").text).strip() != '(Hidden)':
                        raise Exception('Quiz task 2 should be hidden')
                    testutils.logout(wd)  
            
            if args.j <= 5:  
                #login as a student 
                #check hidden task is visible or not (should not)
                testutils.login(wd, args.target, studentname) 
                testutils.go_to_course(wd,coursename)
                time.sleep(0.5)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                time.sleep(1)
                if len(wd.find_elements_by_css_selector('.task-block')) != 2:
                    raise Exception('Quiz task 2 is hidden by teacher but it still displaying in student tasks')
                testutils.logout(wd) 
                      
            if args.j <= 6: 
                ##teacher    
                testutils.login(wd, args.target, username) 
                testutils.go_to_course(wd,coursename)
                time.sleep(0.5)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                ##show the quiz task and check 
                testutils.wait_for(wd,By.ID,'btNewTask')
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[1].click()
                #wd.find_element_by_link_text('Quiz task 2').click()
                time.sleep(2)
                testutils.wait_for(wd,By.ID,'btn_edit').click()
                time.sleep(2)
                wd.find_element_by_id('btn-hide-task').click()
                time.sleep(2)
                wd.find_element_by_id("btn_edit_save").click()  
                time.sleep(1)
                if (wd.find_element_by_xpath("//div[@class='task-block'][2]/span[1]/strong").text).strip() == '(Hidden)':
                    raise Exception('Quiz task 2 should not be hidden')
                ##check task submit status and change no. of attempts    ##3 times 
                if  wd.find_elements_by_css_selector('.task-returned.pull-right')[0].text != '(0 of 1 returned)':
                    raise Exception('Task return staus should be (0 of 1 returned)')
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click() 
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.alert.alert-error').text != 'Not returned':
                    raise Exception('Quiz staus should be Not returned') 
                testutils.wait_for(wd,By.ID,'btn_edit').click()  
                testutils.wait_for(wd,By.ID,'quizattempts').click()
                testutils.wait_for(wd,By.ID,'quizattempts').send_keys('3')
                #testutils.wait_for(wd,By.CSS_SELECTOR,'option[value="3"]').click()
                #wd.find_element_by_xpath("//select/option[3]").click() 
                wd.find_element_by_id("btn_edit_save").click()  
                time.sleep(1)
                testutils.logout(wd)  
            
            if args.j <= 7:  
                #login as a student 
                #check all tasks is visible or not (should be )
                testutils.login(wd, args.target, studentname) 
                testutils.go_to_course(wd,coursename)
                time.sleep(0.5)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                time.sleep(1)
                if len(wd.find_elements_by_css_selector('.task-block')) != 3:
                    raise Exception('Quiz task 2 is hidden by teacher but it still displaying in student tasks')
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click() 
                if not testutils.wait_for(wd,By.ID,'button-quiz-start').is_displayed():
                    raise Exception('Start button should be here')
                testutils.wait_for(wd,By.ID,'button-quiz-start').click()  
                testutils.wait_for(wd,By.ID,'button-quiz-finish')
                wd.find_element_by_xpath("(//input[@type='checkbox'])[1]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[6]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[8]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[10]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[11]").click()
                testutils.wait_for(wd,By.ID,'button-quiz-finish').click()
                wd.refresh()
                time.sleep(10)
                testutils.wait_for(wd,By.ID,'button-quiz-end')
                ##checking buttons display and remaining quiz attempts and quiz result
                if wd.find_element_by_xpath("//div[@class='block']/div[1]/h3").text != 'You can take this quiz 2 more times':
                    raise Exception('Remaining quiz attempt option is not correct')
                if not wd.find_element_by_id('button-quiz-restart').is_displayed():
                    raise Exception('Restart quiz button is not displayed but it should be displayed')   
                if not wd.find_element_by_id('button-quiz-end').is_displayed():
                    raise Exception('See rusults button is not displayed but it should be displayed')        
                if not wd.find_elements_by_css_selector('.block.quiz_result')[0].text == '3 / 5 correct' and wd.find_elements_by_css_selector('.block.quiz_result')[1].text == '3 / 5 correct':
                    raise Exception('Quiz result is not correct it should be 3 / 5 correct but it is  '+wd.find_elements_by_css_selector('.block.quiz_result')[0].text)
                    
                ##checking selected answer is correct or incorrect 
                ##Question 1 
                if (wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[2]/span").text).strip() != 'Correct':
                    raise Exception('Selected ans is correct but it is not showing as correct')
                ##Question 2
                
                #print wd.find_element_by_xpath("//div[@class='quiz_answer_block'][2]/div[1]/span").text
                #if wd.find_element_by_xpath("//div[@class='quiz_answer_block'][2]/div[1]/span").text != 'Incorrect':
                    #raise Exception('Selected ans is correct but it is not showing as correct')
                ##checking restart quiz 
                wd.find_element_by_id('button-quiz-restart').click()
                testutils.wait_for(wd,By.ID,'button-quiz-finish')
                wd.find_element_by_xpath("(//input[@type='checkbox'])[1]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[6]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[8]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[10]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[12]").click()
                testutils.wait_for(wd,By.ID,'button-quiz-finish').click()
                wd.refresh()
                time.sleep(10)
                if wd.find_element_by_xpath("//div[@class='block']/div[1]/h3").text != 'You can take this quiz 1 more time':
                    raise Exception('Remaining quiz attempt option is not correct')
                if not wd.find_element_by_id('button-quiz-restart').is_displayed():
                    raise Exception('Restart quiz button is not displayed but it should be displayed')   
                if not wd.find_element_by_id('button-quiz-end').is_displayed():
                    raise Exception('See rusults button is not displayed but it should be displayed') 
                if wd.find_elements_by_css_selector('.block.quiz_result')[0].text != '4 / 5 correct' and  wd.find_elements_by_css_selector('.block.quiz_result')[1].text != '4 / 5 correct':
                    raise Exception('Quiz result is not correct it should be 4 / 5 correct but it is  '+wd.find_elements_by_css_selector('.block.quiz_result')[0].text)         
                #restart again 
                wd.find_element_by_id('button-quiz-restart').click()
                testutils.wait_for(wd,By.ID,'button-quiz-finish')
                wd.find_element_by_xpath("(//input[@type='checkbox'])[3]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[6]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[8]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[10]").click()
                wd.find_element_by_xpath("(//input[@type='checkbox'])[12]").click()
                testutils.wait_for(wd,By.ID,'button-quiz-finish').click()
                wd.refresh()
                time.sleep(10)   
                if not (wd.find_elements_by_css_selector('.block.quiz_result')[0].text and wd.find_elements_by_css_selector('.block.quiz_result')[1].text )== '4 / 5 correct':
                    raise Exception('Quiz result is not correct it should be 4 / 5 correct but it is  '+wd.find_elements_by_css_selector('.block.quiz_result')[0].text)             
                ##check incorrect and correct ans (3rd attempt)
                if wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[2]").text != 'No Correct answer' and wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[2]/span").text != 'Correct answer':
                    raise Exception('After last attempt it should shown correct answers of Questions')
                testutils.logout(wd)  
                
            if args.j <= 8: 
                ##teacher    
                testutils.login(wd, args.target, username) 
                testutils.go_to_course(wd,coursename)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                ##checking finished task in graded task section
                if wd.find_element_by_xpath("//div[@id='tasks-col2']/div[1]/a[1]/span") .text != 'Quiz task 1':
                    raise Exception('Finished task is not in graded task section ')
                wd.find_element_by_xpath("//div[@id='tasks-col2']/div[1]/a[1]/span").click() 
                #view finished task 
                wd.find_element_by_xpath("//div[@class='task-student-block']/div[1]/a").click()
                if wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[2]").text != 'No Correct answer' or wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[3]").text != 'Not bad Incorrect' or wd.find_element_by_xpath("//div[@class='block'][2]/div[2]/div[3]").text != 'May be Correct':
                    raise Exception('Instructor could not view the finished quiz')
                ##set 2 attempts to Quiztask 2 (to check See results for Quiz)    
                wd.find_element_by_id('bt-course').click()
                testutils.go_to_course(wd,coursename)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click()
                testutils.wait_for(wd,By.ID,'btn_edit').click()
                testutils.wait_for(wd,By.ID,'quizattempts').click()
                testutils.wait_for(wd,By.ID,'quizattempts').send_keys('2')
                wd.find_element_by_id('btn_edit_save').click()
                time.sleep(1.5)
                ##Delete added question 
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[1].click()
                testutils.wait_for(wd,By.ID,'btn_edit').click()
                wd.find_element_by_id('btn-add-new-question').click()
                wd.find_elements_by_css_selector('.richtext')[1].click()
                wd.find_elements_by_css_selector('.richtext')[1].send_keys('Is it new Question?')
                wd.find_element_by_xpath("//div[@class='block'][4]/div[2]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div[2]/span").clear()
                wd.find_element_by_xpath("//div[@class='block'][4]/div[2]/span").send_keys("Yes")
                wd.find_element_by_xpath("//div[@class='block'][4]/div[4]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div[4]/span").clear()
                wd.find_element_by_xpath("//div[@class='block'][4]/div[4]/span").send_keys("No")
                wd.find_element_by_xpath("//div[@class='block'][4]/div[4]/label/input").click()
                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(2)
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[1].click()
                testutils.wait_for(wd,By.ID,'btn_edit').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.btn-remove-question').click()
                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(1)
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[1].click()
                testutils.wait_for(wd,By.ID,'btn_edit').click()
                if len(wd.find_elements_by_css_selector('.richtext')) != 1:
                    raise Exception('Can not able to delete newly added question')
                ##Delete Quiz 3    
                wd.find_element_by_id("btn_edit_save").click()
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[1].click()
                wd.find_element_by_css_selector('.btn.btn-primary.dropdown-toggle').click()
                testutils.wait_for(wd,By.ID,'btn_delete_task').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-danger').click()
                if len(wd.find_elements_by_css_selector('.block.task-name.setupdone')) != 2:
                    raise Exception('Can not able to delete Quiz task')
                testutils.logout(wd)
                
            if args.j <= 9: 
                ##Student     
                testutils.login(wd, args.target, studentname) 
                testutils.go_to_course(wd,coursename)
                time.sleep(0.5)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click() 
                wd.find_elements_by_css_selector('.block.task-name.setupdone')[0].click()
                testutils.wait_for(wd,By.ID,'button-quiz-start').click()
                testutils.wait_for(wd,By.ID,'button-quiz-finish')
                wd.find_element_by_xpath("(//input[@type='checkbox'])[1]").click()
                testutils.wait_for(wd,By.ID,'button-quiz-finish').click()
                wd.refresh()
                time.sleep(10)
                testutils.wait_for(wd,By.ID,'button-quiz-end').click()
                if wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[1]").text != 'a=1 Incorrect' and wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[2]/span").text != 'Correct answer':
                    raise Exception('Can not able to see results after See Results button click')
                testutils.logout(wd)
                
            if args.j <= 10: 
                ##Teacher     
                testutils.login(wd, args.target, username) 
                testutils.go_to_course(wd,coursename)
                time.sleep(0.5)
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                testutils.wait_for(wd,By.ID,"btNewTask").click()
                testutils.wait_for(wd,By.ID,'btn-task-type-quiz').click()
                testutils.wait_for(wd,By.ID,'due-date-edit-checkbox').click()
                if testutils.wait_for(wd,By.ID,'deadline-date').is_displayed() != True and testutils.wait_for(wd,By.ID,'deadline-time-input').is_displayed() != True:
                    raise Exception('No due date: Date and time shoul be disabled')
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").send_keys("Quiz task for no duedate")
                wd.find_element_by_css_selector('.richtext').click()
                wd.find_element_by_css_selector('.richtext').send_keys('Quiz question?')
                wd.find_elements_by_css_selector('SPAN.editable.inputfield.livevalidation')[0].click()
                wd.find_elements_by_css_selector('SPAN.editable.inputfield.livevalidation')[0].send_keys('Y')
                wd.find_elements_by_css_selector('SPAN.editable.inputfield.livevalidation')[1].click()
                wd.find_elements_by_css_selector('SPAN.editable.inputfield.livevalidation')[1].send_keys('N')
                wd.find_element_by_xpath("//div[@class='block'][2]/div[2]/label/input").click() 
                testutils.wait_for(wd,By.ID,'btn_edit_save').click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,'SPAN.task-duedate.pull-left').text != '(No due date)':
                    raise Exception("It should be 'No due date'")
                
        except Exception, e:
            testutils.save_screenshot(self)
            testutils.save_browser_log(self)
            raise
            
    def displayValues(self):
        wd = self.wd; coursename = self.coursename
        if wd.find_element_by_id("full-name").find_element_by_tag_name("span").text != "New task":
            raise Exception("Task name default value should be 'New task'")
        if wd.find_element_by_id("task-view-course-name").text != coursename:
            raise Exception("Course name should be "+coursename+" " +"but it is" +" "+wd.find_element_by_id("task-view-course-name").text)
        d = date.today()+timedelta(days=7)
        due_date ="%s.%s.%s" % (d.day, '0'+str(d.month), d.year)
        if wd.find_element_by_id("deadline-date").get_attribute("value") != due_date:
            raise Exception("Default due date should be"+" "+due_date+" "+"but it is"+" "+wd.find_element_by_id("deadline-date").get_attribute("value"))
        current_time = time.strftime("%H:%M")
        if wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value") != current_time:
                raise Exception("Default due time should be"+" "+current_time+" "+"but it is"+" "+wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value"))            
        return          
    
    def addAnswerOption_question1(self):
        wd = self.wd
        for i in range(1,5): 
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").click()
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").clear()
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys(" ")
            if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "This field can not be empty":
                raise Exception("Answer option"+" "+str(i)+" "+ "can not be empty")
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("jjdddddddddddddddddddddd Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good no Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good nohhhhhhhhhhhhhfffffffffhhhhhhhhhhhhhhhhhhhhhhhhhhdddddddddddssddhhhhhhh")
            if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "Character limit has been reached":
                raise Exception("Quiz answer"+" " +str(i)+" "+":Character limit has been reached")  
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").click()
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").clear()
            if i == 1:
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Yes") 
            if i == 2:
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("No") 
            if i == 3:
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Not bad") 
            if i == 4:
                checkboxCount = i
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Not good") 
        return checkboxCount 
     
    def addAnswerOption_question2(self):
        wd = self.wd;
        for i in range(2,7): 
            if i == 2 or i == 4 or i == 6:
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").clear()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "This field can not be empty":
                    raise Exception("Answer option"+" "+str(i)+" "+ "can not be empty")
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("ggddddddddddddddddddddddddNot good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good no Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good nohhhhhhhhhhhhhfffffffffhhhhhhhhhhhhhhhhhhhhhhhhhhdddddddddddssddhhhhhhh")
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "Character limit has been reached":
                    raise Exception("Quiz answer"+" " +str(i)+" "+":Character limit has been reached")
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").clear()
                if i == 2:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("Yes") 
                if i == 4:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("No") 
                if i == 6:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("May be") 
        return 
    
    def add3more_questions(self): ##question No.3,4,5 
        wd = self.wd;
        for j in range(2,5):  
                        ##Questions
            wd.find_element_by_id('btn-add-new-question').click()
            wd.find_elements_by_css_selector('.richtext')[j].click()
            wd.find_elements_by_css_selector('.richtext')[j].send_keys('sssssssss sdfghfjkhdkkkkkkkkkkk erfygethrtbw hgrtereht jgerygret wjfygreert whrgyreret reyetert erytere sdfshbfffffffffffffffffffffffff sdjhfuriw7tsjdgfh weurytwerwetewur dfgwyuwr wdfywufqwehf wjfdwertw sjdfgryetret weyrwtwyrutfrw wuerw weytreywew ryfeutfeyfgrew rwyyyyyyyyyyyyw ryteureb rfyeetet erftrejhbgre djswhfwgfw dfhrfetjgre wjerghvbgfsjd qejwerwfbrf ejwgrhtrwg sfgbfhhhhhhhhhhhhhhhhhhhhhhhg gsssssssssssss sdhgffsjhvsdgggg ggggggggggggggggggggggggggggggggggggg ggggggggggggggggggggggggggggg gggggggggggggggggggggggggggggnciwcfhccccccwwwwwwwwwwwwwwwwfhhhhhhhhhhhhhhhiwwwwwwwwwwwwwfhhhhhhhhhhhhhhhhhhhhhnsjjjjjjjjjjjjjjjjjjjjj wdeferggggggggggggggggggd frgteeeeeeeeeeeeeeee dfgegtey dfffdgfdhghgfhjhj ghhyruruytu dfhrghghyruyuy dfhghgh xsfggggggggggggggggd fddgffhgfjhj fghgfffffffffffffffff dsfyreter eyruwerwef ruygfwriywtw eyrgfwerew weiyreiteri jgdjjhgytyiadfgt r rfgtyreityer wriyterte wiyterutheiuge grhreiugte kgrhrethekyetwrthwjtwrsgu reiignalwerfuwinbfdgfdeguer fskhgvfdgvfdgjfdg bkdjfbhgdkdbnsiuuuuuuuuuuuv ffffffffffffffffffffjb jdffffffffffffffff jrfhrwdfvksfjdvsdfhsdfsdfhsdfjsd fjkghdhghgh hdfisufgyerwigbireuywityrwutr fhgureietigrwehgurww rgyireiyteriterueyt sfjgerhtjtrekherytjeureri riuyteriutriheueyer wrutieerieet gfurieeyrter riuyretierut dfk sfgyfsyff eygrreitertieurt figyeirteier feigteryterter rigyeryter reiyteertr ertyerergtr eryyergrwgre weryeeeut feigyreter rieyteergetr fdihgfeeyriert dhgeytteiruwsfvh nsfdiureofhreigeugi eufywirwegtfrw riueterbgvsweuithfrwihvbsolidghure dfgihieurgert eygreitghrwuf ryyeert wriuwtfw dghdghjtryorhtijboeiguty ghhredueitgery fbgrekgteheky ekiguheirugterfgbvsnkfhgryutw dwfuhwritufyrwtiuw ifyriwetyert erygtreiuteyr fiugyreiteru dfhsiugyeygety gierigtyeiuyte rgtireigtuert rieytieriugte riyiretyert sfgfdhtrytuy5uytutuiti fbdhghgrh rtyrtyty tytryt fgfghrhyr nnnnnnaaaaaaaaa ffffffffff  vvvvvvvvvvv iiiiiiiiii kkkkkkkkkkkddddddd hhhhhhhhhhh lllllllll qqqqqqqqqqaaaaaaaaaa zzzzzzz            cccccccccccccc .............888888888884444444  222222222222222 ergeyetytyt dfgherygterydfhdf sdfhtdehtr dfygwytew dhhhhhhhhhhhhtr fsygutrw eyreeeew weyrfteweuw')
            time.sleep(1)
               ##Answer options
            for i in range(2,5): 
                if i == 2 or i == 4:
                    if j == 2:
                        b = 6
                    if j == 3:
                        b = 8 
                    if j == 4:
                        b = 10   
                    wd.find_element_by_xpath("//div[@class='block']["+str(b)+"]/div["+str(i)+"]/span").click()
                    wd.find_element_by_xpath("//div[@class='block']["+str(b)+"]/div["+str(i)+"]/span").clear()
                    if i == 2: ##ans option 1
                        wd.find_element_by_xpath("//div[@class='block']["+str(b)+"]/div["+str(i)+"]/span").send_keys("Option 1") 
                    if i == 4: ##ans option 2
                        wd.find_element_by_xpath("//div[@class='block']["+str(b)+"]/div["+str(i)+"]/span").send_keys("Option 2") 
                wd.find_element_by_xpath("//div[@class='block']["+str(b)+"]/div[4]/label/input").click()          
        wd.find_element_by_id("btn_edit_save").click()
        time.sleep(2.5) 
        return    
    def tearDown(self):
        testutils.tear_down(self)            
