#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.tasks.teacher_can_view_submissions'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'student@127.0.0.1'
        self.teachername = 'teacher@127.0.0.1'
        self.coursename = 'Teacher Can View Submissions Course'
        self.taskname = 'Teacher Can View Submissions Task'
        testdata.delete(self.args, 'submissions', 'student@127.0.0.1')
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.teachername)
        testdata.require(self.args, 'user', self.teachername)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.require(self.args, 'task_in_course', self.taskname, self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.teachername, self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.username, self.coursename)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename; teachername = self.teachername; taskname = self.taskname
        try:
            if args.j <= 1:
                # Teacher verify before submitting as student
                testutils.login(wd, args.target, teachername)
                wd.find_element_by_xpath("//div[@class='course-tasks']//td[.='"+taskname+"']")
                wd.find_element_by_xpath("//div[@class='course-tasks']//strong[.='(0 of 1 returned)']")
                wd.find_element_by_xpath("//a[@id='bt-settings']/img").click()
                testutils.wait_for(wd, By.ID, "header-logout-link")
                testutils.wait_for(wd, By.ID, "header-logout-link").click()
                
                # Submit something as student
                testutils.login(wd, args.target, username)
                wd.find_element_by_xpath("//div[@class='span9']//div[.='"+coursename+"']").click()
                testutils.wait_for(wd, By.ID, "course-button-tasks")
                testutils.wait_for(wd, By.ID, "course-button-tasks").click()
                testutils.wait_for(wd, By.XPATH, "//div[@class='task-block']//span[.='"+taskname+"']")
                wd.find_element_by_xpath("//div[@class='task-block']//span[.='"+taskname+"']").click()
                testutils.wait_for(wd, By.ID, "btn-submit-task")
                testutils.wait_for(wd, By.ID, "btn-submit-task").click()
                testutils.wait_for(wd, By.ID, "submission-text").click()
                testutils.wait_for(wd, By.ID, "submission-text").clear()
                testutils.wait_for(wd, By.ID, "submission-text").send_keys("Foo Bar Submission")
                time.sleep(1)
                testutils.wait_for(wd, By.ID, "btn-draft-submission").click() #save draft
                wd.find_element_by_xpath("//a[@id='bt-settings']/img").click()
                testutils.wait_for(wd, By.ID, "header-logout-link")
                testutils.wait_for(wd, By.ID, "header-logout-link").click()
                
                # Teacher tests
                testutils.login(wd, args.target, teachername)
                wd.find_element_by_xpath("//div[@class='course-tasks']//strong[.='(0 of 1 returned)']")
                wd.find_element_by_xpath("//a[@id='bt-settings']/img").click()
                testutils.wait_for(wd, By.ID, "header-logout-link")
                testutils.wait_for(wd, By.ID, "header-logout-link").click()
                
                # Submit final with student
                testutils.login(wd, args.target, username)
                wd.find_element_by_xpath("//div[@class='span9']//div[.='"+coursename+"']").click()
                testutils.wait_for(wd, By.ID, "course-button-tasks").click()
                wd.find_element_by_xpath("//div[@class='task-block']//span[.='"+taskname+"']").click()
                testutils.wait_for(wd, By.ID, "btn-submit-task")
                testutils.wait_for(wd, By.ID, "btn-submit-task").click()
                testutils.wait_for(wd, By.ID, "btn-submit-submission").click()
                testutils.wait_for(wd, By.XPATH, "//a[@id='bt-settings']/img")
                wd.find_element_by_xpath("//a[@id='bt-settings']/img").click()
                testutils.wait_for(wd, By.ID, "header-logout-link")
                testutils.wait_for(wd, By.ID, "header-logout-link").click()
                
                # Teacher tests
                testutils.login(wd, args.target, teachername)
                wd.find_element_by_xpath("//div[@class='course-tasks']//strong[.='(All returned)']")
            
        except Exception, e:
            testutils.save_screenshot(self)
            raise
            
    def tearDown(self):
        testutils.tear_down(self)

