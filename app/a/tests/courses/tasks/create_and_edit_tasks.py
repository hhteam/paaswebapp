#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys
from datetime import timedelta,date,datetime
import lxml
from selenium.webdriver.common.keys import Keys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.tasks.create_and_edit_tasks'

    progress_status = '0'

     
    def setUp(self):
        testutils.set_up(self)
      
    def testdata_setup(self):
        self.username = 'teacher@127.0.0.1'
        self.studentname = 'student@127.0.0.1'
        self.coursename = 'Create and edit tasks Course'
        self.taskname = 'Newly created task'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.studentname)
        testdata.require(self.args, 'user', self.studentname)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        #testdata.require(self.args, 'task_in_course', self.taskname, self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        testdata.delete(self.args, 'user_is_teacher',  self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.studentname, self.coursename)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename; taskname = self.taskname; studentname = self.studentname
        try:
            testutils.login(wd, args.target, username)
            testutils.go_to_course(wd,coursename)
            if args.j <= 1:
                          # go to task
                testutils.wait_for(wd,By.ID,"course-button-tasks").click()
                wd.refresh()
                time.sleep(10)
                wd.find_element_by_id("btNewTask").click()
                if wd.find_element_by_id("btn-task-type-upload").get_attribute("class") != "btn active":
                    raise Exception()
                  ##check default values
                self.displayValues()            
                
            if args.j <= 2:        
                     # Written text or file upload task 
                       ##default save without adding,due date;.
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()    
                wd.find_element_by_id("full-name").send_keys("New task")        
                wd.find_element_by_id("intro").click()
                wd.find_element_by_id("intro").clear()
                wd.find_element_by_id("intro").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Instructions should not be empty")
                wd.find_element_by_id("intro").send_keys("Amazing task") 
                testutils.wait_for(wd,By.ID,"btn_edit_save").click()
                #time.sleep(2) 
                testutils.wait_for(wd,By.ID,"btNewTask")
               
                         #check changes 
                #if testutils.wait_for(wd,By.CSS_SELECTOR,'.block.task-name').find_element_by_tag_name("span").text  != "New task":
                    #raise Exception("Task name should be 'New task'") 
                    
                wd.find_element_by_id("btNewTask").click()
                if wd.find_element_by_id("btn-task-type-upload").get_attribute("class") != "btn active":
                    raise Exception()
                             ##check default values
                if wd.find_element_by_id("full-name").find_element_by_tag_name("span").text != "New task":
                    raise Exception("Task name default value should be 'New task'")
                if wd.find_element_by_id("task-view-course-name").text != coursename:
                    raise Exception("Course name should be "+coursename+" " +"but it is" +" "+wd.find_element_by_id("task-view-course-name").text)
                d = date.today()+timedelta(days=7)
                due_date ="%s.%s.%s" % (d.day, d.month, d.year)
                if wd.find_element_by_id("deadline-date").get_attribute("value") != due_date:
                    raise Exception("Default due date should be"+" "+due_date+" "+"but it is"+" "+wd.find_element_by_id("deadline-date").get_attribute("value"))
                current_time = time.strftime("%H:%M")
                if wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value") != current_time:
                    raise Exception("Default due time should be"+" "+current_time+" "+"but it is"+" "+wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value"))            
            if args.j <= 2:        
                     # Written text or file upload task 
                       ##default save without adding task title,due date;task name should be 'New task'.    
                wd.find_element_by_id("intro").click()
                wd.find_element_by_id("intro").clear()
                wd.find_element_by_id("intro").send_keys(" ")
                #wd.find_element_by_id("deadline-date").click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Instructions shoul not be empty")
                wd.find_element_by_id("intro").send_keys("Amazing task") 
                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(1) 
                         #check changes 
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.block.task-name').find_element_by_tag_name("span").text  != "New task":
                    raise Exception("Task name should be 'New task'") 

            if args.j <= 3:              
                     ##check with all fields
                wd.find_element_by_id("btNewTask").click()             
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()
                wd.find_element_by_id("full-name").send_keys(" ")

                #wd.find_element_by_id("intro").click()

                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Task name shoul not be empty")
                wd.find_element_by_id("full-name").click()    
                wd.find_element_by_id("full-name").send_keys("Newly created task Newly created taskNewly created taskNewly created taskNewly created taskNewly created taskNewly created taskNewly created taskNewly created task")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "Character limit has been reached":
                    raise Exception("Task name character limit has been reached")
                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()    
                wd.find_element_by_id("full-name").send_keys(taskname)
                wd.find_element_by_id('deadline-date').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"10\")').click();");

                #wd.find_element_by_xpath("//div[@id='deadline-time']/input").click()
                #wd.find_element_by_xpath("//div[@id='deadline-time']/input").clear()
                #wd.find_element_by_xpath("//div[@id='deadline-time']/input").send_keys('11:00')
                  #wd.find_element_by_css_selector(".icon.icon-time").click()
                  #testutils.javascript(wd, "$('div.timepicker-picker:visible tr a i').click();");

                #wd.find_element_by_css_selector(".icon-time").click()
                #wd.find_element_by_css_selector(".timepicker-hour").click()
                #wd.find_element_by_css_selector(".timepicker-hour").send_keys("12.00")
                #wd.find_element_by_xpath("//div[@class='timepicker-hours']/table/tbody/tr[4]/td[0]").click()
                #testutils.javascript(wd, "$('div.timepicker-hours:visible .hours:contains(\"12\")').click();");

                wd.find_element_by_id("intro").click()
                wd.find_element_by_id("intro").clear()
                wd.find_element_by_id("full-name").click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Instructions shoul not be empty")

                wd.find_element_by_id("intro").send_keys("Amazing task") 
                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(2) 
                testutils.wait_for(wd,By.ID,'btNewTask')
                time.sleep(1)
                print wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text
                if wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text != taskname:
                    raise Exception()
                    
            if args.j <= 4:    
                     #Quiz task
                testutils.wait_for(wd,By.ID,"btNewTask").click()    
                testutils.wait_for(wd,By.ID,"btn-task-type-quiz").click()

                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()
                wd.find_element_by_id("full-name").send_keys("Quiz task")
                wd.find_element_by_id('deadline-date').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");
                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"6\")').click();"); 

                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"7\")').click();"); 

                wd.find_element_by_css_selector('.richtext').click()
                wd.find_element_by_css_selector('.richtext').clear()
                wd.find_element_by_css_selector('.richtext').send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Question 1 should not be empty")
                wd.find_element_by_css_selector('.richtext').send_keys("Is quiz good?")
                 ###Add another option 
                for i in range(2):
                    wd.find_elements_by_css_selector('.btn-add-quiz-answer')[0].click()
                    #Answer options
                    checkbox_count =self.addAnswerOption1()
                    ##Check if it is added or not 
                for i in range(1,5): 
                    wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").click()
                    wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").clear()
                    wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys(" ")
                    if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "This field can not be empty":
                        raise Exception("Answer option"+" "+str(i)+" "+ "can not be empty")
                    wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good no")
                    if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "Character limit has been reached":
                        raise Exception("Quiz answer"+" " +str(i)+" "+":Character limit has been reached")
                    wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").click()
                    wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").clear()
                    if i == 1:
                        wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Yes") 
                    if i == 2:
                        wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("No") 
                    if i == 3:
                        wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Not bad") 
                    if i == 4:
                        checkbox_count = i
                        wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Not good")     
                  ##Check if it is added or not 

                if checkbox_count != 4:
                    raise Exception("Could not add another answer option")     
                    ##delete answer option  
                wd.find_elements_by_css_selector('.btn-remove-answer')[1].click()
                    ##check deletion
                checkbox_count -= 1

                if checkbox_count != 3:
                    raise Exception("could not remove answer option ")
                    ###select correct answer option 
                wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/label/input").click()
                     ### Add new questions              
                wd.find_element_by_id("btn-add-new-question").click()
                wd.find_elements_by_css_selector('.btn-add-quiz-answer')[1].click()
                self.addAnswerOption2()

                print checkbox_count  
                if checkbox_count != 3:
                    raise Exception("could not remove answer option ")
                    ###selecet correct answer option 
                wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/label/input").click()
                #if wd.find_element_by_xpath("//div[@class='quiz_answer'][2]/label/input").is_selected() == "true":
                    #raise Exception("")   
                    
                     ### Add new questions              
                wd.find_element_by_id("btn-add-new-question").click()
                wd.find_elements_by_css_selector('.btn-add-quiz-answer')[1].click()
                self.addAnswerOption()
                wd.find_element_by_xpath("//div[@class='block'][4]/div[6]/label/input").click()
                wd.find_element_by_id("btn_edit_save").click()
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Question 2 shoul not be empty")
                wd.find_elements_by_css_selector('.richtext')[1].click()
                wd.find_elements_by_css_selector('.richtext')[1].clear()
                wd.find_elements_by_css_selector('.richtext')[1].send_keys("Is task added?")
                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(2) 
                testutils.wait_for(wd,By.ID,'btNewTask')

                time.sleep(1)
                print wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text
                if wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text != "Quiz task":
                    raise Exception()
     
 
            if args.j <= 5:
                    ##No submission required
                wd.find_element_by_id("btNewTask").click()    
                testutils.wait_for(wd,By.ID,'btn-task-type-nosubmissions').click()

                self.displayValues() 

                wd.find_element_by_id("full-name").click()
                wd.find_element_by_id("full-name").clear()
                wd.find_element_by_id("full-name").send_keys("No submission required task")
                wd.find_element_by_id('deadline-date').click()
                testutils.javascript(wd, "$('div.datepicker-days:visible tr th.next').click();");

                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"15\")').click();");
                time.sleep(1) 

                testutils.javascript(wd, "$('div.datepicker-days:visible .day:contains(\"15\")').click();"); 
                wd.find_element_by_id("intro").click()
                wd.find_element_by_id("intro").clear()
                wd.find_element_by_id("intro").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,".tooltip-inner").text != "This field can not be empty":
                    raise Exception("Instructions should not be empty")
                wd.find_element_by_id("intro").send_keys("No submission required for this task") 

                testutils.wait_for(wd,By.ID,"btn_edit_save").click()
                time.sleep(5) 
                
            if args.j <= 6:
                
                 ### check saved task
                testutils.wait_for(wd,By.ID,'btNewTask')   
                time.sleep(2)
                if wd.find_element_by_xpath("//div[@class='task-block'][3]/a/span").text != taskname:
                    raise Exception()  
                time.sleep(2)  
                print wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text
                if wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").text != "Quiz task":
                    raise Exception()   
                if wd.find_element_by_xpath("//div[@class='task-block'][4]/a/span").text != "No submission required task":
                    raise Exception()              
                        ###check selected quiz answer option
                wd.find_element_by_link_text("Quiz task").click()        
                #wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").click()
                testutils.wait_for(wd,By.ID,'btn_edit').click()  
                if wd.find_element_by_xpath("//div[@class='block'][2]/div[4]/label/input").is_selected() != True:
                    raise Exception() 
                if wd.find_element_by_xpath("//div[@class='block'][4]/div[6]/label/input").is_selected() != True:
                    raise Exception()
                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(1)  
                  ##check saved task are displaying based on days 
                testutils.wait_for(wd,By.ID,'btNewTask')  
                for i in range(1,5):
                    days_count = wd.find_element_by_xpath("//div[@class='task-block']["+str(i)+"]/span/strong").text
                    days_count,d = days_count.split()
                    days_count = days_count[1:]
                    if i == 1:
                        count1 = days_count
                    if i == 2:
                        count2 = days_count 
                    if i == 3:
                        count3 = days_count
                    if i == 4:
                        count4 = days_count
                if not int(count1) < int(count2) < int(count3) < int(count4):
                    raise Exception("Tasks are not displayed based on due date") 
                testutils.logout(wd)
                     
            if args.j <= 7:
                  ##  student
                testutils.login(wd, args.target, studentname) 
                time.sleep(1)
                   ##checking tasks are there 
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a/table/tbody/tr/td[1]").text != "New task":
                    raise Exception()
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[2]/a/table/tbody/tr/td[1]").text != "Quiz task":
                    raise Exception()
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[3]/a/table/tbody/tr/td[1]").text != taskname:
                    raise Exception()        
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[4]/a/table/tbody/tr/td[1]").text != "No submission required task":
                    raise Exception()
                      ###check can able to see task 
                self.visibleToStudents_and_finishQuiz()
                global progress_status 
                progress_status = '0'
                self.submit_textTask()
                      ##check able to give rating for course
                wd.find_element_by_id('bt-course').click()  
                testutils.wait_for(wd,By.CSS_SELECTOR,'.subtitle-blue').click()
                testutils.logout(wd)
            
            if args.j <= 8:
                   ##check tasks notification 
                testutils.login(wd, args.target, username)
                notification = testutils.wait_for(wd, By.ID, "courses_ncount").text  
                if len(notification) == 0 or int(notification) != 1: 
                    raise Exception("Notification count should be 2, it is "+str(notification))
                testdata.mark_notifications_read(args, username)
                time.sleep(1)
                   ##check display values
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[2]/a/table/tbody/tr/td[2]/strong").text != "(All returned)" and wd.find_element_by_xpath("//div[@class='course-tasks']/div[2]/a/table/tbody/tr/td[1]").text != "Quiz task":
                    raise Exception("Quiz Task:should be all returned but it is "+wd.find_element_by_xpath("//div[@class='course-tasks']/div[2]/a/table/tbody/tr/td[2]/strong").text)
                if wd.find_element_by_xpath("//div[@class='course-tasks']/div[3]/a/table/tbody/tr/td[2]/strong").text != "(All returned)" and wd.find_element_by_xpath("//div[@class='course-tasks']/div[2]/a/table/tbody/tr/td[1]").text != taskname:
                    raise Exception("Newly Created task :should be all returned but it is "+wd.find_element_by_xpath("//div[@class='course-tasks']/div[3]/a/table/tbody/tr/td[2]/strong").text)
                    ##check no notification for task that is saved as draft by student
                wd.find_element_by_xpath("//div[@class='course-tasks']/div[1]/a").click()
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.alert.alert-error') and wd.find_element_by_css_selector('.alert.alert-error').text != "Not returned":
                    if testutils.is_element_visible(wd,By.CSS_SELECTOR,'.task-submission-file'):
                        raise Exception(" ")
                self.grade_task()        
                self.view_finished_task()
                   #change course to public ,to check student able to give feedback       
                time.sleep(1)   
                wd.find_element_by_id('bt-course').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.subtitle-blue').click()
                testutils.wait_for(wd,By.ID,'course-button-settings').click()
                testutils.wait_for(wd,By.ID,'button-publish-course').click()
                testutils.logout(wd)
                
            if args.j <= 9:
                  ##  student   ##for feedback
                testutils.login(wd, args.target, studentname) 
                time.sleep(1) 
                testutils.wait_for(wd,By.CSS_SELECTOR,'.subtitle-blue').click() 
                if testutils.is_element_visible(wd,By.CSS_SELECTOR,'.fa.fa-star-o'):
                    raise Exceoption('Student can able to give rating for public course but it should not be')  
                  ##check progress status 
                time.sleep(1)  
                if wd.find_element_by_css_selector('.bar').text == '100%' or wd.find_element_by_css_selector('.bar').text == '0%':
                    raise Exception("Completed task percentage should be 25% but it is " +wd.find_element_by_css_selector('.bar').text)      
                ## need to do feedback here
                testutils.logout(wd)
            
            if args.j <= 10:
                ###Checking progressbar status for tasks  
                #global progress_status 
                progress_status = 'Progress'
                testutils.login(wd, args.target, username)
                testutils.go_to_course(wd,coursename)
                testutils.wait_for(wd,By.ID,'course-button-tasks').click()
                testutils.wait_for(wd,By.ID,'btNewTask')
                for i in range(0,4):  
                    i = 0
                    wd.find_elements_by_css_selector('.block.task-name')[i].click()
                    testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-primary.dropdown-toggle').click()
                    time.sleep(1)
                    testutils.wait_for(wd,By.ID,'btn_delete_task').click()
                    time.sleep(1)
                    testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-danger').click()
                    time.sleep(0.5)
                testutils.wait_for(wd,By.ID,'btNewTask').click()
                time.sleep(1)
                testutils.wait_for(wd,By.ID,'intro').click()
                wd.find_element_by_id('intro').send_keys('task about courses')
                wd.find_element_by_id('btn_edit_save').click()
                time.sleep(1)
                wd.find_element_by_id('course-button-overview').click()
                time.sleep(5)
                #wd.find_element_by_xpath('//div[@id=section-0]/div/h3/span/button').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.btn_edit').click()
                testutils.wait_for(wd, By.ID, 'skills-input').click()
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys("Test Skill1,Skill2,Skill3")
                testutils.wait_for(wd, By.ID, 'skills-input').send_keys(Keys.RETURN)
                time.sleep(0.5)
                wd.find_element_by_id('btn_edit_save').click()
                time.sleep(1) 
                ##logout and sign in as a student
                testutils.logout(wd)
                testutils.login(wd,args.target,studentname)
                self.submit_textTask()
                wd.find_element_by_id('bt-course').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.subtitle-blue').click() 
                time.sleep(1)
                #print wd.find_element_by_xpath("//div[@id='course-menu-right']/span").text
                if wd.find_element_by_xpath("//div[@id='course-menu-right']/span").text != '0%':
                    raise Exception()
                ##logout and sign in as a teacher
                testutils.logout(wd)
                testutils.login(wd, args.target, username)
                testutils.go_to_course(wd,coursename)
                testutils.wait_for(wd,By.ID,'course-button-tasks').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.task-name.setupdone').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'a.task-submission-file.task-submission-text.btn').click()
                time.sleep(1)
                wd.find_element_by_id('cboxClose').click()    
                wd.find_element_by_css_selector('a.grade-item').click()
                wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").click()
                wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").clear()
                wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").send_keys("100") 
                wd.find_element_by_xpath("//div[@class='modal-body']/textarea").click()
                wd.find_element_by_xpath("//div[@class='modal-body']/textarea").send_keys("Good work")
                wd.find_element_by_link_text('Save').click()
                time.sleep(1)
                 ##logout and sign in as a student
                testutils.logout(wd)
                testutils.login(wd,args.target,studentname)
                testutils.go_to_course(wd,coursename)
                time.sleep(1)
                if wd.find_element_by_css_selector('.bar').text != '100%':
                    raise Exception("Completed task percentage should be 100% but it is " +wd.find_element_by_css_selector('.bar').text)
                if len(wd.find_elements_by_css_selector('.skill-label')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.skill-label'))))
                time.sleep(1)
                wd.find_element_by_id('bt-settings').click()
                testutils.wait_for(wd,By.LINK_TEXT,'My Profile').click()
                time.sleep(0.5)
                if not testutils.is_element_visible(wd,By.ID,'skills-progress'):
                    raise Exception('Skill labels should be visible ')
                if len(wd.find_elements_by_css_selector('.label-success')) != 3:
                    raise Exception("There should be three skill labels, there is "+str(len(wd.find_elements_by_css_selector('.label-success'))))   
                time.sleep(1)
                


                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(3) 
                testutils.wait_for(wd,By.ID,'btNewTask')
                #time.sleep(1)
                print wd.find_element_by_xpath("//div[@class='task-block'][4]/a/span").text
                if wd.find_element_by_xpath("//div[@class='task-block'][4]/a/span").text != "No submission required task":
                    raise Exception()
                
            if args.j <= 6:
                        ###check selected quiz answer option
                #testutils.wait_for(wd,By.ID,'btNewTask')
                wd.find_element_by_xpath("//div[@class='task-block'][2]/a/span").click()
                testutils.wait_for(wd,By.ID,'btn_edit').click()  
                if wd.find_element_by_xpath("//div[@class='block'][2]/div[4]/label/input").is_selected() != True:
                    raise Exception() 
                if wd.find_element_by_xpath("//div[@class='block'][4]/div[6]/label/input").is_selected() != True:
                    raise Exception()
                wd.find_element_by_id("btn_edit_save").click()
                time.sleep(1)        
                

        except Exception, e:
            testutils.save_screenshot(self)
            testutils.save_browser_log(self)
            raise
    def addAnswerOption(self):
        wd = self.wd;
        for i in range(2,7): 
            if i == 2 or i == 4 or i == 6:
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").clear()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "This field can not be empty":
                    raise Exception("Answer option"+" "+str(i)+" "+ "can not be empty")
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good no")
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "Character limit has been reached":
                    raise Exception("Quiz answer"+" " +str(i)+" "+":Character limit has been reached")
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").clear()
                if i == 2:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("Yes") 
                if i == 4:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("No") 
                if i == 6:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("May be") 
        return 
           
            
    def displayValues(self):
        wd = self.wd; coursename = self.coursename
        if wd.find_element_by_id("full-name").find_element_by_tag_name("span").text != "New task":
            raise Exception("Task name default value should be 'New task'")
        if wd.find_element_by_id("task-view-course-name").text != coursename:
            raise Exception("Course name should be "+coursename+" " +"but it is" +" "+wd.find_element_by_id("task-view-course-name").text)
        d = date.today()+timedelta(days=7)
        due_date ="%s.%s.%s" % (d.day, d.month, d.year)
        #if wd.find_element_by_id("deadline-date").get_attribute("value") != due_date:
            #raise Exception("Default due date should be"+" "+due_date+" "+"but it is"+" "+wd.find_element_by_id("deadline-date").get_attribute("value"))
            
        current_time = time.strftime("%H:%M")
        #if wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value") != current_time:
                #raise Exception("Default due time should be"+" "+current_time+" "+"but it is"+" "+wd.find_element_by_id("deadline-time").find_element_by_tag_name("input").get_attribute("value"))            
        return      
         
    def addAnswerOption1(self):
        wd = self.wd
        for i in range(1,5): 
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").click()
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").clear()
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys(" ")
            if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "This field can not be empty":
                raise Exception("Answer option"+" "+str(i)+" "+ "can not be empty")
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("jjdddddddddddddddddddddd Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good no Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good nohhhhhhhhhhhhhfffffffffhhhhhhhhhhhhhhhhhhhhhhhhhhdddddddddddssddhhhhhhh")
            if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "Character limit has been reached":
                raise Exception("Quiz answer"+" " +str(i)+" "+":Character limit has been reached")  
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").click()
            wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").clear()
            if i == 1:
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Yes") 
            if i == 2:
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("No") 
            if i == 3:
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Not bad") 
            if i == 4:
                checkboxCount = i
                wd.find_element_by_xpath("//div[@class='quiz_answer']["+str(i)+"]/span").send_keys("Not good") 
        return checkboxCount
         
    def addAnswerOption2(self):
        wd = self.wd;
        for i in range(2,7): 
            if i == 2 or i == 4 or i == 6:
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").clear()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys(" ")
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "This field can not be empty":
                    raise Exception("Answer option"+" "+str(i)+" "+ "can not be empty")
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("ggddddddddddddddddddddddddNot good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good no Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot Not good Not goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot goodNot good nohhhhhhhhhhhhhfffffffffhhhhhhhhhhhhhhhhhhhhhhhhhhdddddddddddssddhhhhhhh")
                if testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner').text != "Character limit has been reached":
                    raise Exception("Quiz answer"+" " +str(i)+" "+":Character limit has been reached")
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").click()
                wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").clear()
                if i == 2:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("Yes") 
                if i == 4:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("No") 
                if i == 6:
                    wd.find_element_by_xpath("//div[@class='block'][4]/div["+str(i)+"]/span").send_keys("May be") 
        return 
           
    def visibleToStudents_and_finishQuiz(self):
        wd = self.wd; args = self.args; username = self.username
        for i in range(1,4): 
            wd.find_element_by_xpath("//div[@class='course-tasks']/div["+str(i)+"]/a").click()
            if i == 1:
                testutils.wait_for(wd,By.ID,'btn-submit-task')
                if not testutils.is_element_visible(wd,By.ID,'full-name') and wd.find_element_by_id('full-name').text != 'New task':
                    raise Exception("Task Title is not visible to students")
                if not testutils.is_element_visible(wd,By.ID,'due-date-view'):
                    raise Exception("Task due date and time is not visible to students")   
                if not testutils.is_element_visible(wd,By.ID,'intro') and (wd.find_element_by_id('intro').text).strip() != "Amazing task":
                    raise Exception("Task instruction is not visible to students") 
                time.sleep(1)      
                    ## student complete task and save as draft
                testutils.wait_for(wd,By.ID,'btn-submit-task').click() 
                wd.find_element_by_id('submission-text').click()
                wd.find_element_by_id('submission-text').clear() 
                wd.find_element_by_id('submission-text').send_keys("task is submitted")
                count =0
                while  count < 10 and testutils.wait_for(wd,By.ID,"btn-draft-submission").get_attribute("disabled") == 'true':
                    count = count+1
                    time.sleep(1)
                    if count == 10:
                        raise Exception() 
                wd.find_element_by_id('btn-draft-submission').click()
                time.sleep(1)
               
                if not testutils.is_element_visible(wd,By.ID,'no-submission-yet-text') and wd.find_element_by_id('draft-text').text != "Nothing submitted yet":
                    raise Exception()
            if i == 2:
                if not testutils.is_element_visible(wd,By.ID,'full-name') and testutils.wait_for(wd,By.ID,'full-name').text != "Quiz task":
                    raise Exceoption("Quiz task title is not visible to students")
                if not testutils.is_element_visible(wd,By.ID,'due-date-view'):
                    raise Exception("Quiz task due date and time is not visible to students")  
                testutils.wait_for(wd,By.ID,'button-quiz-start').click()
                if  wd.find_elements_by_css_selector ('.quiz_question')[0].is_displayed() and  wd.find_elements_by_css_selector('.quiz_answer_block')[0].is_displayed() == False:
                    raise Exception("Quiz task:Question and answer block is not visible to students")
                     #attempt answer to quiz    
                #wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/label[1]/input").click()
                #wd.find_element_by_xpath("(//input[@type='checkbox'])[6]").click()
                ##wd.find_element_by_css_selector('BUTTON.button-quiz-finish.btn.btn-primary').click()
                ##wd.find_element_by_xpath("//div[@class='content-box']/div[2]/div[5]/button").click()
                #testutils.wait_for(wd,By.ID,"button-quiz-finish").click()
               
                     ### Quiz results and checking information
                #if testutils.wait_for(wd,By.CSS_SELECTOR,'.block.quiz_result').text != "1 / 2 correct":
                    #raise Exception("Quiz result should be 1 / 2 correct but it is "+wd.find_element_by_css_selector('.block.quiz_result').text)
                           ###Question 1
                #if wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[1]").text != "Yes Incorrect":
                    #raise Exception("Student selected option is incorrect but it is not indicate to students")
                #if wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[1]").text == "Yes Incorrect":
                    #if wd.find_element_by_xpath("//div[@class='quiz_answer_block'][1]/div[2]").text != "No Correct answer":
                        #raise Exception("Correcet answer is not showing to student")
                            ###Question 2
                #if  wd.find_element_by_xpath("//div[@class='block'][2]/div[2]/div[3]").text != "May be Correct":
                    #raise Exception("Student select the correct option but it is not indicate to student")     
                wd.find_element_by_id("bt-course").click()
            if i == 3:  
                if (wd.find_element_by_id('intro').text).strip() != "No submission required for this task" and not testutils.is_element_visible(wd,By.ID,'intro'):
                    raise Exception("Task is not visible to students") 
                if  wd.find_element_by_id('intro').get_attribute('contenteditable') != None:
                    raise Exception("No submission task is able to edit by student but it should not be")                           
            wd.find_element_by_id('bt-course').click()
        return   
     
    def submit_textTask(self):
        wd = self.wd;
        if progress_status == 'Progress':
            wd.find_element_by_xpath("//div[@class='course-tasks']/div/a").click()
        else:
            wd.find_element_by_xpath("//div[@class='course-tasks']/div[3]/a").click()  ##div[2] should be
        time.sleep(3) 
        testutils.wait_for(wd,By.ID,'btn-submit-task').click()
        wd.find_element_by_id('submission-text').click()
        wd.find_element_by_id('submission-text').clear() 
        wd.find_element_by_id('submission-text').send_keys("Text task submitted")
        count =0
        while  count < 10 and testutils.wait_for(wd,By.ID,"btn-submit-submission").get_attribute("disabled") == 'true':
            count = count+1
            time.sleep(1)
            if count == 10:
                raise Exception() 
        wd.find_element_by_id('btn-submit-submission').click()
        time.sleep(1) 
        return
   
    def grade_task(self):
        wd = self.wd; coursename = self.coursename
        testutils.go_to_course(wd,coursename)
        testutils.wait_for(wd,By.ID,"course-button-tasks").click()
        time.sleep(1)
        wd.find_elements_by_css_selector('.block.task-name.setupdone')[2].click() #1
        time.sleep(2)
        testutils.wait_for(wd,By.CSS_SELECTOR,'a.task-submission-file.task-submission-text.btn').click()
        time.sleep(1)
        if wd.find_element_by_xpath("//div[@id='cboxLoadedContent']/div[2]/p").text != "Text task submitted":
            raise Exception("Submitted task is 'Text task submitted' but it is "+ wd.find_element_by_xpath("//div[@id='cboxLoadedContent']/div[2]/p").text)
        wd.find_element_by_id('cboxClose').click()    
        wd.find_element_by_css_selector('a.grade-item').click()
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").click()
            #without grade
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").send_keys(" ")
        wd.find_element_by_link_text('Save').click()
        time.sleep(1)
        if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.control-group.error'):
            raise Exception("Grade value should not be empty")
              #grade value above 100
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").clear()      
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").send_keys("103")
        time.sleep(1)
        wd.find_element_by_link_text('Save').click()
        time.sleep(1)
        if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'.control-group.error'):
            raise Exception("Grade value should not be above 100")
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").click()
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").clear()
        wd.find_element_by_xpath("//div[@class='modal-body']/div[1]/input").send_keys("90") 
        wd.find_element_by_xpath("//div[@class='modal-body']/textarea").click()
        wd.find_element_by_xpath("//div[@class='modal-body']/textarea").send_keys("Good")
        wd.find_element_by_link_text('Save').click()
             #check graded value is correct
        time.sleep(1) 
        if (wd.find_element_by_css_selector('A.grade-item.ignored_link.btn').text).strip() != 'Grade 90/100':
            raise Exception("Grade value should be Grade 90/100 but it is "+ wd.find_element_by_css_selector('a.grade-item').text)  
           
        return
    
    def view_finished_task(self):
        wd = self.wd; coursename = self.coursename
        testutils.go_to_course(wd,coursename)
        wd.find_element_by_id("course-button-tasks").click()
        wd.find_element_by_link_text("Quiz task").click() 
        wd.find_element_by_css_selector('a.btn.setupdone').click()
        return        
    
    def student_feedback(self):
        wd = self.wd; coursename = self.coursename
        testutils.go_to_course(wd,coursename)
        return
    
    def tearDown(self):
        testutils.tear_down(self)

