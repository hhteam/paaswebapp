#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'courses.tasks.can_submit_assignment_text'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'student@127.0.0.1'
        self.teachername = 'teacher@127.0.0.1'
        self.coursename = 'Submit Assignment Text Course'
        self.taskname = 'Submit Assignment Task'
        testdata.delete(self.args, 'submissions', 'student@127.0.0.1')
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.teachername)
        testdata.require(self.args, 'user', self.teachername)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.require(self.args, 'task_in_course', self.taskname, self.coursename)
        testdata.require(self.args, 'user_is_teacher', self.teachername, self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.username, self.coursename)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; coursename = self.coursename; teachername = self.teachername; taskname = self.taskname
        try:
            testutils.login(wd, args.target, username)
            
            if args.j <= 1:          
                wd.find_element_by_xpath("//div[@class='span9']//div[.='"+coursename+"']")
                if not testutils.is_element_present(wd, By.XPATH, "//div[@class='course-tasks']//td[.='"+taskname+"']"):
                    raise Exception("FAIL: Task "+taskname+" expected in dashboard")
                testutils.go_to_task(wd, taskname, coursename)
                if wd.find_element_by_css_selector(".submit_file").is_displayed():
                    raise Exception("FAIL: File submit button must not be visible before edit mode")
                if not testutils.is_element_present(wd, By.ID, "btn-submit-task"):    
                    raise Exception("FAIL: There must be a submit task button to go in to edit mode")
                if wd.page_source.find("Nothing submitted yet") == -1:
                    raise Exception("FAIL: Task must have 'nothing submitted yet' text visible if nothing submitted yet")
                wd.find_element_by_id("btn-submit-task").click()
                # File submit button must be visible in edit mode
                testutils.wait_for(wd, By.CSS_SELECTOR, ".submit_file")
                wd.find_element_by_id("submission-text").click()
                wd.find_element_by_id("submission-text").clear()
                wd.find_element_by_id("submission-text").send_keys("Foo Bar Submission")
                time.sleep(1)
                testutils.wait_for(wd, By.ID, "btn-draft-submission").click() #save draft
                testutils.wait_for(wd, By.ID, "bt-course")
                if wd.page_source.find("Foo Bar Submission") == -1:
                    raise Exception("Draft submission should have been saved")
                wd.find_element_by_xpath("//a[@id='bt-course']/div").click()
                wd.find_element_by_xpath("//div[@class='span9']//div[.='"+coursename+"']")
                if not testutils.is_element_present(wd, By.XPATH, "//div[@class='course-tasks']//td[.='"+taskname+"']"):
                    raise Exception("FAIL: Task "+taskname+" expected in dashboard")
                testutils.go_to_task(wd, taskname, coursename)
                testutils.wait_for(wd, By.ID, "btn-submit-task").click()
                while testutils.wait_for(wd, By.ID, "btn-submit-submission").get_attribute("disabled") == 'true':
                    time.sleep(0.1)
                wd.find_element_by_id("btn-submit-submission").click() #save final
                time.sleep(1) # wait for save to happen in bg
                testutils.wait_for(wd, By.XPATH, "//a[@id='bt-course']/div")
                wd.find_element_by_xpath("//a[@id='bt-course']/div").click()
                testutils.wait_for(wd, By.XPATH, "//div[@class='span9']//div[.='"+coursename+"']")
                if testutils.is_element_present(wd, By.XPATH, "//div[@class='course-tasks']//td[.='"+taskname+"']"):
                    raise Exception("FAIL: Task "+taskname+" NOT expected in dashboard")
            
        except Exception, e:
            testutils.save_screenshot(self)
            raise
            
    def tearDown(self):
        testutils.tear_down(self)
