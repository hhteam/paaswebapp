#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils as t
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = '_template'

    def setUp(self):
        t.set_up(self)
        
    def testdata_setup(self):
        self.username = 'PUT A USERNAME HERE'
        self.coursename = 'PUT A COURSENAME HERE'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        # teacher, uncomment
        #testdata.require(self.args, 'user_is_teacher', self.username, self.coursename)
        # student, uncomment
        #testdata.require(self.args, 'user_is_enrolled', self.username, self.coursename)

    def runTest(self):
        wd = self.wd
        try:
            t.login(wd, self.args.target, self.username)
            # go to course, uncomment
            #t.go_to_course(wd, self.coursename)
            
            ### INSERT STUFF HERE
            
        except Exception, e:
            t.fail_test(self)
            raise
            
    def tearDown(self):
        t.tear_down(self)
