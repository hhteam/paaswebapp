#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'accounts.settings'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'settings@127.0.0.1'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username
        try:
            testutils.login(wd, args.target, username)
            
            if args.j <= 1:
                # settings page opens
                password='FooBar02'  
                time.sleep(1)  
                testutils.wait_for(wd, By.ID, 'bt-settings').click()
                testutils.wait_for(wd, By.ID, "settings-link").click()
                wd.refresh()
                # default timezone should be Helsinki
                testutils.wait_for(wd, By.ID, "saveBtn")
                if wd.find_element_by_id("timezone-input").get_attribute("value") != 'Europe/Helsinki':
                    raise Exception("Default timezone should be Europe/Helsinki, it is "+wd.find_element_by_id("timezone-input").get_attribute("value"))
                  #change password
                testutils.wait_for(wd, By.ID, "changepassword").click()
                testutils.wait_for(wd,By.ID,"cancelpassword")
                  #Without current password
                wd.find_element_by_id("password1").click()
                wd.find_element_by_id("password1").clear()
                wd.find_element_by_id("password1").send_keys("settings123")
                wd.find_element_by_id("password2").click()
                wd.find_element_by_id("password2").clear()
                wd.find_element_by_id("password2").send_keys("settings123")
                wd.find_element_by_id("updatepassword").click()
                if testutils.wait_for(wd,By.ID,"password-error").text != "Current password cannot be empty.":
                    raise Exception("'Current password' field is required")
                  #with invalid current password
                wd.find_element_by_id("origpassword").click()
                wd.find_element_by_id("origpassword").clear()
                wd.find_element_by_id("origpassword").send_keys("foobar") 
                wd.find_element_by_id("updatepassword").click()
                if testutils.wait_for(wd,By.ID,"password-error").text != "Password should be at least 8 characters long and contain at least 1 small case letter, 1 capital letter and a digit.":
                    raise Exception("Password should be at least 8 characters long and contain at least 1 small case letter, 1 capital letter and a digit.")  
                  #Without new password 1
                wd.find_element_by_id("origpassword").click()
                wd.find_element_by_id("origpassword").clear()
                wd.find_element_by_id("origpassword").send_keys(password)
                wd.find_element_by_id("password1").click()
                wd.find_element_by_id("password1").clear()
                wd.find_element_by_id("updatepassword").click()
                if testutils.wait_for(wd,By.ID,"password-error").text != "Password cannot be empty.":
                    raise Exception("'Newt password1' field is required")
                  #Without new password 2
                wd.find_element_by_id("password1").send_keys("settings123")    
                wd.find_element_by_id("password2").click()
                wd.find_element_by_id("password2").clear()
                wd.find_element_by_id("updatepassword").click()
                if testutils.wait_for(wd,By.ID,"password-error").text != "Password cannot be empty.":
                    raise Exception("'Newt password2' field is required")
                   # check with 2 different passwords
                wd.find_element_by_id("password2").click()
                wd.find_element_by_id("password2").clear()
                wd.find_element_by_id("password2").send_keys("settings1") 
                wd.find_element_by_id("updatepassword").click()
                if testutils.wait_for(wd,By.ID,"password-error").text != "New passwords do not match.":
                    raise Exception("New passwords does not match") 
                   ##new passwords contains only 7 characters 
                wd.find_element_by_id("password1").click()
                wd.find_element_by_id("password1").clear()      
                wd.find_element_by_id("password1").send_keys("setting")
                wd.find_element_by_id("password2").click()
                wd.find_element_by_id("password2").clear()      
                wd.find_element_by_id("password2").send_keys("setting")
                wd.find_element_by_id("updatepassword").click()
                if testutils.wait_for(wd,By.ID,"password-error").text!= 'Password should be at least 8 characters long and contain at least 1 small case letter, 1 capital letter and a digit.':
                    raise Exception("Passwords must be at least 8 characters long")
                   #new passwords without digits
                wd.find_element_by_id("password1").click()
                wd.find_element_by_id("password1").clear()      
                wd.find_element_by_id("password1").send_keys("settings")
                wd.find_element_by_id("password2").click()
                wd.find_element_by_id("password2").clear()      
                wd.find_element_by_id("password2").send_keys("settings")
                wd.find_element_by_id("updatepassword").click()
                if testutils.wait_for(wd,By.ID,"password-error").text!= 'Password should be at least 8 characters long and contain at least 1 small case letter, 1 capital letter and a digit.':
                    raise Exception("Passwords must have at least 1 digit(s).")
                  #with all fields
                wd.find_element_by_id("password1").click()
                wd.find_element_by_id("password1").clear()      
                wd.find_element_by_id("password1").send_keys("Settings123")
                wd.find_element_by_id("password2").click() 
                wd.find_element_by_id("password2").click()
                wd.find_element_by_id("password2").clear()
                wd.find_element_by_id("password2").send_keys("Settings123") 
                wd.find_element_by_id("updatepassword").click() 
                time.sleep(0.5)
                if wd.find_element_by_id('password-success').text != 'Your password has been updated successfully.':
                    raise Exception('Password does not change')
                testutils.wait_for(wd, By.ID, "changepassword").click() 
                if  testutils.is_element_visible(wd,By.ID,"changepassword"):
                    raise Exception("Change password cancel is not working") 
                wd.find_element_by_id("cancelpassword").click()
                time.sleep(1)
                if not testutils.is_element_visible(wd,By.ID,"changepassword"):
                    raise Exception("Change password cancel is not working") 
            if args.j <= 2:        
                time.sleep(1)     
              
                        # change timezone
                wd.find_element_by_id("timezone-input").click()
                wd.find_element_by_id("timezone-input").clear()
                wd.find_element_by_id("timezone-input").send_keys("Europe/")
                time.sleep(0.1)
                wd.find_element_by_id("timezone-input").send_keys("Lon")
                wd.find_element_by_css_selector(".tt-suggestion").click()
                    #change email me when and receive updates
                wd.find_element_by_xpath("(//input[@name='event'])[3]").click()
                wd.find_element_by_xpath("(//input[@name='forum'])[2]").click()
                wd.find_element_by_xpath("(//input[@name='grade'])[3]").click()
                wd.find_element_by_xpath("(//input[@name='material'])[2]").click()
                wd.find_element_by_xpath("(//input[@name='section'])[3]").click()
                wd.find_element_by_xpath("(//input[@name='task'])[1]").click()
                wd.find_element_by_xpath("(//input[@name='user_enrolled'])[2]").click()
                wd.find_element_by_xpath("(//input[@name='user_submitted'])[1]").click()
                wd.find_element_by_id("platform_updates").click()  #unchecked
                           #save changes
                wd.find_element_by_id("saveBtn").click()
                time.sleep(1)
                wd.refresh()
                           # go and check
                testutils.wait_for(wd, By.ID, "timezone-input")
                if wd.find_element_by_id("timezone-input").get_attribute("value") != 'Europe/London':
                    raise Exception("New timezone should be Europe/London, it is "+wd.find_element_by_id("timezone-input").get_attribute("value"))
                if wd.find_element_by_xpath("(//input[@name='event'])[3]").get_attribute("checked") != "true":
                    raise Exception("Email me when calender event is changed --selected option is not correct")
                if wd.find_element_by_xpath("(//input[@name='forum'])[2]").get_attribute("checked") != "true":
                    raise Exception("Email me when someone posted in a discussion --selected option is not correct")
                if wd.find_element_by_xpath("(//input[@name='grade'])[3]").get_attribute("checked") != "true":
                    raise Exception("Email me when task is graded --selected option is not correct")   
                if wd.find_element_by_xpath("(//input[@name='material'])[2]").get_attribute("checked") != "true":
                    raise Exception("Email me when new course material is added --selected option is not correct")
                if wd.find_element_by_xpath("(//input[@name='section'])[3]").get_attribute("checked") != "true":
                    raise Exception("Email me when course content is changed --selected option is not correct")
                if wd.find_element_by_xpath("(//input[@name='task'])[1]").get_attribute("checked") != "true":
                   raise Exception("Email me when course task is changed --selected option is not correct")
                if wd.find_element_by_xpath("(//input[@name='user_enrolled'])[2]").get_attribute("checked") != "true":
                   raise Exception("Email me when new student has enrolled --selected option is not correct")
                if wd.find_element_by_xpath("(//input[@name='user_submitted'])[1]").get_attribute("checked") != "true":
                   raise Exception("Email me when student submitted a task --selected option is not correct")   
                if wd.find_element_by_id("platform_updates").is_selected() != False:
                    raise Exception("Receive updates from Eliademy is selected but it should be not selected")
            if args.j <= 3:        
                    #changing language and receive updates
                wd.find_element_by_id("languageselect").click()
                wd.find_element_by_id("languageselect").send_keys("Tamil (ta)")
                #wd.find_element_by_css_selector('option[value="ta"]').click()  
                wd.find_element_by_id("platform_updates").click()  #checked
                wd.find_element_by_id("saveBtn").click()
                time.sleep(2)
                testutils.wait_for(wd,By.ID,'bt-settings').click()
                testutils.wait_for(wd, By.ID, "settings-link").click()
                wd.refresh()
                   #check changes
                language = wd.find_element_by_id("languageselect").get_attribute("value")
                if language != "ta":
                    raise Exception("Language name is not correct")   
                if wd.find_element_by_id("platform_updates").get_attribute("checked") !="true":
                    raise Exception("Receive updates from Eliademy is not selected but it should be selected")    
                    
            if args.j <= 4:
                ##checking Add social accounts buttons are there
                if not wd.find_element_by_css_selector('BUTTON.btn.btn-facebook').is_displayed():
                    raise Exception('Add Facebook account button is not displayed')
                if not wd.find_element_by_css_selector('BUTTON.btn.btn-linkedin').is_displayed():
                    raise Exception('Add linkedin account button is not displayed')
                if not wd.find_element_by_css_selector('BUTTON.btn.btn-twitter').is_displayed():
                    raise Exception('Add twitter account button is not displayed')    
                
        except Exception, e:
            testutils.save_screenshot(self)
            raise
        
    def tearDown(self):
        testutils.tear_down(self)
