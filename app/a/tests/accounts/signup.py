#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time,datetime
from datetime import timedelta,date,datetime
import sys


class CBTestTestCase(unittest.TestCase):
    
    _testname = 'accounts.signup'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'signuptest@127.0.0.1'
        testdata.delete(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', 'test113@127.0.0.1')
        testdata.delete(self.args, 'cohort', 'TestOrgName')
        
    def runTest(self):
        wd = self.wd; args = self.args; username = self.username
        try:
            if args.j <= 1:
                wd.get(args.target)
                print("Signup")
                testutils.wait_for(wd, By.ID, "epdagree").click()
                testutils.wait_for(wd, By.ID, "explicitsubmit").click()
                testutils.wait_for(wd, By.ID, "main-login-button").click()
                
                testutils.wait_for(wd, By.LINK_TEXT, "Sign up").click()
                time.sleep(1)
                testutils.wait_for(wd, By.ID, "firstname")
                print("check we have button and fields")
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'a.btn.btn-facebook') or not testutils.is_element_visible(wd,By.CSS_SELECTOR,'A.btn.btn-linkedin'):
                    raise Exception("Login with Facebook and LinkedIn button should be visible")
                if not testutils.is_element_visible(wd,By.CSS_SELECTOR,'A.btn.btn-github') or not testutils.is_element_visible(wd,By.CSS_SELECTOR,'A.btn.btn-google-plus'):
                    raise Exception("Login with Live and google+ button should be visible")
                print("Testing without Firstname")
                testutils.wait_for(wd, By.ID, "lastname").click()
                testutils.wait_for(wd, By.ID, "lastname").clear()
                testutils.wait_for(wd, By.ID, "lastname").send_keys("Test")
                testutils.wait_for(wd, By.ID, "email").click()
                testutils.wait_for(wd, By.ID, "email").clear()
                testutils.wait_for(wd, By.ID, "email").send_keys(username)
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").clear()
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").send_keys("FooBar02")
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn-success").click()
                time.sleep(1)
                if testutils.wait_for(wd, By.ID, "signup_firstname").find_element_by_tag_name("span").text != "Missing given name":
                    raise Exception("First name is missing")
                print("Testing without Lastname")
                
                testutils.wait_for(wd, By.ID, "firstname").clear()
                testutils.wait_for(wd, By.ID, "firstname").send_keys("Signup")
                testutils.wait_for(wd, By.ID, "lastname").click()
                testutils.wait_for(wd, By.ID, "lastname").clear()
                testutils.wait_for(wd, By.ID, "lastname").send_keys("")
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn-success").click()
                time.sleep(1)
                if testutils.wait_for(wd, By.ID, "signup_lastname").find_element_by_tag_name("span").text != "Missing surname":
                    raise Exception("Last name is missing")
                print("Testing without Email")
                testutils.wait_for(wd, By.ID, "lastname").clear()
                testutils.wait_for(wd, By.ID, "lastname").send_keys("Test")
                testutils.wait_for(wd, By.ID, "email").clear()
                testutils.wait_for(wd, By.ID, "email").send_keys()
                testutils.wait_for(wd, By.CSS_SELECTOR, "input.btn.btn-success").click() 
                time.sleep(1.5)
                if testutils.wait_for(wd, By.ID, "signup_email").find_element_by_tag_name("span").text != "Invalid email address":
                    raise Exception("Email address is missing") 
                print("Existing email address")
                testutils.wait_for(wd, By.ID, "email").clear()
                testutils.wait_for(wd, By.ID, "email").send_keys("aaaa@127.0.0.1")##existing email address
                testutils.wait_for(wd, By.CSS_SELECTOR, "input.btn.btn-success").click() 
                time.sleep(1)
                if testutils.wait_for(wd, By.ID, "signup_email").find_element_by_tag_name("span").text != "This email address is already in use. Please choose another email or login with your existing account.":
                    raise Exception('Email address is already in use') 
                print("Testing without password")
                testutils.wait_for(wd, By.ID, "email").clear()
                testutils.wait_for(wd, By.ID, "email").send_keys(username)
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").clear()
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").send_keys("")
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn-success").click()
                time.sleep(1)
                if testutils.wait_for(wd, By.ID, "signup_password").find_element_by_tag_name("span").text != "Passwords must be at least 8 characters long.Passwords must have at least 1 digit(s).":
                    raise Exception("Passwords must be at least 8 characters long.Passwords must have at least 1 digit(s).")
                print("Testing with all fields")
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").clear()
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").send_keys("FooBar02")
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn-success").click()
                time.sleep(2)
                testutils.wait_for(wd, By.ID, "course-new-link")
                print(wd.find_element_by_id("course-new-link").text)
                print("logout")
                wd.refresh()
                time.sleep(2)
                testutils.logout(wd)

            if args.j <= 2:    
                print("*****Login with new email and password")
                testutils.wait_for(wd,By.ID,'main-login-button').click()
                wd.find_element_by_xpath("//input[@id='username']").click()
                wd.find_element_by_xpath("//input[@id='username']").send_keys(username)
                wd.find_element_by_xpath("//input[@id='password']").click()
                wd.find_element_by_xpath("//input[@id='password']").send_keys("FooBar02")
                wd.find_element_by_xpath("//input[@value='Log in']").click()
                testutils.wait_for(wd,By.ID,'course-new-link')
                if not testutils.is_element_present(wd,By.ID,'course-new-link'):
                    raise Exception("Could not find the video")
                time.sleep(0.5)
                #self.assertEqual("Create New Course", driver.find_element_by_id("course-new-link").text)
                
                testutils.logout(wd)    
        
    def tearDown(self):
        testutils.tear_down(self)
