#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'accounts.login'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'student@127.0.0.1'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)

    def runTest(self):
        wd = self.wd; args = self.args; username = self.username
        try:
            testutils.login(wd, args.target, username)
        except Exception, e:
            testutils.save_screenshot(self)
            raise
            
    def tearDown(self):
        testutils.tear_down(self)
