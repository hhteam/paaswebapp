#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys
import unicodedata as ud

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'accounts.language'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'language@127.0.0.1'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', 'signuplanguge@127.0.0.1')
        
    def runTest(self):
        wd = self.wd; args = self.args; username = self.username
        try:
            if args.j <= 1:
                wd.get(args.target)
                  ##change language and login 
                testutils.wait_for(wd,By.ID,'language-dropdown').click()
                wd.find_element_by_xpath("//ul[@class='dropdown-menu'][2]/li[2]/a").click()
                time.sleep(1)
                if testutils.wait_for(wd,By.ID,'language-dropdown').text != 'Suomi':
                    raise Exception('Language is not changed to Suomi')
                testutils.login(wd,args.target,username)
                testutils.wait_for(wd,By.ID,'course-new-link')
                wd.refresh() 
                time.sleep(7)   
                if wd.find_element_by_id('course-new-link').text.lower() != 'create new course':
                    raise Exception_('Language Should be in English, text: ' +  wd.find_element_by_id('course-new-link').text)
                    #Changing language  in profile settings
                testutils.wait_for(wd, By.XPATH, "//div[@id='mainmenu']/div[3]/a/i").click()
                time.sleep(2)
                testutils.wait_for(wd,By.ID,'settings-link').click()
                time.sleep(2)
                testutils.wait_for(wd,By.ID,'languageselect').click()
                wd.find_element_by_id("languageselect").send_keys("Tamil (ta)")
                #wd.find_element_by_css_selector('option[value="ta"]').click()  
                testutils.wait_for(wd,By.ID,'saveBtn').click()
                time.sleep(3)
                  ##check changes
                ucode = testutils.wait_for(wd,By.ID,'saveBtn').text
                txt = ud.normalize('NFC', ucode)
                if testutils.wait_for(wd,By.ID,'saveBtn').text != txt:
                    raise Exception('It should be in Tamil language but it is in '+testutils.wait_for(wd,By.ID,'saveBtn').text)
                testutils.logout(wd)
                 #check login page is in Suomi or not
                if testutils.wait_for(wd,By.ID,'language-dropdown').text != 'Suomi' and wd.find_element_by_tag_name("html").get_attribute('lang') != 'fi':
                    raise Exception('Language should be in Finnish but it is '+testutils.wait_for(wd,By.ID,'language-dropdown').text )
                testutils.wait_for(wd,By.CSS_SELECTOR,'.image').click()
                testutils.wait_for(wd,By.ID,'language-dropdown').click()
                wd.find_element_by_xpath("//ul[@class='dropdown-menu'][1]/li[1]/a").click()    ##English
              
            if args.j <= 2:      
                ##change language and signup
                testutils.wait_for(wd,By.ID,'language-dropdown').click()
                wd.find_element_by_xpath("//ul[@class='dropdown-menu'][2]/li[2]/a").click() ##Suomi
                time.sleep(1)
                testutils.wait_for(wd,By.CSS_SELECTOR,'.btn-success').click()
                testutils.wait_for(wd,By.CSS_SELECTOR,'.social-signup')
                if wd.find_element_by_tag_name("html").get_attribute('lang') != 'fi':
                    raise Exception('Web page is still in english but but it should be in Finnish')
                testutils.wait_for(wd, By.ID, "firstname").click()
                testutils.wait_for(wd, By.ID, "firstname").send_keys("Signup")
                testutils.wait_for(wd, By.ID, "lastname").click()
                testutils.wait_for(wd, By.ID, "lastname").send_keys("Test")
                testutils.wait_for(wd, By.ID, "email").click()
                testutils.wait_for(wd, By.ID, "email").send_keys('signuplanguge@127.0.0.1')
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").click()
                testutils.wait_for(wd, By.CSS_SELECTOR, "#signup_password input").send_keys("FooBar02")
                testutils.wait_for(wd, By.CSS_SELECTOR, ".btn-success").click()
                time.sleep(2)
                if testutils.wait_for(wd, By.ID, "course-new-link").text != 'Luo uusi kurssi':
                    raise Exception('Web page is still in english but but it should be in Finnish')
               
        except Exception, e:
            testutils.fail_test(self)
            raise
    
    def tearDown(self):
        testutils.tear_down(self)
