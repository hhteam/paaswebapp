#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys


class CBTestTestCase(unittest.TestCase):

    _testname = 'accounts.profile'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'profile@127.0.0.1'
        self.username2 = 'profile2@127.0.0.1'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', self.username2)
        testdata.require(self.args, 'user', self.username2)
        self.coursename = 'Profile Course'  # we need a course so that user2 can see user1
        testdata.delete(self.args, 'course', self.coursename)
        testdata.require(self.args, 'course', self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.username, self.coursename)
        testdata.require(self.args, 'user_is_enrolled', self.username2, self.coursename)

    def runTest(self):
        wd = self.wd;args=self.args;username=self.username 
        profilename = "User 121" 
        try:
            testutils.login(wd,args.target,username)
            testutils.wait_for(wd, By.ID, "bt-settings")
            testutils.wait_for(wd, By.CSS_SELECTOR, "img.userpicture").click()
            testutils.wait_for(wd, By.LINK_TEXT, "My Profile").click()
            wd.refresh()
            testutils.wait_for(wd, By.CSS_SELECTOR, "img.thumbnail")
            ##Add Social account button 
            if not wd.find_element_by_css_selector('.dropdown-toggle.ignored_link').is_displayed():
                raise Exception('Add social account is not displayed in profile page') 
            wd.find_element_by_link_text("Edit").click()
            #check FB,In,Twitter buttons are displayed or not 
            if not wd.find_element_by_css_selector('BUTTON.btn.btn-facebook').is_displayed():
                raise Exception('Add Facebook account button is not displayed')
            if not wd.find_element_by_css_selector('BUTTON.btn.btn-linkedin').is_displayed():
                raise Exception('Add linkedin account button is not displayed')
            if not wd.find_element_by_css_selector('BUTTON.btn.btn-twitter').is_displayed():
                raise Exception('Add twitter account button is not displayed')     
            testutils.wait_for(wd, By.CSS_SELECTOR, ".editable").click()
            testutils.wait_for(wd, By.CSS_SELECTOR, ".editable").clear()
            testutils.wait_for(wd, By.CSS_SELECTOR, ".editable").send_keys(profilename)
            wd.find_elements_by_css_selector('.input-xxlarge.inputfield')[0].click()
            wd.find_elements_by_css_selector('.input-xxlarge.inputfield')[0].clear()
            wd.find_elements_by_css_selector('.input-xxlarge.inputfield')[0].send_keys("CBTec Profile")
            wd.find_elements_by_css_selector('.input-xxlarge.inputfield')[1].click()
            wd.find_elements_by_css_selector('.input-xxlarge.inputfield')[1].clear()
            wd.find_elements_by_css_selector('.input-xxlarge.inputfield')[1].send_keys("CBTec")
            wd.find_elements_by_css_selector('.input-medium')[0].click()
            wd.find_elements_by_css_selector('.input-medium')[0].clear()
            wd.find_elements_by_css_selector('.input-medium')[0].send_keys("Fier")
                #Country
            wd.find_elements_by_css_selector('.input-medium')[1].click()  
            testutils.wait_for(wd, By.CSS_SELECTOR, 'option[value="AR"]').click()
            #wd.find_element_by_xpath("//select/option[11]").click()  
            time.sleep(1)
            # wd.find_elements_by_css_selector('.input-medium')[1].send_keys("Argentina")
              ##Social networks and Bio 
            self.socialNetwork_and_bio()  
                #save changes
            testutils.wait_for(wd, By.ID, "profile-save-btn").click()
            time.sleep(1)
            wd.refresh()
            testutils.wait_for(wd, By.CSS_SELECTOR, "img.thumbnail")
            userurl = wd.current_url + '/' + str(testdata.get(args, 'user', self.username, 'username', 'id'))
            
            ## login as user2 and check that that user can see everything that she should
            #testutils.logout(wd)
            #testutils.login(wd, args.target, self.username2)
            #time.sleep(10)
            #wd.get(userurl)
            #testutils.wait_for(wd, By.CSS_SELECTOR, 'img.thumbnail')
           
            ## SKILLS
            ## Make sure skills can not be seen for other users
            #if testutils.is_element_present(wd, By.ID, "skills-progress") or testutils.is_element_present(wd, By.ID, "skills-progress-empty"):
                #raise Exception("Should not be able to see skills blocks on other persons profile")
            ## log in back as user 1
            #testutils.logout(wd)
            #testutils.login(wd, args.target, self.username)
            #wd.get(userurl)
            #testutils.wait_for(wd, By.CSS_SELECTOR, 'img.thumbnail')
            # No skills achieved should be visible
            if not testutils.is_element_visible(wd, By.ID, "skills-progress-empty"):
                raise Exception("Skills progress empty placeholder text should be visible")
            # Add skills
            testdata.require(self.args, "user_has_skill", self.username, "Profile Test Skill")
            # refresh
            wd.get(userurl)
            testutils.wait_for(wd, By.CSS_SELECTOR, 'img.thumbnail')
            if not testutils.is_element_visible(wd, By.ID, "skills-progress"):
                raise Exception("Skills progress should be visible")
                
        except Exception, e:
            testutils.fail_test(self)
            raise 
            
    def socialNetwork_and_bio(self):
        wd = self.wd; profilename = "User 121" 
        for i in range(0,10):
                 #Facebook link
            wd.find_elements_by_css_selector('.input-xlarge')[i].click()
            if i == 0:
                facebook = wd.find_elements_by_css_selector('.input-xlarge')[0].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[0].send_keys(facebook[:-8]+profilename)
            if i == 1:      
                 #Twitter link
                twitter = wd.find_elements_by_css_selector('.input-xlarge')[1].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[1].send_keys(twitter[:-8]+profilename)
            if i == 2:      
                 #Skype link
                wd.find_elements_by_css_selector('.input-xlarge')[2].click()
                wd.find_elements_by_css_selector('.input-xlarge')[2].send_keys(profilename)
            if i == 3: 
                 #LinkedIn 
                linkedin = wd.find_elements_by_css_selector('.input-xlarge')[3].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[3].send_keys(linkedin[:-8]+profilename)
            if i == 4: 
                #vk
                vk = wd.find_elements_by_css_selector('.input-xlarge')[4].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[4].send_keys(vk[:-8]+profilename)
            if i == 5:  
                #Googleplus
                googleplus = wd.find_elements_by_css_selector('.input-xlarge')[5].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[5].send_keys(googleplus[:-8]+profilename)
            if i == 6:   
                #weibo
                weibo = wd.find_elements_by_css_selector('.input-xlarge')[6].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[6].send_keys(weibo[:-8]+profilename)
            if i == 7:   
                #pinterest
                pinterest = wd.find_elements_by_css_selector('.input-xlarge')[7].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[7].send_keys(pinterest[:-8]+profilename)
            if i == 8:  
                #instagram
                instagram = wd.find_elements_by_css_selector('.input-xlarge')[8].get_attribute('placeholder')
                wd.find_elements_by_css_selector('.input-xlarge')[8].send_keys(instagram[:-8]+profilename)
            if i == 9:
                #other
                wd.find_elements_by_css_selector('.input-xlarge')[9].send_keys("www."+profilename+".com")
            #Bio
        wd.find_elements_by_css_selector('.input-xxlarge')[2].click()    
        wd.find_elements_by_css_selector('.input-xxlarge')[2].clear()
        wd.find_elements_by_css_selector('.input-xxlarge')[2].send_keys("Born in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBornrn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in IndiaBorn in India")  
        testutils.wait_for(wd,By.CSS_SELECTOR,'.tooltip-inner')
        if testutils.wait_for(wd, By.CSS_SELECTOR, ".tooltip-inner").text != "Character limit has been reached":
            raise Exception("Bio Character limit is reached")
        wd.find_elements_by_css_selector('.input-xxlarge')[2].clear()    
        wd.find_elements_by_css_selector('.input-xxlarge')[2].send_keys("Born in India")        
        return        
            
    def check_data(self):
        wd = self.wd; profilename = "User 121" ;username=self.username 
       
        if wd.find_element_by_xpath("//div[@id='settings-page-content']/div[1]/h3[1]").text != profilename:
                 raise Exception("User name is: "+wd.find_element_by_xpath("//div[@id='settings-page-content']/div[1]/h3[1]").text + " - should be: User 121")
        if wd.find_element_by_xpath("//div[@class='row-fluid']/span[2]/div[1]").text  != "CBTec Profile":
            raise Exception("profile title is not correct")
        if wd.find_element_by_xpath("//div[@class='row-fluid']/span[2]/div[2]").text  != "CBTec":
             raise Exception("Organization name is not correct") 
        LocationCountry = wd.find_element_by_xpath("//div[@class='row-fluid']/span[2]/div[3]").text 
        location,country = (LocationCountry.split(','))
        if location != "Fier":
            raise Exception("Location name is not correct") 
        if country.strip() != "Argentina":
            raise Exception("Country name is not correct") 
        if wd.find_element_by_xpath("//div[@class='row-fluid']/span[2]/div[4]/a").text != username:
            raise Exception('Email address is not correct')    
        if wd.find_element_by_xpath("//div[@class='row-fluid']/span[2]/div[5]/a").text != profilename:
            raise Exception("Skype link is not correct")
        if wd.find_element_by_xpath("//div[@class='row-fluid']/span[2]/div[6]/a").text != "www."+profilename+".com":
            raise Exception("other link value is not correct")     
        if not wd.find_element_by_xpath("//div[@class='social-toggles well']/a[1]/i").is_displayed():
            raise Exception('Googleplus icon is not displayed')
        if not wd.find_element_by_xpath("//div[@class='social-toggles well']/a[2]/i").is_displayed():
            raise Exception('VK icon is not displayed') 
        if not wd.find_element_by_xpath("//div[@class='social-toggles well']/a[3]/i").is_displayed():
            raise Exception('weibo icon is not displayed')
        if not wd.find_element_by_xpath("//div[@class='social-toggles well']/a[4]/i").is_displayed():
            raise Exception('pinterest icon is not displayed')
        if not wd.find_element_by_xpath("//div[@class='social-toggles well']/a[5]/i").is_displayed():
            raise Exception('instagram icon is not displayed')            
        if wd.find_element_by_xpath("//div[@id='settings-page-content']/div[1]/div[3]").text  != "Born in India":
            raise Exception("Bio description is not correct ")
       
        return  
              
   
    def tearDown(self):
        testutils.tear_down(self)

   
def main():	
    return 0

if __name__ == '__main__':
    main()
