#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time,datetime
from datetime import timedelta,date,datetime
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'accounts.signup-business'

    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'signuptest@127.0.0.1'
        testdata.delete(self.args, 'user', self.username)
        testdata.delete(self.args, 'user', 'test113@127.0.0.1')
        testdata.delete(self.args, 'cohort', 'TestOrgName')
        
    def runTest(self):
        print("*****Signup for Business") 
        wd = self.wd; args = self.args; username = self.username
        try:
            wd.get(args.target)
            testutils.wait_for(wd, By.ID, "epdagree").click()
            testutils.wait_for(wd, By.ID, "explicitsubmit").click()
            
            print("*****Signup for Business")               
            self.signup()
                
            print("*****Login with new organization email and password")
            self.login_business()
               
        except Exception, e:
            testutils.fail_test(self)
            raise
    
    def signup(self):
        wd = self.wd;
        #print("going to get in to busimess")
        testutils.wait_for(wd, By.LINK_TEXT, "Business").click()
        testutils.wait_for(wd, By.ID, "orgname_input_field").click()
        testutils.wait_for(wd, By.ID, "orgname_input_field").clear()
        testutils.wait_for(wd, By.ID, "orgname_input_field").send_keys("TestOrgName")
        testutils.wait_for(wd, By.XPATH, "//input[@name='email']").click()
        testutils.wait_for(wd, By.XPATH, "//input[@name='email']").clear()
        testutils.wait_for(wd, By.XPATH, "//input[@name='email']").send_keys("test113@127.0.0.1")
        testutils.wait_for(wd, By.XPATH, "//input[@name='password']").click()
        testutils.wait_for(wd, By.XPATH, "//input[@name='password']").clear()
        testutils.wait_for(wd, By.XPATH, "//input[@name='password']").send_keys("testtest1")
        testutils.wait_for(wd, By.XPATH, "//input[@value='Create account']").click()
        print("*************Looking landing page to go away")
        for i in range(120):
            try:
                if "Eliademy for Business" != wd.title: break
            except: pass
            time.sleep(5)
            print(str((i+1)*5) + " seconds")
        else: self.fail("time out")
        print("Gone - looking for logo")
        testutils.wait_for(wd,By.ID, "logo")
        print("Looking for intro video")
        testutils.wait_for(wd,By.LINK_TEXT,"Done").click()
        print("Looking for organization details")
        testutils.wait_for(wd,By.CSS_SELECTOR, "h3.block-title > span")
        
        if not testutils.is_element_present(wd,By.CSS_SELECTOR, "h3.block-title > span"):
            raise Exception("Could see organization detail")
        else:  
            testutils.logout(wd)
        return
        
    def login_business(self):
        wd = self.wd;
        testutils.wait_for(wd,By.ID,'main-login-button').click()
        wd.refresh()
        testutils.wait_for(wd,By.ID,'username').click()
        testutils.wait_for(wd,By.ID,'username').send_keys('test113@127.0.0.1')
        testutils.wait_for(wd,By.ID,'password').click()
        testutils.wait_for(wd,By.ID,'password').send_keys('testtest1')
        wd.find_element_by_xpath("//div[@class='controls submit']/input").click()
        testutils.wait_for(wd,By.ID,'course-new-link')
        if not testutils.is_element_present(wd,By.ID,'course-new-link'):
            raise Exception("Could not login with new business username and password")
        return    
        
    def tearDown(self):
        testutils.tear_down(self)
