#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils
import unittest

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time,datetime
from datetime import timedelta,date,datetime
import sys


class CBTestTestCase(unittest.TestCase):
    
    _testname = 'cohorts.organization_details'
    
    def setUp(self):
        testutils.set_up(self)
        
    def testdata_setup(self):
        self.username = 'test113@127.0.0.1'
        testdata.delete(self.args, 'user', 'test113@127.0.0.1')
        testdata.delete(self.args, 'cohort', 'Luksia')
        #testdata.delete(self.args, 'cohort', self.cohortname)
        #testdata.require(self.args, 'cohort', self.cohortname, self.username, 'cohort.127.0.0.1')
       
       
        
    def runTest(self):
        wd = self.wd; args = self.args; username = self.username; #cohortname = self.cohortname
        try:
                
            if args.j <= 1:
               
                ##Administration details
                #testdata.create_cohort(cohortname,username)
                wd.get(args.target)
                self.signup()
                testutils.wait_for(wd,By.ID,'bt-settings').click()
                #time.sleep(0.5)
                testutils.wait_for(wd,By.ID,'organization-link').click()
                #check Save button: disable and first name ,lastname, access time
                if not testutils.wait_for(wd,By.ID,'save-org-settings-btn').get_attribute('disabled') == 'true':
                    raise Exception('Save button is enabled but it should be disabled')
                if not wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[1]").is_displayed() and not wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[1]").text == 'TestOrgName':
                    raise Exception('Firstname name should be TestOrgName but it is '+wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[1]").text)
                if not wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[2]").is_displayed() and not wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[2]").text == 'Administrator':
                    raise Exception('Firstname name should be Administrator but it is '+wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[2]").text)
                d = date.today()
                access_date ="%s.%s.%s" % (d.day, d.month, d.year)
                current_time = time.strftime("%H:%M")     
                if not wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[3]").is_displayed() and not wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[3]").text == access_date+','+current_time:
                    raise Exception('Last Access time should be'+access_date+','+current_time +'but it is'+wd.find_element_by_xpath("//table[@class='table table-hover']/tbody/tr[2]/td[3]").text)
                testutils.wait_for(wd,By.CSS_SELECTOR,'.inputfield.livevalidation').click()
                wd.find_element_by_css_selector('.inputfield.livevalidation').clear()
                wd.find_element_by_css_selector('.inputfield.livevalidation').send_keys('CloudberyTec')
                wd.find_element_by_xpath("//input[@id='cohort_course_privacy']").click()
                wd.find_element_by_xpath("//input[@id='cohort_course_privacy_ext']").click()
                wd.find_element_by_xpath("//input[@id='cohort_enroll_insecure']").click()
                if testutils.wait_for(wd,By.ID,'save-org-settings-btn').get_attribute('disabled') == 'true':
                    raise Exception('Save button is disabled but it should be enabled')
                testutils.wait_for(wd,By.ID,'save-org-settings-btn').click()
                time.sleep(1)  
                wd.refresh()
            if args.j <= 2:
                ##Check changes
                if not testutils.wait_for(wd,By.CSS_SELECTOR,'.inputfield.livevalidation').get_attribute('value') == 'CloudberyTec':
                    raise Exception('Organization name should be CloudberyTec but it is '+testutils.wait_for(wd,By.CSS_SELECTOR,'.inputfield.livevalidation').text)
                for j in range(1,4):
                    if not wd.find_element_by_xpath("//div[@class='well']/label["+str(j)+"]/input").get_attribute('checked')  == 'true':
                        if j == 1:
                            raise Exception('Allow any instructors to control course privacy is not selected but it should be selected')
                        if j == 2:
                            raise Exception('Allow external instructor to enroll users of this organization is not selected but it should be selected')
                        if j == 3:
                            raise Exception('Allow users to self register and join organization learning space is not selected but it should be selected')
               
            if args.j <= 3:
                ##User directory
                testutils.wait_for(wd,By.ID,'directory-link').click()
                testutils.wait_for(wd,By.ID,'admin-add-users-btn').click()
                testutils.wait_for(wd,By.ID,'invite-emails-input').click()
                testutils.wait_for(wd,By.ID,'invite-emails-input').click()
                testutils.wait_for(wd,By.ID,'invite-emails-input').send_keys('shanthi.am@cloudberrytec.com')
                wd.find_element_by_id('send-invite').click()
                time.sleep(100)   
               
               
        except Exception, e:
            testutils.fail_test(self)
            raise
    
    def signup(self):
        wd = self.wd; username = self.username
        testutils.wait_for(wd, By.LINK_TEXT, "Business").click()
        testutils.wait_for(wd, By.ID, "orgname_input_field").click()
        testutils.wait_for(wd, By.ID, "orgname_input_field").clear()
        testutils.wait_for(wd, By.ID, "orgname_input_field").send_keys("Luksia")
        testutils.wait_for(wd, By.XPATH, "//input[@name='email']").click()
        testutils.wait_for(wd, By.XPATH, "//input[@name='email']").clear()
        testutils.wait_for(wd, By.XPATH, "//input[@name='email']").send_keys(username)
        testutils.wait_for(wd, By.XPATH, "//input[@name='password']").click()
        testutils.wait_for(wd, By.XPATH, "//input[@name='password']").clear()
        testutils.wait_for(wd, By.XPATH, "//input[@name='password']").send_keys("testtest1")
        testutils.wait_for(wd, By.XPATH, "//input[@value='Create account']").click()
        time.sleep(5)
        testutils.wait_for(wd,By.CSS_SELECTOR,'.btn.btn-primary.ignored_link').click()
        testutils.wait_for(wd,By.ID,'details-link')
        time.sleep(1)
        return
        
    def tearDown(self):
        testutils.tear_down(self)
