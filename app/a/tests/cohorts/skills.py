#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


"""

import testdata
import testutils as t
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import traceback
import time
import sys

class CBTestTestCase(unittest.TestCase):
    
    _testname = 'cohorts.skills'

    def setUp(self):
        t.set_up(self)
        
    def testdata_setup(self):
        self.username = 'cohort.skills@cohort.127.0.0.1'
        self.cohortname = 'Cohort Skills Cohort'
        testdata.delete(self.args, 'user', self.username)
        testdata.require(self.args, 'user', self.username)
        testdata.delete(self.args, 'cohort', self.cohortname)
        testdata.require(self.args, 'cohort', self.cohortname, self.username, 'cohort.127.0.0.1')

    def runTest(self):
        wd = self.wd
        try:
            t.login(wd, self.args.target, self.username)
            
            t.wait_for(wd, By.ID, "bt-settings").click()
            t.wait_for(wd, By.ID, "organization-link").click()
            t.wait_for(wd, By.CSS_SELECTOR, ".modal-footer .btn-primary").click()
            t.wait_for(wd, By.ID, "skills-link").click()
            t.wait_for(wd, By.CSS_SELECTOR, ".block-title")
            
            
        except Exception, e:
            t.fail_test(self)
            raise
            
    def tearDown(self):
        t.tear_down(self)
