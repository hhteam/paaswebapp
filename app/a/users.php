<?php
/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

require_once "config.php";

// A hardcoded list of users with special powers.
if (isset($config->SpecialUsers) and is_array($config->SpecialUsers))
{
    echo json_encode($config->SpecialUsers);
}
else
{
    echo json_encode(array());
}
