/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

CKEDITOR.plugins.add("ayoutube", {
    icons: "ayoutube",
    init: function (editor)
    {
        editor.addCommand("insertYoutube", new CKEDITOR.dialogCommand("youtubeDialog",
            { allowedContent: "img[src,data-attachment-youtube,width,height]{float,margin,margin-top,margin-bottom,margin-left,margin-right,width,height}" }));

        editor.ui.addButton("AYoutube", {
            label: "Add YouTube Video",
            command: "insertYoutube",
            toolbar: "insert,0" });

        CKEDITOR.dialog.add("youtubeDialog", this.path + "dialogs/ayoutube.js");
    }});
