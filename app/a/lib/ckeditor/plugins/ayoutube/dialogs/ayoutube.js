/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

CKEDITOR.dialog.add('youtubeDialog', function (editor)
{
    return {
        title: 'Insert YouTube Video',
        minWidth: 400,
        minHeight: 100,
        contents: [{
            id: 'tab-basic',
            label: 'Basic Settings',
            elements: [{
                type: 'text',
                id: 'link',
                label: 'Please copy YouTube URL from address field of your browser and paste it here.',
                validate: CKEDITOR.dialog.validate.regex(/^((http|https):\/\/)?(www\.)?youtu(\.be|be\.com)/i,
                    "Link should look like http://www.youtube.com/watch?123 or http://youtu.be/123")
            }]
        }],

        onOk: function ()
        {
            var url = this.getValueOf("tab-basic", "link"), videoid;

            try {
                videoid = /v\=([a-zA-Z0-9\-_]+)/.exec(url)[1];
            } catch (err) {
                videoid = /youtu\.be\/([a-zA-Z0-9\-_]+)/.exec(url)[1];
            }

            //IE fix - disable confirmleave for preview image loading
            var oldConfirmLeave = _Router.getConfirmLeave();
            _Router.setConfirmLeave(null);
            setTimeout(function ()
            {
                var img = editor.document.createElement("img");

                img.setAttribute("data-attachment-youtube", videoid);
                img.setAttribute("src", "https://img.youtube.com/vi/" + videoid + "/hqdefault.jpg");
                img.$.style.width = "480px";
                img.$.style.height = "360px";

                editor.insertElement(img);
                // return old confirmleave
                _Router.setConfirmLeave(oldConfirmLeave);
            }, 100);
        }
    };
});
