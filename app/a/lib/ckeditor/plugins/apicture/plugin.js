/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

CKEDITOR.plugins.add("apicture", {
    icons: "apicture",
    init: function (editor)
    {
        if (!editor._attachments /*|| !_User.hasFeature("picture")*/)
        {
            return;
        }

        editor.addCommand("insertPicture", { exec: function (editor)
        {
            var owner = { upload_process: function () {} };

            _Uploader.owner(owner).restrictFileType("image/*").exec(
                function (data)
                {

                },
                function (data)
                {

                },
                function (data)
                {
                    var d = { type: "file", visible: false, filename: data.origname, name: data.filename };

                    _.extend(d, _.pick(editor._attachments.data, "sectionid", "courseid", "taskid", "source"));

                    var m = new editor._attachments.model.model(d);

                    editor._attachments.model.add(m);

                    var img = editor.document.createElement("img");
                    img.setAttribute("data-attachment-id", m.cid);
                    img.setAttribute("src", MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + data.origname);

                    editor.insertElement(img);
                    if (editor.widgets && editor.plugins.image2) {
                      widget = editor.widgets.initOn(img,'image');
                      widget.focus();
                    }
                });
        }, allowedContent: "img[src,data-attachment,data-attachment-id,width,height]{float,margin,margin-top,margin-bottom,margin-left,margin-right,width,height}" });

        editor.ui.addButton("APicture", {
            label: "Add Picture",
            command: "insertPicture",
            toolbar: "insert,0" });
    }});
