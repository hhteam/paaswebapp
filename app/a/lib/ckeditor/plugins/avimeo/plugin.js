/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

CKEDITOR.plugins.add("avimeo", {
    icons: "avimeo",
    init: function (editor)
    {
        editor.addCommand("insertVimeo", new CKEDITOR.dialogCommand("vimeoDialog",
            { allowedContent: "img[src,data-attachment-vimeo,width,height]{float,margin,margin-top,margin-bottom,margin-left,margin-right,width,height}" }));

        editor.ui.addButton("AVimeo", {
            label: "Add Vimeo Video",
            command: "insertVimeo",
            toolbar: "insert,0" });

        CKEDITOR.dialog.add("vimeoDialog", this.path + "dialogs/avimeo.js");
    }});
