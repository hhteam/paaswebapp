/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

CKEDITOR.dialog.add('vimeoDialog', function (editor)
{
    return {
        title: 'Insert Vimeo Video',
        minWidth: 400,
        minHeight: 100,
        contents: [{
            id: 'tab-basic',
            label: 'Basic Settings',
            elements: [{
                type: 'text',
                id: 'link',
                label: 'Please copy Vimeo URL from address field of your browser and paste it here.',
                validate: CKEDITOR.dialog.validate.regex(/^((http|https):\/\/)?(www\.)?vimeo\.com/i,
                    "Link should look like http://vimeo.com/123")
            }]
        }],

        onOk: function ()
        {
            var url = this.getValueOf("tab-basic", "link"),
                videoid = /.com\/(\d+)/.exec(url)[1];
            //IE fix - disable confirmleave for preview image loading
            var oldConfirmLeave = _Router.getConfirmLeave();
            _Router.setConfirmLeave(null);

            $.ajax({
                type: 'GET',
                url: 'https://vimeo.com/api/v2/video/' + videoid + '.json',
                dataType: 'jsonp',
                success: function (vdata)
                {
                    var img = editor.document.createElement("img");

                    img.setAttribute("data-attachment-vimeo", videoid);
                    img.setAttribute("src", vdata[0].thumbnail_large);
                    img.$.style.width = "640px";
                    img.$.style.height = "360px";

                    editor.insertElement(img);
                },
                complete: function (data)
                {
                    // return old confirmleave
                    _Router.setConfirmLeave(oldConfirmLeave);
                }
            });
        }
    };
});
