/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

CKEDITOR.plugins.add("asound", {
    icons: "asound",
    init: function (editor)
    {
        if (!editor._attachments || !_User.hasFeature("sound"))
        {
            return;
        }

        editor.addCommand("insertSound", { exec: function (editor)
        {
            var owner = { upload_process: function () {} };

            _Uploader.setFileUploadLimit(536870912).owner(owner).restrictFileType("audio/*").restrictFileExts(['mp3']).exec(
                function (data) { },
                function (data) { },
                function (data)
                {
                    var d = { type: "file", visible: false, filename: data.origname, name: data.filename };

                    _.extend(d, _.pick(editor._attachments.data, "sectionid", "courseid", "taskid", "source"));

                    var m = new editor._attachments.model.model(d);

                    editor._attachments.model.add(m);

                    var el = editor.document.createElement("img");

                    el.setAttribute("data-attachment-id", m.cid);
                    el.setAttribute("data-attachment-type", "audio");
                    el.setAttribute("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAAeCAYAAACWuCNnAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA1hAAANYQE8JGIuAAAAB3RJTUUH3QYaCzIm4bjZZwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAMbUlEQVR42u1da0xb5R9+zjm9UgattEIYsDm6iVtMGzUmzYwxbsp00QTMErMoS1Q+LV4WBHEmzhEydvHjNvbBoFtMzCxMtixzQ1Fj8Bp0mwbYJKIggyKwQe2Vc/t/8P+enB7KoDcm7H2SNy3N6el5L8/z/m7nwMiyDADo6uqST506hcuXL4OCgoLivwCXy4Wqqiq43W4GABhZlnHw4EH5woULdHQoKCj+k6iqqsKOHTsY3aVLl6hYUVBQ/Kdx4sQJuFwumcvKynpnbGxs2XZUlgGGmf2egoJiaWFsbAzMo48+Kqd6IpZlIUkSHVEKCsqNjEKXjpOQCbnVk0MSCPN9xsQxsxhqelGkEQzDQJbl206s4vEtnRxjUz1BbW0tXn75Zdjt9hjhWuxBUjdJkpT32mOIwJKm/S4FRTqsKlmWUVhYiNraWtjt9ttCqOK1eBxLhWfcXXfd9U6yXzaZTNi1axdcLhfKy8uRnZ2NP//8E+FwOMbiypT1QjpOBoFlWYiiqIjSXIOobnNZhdTiokjFBSwrK0NTUxNcLhd8Ph+uXLmiCBk5bsWKFdi3bx9WrVqFn3/+eUmuuXgcnI9jpJ/J9FeX6uTMzMxAFEXk5ORg+/bteOKJJ3DmzBm0t7fD7/eDYZiMuIragVKrudFonHcwJElCJBIBwzCKqJJrpaJFkYpYeTwevPXWWzCbzRBFERzHxV1/RqMRRUVFcLvdEAQB77//vuJKLkWxIhwXRRFmsxmyLCMcDs/iGGnJcEyX6gUzDAOO4yAIAmRZhs1mw44dO7B161a0t7fjzJkzCAaDGYlxaVX86aefxqZNm7By5Uro9Xrl+uJdM8/zuHbtGjo7O3H69GmIoqhMABnQeN+noNBa4up41datW7Fr1y4AQCQSgclkijmWZVmwLItt27bh66+/xosvvojDhw/jueeew5UrV/Ddd98tOdEijeM4PPXUU9i8eTOKiorAMAyGh4fxxRdf4JNPPlE4pg0ZJcKxlLKEZrMZx48fxx133AFBEGImzmg0Avg3FdnW1oZz587NchXT5S/b7XY0NjbinnvuSep8fX192L17N8bHx2MWlXonoKCYD/8vbgTP8woPTCYTjh49ira2NoUfZWVlOHLkCCYnJ/HSSy/Bbrfj2LFj+Pvvv/HCCy9gZmZmycWs8vLy0NjYiPXr18c9/rfffsObb775b2lCChxLSbCysrIUwYpEIopyEhNQlmVlhxkZGYHX68X58+eVCUl2J1G7f3q9HseOHYPT6YQgCMogzHdecowgCDAYDBgaGsLBgweVnUKn00Gn01HRSgDhcBjXr1/H8PDwoidebgWsVivWrFkDSZKwadMmPPnkk4hEIooVIQgCLBYLmpub0draCpvNhpKSEly+fBkbN25EQ0MDLl68iNdffx11dXUoLy/Hnj170NXVlTA3cnNzUVFRAafTCbPZnPG+i6KohIN4nofb7YbNZlM4SNxecqzRaMTAwACqq6sRjUYVz0zrImbUJdQKSDyEQiElY/Lqq6+ioqICXq8XHR0dEAQhaWUnHXzmmWfgdDoxMzMTY7ktRLDIaygUQklJCUpLS+H1epUBp1ZWYjAajXA4HLjvvvtw8eLFZT1uHMdh//79WLt27ay1TpI/JAFE1lN1dTXKy8uxb98+dHZ24vPPP8fmzZtRXFyMU6dO4fHHH4fH40lYsHJzc1FXVweO4yCKIiKRSMatK0EQwPM8OI6D1WqFzWbDzMyMYqyoOciyLILBINasWYPKykp89NFHSYdc0rINqssEtI2IQiAQwD///IOSkhLU1NTg6NGj2LhxY0oCKYoiKioqFBUXBCHm9WZNfQw5X3l5OURRnBWEp21hjed5jIyMYHR0FMXFxctenAsLCxGNRjE1NYXp6WllLanXlpq4p0+fxvj4OHbu3AmWZXHu3DlIkoQHHngAf/zxB2RZxqpVq2Ksk4Vg27ZtMBgMipBmupGNnFjV5Jrn4peaY1u2bIn5LFEPK+MWltaqmZqagiAIKC0txfPPP49vvvkm6d81GAywWq3K4CTrYkqSBEEQUFBQgKysLGWnIBNzO7g36cTU1BTKysowPDy8bPsoyzJ4no8pVSACpXab1OJz9epV9PT04JFHHoHJZMLk5CQYhoHValU2ymTcuXXr1imhDGK1ZPKVcJnjOEiShKysLPA8f1MOSpIEnudx5513Ijs7W7FGyVguqktIFDVe+lY9wSTdabVa0d/fj71796a0YNQLRx30T1awyESoXUHqFiYO9Q683EHWTrxNjQgW4YXH44HH48Hw8DBCoRBcLhcAYHR0FCaTCRzHwe/3J3wNRDwWQ6wYhlG4Loqi0m9BEG7KQUmS4tZoac+9KIJF3LO5AtuiKEKn0yEvLw+BQAAtLS3wer0pZ0MikQimp6fhcDgQCAQWHHCf6/p8Ph+i0ahi9tJsYXLIz8/H1NTUsu4jwzDQ6/VKLEcd9yTQxmgLCgoQCoVw4MABAEBFRQUYhkF3dzfKysoAAP39/cp5FuoW/v7779iwYUNcDmYqfkd+S6fTIRwOIycnB8FgMKbMQw2SgLh27RoCgUDSnEqbYMWraCcmot1uB8Mw6OjowPHjx+Hz+dKyiwNAW1sbdu7cCZ7nodPpZllg8wXdZVlGNBqFwWBAR0cH9Hp9jFiRTAbFwlx0h8MBu92OH3/8cVn3NRqNYnBwEOvXr0dubq7iCvv9fuj1+hgLi6zF9vZ2dHZ2wu/349lnn8X999+P8+fPY2JiAjU1NWAYBt9+++2C1q8aJ0+exNtvvw2DwbBo9y7q9XqIogiDwYDR0VHYbDZEo1Ho9XpFsNR9iEajMBqNuHDhgsItNQ8X1SUkFhb5UfK31WqFxWLBr7/+ipaWFvzyyy8J7x5ziQ0J2nm9Xjz22GNYt24dhoaGEnJHSIB09erVGBoawsDAAO69917o9XoYDIYY8aKYH6FQCJOTk8terIgY1dfXY/Xq1QCA4uJivPLKK+A4DhMTE4r1pRYflmXh9/tx9913o7q6Gn19fTh06BA8Hg8efPBB9PX1KU/8TUSwbty4gYaGBmzfvh1OpxMWiyWjbiEJwxAvZ3x8HH6/H4WFhfjrr79iPB3CsZKSEgwODuLkyZMxxd6JiFVaBEsdcCfBb5PJhIKCAoyNjaG5uRmffvppjAuWjl2AiJ4gCKipqcH+/fuxYcMGBAIBhMPheX+DZVmYzWZkZ2ejt7cXdXV1mJiYUKwq6g5SzIdgMIienh4AQE9PD0ZGRtDQ0ID8/HyMjIzMGXe9evUq9uzZg+7ubpSWlqK+vh48z+Pw4cOzgtuJiNaRI0cWpd/a2+EkSYLdbsehQ4ewdu1a+P1+pUjcbDYjJycHAwMDqK2tVeo1E62/UtzRVG5+1ul0qKysVMoWdDodioqKwHEcvF4vmpqa0NvbGyNW6YgdaHegUCiEs2fP4vr168jNzYXFYoHRaFTSsKQIVJ2a5Xkeg4OD+PDDD3HgwAGEQiEleKmNX1FQ3Gw9kuSMz+fD999/j4cffhj5+fkYGxuDw+FAd3c3ent7YyyVoaEhOJ1ONDc3w2g04t1338UPP/ywJG7LicfBYDCIs2fPIhgMIi8vD1arFRzHwefzobW1FY2Njbhx48YsjiUqXCnfmvPBBx8A+PfJDdnZ2ejq6kJLSwsGBwfT4v4tROFJ4JxkMIxGY0w8Kx4EQUAkElGyK9qaomR3AIrbF2StOxwONDU1YeXKleA4Du+99x4+/vjjGC6QpzW88cYb+PLLL/HZZ58t2XsI43FwxYoVAAC/359WjqXkEjIMA7PZDIvFgv7+frS0tCjxCzI5mQgCqjtIfoekjon1RO7nuql5+X9ri+x6VKwoUgFJPI2Pj+O1117D3r174Xa745YqSJKE6elp7N69e5bLuFQsSy0H1XWL6qp/Us6QDoMgJcGKRCL46aef0NfXh9bWVmXCFuNJi9oBU98xvpCJJyKlrbWiQkWRquVBQiT19fXYsmULvvrqK0Wk5lrLS/HhkVoOkn5os/VabqXCsbQ80/1WDrz2mTzazxYy2Nrn81DBoqBI3D3UvtfyLR0cS0tZAzEJb8UuoRWeuSpmb1ZJqz0HBUW6cDv8E4p4HMwUx1hye0Cqvvt/YdDmcu9u9jl1BSkyidvln1AkwrVk4XK5wFZWVi7rwaMiRUFx6zmYDlRVVYF96KGHmOUoWhQUFMsHVVVVcLvdDEP8zUuXLsknTpxQbg2goKCguNVwuVyKWAHA/wDbTl7wCNoDsQAAAABJRU5ErkJggg==");

                    editor.insertElement(el);

                    if (editor.widgets && editor.plugins.image2)
                    {
                        widget = editor.widgets.initOn(el, "image");
                        widget.focus();
                    }
                });
        }, allowedContent: "img[src,data-attachment,data-attachment-type,data-attachment-id,width,height]{float,margin,margin-top,margin-bottom,margin-left,margin-right,color,vertical-align,width,height}" });

        editor.ui.addButton("ASound", {
            label: "Add Sound",
            command: "insertSound",
            toolbar: "insert,0" });
    }});
