/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

CKEDITOR.dialog.add('slideshareDialog', function (editor)
{
    return {
        title: 'Insert SlideShare',
        minWidth: 400,
        minHeight: 100,
        contents: [{
            id: 'tab-basic',
            label: 'Basic Settings',
            elements: [{
                type: 'text',
                id: 'link',
                label: 'Please copy SlideShare URL from address field of your browser and paste it here.',
                validate: CKEDITOR.dialog.validate.regex(/^((http|https):\/\/)?(www\.)?slideshare\.net/i,
                    "Link should look like http://www.slideshare.net/title")
            }]
        }],

        onOk: function ()
        {
            var url = this.getValueOf("tab-basic", "link");

            //IE fix - disable confirmleave for preview image loading
            var oldConfirmLeave = _Router.getConfirmLeave();
            _Router.setConfirmLeave(null);
            
            $.ajax({ url: RootDir + "slideshare.php?thumbnail=" + url, 
                success: function (data)
                {
                    var img = editor.document.createElement("img");

                    img.setAttribute("src", data);
                    img.setAttribute("data-attachment-slideshare", url);
                    img.$.style.width = "427px";
                    img.$.style.height = "356px";

                    editor.insertElement(img);
                },
                complete: function (data)
                {
                    // return old confirmleave
                    _Router.setConfirmLeave(oldConfirmLeave);
                }
            });
        }
    };
});
