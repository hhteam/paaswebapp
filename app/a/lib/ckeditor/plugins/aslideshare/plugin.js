/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

CKEDITOR.plugins.add("aslideshare", {
    icons: "aslideshare",
    init: function (editor)
    {
        editor.addCommand("insertSlideshare", new CKEDITOR.dialogCommand("slideshareDialog",
            { allowedContent: "img[src,data-attachment-slideshare,width,height]{float,margin,margin-top,margin-bottom,margin-left,margin-right,width,height}" }));

        editor.ui.addButton("ASlideshare", {
            label: "Add SlideShare",
            command: "insertSlideshare",
            toolbar: "insert,0" });

        CKEDITOR.dialog.add("slideshareDialog", this.path + "dialogs/aslideshare.js");
    }});
