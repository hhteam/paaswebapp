/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

CKEDITOR.plugins.add("avideo", {
    icons: "avideo",
    init: function (editor)
    {
        if (!editor._attachments || !_User.hasFeature("video"))
        {
            return;
        }

        editor.addCommand("insertVideo", { exec: function (editor)
        {
            var progressEl = document.createElement("div"),
                progress = document.createElement("div");

            progressEl.setAttribute("class", "progress");
            progressEl.appendChild(progress);
            progressEl.style.position = "fixed";
            progressEl.style.width = "500px";
            progressEl.style.height = "30px";
            progressEl.style.top = "50%";
            progressEl.style.left = "50%";
            progressEl.style.marginLeft = "-250px";
            progressEl.style.marginTop = "-15px";
            progressEl.style.zIndex = 1000;

            progress.setAttribute("class", "bar");
            progress.style.width = "0px";

            var owner = { upload_process: function (e, data)
            {
                progress.style.width = Math.round(data.loaded / data.total * 100) + "%";
            } };

            _Uploader.setFileUploadLimit(536870912).owner(owner).restrictFileType("video/*").restrictFileExts(['mp4','webm','ogg']).exec(
                function (data) { document.body.appendChild(progressEl); },
                function (data) { document.body.removeChild(progressEl); },
                function (data)
                {
                    document.body.removeChild(progressEl);

                    var d = { type: "file", visible: false, filename: data.origname, name: data.filename };

                    _.extend(d, _.pick(editor._attachments.data, "sectionid", "courseid", "taskid", "source"));

                    var m = new editor._attachments.model.model(d);

                    editor._attachments.model.add(m);

                    var video = document.createElement("video");

                    video.addEventListener("loadeddata", function ()
                    {
                        var a = video.videoWidth > 200 ? 200 / video.videoWidth : 1;
                        var canvas = document.createElement("canvas");
                        canvas.width = video.videoWidth * a;
                        canvas.height = video.videoHeight * a;

                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

                        // Inserting image instead of video element to
                        // enable manipulations.

                        var el = editor.document.createElement("img");

                        el.setAttribute("data-attachment-id", m.cid);
                        el.setAttribute("data-attachment-type", "video");
                        el.setAttribute("width", video.videoWidth);
                        el.setAttribute("height", video.videoHeight);
                        el.setAttribute("src", canvas.toDataURL("image/jpg"));

                        editor.insertElement(el);

                        if (editor.widgets && editor.plugins.image2)
                        {
                            widget = editor.widgets.initOn(el, "image");
                            widget.focus();
                        }
                    }, false);

                    video.addEventListener("loadedmetadata", function ()
                    {
                        if (video.duration > 5)
                        {
                            video.currentTime += 5;
                        }
                    }, false);

                    video.setAttribute("src", MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + data.origname);
                    video.setAttribute("preload", "metadata");
                });
        }, allowedContent: "img[src,data-attachment,data-attachment-type,data-attachment-id,width,height,preload]{float,margin,margin-top,margin-bottom,margin-left,margin-right,color,vertical-align,width,height}" });

        editor.ui.addButton("AVideo", {
            label: "Add Video",
            command: "insertVideo",
            toolbar: "insert,0" });
    }});
