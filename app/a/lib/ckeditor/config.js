/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */


CKEDITOR.editorConfig = function( config ) {
    config.toolbarGroups = [
        { name: 'styles' },
        { name: 'clipboard', groups: [ 'undo', 'clipboard' ] },
        { name: 'insert' },
        { name: 'editing',     groups: [ 'spellchecker' ] },
        '/',
        /*{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },*/
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list'/*, 'indent'*/, 'blocks', 'align'/*, 'spellchecker'*/ ] },
        { name: 'links' },
        { name: 'colors' },
        { name: 'document', groups: [ 'mode' ] },
        '/',
        { name: 'tools' }
    ];

    config.tabSpaces = 4;
    config.enableTabKeyTools = true;
    config.oembed_WrapperClass = "embeddedContentCustom";
    config.oembed_maxWidth = '100%';
    config.oembed_maxHeight = '400';
    config.extraPlugins = "apicture,asound,avideo,image2,codesnippet,sourcearea,sourcedialog,autoembed,autolink,embed,mathjax,html5audio";
    config.removeButtons = 'NewPage,Iframe,Flash,Styles,ShowBlocks,PageBreak,CreateDiv,Smiley,HorizontalRule,SpecialChar,Image,asound,avideo';
    config.mathJaxLib = '//cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML';
    config.removePlugins = "magicline";
    config.format_tags = 'p;h1;h2;h3;pre';
    config.removeDialogTabs = 'link:advanced;table:advanced';
    config.disableNativeSpellChecker = false;
    config.extraAllowedContent = { "*": { classes: "placeholder,ck-align-left,ck-align-center,ck-align-right", attributes: "data-placeholder,data-attachment,data-attachment-id,data-attachment-youtube,data-attachment-vimeo, data-entry-id, data-attachment-type" },"figure": { classes: '!caption', styles: "float,display"}, "figcaption": true, "iframe": {"attributes": "*"} };
    config.fontSize_sizes = '10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;30/30px;32/32px;';
    config.font_defaultLabel = 'Helvetica';
    config.image2_captionedClass='caption';
    config.image2_alignClasses = [ 'ck-align-left', 'ck-align-center', 'ck-align-right' ];
    config.fontSize_defaultLabel = '14';
    config.font_names="Helvetica/Helvetica Neue,Helvetica,sans-serif;Arial/Arial,sans-serif;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif";
    config.embed_provider = '/oembed?url={url}&callback={callback}';
    config.codeSnippet_languages = {
        bash: 'Bash',
        cpp: 'C++',
        cs: 'C#',
        css: 'CSS',
        diff: 'Diff',
        html: 'HTML',
        http: 'HTTP',
        ini: 'INI',
        java: 'Java',
        javascript: 'JavaScript',
        json: 'JSON',
        makefile: 'Makefile',
        markdown: 'Markdown',
        objectivec: 'Objective-C',
        pascal: 'Pascal',
        perl: 'Perl',
        php: 'PHP',
        python: 'Python',
        r: 'R',
        ruby: 'Ruby',
        sql: 'SQL',
        vbscript: 'VBScript',
        xhtml: 'XHTML',
        xml: 'XML'
    };
};

/*
CKEDITOR.on("dialogDefinition", function (ev)
{
    if (ev.data.name == 'link')
    {
        ev.data.definition.getContents('target').get('linkTargetType').default = "_blank";
    }
});
*/
