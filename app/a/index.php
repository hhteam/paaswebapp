<?php
/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

require_once 'config.php';

$version = 2017041900;

$path = explode("/", substr($_SERVER["REQUEST_URI"], strlen($config->RootDir)));
$userId = 0;
$courseInfo = array();
$certInfo = array();
$courseIsPublic = false;
$userIsEnrolledToCourse = false;
$inviteEmail = null;
$inviteNewUser = null;
$allDataValid = false;

try
{
    $db = new PDO("mysql:dbname={$config->db_name};host={$config->db_host}", $config->db_user, $config->db_password);

    // Are we viewing a course?
    if (count($path) > 1 && $path[0] == "courses" && $path[1] != "reuse" && $path[1] != "wizard" && $path[1] != "import")
    {
        if (count($path) > 3 && $path[2] == "certificate") {
            // It's a certificate!
            $q = $db->prepare("SELECT userid, hashurl FROM {$config->db_prefix}monorail_certificate WHERE hashurl=?");

            $q->bindParam(1, $path[3]);

            if ($q->execute()) {
                $certInfo = $q->fetch(PDO::FETCH_ASSOC);
            }
        }

        $q = $db->prepare("SELECT c.id AS id, c.fullname AS fullname, cs.summary AS summary, mcd.code AS code, mcd.courseid AS cid " .
            "FROM {$config->db_prefix}monorail_course_data AS mcd " .
            "INNER JOIN {$config->db_prefix}course AS c ON c.id=mcd.courseid " .
            "LEFT JOIN {$config->db_prefix}course_sections AS cs ON cs.course=mcd.courseid AND cs.section=0 " .
                "WHERE mcd.code=?");

        $q->bindParam(1, $path[1]);

        if ($q->execute())
        {
            $courseInfo = $q->fetch(PDO::FETCH_ASSOC);

            $q = $db->prepare("SELECT id FROM {$config->db_prefix}monorail_course_perms WHERE course=? AND caccess=3 AND privacy=1");
            $q->bindParam(1, $courseInfo["cid"]);

            if ($q->execute()) {
                if ($q->fetch() !== FALSE) {
                    $courseIsPublic = true;
                }
            }
        }
    }
    // It's an invite link with email?
    else if (count($path) > 2 && $path[0] == "invitation")
    {
        $inviteEmail = $path[2];

        $q = $db->prepare("SELECT id FROM {$config->db_prefix}user WHERE email=?");
        $q->bindParam(1, $inviteEmail);

        if ($q->execute()) {
            if ($q->fetch() === FALSE) {
                $inviteNewUser = true;
            } else {
                $inviteNewUser = false;
            }
        }
    }

    // Is this a logged-in user?
    if (@$_COOKIE["MoodleSession"])
    {
        $q = $db->prepare("SELECT userid FROM {$config->db_prefix}sessions WHERE sid=?");
        $q->bindParam(1, $_COOKIE["MoodleSession"]);

        if ($q->execute())
        {
            $userId = (int) $q->fetchColumn();
        }
    }

    // Is this user enrolled to the course?
    if ($userId)
    {
        if (!empty($courseInfo))
        {
            $q = $db->prepare("SELECT ra.roleid AS role FROM {$config->db_prefix}role_assignments AS ra" .
                " INNER JOIN {$config->db_prefix}context AS ctx ON ctx.id = ra.contextid" .
                " INNER JOIN {$config->db_prefix}course AS c ON c.id=ctx.instanceid" .
                    " WHERE ra.userid=? AND c.id=?");

            $q->bindParam(1, $userId);
            $q->bindParam(2, $courseInfo["id"]);

            if ($q->execute())
            {
                if ($q->fetchColumn())
                {
                    $userIsEnrolledToCourse = true;
                }
            }
        }
    }

    $allDataValid = true;
}
catch (Exception $ex)
{
    error_log("UNABLE TO ACCESS MOODLE DB: " . $ex);
}

if ($allDataValid)
{
    if (!$userId)
    {
        // If user not logged in.

        if (!empty($certInfo))
        {
            // Viewing public certificate page.
            header("Location: /cert/" . $certInfo["hashurl"] . ".html");
            exit(0);
        }
        else if ($courseIsPublic)
        {
            // Public course requested - show tags-page and
            // redirect with javascript... to catalog 
            include __DIR__ . "/inc/course_page.php";
        }
        else if ($inviteEmail)
        {
            // Have invite email.
            setcookie("loginRedirect", $_SERVER["REQUEST_URI"], 0, "/");

            if ($inviteNewUser) {
                header("Location: {$config->SignupUrl}?email=" . $inviteEmail, true, 303);
            } else {
                header("Location: {$config->LoginUrl}?username=" . $inviteEmail, true, 303);
            }
        }
        else
        {
            // Or else, redirect normally...
            setcookie("loginRedirect", $_SERVER["REQUEST_URI"], 0, "/");
            header("Location: {$config->LoginUrl}", true, 303);
        }

        exit(0);
    }
    else
    {
        if (!empty($certInfo) && $certInfo["userid"] != $userId)
        {
            // User is trying to view certificare of someone else.

            header("Location: /cert/" . $certInfo["hashurl"] . ".html");
            exit(0);
        }

        if (!empty($courseInfo) && !$userIsEnrolledToCourse)
        {
            // User is logged in, but is trying to view a course it doesn't belong to.
            // Redirecting to magneto page...

            header("Location: " . $config->CatalogUrl . "/catalog/product/view/sku/" . $courseInfo["code"], true, 303);
            exit(0);
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en" id="html-element">
<head>
 <meta charset="UTF-8">
 <meta name="viewport" content="width=320,initial-scale=1.0,maximum-scale=1">
 <style>
@-webkit-viewport{width:320;}
@-moz-viewport{width:320;}
@-ms-viewport{width:320;}
@-o-viewport{width:320;}
@viewport{width:320;}
 </style>
<?php /*
<script>
window.onerror = function (err, url, lnum)
{
    try
    {
        jQuery.post(MoodleDir + "theme/monorail/ext/ajax_error_report.php", { "ERROR": err, "URL": url, "LINE": lnum });
    }
    catch (err)
    { }
}
</script>
*/ ?>
 <script>
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
        document.createTextNode(
            "@-ms-viewport{width:768px!important}"
        )
    );
    document.getElementsByTagName("head")[0].
        appendChild(msViewportStyle);
}
 </script>
 <title></title>

 <link rel="shortcut icon" type="image/x-icon" href="<?php echo $CFG->landing_page; ?>/img/favicon/favicon.ico" />
 <link rel="apple-touch-icon" href="<?php echo $CFG->landing_page; ?>/img/header_logo.png/favicon/favicon-apple.png" />
 <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese">
 <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Tangerine|Cinzel">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/colorbox.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/bootstrap.min.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/bootstrap-responsive.min.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/bootstrap-datetimepicker.min.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/typeahead.js-bootstrap.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/datepicker.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/jquery.timepicker.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/ckeditor/ckeditor.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/ckeditor/plugins/codesnippet/lib/highlight/styles/default.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/social-buttons.min.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/bootstrap-progressbar-2.3.1.min.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/magnific-popup.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>lib/css/bootstrap2-toggle.min.css?v=<?php echo $version?>"/>
 <link rel="stylesheet" href="<?php echo $config->EliademyUrl;?>/css/catalog.css?v=<?php echo $version?>"/>
<?php if (@$config->Production): ?>
 <link rel="stylesheet" href="<?php echo $config->RootDir;?>app/css/main.css?v=<?php echo $version?>"/>
<?php else: ?>
 <link rel="stylesheet/less" type="text/css" href="<?php echo $config->RootDir;?>app/css/main.less?v=<?php echo $version?>"/>
<?php endif ?>
 <script>
  var RootDir = "<?php echo $config->RootDir;?>";
  var MoodleDir = "<?php echo $config->MoodleDir;?>";
  var EliademyUrl = "<?php echo $config->EliademyUrl;?>";
  var ExitUrl = "<?php echo $config->ExitUrl;?>";
  var LoginUrl = "<?php echo $config->LoginUrl; ?>";
  var CatalogUrl = "<?php echo $config->CatalogUrl; ?>";
  var GApiKey = "<?php echo $config->GApiKey; ?>";
  var Version = "<?php echo $version ?>";
  var FileUploadLimit = <?php echo $config->FileUploadLimit; ?>;
  var RelayHost = "<?php echo $config->RelayHost ?>";
  var vidUrl = "<?php echo $config->vidUrl ?>";
  var vidPartnerId = "<?php echo $config->vidPartnerId ?>";
  var vidPlayerId = "<?php echo $config->vidPlayerId ?>";
  var FBAppID = "<?php echo $config->FBAppID; ?>";
  var LINAppID = "<?php echo $config->LINAppID; ?>";
  var PayPalID = "<?php echo $config->PayPalID; ?>";
  var videoUploadEnabled = "<?php echo $config->videoUploadEnabled; ?>";
  var _Production = <?php echo (@$config->Production) ? "true" : "false" ?>;
  var _Config = {
    cert_price: <?php echo @$config->cert_price ?>,
    cert_teacher_cut: <?php echo @$config->cert_teacher_cut; ?>
  };
<?php if (!@$config->Production): ?>
  var less = { env: "development" };
<?php endif ?>
  var _User = undefined, _Router = undefined, _Uploader = undefined;
  var _UserId = <?php echo $userId; ?>;
 </script>
 <script src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>
 <script src="//connect.facebook.net/en_US/all"></script>
 <script src="//apis.google.com/js/client.js"></script>
 <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-527241bd3919e52b"></script>
 <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
 <script src="https://js.braintreegateway.com/v1/braintree-data.js"></script>
<script type="text/javascript">if (window.location.hash == '#_=_')window.location.hash = '';</script>
<?php if (@$config->Production): ?>
 <script data-main="<?php echo $config->RootDir;?>app/js/main-built.js?v=<?php echo $version; ?>" src="<?php echo $config->RootDir;?>lib/js/require.js?v=<?php echo $version; ?>"></script>
<?php else: ?>
 <script src="<?php echo $config->RootDir ?>lib/js/less-1.3.3.min.js"></script>
 <script src="<?php echo $config->RelayHost ?>/socket.io/socket.io.js"></script>
 <script data-main="<?php echo $config->RootDir;?>app/js/main.js?v=<?php echo $version; ?>" src="<?php echo $config->RootDir;?>lib/js/require.js?v=<?php echo $version; ?>"></script>
<?php endif?>
</head>
<body>
 <div id="progress-indicator" style="display: block;"></div>
</body>
</html>
