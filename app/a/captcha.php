<?php
require_once(dirname(dirname(__FILE__)) . '/config.php');
global $CFG;
global $_SERVER;
$challenge_key = $CFG->solvemedia_challenge_key;
$solvemedia_uri = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? "https://api-secure.solvemedia.com" : "http://api.solvemedia.com";
?>
<html>
    <body style="margin:0;">
        <div>
            <script type='text/javascript'  
                    src='<?php echo($solvemedia_uri); ?>/papi/challenge.script?k=<?php echo($challenge_key); ?> ' >
            </script>
        </div>
    </body>
</html>
