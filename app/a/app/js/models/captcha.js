/**
 * Eliademy.com
 * 
 * @copyright CBTec Oy
 * @license All rights reserved
 */

define(["app/tools/servicecalls", "backbone"], function (SC) {
    return Backbone.Model.extend({
        validate: function () {
            var success = false;
            $.ajaxSetup({
                async: false
            });
            SC.call("local_monorailservices_verify_captcha", {
                id: this.get('id'),
                challenge: this.get('challenge'),
                response: this.get('response')
            }, function (data) {
                success = (data.success == 1);
            }, this, {
                errorHandler: function (data) {
                }
            });
            $.ajaxSetup({
                async: true
            });
            return success;
        }
    });
});
