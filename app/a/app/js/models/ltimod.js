/**
 * Eliademy.com
 * 
 * @copyright CBTec Oy
 * @license All rights reserved
 */

define([ "app/tools/servicecalls", "backbone" ], function(SC) {
  return Backbone.Model.extend({

    sync : function(method, model, options) {
      var thisThis = this;
      var data = this.toJSON()
      if (method == "create") {
        SC.call("local_monorailservices_add_ltimod", {
          ltimod : {
            sectionid : data.sectionid,
            course : data.course,
            name : data.name,
            typeid : data.typeid,
          }
        }, function(data) {
          model.set(data.ltitool[0], {
            silent : true
          });
          if ("warnings" in data && data.warnings.length > 0) {
            options.error(method, model, options);
            return;
          }
          options.success(method, model, options);
        }, this);
      } else if (method == "delete") {
        SC.call("local_monorailservices_remove_ltimod", {
          moduleid : data.cmid
        }, function(data) {
          options.success(method, model, options);
        }, this);
      } else if (method == "update") {
        SC.call("local_monorailservices_update_ltimod", {
          ltimods : [ {
            moduleid : data.cmid,
            name : data.name,
          } ]
        }, function(data) {
          model.set({
            name : data.name
          }, {
            silent : true
          });
          options.success(method, model, options);
        }, this);
      }
      return this;
    },

    defaults : function() {
      return {
        id : undefined,
        name : null,
        cmid : undefined,
        typeid : null,
        launchurl : null,
        iconurl : null,
      };
    }
  });
});
