/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create":
                    SC.call("local_monorailservices_add_cert_order", { "courseid": model.get("courseid"), "status": model.get("status"), "address": model.get("address") }, function (ret)
                    {
                        model.set({ "id": ret.id });

                        options.success(method, model, options);
                    }, this);
                    break;

                default:
                    console.warn("cert order unhandled method: " + method);
                    break;
            }
        }
    });
});
