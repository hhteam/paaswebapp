/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["i18n!nls/strings", "app/tools/servicecalls", "app/tools/utils", "backbone"],
    function (str, SC, Utils)
{

    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var eventData = model.toJSON();
            eventData.timeavailable = Utils.dateToString(new Date(eventData.timeavailable * 1000));
            eventData.timedue = Utils.dateToString(new Date(eventData.timedue * 1000));

            switch (method)
            {
                case "create":
                    var eventData = _.pick(model.toJSON(), "name", "welcome", "timeavailable", "timedue", "description", "course");
                    eventData.timeavailable = Utils.dateToString(new Date(eventData.timeavailable * 1000));
                    eventData.timedue = Utils.dateToString(new Date(eventData.timedue * 1000));
                    SC.call("local_monorailservices_create_bbb_events", { events: [ eventData ] }, function (data)
                    {
                        options.success(method, model, options);
                    }, this, { errorHandler: function (err)
                    {
                        options.error(method, model, _.extend(options, { err: err }));
                    }});
                    break;

                case "update":
                    SC.call("local_monorailservices_update_bbb_events", { events: [ eventData ] }, function (data)
                    {
                        options.success(method, model, options);
                    }, this, { errorHandler: function (err)
                    {
                        options.error(method, model, options);
                    }});
                    break;

                case "delete":
                    SC.call("local_monorailservices_delete_bbb_events", { eventid: model.get("id") }, function (data)
                    {
                        options.success(method, model, options);
                    }, this, { errorHandler: function (err)
                    {
                        options.error(method, model, options);
                    }});

                    break;

                default:
                    console.warn("Unhandled method in course model sync: " + method);
            }
        },

      defaults: function ()
      {
        return {
            id: null,
            name: str.new_live_session,
            description: "",
            welcome: "",
            timedue: "",
            timeavailable: ""
        };
      }
  });
});
