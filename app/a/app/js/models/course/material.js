/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["i18n!nls/strings", "app/tools/servicecalls", "app/collections/course/sections", "app/collections/task/coursetasks"],
    function (str, SC, SectionCollection, TasksCollection)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_material", { courseid: (parseInt(model.get("id")) || 0), code: (model.get("code") || "") }, function (data)
                    {
                        var c = data.course;
                        c.course_country = c.course_country ?
                            _.object( c.course_country.split(','), [] ) : {};
                        // TODO: move the rest from router::reuseCourse()

                        model.set(c);

                        var sect = new SectionCollection(),
                            task = new TasksCollection();

                        sect.setCourseId(c.id);
                        task.setCourseId(c.id);

                        model.set("sections", sect);
                        model.set("tasks", task);

                        options.success(method, model, options);
                    }, this);
                    break;

                default:
                    console.warn("Unhandled method " + method + " in course material model.");
            }
        },

        validate: function (attrs, options)
        {
            if ("fullname" in attrs)
            {
                if (attrs.fullname == "")
                {
                    return str.fill_missing_fields;
                }

                if (attrs.fullname.length > 120)
                {
                    return str.course_fname_limit;
                }
            }

            if ("category" in attrs || ("fullname" in attrs && "code" in attrs)) {
                if (!(parseInt(attrs.category) || 0)) {
                    return str.msg_select_course_category;
                }
            }

            if ("enddate" in attrs && attrs.enddate !== null && attrs.enddate.length)
            {
                if ("startdate" in attrs)
                {
                    var start = attrs.startdate;
                } else
                {
                    var start = this.model.get("startdate");
                }
                if (attrs.enddate < start)
                {
                    return str.course_end_before_start;
                }
            }
        }
    });
});
