/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "backbone"], function (SC)
{
	return Backbone.Model.extend(
	{
        sync: function (method, model, options)
        {
            switch (method) {
                case "patch":
                    if ("role" in options.attrs)
                    {
                        var data = {
                            id: model.get("courseid"),
                            userid: model.get("id"),
                            rolename: ""
                        };

                        var fun = "";

                        switch (options.attrs.role) {
                            case "student":
                                fun = "local_monorailservices_unassign_roles";
                                data.rolename = (model.get("manager") && !model.get("teacher")) ? "manager" : "editingteacher";
                                break;

                            case "teacher":
                                fun = "local_monorailservices_assign_roles";
                                data.rolename = "editingteacher";
                                break;

                            case "manager":
                                fun = "local_monorailservices_assign_roles";
                                data.rolename = "manager";
                                break;
                        }

                        SC.call(fun, { assignments: [ data ] }, function (data)
                        {
                            options.success(method, model, options);
                        });
                    }
                    else
                    {
                        console.warn("Unhandled PATCH in course participant model.");
                    }
                    break;

                case "delete":
                    SC.call("local_monorailservices_unenrol_users", { enrolments: [ { courseid: model.get("courseid"), userid: model.get("id") } ] }, function (data)
                    {
                        options.success(method, model, options);
                    });
                    break;

                default:
                    console.warn("Unhandled method in course participant model " + method);
            }
        },

        picurl: function(size) {
            switch (size) {
                case 'x':
                    var s = '512';
                    break;
                case 'l':
                    var s = '185';
                    break;
                case 'm':
                    var s = '90';
                    break;
                case 's':
                    var s = '32';
                    break;
            }
            return MoodleDir+'public_images/user_picture/'+this.get("profilepichash")+'_'+s+'.jpg';
        },

		defaults: function ()
        {
            return {
                id: null,
                courseid: null,
                fullname: null,
                title: null,
                profilepichash: "default",
                url: null,
                manager: false,
                teacher: false
            };
		},

        toJSON: function () {
            var json = _.clone(this.attributes);
            json.picurl = {
                x: this.picurl('x'),
                l: this.picurl('l'),
                m: this.picurl('m'),
                s: this.picurl('s'),
            }
            return json;
        },
	});
});
