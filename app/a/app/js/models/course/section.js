/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/collections/attachments", "app/tools/servicecalls", "i18n!nls/strings", "app/tools/utils"],
    function (AttachmentCollection, SC, str, Utils)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var data = _.extend({ "course": model.get("courseid") }, _.pick(model.toJSON(), "id", "name", "summary", "visible"));

            if (model.isNew() && model.get("section") !== 0) {
                data['isnew'] = 1;
                data['visible'] = 0;
            }

            if (model.has("section"))
            {
                data.section = model.get("section");
            }

            switch (method)
            {
                case "create":
                    data.id = 0;

                case "update":
                    SC.call("local_monorailservices_upd_course_sect", { sections: [ data ] }, function (data)
                    {
                        if (model.isNew())
                        {
                            model.set({ id: data.section[0].sectionid });
                            model.trigger("newSectionCreated");
                        }

                        options.success(method, model, options);

                        model.trigger('save-skills');
                    }, this, { deferred: options.deferred });
                    break;

                case "delete":
                    SC.call("local_monorailservices_del_course_sect", { sections: [ { id: model.get("id") } ] }, function (data)
                    {
                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });
                    break;
            }
        },

        validate: function (attrs, options)
        {
            if ("name" in attrs)
            {
                if (attrs.name.length > 100)
                {
                    return str.error_limit;
                }

                if (Utils.isEmpty(attrs.name))
                {
                    return str.fill_missing_fields;
                }
            }
        },

        defaults: function ()
        {
            return {
                courseid: 0,
                summary: "",
                visible: 1,
                section: null,
                valuechange: false,
                attachments: new AttachmentCollection()
            };
        },

        name: "section"
    });
});
