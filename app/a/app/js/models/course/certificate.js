/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create": case "update":
                    SC.call("local_monorailservices_update_cert", { "cert": this.toJSON() }, function (ret)
                    {
                        this.set(ret);

                        options.success(method, model, options);
                    }, this);
                    break;

                default:
                    console.warn("CERTIFICATE unhandled method: " + method);
                    break;
            }
        },

        defaults: {
            courseid: undefined,
            userid: undefined,
            state: undefined,
            hashurl: undefined
        }
    });
});
