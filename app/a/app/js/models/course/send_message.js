/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"],
    function (SC)
{
	return Backbone.Model.extend(
	{

        mCourseId: null,
        mUserIds: null,
        mSummary : '',

        setCourseId: function (courseid)
        {
            this.mCourseId = courseid;
            return this;
        },
        
        setUserIds: function (userids)
        {
            this.mUserIds = userids;
            return this;
        },

        setSummary: function (summary)
        {
            this.mSummary = summary;
            return this;
        },
        
        sync: function (method, model, options)
        {
            var that = this;
    
            if (method == "create") {
                SC.call("local_monorailservices_send_message", { courseid: this.mCourseId, userids: this.mUserIds , summary: this.mSummary }, 
                    function (data) {
                        options.success(data);
                    }, 
                    this, { errorHandler: function (data) {
                        options.error(data);
                    }});
            }
            return this;
        },

		defaults: function ()
        {
            return {
            };
		}
	});
});
