/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["backbone"], function ()
{
    return Backbone.Model.extend(
    {
        defaults: {
            id: undefined,
            name: null,
            stock_backgrounds: [],
            description: null,
            idnumber: undefined 
        }
    });
});
