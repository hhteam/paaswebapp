/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/collections/course/sections", "app/tools/servicecalls", "app/collections/course/participants",
        "app/collections/course/invited_users", "app/collections/forum/forums", "i18n!nls/strings", "app/tools/warnings",
        "app/collections/forum/recentposts","app/collections/forum/recentdiscussions", 
        "app/collections/task/coursetasks", "app/collections/course/liveevents"],
    function (SectionCollection, SC, ParticipantsCollection, InvitedUsersCollection, ForumCollection, str, Warnings,
        RecentPostsCollection, RecentDiscussionsCollection, CourseTaskCollection, LiveEventsCollection)
{

	return Backbone.Model.extend(
	{
        sectionsHaveChanged: false,

        sync: function (method, model, options)
        {
            // FIXME: courseData is necessary only when saving data, not loading
            var courseData = _.pick(model.toJSON(), "fullname", "shortname", "categoryid", "coursebackground", "startdate", "mainteacher", "cert_enabled", "language", "keywords", "licence", "participants_hidden", "cert_custom", "cert_custom_info");

            if (model.get("enddate") !== null && model.get("enddate") !== '')
            {
                courseData.enddate = Math.floor(model.get("enddate")/1000);
            }
            /*
            //If start date o , course is self paced
            if (! courseData.startdate) {
                var now = new Date();
                courseData.startdate = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate());
            }
            */
            courseData.startdate = Math.floor(courseData.startdate/1000);
            if (courseData.mainteacher == null)
            {
                courseData.mainteacher = _User.get("id");
            }

            if (!(parseInt(courseData['categoryid']) || 0)) {
                courseData['categoryid'] = 15;
            }

            // if wizard mode, we want create
            if (model.get("wizard_mode") == 1) {
                model.id = 0;
                method = 'create';
            }

            courseData.course_country
                    = _.keys( model.attributes.course_country ).sort().join();

            switch (method)
            {
                case "create":

                    var cohortid = _User.get("adminCohort");

                    if (!cohortid)
                    {
                        cohortid = _User.get("cohorts");

                        if (typeof cohortid == "string" && cohortid.indexOf(",") != -1)
                        {
                            cohortid = cohortid.split(",")[0];
                        }
                    }

                    if (cohortid)
                    {
                        courseData.cohortid = cohortid;
			            model.set({ cohortid: courseData.cohortid }, { silent: true });
                    }

                    courseData.numsections = 0;

                    SC.call("local_monorailservices_create_courses", { courses: [ courseData ] }, function (data)
                    {
                        model.set({ id: data[0].id, code: data[0].code }, { silent: true });

                        if (data[0].shortname) {
                            model.set({shortname: data[0].shortname} , { silent: true });
                        }

                        model.get("participants").setCourseId(model.get("id"));
                        model.get("invitedUsers").setId(model.get("id"));
                        model.get("forums").setCourseId(model.get("id"));
                        model.get("assignments").setCourseId(model.get("id"));
                        model.get("liveevents").setCourseId(model.get("id"));

                        options.success(method, model, options);

                        model.get("sections").each(function (sec)
                        {
                            sec.set({ courseid: data[0].id }, { silent: true });
                            sec.save();
                        });
                    }, this, { deferred: options.deferred });

                    break;

                case "update":
                    courseData.id = model.get("id");

                    courseData["course_privacy"] = model.get("course_privacy");
                    courseData["course_access"] = model.get("course_access");

                    courseData["course_logo"] = model.get("course_logo");
                    if((courseData["course_privacy"] == 1) && (courseData["course_access"] == 3)) {
                      //Public and open may have price set
                      courseData["course_price"] = model.get("course_price");
                      courseData["course_paypal_account"] = model.get("course_paypal_account");
                    }

                    SC.call("local_monorailservices_update_courses", { courses: [ courseData ] }, function (data)
                    {
                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });

                    break;

                case "patch":
                    SC.call("local_monorailservices_update_courses", { courses: [ _.extend({ id: model.get("id") }, options.attrs) ] }, function (data)
                    {
                        model.set(options.attrs, { silent: true });

                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });

                    break;

                case "read":
                    SC.call("local_monorailservices_get_course_info", { coursecode: model.get("code") || "", courseid: model.get("id") || 0 }, function (data)
                    {
                        model.set({
                            id: data.courseid,
                            code: data.code,
                            shortname: data.shortname,
                            fullname: data.fullname,
                            completed: data.completed,
                            coursebackground: data.coursebackground,
                            coursebackgroundtn: data.coursebackgroundtn,
                            course_logo: data.course_logo,
                            inviteUrl: data.inviteurl,
                            invitesOpen: (data.invitesopen == 1) ? true : false,
                            inviteCode: data.invitecode,
                            can_edit: (data.canedit == 1) ? true : false,
                            cantwrite: (data.cantwrite == 1) ? true : false,
                            categoryid: (data.categoryid > 15) ? 0 : data.categoryid,
                            course_privacy: data.course_privacy,
                            course_access: data.course_access,
                            course_review: data.course_review,
                            review_submit_time: data.review_submit_time,
                            course_rating: data.course_rating,
                            course_price: data.course_price,
                            course_paypal_account: data.course_paypal_account,
                            cohortid: data.cohortid,
                            ccprivate: data.ccprivate,
                            ccprivateext: data.ccprivateext,
                            startdate: data.startdate*1000,
                            enddate: (data.enddate !== 0) ? data.enddate*1000 : null,
                            taskscount: data.taskcount,
                            mainteacher: data.mainteacher,
                            teacher_title: data.teacher_title,
                            teacher_name: data.teacher_name,
                            teacher_avatar: data.teacher_avatar,
                            durationstr: data.durationstr,
                            skills: data.skills,
                            cert_enabled: data.cert_enabled,
                            cert_hashurl: data.cert_hashurl,
                            cert_issuetime: data.cert_issuetime,
                            cert_sigpic: data.cert_sigpic,
                            cert_orglogo: data.cert_orglogo,
                            progress: data.progress,
                            extcourse: data.extcourse,
                            extprovider: (data.extcourse == 1) ? data.extprovider : null,
                            students: (data.canedit == 1) ? data.students : 0,
                            _students: data._students,
                            language: data.language,
                            keywords: data.keywords,
                            licence: data.licence,
                            derivedfrom: data.derivedfrom,
                            derivedfrom_name: data.derivedfrom_name,
                            derivedfrom_code: data.derivedfrom_code,
                            participants_hidden: data.participants_hidden,
                            cert_custom: data.cert_custom,
                            cert_custom_info: data.cert_custom_info,
                            course_country: data.course_country
                                ? _.object( data.course_country.split(','), [] )
                                : {},
                        });

                        model.get("sections").setCourseId(data.courseid);
                        model.get("participants").setCourseId(data.courseid);
                        model.get("forums").setCourseId(data.courseid);
                        model.get("assignments").setCourseId(data.courseid);
                        model.get("liveevents").setCourseId(data.courseid);

                        // if user can edit, lets get invited users email list ready just in case
                        if (data.canedit == 1) {
                            model.get("invitedUsers")
                                .setId(data.courseid)
                                .setCode(data.invitecode);
                        }

                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });
                    break;

                case "delete":
                    SC.call("local_monorailservices_delete_course", { courseid: model.get("id") }, function (data)
                    {
                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });

                    break;

                default:
                    console.warn("Unhandled method in course model sync: " + method);
            }
        },

        validate: function(attrs, options)
        {
            // Check attachment name
            for(var name in attrs) {
                var value = attrs[name];

                if ( name.indexOf('|attachments|') !== -1 && name.indexOf('|name') !== -1 ) {
                    if (value == '')
                    {
                        return {err: str.fill_missing_fields, value: str.label_attachment};
                    }
                }
            }

            if ("fullname" in attrs)
            {
                if (attrs.fullname == "")
                {
                    return str.fill_missing_fields;
                }

                if (attrs.fullname.length > 120)
                {
                    return str.course_fname_limit;
                }
            }

            if ("enddate" in attrs && attrs.enddate !== null && attrs.enddate.length)
            {
                if ("startdate" in attrs)
                {
                    var start = attrs.startdate;
                } else
                {
                    var start = this.model.get("startdate");
                }
                if (attrs.enddate < start)
                {
                    return str.course_end_before_start;
                }
            }
        },

        // TODO: remove this abomination...
        assignRole: function(userids) {
        	var assign = [];
        	var courseid = this.get('id');
        	_.each(userids, function(uid) {
        		assign.push({id: courseid, userid: uid, rolename:'editingteacher'});
            });
        	SC.call("local_monorailservices_assign_roles", { assignments: assign }, function (data)
            {

                // process any errors/warnings
                /*
                if(data && data.warnings.length)
                {
                    Warnings.processWarnings({msgtype:'info', message: data.warnings});
                }
                */
            });
        },

        unassignRole: function(userids) {
        	var unassign = [];
        	var courseid = this.get('id');
        	_.each(userids, function(uid) {
        		unassign.push({id: courseid, userid: uid, rolename:'editingteacher'});
            });
        	SC.call("local_monorailservices_unassign_roles", { assignments: unassign }, function (data)
            {
                // process any errors/warnings
                /*
                if(data && data.warnings.length)
                {
                    Warnings.processWarnings({msgtype:'info', message: data.warnings});
                }
                */
            });
        },

        unenrolUser: function(userids) {
        	var unenrol = [];
        	var courseid = this.get('id');
        	_.each(userids, function(uid) {
        		unenrol.push({courseid: courseid, userid: uid});
            });
        	SC.call("local_monorailservices_unenrol_users", { enrolments: unenrol }, function (data)
            {
                // process any errors/warnings
                /*
                if(data && data.warnings.length)
                {
                    Warnings.processWarnings({msgtype:'info', message: data.warnings});
                }
                */
            });
        },

        closeEnrollment: function() {
            this.setEnrollment(false);
        },

        openEnrollment: function() {
            this.setEnrollment(true);
        },

        setEnrollment: function(status) {
            this.set('invitesOpen', status);
            SC.call("local_monorailservices_set_enrollment", { courseid: this.get('id'), status: (status) ? 1 : 0 }, function (data)
            {
                // process any errors/warnings
                /*
                if(data && data.warnings.length)
                {
                    Warnings.processWarnings({msgtype:'info', message: data.warnings});
                }
                */
            });
        },

        toggleCourseStatus: function(status) {
            this.set('completed', status);
            this.set('enddate', (! status) ? 0 : new Date().getTime());
            SC.call("local_monorailservices_set_course_status", { courseid: this.get('id'), status: status }, function (data)
            {
                // process any errors/warnings
                /*
                if (data && data.warnings.length)
                {
                    Warnings.processWarnings({msgtype:'info', message: data.warnings});
                }
                */
            });
        },

        validateSkillName: function(skillname, success, error, context) {
            SC.call("local_monorailservices_validate_skill", { skillname: skillname }, function (data)
            {
                if (typeof data === 'undefined' || ! data || data.result == 1) {
                    error(context, data, skillname);
                } else {
                    success(context, data, skillname);
                }
            });
        },

        saveSkills: function(success, error, context) {
            var that = this;
            SC.call("local_monorailservices_save_skills", { courseid: this.get("id"), skills: this.get("skills") }, function (data)
            {
                if (typeof data === 'undefined' || ! data || data.result == 1) {
                    if (typeof data.skills !== 'undefined') {
                        that.set('skills', data.skills);
                    }
                    error(context, data);
                } else {
                    if (typeof data.skills !== 'undefined') {
                        that.set('skills', data.skills);
                    }
                    success(context, data);
                }
            });
        },

        courseUrl: function() {
            return ((this.get("course_privacy") == 1)) ? EliademyUrl + "/" + this.get("code") : EliademyUrl + RootDir + "courses/" + this.get("code");
        },

        addCountry: function( country ) {
            this.attributes.course_country[country] = 0;
            this.trigger('change:course_country');
        },

        removeCountry: function( country ) {
            delete this.attributes.course_country[country];
            this.trigger('change:course_country');
        },

        defaults: function ()
        {
            return {
                id: null,
                fullname: "",
                shortname: "",
                code: "",
                completed: 0,
                assignments: new CourseTaskCollection(),
                sections: new SectionCollection(),
                participants: new ParticipantsCollection(),
                background: "",
                can_edit: false,
                cantwrite: false,
                inviteUrl: "",
                invitesOpen: false,
                invitedUsers: new InvitedUsersCollection(),
                liveevents: new LiveEventsCollection(),
                inviteCode: null,
                categoryid: 0,
                wizard_mode: 0,
                course_access: 1,
                course_privacy: 1,
                course_review: 0,
                course_rating: 0,
                review_submit_time: "",
                course_price: 0,
                course_paypal_account: "",
                course_logo: "",
                startdate: 0,
                enddate: null,
                mainteacher: 0,
                teacher_name: null,
                teacher_title: null,
                teacher_avatar: null,
                durationstr: null,
                cohortid: 0,
                ccprivate: 1,
                ccprivateext: 0,
                skills: [],
                progress: 0,
                students: 0,
                cert_enabled: 0,
                cert_hashurl: "",
                cert_issuetime: 0,
                licence: null,
                derivedfrom: null,
                derivedfrom_name: null,
                derivedfrom_code: null,
                forums: new ForumCollection(),
                recent_posts: new RecentPostsCollection(),
                recent_discussions : new RecentDiscussionsCollection(),
                cert_custom: 0,
                cert_custom_info: "",
                course_country: {},
            };
        }
    });
});
