/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create": case "update":
                    SC.call("local_monorailservices_rate_course", {
                        courseid: model.get("courseid"),
                        stars: model.get("stars") || -1,
                        review: model.get("review"),
                        shareon: model.get("shareon"),
                        enable_autoshare: model.get("enable_autoshare"),
                        image: model.get("image"),
                        share_message: model.get("share_message") },
                        function ()
                        {
                            options.success(method, model, options);
                        }, this);
                    break;
            }
        },

        defaults: {
            courseid: undefined,        // Course id
            stars: undefined,           // Star count
            review: undefined,          // Review text
            share_message: undefined,   // Message to share
            shareon: null,              // Where to share
            image: "",                  // Image to share
            enable_autoshare: 0         // Update sharing settings
        }
    });
});
