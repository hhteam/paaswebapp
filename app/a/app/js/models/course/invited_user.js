/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["i18n!nls/strings", "app/tools/servicecalls", "app/views/dialogs/error_message", "backbone"],
    function (str, SC, ErrorDialog)
{
	return Backbone.Model.extend(
	{
        mId: null,
        mCode: null,
        mSummary : '',

        setId: function (id)
        {
            this.mId = id;
            return this;
        },

        setCode: function (code)
        {
            this.mCode = code;

            return this;
        },

        setSummary: function (summary)
        {
            this.mSummary = summary;

            return this;
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create":
                    SC.call("local_monorailservices_invite_send", { id: this.mId, code: this.mCode, email: this.get('email'), resend: 0,
                                                summary: this.mSummary, userTag: model.get("userTag"), userRole: model.get("userRole") },
                        function (data) {
                            var errors = [];

                            if (data instanceof Object && data.warnings instanceof Array) {
                                for (var i=0; i<data.warnings.length; i++) {
                                    if (data.warnings[i].warningcode == "2") {
                                        errors.push(data.warnings[i].message);
                                    }
                                }
                            }

                            if (errors.length) {
                                options.error(data);
                                (new ErrorDialog({ header: str.button_send_email_invitation,
                                    message: str.join_cohort_email_failed.replace("%EMAIL", errors.join(", ")) })).render();
                            } else {
                                options.success(data);
                            }
                        },
                        this,
                        { errorHandler: function (data) {
                            options.error(data);
                        }});
                    break;

                case "delete":
                    SC.call("local_monorailservices_del_course_invite", { id: model.get("id") }, function ()
                    {
                        options.success(method, model, options);
                    });
                    break;

                default:
                    console.warn("Unhandled method in invited user model: " + method);
            }

            return this;
        },

		defaults: function ()
        {
            return {
                email: null,
                invitedby: null,
                invitedwhen: null,
                userTag: 0
            };
		}
	});
});
