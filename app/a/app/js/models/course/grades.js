/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "app/collections/course/certificates"], function (SC, CertificateCollection)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_course_grades", { courseid: this.get("courseid") }, function (ret)
                    {
                        try
                        {
                            this.set({ courseid: ret.courseid, tasks: ret.tasks, users: ret.users });
                            this.get("certificates").reset(Array.prototype.slice.call(ret.certificates));
                        }
                        catch (err)
                        {
                            this.clear();
                        }

                        options.success(method, model, options);
                    }, this);
                    break;
            }
        },

        defaults: function ()
        {
            return {
                courseid: undefined,
                tasks: [],
                users: [],
                certificates: new CertificateCollection()
            };
        }
    });
});
