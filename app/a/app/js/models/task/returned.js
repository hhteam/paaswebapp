/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {

        picurl: function(size) {
            switch (size) {
                case 'x':
                    var s = '512';
                    break;
                case 'l':
                    var s = '185';
                    break;
                case 'm':
                    var s = '90';
                    break;
                case 's':
                    var s = '32';
                    break;
            }
            return MoodleDir+'public_images/user_picture/'+this.get("profilepichash")+'_'+s+'.jpg';
        },

        sync: function (method, model, options)
        {
            var rawData;

            switch (method)
            {
                case "create":
                    rawData = model.toJSON();

                    SC.call("local_monorailservices_update_grades",
                        { grades: [ _.extend({ userid: rawData.studentid }, _.pick(rawData, "assignid", "grade", "feedback", "revert_to_draft")) ] },
                        function (ret)
                        {
                            options.success(method, model, options);
                        }, this, { deferred: options.deferred });

                    break;
            }
        },

        toJSON: function () {
            var json = _.clone(this.attributes);
            json.picurl = {
                x: this.picurl('x'),
                l: this.picurl('l'),
                m: this.picurl('m'),
                s: this.picurl('s'),
            }
            return json;
        },
    });
});
