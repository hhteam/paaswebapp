/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "app/tools/utils", "backbone"], function (str, Utils)
{
    return Backbone.Model.extend(
    {
        defaults: {
            text: "",
            correct: false
        },

        toJSON: function (cid)
        {
            var json = _.clone(this.attributes);

            if (cid)
            {
                json.cid = this.cid;
            }

            json.correct = this.attributes.correct ? 1 : 0;

            return json;
        },

        validate: function (attrs, options)
        {
            if ("text" in attrs)
            {
                if (Utils.isEmpty(attrs.text))
                {
                    return str.fill_missing_fields;
                }
                else if (attrs.text.length > 500)
                {
                    return str.error_limit;
                }
            }
        }
    });
});
