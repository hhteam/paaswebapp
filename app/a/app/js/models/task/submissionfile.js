/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create":
                    var rawData = model.toJSON(), data = { };
                    _.extend(data, _.pick(rawData, "assignid", "filename", "name"));
                    return SC.call("local_monorailservices_add_submissions", { submissions: [ data ] },
                        function (data) {
                            if ("files" in data && data.files.length > 0) {
                                // fix model data
                                model.set('url', data.files[0].url);
                                model.set('id', data.files[0].id);
                                model.set('type', data.files[0].filetype);
                                model.set('icon', model.icon(data.files[0].filetype));
                            }
                            options.success(method, model, options);
                        }, this, { deferred: options.deferred });
                    break;

                case "delete":
                    return SC.call("local_monorailservices_del_submissions",
                        { fileitems: [ { assignmentid: model.get("assignid"), fileids: [ model.get("id") ] } ] },
                        function (data) {
                            options.success(method, model, options);
                        }, this, { deferred: options.deferred });
            }
        },
        
        toJSON: function () {
            var json = _.clone(this.attributes);
            json.cid = this.cid;
            return json;
        },
        
        icon: function (filetype) {
            if(!filetype) {
                filetype = this.get("type");
            }
            // Since these are not file resources we do not get icon from Moodle
            switch (filetype) {
                        case "pdf":
                        case "application/pdf":
                            return 'icon-pdf.png';
                        break;

                        case "application/vnd.ms-excel":
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            return 'icon-excel.png';
                        break;

                        case "application/vnd.oasis.opendocument.spreadsheet":
                            return 'icon-spreadsheet.png';
                            break;

                        case "application/vnd.ms-powerpoint":
                        case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                            return 'icon-powerpoint.png';
                        break;

                        case "application/vnd.oasis.opendocument.presentation":
                            return 'icon-presentation.png';
                            break;

                        case "application/msword":
                        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                            return 'icon-word.png';
                        break;

                        case "text/plain":
                        case "text/rtf":
                        case "application/vnd.oasis.opendocument.text":
                            return 'icon-text.png';
                            break;
                        default:
                            return 'icon-download.png';
                            break;
            }
        },

        defaults: {
            assignid: 0,
            filename: null,
            name: null,
            url: '#',
            media: null,
            remove: false,
            type: null,
            icon: null
        }
    });
});
