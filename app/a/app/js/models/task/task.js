/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["i18n!nls/strings", "app/tools/servicecalls", "app/collections/attachments", "app/collections/task/questions", "app/tools/utils", "app/tools/inlinecontent"],
    function (str, SC, AttachmentCollection, QuestionCollection, Utils, InlineContent)
{
    return Backbone.Model.extend(
    {
        initialize: function ()
        {
            if (Object.prototype.toString.apply(this.attributes.questions) == "[object Array]")
            {
                this.attributes.questions = new QuestionCollection(this.attributes.questions);
            }
        },

        sync: function (method, model, options)
        {
            var rawData = model.toJSON(), data = { };

            switch (method)
            {
                case "create":
                    data['course'] = rawData.courseid;
                    data["nosubmissions"] = rawData.nosubmissions;
                    data["attempts"] = rawData.attempts;

                case "update":
                    data['sectionid'] = rawData.sectionid;
                    data["options"] = JSON.stringify(_.pick(rawData, "startmsg", "endmsg", "question_order"));
                    data["visible"] = 1;  //rawData.visible; -- visible/hidden button is gone... If user had hidden tasks, there is no way to unhide them in UI anymore...

                    _.extend(data, _.pick(rawData, "id", "instanceid", "name", "intro", "modulename", "attempt", "attempts", "current_question", "endquiz", "nolatesubmissions"));

                    if (rawData.duedate > 0) {
                        data.duedate = Utils.dateToString(new Date(rawData.duedate * 1000));
                    }

                    if (rawData.startdate > 0) {
                        data.startdate = Utils.dateToString(new Date(rawData.startdate * 1000));
                    }

                    data.visible = 1;
                    data.intro = InlineContent.embedAttachments(data.intro, model.get("attachments"));
                    InlineContent.flagInlinedAttachments(data.intro,model.get("attachments"));
                    model.set({ "intro": data.intro }, { silent: true });

                    if (typeof options.quietsave !== 'undefined' && options.quietsave) {
                        // Do a quiet save, no notifications created
                        data.quiet = 1;
                    }

                    if (rawData.modulename == "quiz")
                    {
                        data.questions = [];

                        rawData.questions.forEach(function (question)
                        {
                            question.text = InlineContent.embedAttachments(question.text, model.get("attachments"));
                            InlineContent.flagInlinedAttachments(question.text,model.get("attachments"));

                            data.questions.push(question);
                        });
                    }

                    SC.call("local_monorailservices_upd_assignments", { assignments: [ data ] }, function (data)
                    {
                        if (model.isNew())
                        {
                            var taskData = data['assignments'][0];

                            // assignments were saved/updated
                            if ("assignments" in data && data['assignments'].length > 0)
                            {
                                model.set({ id: taskData["id"], instanceid: taskData["assignid"] }, { silent: true });
                            }
                        }

                        options.success(method, model, options);

                    }, this, { deferred: options.deferred });
                    //In update - user has write access
                    InlineContent.removeDeletedInlineAttachments(model.get("attachments"));
                    break;

                case "delete":
                    SC.call("local_monorailservices_del_assignments", { assignids: [ rawData.instanceid ] },
                        function (data) {
                            options.success(method, model, options);
                            if ("warnings" in data && data.warnings.length > 0) {
                                if (typeof options.error === 'function') {
                                    options.error(method, model, options);
                                }
                            }
                        }, this, { deferred: options.deferred, errorHandler: function () {
                            if (typeof options.error === 'function') {
                                options.error(method, model, options);
                            }
                        }}
                    );
                    break;

                case "read":
                    SC.call("local_monorailservices_get_task_details", { instanceid: rawData.instanceid }, function (data)
                    {
                        if (data instanceof Object && data.warnings instanceof Array && data.warnings.length > 0)
                        {
                            // A problem!
                            options.success(method, model, options);
                        }
                        else
                        {
                            model.set(_.omit(data, "questions", "duedate", "startdate", "options"), { silent: true });
                            model.get("questions").reset(data.questions, { silent: true });

                            if ('duedate' in data && data.duedate.length > 0) {
                                model.set({ duedate: Math.floor(Utils.stringToDate(data.duedate).getTime() / 1000) });
                            } else {
                                model.set({ duedate: 0 });
                            }

                            if ('startdate' in data && data.startdate.length > 0) {
                                model.set({ startdate: Math.floor(Utils.stringToDate(data.startdate).getTime() / 1000) });
                            } else {
                                model.set({ startdate: 0 });
                            }

                            if ("options" in data && data.options) {
                                try {
                                    var opt = JSON.parse(data.options);
                                    for (var i in opt) {
                                        model.set(i, opt[i]);
                                    }
                                } catch (err) {
                                }
                            }

                            var attachments = [];

                            _.each(data.fileinfo, function (f)
                            {
                                attachments.push(_.extend(f, { taskid: data.id, taskinstance: data.instanceid, source: "task_files" }));
                            });

                            _.each(data.vfileinfo, function (f)
                            {
                                var vurl =  Utils.videoThumbnailUrl(f.id);
                                attachments.push(_.extend(f, { taskid: data.id, taskinstance: data.instanceid, url: vurl, source: "task_vfiles" }));
                            });

                            model.get("attachments").reset(attachments, { silent: true });

                            options.success(method, model, options);
                        }
                    }, this);
                    break;
            }
        },

        validate: function (attrs, options)
        {
            if ("name" in attrs)
            {
                if (Utils.isEmpty(attrs.name))
                {
                    return str.fill_missing_fields;
                }

                if (attrs.name.length > 120)
                {
                    return str.error_limit;
                }
            }

            if (attrs.modulename != "quiz" && "intro" in attrs && Utils.isEmpty(attrs.intro))
            {
                return str.fill_missing_fields;
            }

            if ("startdate" in attrs && "duedate" in attrs && attrs.startdate > 0 && attrs.duedate > 0 && attrs.startdate > attrs.duedate) {
                return str.label_err_end_before_start;
            }

            if (attrs.modulename == "quiz" && attrs.questions)
            {
                var err = attrs.questions.validate();

                if (err) {
                    return err;
                }

                if (!attrs.questions.length) {
                    return str.label_quiz_questions_missing;
                }
            }
        },

		defaults: function ()
        {
            return {
                id: undefined,
                instanceid: undefined,
                modulename: "",
                name: "",
                duedate: 0,
                startdate: 0,
                intro: "",
                courseid: null,
                coursecode: "",
                nosubmissions: 0,
                can_edit: false,
                can_upload_files: true,
                questions: new QuestionCollection(),
                attachments: new AttachmentCollection(),
                section_visible: true,
                visible: 1,
                endquiz: 0,
                isgraded: 0,
                attempt: 0,
                attempts: 1,
                attemptsleft: 0,
                answers: []
            };
		},

        toJSON: function (cid)
        {
            var json = _.clone(this.attributes);

            if (this.attributes.questions instanceof Object && this.attributes.questions.toJSON instanceof Function)
            {
                json.questions = this.attributes.questions.toJSON(cid);
            }
            else
            {
                json.questions = [];
            }

            if (this.attributes.attachments instanceof Object && this.attributes.attachments.toJSON instanceof Function)
            {
                json.attachments = this.attributes.attachments.toJSON(cid);
            }
            else
            {
                json.attachments = [];
            }

            return json;
        }
	});
});
