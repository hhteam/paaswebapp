/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["i18n!nls/strings", "app/tools/servicecalls", "app/collections/task/submissionfiles", "app/tools/utils"],

    function (str, SC, SubmissionFilesCollection, Utils)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var rawData = model.toJSON(), data = { };
            _.extend(data, _.pick(rawData, "assignid", "text", "status"));

            switch (method)
            {
                case "create":
                case "update":
                    SC.call("local_monorailservices_task_submission", data,
                        function (data)
                        {
                            if ("timesubmitted" in data)
                            {
                                model.set("timesubmitted", data['timesubmitted']);
                            }

                            var filesToSave = 0, filesToDelete = 0;

                            _.each(model.get("files").models, function (file)
                            {
                                if (file.isNew() && ! file.get("remove"))
                                {
                                    filesToSave++;
                                }
                                else if (file.get("remove") && ! file.isNew())
                                {
                                    filesToDelete++;
                                }
                            });

                            var completeSave = function ()
                            {
                                if (filesToDelete < 1 && filesToSave < 1)
                                {
                                    options.success(method, model, options);

                                    if (options.deferred instanceof Object && options.deferred.then instanceof Function)
                                    {
                                        options.deferred.resolveWith(this);
                                    }
                                }
                            };

                            completeSave();

                            // Save files
                            _.each(model.get("files").models, function (file)
                            {
                                if (file.isNew() && ! file.get("remove"))
                                {
                                    file.save(undefined, { success: function ()
                                    {
                                        filesToSave--;
                                        completeSave();
                                    }});
                                }
                                else if (file.get("remove") && ! file.isNew())
                                {
                                    file.destroy({ success: function ()
                                    {
                                        filesToDelete--;
                                        completeSave();
                                    }});
                                }
                            });
                        }, this);
                    break;

                case "delete":
                    console.log("Unsupported delete operation for submission");
                    break;
            }
        },

        validate: function (attrs, options)
        {
            if ("text" in attrs && "files" in attrs)
            {
                if (Utils.isEmpty(attrs.text) && attrs.files.length < 1)
                {
                    return str.task_instructions;
                }
            }
        },

        defaults: {
            assignid: 0,
            text: null,
            status: 0,   // 0 draft, 1 published
            files: new SubmissionFilesCollection(),
            timesubmitted: 0
        }
    });
});
