/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["backbone"], function ()
{
    return Backbone.Model.extend(
    {
        idAttribute: "key",

        defaults: {
            key: "",
            value: ""
        }
    });
});
