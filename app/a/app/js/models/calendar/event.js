/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "i18n!nls/strings", "app/tools/utils", "backbone"], function (SC, str, Utils)
{
    return Backbone.Model.extend(
    {
        initialize: function ()
        {
            if (this.get("timestart"))
            {
                this.set("timestart", Math.floor(Utils.stringToDate(this.get("timestart")).getTime() / 1000));
            }

            if (this.get("timeend"))
            {
                this.set("timeend", Math.floor(Utils.stringToDate(this.get("timeend")).getTime() / 1000));

                this.set("timeduration", this.get("timeend") - this.get("timestart"));
            }
        },

        sync: function (method, model, options)
        {
            var data;

            switch (method)
            {
                case "create":
                    data = model.pick("name", "description", "location", "timestart", "timeduration", "courseid", "eventtype");

                    data.timestart = Utils.dateToString(new Date(data.timestart * 1000));

                    SC.call("local_monorailservices_create_calendar_events", { events: [ data ] }, function (res)
                    {
                        if (res instanceof Object && res.events instanceof Array && res.events.length > 0)
                        {
                            model.set({ id: res.events[0].id });

                            options.success(method, model, options);
                        }
                    }, this);
                    break;

                case "update":
                    data = model.pick("id", "name", "description", "location", "timestart", "timeduration", "courseid", "eventtype");

                    data.timestart = Utils.dateToString(new Date(data.timestart * 1000));

                    SC.call("local_monorailservices_update_calendar_events", { events: [ data ] }, function (res)
                    {
                        options.success(method, model, options);
                    }, this);
                    break;

                case "delete":
                    SC.call("local_monorailservices_delete_calendar_events", { events: [ { eventid: model.get("id"), repeat: 0 } ] }, function ()
                    {
                        options.success(method, model, options);
                    }, this);
                    break;

                default:
                    console.warn("Unimplemented calendar event model method: " + method);
            }
        },

        validate: function (attrs, options)
        {
            if ("name" in attrs)
            {
                if (!attrs.name)
                {
                    return str.fill_missing_fields;
                }

                if (attrs.name.length > 100)
                {
                    return str.error_limit;
                }
            }
        }
    });
});
