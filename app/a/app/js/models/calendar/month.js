/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "app/collections/calendar/events", "moment", "backbone"], function (SC, EventCollection, moment)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_user_events", {
                        timestart: moment(model.get("id") + "01", "YYYYMMDD").format("X"),
                        untiltime: moment(model.get("id") + "01", "YYYYMMDD").add("M", 1).format("X") }, function (res)
                    {
                        if (res instanceof Object && res.events instanceof Array)
                        {
                            model.get("events").reset(res.events);
                        }

                        model.set("name", moment(model.get("id") + "01", "YYYYMMDD").format("MMMM YYYY"));

                        options.success(method, model, options);
                    }, this);
                    break;

                default:
                    console.warn("Unimplemented calendar month model method: " + method);
            }
        },

        defaults: function ()
        {
            return {
                events: new EventCollection(),
                name: ""
            };
        }
    });
});
