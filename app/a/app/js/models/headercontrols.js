/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define([ "i18n!nls/strings", "app/views/dialogs/yes_no", "backbone" ], function(str, YesNoDialog) {
    var HeaderControls = new (Backbone.Model.extend({
        defaults : {
            state : 0
        },

        editButtons : function() {
            switch (this.get("state")) {
            case 0:
                return [];

            case 1:
                return [ {
                    label : str.edit,
                    handler : startHandler
                } ];

            case 2:
                return [ {
                    label : str.button_cancel,
                    handler : cancelHandler,
                    "class" : "btn",
                    "id"    : "btn_edit_cancel"
                }, {
                    label : str.button_save,
                    handler : saveHandler,
                    "class" : "btn btn-primary",
                    "id"    : "btn_edit_save"
                } ];

            case 3:
                return [ {
                    label : str.button_cancel,
                    handler : cancelHandler,
                    "class" : "btn",
                    "id"    : "btn_edit_cancel"
                } ];

            case 4:
                return [ {
                    label: str.button_cancel,
                    handler : cancelHandler,
                    "class" : "btn",
                    "id"    : "btn_edit_cancel"
                }, {
                    label: str.task_save_draft,
                    handler : draftHandler,
                    "class" : "btn btn-primary",
                    "id"    : "btn-draft-submission"
                }, {
                    label: str.button_submit,
                    handler : finalHandler,
                    "class" : "btn btn-primary",
                    "id"    : "btn-submit-submission"
                } ];

            case 5:
                return [ {
                    label: str.button_cancel,
                    handler : cancelHandler,
                    "class" : "btn",
                    "id"    : "btn_edit_cancel"
                }, {
                    label: str.task_submit_changes,
                    handler : finalHandler,
                    "class" : "btn btn-primary",
                    "id"    : "btn-submit-submission"
                } ];

            case 6:
                return [ {
                    label: str.button_cancel,
                    handler : cancelHandler,
                    "class" : "btn",
                    "id"    : "btn_edit_cancel"
                }, /* {
                    label: str.button_import,
                    handler : importHandler,
                    "class" : "btn",
                    "id"    : "btn-import"
                }, {
                    label: str.button_reuse,
                    handler : reuseHandler,
                    "class" : "btn",
                    "id"    : "btn-reuse"
                }, */ {
                    label : str.button_next,
                    handler : saveHandler,
                    "class" : "btn btn-primary",
                    "id"    : "btn_edit_save"
                } ];

            case 7:
                return [ {
                    label: str.button_cancel,
                    handler : cancelHandler,
                    "class" : "btn",
                    "id"    : "btn_edit_cancel"
                } /*,{
                    label : str.button_next,
                    handler : reuseHandler,
                    "class" : "btn btn-primary",
                    "id"    : "btn_edit_save"
                }*/ ];

            case 8:
                return [{ label: str.label_finish_later, handler: finishLaterHandler }];

            default:
                console.warn("Unknown header controls state: " + this.get("state"));
            }
        },

        get : function(name) {
            if (name in this && this[name] instanceof Function) {
                return this[name]();
            } else {
                return Backbone.Model.prototype.get.call(this, name);
            }
        }
    }));

    var startHandler = function() {
        HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 2 });
        HeaderControls.get("handler").startEdit();
    };

    var saveHandler = function()
    {
        HeaderControls.set({ "oldstate": HeaderControls.get("state") }, { silent: true });

        var deferred = HeaderControls.get("handler").saveEdit();

        if (deferred instanceof Object && deferred.then instanceof Function)
        {
            deferred.done(function ()
            {
                HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
            });
        }
        else
        {
            console.warn("Was expecting a deferred, got");
            console.log(deferred);
            HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
        }
    };

    var cancelHandler = function() {
        HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
        HeaderControls.get("handler").cancelEdit();
    };

    var draftHandler = function()
    {
        var deferred = HeaderControls.get("handler").saveDraft();

        if (deferred instanceof Object && deferred.then instanceof Function)
        {
            deferred.done(function ()
            {
                HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
            });
        }
        else
        {
            console.warn("Was expecting a deferred, got");
            console.log(deferred);
            HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
        }
    };

    var finalHandler = function()
    {
        var deferred = HeaderControls.get("handler").saveFinal();

        if (deferred instanceof Object && deferred.then instanceof Function)
        {
            deferred.done(function ()
            {
                HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
            });
        }
        else
        {
            console.warn("Was expecting a deferred, got");
            console.log(deferred);
            HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
        }
    };

    var finishLaterHandler = function () {
        (new YesNoDialog({
            message: str.confirm_finish_later_msg,
            context: this,
            button_primary: str.label_finish_later,
            button_secondary: str.label_continue_answering,
            invert_danger: true,
            accepted: function () {
                HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
                HeaderControls.get("handler").finishLater();
            }})).render();
    };

    /*
    var importHandler = function() {
        HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
        HeaderControls.get("handler").handleImport();
    };

    var reuseHandler = function() {
        HeaderControls.set({ "oldstate": HeaderControls.get("state"), "state": 1 });
        HeaderControls.get("handler").handleReuse();
    };
    */

    return HeaderControls;
});
