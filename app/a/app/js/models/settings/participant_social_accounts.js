/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            if (method == "read")
            {
                SC.call("local_monorailservices_get_social_settings", {'userid': model.get("userid") }, function (data)
                {
                    model.set("accounts", data);
                    options.success(method, model, options);
                }, this, { errorHandler: function ()
                {
                    model.set("accounts", []);
                }});
            }
        },

        defaults: {
            userid: null,
            accounts: []
        }
    });
});
