/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create": case "update":
                    var data = this.toJSON();
                    SC.call("local_monorailservices_update_social_settings", { settings : [{
                                type : data.type,
                                share_enroll : data.share_enroll,
                                share_complete : data.share_complete,
                                share_certificate : data.share_certificate,
                              }]
                     }, function (ret)
                    {
                        options.success(method, model, options);
                    }, this);
                    break;

                default:
                    console.warn("soc. account unhandled method: " + method);
                    break;
            }
        },

        defaults: {
            type: undefined,
            account: "",
            accounturl: "",
            share_enroll: false,
            share_complete: false,
            share_certificate: false
        }
    });
});
