/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var data;

            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_crs_activity", { period_start: model.get("period_start"), period_end: model.get("period_end") }, function (res)
                    {
                        model.set({ courses: res.courses });

                        options.success(method, model, options);
                    });
                    break;

                default:
                    console.warn("Unhandled method '" + method + "' in course activity analytics.");
            }
        },

        defaults: {
            courses: [ ],
            period_start: null,
            period_end: null
        }
    });
});
