/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_cal_url", {}, function (data)
                    {
                        model.set(data);
                        options.success(method, model, options);
                    }, this);
                    break;
            }
        },

        defaults: {
            calurl: null 
        }
    });
});
