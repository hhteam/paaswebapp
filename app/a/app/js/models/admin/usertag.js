/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var data;

            switch (method)
            {
                case "create":
                    data = [ { name: model.get("name") } ];

                    SC.call("local_monorailservices_create_usertags", { "tags": data }, function (res)
                    {
                        if (res instanceof Array && res.length)
                        {
                            model.set({ id: res[0].id }, { silent: true });
                        }

                        options.success(method, model, options);
                    });
                    break;

                case "delete":
                    data = [ { id: model.get("id") } ];

                    SC.call("local_monorailservices_delete_usertags", { "tags": data }, function (res)
                    {
                        options.success(method, model, options);
                    });
                    break;

                case "update":
                    data = [ { id: model.get("id"), name: model.get("name") } ];

                    SC.call("local_monorailservices_update_usertags", { "tags": data }, function (res)
                    {
                        options.success(method, model, options);
                    });
                    break;

                default:
                    console.warn("Unhandled method '" + method + "' in usertag.");
            }
        },

        defaults: {
            id: undefined,
            name: ""
        }
    });
});
