/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var data;

            switch (method)
            {
                case "create":
                    SC.call("local_monorailservices_assign_usertags", { tags: [ { userid: model.get("userid"), tagid: model.get("tagid") } ] },
                        function (res)
                        {
                            if (res instanceof Array && res.length)
                            {
                                model.set({ id: res[0].id });
                            }

                            options.success(method, model, options);
                        });
                    break;

                case "delete":
                    SC.call("local_monorailservices_unassign_usertags", { tags: [ { userid: model.get("userid"), tagid: model.get("tagid") } ] },
                        function (res)
                        {
                            options.success(method, model, options);
                        });
                    break;

                default:
                    console.warn("Unhandled method '" + method + "' in usertagrelation.");
            }
        },

        defaults: {
            id: undefined,
            userid: undefined,
            tagid: undefined,
            tagname: ""
        }
    });
});
