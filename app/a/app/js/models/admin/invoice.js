/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["backbone"], function ()
{
    var M = Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                default:
                    console.warn("Unhandled method '" + method + "' in invoice model.");
            }
        }
    });

    M._noRead = true;

    return M;
});
