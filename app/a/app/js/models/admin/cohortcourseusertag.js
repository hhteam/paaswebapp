/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create":
                    SC.call("local_monorailservices_enrol_usertags", { tags: [ _.pick(model.toJSON(), "tagid", "courseid", "rolename") ] }, function (ret)
                    {
                        if (ret instanceof Array && ret.length)
                        {
                            model.set({ id: parseInt(ret[0].id) });
                        }

                        options.success(method, model, options);
                    });
                    break;

                case "delete":
                    SC.call("local_monorailservices_unenrol_usertags", { tags: [ _.pick(model.toJSON(), "tagid", "courseid") ] }, function (ret)
                    {
                        options.success(method, model, options);
                    });
                    break;

                default:
                    console.warn("Unhandled method '" + method + "' in cohort course usertag model.");
            }
        },

        defaults: {
            id: undefined,          // Relation id
            name: "",               // Tag name
            tagid: undefined,       // Tag id
            courseid: undefined,    // Course id
            rolename: ""            // Role name
        }
    });
});
