/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "app/collections/admin/usertagrelations"], function (SC, UserTagRelationCollection)
{
    return Backbone.Model.extend(
    {
        initialize: function (attr, opt)
        {
            this.cohort = opt.cohort;

            if (attr.tags instanceof Array)
            {
                this.attributes.tags = new UserTagRelationCollection();

                this.attributes.tags.reset(_.map(attr.tags, function (tag)
                {
                    return { tagid: tag.id, tagname: tag.name, userid: attr.id, id: tag.relid };
                }));
            }

            if (attr.customfields instanceof Array)
            {
                for (var i=0; i<attr.customfields.length; i++)
                {
                    this.attributes["customfield_" + attr.customfields[i].shortname] = attr.customfields[i].value;
                }
            }
        },

        sync: function (method, model, options)
        {
            var data = model.toJSON();

            switch (method)
            {
                case "create":
                    // Create cohort user.
                    SC.call("local_monorailservices_add_cohort_members",
                        { userids: [ data.id ], cohortid: data.cohortid, inviteid: this.cohort.get("inviteCode") }, function (res)
                    {
                        options.success(method, model, options);
                    });
                    break;

                case "delete":
                    if (data.id > 0 && (data.extuser == 1) && (data.state == 6)) {
                        // Remove cohort user.
                        SC.call("local_monorailservices_unenroll_cohort_extuser",
                            { userids: [ data.id ], cohortid: data.cohortid }, function (res)
                        {
                            options.success(method, model, options);
                        });
                        break;
                    } else if (data.id > 0)
                    {
                        if(_User.get('id') == data.id) {
                            model.set('state', 1, { silent: true });
                            return;
                        }
                        // Remove cohort user.
                        SC.call("local_monorailservices_remove_cohort_members",
                            { userids: [ data.id ], cohortid: data.cohortid }, function (res)
                        {
                            options.success(method, model, options);
                        });
                        break;
                    }
                    else
                    {
                        // Remove invite.
                        SC.call("local_monorailservices_revoke_inviteduser",
                            { useremail: data.email, cohortid: this.cohort.get("id"), type: "cohort" }, function ()
                        {
                            options.success(method, model, options);
                        });
                        break;
                    }

                case "update":
                    if (data.state == 1)
                    {
                        // Unsuspend user.
                        SC.call("local_monorailservices_add_cohort_members",
                            { userids: [ data.id ], cohortid: data.cohortid, inviteid: this.cohort.get("inviteCode") }, function (res)
                        {
                            options.success(method, model, options);
                        });
                        break;
                    }
                    else if (data.state == 3)
                    {
                        // Resend invite.
                        SC.call("local_monorailservices_invite_send",
                            { id: this.cohort.get("id"), code: this.cohort.get("inviteCode"), email: data.email, resend: 1 }, function (res)
                        {
                            options.success(method, model, options);
                        });
                        break;
                    }
                    else if (data.state == 4)
                    {
                        // Add as admin.
                        SC.call("local_monorailservices_update_cohort_admins",
                            { userids: [ data.id ], cohortid: data.cohortid, admin: ((data.usertype == 2 || data.usertype == 3) ? 1 : 0), usertype: data.usertype }, function (res)
                        {
                            if (res instanceof Object && res.warnings instanceof Array && res.warnings.length > 0) {
                                options.failed = true;
                            }

                            //model.set('admin', 1, { silent: true });
                            options.success(method, model, options);
                        });
                        break;
                    }
                    else if (data.state == 5)
                    {
                        //Admin cannot self demote himself
                        if(_User.get('id') == data.id) {
                            return;
                        }
                        // Remove as admin.
                        SC.call("local_monorailservices_update_cohort_admins",
                            { userids: [ data.id ], cohortid: data.cohortid }, function (res)
                        {
                            model.set('state', 1, { silent: true });
                            model.set('admin', 0, { silent: true });
                            options.success(method, model, options);
                        });
                        break;
                    } else {
                        console.warn("Invalid state: " + data.state);
                    }

                default:
                    console.warn("Unhandled method '" + method + "' in cohortuser.");
            }
        },

        picurl: function(size) {
            switch (size) {
                case 'x':
                    var s = '512';
                    break;
                case 'l':
                    var s = '185';
                    break;
                case 'm':
                    var s = '90';
                    break;
                case 's':
                    var s = '32';
                    break;
            }
            return MoodleDir+'public_images/user_picture/'+this.get("profilepichash")+'_'+s+'.jpg';
        },

        defaults: function ()
        {
            return {
                cohortid: undefined,
                state: undefined,   /* 1 - normal, 2 - suspended, 3 - invited, 4 - Add as admin, 5 - Remove as admin, 6 - Ext user */

                id: undefined,
                username: "",
                firstname: "",
                lastname: "",
                fullname: "",
                email: "",
                maildisplay: 1,
                skype: "",
                institution: "",
                interests: "",
                firstaccess: undefined,
                lastaccess: undefined,
                courses: "",
                extuser: false,
                lastaccessdate: "",
                city: "",
                country: "",
                profilepichash: "default",
                skills: [],

                tags: new UserTagRelationCollection()
            };
        },

        toJSON: function () {
            var json = _.clone(this.attributes);
            json.picurl = {
                x: this.picurl('x'),
                l: this.picurl('l'),
                m: this.picurl('m'),
                s: this.picurl('s'),
            }
            json.tags = this.attributes.tags.toJSON();
            return json;
        },
    });
});
