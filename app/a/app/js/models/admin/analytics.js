/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return new (Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var data;

            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_cohort_stats", { }, function (res)
                    {
                        model.set({ courses: res.courses, users: res.users, id: 1 });

                        options.success(method, model, options);
                    });
                    break;

                default:
                    console.warn("Unhandled method '" + method + "' in admin analytics.");
            }
        },

        defaults: {
            courses: [ ],
            users: [ ]
        }
    }));
});
