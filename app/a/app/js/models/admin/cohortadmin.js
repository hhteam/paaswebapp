/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create":
                    SC.call("local_monorailservices_update_cohort_admins", { userids: [ model.get("userid") ], cohortid: model.get("cohortid"), admin: 1 }, function ()
                    {
                        options.success(method, model, options);
                    }, this);
                    break;

                case "update":
                    break;

                case "delete":
                    break;
            }
        },

        defaults: function ()
        {
            return {
                userid: undefined,
                cohortid: undefined,
                name: "",
                email: "",
                icon: ""
            };
        }
    });
});
