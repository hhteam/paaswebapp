/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "app/tools/servicecalls", "app/collections/admin/cohortusers", "app/collections/admin/cohortextusers", 
        "app/collections/course/invited_users", "app/collections/admin/cohortcourses"],
function (str, SC, CohortUserCollection, CohortExtUserCollection, InvitedUsersCollection, CohortCourseCollection)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var data = null;

            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_cohorts_info", { cohortids: [ model.get("id") ] }, function (res)
                    {
                        if (res.length == 1 && res[0].id == model.get("id"))
                        {
                            model.set(res[0], { silent: true });

                            if (!model.get('cohorticonurl'))
                            {
                                model.set({ cohorticonurl: RootDir + "app/img/logo-course-title.png" }, { silent: true });
                            }

                            //fetch the invited users info
                            model.get("invitedUsers").setId(model.get("id")).setType("cohort").setCode(model.get('inviteCode')).fetch();
                        }
                        else
                        {
                            // not an admin in this cohort?
                        }

                        options.success(method, model, options);
                    }, this);
                    break;

                case "create":
                    SC.call("local_monorailservices_add_cohorts", { cohorts: [ { name: model.get("name"), movecourses: model.get("movecourses") } ] }, function (res)
                    {
                        if (res.cohortids instanceof Array && res.cohortids.length > 0)
                        {
                            model.set({ id: res.cohortids[0] }, { silent: true });
                            options.success(method, model, options);
                        }
                    }, this);
                    break;

                case "update":
                    data = {
                        id: model.get("id"),
                        name: model.get("name"),
                        settings: model.get('settings'),
                        organization_details: model.get("organization_details")
                    };

                    if (model.hasChanged("cohortcoverurl") || model.get("cohortcoverchanged"))
                    {
                        $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_save_cohort_cover.php",
                                 data: {filename: model.get("cohortcoverurl"), cohort: model.get('id')} });
                    }

                    if (model.hasChanged("cohorticonurl") || model.get("cohorticonchanged"))
                    {
                        $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_save_cohort_logo.php",
                                 data: {filename: model.get("cohorticonurl"), cohort: model.get('id')},
                                 context: this, dataType: 'json',
                            success: function (fname)
                            {
                                data.iconfilename = fname + "?v=" + (new Date()).getTime();

                                SC.call("local_monorailservices_update_cohorts", { cohorts: [ data ] }, function ()
                                {
                                    options.success(method, model, options);
                                }, this);
                            },
                            error: function ()
                            {
                                SC.call("local_monorailservices_update_cohorts", { cohorts: [ data ] }, function ()
                                {
                                    options.success(method, model, options);
                                }, this);
                            }
                        });
                    }
                    else
                    {
                        SC.call("local_monorailservices_update_cohorts", { cohorts: [ data ] }, function ()
                        {
                            options.success(method, model, options);
                        }, this);
                    }

                    break;

                case "patch":
                    SC.call("local_monorailservices_update_cohorts", { cohorts: [ _.extend({ id: model.get("id") }, options.attrs) ] }, function (data)
                    {
                        model.set(options.attrs, { silent: true });

                        options.success(method, model, options);
                    }, this);
                    break;

                default:
                    console.warn("TODO: " + method + " in cohort model!");
            }
        },

        initialize: function (attr)
        {
            this.attributes.users = new CohortUserCollection();
            this.attributes.users.cohortid = attr.id;
            this.attributes.users.cohort = this;

            this.attributes.courses = new CohortCourseCollection();
            this.attributes.courses.cohortid = attr.id;
            this.attributes.courses.cohort = this;

            this.attributes.extusers = new CohortExtUserCollection();
            this.attributes.extusers.cohortid = attr.id;
            this.attributes.extusers.cohort = this;
        },

        validate: function (attrs, options)
        {
            if ("name" in attrs)
            {
                if (!attrs.name)
                {
                    return str.fill_missing_fields;
                }
                else if (attrs.name.length > 40)
                {
                    return str.error_limit;
                }
            }
        },

        defaults: function ()
        {
            return {
                id: undefined,
                invitedUsers:new InvitedUsersCollection(),
                name: "",
                movecourses: 0,
                admins: "",
                googleapp: false,
                cohorticonurl: "",
                inviteCode: "",
                openinvites: 0,
                inviteurl: "",
                invitesopen: 0,
                licences: 0,
                extusers: undefined,
                valid_until: null,
                is_paid: null,
                domain: "",
                isadmin: 0,
                restrictdomain: 0,
                users: undefined,
                settings: undefined,
                courses: undefined,
                skillreport: null,
                activeusercount: 0,
                organization_details: null
            };
        }
    });
});
