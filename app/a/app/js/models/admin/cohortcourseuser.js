/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["backbone"], function ()
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                default:
                    console.warn("Unhandled method '" + method + "' in cohort course user model.");
            }
        },

        defaults: {
            cohortid: undefined,
            courseid: undefined,
            id: undefined,
            username: "",
            firstname: "",
            lastname: "",
            email: "",
            maildisplay: 1,
            roles: undefined
        }
    });
});
