/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            var data;

            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_cohort_userstat", { userid: model.get("userid") }, function (res)
                    {
                        model.set({ usertags: res.usertags, courses: res.courses, activity: res.activity });

                        options.success(method, model, options);
                    });
                    break;

                default:
                    console.warn("Unhandled method '" + method + "' in cohort user analytics.");
            }
        },

        defaults: {
            userid: null,
            usertags: [],
            courses: []
        }
    });
});
