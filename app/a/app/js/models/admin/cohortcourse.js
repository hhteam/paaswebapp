/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/collections/admin/cohortcourseusers", "app/collections/admin/cohortcourseusertags"],
    function (CohortCourseUserCollection, CohortCourseUserTagCollection)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                default:
                    console.warn("Unhandled method '" + method + "' in cohor course model.");
            }
        },

        initialize: function (attr, options)
        {
            this.attributes.users = new CohortCourseUserCollection();
            this.attributes.users.cohortid = attr.cohortid;
            this.attributes.users.courseid = attr.id;
            this.attributes.users.cohort = options.cohort;

            this.attributes.tags = new CohortCourseUserTagCollection();
            this.attributes.tags.cohortid = attr.cohortid;
            this.attributes.tags.courseid = attr.id;
            this.attributes.tags.cohort = options.cohort;
        },

        defaults: {
            id: undefined,
            cohortid: undefined,
            fullname: "",
            shortname: "",
            intro: "",
            code: "",
            invitecode: "",
            invitesopen: undefined,
            teacher_name: "",
            teacher_title: "",
            teacher_avatar: "",
            course_background: "",
            course_backgroundtn: "",
            course_logo: "",
            participants: 0,
            category: 0,
            skills: [],
            users: undefined,
            tags: undefined
        }
    });
});
