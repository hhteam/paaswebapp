/**
 * Eliademy.com
 * 
 * @copyright CBTec Oy
 * @license All rights reserved
 */

define([ "app/tools/servicecalls", "backbone" ], function(SC) {
  return Backbone.Model.extend({

    sync : function(method, model, options) {
      var thisThis = this;
      var data = this.toJSON()
      if (method == "create") {
        SC.call("local_monorailservices_add_ltitool", {
          ltitool : [ {
            course : data.course,
            toolurl : (typeof data.toolurl !== 'undefined' ) ? data.toolurl : '', 
            toolname : (typeof data.toolname !== 'undefined' ) ? data.toolname : '', 
            instructorchoicesendname : (typeof data.instructorchoicesendname !== 'undefined' ) ? +data.instructorchoicesendname : 1, 
            instructorchoicesendemailaddr : (typeof data.instructorchoicesendemailaddr !== 'undefined' ) ? +data.instructorchoicesendemailaddr : 0, 
            resourcekey : (typeof data.resourcekey !== 'undefined' ) ? data.resourcekey : '', 
            password : (typeof data.password !== 'undefined' ) ? data.password : '', 
            icon : (typeof data.icon !== 'undefined' ) ? data.icon : '', 
          } ]
        }, function(data) {
          model.set(data.ltitool[0], {
            silent : true
          });
          options.success(method, model, options);
        }, this);
      } else if (method == "delete") {
        SC.call("local_monorailservices_remove_ltitool", {
          toolid: data.id  
        }, function(data) {
          options.success(method, model, options);
        }, this);
      } else if (method == "update") {
        SC.call("local_monorailservices_update_ltitool", {
          ltitool : [ {
            id : data.id,
            toolurl : (typeof data.toolurl !== 'undefined' ) ? data.toolurl : '', 
            toolname : (typeof data.toolname !== 'undefined' ) ? data.toolname : '', 
            instructorchoicesendname : (typeof data.instructorchoicesendname !== 'undefined' ) ? +data.instructorchoicesendname : 1, 
            instructorchoicesendemailaddr : (typeof data.instructorchoicesendemailaddr !== 'undefined' ) ? +data.instructorchoicesendemailaddr : 0, 
            resourcekey : (typeof data.resourcekey !== 'undefined' ) ? data.resourcekey : '', 
            password : (typeof data.password !== 'undefined' ) ? data.password : '', 
            icon : (typeof data.icon !== 'undefined' ) ? data.icon : '', 
          } ]
        }, function(data) {
          options.success(method, model, options);
        }, this);
      }
      return this;
    },

    defaults : function() {
      return {
        id : undefined,
        name : null,
        url : null,
        custom : false,
        iconurl : null,
        config : undefined 
      };
    }
  });
});
