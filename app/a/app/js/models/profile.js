/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_users_by_id", { userids: [ model.get("id") ] }, function (data)
                    {
                        if (data instanceof Array && data.length > 0)
                        {
                            var udata = data[0];
                            if ("customfields" in udata)
                            {
                                _.each(udata.customfields, function (fld)
                                {
                                     udata["customfields_" + fld.shortname] = fld.value;
                                });
                            }
                        }
                        model.set(udata);
                        options.success(method, model, options);
                    }, this);
                    break;
            }
        },

        picurl: function(size) {
            switch (size) {
                case 'x':
                    var s = '512';
                    break;
                case 'l':
                    var s = '185';
                    break;
                case 'm':
                    var s = '90';
                    break;
                case 's':
                    var s = '32';
                    break;
            }
            return MoodleDir+'public_images/user_picture/'+this.get("profilepichash")+'_'+s+'.jpg';
        },

        defaults: {
            id: undefined,
            username: undefined,
            firstname: undefined,
            lastname: undefined,
            fullname: undefined,
            email: undefined,
            skype: undefined,
            institution: undefined,
            fcid: undefined,
            interests: undefined,
            firstaccess: undefined,
            lastaccess: undefined,
            lang: undefined,
            timezone: undefined,
            timeoffset: undefined,
            description: undefined,
            city: undefined,
            url: undefined,
            country: undefined,
            sessionkey: undefined,
            tutorial: undefined,
            notificationsettings: undefined,
            customfields: undefined,
            preferences: undefined,
            cohortid: 0,
            maildisplay: 1,
            customfields: undefined,
            enrolledcourses: undefined,
            profilepichash: "default",
        },

        toJSON: function () {
            var json = _.clone(this.attributes);
            json.picurl = {
                x: this.picurl('x'),
                l: this.picurl('l'),
                m: this.picurl('m'),
                s: this.picurl('s'),
            }
            return json;
        },
    });
});
