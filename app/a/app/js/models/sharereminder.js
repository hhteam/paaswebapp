/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_share_reminder", { entryid: model.get("entryid"), entrytype: model.get("entrytype") }, function (data)
                    {
                        model.set(data);
                        options.success(method, model, options);
                    }, this);
                    break;

                case "create": case "update":
                    SC.call("local_monorailservices_save_share_reminder", { reminderid: model.get("reminderid"), action: model.get("action") }, function (data)
                    {
                        options.success(method, model, options);
                    }, this);
                    break;
            }
        },

        defaults: {
            entryid: undefined,
            entrytype: undefined,
            reminderid: undefined,
            show: 0
        }
    });
});
