/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["backbone"], function ()
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    $.ajax({ url: EliademyUrl + "/strings.php", data: { lang: model.get("lang") }, dataType: "json" }).done(function (data)
                    {
                        model.set("str", data);
                        options.success(method, model, options);
                    });
                    break;
            }
        },

        defaults: {
            lang: undefined,
            str: { }
        }
    });
});
