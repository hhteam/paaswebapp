/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "backbone"],
    function (SC)
{
	return Backbone.Model.extend(
	{
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_check_invite.php", data: {code: model.id}, dataType: "json", success: function (data)
                    {
                        model.set("userid", parseInt(data.userid));
                        model.set("active", parseInt(data.active));
                        model.set("found", true);
                        if(data.type == 'course') {
                            model.set({courseId: parseInt(data.courseid), courseCode: data.coursecode, inviteType: 'course'});
                        } else if (data.type == 'cohort') {
                            model.set({cohortId: parseInt(data.cohortid), cohortName: data.cohortName, inviteType: 'cohort'});
                        }
                        options.success(method, model, options);
                    },
                      error: function (data)
                    {
                        model.set("userid", 0);
                        model.set("active", 0);
                        model.set("found", false);
                        model.set("courseId", 0);
                        options.error(method, model, options);
                    }});
                    break;

                default:
                    console.warn("Unhandled method in invitation model " + method);
            }
        },

        doEnrollment: function(model,token, email) {

            $invitetype = model.get("inviteType");
            if($invitetype == 'course') {
                params = [{
                    rolename: 'student',
                    inviteid: model.get("id"),
                    token:  (typeof token === "undefined") ? null : token,
                    email:  (typeof email === "undefined") ? null : email,
                    courseid: model.get("courseId"),
                    userid: model.get("userid")
                }];

                SC.call("local_monorailservices_enrol_users", { enrolments: params },
                    function (data)
                    {
                        model.trigger("enrolldone");
                    }, this, { errorHandler: function (data)
                    {
                        // error
                        model.set('found', false);
                        model.trigger("enrollerror");
                    }});
            } else if ($invitetype == 'cohort') {
                if(!model.get("active")) {
                    //Invite no longet valid
                    model.set('found', false);
                    model.trigger("enrollerror");
                    return;
                }
                SC.call("local_monorailservices_add_cohort_members", { userids: [model.get("userid")],
                                               cohortid: model.get("cohortId"), inviteid: model.get("id")},
                    function (data)
                    {
                        if(data.warnings.length > 0) {
                            model.trigger("enrollerror");
                            return;
                        }
                        model.trigger("enrolldone");
                    }, this, { errorHandler: function (data)
                    {
                        // error
                        model.set('found', false);
                        model.trigger("enrollerror");
                    }}
                );
            }
        },

		defaults: function ()
        {
            return {
                id: null,
                userid: null,
                active: null,
                found: false,
                courseId: null,
                cohortId: null,
                cohortName: "",
                courseCode: "",
                inviteType: ""
            };
		}
	});
});
