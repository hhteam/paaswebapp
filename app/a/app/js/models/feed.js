/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["backbone"], function ()
{
    return Backbone.Model.extend(
    {
        defaults: {
            id: null,
            userid: null,
            fullname: null,
            read: null,
            coursename: null,
            text: null,
            timestamp: null,
            sectionid: null,
        }
    });
});
