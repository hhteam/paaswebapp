/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "i18n!nls/strings", "moment", "backbone"], function (SC, str, moment)
{
    var sUsers = [ ];

	return new (Backbone.Model.extend(
	{
        initialize: function ()
        {
            $.ajax({ url: RootDir + "users.php", dataType: "json", success: function (data)
            {
                sUsers = data;
            }});
        },

        sync: function (method, model, options)
        {
            var origData, data, haveInfo, haveLangs;

            switch (method)
            {
                case "read":
                    haveInfo = false;
                    haveLangs = false;

                    SC.call("local_monorailservices_supported_langs", { },
                        function (ret)
                        {
                            try
                            {
                                model.set({ langs: ret.languages }, { silent: true });
                            }
                            catch (err)
                            {
                                console.warn("Invalid data from local_monorailservices_supported_langs");
                            }

                            haveLangs = true;

                            if (haveInfo)
                            {
                                model.trigger("updated");
                            }
                        }, this);
                   
                    SC.call("local_monorailservices_get_users_by_id", { },
                        function (ret)
                        {
                            try
                            {
                                data = ret[0];
                                if ("customfields" in data)
                                {
                                    _.each(data.customfields, function (fld)
                                    {
                                        data["customfields_" + fld.shortname] = fld.value;
                                    });
                                }
                                if('customfields_videolimit' in data) {
                                  data['videolimit'] = parseInt(data["customfields_videolimit"]) * 1024; //MB
                                }    

                                // create easily readable list of course ID's where enrolled
                                data.enrolledcourseslist = new Array();
                                if (typeof data.enrolledcourses !== 'undefined') {
                                    for (var i = 0; i < data.enrolledcourses.length; i++) {
                                        data.enrolledcourseslist.push(data.enrolledcourses[i].id);
                                    }
                                }

                                model.set(data);

                                // XXX: this field sometimes comes empty after
                                // signup.
                                if (!data.fullname)
                                {
                                    model.fetch();
                                }
                            }
                            catch (err)
                            {
                                console.warn("Invalid data from local_monorailservices_get_users_by_id");
                            }

                            haveInfo = true;

                            if (haveLangs)
                            {
                                model.trigger("updated");
                                moment.locale(_User.get("lang"));
                            }
                        }, this);
                    break;

                case "update":                	
                    origData = model.toJSON();

                    data = _.pick(origData,
                        "id",
                        "firstname",
                        "lastname",
                        "city",
                        "country",
                        "skype",
                        "interests",
                        "lang",
                        "tutorial",
                        "notificationsettings",
                        "timezone",
                        "maildisplay",
                        "description",
                        "institution");

                    data.customfields = [ ];

                    for (var i in origData)
                    {
                        if (i.substr(0, 13) == "customfields_")
                        {
                            data.customfields.push({ type: "text", shortname: i.substr(13), name: i.substr(13), value: origData[i] });
                        }
                    }

                    var thisThis = this;

                    SC.call("local_monorailservices_update_users", { users: [ data ] },
                        function (ret)
                        {
                    		if (model.get('updateTutorial') || options.silent) {
                    			// Just update tutorial, not fetch new database
                    			model.set('updateTutorial', false);
                    		}
                    		else {
                    			model.fetch();
                    		}
                        }, this);

                    break;

                default:
                    console.warn("Unhandled method in user sync: " + method);
            }
        },

        hasFeature: function (name)
        {
            switch (name)
            {
                case "picture": case "sound": case "video":
                    return sUsers.indexOf(parseInt(this.get("id"))) != -1;

                default:
                    return true;
            }
        },

        validate: function (attr, opts)
        {
            if ("fullname" in attr)
            {
                if (!attr.fullname) return str.fill_missing_fields;
                if (attr.fullname.length > 100) return str.error_limit;
            }

            if (typeof attr.city == "string")
            {
                if (attr.city.length > 100) return str.error_limit;
            }

            if (typeof attr.skype == "string")
            {
                if (attr.skype.length > 50) return str.error_limit;
            }

            if (typeof attr.institution == "string")
            {
                if (attr.institution.length > 40) return str.error_limit;
            }

            if (typeof attr.description == "string")
            {
                if (attr.description.length > 1000) return str.error_limit;
            }

            var links = ["facebook", "twitter", "linkedin", "vk", "googleplus", "weibo", "pinterest", "instagram", "web"];

            for (var i=links.length-1; i>=0; i--)
            {
                if (typeof attr["customfields_" + links[i]] == "string")
                {
                    if (attr["customfields_" + links[i]].length > 200)
                    {
                        return str.error_limit;
                    }
                }
            }
        },
        
        picurl: function(size) {
            switch (size) {
                case 'x':
                    var s = '512';
                    break;
                case 'l':
                    var s = '185';
                    break;
                case 'm':
                    var s = '90';
                    break;
                case 's':
                    var s = '32';
                    break;
            }
            return MoodleDir+'public_images/user_picture/'+this.get("profilepichash")+'_'+s+'.jpg';
        },

        defaults: {
            id: undefined,
            username: undefined,
            firstname: undefined,
            lastname: undefined,
            fullname: undefined,
            email: undefined,
            skype: undefined,
            institution: undefined,
            fcid: undefined,
            interests: undefined,
            firstaccess: undefined,
            lastaccess: undefined,
            lang: undefined,
            timezone: undefined,
            timeoffset: undefined,
            description: undefined,
            videousage: undefined,
            videolimit: undefined,
            city: undefined,
            url: undefined,
            country: undefined,
            sessionkey: undefined,
            profilepichash: "default",
            tutorial: undefined,
            notificationsettings: undefined,
            customfields: { },
            preferences: undefined,
            enrolledcourses: undefined,
            maildisplay: 1, 
            cohorts: undefined,
            skills: [],
        },
        
        toJSON: function () {
            var json = _.clone(this.attributes);
            json.picurl = {
                x: this.picurl('x'),
                l: this.picurl('l'),
                m: this.picurl('m'),
                s: this.picurl('s'),
            }
            return json;
        },
	}));
});
