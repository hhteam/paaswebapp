/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "backbone"], function (SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "create":
                    SC.call("local_monorailservices_upd_forum_post", { posts: [ model.pick("discussion", "parent", "subject", "message", "new_attachments") ] }, function (res)
                    {
                        if (res instanceof Object && res.posts instanceof Array && res.posts.length > 0)
                        {
                            model.set({ id: res.posts[0].id });

                            options.success(method, model, options);
                        }
                    }, this, { deferred: options.deferred });
                    break;

                case "update":
                    SC.call("local_monorailservices_upd_forum_post", { posts: [ model.pick("id", "subject", "message", "new_attachments", "deleted_attachments") ] }, function (res)
                    {
                        if (res instanceof Object && res.posts instanceof Array && res.posts.length > 0)
                        {
                            options.success(method, model, options);
                        }
                    }, this, { deferred: options.deferred });
                    break;

                case "delete":
                    SC.call("local_monorailservices_del_fpost", { postid: model.get("id"), forumid: model.get("forum") }, function ()
                    {
                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });
                    break;

                default:
                    console.warn("Unimplemented post model method: " + method);
            }
        },

        defaults: {
            subject: "",
            message: ""
        }
    });
});
