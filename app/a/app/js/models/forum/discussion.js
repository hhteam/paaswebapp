/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "app/collections/forum/posts", "app/tools/servicecalls"], function (str, PostCollection, SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    console.warn("CREATING FAKE DISCUSSION!");
                    model.get("posts").setDiscussionId(model.get("id")).reset();
                    options.success(method, model, options);
                    break;

                case "update":
                    SC.call("local_monorailservices_upd_forum_discussion", { discussions: [ model.pick("id", "forum", "course", "name") ] }, function (res)
                    {
                        if (res instanceof Object && res.discussions instanceof Array && res.discussions.length > 0)
                        {
                            // Discussion message is actually first post.
                            model.get("posts").at(0).save({ message: model.get("message"), new_attachments: model.get("new_attachments"), deleted_attachments: model.get("deleted_attachments") }, { success: function ()
                            {
                                options.success(method, model, options);
                            }});
                        }
                    }, this, { deferred: options.deferred });
                    break;

                case "create":
                    SC.call("local_monorailservices_upd_forum_discussion", { discussions: [ model.pick("forum", "course", "name", "message", "new_attachments") ] }, function (res)
                    {
                        if (res instanceof Object && res.discussions instanceof Array && res.discussions.length > 0)
                        {
                            model.set({ id: res.discussions[0].id });

                            options.success(method, model, options);
                        }
                    }, this, { deferred: options.deferred });
                    break;

                case "delete":
                    SC.call("local_monorailservices_del_fdiscussion", { discussionid: model.get("id"), forumid: model.get("forum") }, function ()
                    {
                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });
                    break;

                default:
                    console.warn("Unimplemented discussion model method: " + method);
            }
        },

        initialize: function (attrib, options)
        {
            this.attributes.posts.setDiscussionId(attrib.id);
        },

        validate: function (attrs, options)
        {
            if ("name" in attrs)
            {
                if (!attrs.name)
                {
                    return str.fill_missing_fields;
                }

                if (attrs.name.length > 100)
                {
                    return str.error_limit;
                }
            }
        },

        defaults: function ()
        {
            return {
                posts: new PostCollection(),
                name: "",
                message: ""
            };
        }
    });
});
