/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "app/tools/servicecalls", "backbone"], function (str, SC)
{
    return Backbone.Model.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "update":
                    SC.call("local_monorailservices_update_forums", { forums: [ model.pick("id", "name", "intro") ] }, function (res)
                    {
                        if (res instanceof Object && res.forum instanceof Array && res.forum.length > 0)
                        {
                            options.success(method, model, options);
                        }
                    }, this, { deferred: options.deferred });
                    break;

                case "create":
                    SC.call("local_monorailservices_create_forums", { forums: [ model.pick("name", "intro", "sectionid", "course") ] }, function (res)
                    {
                        if (res instanceof Object && res.forum instanceof Array && res.forum.length > 0)
                        {
                            model.set({ id: res.forum[0].forumid });

                            options.success(method, model, options);
                        }
                    }, this, { deferred: options.deferred });
                    break;

                case "delete":
                    SC.call("local_monorailservices_del_forums", { forums: [ { forumid: model.get("id") } ] }, function ()
                    {
                        options.success(method, model, options);
                    }, this, { deferred: options.deferred });
                    break;

                default:
                    console.warn("Unimplemented forum model method: " + method);
            }
        },

        validate: function (attrs, options)
        {
            if ("name" in attrs)
            {
                if (!attrs.name)
                {
                    return str.fill_missing_fields;
                }

                if (attrs.name.length > 50)
                {
                    return str.error_limit
                }
            }

            if ("intro" in attrs)
            {
                if (!attrs.intro)
                {
                    return str.fill_missing_fields;
                }

                if (attrs.intro.length > 200)
                {
                    return str.error_limit;
                }
            }
        },

        defaults: {
            name: "",
            intro: ""
        }
    });
});
