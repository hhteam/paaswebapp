/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/base.html", "i18n!nls/strings", "app/views/dialogs/yes_no", "app/views/pages/task", "app/views/pages/quiz",
        "app/models/course/course", "app/views/pages/courseimport", "app/views/pages/coursereuse", "app/views/pages/courses",
        "app/views/pages/course", "app/views/pages/settings/profile", "app/views/pages/settings/settings", "app/views/pages/settings/analytics",
        "app/models/invitation", "app/views/pages/org_catalog", "app/views/pages/catalog", "app/views/header/header", "app/views/pages/admin/user_directory",
        "app/views/pages/admin/organization_details", "app/views/pages/admin/organization_course_details", "app/views/pages/admin/course_manager",
        "app/tools/dataloader", "app/collections/profiles", "app/views/pages/invite", "app/views/dialogs/messagebox",
        "app/tools/tracker", "app/views/pages/admin/analytics", "app/views/pages/calendar", "app/collections/course/coursesshort",
        "app/views/pages/admin/plan_details", "app/views/pages/admin/payment_method", "app/views/pages/admin/prepaid_licences",
        "app/views/pages/admin/invoices", "app/views/pages/admin/invoice", "app/views/pages/proupgrade", "app/views/pages/reuse/public_catalog",
        "app/views/pages/course/preview", "app/views/pages/task/preview", "app/collections/course/materials", "app/collections/empty",
        "app/views/pages/admin/public_page", "app/views/pages/task/quiz_run", "moment", "googleanalytics"],

    function (baseTpl, str, YesNoDialog, TaskPage, QuizPage, CourseModel, CourseImportPage, CourseReuseSelectorPage, CoursesPage, CoursePage, ProfilePage,
              SettingsPage, AnalyticsPage, InvitationModel, OrgCatalogPage, CatalogPage, HeaderView, UserDirectoryPage, OrgDetailsPage, OrgCourseDetailsPage,
              CourseManagerPage, DataLoader, ProfileCollection, InvitePage, MessageBox, Tracker, OrganizationAnalytics,
              CalendarPage, CourseShortCollection, PlanDetailsPage, PaymentMethodPage, PrepaidLicencesPage, InvoiceListPage, InvoicePage,
              ProUpgradePage, ReusePublicCatalogPage, CoursePreviewPage, TaskPreviewPage, CourseMaterialCollection, EmptyCollection,
              PublicPagePage, QuizRunPage, moment)
{
    var sCurrentPage = null,
        sCurrentHeader = null,
        sHaveBase = false,
        sConfirmLeave = null,
        sLeaveAcceptHandler = null,
        sLeaveRejectHandler = null,
        sLastUrl = null,
        sNewUrl = null,
        sCountryCode = null;

    window.onbeforeunload = function ()
    {
        Tracker.track("closewindow");
        Tracker.flush();

        if (sConfirmLeave !== null) {
            return sConfirmLeave;
        }
        if (window.coursePageWindowResizeEvent) {
            window.off('resize', window.adjustShortnameFieldHandler);
            window.adjustShortnameFieldHandler = null;
            window.coursePageWindowResizeEvent = false;
        }
    };

	return new (Backbone.Router.extend(
	{
        initialize: function ()
        {
            return this.bind('all', this._trackPageview);
        },

        _trackPageview: function (evname)
        {
            if (evname == "route")
            {
                var url = Backbone.history.getFragment();

                try {
                    ga('send', 'pageview', RootDir + url);
                }
                catch (err) { }
            }

            return this;
        },

        template: _.template(baseTpl),

        showPage: function (page)
        {
            $("#page-warnings-container").empty();

            if (sConfirmLeave)
            {
                if (sLastUrl)
                {
                    sNewUrl = window.location.href;
                    this.navigate(sLastUrl.substr(RootDir.length + sLastUrl.indexOf(RootDir)), { trigger: false });
                }
                else
                {
                    sNewUrl = null;
                }

                (new YesNoDialog({
                    message: sConfirmLeave,
                    context: this,
                    button_primary: str.button_cancel_editing,
                    button_secondary: str.button_save_changes,
                    accepted: function ()
                    {
                        // Don't save, go immediately
                        if (sLeaveRejectHandler instanceof Function)
                        {
                            sLeaveRejectHandler();
                        }

                        if (sNewUrl)
                        {
                            this.navigate(sNewUrl.substr(RootDir.length + sNewUrl.indexOf(RootDir)), { trigger: false });
                        }

                        sConfirmLeave = null;
                        this.doShowPage(page);
                    },
                    rejected: function ()
                    {
                        // Save and then go
                        var thisThis = this;

                        if (sLeaveAcceptHandler instanceof Function)
                        {
                            $.when(sLeaveAcceptHandler()).then(function ()
                            {
                                // Save didn't fail, can go
                                sConfirmLeave = null;

                                if (sNewUrl)
                                {
                                    thisThis.navigate(sNewUrl.substr(RootDir.length + sNewUrl.indexOf(RootDir)), { trigger: false });
                                }

                                thisThis.doShowPage(page);
                            },
                            function ()
                            {
                                // stay on page
                                if (!sNewUrl)
                                {
                                    thisThis.navigate(sLastUrl.substr(RootDir.length + sLastUrl.indexOf(RootDir)), { trigger: false });
                                }
                            });
                        }
                        else
                        {
                            // stay on page
                            if (!sNewUrl)
                            {
                                this.navigate(sLastUrl.substr(RootDir.length + sLastUrl.indexOf(RootDir)), { trigger: false });
                            }
                        }
                    }})).render();
            }
            else
            {
                this.doShowPage(page);
            }
        },

        doShowPage: function (page)
        {
            if (sCurrentPage && sCurrentPage.hide instanceof Function)
            {
                sCurrentPage.hide();
            }

            if (sCurrentPage && sCurrentPage.undelegateEvents instanceof Function)
            {
                sCurrentPage.undelegateEvents();
            }

            if (_User.isNew() && !page.anonymousAllowed)
            {
                // store where user is to totalstorage
                localStorage.setItem('login.wants-url', document.location.href);
                window.location = LoginUrl;

                return;
            }

            sCurrentPage = page;

            if (page.nobase)
            {
                sHaveBase = false;
                sCurrentHeader = null;
            }
            else
            {
                if (!sHaveBase)
                {
                    // remove non colorbox stuff under body
                    $('body > div').each(function() {
                        if ($(this).attr('id') != 'cboxOverlay' && $(this).attr('id') != 'colorbox') {
                            $(this).remove();
                        }
                    });
                    // add base template
                    $(document.body).append(this.template({ str: str, lang: _User.get('lang'), user: _User.toJSON() }));
                    sHaveBase = true;

                    if (sCountryCode)
                    {
                        if (sCountryCode == "CN")
                        {
                            $("#footer-follow").hide();
                        }
                    }
                    else
                    {
                        $.ajax({ url: EliademyUrl + "/ipcheck/code.php", success: function (code)
                        {
                            sCountryCode = code;

                            if (code == "CN")
                            {
                                $("#footer-follow").hide();
                            }
                        }});
                    }
                }
            }

            if (page.$el)
            {
                page.setElement(page.$el.selector);
            }

            if (page.show instanceof Function)
            {
                page.show();
            }
            else
            {
                page.render();
            }

            if (!page.nobase)
            {
                var header = (page.customHeader === undefined ? HeaderView :
                    (page.customHeader instanceof Function ? page.customHeader() : page.customHeader ));

                if (sCurrentHeader != header)
                {
                    sCurrentHeader = header;
                    header.setElement(header.$el.selector);
                    header.render();
                }

                // Highlight top menu according to current view (or course by default).
                $("#mainmenu .menubutton").removeClass("active");
                $("#bt-" + (page.menuHighlight !== undefined ? page.menuHighlight : "course")).addClass("active");
            }

            if (page.pageTitle)
            {
                document.title = _.unescape(page.pageTitle instanceof Function ? page.pageTitle() : page.pageTitle).replace("&nbsp;", " ");
            }
            else
            {
                document.title = _.unescape(str.site_name).replace("&nbsp;", " ");
            }

            window.scrollTo(0, 0);
            sLastUrl = window.location.href;
        },

        setConfirmLeave: function (message, acceptHandler, rejectHandler)
        {
            sConfirmLeave = message;
            sLeaveAcceptHandler = acceptHandler;
            sLeaveRejectHandler = rejectHandler;
        },

        getConfirmLeave: function () {
            return sConfirmLeave;
        },

        countryCode: function ()
        {
            return sCountryCode;
        },

        routes: {
            "": "courses",
            "completed": "completedCourses",
            "teaching": "teachingCourses",
            "teaching/completed": "teachingCompletedCourses",
            "tasks/new/:courseid": "tasknew",
            "tasks/new/:courseid/:sectionid": "tasknew",
            "tasks/:id": "task",
            "tasks/:id/:subid": "taskSubid",
            "quizzes/new/:courseid": "quiznew",
            "quizzes/new/:courseid/:sectionid": "quiznew",
            "quizzes/:id": "quiz",
            "quizzes/:id/:subid": "quizSubid",
            "quiz-take/:id": "quizRun",
            "courses/wizard": "coursewizard",
            "courses/import": "courseimport",
            "courses/reuse": "coursereuse",
            "reuse-public-course": "reusePublicCourse",
            "reuse-preview/:code": "reusePreview",
            "reuse-course/:code": "reuseCourse",
            "reuse-preview-task/:instance": "reusePreviewTask",
            "certificate/:code": "certificate",
            "courses/:id": "course",
            "courses/:id/:subpage": "course",
            "courses/:id/:subpage/:subsubpage": "course",
            "courses/:id/:subpage/:subsubpage/:subsubsubpage": "course",
            "profile": "profile",
            "profile/:id": "profile",
            "settings": "settings",
            "analytics": "analytics",
            "invitation/:code": "invitation",
            "invitation/:code/:email/:action": "invitation",
            "invitation/:code/:email/:action/:token": "invitation",
            "organization-details": "orgDetails",
            "organization-course-details/:code": "orgCourseDetails",
            "go-premium": "orgUpgrade",
            "user-directory": "userDirectory",
            "user-directory/:page": "userDirectory",
            "course-manager": "courseManager",
            //"organization-skills": "organizationSkills",
            "organization-analytics": "organizationAnalytics",
            "plan-details": "proPlanDetails",
            "payment-method": "proPaymentMethod",
            "public-page": "proPublicPage",
            "invoices": "proInvoiceList",
            "invoices/:id": "proInvoiceView",
            "invoices/:id/print": "proInvoicePrint",
            //"offer-scanlang": "prepaidLicences",
            //"offer-scanlang/:code": "prepaidLicences",
            "offer-annual": "prepaidLicences",
            "offer-annual/:code": "prepaidLicences",
            "catalog": "catalog",
            "org/:id": "orgCatalog",
            "calendar": "calendar",
            "calendar/:date": "calendar",
            "!": "ignoreroute",
            "*any": "other"
        },

		task: function (id)
		{
            TaskPage.instanceid = parseInt(id);
            TaskPage.model = null;
            TaskPage.isnew = false;
            TaskPage.subid = undefined;

            this.showPage(TaskPage);
		},

        tasknew: function (coursecode, sectionid)
		{
            if (!sectionid) {
                sectionid = null;
            }

            TaskPage.id = null;
            TaskPage.model = null;
            TaskPage.coursecode = coursecode;
            TaskPage.sectionid = sectionid;
            TaskPage.isnew = true;
            TaskPage.subid = undefined;

            this.showPage(TaskPage);
		},

        taskSubid: function (id, subid)
        {
            TaskPage.instanceid = parseInt(id);
            TaskPage.model = null;
            TaskPage.isnew = false;
            TaskPage.subid = parseInt(subid);

            this.showPage(TaskPage);
        },

		quiz: function (id) {
            QuizPage.instanceid = parseInt(id);
            QuizPage.model = null;
            QuizPage.isnew = false;
            QuizPage.subid = undefined;

            this.showPage(QuizPage);
		},

        quiznew: function (coursecode, sectionid) {
            if (!sectionid) {
                sectionid = null;
            }

            QuizPage.id = null;
            QuizPage.model = null;
            QuizPage.coursecode = coursecode;
            QuizPage.sectionid = sectionid;
            QuizPage.isnew = true;
            QuizPage.subid = undefined;

            this.showPage(QuizPage);
		},

        quizSubid: function (id, subid) {
            QuizPage.instanceid = parseInt(id);
            QuizPage.model = null;
            QuizPage.isnew = false;
            QuizPage.subid = parseInt(subid);

            this.showPage(QuizPage);
        },

        quizRun: function (id) {
            QuizRunPage.instanceid = parseInt(id);

            this.showPage(QuizRunPage);
        },

        coursewizard: function ()
		{
            CoursePage.model = new CourseModel({ wizard_mode: 1 });
            CoursePage.model.get("sections").add([ { name: str.overview_content, section: 0 } ]);
            CoursePage.currentsub = 'settings';
            CoursePage.model.set({ "can_edit": true, "teacher_name": _User.get("fullname") });
            CoursePage.want_edit_mode = true;
            this.showPage(CoursePage);
		},

        courseimport: function ()
        {
            this.showPage(CourseImportPage);
        },

        coursereuse: function ()
        {
            this.showPage(CourseReuseSelectorPage);
        },

        reusePublicCourse: function ()
        {
            this.showPage(ReusePublicCatalogPage);
        },

        reusePreview: function (code)
        {
            CoursePreviewPage.code = code;
            CoursePreviewPage.action = "reuse";

            this.showPage(CoursePreviewPage);
        },

        reuseCourse: function (code)
        {
            DataLoader.exec({ collection: CourseMaterialCollection, context: this, where: { code: code } }, function (course)
            {
                CoursePage.model = course;

                CoursePage.model.set({
                    can_edit: 1,
                    wizard_mode: 3,
                    participants: EmptyCollection,
                    skills: EmptyCollection,
                    startdate: parseInt(course.get("startdate")) * 1000,
                    enddate: parseInt(course.get("enddate")) * 1000
                });

                CoursePage.currentsub = 'settings';
                CoursePage.want_edit_mode = true;

                this.showPage(CoursePage);
            });
        },

        reusePreviewTask: function (instance)
        {
            TaskPreviewPage.instance = instance;

            this.showPage(TaskPreviewPage);
        },

		courses: function ()
		{
            // When opening home page, check if user wants to be redirected
            // somewhere else. Old login page, which used to do this, is
            // obsoleted by the website.
            var wantsUrl = localStorage.getItem("login.wants-url");

            if (wantsUrl && wantsUrl.indexOf(document.location.protocol+'//'+document.domain) == 0)
            {
                localStorage.removeItem('login.wants-url');

                if (wantsUrl.indexOf(document.location.protocol+'//'+document.domain+RootDir) == 0)
                {
                    // Redirecting within the app.
                    var wantWhere = wantsUrl.replace(document.location.protocol+'//'+document.domain+RootDir, '/');

                    if (wantWhere != "/")
                    {
                        // Don't re-navigate to the same page.
                        this.navigate(wantWhere, { trigger: true });
                        return;
                    }
                }
                else
                {
                    // Redirecting outside the app (same domain/protocol)
                    window.location.href = wantsUrl;
                }
            }

            CoursesPage.collection = CourseShortCollection.LearningOngoing;

            this.showPage(CoursesPage);
		},

        completedCourses: function ()
        {
            CoursesPage.collection = CourseShortCollection.LearningCompleted;

            this.showPage(CoursesPage);
        },

        teachingCourses: function ()
        {
            CoursesPage.collection = CourseShortCollection.TeachingOngoing;

            this.showPage(CoursesPage);
        },

        teachingCompletedCourses: function ()
        {
            CoursesPage.collection = CourseShortCollection.TeachingCompleted;

            this.showPage(CoursesPage);
        },

        course: function (id, subpage, subsubpage, subsubsubpage)
        {
            CoursePage.tutorial = false;

            var show = function ()
            {
              if (CoursePage.code != id) {
                CoursePage.model = null;
                CoursePage.code = id;
              }

              if (typeof subpage === 'undefined' || ! subpage) {
                subpage = 'overview';
              }

              CoursePage.currentsub = subpage;
              CoursePage.currentsubsub = subsubpage;
              CoursePage.currentsubsubsub = subsubsubpage;

              this.showPage(CoursePage);
            };

            if (_User.isNew()) {
              //User may be logged in or a new user
              $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_is_course_public.php?code="+id, context: this, dataType: "json",
                 success: function (data)
                 {
                    if(!data.logged && data.public) {
                      window.location = EliademyUrl+'/'+id;
                    } else {
                     show.call(this);
                    }

                 }, error: function ()
                 {
                     show.call(this);
                 }});
            } else {
                show.call(this);
            }
        },

        profile: function (id)
        {
            var show = function ()
            {
                if (!id  || id == "edit")
                {
                    // Showing profile of current user.
                    ProfilePage.model = _User;
                    ProfilePage.editMode = id == "edit";
                    this.showPage(ProfilePage);
                }
                else if (id == _User.get("id"))
                {
                    this.navigate("/profile", { trigger: true, replace: true });
                }
                else
                {
                    // Showing profile of some other user.
                    DataLoader.exec({ context: this, collection: ProfileCollection, id: id }, function (model)
                    {
                        ProfilePage.model = model;
                        ProfilePage.editMode = false;
                        this.showPage(ProfilePage);
                    });
                }
            };

            if (_User.get("sessionkey"))
            {
                // User model loaded, can show profile
                show.call(this);
            }
            else
            {
                // Will show profile when user model loads...
                _User.once("updated", show, this);
            }
        },

        settings: function ()
        {
            this.showPage(SettingsPage);
        },

        analytics: function ()
        {
            this.showPage(AnalyticsPage);
        },

	invitation: function(code,email,action,token) {
	  var processInvite = function ()
	  {
	    var invite = new InvitationModel({id:code});;
            _User.off("updated", processInvite);

	    invite.fetch({ success: function (method, model, options)
	      {
	      invite.once("enrolldone", function() {

          try {
            ga('send', 'enroll-all', code);
          }
          catch (err) { }

	        if(invite.get("inviteType") == 'cohort') {
	          _User.set({ cohorts: invite.get("cohortId"), orgname: invite.get("cohortName") }, { silent: true });
                  _Router.navigate('/', { trigger: true });
                  location.reload();
	        } else if(invite.get("inviteType") == 'course') {
	          window.location = RootDir + "courses/" + invite.get("courseCode");
	        }
	        _User.trigger("updated");
	      });
	      invite.once("enrollerror", function() {
	        (new MessageBox({ message: str.invite_expired, backdrop: false,
	          rejected: function (){_User.trigger("updated");_Router.navigate('/', { trigger: true });},
	          accepted: function (){_User.trigger("updated");_Router.navigate('/', { trigger: true });}})).render();
	      });
              if(invite.get("inviteType") == 'course' && action == "enroll") {
                 invite.doEnrollment(invite,token,email);
              } else {
	         invite.doEnrollment(invite);
              }
	      }, error: function (method, model, options) {
	        (new MessageBox({ message: str.invite_expired, backdrop: false,
	          rejected: function (){_User.trigger("updated");_Router.navigate('/', { trigger: true });},
	          accepted: function (){_User.trigger("updated");_Router.navigate('/', { trigger: true });}})).render();
	      }});
	  };
	  if (_User.get("sessionkey"))
	  {
	    // User model loaded, can show profile
            this.showPage(InvitePage);
	    processInvite.call(this);
	  }
	  else
	  {
	    //Check first if user is logged in if yes
	    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_is_course_public.php", context: this, dataType: "json",
	      success: function (data)
	      {
	        if(data.logged) {
	          _User.once("updated", processInvite, this);
                  this.showPage(InvitePage);
        	} else {
	          localStorage.setItem('login.wants-url', document.location.href);
              if (action == "signup")
              {
                  window.location = EliademyUrl + "/signup?email=" + email;
              }
              else
              {
		          window.location = LoginUrl;
              }
        	}
	      }, error: function ()
	      {
	        localStorage.setItem('login.wants-url', document.location.href);
	        window.location = LoginUrl;
	      }});
	  }
	},

        orgUpgrade: function ()
        {
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                cohortid = _User.get("cohorts");

                if (typeof cohortid == "string" && cohortid.indexOf(",") != -1) {
                    cohortid = cohortid.split(",")[0];
                }
            }

            if (cohortid) {
                // User already has a cohort, nothing to see here
                // TODO: show some message maybe?
                window.history.back();
                return;
            }

            this.showPage(ProUpgradePage);
        },

        orgDetails: function ()
        {
            var show = function ()
            {
                this.showPage(OrgDetailsPage);
            };
            if (_User.get("sessionkey"))
            {
                // User model loaded, can show org details page
                show.call(this);
            }
            else
            {
                // Will show org details when user model loads...
                _User.once("updated", show, this);
            }
        },

        orgCourseDetails: function (code)
        {
            if (OrgCourseDetailsPage.code != code) {
                OrgCourseDetailsPage.model = null;
                OrgCourseDetailsPage.code = code;
            }

            this.showPage(OrgCourseDetailsPage);
        },

        userDirectory: function ()
        {
            this.showPage(UserDirectoryPage);
        },

        courseManager: function ()
        {
            this.showPage(CourseManagerPage);
        },

        organizationAnalytics: function ()
        {
            this.showPage(OrganizationAnalytics);
        },

        proPlanDetails: function ()
        {
            this.showPage(PlanDetailsPage);
        },

        proPaymentMethod: function ()
        {
            this.showPage(PaymentMethodPage);
        },

        proPublicPage: function ()
        {
            this.showPage(PublicPagePage);
        },

        proInvoiceList: function () {
            this.showPage(InvoiceListPage);
        },

        proInvoiceView: function (id) {
            InvoicePage.id = id;
            this.showPage(InvoicePage);
        },

        proInvoicePrint: function (id) {
            InvoicePage.id = id;
            InvoicePage._print = true;
            this.showPage(InvoicePage);
        },

        prepaidLicences: function (code) {
            PrepaidLicencesPage.code = code;
            this.showPage(PrepaidLicencesPage);
        },

        catalog: function ()
        {
            this.showPage(CatalogPage);
        },

        orgCatalog: function (id) {
            OrgCatalogPage.CurrentId = id;
            HeaderView.CurrentCatalog = id;
            HeaderView.render();

            this.showPage(OrgCatalogPage);
        },

        calendar: function (date)
        {
            if (date)
            {
                CalendarPage.date = date;
            }
            else
            {
                CalendarPage.date = moment().format("YYYYMM");
            }

            this.showPage(CalendarPage);
        },

		other: function (any)
		{
            console.warn("Invalid url.");
            //this.navigate("/", { trigger: true });
		},

        ignoreroute: function (any)
        {
            console.warn("Ignoring route.");
            this.navigate("/", { trigger: true });
        },

        certificate: function (code) {
            this.navigate("/courses/" + code + "/certificate", { trigger: true });
        },

        currentPage: function ()
        {
            return sCurrentPage;
        }
	}));
});
