/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

requirejs.config(
{
	baseUrl: RootDir + "lib/js",
	urlArgs: "v=" + Version,

	paths:
	{
		app: "../../app/js",
		templates: "../../app/templates",
		nls: "../../app/nls",

		jquery: "jquery-1.10.2.min",
		underscore: "underscore-min",
		backbone: "backbone-min",
		bootstrap: "bootstrap.min",
        "jquery.ui.widget": "jquery.ui.widget-min",
        iframetransport: "jquery.iframe-transport",
        jqueryupload: "jquery.fileupload",
        jqueryflot: "jquery.flot",
        jqueryflotpie: "jquery.flot.pie",
        jqueryflottime: "jquery.flot.time",
        jqueryflotcategories: "jquery.flot.categories",
        jqueryflottooltip: "jquery.flot.tooltip",
        jqueryflotaxislabels: "jquery.flot.axislabels",
        jqueryflotstack: "jquery.flot.stack",
        jquerytimezone: "jquery.timezone-picker.min",
        jquerymaphilight: "jquery.maphilight.min",
        colorbox: "jquery.colorbox-min",
        imgplaceholder: "holder",
        datepicker: "bootstrap-datepicker",
        datetimepicker: "bootstrap-datetimepicker.min",
        oembed: "jquery.oembed.min",
        spin: "spin.min",
        googleanalytics: "googleanalytics",
        mixpanel: "mixpanel",
        scrollto: "jquery.scrollTo-1.4.3.1-min",
        jqueryui: "jquery-ui-1.10.2.custom.min",
        ckeditor: "../ckeditor/ckeditor",
        ckeditor_hljs: "../ckeditor/plugins/codesnippet/lib/highlight/highlight.pack",
        typeahead: "typeahead.min",
        progressbar: "bootstrap-progressbar.min",
        fastclick: "fastclick",
        filesaver: "filesaver",
        moment: "moment-with-locales",
        momenttimezone: "moment-timezone-with-data",
        magnificpopup: "magnificpopup.min",
        timepicker: "jquery.timepicker.min",
        datepair: "jquery.datepair.min",
        toggler: "bootstrap2-toggle.min"
	},

	shim:
	{
		underscore: { exports: "_" },
		backbone: { deps: ["jquery", "underscore"], exports: "Backbone" },
		bootstrap: { deps: ["jquery"] },
        colorbox: { deps: ["jquery"] },
        "jquery.ui.widget": { deps: ["jquery"] },
        iframetransport: { deps: ["jquery"] },
        jqueryupload: { deps: ["jquery", "jquery.ui.widget", "iframetransport"] },
        jqueryflot: { deps: ["jquery"] },
        jqueryflotpie: { deps: ["jqueryflot"] },
        jqueryflottime: { deps: ["jqueryflot"] },
        jqueryflotcategories: { deps: ["jqueryflot"] },
        jqueryflottooltip: { deps: ["jqueryflot"] },
        jqueryflotaxislabels: { deps: ["jqueryflot"] },
        jqueryflotstack: { deps: ["jqueryflot"] },
        oembed: { deps: ["jquery"] },
        scrollto: { deps: ["jquery"] },
        jqueryui: { deps: ["jquery"] },
        typeahead: { deps: ["jquery"] },
        datepicker: { deps: ["jquery"] },
        datetimepicker: { deps: ["jquery"] },
        progressbar: { deps: ["jquery"] },
        ckeditor: { exports: "CKEDITOR" },
        ckeditor_hljs: { exports: "hljs" },
        magnificpopup: { deps: ["jquery"] },
        timepicker: { deps: ["jquery"] },
        datepair: { deps: ["jquery"] },
        jquerytimezone: { deps: ["jquery"] },
        jquerymaphilight: { deps: ["jquery"] },
        toggler: { deps: ["jquery"] }
	},

        config: {
        //Set the config for the i18n
        //module ID
        i18n: {
            locale: localStorage.getItem('locale') || 'en'
          }
        },
    deps: [ "bootstrap", "toggler", "mixpanel" ]
});

require(["app/models/user", "app/tools/servicecalls", "fastclick", "app/router", "app/tools/uploader", "app/models/admin/cohort",
         "app/models/admin/cohortadmin", "app/models/invitation", "app/views/dialogs/messagebox", "app/collections/feed",
         "app/views/notification", "app/tools/ajaxprogress", "domReady!"],

    function (UserModel, SC, FastClick, Router, Uploader, CohortModel, CohortAdminModel, InvitationModel, MessageBox,
        FeedCollection, NotificationView, AjaxProgress)
{
    _User = UserModel;
    _Router = Router;
    _Uploader = Uploader;
    _NotificationView = new NotificationView;

    AjaxProgress.init();

    if (localStorage.getItem("e4b_orgname"))
    {
        // New business user, new cohort.

        var createCohort = function ()
        {
            var cohort = new CohortModel({ name: localStorage.getItem("e4b_orgname") });

            cohort.save(undefined, { success: function (model, response, options)
            {
                var admin = new CohortAdminModel({ userid: UserModel.get("id"), cohortid: model.get("id") });

                admin.save(undefined, { success: function (model, response, options)
                {
                    localStorage.removeItem("e4b_orgname");

                    UserModel.set({ institution: cohort.get("name") }, { silent: true });
                    UserModel.set({ cohortid: cohort.get("id") }, { silent: true });
                    UserModel.set({ adminCohort: cohort.get("id") }, { silent: true });
                    UserModel.off("updated", createCohort);
                    UserModel.trigger("updated");

                    _Router.navigate("/catalog", { trigger: true });
                }});
            }});
        };

        UserModel.on("updated", createCohort);
    }
    else
    {
        // Normal login.
    }

    $(document.body).on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });
    $(document.body).tooltip({ container: "body", selector: "*[data-toggle='tooltip']" });

    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_get_token.php", context: this, dataType: "json",
        success: function (data)
        {
            if (data instanceof Object && data.token)
            {
                SC.setToken(data.token);
                SC.setSesskey(data.sesskey);
                UserModel.set({ id:            data.userid,
                                lang:          data.lang,
                                adminCohort:   data.acid,
                                cohorts:       data.ucids,
                                trial_expired: data.expired,
                                admin_msg:     data.adminMsg,
                                isPaas:        data.isPaas,
                                auth:          data.auth },
                              { silent:        true });
                SC.createSocket(FeedCollection);
                // If lang comes empty, don't end up in reload loop
                if (typeof data.lang === 'undefined' || ! data.lang.length) {
                    data.lang = 'en';
                }
                requirejs.config({config: { i18n: { locale: data.lang }}});
                //Dynamic language change does not work with requireJS it has already loaded
                //the language strings at the startup/reload
                if(localStorage.getItem('locale') !== data.lang) {
                    localStorage.setItem('locale', data.lang);
                    location.reload();
                    return;
                }
                UserModel.on("updated", function() {
                    FeedCollection.fetch({ always: true })

                    //trying to identify user and pass some data
                    mixpanel.identify(data.userid);
                    mixpanel.people.set({
                        "$first_name": _User.get("firstname"),
                        "$last_name": _User.get("lastname"),
                        //"$email": data.email,
                        "$email": _User.get("email"),
                        "userid": data.userid,
                        //"fullname": _User.get("fullname"),
                        "locale": data.lang
                    });

                    if (!Backbone.History.started) {
                        Backbone.history.start({ pushState: true, root: RootDir });
                    }

                }, FeedCollection);
                UserModel.fetch();

            }
            else
            {
                if (!Backbone.History.started) {
                    Backbone.history.start({ pushState: true, root: RootDir });
                }
            }

        }, error: function ()
        {
            if (!Backbone.History.started) {
                Backbone.history.start({ pushState: true, root: RootDir });
            }
        }});

    try
    {
        addthis.init();
    }
    catch (err)
    { }

    FastClick.attach(document.body);
});
