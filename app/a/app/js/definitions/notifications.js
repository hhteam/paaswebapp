/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings"], function (str)
{
    // First matching message is shown.
    return [
        {
            type: "message",
            since: new Date("2015-07-20T01:00+03:00"),
            message: str.banner_donate +
                " <a href=\"https://eliademy.com/catalog/donate/support-with-5.html\" style=\"color: white;\">" + str.banner_donate_more + "</a>"
        },
        {   // Reminding users to update timezone.
            type: "action",
            since: new Date("2013-08-20T17:00+03:00"),
            condition: function ()
            {
                return !_User.get("timezonecustom");
            },
            message: str.banner_update_timezone +
                " <a style=\"color: white; text-decoration: underline;\" href=\"/settings\">"
                   + str.settings + "</a>"
        }
    ];
});
