/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

/* TODO: usage documentation for DataLoader */

define([], function ()
{
    var loadModelById = function (data, success, checkpage)
    {
        var model = data.collection.get(data.id);

        if (model)
        {
            if (!checkpage || checkpage == window.location.href)
            {
                success.call(data.context ? data.context : this, model);
            }
        }
        else
        {
            if (data.collection.model._noRead === true)
            {
                data.collection.once("reset", function ()
                {
                    var model = data.collection.get(data.id);

                    if (model)
                    {
                        if (!checkpage || checkpage == window.location.href)
                        {
                            success.call(data.context ? data.context : this, model);
                        }
                    }
                    else if (data.error instanceof Function)
                    {
                        data.error.call(data.context ? data.context : this);
                    }
                }, this);

                data.collection.fetch();
            }
            else
            {
                model = new data.collection.model({ id: data.id });

                model.fetch({ success: function (mod, method, options)
                {
                    data.collection.add(model, { silent: true });

                    if (!checkpage || checkpage == window.location.href)
                    {
                        success.call(data.context ? data.context : this, model);
                    }
                }});
            }
        }
    };

    var loadModelByWhere = function (data, success, checkpage)
    {
        var model = data.collection.where(data.where)[0];

        if (model)
        {
            if (!checkpage || checkpage == window.location.href)
            {
                success.call(data.context ? data.context : this, model);
            }
        }
        else
        {
            if (data.collection.model._noRead === true)
            {
                data.collection.once("reset", function ()
                {
                    var model = data.collection.where(data.where)[0];

                    if (model)
                    {
                        if (!checkpage || checkpage == window.location.href)
                        {
                            success.call(data.context ? data.context : this, model);
                        }
                    }
                    else if (data.error instanceof Function)
                    {
                        data.error.call(data.context ? data.context : this);
                    }
                }, this);

                data.collection.fetch();
            }
            else
            {
                model = new data.collection.model(data.where);

                data.collection.add(model);

                model.fetch({ success: function (mod, method, options)
                {
                    if (!checkpage || checkpage == window.location.href)
                    {
                        success.call(data.context ? data.context : this, model);
                    }
                }});
            }
        }
    };

    var loadModelByAt = function (data, success, checkpage)
    {
        var model = data.collection.at(data.at);

        if (model)
        {
            if (!checkpage || checkpage == window.location.href)
            {
                success.call(data.context ? data.context : this, model);
            }
        }
        else
        {
            data.collection.once("reset", function ()
            {
                var model = data.collection.at(data.at);

                if (model)
                {
                    if (!checkpage || checkpage == window.location.href)
                    {
                        success.call(data.context ? data.context : this, model);
                    }
                }
                else if (data.error instanceof Function)
                {
                    data.error.call(data.context ? data.context : this);
                }
            }, this);

            data.collection.fetch();
        }
    };

    var loadCollection = function (data, success, checkpage)
    {
        var mustFetch = false,
            timestamp = (new Date()).getTime() / 1000;

        // is dirty overrides emptiness.
        if (data instanceof Object && data.collection instanceof Object)
        {
            if (data.collection.hasOwnProperty("_isDirty"))
            {
                if (data.collection._isDirty)
                {
                    mustFetch = true;
                }
            }
            else if (!data.collection.length)
            {
                mustFetch = true;
            }
        }

        // max age overrides all (when max age reached)
        if (!mustFetch && data.collection.hasOwnProperty("maxAge") && data.collection.hasOwnProperty("_lastFetch") &&
            timestamp - data.collection._lastFetch > data.collection.maxAge)
        {
            mustFetch = true;
        }

        if (mustFetch)
        {
            data.collection.once("reset", function ()
            {
                if (!checkpage || checkpage == window.location.href)
                {
                    success.call(data.context ? data.context : this, data.collection);
                }
            }, this);

            data.collection.fetch();
            data.collection._lastFetch = timestamp;
        }
        else
        {
            if (!checkpage || checkpage == window.location.href)
            {
                success.call(data.context ? data.context : this, data.collection);
            }
        }
    };

    var loadModel = function (data, success, checkpage)
    {
        if (!data.model.isNew())
        {
            if (!checkpage || checkpage == window.location.href)
            {
                success.call(data.context ? data.context : this, data.model);
            }
        }
        else
        {
            data.model.fetch({ success: function (mod, resp, options)
            {
                if (!checkpage || checkpage == window.location.href)
                {
                    success.call(data.context ? data.context : this, data.model);
                }
            }});
        }
    };

    return {
        exec: function (data, success)
        {
            if ("id" in data)
            {
                loadModelById(data, success, data.anypage ? null : window.location.href);
            }
            else if ("where" in data)
            {
                loadModelByWhere(data, success, data.anypage ? null : window.location.href);
            }
            else if ("at" in data)
            {
                loadModelByAt(data, success, data.anypage ? null : window.location.href);
            }
            else if ("model" in data)
            {
                loadModel(data, success, data.anypage ? null : window.location.href);
            }
            else
            {
                loadCollection(data, success, data.anypage ? null : window.location.href);
            }
        }
    };
});
