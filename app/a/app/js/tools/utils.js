/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["momenttimezone", "magnificpopup"], function (moment)
{
    return {
        padLeft: function (txt, chr, len)
        {
            txt += "";

            var l = txt.length;

            while (l < len)
            {
                l++;
                txt = chr + txt;
            }

            return txt;
        },

        externalUrlData : function (url,attrib)
        {
            try
            {
                if (/^((http|https):\/\/)?(www\.)?youtu(\.be|be\.com)/.test(url)) {
                    // a youtube link id contain alphabet, digit, and -\_
                    try {
                        attrib.videoid = /v\=([a-zA-Z0-9\-_]+)/.exec(url)[1];
                    } catch (err) {
                        attrib.videoid = /youtu\.be\/([a-zA-Z0-9\-_]+)/.exec(url)[1];
                    }
                    attrib.type = "youtube";
                } else if (/^((http|https):\/\/)?(www\.)?slideshare\.net/.test(url)) {
                    // a slideshare link
                    attrib.type = "slideshare";
                    attrib.oembedurl = url;
                } else if (/^((http|https):\/\/)?(www\.)?vimeo\.com/.test(url)) {
                    // a vimeo link id should be
                    attrib.videoid = /.com\/(\d+)/.exec(url)[1];
                    attrib.type = "vimeo";
                }
            }
            catch (err)
            {
                console.warn("Invalid external url.");
            }

            return attrib;
        },

        videoThumbnailUrl : function(resid)
        {
            var url = vidUrl + '/p/' + vidPartnerId +'/sp/'+vidPartnerId+'00/thumbnail/entry_id/'+resid+'/version/0/width/480/height/360';
            return url;
        },

        //http://regexlib.com/Search.aspx?k=password
        validatePassword: function(password)
        {
            var re = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            return !re.test(password);
        },

        // http://stackoverflow.com/a/46181
        validateEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },

        validateUrl: function (url)
        {
            //source: http://www.dzone.com/snippets/validate-url-regexp
            var regexp = /((ftp|http|https):\/\/)?(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

            //source : http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.js
            //var regexp = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/
            return regexp.test(url);
         },

        // Small customization to function by user Scott Dowding @ http://stackoverflow.com/a/488073/1489738
        isScrolledIntoView: function (elem, offset) {
            if ($(elem).length) {
                var docViewTop = $(window).scrollTop();
                var docViewBottom = docViewTop + $(window).height();

                var elemTop = $(elem).offset().top + offset;
                var elemBottom = elemTop + 20;

                return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
            } else {
                return false;
            }
        },

        isEmpty: function (html)
        {
            var div = document.createElement("div");
            div.innerHTML = html;

            if (div.getElementsByTagName("img").length > 0 ||
                div.getElementsByTagName("iframe").length > 0 ||
                div.getElementsByTagName("audio").length > 0 ||
                div.getElementsByTagName("video").length > 0)
            {
                return false;
            }

            return $(div).text().replace(/^\s+|\s+$/g, '').length == 0;
        },

        /* Add:
         *
         *  "click [data-call]": Utils.callMapper
         *
         * in view events, and all elements with data-call="<method name>"
         * will be automatically connected to specified methods.
         *
         * Now with double click prevention!
         */
        callMapper: function (ev)
        {
            ev.preventDefault();

            var el = ev.target;

            while (el && !el.attributes["data-call"])
            {
                el = el.parentNode;
            }

            if (el && document.body.contains(el))
            {
                if ($(el).hasClass("disabled"))
                {
                    return;
                }

                var fun = this[el.attributes["data-call"].value];

                if (fun instanceof Function)
                {
                    if (el.hasAttribute("data-utils-lc"))
                    {
                        if (((new Date()).getTime() - el.attributes["data-utils-lc"].value) < 500)
                        {
                            return;
                        }
                    }

                    el.setAttribute("data-utils-lc", (new Date()).getTime());

                    fun.call(this, ev, el);
                }
                else
                {
                    // zombie views...
                    //console.warn("Method not found.");
                }
            }
        },

        // Show iframe overlay dialog
        showIframe: function (url)
        {
            var viewer = document.createElement("div");

            viewer.setAttribute("style", "background: #666; position: absolute; top: " + $(window).scrollTop() + "px; left: " +
                $(window).scrollLeft() + "px; width: " + window.innerWidth + "px; height: " + window.innerHeight + "px; z-index: 1000;");

            viewer.innerHTML = "<div style=\"text-align: right; line-height: 30px; padding-right: 20px;\">" +
                "<i class=\"fa fa-times\" style=\"font-size: 18px; color: #FFF; cursor: pointer;\"></i></div>" +
                "<iframe style=\"margin: 0px 0px 0px 20px; width: " + (window.innerWidth - 40) + "px; height: " +
                (window.innerHeight - 50) + "px; border: none;\" src=\"" + url + "\"></iframe>";

            document.body.style.overflow = "hidden";
            document.body.appendChild(viewer);

            viewer.querySelector("i").addEventListener("click", function (ev)
            {
                document.body.removeChild(viewer);
                document.body.style.overflow = "visible";
            });
        },

        mimeIcon: function (mimeType)
        {
            switch (mimeType)
            {
                case "application/pdf":
                    return RootDir + '/app/img/icon-pdf.png';

                case "application/vnd.ms-excel":
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return RootDir + '/app/img/icon-excel.png';

                case "application/vnd.oasis.opendocument.spreadsheet":
                    return RootDir + '/app/img/icon-spreadsheet.png';

                case "application/vnd.ms-powerpoint":
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                    return RootDir + '/app/img/icon-powerpoint.png';

                case "application/vnd.oasis.opendocument.presentation":
                    return RootDir + '/app/img/icon-presentation.png';

                case "application/msword":
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    return RootDir + '/app/img/icon-word.png';

                case "text/plain":
                case "text/rtf":
                case "application/vnd.oasis.opendocument.text":
                    return RootDir + '/app/img/icon-text.png';

                default:
                    return RootDir + '/app/img/icon-download.png';
            }
        },

        setupPopups: function (el)
        {
            $(el).find('[data-utils="gallery"]').each(function ()
            {
                $(this).magnificPopup({
                    delegate: 'a[data-utils="popup"]',
                    type: "image",
                    mainClass: 'mfp-with-zoom',
                    zoom: { enabled: true, duration: 300, easing: 'ease-in-out' },
                    gallery: { enabled: true }
                });
            });

            $(el).find('[data-utils="popup-youtube"]').each(function ()
            {
                $(this).magnificPopup({ type: "iframe" });
            });

            $(el).find('[data-toggle="toggle"]').each(function () {
                $(this).bootstrapToggle();
            });
        },

        // Hide element. Works with !important.
        hide: function (el)
        {
            if (!el)
            {
                return;
            }

            var s = (el.getAttribute("style") || "").replace(/display\s*:[^;]+;\s*/gi, "");

            el.setAttribute("style", s + "display: none !important;");
        },

        // Removes display property from inline element style.
        unhide: function (el)
        {
            if (!el)
            {
                return;
            }

            var s = (el.getAttribute("style") || "").replace(/display\s*:[^;]+;\s*/gi, "");

            el.setAttribute("style", s);
        },

        /* Convert date object to ISO string with timezone offset info, assuming that
         * date object has date in User timezone (not javascript/browser)
         * */
        dateToString: function (date)
        {
            return moment.tz(date.getFullYear() + "-" + this.padLeft(date.getMonth() + 1, "0", 2) + "-" + this.padLeft(date.getDate(), "0", 2)
                + " " + this.padLeft(date.getHours(), "0", 2) + ":" + this.padLeft(date.getMinutes(), "0", 2), _User.get("timezone")).format();
        },

        /* Convert date string (ISO format with timezone offset info) to date
         * object (in User timezone, not javascript/browser).
         * */
        stringToDate: function (str)
        {
            var m = moment(str).tz(_User.get("timezone")).toArray();
            var dd = new Date();

            dd.setFullYear(m[0]);
            dd.setMonth(m[1]);
            dd.setDate(m[2]);
            dd.setHours(m[3]);
            dd.setMinutes(m[4]);

            return dd;
        },

        premiumDisabledMessage: function (company_status) {
            switch (company_status) {
                case 4: return "not_available";
                case 6: return "label_trial_expired";
                case 7: return "label_pro_payment_failed";
                default: return "";
            }
        }
    };
});
