/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["spin"], function (Spinner)
{
    var sHold = 0,              // "reference counter" of manual activations.
        sAjaxProgress = false,  // Are any ajax requests in progress.
        sSpinnerOn = false,     // Is spinner currently on.
        sAjaxEnabled = true;    // Is spinner automatically shown for ajax requests.

    // spin parameter setup
    var opts = {
          lines: 13, // The number of lines to draw
          length: 7, // The length of each line
          width: 4, // The line thickness
          radius: 10, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          color: '#000', // #rgb or #rrggbb
          speed: 1, // Rounds per second
          trail: 60, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: true, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9 // The z-index (defaults to 2000000000)
    };

    var showProgress = function()
    {
        if (sAjaxProgress && !sSpinnerOn)
        {
            sSpinnerOn = true;
            $("#progress-indicator").show().spin(opts);
        }
    };

    return {
        init: function ()
        {
            //jQuery plugin
            $.fn.spin = function(opts) {
                this.each(function() {
                    var $this = $(this),
                        data = $this.data();

                    if (data.spinner) {
                        data.spinner.stop();
                        delete data.spinner;
                    }
                    if (opts !== false) {
                        data.spinner = new Spinner($.extend({color: $this.css('color')},opts)).spin(this);
                    }
                });
                return this;
            };

            $(document)
                .ajaxStart(function ()
                {
                    if (!sAjaxEnabled)
                    {
                        return;
                    }

                    if (!sAjaxProgress)
                    {
                        sAjaxProgress = true;

                        setTimeout(showProgress, 500);
                    }
                })
                .ajaxStop(function ()
                {
                    if (!sAjaxEnabled)
                    {
                        return;
                    }

                    sAjaxProgress = false;

                    if (sHold <= 0 && sSpinnerOn)
                    {
                        sSpinnerOn = false;
                        $("#progress-indicator").spin(false).hide();
                    }
                });

            $.ajaxSetup({ timeout: 600000 });
        },

        hold: function ()
        {
            if (!sSpinnerOn)
            {
                sSpinnerOn = true;
                $("#progress-indicator").show().spin(opts);
            }

            sHold++;

            return {
                release: function ()
                {
                    sHold--;

                    if (sHold <= 0 && sSpinnerOn)
                    {
                        sSpinnerOn = false;
                        $("#progress-indicator").spin(false).hide();
                    }
                }
            }
        },

        disableAjax: function ()
        {
            sAjaxEnabled = false;
        },

        enableAjax: function ()
        {
            sAjaxEnabled = true;
        }
    };
});
