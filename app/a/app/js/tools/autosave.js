/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(function ()
{
    var _self = { };

    /* Call this before performing WS call.
     *
     * model - model
     * fields - fields to be saved
     * deferred - should resolve if operation was successful
     * */
    _self.save = function (model, fields, deferred)
    {
        var key = "m_autosave_" + model.name + "_" + location.href,
            data = { values: [ ] };

        for (var i=0; i<fields.length; i++)
        {
            data.values.push({ field: fields[i], value: model.get(fields[i]) });
        }

        if (deferred instanceof Object && deferred.done instanceof Function)
        {
            localStorage.setItem(key, JSON.stringify(data));

            deferred.done(function ()
            {
                // All OK, remove auto save.
                localStorage.removeItem(key);
            });
        }
        else
        {
            if (!_Production)
            {
                console.warn("Autosave won't work without deferred!");
                console.trace();
            }
        }
    };

    /* Call this on pages where auto save should be enabled.
     *
     * name - name of the model
     * model - model
     * view - view with the startEdit method
     * */
    _self.restore = function (model, view)
    {
        var key = "m_autosave_" + model.name + "_" + location.href,
            data = localStorage.getItem(key);

        if (data)
        {
            localStorage.removeItem(key);

            console.log("Autosave found!");
        }
    };

    return _self;
});
