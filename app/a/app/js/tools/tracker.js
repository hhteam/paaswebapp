/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(function ()
{
    var sLog = [ ],
        sTimer = null,
        sLastAction = { };

    var flush = function ()
    {
        if (sTimer)
        {
            window.clearTimeout(sTimer);
            sTimer = null;
        }

        if (sLog.length)
        {
            $.ajax({
                type: "POST",
                url: MoodleDir + "theme/monorail/ext/ajax_save_stats.php",
                data: { stats: JSON.stringify(sLog) },
            });

            sLog = [ ];
        }
    };

    return {
        track: function (action, details)
        {
            var time = Math.floor(Date.now() / 1000);

            if (sLastAction.action == action && action == "courseview" &&
                sLastAction.details.courseid == details.courseid && time <= sLastAction.time + 1)
            { }
            else
            {
                sLog.push({ action: action, details: details, time: time });

                sLastAction = { action: action, details: details, time: time };

                if (sTimer)
                {
                    window.clearTimeout(sTimer);
                    sTimer = null;
                }

                sTimer = window.setTimeout(function ()
                {
                    sTimer = null;

                    flush();
                }, 4000);
            }
        },

        flush: flush
    };
});
