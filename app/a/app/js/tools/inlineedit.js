/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

/* Configure the input fields with following classes:
 *
 * livevalidation - field will be validated against model validator after each key press (and tooltip shown for errors)
 * */

define(["i18n!nls/strings", "app/tools/inlinecontent", "app/tools/utils"], function (str, InlineContent, Utils)
{
    var getValue = function (model, field)
    {
        if (field === undefined)
        {
            console.warn("An inline input field doesn't have field name!");
            return null;
        }

        var parts = field.split("|"),
            i = 0, c = parts.length, val = model, v;

        if (c == 1)
        {
            v = model.get(field);

            return v ? v : "";
        }
        else
        {
            for (; i<c; i++)
            {
                try
                {
                    v = val.get(parts[i]);
                }
                catch (err)
                {
                    console.warn("Unable to get value from " + parts[i]);
                }

                val = v;
            }

            return val ? val : "";
        }
    };

    var lastValidationError = "";

    // map of existing ckeditor editors, used when attachment list is
    // updated after editor has been initialized (currently after wizard
    // only, but later - who knows)
    var sCKEditorAttachments = { };

    var validateField = function (model, field, value)
    {
        var attrs = { };

        var parts = field.split("|"),
            i = 0, c = parts.length - 1,
            mod = model;

        for (; i<c; i++)
        {
            try
            {
                mod = mod.get(parts[i]);
            }
            catch (err)
            {
                console.warn("Unable to get value from " + parts[i]);
                break;
            }
        }

        try
        {
            if ("validate" in mod && mod.validate instanceof Function)
            {
                attrs[parts[parts.length - 1]] = value;

                return mod.validate(attrs);
            }
        }
        catch (err)
        {
            console.warn("Cannot validate " + field);
        }
    };

    var setValue = function (model, field, value, dry)
    {
        if (!field)
        {
            console.warn("data-field not specified for editable element!");

            return dry ? true : false;
        }

        var parts = field.split("|"),
            i = 0, c = parts.length - 1, val = model, v, vErr;

        if (c == 0)
        {
            if (!dry && (vErr = validateField(model, field, value)))
            {
                lastValidationError = vErr;

                return false;
            }
            oldValue = model.get(field);

            model.set(field, value, { silent: true });

            if (oldValue != value)
            {
                model.trigger('valuechanged');
            }
        }
        else
        {
            for (; i<c; i++)
            {
                try
                {
                    v = val.get(parts[i]);
                }
                catch (err)
                {
                    console.warn("Unable to set value to " + parts[i]);
                }

                val = v;
            }

            if (!dry && (vErr = validateField(val, parts[i], value)))
            {
                lastValidationError = vErr;

                return false;
            }

            oldValue = val.get(parts[i]);

            val.set(parts[i], value, { silent: true });

            if (oldValue != value)
            {
                val.trigger('valuechanged');

                // Trigger parent model as well (no one is listening for children)...
                model.trigger('valuechanged');
            }
        }

        return true;
    };

    /* Extract value from any element.
     * */
    var getElementValue = function (element)
    {
        var el = $(element);

        if (el.hasClass("editable"))
        {
            if (el.attr("data-placeholder") && el.find(".placeholder").length > 0)
            {
                return "";
            }

            if (el.hasClass("richtext"))
            {
                if (el.attr("data-ckedit-id"))
                {
                    return CKEDITOR.instances[el.attr("data-ckedit-id")].getData();
                }
                else
                {
                    return el.get(0).innerHTML;
                }
            }
            else
            {
                return $.trim(el.text());
            }
        }
        else if (el.prop("tagName") == "INPUT" && (el.prop("type") == "checkbox" || el.prop("type") == "radio"))
        {
            return el.is(":checked") ? 1 : 0;
        }
        else
        {
            return $.trim(el.val());
        }

        return "";
    };

    // Borrowed from http://stackoverflow.com/questions/487073/check-if-element-is-visible-after-scrolling
    function isScrolledIntoView(elem)
    {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    var liveValidator = function (model, element, event, focus)
    {
        var err = validateField(model, $(element).attr("data-field"), getElementValue(element));
        if (err)
        {
            if ( $(element).data('tooltip') ) {
                var errmsg =  $(element).data('tooltip').options.title;

                // Destroy tooltip if the message is different
                if (typeof(err) == 'object') {
                    if (err.err !== errmsg) {
                        $(element).tooltip("destroy");
                    }
                } else {
                    if (err !== errmsg) {
                        $(element).tooltip("destroy");
                    }
                }
            }

            if (typeof(err) == 'object') {
                $(element).attr("title", err.err);
                $(element).tooltip({ title: err.err, trigger: 'manual', animation: false });
                $(element).tooltip('show');

                if (focus)
                {
                    $(element).focus();

                    if (!isScrolledIntoView(element))
                    {
                        $.scrollTo(element, 800, { offset: { top: -100 } });
                    }
                }

                // TODO: use placeholder from element instead of predefined
                // string... and make a normal placeholder mechanism
                if ( event == 'blur' && $(element).prop('tagName') != 'INPUT'
                    && $(element).hasClass('notempty') && $(element).html() == '') {
                    $(element).html(str.label_attachment);
                }
            }
            else {
                // must set element title attribute here, because bootstrap
                // is picking that instead of what's given, and ckeditor is
                // setting it's own title attribute...
                $(element).attr("title", err);
                $(element).tooltip({ title: err, trigger: "manual", animation: false });
                $(element).tooltip("show");

                if (focus)
                {
                    $(element).focus();

                    if (!isScrolledIntoView(element))
                    {
                        $.scrollTo(element, 800, { offset: { top: -100 } });
                    }
                }
            }
        }
        else
        {
            $(element).tooltip("destroy");
        }

        return err;
    };

    var InlineEdit = function ()
    {
        this.mView = null;
        this.mEnabled = false;
        this.mAttachments = null;
    };

    InlineEdit.prototype.isActive = function ()
    {
        return this.mEnabled;
    };

    /* Set attachment list where this editor will store attachments.
     * */
    InlineEdit.prototype.setAttachmentList = function (list, key)
    {
        if (!key)
        {
            this.mAttachments = list;
        }
        else
        {
            if (this.mAttachments === null)
            {
                this.mAttachments = { key: list };
            }
            else
            {
                if ("model" in this.mAttachments)
                {
                    console.warn("Inconsistency in attachment list assignment for rich edit!");
                }

                this.mAttachments[key] = list;
            }

            if (sCKEditorAttachments[key])
            {
                sCKEditorAttachments[key]._attachments = list;
            }
        }
    };

    /* Initialize in-line editor with the given view.
     * */
    InlineEdit.prototype.init = function (view)
    {
        this.mEnabled = false;
        this.mView = view;
        this.setup(this.mView.$el);
    };

    InlineEdit.prototype.reInit = function (node)
    {
        this.setup($(node));

        if (this.mEnabled)
        {
            this.start(node, true);
        }
    };

    InlineEdit.prototype.placeholderActive = function (node)
    {
        if ($(node).attr("data-placeholder") && $(node).children().first().hasClass("placeholder")) {
            return true;
        }
        else
        {
            return false;
        }
    };

    InlineEdit.prototype.setup = function (node)
    {
        var thisThis = this;

        // Events for inline-editable fields.

        node.find(".editable")
            .on("focus", function (ev)
            {
                if (!thisThis.mEnabled)
                {
                    return;
                }

                var $t = $(this).closest(".editable");

                if ($t.attr("data-placeholder") && $t.find(".placeholder").length > 0)
                {
                    setTimeout(function ()
                    {
                        $t.find(".placeholder").remove();
                        $t.focus();
                    }, 10);
                }

                $t.data("original", $t.html());
            })
            .on("blur", function (ev)
            {
                if (!thisThis.mEnabled)
                {
                    return;
                }

                var $t = $(this).closest(".editable");

                if ($t.attr("data-placeholder") && Utils.isEmpty(getElementValue(this)))
                {
                    $t.html("<span class=\"placeholder\" style=\"color: #888;\">" + $t.attr("data-placeholder") + "</span>");
                }
            });

        node.find(".editable:not('.richtext')")
            .on("keydown", function (ev)
            {
                if (!thisThis.mEnabled)
                {
                    return;
                }

                var $t = $(this).closest(".editable");

                switch (ev.keyCode)
                {
                    case 27:
                        $t.html($t.data("original"));
                        $t.blur();
                        break;

                    case 13:
                        if (!$t.hasClass("multiline"))
                        {
                            $t.blur();
                        }
                        break;
                }
            })
            .on("click", function (ev)
            {
                if (thisThis.mEnabled)
                {
                    ev.stopPropagation();
                    ev.preventDefault();
                }
            })
            .on("drop", function (ev)
            {
                if (!thisThis.mEnabled)
                {
                    return;
                }

                ev.stopPropagation();
                ev.preventDefault();
            })
            .on("paste", function (ev)
            {
                ev.preventDefault();

                var sel = window.getSelection(),
                    ran = sel.getRangeAt(0),
                    $t = $(this),
                    no = document.createTextNode(ev.originalEvent.clipboardData.getData('Text'));

                sel.removeAllRanges();
                sel.addRange(ran);
                ran.deleteContents();
                ran.insertNode(no);

                $t.focus();
                ran = document.createRange();
                ran.selectNodeContents(no);
                ran.collapse(false);
                sel.removeAllRanges();
                sel.addRange(ran);

                if ($t.hasClass('livevalidation'))
                {
                    liveValidator(thisThis.mView.model, $t);
                }
            });

        // Live validation fields.

        node.find(".livevalidation")
            .on("keyup", function (ev)
            {
                if (thisThis.mEnabled)
                {
                    liveValidator(thisThis.mView.model, this);
                }
            })
            .on("blur", function (ev)
            {
                if (thisThis.mEnabled)
                {
                    liveValidator(thisThis.mView.model, this, 'blur');
                }
            });
    };

    /* Enter editing mode.
     * */
    InlineEdit.prototype.start = function (node, restart)
    {
        var thisThis = this;

        if (this.mEnabled && !restart)
        {
            // XXX: trying to enable editing when it's already enabled...
            console.warn("Trying to re-enable editor...");
            return;
        }

        this.mEnabled = true;

        (node ? node : this.mView.$el).find(".editable").attr("contenteditable", true).filter(".richtext").each(function ()
        {
            var att = thisThis.mAttachments ?
                        ("model" in thisThis.mAttachments ?
                            thisThis.mAttachments : thisThis.mAttachments[$(this).attr("data-field")]) : null,
                that = this;

            if (att)
            {
                // Unreplace replaced attachments (to switch to
                // manipulatable images)
                $(this).html(getValue(thisThis.mView.model, $(this).attr("data-field")));
                InlineContent.fixTextDirection($(this));
                InlineContent.fixAttachmentUrls($(this), att.model, true, true);
            }

            // Initialize CKEDITOR for rich text fields.
            require(["ckeditor"], function ()
            {
                CKEDITOR.disableAutoInline = true;
                CKEDITOR.timestamp = Version;
                CKEDITOR.config.language = _User.get("lang");
                CKEDITOR.config.contentsLangDirection = CKEDITOR.config.language == "ar" ? "rtl" : "ui";

                var editor = CKEDITOR.inline(that);
                var preventDrag = true;

                $(that)
                    .on('dragstart', function (ev)
                    {
                       preventDrag = false;
                    })
                    .on('drop', function (ev)
                    {
                       if (preventDrag)
                       {
                          ev.preventDefault(true);
                       }

                       preventDrag = true;
                    })
                    .attr("data-ckedit-id", editor.name);

                if (att)
                {
                    editor._attachments = att;
                    sCKEditorAttachments[$(that).attr("data-field")] = editor;
                }
            });
        });

        (node ? node : this.mView.$el).find(".editable").each(function ()
        {
            if ($(this).attr("data-placeholder") && Utils.isEmpty(getElementValue(this)))
            {
                $(this).html("<span class=\"placeholder\" style=\"color: #888;\">" + $(this).attr("data-placeholder") + "</span>");
            }
        });
    };

    /* Leave editing mode reverting all changes.
     * */
    InlineEdit.prototype.rollback = function ()
    {
        this.mEnabled = false;
        var thisThis = this;

        this.mView.$el.find(".editable").each(function ()
        {
            $(this).attr("contenteditable", false).html(getValue(thisThis.mView.model, $(this).attr("data-field")));
        });

        this.mView.$el.find("textarea[data-field],select[data-field],input[data-field]").each(function ()
        {
            $(this).val(getValue(thisThis.mView.model, $(this).attr("data-field")));
        });

        this.mView.$el.find("[data-ckedit-id]").each(function ()
        {
            try
            {
                CKEDITOR.instances[$(this).attr("data-ckedit-id")].destroy();
            }
            catch (err)
            { }
        });

        sCKEditorAttachments = { };
    };

    /* Leave editing mode keeping the changes.
     *
     * Returns true if values successfully are set to model, false if
     * validation fails. Validation is performed by model ('override'
     * validate method when creating model).
     *
     * options: { dry: true/false, deferred: deferred }
     *
     * If dry is true, then move all data from input fields to models without
     * validating and without leaving edit mode.
     *
     * If deferred is provided, then input fields will leave edit mode only
     * if/when deferred is resolved.
     * */
    InlineEdit.prototype.commit = function (options)
    {
        var thisThis = this,
            allOk = true,
            dry = (options instanceof Object) ? options.dry : false;

        if (!this.mView)
        {
            console.warn("Inline edit got lost?");
            return;
        }

        // Collect values...
        this.mView.$el.find(".editable,select[data-field],input[data-field],textarea[data-field]").each(function ()
        {
            if (getElementValue(this) !== thisThis.mView.model.get($(this).attr("data-field")))
            {
                if (!setValue(thisThis.mView.model, $(this).attr("data-field"), getElementValue(this), dry))
                {
                    $(this).focus();
                    allOk = false;
                    return;
                }
            }
        });

        if (dry)
        {
            return true;
        }

        if (!allOk)
        {
            this.errorString = lastValidationError;

            return false;
        }

        // Final model validation.
        if (this.mView.model.validate instanceof Function)
        {
            this.errorString = this.mView.model.validate(this.mView.model.attributes);

            if (this.errorString)
            {
                return false;
            }
        }

        var exitEditMode = function ()
        {
            this.mView.$el.find(".editable").each(function ()
            {
                $(this).attr("contenteditable", false);
            });

            this.mView.$el.find("[data-ckedit-id]").each(function ()
            {
                try
                {
                    CKEDITOR.instances[$(this).attr("data-ckedit-id")].destroy();
                }
                catch (err)
                { }
            });

            this.mEnabled = false;

            sCKEditorAttachments = { };
        };

        if (options instanceof Object && options.deferred instanceof Object)
        {
            options.deferred.done(_.bind(exitEditMode, this));
        }
        else
        {
            if (!_Production)
            {
                console.log("Inline edit without deferred? FIX IT!");
                console.trace();
            }

            exitEditMode.apply(this);
        }

        return true;
    };

    /* Show tool-tips for fields with invalid values (and focus the first
     * invalid field).
     *
     * Returns true if invalid fields were found.
     * */
    InlineEdit.prototype.triggerValidation = function ()
    {
        var model = this.mView.model,
            firstErr = true;

        this.mView.$el.find(".editable,select[data-field],input[data-field],textarea[data-field]").each(function ()
        {
            if (liveValidator(model, this, null, firstErr))
            {
                firstErr = false;
            }
        });

        return !firstErr;
    };

    return InlineEdit;
});
