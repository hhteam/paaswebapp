/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/ajaxprogress", "app/views/dialogs/error_message"], function (AjaxProgress, ErrorDialog)
{
    var sToken = null,
        sSessionKey = null;

    var _self = {
        socket: null,

        call: function (name, args, handler, owner, options, retryHolder)
        {
            var deferred = (options instanceof Object && options.deferred instanceof Object) ?
                                options.deferred : $.Deferred();

            var data = {
                wsfunction: name,
                moodlewsrestformat: "json"
            };

            _.extend(data, args);

            var url = MoodleDir + "local/monorailservices/api/call.php", method;

            switch (name)
            {
                case "local_monorailservices_course_get_cont":
                case "local_monorailservices_enrolled_users":
                case "local_monorailservices_get_course_info":
                case "local_monorailservices_get_user_courses":
                case "local_monorailservices_get_users_by_id":
                case "local_monorailservices_supported_langs":
                case "local_monorailservices_getcrs_categories":
                case "local_monorailservices_get_countries":
                    method = "GET";
                    break;

                default:
                    method = "POST";
            }

            var handleFailure = function (err)
            {
                (new ErrorDialog()).render();

                deferred.rejectWith((owner ? owner : this), [ err ]);
            };

            var handleFailedCall = function (err, type)
            {
                if (!_Production)
                {
                    console.warn("WEB SERVICE CALL FAILED:");
                    console.log(data);
                    console.log(err);
                    console.warn("------------------------");
                }

                if (options instanceof Object && options.errorHandler instanceof Function)
                {
                    // The function knows what to do.
                    options.errorHandler.apply((owner ? owner : this), [ err ]);

                    deferred.rejectWith((owner ? owner : this), [ err ]);
                }
                else
                {
                    var h = retryHolder ? retryHolder : AjaxProgress.hold();

                    if (!h.retries)
                    {
                        h.retries = 1;
                    }
                    else
                    {
                        h.retries++;
                    }

                    switch (type)
                    {
                        case 1:
                            // Bad arguments?
                            if (h.retries < 2)
                            {
                                setTimeout(function ()
                                {
                                    console.warn("CALL FAILED. TRYING ONE MORE TIME.");

                                    if (!(options instanceof Object))
                                    {
                                        options = { };
                                    }

                                    // Reuse the same deferred for retry
                                    // calls.
                                    options.deferred = deferred;

                                    _self.call(name, args, handler, owner, options, h);
                                }, 3000);
                            }
                            else
                            {
                                h.release();
                                handleFailure(err);
                            }
                            break;

                        case 0:
                            // No network?
                            if (h.retries < 10)
                            {
                                setTimeout(function ()
                                {
                                    console.warn("NETWORK DOWN? TRYING AGAIN. " + h.retries);

                                    if (!(options instanceof Object))
                                    {
                                        options = { };
                                    }

                                    // Reuse the same deferred for retry
                                    // calls.
                                    options.deferred = deferred;

                                    _self.call(name, args, handler, owner, options, h);
                                }, 2000);
                            }
                            else
                            {
                                h.release();
                                handleFailure(err);
                            }
                            break;
                    }
                }
            };

            var handleSucessfulCall = function (res)
            {
                if (retryHolder)
                {
                    retryHolder.release();
                }

                if (handler instanceof Function)
                {
                    handler.apply((owner ? owner : this), [ res ]);
                }

                deferred.resolveWith((owner ? owner : this), [ res ]);
            };

            $.ajax({ url: url, type: method, data: data,
                success: function (res)
                {
                    // Web services should only return JSON. Nothing else
                    // is accepted.
                    if (typeof res == "string")
                    {
                        try
                        {
                            res = $.parseJSON($.trim(res));
                        }
                        catch (err)
                        {
                            handleFailedCall(err, 1);
                            return;
                        }
                    }

                    // Normal call.
                    if (res instanceof Object && res.status == "ok")
                    {
                        // XXX: there could be warnings. Some fatal,
                        // some not so fatal...

                        handleSucessfulCall(res.result);
                    }
                    else if (res instanceof Object && res.status == "nouser")
                    {
                        // TODO: don't loose the data in this call?
                        // XXX: Save call data and repeat at some point?
                        localStorage.setItem('login.wants-url', document.location.href);
                        window.location = LoginUrl;
                    }
                    else
                    {
                        // XXX: Save call data and repeat at some point?
                        handleFailedCall(res, 1);
                    }
                },
                error: function (xhr, stat, err)
                {
                    // Network error?
                    // XXX: Save call data and repeat at some point?
                    handleFailedCall(err, 0);
                }
            });

            return deferred;
        },

        setToken: function (token)
        {
            sToken = token;
        },

        getToken: function ()
        {
            return sToken;
        },

        setSesskey: function (key)
        {
            sSessionKey = key;
        },

        getSesskey: function ()
        {
            return sSessionKey;
        },

        createSocket: function(FeedCollection) {
            try
            {
                this.socket = io.connect(RelayHost, {
                    secure: true
                });
                this.socket.emit('relay_hookup', { sesskey: this.getSesskey(), userid: _User.get('id') });
                thisThis = this;
                this.socket.on('relay_ok', function() {
                    thisThis.socket.on('notifications', function() {
                        FeedCollection.count += 1;
                        FeedCollection.fetch({ always: true, increment: false });
                    }, FeedCollection);
                });
            }
            catch (err)
            {
                console.warn("socket io error");
            }
        },

        closeSocket: function() {
            try
            {
                this.socket.disconnect();
            }
            catch (err)
            {
                console.warn("socket io error");
            }
        },
    };

    return _self;
});
