/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define([],function ()
{
    var copyAttributes = function (source, target)
    {
        ["width", "height", "cssFloat", "marginRight", "marginLeft", "marginTop", "marginBottom", "margin", "verticalAlign"].forEach(function (f)
        {
            if (source.style[f])
            {
                target.style[f] = source.style[f];
            }
        });

        ["width", "height"].forEach(function (f)
        {
            if (source.hasAttribute(f))
            {
                target.setAttribute(f, source.getAttribute(f));
            }
        });
    };

    return {
        /* Prepare HTML with attachments for saving.
         * */
        embedAttachments: function (html, attachments)
        {
            var el = document.createElement("div");

            el.innerHTML = html;

            $(el).find("*[data-attachment-id]").each(function ()
            {
                var att = attachments.get(this.getAttribute("data-attachment-id"));

                if (att && parseInt(att.get("id")) > 0)
                {
                    // Replace cid with real attachment id
                    this.removeAttribute("data-attachment-id");
                    this.setAttribute("data-attachment", att.get("id"));

                    if (!this.hasAttribute("data-attachment-type"))
                    {
                        // Remove URL that points to temporary file
                        switch (this.nodeName)
                        {
                            case "IMG": case "AUDIO": case "VIDEO":
                                this.removeAttribute("src");
                                break;
                        }
                    }
                    if (this.hasAttribute("data-attachment-type") && (this.getAttribute("data-attachment-type") == "eliademyvideo"))
                    {
                       var dat = att.toJSON();
                       //Update thumbnail and entryid for this video attachment
                       if(dat.hasOwnProperty('vresid') && this.hasAttribute("data-entry-id")) {
                         this.setAttribute("data-entry-id", att.get("vresid"));
                       }
                       this.setAttribute("src", att.get("url"));
                    }
                }
            });

            return el.innerHTML;
        },

        flagInlinedAttachments : function(html, attachments)
        {
            var el = document.createElement("div");
            el.innerHTML = html;

            $(el).find("*[data-attachment]").each(function ()
            {
                var att = attachments.get(this.getAttribute("data-attachment"));

                if (att)
                {
                    //This attachment is inlined
                    att.set({ inline: true }, { silent: true });
                }
            });

        },

        removeDeletedInlineAttachments : function(attachments)
        {
            //This is tricky , make sure that you don't remove required attachments
            attachments.each(function(att){
               var cdata = att.toJSON();
               if(typeof cdata.inline === 'undefined') {
                   if((cdata.source == 'course_modules') && (cdata.visible == false)) {
                     attachments.remove(att);
                     att.destroy({success: function(model, response) {
                     }});
                   } else if ((cdata.source == 'task_files') || (cdata.source == 'task_vfiles')) {
                     if(att.get('contextualdata')) {
                       var ctxdata  = JSON.parse(att.get('contextualdata'));
                       if(ctxdata.visible == false) {
                         att.destroy({success: function(model, response) {
                        }});
                       }
                    }
                 }
               } else {
                    //Remove inline attr setting
                    att.unset('inline' , { silent: true });
               }
            });
        },

        fixMathJaxText: function (par)
        {
            if (typeof MathJax != "undefined")
            {
                try
                {
                    par.find(".math-tex").each(function ()
                    {
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub,this]);
                    });
                }
                catch (err)
                { }
            }
        },

        fixTextDirection: function (par)
        {
            if(_User.get("lang") == "ar") {
                par.find(".editable").filter(".richtext").each(function ()
                {
                    this.setAttribute("dir", "rtl");
                });
            }
        },

        /* Display attachments inside HTML.
         * */
        fixAttachmentUrls: function (par, attachments, noreplace, canedit)
        {
            if (attachments instanceof Object)
            {
                par.find("*[data-attachment]").each(function ()
                {
                    var att = attachments.get(this.getAttribute("data-attachment"));
                    if (!att) {
                        //console.warn("Attachment " + this.getAttribute("data-attachment") + " not found.");
                        // It's OK. Sometimes.
                    } else {
                        if (this.hasAttribute("data-attachment-type"))
                        {
                            if (noreplace !== true)
                            {
                                var type = this.getAttribute("data-attachment-type");
                                switch (type)
                                {
                                    case "video": case "audio":
                                        // Replace with video/audio element.
                                        var el = document.createElement(type);

                                        el.setAttribute("src", att.get("url"));
                                        el.setAttribute("controls", true);
                                        el.setAttribute("preload", "metadata");
                                        el.style.verticalAlign = "middle";

                                        copyAttributes(this, el);

                                        $(this).replaceWith(el);
                                        break;
                                    case "eliademyvideo":
                                        if (!att.get("visible")) {
                                            var entry_id = this.getAttribute("data-entry-id");
                                            var eurl = vidUrl + '/p/' + vidPartnerId +'/sp/'+vidPartnerId+'00/embedIframeJs/uiconf_id/'+vidPlayerId+'/partner_id/'+vidPartnerId+'?iframeembed=true&playerId=kaltura_player_'+new Date().getTime()+'&entry_id='+entry_id;
                                            var el = document.createElement('iframe');
                                            el.setAttribute("width", 480);
                                            el.setAttribute("height", 360);
                                            el.setAttribute("frameborder", 0);
                                            el.setAttribute("allowfullscreen", true);
                                            el.setAttribute("webkitallowfullscreen", true);
                                            el.setAttribute("mozAllowFullScreen", true);
                                            copyAttributes(this, el);
                                            el.setAttribute("src", eurl);
                                            $(this).replaceWith(el);
                                        } else {
                                            $(this).remove();
                                        }
                                        break;
                                    default:
                                        console.warn("Unsupported attachment type: " + type);
                                }
                            }
                        }
                        else
                        {
                            // Set URL to attachment file.
                            switch (this.nodeName)
                            {
                                case "IMG":
                                    // XXX: some popup? But images may have
                                    // links...

                                case "AUDIO": case "VIDEO":
                                    this.setAttribute("src", att.get("url"));
                                    break;
                            }
                        }
                    }
                });
            }

            if (noreplace !== true)
            {
                par.find("*[data-attachment-youtube]").each(function ()
                {
                    var videoid = this.getAttribute("data-attachment-youtube");

                    $.ajax({ context: this, url: "https://gdata.youtube.com/feeds/api/videos/" + videoid + "?v=2", dataType: "text", success: function (gdata)
                    {
                        // XXX: some magic to determine if video can play
                        // on not-tube-you site.
                        if ($(gdata).find("yt\\:accessControl[action='autoPlay']").attr("permission") == "allowed")
                        {
                            var el = document.createElement("iframe");

                            el.setAttribute("src", "https://www.youtube.com/embed/" + videoid + "?autoplay=0");
                            el.setAttribute("frameborder", "0");
                            el.setAttribute("type", "text/html");
                            el.style.verticalAlign = "middle";

                            copyAttributes(this, el);

                            $(this).replaceWith(el);
                        }
                        else
                        {
                            this.addEventListener("click", function ()
                            {
                                window.open("https://www.youtube.com/watch?v=" + videoid, "_blank");
                            });
                        }
                    }});
                });

                par.find("*[data-attachment-vimeo]").each(function ()
                {
                    var videoid = this.getAttribute("data-attachment-vimeo");

                    var el = document.createElement("iframe");

                    el.setAttribute("src", "https://player.vimeo.com/video/" + videoid + "?autoplay=0");
                    el.setAttribute("frameborder", "0");
                    el.setAttribute("type", "text/html");
                    el.style.verticalAlign = "middle";

                    copyAttributes(this, el);

                    $(this).replaceWith(el);
                });

                par.find("*[data-attachment-slideshare]").each(function ()
                {
                    $(this).replaceWith($("<a>").attr("href", this.getAttribute("data-attachment-slideshare")).oembed());
                });
            }

            // And more ... (re-factor all into a single function?)

            if (!canedit && par.find('pre code').length > 0)
            {
                // Don't fix code highlight for users who can edit, because
                // editing doesn't work on fixed parts. So code will appear
                // highlighted only for students. For teachers it will be
                // highlighted when they enter edit mode.

                require(["ckeditor_hljs"], function ()
                {
                    par.find('pre code').each(function(i, block)
                    {
                        hljs.highlightBlock(block);
                    });
                });
            }
        },
    };
});
