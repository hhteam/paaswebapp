/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(function ()
{
	return {
		setupNode: function (node)
		{
            var thisThis = this;

			$(node).find("a").not(".ignored_link, .setupdone, [data-call]")
				.click(function (ev)
				{
					ev.preventDefault();

					var el = $(ev.target);                                        

					while (el.get(0).nodeName.toLowerCase() != "a")
					{
						el = el.parent();
					}

					var path = el.attr("href");

                    if (path == undefined)
                    {
                        console.warn("Trying to handle invalid link...");
                        return;
                    }
                    if(!$(this).hasClass("ignored_link"))
                    {
                        thisThis.go(path);
                    }

				}).addClass("setupdone");
		},

		setupView: function (view)
		{
			this.setupNode(view.$el);
		},

        go: function (path)
        {
            if (path.substring(0, 3) == "../")
            {
                // Link to old-school moodle.
                window.location.href = MoodleDir + path.substring(3);
            }
            else if( path.substring(0, 7) == 'mailto:')
                window.open( path );    // FIXME: how to prevent opening a tab?
            else if (path.substring(0, 7) == "http://" ||
                     path.substring(0, 8) == "https://")
            {
                // Link to outside.
                window.open(path, "_blank");
            }
            else if (path.substring(0, 1) == "/")
            {
                // Internal link.
                _Router.navigate(path, { trigger: true });
            }
            else if (path == ":back")
            {
                // Special link to go back.
                window.history.go(-1);
            }
            else
            {
                // Link to outside, probably.
                window.open("http://" + path, "_blank");
            }
        }
	};
});
