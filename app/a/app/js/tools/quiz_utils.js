/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define([], function ()
{
    return {
        getResult: function (model, question, userid) {
            var answerTicks = {},
                correct = true,
                hasAnswer = false,
                student_answer = "";

            if (parseInt(question.get("type")) == 2) {
                correct = false;

                if (model.get("answers") instanceof Object && model.get("answers").length > 0) {
                    var userIdx = 0,
                        answ = model.get("answers");

                    if (!userid) {
                        if (answ.length > 1) {
                            console.log("Have answers from multiple users, but no userid!");
                        }
                    } else {
                        for (var i=0; i<answ.length; i++) {
                            if (userid == answ[i].student) {
                                userIdx = i;
                                break;
                            }
                        }
                    }

                    var aqs = answ[userIdx].questions;

                    for (var i=0; i<aqs.length; i++) {
                        if (parseInt(aqs[i].question) == parseInt(question.get("id"))) {
                            student_answer = aqs[i].answers[0].checked;
                            hasAnswer = true;
                            break;
                        }
                    }
                }

                if (question.get("answers").at(0).has("student_answer")) {
                    student_answer = question.get("answers").at(0).get("student_answer");
                    hasAnswer = true;
                }

                question.get("answers").each(function (ans, anum) {
                    if (anum && ans.get("text").trim().toLowerCase() == student_answer.trim().toLowerCase()) {
                        correct = true;
                    }
                });
            } else {
                if (model.get("answers") instanceof Object && model.get("answers").length > 0) {
                    var userIdx = 0,
                        answ = model.get("answers");

                    if (!userid) {
                        if (answ.length > 1) {
                            console.log("Have answers from multiple users, but no userid!");
                        }
                    } else {
                        for (var i=0; i<answ.length; i++) {
                            if (userid == answ[i].student) {
                                userIdx = i;
                                break;
                            }
                        }
                    }

                    // Tick-box question.
                    var aqs = answ[userIdx].questions;

                    for (var i=0; i<aqs.length; i++) {
                        for (var j=0; j<aqs[i].answers.length; j++) {
                            answerTicks[aqs[i].answers[j].answer] = parseInt(aqs[i].answers[j].checked);
                        }
                    }
                }

                question.get("answers").each(function (ans) {
                    if (ans.has("student_answer")) {
                        answerTicks[ans.get("id")] = parseInt(ans.get("student_answer"));
                    }

                    if (parseInt(ans.get("correct")) != parseInt(answerTicks[ans.get("id")])) {
                        correct = false;
                    }

                    if (typeof answerTicks[ans.get("id")] != "undefined") {
                        hasAnswer = true;
                    }
                }, this);
            }

            return { correct: correct, ticks: answerTicks, hasAnswer: hasAnswer, student_answer: student_answer };
        }
    };
});
