/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "app/collections/settings/social_accounts"],
    function (SC , SocialAccountsCollection)
{
    var sCallback = null;
    return {

        sharePopup: function (service, url, name, message, picture)
        {
            switch (service)
            {
                case "facebook":
                    window.open("https://www.facebook.com/dialog/feed?app_id=" + FBAppID + "&display=page" +
                        "&name=" + encodeURIComponent(name) +
//                        "&description=" + encodeURIComponent(message) +
                        "&link=" + encodeURIComponent(url) +
                        "&picture=" + encodeURIComponent(picture) +
                        "&redirect_uri=" + encodeURIComponent("https://facebook.com"));
                    break;

                case "twitter":
                    window.open("https://twitter.com/intent/tweet?url=" + encodeURIComponent(url) +
                        "&text=" + encodeURIComponent(message));
                    break;

                case "linkedin":
                    window.open("http://www.linkedin.com/shareArticle?mini=true" +
                        "&title=" + encodeURIComponent(name) +
                        "&url=" + encodeURIComponent(url)// +
//                        "&summary=" + encodeURIComponent(message)
                        );
                    break;

                case "google":
                    window.open("https://plus.google.com/share?url=" + encodeURIComponent(url));
                    break;

                case "email":
                    window.location.href = "mailto:?subject=" + encodeURIComponent(name) + "&body=" + encodeURIComponent(message + "\n\n" + url)
                    break;

                default:
                    addthis.update("share", "url", url);
                    addthis_sendto("more");
            }
        },
 
        socialAccountAdded: function()
        {
            if (sCallback instanceof Function)
            {
                SocialAccountsCollection.reset();
                sCallback();
            }
            else
            {
               console.warn('No callback for social'); 
            }
        }, 

        addFacebookAccount: function (callback)
        {
            var win = window.open(
                "https://www.facebook.com/dialog/oauth/?client_id=" + FBAppID + "&redirect_uri=" + window.location.protocol + "//"
                  + window.location.host + MoodleDir + "auth/googleoauth2/facebook.php&scope=email,user_birthday,user_location,publish_actions&response_type=code", null, "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");

            sCallback = callback;
            window.socialAccountsUpdate = _.bind(this.socialAccountAdded, this);
        },

        removeAccount: function(socialact, callback)
        {
            SC.call("local_monorailservices_delete_social_account", {account: socialact},
                  function (data) {
                      if (callback instanceof Function)
                      {
                          SocialAccountsCollection.reset();
                          callback();
                      }
                  }, this, { errorHandler: function (data) {
                      if (callback instanceof Function)
                      {
                          callback();
                      }
                  }});

        },

        removeFacebookAccount: function (callback)
        {
            this.removeAccount('facebook',callback);
        },


        addTwitterAccount: function (callback)
        {
            var win = window.open(MoodleDir + "theme/monorail/twitter.php", null, "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");
            sCallback = callback;
            window.socialAccountsUpdate = _.bind(this.socialAccountAdded, this);
        },

        removeTwitterAccount: function (callback)
        {
            this.removeAccount('twitter',callback);
        },

        addLinkedinAccount: function (callback)
        {
            var win = window.open("https://www.linkedin.com/uas/oauth2/authorization?client_id="+ LINAppID + "&redirect_uri="  + window.location.protocol + "//" + window.location.host + MoodleDir + "auth/googleoauth2/linkedin.php&scope=r_emailaddress&state=" + new Date().getTime() + "&response_type=code", null, "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");

            sCallback = callback;
            window.socialAccountsUpdate = _.bind(this.socialAccountAdded, this);
        },

        addPaypalAccount: function (state, callback)
        {
            var url = "https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/authorize?client_id="
                + encodeURIComponent(PayPalID)
                + "&response_type=code&scope=email&nonce=" + (new Date().getTime()) + "&redirect_uri="
                + encodeURIComponent("https://eliademy.com/app/theme/monorail/ext/callback_paypal.php")
                + "&state=" + encodeURIComponent(state);

            window.open(url, null, "width=400,height=550,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");

            window.paypalAccountAdded = function ()
            {
                if (callback instanceof Function)
                {
                    callback();
                }
            };
        },

        removeLinkedinAccount: function (callback)
        {
            this.removeAccount('linkedin',callback);
        },

        addGoogleAccount: function (callback)
        {
        },

        removeGoogleAccount: function (callback)
        {
            this.removeAccount('google',callback);
        },

        addLiveAccount: function (callback)
        {
        },

        removeLiveAccount: function (callback)
        {
            this.removeAccount('live',callback);
        }
    };
});
