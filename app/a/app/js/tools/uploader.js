/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings","app/tools/warnings", "jqueryupload"], function (str, WarningTool)
{
    var sLastOwner = null,
        //restrict type of file which is gonna to open 
        filetype = null,
        filename = null,
        filesize = 0,
        warning = true,
        jqXHR = null, 
        customFileUploadLimit = FileUploadLimit,
        extensions;
    return {
        owner: function (ow)
        {
            sLastOwner = ow;

            return this;
        },
        showWarning: function (warn)
        {
           warning = warn; 
           return this;
        },

        cancelUpload: function()
        {
          if(jqXHR && jqXHR.readystate != 4){
            jqXHR.abort();
          }
        },
 
        restrictFileType: function(type)
        {
            filetype = type;
            return this;
        },
        restrictFileExts: function(exts)
        {
            // Array of allowed file extensions
            extensions = exts;
            return this;
        },
        setFileUploadLimit: function(size)
        {
            customFileUploadLimit = size;
            return this;
        },
        exec: function (handler, error, done, url)
        {
            var form = $("<form style=\"position: absolute; top: 0px; left: 0px;\">"),
                input = $('<input name="qqfile" type="file" > ').appendTo(form);

            if(filetype) input.attr("accept",filetype);

            if (typeof customFileUploadLimit === 'undefined') {
                customFileUploadLimit = FileUploadLimit;
            }

            input.fileupload(
            {
                url: (url ? url : MoodleDir + "theme/monorail/ext/ajax_upload_file_chunk.php"),
                multipart: false,
                maxChunkSize: 300000,
                pasteZone: null,
                
                add: function (e, data) {  
                    if (data.files[0].size ==  0) {
                        if(warning) {
                          WarningTool.processWarnings({msgtype:'error', message:str.err_file_empty});
                          $('#page-warnings').addClass('alert-error');
                        }
                        if (error instanceof Function) {
                            error.apply(sLastOwner, [{ error: msg }]);
                        }
                        customFileUploadLimit = FileUploadLimit;
                        extensions = null;
                        return;
                    }

                    if (typeof extensions === 'object' && extensions !== null && extensions.indexOf(data.files[0].name.split('.').pop().toLowerCase()) == -1) {
                        var msg = str.error_file_type.replace('%TYPES', extensions.join(', '));
                        if(warning) {
                          WarningTool.processWarnings({msgtype:'error', message:msg});
                          $('#page-warnings').addClass('alert-error');
                        }
                        if (error instanceof Function) {
                            error.apply(sLastOwner, [{ error: msg }]);
                        }
                        customFileUploadLimit = FileUploadLimit;
                        extensions = null;
                        return;
                    }

                    if (data.files[0].size > customFileUploadLimit) {
                        var msg = str.err_uploaded_file_exceeds_limit.replace('%LIMIT', customFileUploadLimit / 1024 / 1024);
                        if(warning) {
                          WarningTool.processWarnings({msgtype:'error', message:msg});
                          $('#page-warnings').addClass('alert-error');
                        }
                        if (error instanceof Function) {
                            error.apply(sLastOwner, [{ error: msg }]);
                        }
                        customFileUploadLimit = FileUploadLimit;
                        extensions = null;
                        return;
                    }

                    data.formData = { qqfile: data.files[0].name };
                    filename = data.files[0].name;
                    filesize = data.files[0].size;
                    if (handler instanceof Function) {
                        handler.apply();
                    }
                    customFileUploadLimit = FileUploadLimit;
                    if ($('#testdata-filename').length && $('#testdata-filename').val().length) {
                        // Test data, don't really upload, just trigger done handler
                        resData = { filename: $('#testdata-filename').val(), origname: $('#testdata-filename').val() };
                        done.apply(sLastOwner, [ resData ]);
                        customFileUploadLimit = FileUploadLimit;
                        extensions = null;
                    } else {
                        // Normal flow
                        jqXHR = data.submit();
                    }
                 },
                done: function (e, data)
                {
                    var resData = { };

                    try
                    {
                        var res = JSON.parse(data.result);

                        resData = { filename: filename, filesize: filesize, origname: res.files[0].name };
                    }
                    catch (err)
                    {
                        console.warn("Unable to get upload info");

                        resData = { filename: filename, filesize: filesize, origname: data.files[0].name };
                    }

                    if (done instanceof Function)
                    {
                        done.apply(sLastOwner, [ resData ]);
                    }

                    customFileUploadLimit = FileUploadLimit;
                    extensions = null;
                },

                fail: function (e, data)
                {
                    if (error instanceof Function) {
                        error.apply(sLastOwner, [{ error: data.textStatus }]);
                    } else {                    
                        console.warn("Upload failed: " + data.textStatus);
                    }
                    customFileUploadLimit = FileUploadLimit;
                    extensions = null;
                },
                
                progress: function (e, data) {
                	if (typeof sLastOwner.upload_process == 'function') {
                		sLastOwner.upload_process(e, data);
                	}
                    else
                    {
                        //assume have a progress-bar
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress-bar .bar').css(
                            'width',
                            progress + '%'
                        );
                    }
                }
                
            });

            if ($('#testdata-filename').length && $('#testdata-filename').val().length) {
                // Save upload form to DOM so we can work with it via test script
                window.testFileUploadForm = input;
            } else {
                // Normal flow
                input.get(0).click();
            }

            return this;
        }
    };
});
