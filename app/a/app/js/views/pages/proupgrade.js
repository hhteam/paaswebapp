/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/proupgrade.html", "i18n!nls/strings", "app/views/pageview", "app/tools/linkhandler", "app/tools/dataloader",
        "app/tools/utils", "app/collections/landingstrings", "app/models/admin/cohort", "app/collections/course/courses"],
    function (tpl, str, PageView, LinkHandler, DataLoader, Utils, LandingStringsCollection, CohortModel, CourseCollection)
{
    return new (PageView.extend(
    {
        template: _.template(tpl),
        menuHighlight: "none",

        events: {
            "click [data-call]": Utils.callMapper,
            "submit .start_demo_form": "startPro"
        },

        render: function ()
        {
            mixpanel.track("Go Premium page loaded", {"Page name" : "Go Premium page"});

            DataLoader.exec({ collection: LandingStringsCollection, context: this, where: { lang: _User.get("lang") } }, function (lstr)
            {
                this.$el.html(this.template({ str: str, lstr: lstr.get("str") }));

                LinkHandler.setupView(this);
                Utils.setupPopups(this.$el);
            });

            return this;
        },

        startPro: function (ev)
        {
            mixpanel.track("New Premium Trial Started", {"Page name" : "Go Premium page"});
            mixpanel.people.set({ "Plan": "Trial" });
            mixpanel.identify(_User.get("username"));

            ev.preventDefault();

            var cohort = new CohortModel({ name: ev.target["company"].value, movecourses: 1 });

            cohort.save(undefined, { success: function (model, response, options)
            {
                CourseCollection.reset();

                _User.set({ adminCohort: model.get("id") }, { silent: true });
                _User.trigger("updated");
                _Router.navigate("/catalog", { trigger: true });
            }});
        }
    }));
});
