/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/liveevent.html", "text!templates/pages/course/livedemo.html", "i18n!nls/strings", "app/tools/utils",
        "app/tools/linkhandler", "app/tools/dataloader", "app/views/dialogs/messagebox", "app/views/dialogs/liveevent", "app/models/course/liveevent",
        "app/views/dialogs/liveclientcompat", "app/collections/admin/cohorts", "app/collections/landingstrings"],

function (tpl, demoTpl, str, Utils, LinkHandler, DataLoader, MessageBox, LiveEventDialog, LiveEventModel, LiveClientCompatDialog, CohortCollection,
        LandingStringsCollection)
{
    return Backbone.View.extend(
    {
        el: ".course-content",

        template: _.template(tpl),
        demoTemplate: _.template(demoTpl),

        events : {
            "click [data-call]": Utils.callMapper,
        },

        editSession: function(ev,target)
        {
          var model = this.options.parentView.model.get("liveevents").get(target.getAttribute("data-id"));
          var thisThis = this;
          if(model) {
            (new LiveEventDialog(
                { model: model, parent: this,
                  accepted: function () {
                    model.save(undefined, { success: function (model, method, options) {
                      thisThis.options.parentView.model.get("liveevents").reset();
                      thisThis.render();
                    }
                  });
               }
               })).render();
          }
        },

        joinSession: function(ev,target)
        {
          var now = new Date();
          var curr  = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes());
          var currTime = Math.floor(Utils.stringToDate(curr.toISOString()).getTime()/1000);
          var model = this.options.parentView.model.get("liveevents").get(target.getAttribute("data-id"));
          if(model) {
            var pastItem = (model.get('timedue') < currTime) ? true : false;
            if(pastItem) {
                (new MessageBox({ message: str.lapsed_live_session_emsg, backdrop: false })).render();
            } else {
                (new LiveClientCompatDialog({ accepted: function ()
                {
                   $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_open_live.php",
                        data: {id: model.get("id") },
                        context: this,
                        dataType: 'json',
                        success: function (data)
                        {
                          if('error' in data & data.error) {
                            (new MessageBox({ message: data.error, backdrop: false })).render();
                          } else {
                            window.open(data.url);
                          }
                        },

                        error: function (data)
                        {
                            (new MessageBox({ message: str.error_unknown, backdrop: false })).render();
                        }
                    });
                }})).render();
             }
          }
        },

        deleteSession: function(ev,target)
        {
          var model = this.options.parentView.model.get("liveevents").get(target.getAttribute("data-id"));
          if(model) {
            var thisThis = this;
            model.destroy({ success: function ()
            {
               thisThis.options.parentView.model.get("liveevents").reset();
               thisThis.render();
            }});
          }
        },

        addSession: function(ev,target)
        {
          var model = new LiveEventModel({ course: this.options.parentView.model.get("id"),
                         timeavailable: (new Date).getTime()/1000 , timedue: (new Date).getTime()/1000 + 3600 });;
          var thisThis = this;
          if(model) {
            (new LiveEventDialog(
                { model: model, parent: this,
                  accepted: function () {
                    model.save(undefined, { success: function (model, method, options) {
                      thisThis.options.parentView.model.get("liveevents").reset();
                      thisThis.render();
                    }
                  });
               }
               })).render();
          }
        },

        render: function ()
        {
            var course = this.options.parentView.model.toJSON();

            if (course.cohortid)
            {
                DataLoader.exec({ context: this, collection: this.options.parentView.model.get("liveevents") }, function (liveevents)
                {
                    DataLoader.exec({ context: this, collection: CohortCollection, id: course.cohortid }, function (coh)
                    {
                        var upcoming = [ ], lapsed = [ ];

                        liveevents.each(function (it)
                        {
                           var item = it.toJSON();
                           var pastItem = (item.timedue > 0) ?  (item.timedue < ((new Date()).getTime() / 1000)) : false;
                           if(pastItem) {
                             lapsed.push(item);
                           } else {
                             upcoming.push(item);
                           }
                        });

                        this.$el.html(this.template({
                            str: str,
                            upcoming: upcoming,
                            lapsed: lapsed,
                            liveevents: liveevents.toJSON(),
                            course: course,
                            disabled_message: Utils.premiumDisabledMessage(coh.get("company_status")),
                            timezone: _User.get("timezone") }));

                        LinkHandler.setupView(this);
                    });
                });
            }
            else
            {
                if (course.can_edit) {
                    DataLoader.exec({ collection: LandingStringsCollection, context: this, where: { lang: _User.get("lang") } }, function (lstr)
                    {
                        this.$el.html(this.demoTemplate({ str: str, lstr: lstr.get("str"), isPaas : _User.get('isPaas') }));
                        LinkHandler.setupView(this);
                    });
                } else {
                    _Router.navigate("", { trigger: true });
                    return;
                }
            }
        }
    });
});
