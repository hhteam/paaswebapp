/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/settings_access.html", "i18n!nls/strings", "app/tools/linkhandler", "app/views/dialogs/yes_no",
        "app/views/dialogs/messagebox", "app/views/dialogs/invite_email", "app/views/dialogs/enroll_users", "app/tools/dataloader",
        "app/collections/admin/cohorts", "app/views/dialogs/delete_course", "app/models/headercontrols", "app/tools/utils", "app/tools/tracker",
        "app/collections/course/courses", "app/collections/landingstrings", "app/tools/social_utils"],

    function (tpl, str, LinkHandler, YesNoDialog, MessageBox, InviteEmailDialog, EnrollUsersDialog, DataLoader, CohortCollection, DeleteCourseDialog,
        HeaderControls, Utils, Tracker, CourseCollection, LandingStringsCollection, SocialUtils)
{
    return Backbone.View.extend(
    {
        mCohortAdmin: false,
        mCohortUser: false,
        unsavedSettings: false,
        template: _.template(tpl),
        mCanSavePrivate: false,

        events: {
            "click [data-call]": Utils.callMapper,

            "change [name='course_access']": "accessSettingsChanged",
            "change [name='course_licence']": "licenceSettingsChanged",
            "keyup #course-price-input": "accessSettingsChanged"
        },

        initialize: function ()
        {
            var cohortid = _User.get("adminCohort");
            if (!cohortid)
            {
                cohortid = _User.get("cohorts");

                if (typeof cohortid == "string" && cohortid.indexOf(",") != -1)
                {
                    cohortid = cohortid.split(",")[0];
                    this.mCohortUser = true;
                }
            } else {
                this.mCohortAdmin = true;
            }

            this.mAccessMode = "";
        },

        render: function ()
        {
            var pubPriv = this.model.get("can_edit") ? true : false,
                emailShare = pubPriv,
                enrolColl = false,
                cohortid = this.model.get("cohortid");

            if (cohortid)
            {
                if (!this.model.get("ccprivate") && !this.mCohortAdmin)
                {
                    pubPriv = false;
                }

                if (this.mCohortAdmin || (this.model.get("course_privacy") == 1) || pubPriv)
                {
                    emailShare = true;
                }
                else
                {
                    emailShare = false;
                }

                if (this.model.get("ccprivateext") || (this.model.get("course_privacy") == 1) || this.mCohortAdmin || this.mCohortUser)
                {
                    enrolColl = true;
                }
            }

            DataLoader.exec({ collection: LandingStringsCollection, context: this, where: { lang: _User.get("lang") } }, function (lstr)
            {
                var doRender = function (stat) {
                    var mainteacher = this.model.get("participants").get(this.model.get("mainteacher")),
                        course_access = this.model.get("course_access"),
                        course_privacy = this.model.get("course_privacy"),
                        course_price = this.model.get("course_price"),
                        hasStudents = false;

                    if (course_access == 1) {
                        this.mAccessMode = "draft";
                    } else if (course_privacy == 1 && course_access == 2) {
                        this.mAccessMode = "public_invites_only";
                    } else if (course_privacy == 1 && course_access == 3 && course_price == 0) {
                        this.mAccessMode = "public_free";
                    } else if (course_privacy == 1 && course_access == 3 && course_price > 0) {
                        this.mAccessMode = "public_paid";
                    } else if (course_privacy == 2) {
                        this.mAccessMode = "private";
                    }

                    this.model.get("participants").each(function (part) {
                        if (!part.get("teacher") && !part.get("manager")) {
                            hasStudents = true;
                        }
                    });

                    this.$el.html(this.template({
                        str: str,
                        lstr: lstr.get("str"),
                        course: this.model.toJSON(),
                        access_mode: this.mAccessMode,
                        has_students: hasStudents,
                        cohort_course: cohortid ? true : false,
                        cohortAdmin: this.mCohortAdmin,
                        showEnrollColleagues: enrolColl,
                        showEmailShare: emailShare,
                        showChangeAccess: pubPriv,
                        unsavedSettings: this.unsavedSettings,
                        public_course :  (this.model.get("course_privacy") == 1),
                        private_disabled_message: Utils.premiumDisabledMessage(stat),
                        instructor: mainteacher ? mainteacher.get("fullname") : ""
                    }));

                    this.$('[data-toggle="popover"]').popover({ trigger: "hover", placement: "top" });
                    Utils.setupPopups(this.$el);
                    LinkHandler.setupView(this);

                    if (!this.canBeCC()) {
                        this.$("#course-no-cc-info").show();
                        this.$("#course-cc-radio").css({ opacity: "0.6" });
                    }
                };

                if (cohortid) {
                    DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (cohort)
                    {
                        var stat = cohort.get("company_status");
                        this.mCanSavePrivate = (stat != 6 && stat != 4 && stat != 7);
                        doRender.call(this, stat);
                    });
                } else {
                    this.mCanSavePrivate = false;
                    doRender.call(this, 0);
                }
            });
        },

        saveAccessSettings: function (ev, target)
        {
            if (this.mAccessMode == "public_paid") {
                this.model.set({ course_price: parseFloat(this.$("#course-price-input").val()) }, { silent: true });
            } else {
                this.model.set({ course_price: 0 }, { silent: true });
            }

            this.options.parentView.options.parentView.saveCourseDetailsClicked();
        },

        licenceSettingsChanged: function ()
        {
            var newLicence = $("input[name='course_licence']:checked").val();

            if (newLicence == 2) {
                if (!this.canBeCC()) {
                    $("input[name='course_licence'][value='1']").prop("checked", true);
                    return;
                }
            }

            this.model.set({ "licence": newLicence }, { silent: true });

            this.unsavedSettings = true;
            $("#course-settings-save-btn").removeClass("disabled");
            this.$("#course-licence-settings-footer").slideDown();

            _Router.setConfirmLeave(str.edit_cancel,
                _.bind(this.options.parentView.options.parentView.saveCourseDetails, this.options.parentView.options.parentView),
                function () { CourseCollection.reset(); });
        },

        accessSettingsChanged: function () {
            var data = { };
            this.mAccessMode = this.$("[name='course_access']:checked").val();

            switch (this.mAccessMode) {
                case "draft":
                    data = { course_access: 1, course_privacy: 1 };
                    break;

                case "public_invites_only":
                    data = { course_access: 2, course_privacy: 1 };
                    break;

                case "public_free":
                    data = { course_access: 3, course_privacy: 1 };
                    break;

                case "public_paid":
                    data = { course_access: 3, course_privacy: 1 };
                    break;

                case "private":
                    data = { course_access: 2, course_privacy: 2 };
                    break;
            }

            if (this.mAccessMode == "draft") {
                this.$("#course-details-box-draft").show();
            } else {
                this.$("#course-details-box-draft").hide();
            }

            if (this.mAccessMode == "public_invites_only") {
                this.$("#course-details-box-public_invites_only").show();
            } else {
                this.$("#course-details-box-public_invites_only").hide();
            }

            if (this.mAccessMode == "public_free") {
                this.$("#course-details-box-public_free").show();
            } else {
                this.$("#course-details-box-public_free").hide();
            }

            if (this.mAccessMode == "public_paid") {
                this.$("#course-details-box-payment").show();
            } else {
                this.$("#course-details-box-payment").hide();
            }

            if (this.mAccessMode == "private") {
                this.$("#course-details-box-private").show();
            } else {
                this.$("#course-details-box-private").hide();
            }

            this.model.set(data, { silent: true });
            this.unsavedSettings = true;

            $("#course-settings-save-btn").removeClass("disabled");
            this.$("#course-access-settings-footer").slideDown();

            _Router.setConfirmLeave(str.edit_cancel,
                _.bind(this.options.parentView.options.parentView.saveCourseDetails, this.options.parentView.options.parentView),
                function () { CourseCollection.reset(); });
        },

        canBeCC: function ()
        {
            return (this.model.get("course_privacy") == 1) && (this.model.get("course_access") == 3) && (!this.model.get("course_price"));
        },

        linkPaypal: function ()
        {
            SocialUtils.addPaypalAccount("course:" + this.model.get("id"), (function ()
            {
                CourseCollection.reset();

                this.options.parentView.options.parentView.render();
            }).bind(this));
        }
    });
});
