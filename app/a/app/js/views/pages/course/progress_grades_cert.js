/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/course/progress_grades_cert.html", "i18n!nls/strings", "app/tools/linkhandler",
        "app/views/dialogs/yes_no"],
    function (tpl, str, LinkHandler, YesNoDialog)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),
        mGrades: null,

        events: {
            "click [data-action='issue-certificate']": "issueCertificate",
            "click [data-action='issue-certificate-all']": "issueCertificateAll",
            "click [data-action='revoke-certificate']": "revokeCertificate",
            "click [data-action='view-certificate']": "viewCertificate"
        },

        setGrages: function (grades)
        {
            this.mGrades = grades;

            return this;
        },
        
        setPaginationInfo: function (pagiInfo)
        {
            this.mPagiInfo = pagiInfo;

            return this;
        },

        render: function ()
        {
            if (!this.mGrades)
            {
                return this;
            }

            var cert = { };

            this.mGrades.get("certificates").each(function (c)
            {
                cert[parseInt(c.get("userid"))] = c.toJSON();
            });

            this.$el.html(this.template({ str: str, users: this.mGrades.get("users"), certs: cert, pagiInfo:this.mPagiInfo }));
            LinkHandler.setupView(this);

            return this;
        },

        issueCertificateAll: function (ev)
        {
            var issueAll = function ()
            {
                var users = this.mGrades.get("users"); 
                var certs = this.mGrades.get("certificates");
                for (i=0; i<users.length; i++)
                {
                  var cert = certs.findWhere({ userid: users[i].userid });

                  if (cert)
                  {
                    // Certificate already exists - toggle issue state.
                    if(i == (users.length - 1)) {
                      cert.save({ state: 1 }, { success: _.bind(this.render, this) });
                    } else {
                      cert.save({ state: 1 }, { success: function() {} });
                    }
                  }
                  else
                  {
                    // Create new certificate record
                    var data = {
                        userid: users[i].userid,
                        courseid: parseInt(this.mGrades.get("courseid")),
                        state: 1
                    };

                    if(i == (users.length - 1)) {
                      certs.create(data, { success: _.bind(this.render, this) });
                    } else {
                      certs.create(data, { success: function() {} });
                    }

                  }
             }
           };

             (new YesNoDialog({ header: str.issue_all_certs, message: str.issue_all_certs_msg, button_primary: str.button_issue_all, 
                                button_secondary: str.button_cancel, accepted: _.bind(issueAll, this) })).render(); 
        },

        issueCertificate: function (ev)
        {
            ev.preventDefault();

            if (!this.mGrades)
            {
                return;
            }

            var issue = function ()
            {
                var certs = this.mGrades.get("certificates"),
                    userid = parseInt($(ev.target).attr("data-user-id"));

                var cert = certs.findWhere({ userid: userid });

                if (cert)
                {
                    // Certificate already exists - toggle issue state.

                    cert.save({ state: 1 }, { success: _.bind(this.render, this) });
                }
                else
                {
                    // Create new certificate record

                    var data = {
                        userid: userid,
                        courseid: parseInt(this.mGrades.get("courseid")),
                        state: 1
                    };

                    certs.create(data, { success: _.bind(this.render, this) });
                }
            };

            if (!parseInt($(ev.target).attr("data-user-all-submitted")))
            {
                (new YesNoDialog({ message: str.course_not_completed_msg, accepted: _.bind(issue, this) })).render();
            }
            else
            {
                issue.apply(this);
            }
        },

        revokeCertificate: function (ev)
        {
            ev.preventDefault();

            if (this.mGrades)
            {
                this.mGrades.get("certificates")
                    .findWhere({ userid: parseInt($(ev.target).attr("data-user-id")) })
                    .save({ state: 0 }, { success: _.bind(this.render, this) });
            }
        },

        viewCertificate: function (ev)
        {
            ev.preventDefault();

            if (this.mGrades)
            {
                var cert = this.mGrades.get("certificates").get(parseInt($(ev.target).attr("data-cert-id")));

                if (cert)
                {
                    var certUrl = EliademyUrl + "/cert/" + cert.get("hashurl") + ".pdf";

                    window.open(certUrl);
                }
                else
                {
                    console.warn("Certificate model not found!");
                }
            }
        }
    }));
});
