/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/section_tasks.html", "i18n!nls/strings", "backbone"],
    function (tpl, str)
{
    return Backbone.View.extend({
        template: _.template(tpl),

        render: function () {
            $("#course-section-task-block").html(this.template({
                tasks: this.tasks,
                edit_mode: this.em,
                str: str
            }));
        },

        resetTasks: function (tasks) {
            if (tasks instanceof Array) {
                this.tasks = tasks.slice();
            } else {
                this.tasks = [ ];
            }

            this.taskOps = [ ];
        },

        addTask: function (task) {
            var tsk = task.toJSON();
            tsk.id = tsk.instanceid;
            tsk.modname = tsk.modulename;

            this.tasks.push(tsk);
            this.taskOps.push({"op": "add", "task": task});
        },

        removeTask: function (task) {
            var i = 0, id = task.get("instanceid");

            while (this.tasks.length > i) {
                if (this.tasks[i].id == id) {
                    this.tasks.splice(i, 1);
                } else {
                    i++;
                }
            }

            this.taskOps.push({"op": "remove", "task": task});
        },

        saveTasks: function (sectionid, overviewid) {
            var doneTasks = { }, saving = false;

            for (var i=this.taskOps.length-1; i>=0; i--) {
                if (!doneTasks[this.taskOps[i].task.get("instanceid")]) {
                    doneTasks[this.taskOps[i].task.get("instanceid")] = 1;
                    saving = true;

                    if (this.taskOps[i].op == "add") {
                        this.taskOps[i].task.save({ sectionid: sectionid });
                    } else {
                        this.taskOps[i].task.save({ sectionid: overviewid });
                    }
                }
            }

            return saving;
        }
    });
});
