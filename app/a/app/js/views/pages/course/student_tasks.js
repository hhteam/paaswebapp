/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/student_tasks.html", "i18n!nls/strings", "app/tools/linkhandler", "app/tools/dataloader", "backbone"],

    function (tpl, str, LinkHandler, DataLoader)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),
        mCourseView: null,

        bodyClass: "pagelayout-course path-theme path-theme-monorail gecko dir-ltr lang-en pagelayout-incourse jsenabled",
        bodyId: "page-theme-monorail-tasks",

        setCourseView: function (view)
        {
            this.mCourseView = view;

            return this;
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: this.mCourseView.model.get("assignments") }, function (assignments)
            {
                var done = [ ], not_done = [ ];

                assignments.each(function (it)
                {
                    var item = it.toJSON();

                    // Deadline, Date object
                    item.deadline = item.duedate ? item.duedate : null;
                    // Number of days after deadline
                    //item.days = Math.ceil((new Date()).getTime() / 86400000) - Math.ceil(item.duedate / 86400);
                    item.dategraded = item.grades[0].dategraded;

                    // No-submissions items are completed automatically after
                    // deadline.
                    if (item.haveSubmissions || ((item.duedate > 0) && item.nosubmissions && item.duedate < ((new Date()).getTime() / 1000)))
                    {
                        done.push(item);
                    }
                    else
                    {
                        not_done.push(item);
                    }
                }, this);

                this.$el.html(this.template({ str: str, done: done, not_done: not_done, timezone: _User.get("timezone") }));

                LinkHandler.setupView(this);
            });

            return this;
        }
    }));
});
