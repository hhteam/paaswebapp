/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/teacher_tasks.html", "i18n!nls/strings", "app/collections/task/returned", "app/tools/linkhandler",
        "app/tools/dataloader", "app/tools/utils"],
    function (tpl, str, ReturnedCollection, LinkHandler, DataLoader, Utils)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),
        mCourseView: null,

        bodyClass: "pagelayout-course path-theme path-theme-monorail gecko dir-ltr lang-en pagelayout-incourse jsenabled",
        bodyId: "page-theme-monorail-tasks",

        setCourseView: function (view)
        {
            this.mCourseView = view;

            return this;
        },

        initialize: function ()
        {
            ReturnedCollection.on("updated", this.render, this);

            return this;
        },

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            // TODO: ReturnedCollection next in line for re-factoring...
            if (_Router.currentPage().currentsub != "tasks")
            {
                return;
            }

            DataLoader.exec({ context: this, collection: this.mCourseView.model.get("assignments") }, function (assignments)
            {
                var graded = [ ], ungraded = [ ], studentCount = 0;

                this.mCourseView.model.get("participants").each(function (part) {
                    if (!part.get("manager") && !part.get("teacher")) {
                        studentCount++;
                    }
                });

                assignments.each(function (it)
                {
                    var item = it.toJSON();

                    item.count = studentCount;
                    item.returnedCount = item.submitcount;
                    //item.days = Math.ceil((new Date()).getTime() / 86400000) - Math.ceil(item.duedate / 86400);
                    item.dategraded = Object.prototype.toString.call(item.grades) == "[object Array]" && item.grades.length > 0 ? item.grades[0].dategraded : null;
                    item.deadline = item.duedate ? item.duedate : null;
                    item.pastDeadline = (item.duedate > 0) ?  (item.duedate < ((new Date()).getTime() / 1000)) : false;

                    var ret = ReturnedCollection.where({ assignid: item.id });

                    if (ret.length > 0)
                    {
                        var allGraded = true, lastGrade = 0;

                        _.each(ret, function (m)
                        {
                            var mData = m.toJSON();

                            if (!mData.grade)
                            {
                                allGraded = false;
                            }
                            else
                            {
                                if (lastGrade < mData.dategraded)
                                {
                                    lastGrade = mData.dategraded;
                                }
                            }
                        });
                        if (((allGraded) && (item.count != 0)) || (item.pastDeadline && item.nosubmissions))
                        {
                            graded.push(item);
                        }
                        else
                        {
                            ungraded.push(item);
                        }
                    }
                    else
                    {
                        if (item.modulename == "quiz")
                        {
                            if (item.submitcount && item.submitcount == item.count && (item.count != 0))
                            {
                                graded.push(item);
                            }
                            else
                            {
                                ungraded.push(item);
                            }
                        }
                        else
                        {
                            if(((item.count != 0) && (item.count == item.gradedcount)) || (item.pastDeadline && item.nosubmissions)) {
                                graded.push(item);
                            } else {
                                ungraded.push(item);
                            }
                        }
                    }
                }, this);

                this.$el.html(this.template({ str: str, graded: graded, ungraded: ungraded, timezone: _User.get("timezone"), 
                    course: this.mCourseView.model.toJSON(), isPaas : _User.get('isPaas') }));

                LinkHandler.setupView(this);
            });

            return this;
        },

        newTask: function () {
            _Router.setConfirmLeave(null, null, null);
            _Router.navigate("/tasks/new/" + this.mCourseView.model.get("code"), { trigger: true });
        },

        newQuiz: function () {
            _Router.setConfirmLeave(null, null, null);
            _Router.navigate("/quizzes/new/" + this.mCourseView.model.get("code"), { trigger: true });
        }
    }));
});
