/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/certificate.html", "i18n!nls/strings", "app/tools/linkhandler", "app/tools/warnings",
        "app/tools/utils", "app/collections/course/coursesshort", "app/collections/course/coursesfree", "app/tools/social_utils",
        "app/collections/course/courses"],
    function (tpl, str, LinkHandler, WarningTool, Utils, CourseShortCollection, FreeCourseCollection, SocialUtils, CourseCollection)
{
    var centerText = function (ctx, text, x, y, w, fontSize, fontName)
    {
        ctx.font = fontSize + "px " + fontName;

        var m = ctx.measureText(text);

        while (m.width > w)
        {
            fontSize--;
            ctx.font = fontSize + "px " + fontName;
            m = ctx.measureText(text);
        }

        ctx.fillText(text, (w - m.width) / 2 + x, y);
    };

    var fitImage = function (ctx, image, x, y, w, h)
    {
        var ax = image.width / w,
            ay = image.height / h,
            iw = image.width / ax,
            ih = image.height / ax;

        if (iw > w || ih > h)
        {
            iw = image.width / ay;
            ih = image.height / ay;
        }

        ctx.drawImage(image, (w - iw) / 2 + x, (h - ih) / 2 + y, iw, ih);
    };

    return Backbone.View.extend(
    {
        el: ".course-content",
        template: _.template(tpl),
        mBackground: null,
        mCustomBackground: null,
        mCustomEditMode: null,
        mCustomEditElement: null,

        events: {
            "click [data-call]": Utils.callMapper,
            "click input[name=\"cert_mode\"]": "modeUpdated",

            "click #cert-design-upload-logo": "uploadLogo",
            "click #cert-design-upload-sig": "uploadSig",
            "click #cert-design-clear-logo": "clearLogo",
            "click #cert-design-clear-sig": "clearSig",

            "mousemove #cert-custom-canvas": "customCertMouseMove",
            "click #cert-custom-canvas": "customCertClick"
        },

        render: function ()
        {
            if (!this.model.get("can_edit")) {
                return;
            }

            var courseData = this.model.toJSON(),
                info = {};

            if (courseData.cert_custom) {
                try {
                    info = JSON.parse(courseData.cert_custom_info);
                } catch (err) {}

                if (!(info instanceof Object)) {
                    info = {};
                }
            }

            this.$el.html(this.template({ str: str, course: courseData, customCertInfo: info, isPaas : _User.get('isPaas') }));

            LinkHandler.setupView(this);
            Utils.setupPopups(this.$el);

            if (courseData.cert_custom) {
                if (info.thumbnail) {
                    if (!this.mCustomBackground) {
                        this.mCustomBackground = document.createElement("img");
                        this.mCustomBackground.src = info.thumbnail;
                    }

                    this.drawCustomPreview(info, false, false);
                }
            } else {
                if (!this.mBackground) {
                    this.mBackground = document.createElement("img");
                    this.mBackground.src = RootDir + "app/img/cert_preview.png";
                }

                this.drawPreview();
            }

            return this;
        },

        drawPreview: function ()
        {
            var courseData = this.model.toJSON();

            // If background image is not yet loaded, wait and redraw
            if (!this.mBackground.complete)
            {
                this.mBackground.onload = _.bind(this.drawPreview, this);
                return;
            }

            if (courseData.cert_sigpic && !this.$("#cert-design-sigpic").get(0).complete)
            {
                this.$("#cert-design-sigpic").get(0).onload = _.bind(this.drawPreview, this);
                return;
            }

            if (courseData.cert_orglogo && !this.$("#cert-design-orglogo").get(0).complete)
            {
                this.$("#cert-design-orglogo").get(0).onload = _.bind(this.drawPreview, this);
                return;
            }

            var ctx = this.$("#cert-design-canvas").get(0).getContext("2d");

            ctx.drawImage(this.mBackground, 0, 0);

            ctx.strokeStyle = "#000102";

            centerText(ctx, str.certificate_title, 0, 100, 660, 28, "Cinzel");
            centerText(ctx, str.is_awarded, 0, 135, 660, 22, "Cinzel");
            centerText(ctx, str.cert_dummy_stud, 0, 200, 660, 42, "Tangerine");
            centerText(ctx, str.for_completing, 0, 245, 660, 20, "Cinzel");
            centerText(ctx, courseData.fullname, 50, 275, 560, 20, "Cinzel");
            centerText(ctx, moment().format("D MMMM, YYYY"), 50, 390, 150, 10, "Cinzel");
            centerText(ctx, (new String(Math.random())).substr(2), 50, 400, 150, 10, "Cinzel");
            centerText(ctx, courseData.teacher_name, 463, 380, 150, 10, "Cinzel");
            centerText(ctx, courseData.teacher_title, 463, 390, 150, 10, "Cinzel");
            centerText(ctx, _User.get("institution"), 463, 400, 150, 10, "Cinzel");

            if (courseData.cert_orglogo)
            {
                fitImage(ctx, this.$("#cert-design-orglogo").get(0), 69, 295, 110, 70);
            }

            if (courseData.cert_sigpic)
            {
                fitImage(ctx, this.$("#cert-design-sigpic").get(0), 481, 295, 110, 70);
            }
        },

        drawCustomPreview: function (info, noname, nodate) {
            // If background image is not yet loaded, wait and redraw
            if (!this.mCustomBackground.complete)
            {
                this.mCustomBackground.onload = _.bind(this.drawCustomPreview, this, info, false, false);
                return;
            }

            this.$("#cert-custom-canvas").attr({ width: this.mCustomBackground.width, height: this.mCustomBackground.height }).show();

            var ctx = this.$("#cert-custom-canvas").get(0).getContext("2d");

            ctx.drawImage(this.mCustomBackground, 0, 0);

            if (info.student_x && info.student_y && !noname) {
                ctx.strokeStyle = "#000000";
                ctx.font = "18px Cinzel";
                ctx.fillText(str.cert_dummy_stud, (info.student_x / 100) * $("#cert-custom-canvas").width(), (info.student_y / 100) * $("#cert-custom-canvas").height());
            }

            if (info.date_x && info.date_y && !nodate) {
                ctx.strokeStyle = "#000000";
                ctx.font = "12px Cinzel";
                ctx.fillText(moment().format("D MMMM, YYYY"), (info.date_x / 100) * $("#cert-custom-canvas").width(), (info.date_y / 100) * $("#cert-custom-canvas").height());
            }

            // TODO: choose font, size, color. Also choose text alignment
            // (draw a box?)
        },

        uploadLogo: function (ev)
        {
            ev.preventDefault(ev);

            this.uploadImage("cert_orglogo");
        },

        uploadSig: function (ev)
        {
            ev.preventDefault(ev);

            this.uploadImage("cert_sigpic");
        },

        clearLogo: function (ev)
        {
            ev.preventDefault(ev);

            this.setImage("cert_orglogo", "");
        },

        clearSig: function (ev)
        {
            ev.preventDefault(ev);

            this.setImage("cert_sigpic", "");
        },

        uploadImage: function (field)
        {
            var thisThis = this;

            _Uploader.owner(this).restrictFileType('image/*').setFileUploadLimit(0.5 * 1024 * 1024).exec(
                function (filedata)     //add
                { },
                function (data)         //error
                { },
                function (filedata)     //done
                {
                    var extension = filedata.filename.split('.').pop().toLowerCase();

                    if (['jpeg', 'jpg', 'png', 'gif'].indexOf(extension) == -1)
                    {
                        WarningTool.processWarnings({msgtype:'error', message:str.picture_error});
                        return;
                    }

                    thisThis.setImage(field, filedata.filename);
                });
        },

        setImage: function (field, file)
        {
            var thisThis = this;

            $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_set_course_file.php",
                    data: { file: file, field: field, course: thisThis.model.get("id") },
                    success: function (data)
                    {
                        thisThis.model.set(field, data);
                        thisThis.render();
                    },
                    error: function (data)
                    {
                        console.warn("Network error");
                    }
            });
        },

        updateMode: function ()
        {
            this.model.save({ cert_enabled: parseInt(this.$("input[name=\"cert_mode\"]:checked").val()) || 0 },
                { success: function ()
                {
                    $("#cert-mode-update-btn").addClass("disabled");

                    // XXX: where is it?
                    CourseShortCollection.LearningOngoing.reset();
                    CourseShortCollection.LearningCompleted.reset();
                    CourseShortCollection.TeachingOngoing.reset();
                    CourseShortCollection.TeachingCompleted.reset();

                    FreeCourseCollection.reset();
                } });
        },

        modeUpdated: function ()
        {
            this.$("#cert-mode-update-btn").removeClass("disabled");
        },

        linkPaypal: function ()
        {
            SocialUtils.addPaypalAccount("course:" + this.model.get("id"), (function ()
            {
                CourseCollection.reset();

                this.options.parentView.render();
            }).bind(this));
        },

        setDefault: function () {
            if (this.model.get("cohortid")) {
                this.model.save({"cert_custom": 0});
            } else {
                this.model.set("cert_custom", 0);
            }

            this.render();
        },

        setCustom: function () {
            if (this.model.get("cohortid")) {
                this.model.save({"cert_custom": 1});
            } else {
                this.model.set("cert_custom", 1);
            }

            this.render();
        },

        uploadCustomCert: function () {
            var thisThis = this;

            _Uploader.owner(this).restrictFileType('image/*').setFileUploadLimit(10 * 1024 * 1024).exec(
                function (filedata)     //add
                { },
                function (data)         //error
                { },
                function (filedata)     //done
                {
                    var extension = filedata.filename.split('.').pop().toLowerCase();

                    if (['jpeg', 'jpg', 'png'].indexOf(extension) == -1)
                    {
                        WarningTool.processWarnings({msgtype:'error', message:str.picture_error});
                        return;
                    }

                    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_upload_custom_certificate.php",
                            data: { file: filedata.filename, course: thisThis.model.get("id") },
                            success: function (data)
                            {
                                try {
                                    data = JSON.parse(data);
                                } catch (err) {}

                                var info = {};

                                try {
                                    info = JSON.parse(thisThis.model.get("cert_custom_info"));
                                } catch (err) { }

                                if (!(info instanceof Object)) {
                                    info = {};
                                }

                                info.template_id = data.id;
                                info.template = data.original;
                                info.thumbnail = data.thumbnail;
                                thisThis.model.save({"cert_custom_info": JSON.stringify(info)});

                                thisThis.mCustomBackground = null;
                                thisThis.render();
                            },
                            error: function (data)
                            {
                                console.warn("Network error");
                            }
                    });
                });
        },

        placeStudentName: function (ev, target) {
            if (this.mCustomEditMode) {
                this.mCustomEditMode = null;

                this.$("#cert-custom-canvas").css({"cursor": "default"});
                this.$("#cert-custom-placeholder").get(0).removeChild(this.mCustomEditElement);
                $(target).removeClass("active");

                this.mCustomEditElement = null;
            } else {
                this.mCustomEditMode = 1;

                this.$("#cert-custom-canvas").css({"cursor": "move"});
                $(target).addClass("active");

                this.mCustomEditElement = document.createElement("div");
                this.mCustomEditElement.appendChild(document.createTextNode(str.cert_dummy_stud));
                this.mCustomEditElement.style.position = "absolute";
                this.mCustomEditElement.style.zIndex = "1000";
                this.mCustomEditElement.style.fontSize = "18px";
                this.mCustomEditElement.style.cursor = "crosshair";
                this.mCustomEditElement.style.border = "1px dashed #666";
                this.mCustomEditElement.style.background = "#EEE";

                this.$("#cert-custom-placeholder").get(0).appendChild(this.mCustomEditElement);

                var info = {};

                try {
                    info = JSON.parse(this.model.get("cert_custom_info"));
                } catch (err) {}

                if (!(info instanceof Object)) {
                    info = {};
                }

                if (info.student_x && info.student_y) {
                    this.mCustomEditElement.style.marginLeft = Math.round(info.student_x / 100.0 * $("#cert-custom-canvas").width()) + "px";
                    this.mCustomEditElement.style.marginTop = Math.round(info.student_y / 100.0 * $("#cert-custom-canvas").height() - 20) + "px";
                } else {
                    this.mCustomEditElement.style.marginLeft = Math.round($("#cert-custom-canvas").width() / 2 - $(this.mCustomEditElement).width() / 2) + "px";
                    this.mCustomEditElement.style.marginTop = Math.round($("#cert-custom-canvas").height() / 2 - $(this.mCustomEditElement).height() / 2) + "px";
                }

                this.drawCustomPreview(info, true, false);
            }
        },

        placeIssueDate: function (ev, target) {
            if (this.mCustomEditMode) {
                this.mCustomEditMode = null;

                this.$("#cert-custom-canvas").css({"cursor": "default"});
                this.$("#cert-custom-placeholder").get(0).removeChild(this.mCustomEditElement);
                $(target).removeClass("active");

                this.mCustomEditElement = null;
            } else {
                this.mCustomEditMode = 2;

                this.$("#cert-custom-canvas").css({"cursor": "move"});
                $(target).addClass("active");

                this.mCustomEditElement = document.createElement("div");
                this.mCustomEditElement.appendChild(document.createTextNode(moment().format("D MMMM, YYYY")));
                this.mCustomEditElement.style.position = "absolute";
                this.mCustomEditElement.style.zIndex = "1000";
                this.mCustomEditElement.style.fontSize = "12px";
                this.mCustomEditElement.style.cursor = "crosshair";
                this.mCustomEditElement.style.border = "1px dashed #666";
                this.mCustomEditElement.style.background = "#EEE";

                this.$("#cert-custom-placeholder").get(0).appendChild(this.mCustomEditElement);

                var info = {};

                try {
                    info = JSON.parse(this.model.get("cert_custom_info"));
                } catch (err) {}

                if (!(info instanceof Object)) {
                    info = {};
                }

                if (info.date_x && info.date_y) {
                    this.mCustomEditElement.style.marginLeft = Math.round(info.date_x / 100.0 * $("#cert-custom-canvas").width()) + "px";
                    this.mCustomEditElement.style.marginTop = Math.round(info.date_y / 100.0 * $("#cert-custom-canvas").height() - 20) + "px";
                } else {
                    this.mCustomEditElement.style.marginLeft = Math.round($("#cert-custom-canvas").width() / 2 - $(this.mCustomEditElement).width() / 2) + "px";
                    this.mCustomEditElement.style.marginTop = Math.round($("#cert-custom-canvas").height() / 2 - $(this.mCustomEditElement).height() / 2) + "px";
                }

                this.drawCustomPreview(info, false, true);
            }
        },

        customCertMouseMove: function (ev) {
            if (this.mCustomEditMode) {
                var that = this;

                that.mCustomEditElement.style.marginLeft = (ev.offsetX + 5) + "px";
                that.mCustomEditElement.style.marginTop = (ev.offsetY - 25) + "px";
            }
        },

        customCertClick: function () {
            if (!this.mCustomEditMode) {
                return;
            }

            var info = {};

            try {
                info = JSON.parse(this.model.get("cert_custom_info"));
            } catch (err) { }

            if (!(info instanceof Object)) {
                info = {};
            }

            switch (this.mCustomEditMode) {
                case 1:
                    info.student_x = ((parseInt(this.mCustomEditElement.style.marginLeft)) / $("#cert-custom-canvas").width()) * 100;
                    info.student_y = ((parseInt(this.mCustomEditElement.style.marginTop) + 18) / $("#cert-custom-canvas").height()) * 100;
                    break;

                case 2:
                    info.date_x = ((parseInt(this.mCustomEditElement.style.marginLeft)) / $("#cert-custom-canvas").width()) * 100;
                    info.date_y = ((parseInt(this.mCustomEditElement.style.marginTop) + 18) / $("#cert-custom-canvas").height()) * 100;
                    break;

                default:
                    console.warn("Invalid custom certificate edit mode (" + this.mCustomEditMode + ")");
                    return;
            }

            this.model.save({"cert_custom_info": JSON.stringify(info)});

            this.mCustomEditMode = null;

            this.$("#cert-custom-canvas").css({"cursor": "default"});
            this.$("#cert-custom-placeholder").get(0).removeChild(this.mCustomEditElement);
            $("#cert-custom-place-student-btn").removeClass("active");

            this.mCustomEditElement = null;

            this.render();
        }
    });
});
