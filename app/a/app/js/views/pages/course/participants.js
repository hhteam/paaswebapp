/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/participants_teacher.html", "text!templates/pages/course/participants_student.html", "i18n!nls/strings",
        "app/tools/utils", "app/tools/linkhandler", "app/tools/dataloader", "app/views/dialogs/messagebox", "app/views/dialogs/send_message",
        "app/views/dialogs/invite_email", "app/views/dialogs/enroll_users", "app/views/dialogs/yes_no", "app/collections/admin/usertags",
        "app/views/dialogview", "moment"],

function (tplTeacher, tplStudent, str, Utils, LinkHandler, DataLoader, MessageBox, SendMessageDialog, InviteEmailDialog, EnrollUsersDialog,
            YesNoDialog, UserTagsCollection, DialogView)
{
    return Backbone.View.extend(
    {
        el: ".course-content",
        mEditMode: false,
        mCohortAdmin: false,
        mCohortUser: false,
        mCohortId: 0,
        mMoocCourse: false,

        bodyId: "page-theme-monorail-participants",

        templateTeacher: _.template(tplTeacher),
        templateStudent: _.template(tplStudent),

        STUDENTS_PER_PAGE: 24,
        mCurrentPage: 0,

        INVITES_PER_PAGE: 10,
        mCurrentInvitePage: 0,

        sendMessage: function(ev)
        {
            (new SendMessageDialog({ courseid: this.model.get("id"), userids:[] })).render();
            ev.preventDefault();
        },

        events: {
            "click [data-call]": Utils.callMapper,
            "change [data-call][data-toggle=\"toggle\"]": Utils.callMapper
        },

        initialize: function ()
        {
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                cohortid = _User.get("cohorts");

                if (typeof cohortid == "string" && cohortid.indexOf(",") != -1)
                {
                    cohortid = cohortid.split(",")[0];
                    this.mCohortUser = true;
                    this.mCohortId = cohortid;
                }
            } else {
                this.mCohortAdmin = true;
                this.mCohortId = cohortid;
            }
        },

        render: function ()
        {
            if (!this.model.get("can_edit") && this.model.get("participants_hidden")) {
                // Nothing to see here.
                return;
            }

            DataLoader.exec({ context: this, collection: this.model.get("participants") }, function (participants)
            {
                var students = [], teachers = [], managers = [];

                participants.each(function (usr)
                {
                    var user = usr.toJSON();

                    if (user.manager && !user.teacher && (user.cohortadminrole == 1 || user.cohortadminrole == 2)) {
                        // Skip managers.
                        return;
                    }

                    user.cid = usr.cid;
                    user.myself = usr.id == _User.get("id");

                    if (user.teacher) {
                        teachers.push(user);
                    } else if (user.manager) {
                        managers.push(user);
                    } else {
                        students.push(user);
                    }
                }, this);

                if (this.model.get("can_edit"))
                {
                    // Teacher view.
                    var cohortCourse = this.model.get("cohortid") > 0 ? true: false;

                    DataLoader.exec({ collection: this.model.get("invitedUsers"), context: this }, function (invited)
                    {
                        var inv = [];

                        invited.each(function (i) {
                            var ii = i.toJSON();

                            ii.cid = i.cid;

                            inv.push(ii);
                        });

                        inv.sort(function (a, b) {
                            if (a.status == b.status) {
                                return b.invitedwhen - a.invitedwhen;
                            } else {
                                return b.status - a.status;
                            }
                        });

                        this.$el.html(this.templateTeacher({
                            str: str,
                            students: students,
                            teachers: teachers,
                            managers: managers,
                            invited: inv,

                            currentPage: this.mCurrentPage,
                            studentsPerPage: this.STUDENTS_PER_PAGE,
                            firstStudent: this.mCurrentPage * this.STUDENTS_PER_PAGE,
                            pageCount: Math.ceil(students.length / this.STUDENTS_PER_PAGE),
                            showManageEnrollments: this.mCohortAdmin,

                            /*
                            showEnrollColleagues: cohortCourse &&
                                                    ((this.model.get("ccprivate") && this.mCohortUser) ||
                                                    (!this.mCohortUser && this.model.get("ccprivateext")) ||
                                                      this.mCohortAdmin),
                            */

                            showSendMessage: true,
                            course: this.model.toJSON(),

                            currentInvitePage: this.mCurrentInvitePage,
                            invitesPerPage: this.INVITES_PER_PAGE,
                            firstInvite: this.mCurrentInvitePage * this.INVITES_PER_PAGE,
                            invitePageCount: Math.ceil(inv.length / this.INVITES_PER_PAGE), 
                            isPaas : _User.get('isPaas')
                        }));

                        LinkHandler.setupView(this);
                        Utils.setupPopups(this.$el);
                        this.$('[data-toggle="popover"]').popover({ trigger: "hover", placement: "top" });
                    });
                }
                else
                {
                    // Student view
                    this.$el.html(this.templateStudent({
                        str: str,
                        students: students,
                        teachers: teachers,
                        managers: managers,
                        currentPage: this.mCurrentPage,
                        studentsPerPage: this.STUDENTS_PER_PAGE,
                        firstStudent: this.mCurrentPage * this.STUDENTS_PER_PAGE,
                        pageCount: Math.ceil(students.length / this.STUDENTS_PER_PAGE)
                    }));

                    LinkHandler.setupView(this);
                    this.$('[data-toggle="popover"]').popover({ trigger: "hover", placement: "top" });
                }
            });
        },

        inviteEmail: function(ev)
        {
            var thisThis = this;

            this.mMoocCourse = ((this.model.get("course_privacy") == 1) && (this.model.get("course_access") == 3));

            if (this.model.get("cohortid")) {
                DataLoader.exec({ context: this, collection: UserTagsCollection }, function (userTags)
                {
                    (new InviteEmailDialog({
                        cid: this.model.get("id"),
                        code: this.model.get("inviteCode"),
                        moocCourse: this.mMoocCourse,
                        finished: function (stat) {
                            if (stat) {
                                thisThis.model.get("invitedUsers").reset();
                                thisThis.render();
                            }
                        },
                        userTags: userTags
                    })).render();
                });
            } else {
                (new InviteEmailDialog({
                    cid: this.model.get("id"),
                    code: this.model.get("inviteCode"),
                    moocCourse: this.mMoocCourse,
                    finished: function (stat) {
                        if (stat) {
                            thisThis.model.get("invitedUsers").reset();
                            thisThis.render();
                        }
                    }
                })).render();
            }

            ev.preventDefault();
        },
        
        manageEnrolments : function(ev) {
            _Router.navigate("/course-manager", { trigger: true });
            ev.preventDefault();
        },

        /*
        enrollColleagues: function(ev)
        {
            (new EnrollUsersDialog({
                context : this,
                cohortid : this.model.get("cohortid"),//use course cohort id here, not user cohort
                courseid : this.model.get("id"),
                fromTop: 30,
                accepted : function() {
                    this.model.get("participants").reset();
                    this.render();
                },
                rejected : function() {
                    this.model.get("participants").reset();
                    this.render();
                }
            })).render();
        },
        */

        disablePage: function (ev, target)
        {
            var courseView = this.options.parentView;

            this.model.save({ participants_hidden: $(target).is(":checked") ? 0 : 1 }, { success: function ()
            {
                courseView.reloadCourse();
                courseView.render();
            }, patch: true });
        },

        showPage: function (ev, target)
        {
            this.mCurrentPage = parseInt(target.getAttribute("data-page")) || 0;
            this.render();
        },

        showInvitePage: function (ev, target)
        {
            this.mCurrentInvitePage = parseInt(target.getAttribute("data-page")) || 0;
            this.render();
        },

        changeRole: function (ev, target)
        {
            var participant = this.model.get("participants").get(target.getAttribute("data-cid"));

            if (!participant) {
                console.warn("Failed to find participant?!");
                return;
            }

            var thisThis = this;

            participant.save({ role: target.getAttribute("data-role") }, { patch: true, success: function () {
                thisThis.model.get("participants").reset();
                thisThis.render();
            }});
        },

        unenroll: function (ev, target)
        {
            if (this.model.get("course_privacy") == 1 && this.model.get("course_access") == 3 && parseFloat(this.model.get("course_price")) > 0) {
                (new MessageBox({ message: str.label_cannot_unenroll_from_public_courses })).render();
                return;
            }

            var participant = this.model.get("participants").get(target.getAttribute("data-cid"));

            if (!participant) {
                console.warn("Failed to find participant?!");
                return;
            }

            var thisThis = this;

            (new YesNoDialog({ header: str.remove, message: str.label_confirm_remove_user, button_primary: str.remove, button_secondary: str.button_cancel,
                accepted: function ()
                {
                    participant.destroy({ success: function () {
                        thisThis.model.get("participants").reset();
                        thisThis.render();
                    }});
                }})).render();
        },

        cancelInvitation: function (ev, target)
        {
            var inv = this.model.get("invitedUsers").get(target.getAttribute("data-cid"));

            if (!inv) {
                console.warn("Invitation not found!?");
                return;
            }

            var thisThis = this;

            (new YesNoDialog({ header: str.remove, message: str.label_confirm_remove_user, button_primary: str.remove, button_secondary: str.button_cancel,
                accepted: function ()
                {
                    inv.destroy({ success: function () {
                        thisThis.model.get("invitedUsers");
                        thisThis.render();
                    }});
                }})).render();
        },

        copyInvitationLink: function (ev, target) {
            var inv = this.model.get("invitedUsers").get(target.getAttribute("data-cid"));

            if (!inv) {
                console.warn("Invitation not found!?");
                return;
            }

            var dlg = DialogView.extend({
                dropDownMode: true,
                renderDialog: function () {
                    this.$body.html("<div class=\"modal-header\"><h3>" + str.copy_invite_link + "</h3></div><div class=\"modal-body\"><p style=\"max-width: 550px;\">" + str.copy_invite_link_msg +
                        "</p><p><input type=\"text\" value=\"" + inv.get("link") + "\" readonly=\"readonly\" onfocus=\"$(this).select()\" class=\"input-xxlarge\"></p></div>");
                }
            });

            (new dlg({moveAbove: $(target).parent().parent().parent().find("a.dropdown-toggle") })).render();
        }
    });
});
