/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/forumtopics.html", "i18n!nls/strings", "app/tools/dataloader", "app/tools/utils",
        "app/views/pages/forum/posts", "app/tools/linkhandler", "app/views/pages/forum/editpost", "app/models/forum/discussion",
        "app/collections/forum/discussions", "app/views/dialogs/forumdetails", "app/views/dialogs/delete_forum", "moment"],
    function (tpl, str, DataLoader, Utils, ForumPostView, LinkHandler, EditPostView, DiscussionModel, DiscussionCollection,
                ForumDetailsDialog, DeleteForumDialog, moment)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: ".course-content",

        bodyClass: "pagelayout-course path-theme path-theme-monorail gecko dir-ltr lang-en pagelayout-incourse jsenabled",
        bodyId: "page-theme-monorail-discussions",

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: this.options.parentView.model.get("forums") }, function (forums)
            {
                this.model = forums.get(this.options.id);

                if (!this.model)
                {
                    // Tying to view forum that doesn't exist!
                    _Router.navigate("/courses/" + this.options.parentView.model.get("code") + "/discussions", { trigger: true });
                    return;
                }

                forum = this.model.toJSON();

                // If you can edit course, you can edit forums.
                forum.can_edit = this.options.parentView.model.get("can_edit");

                var discs = [ ];

                for (var i=0; i<forum.discussions.length; i++)
                {
                    discs.push(DiscussionCollection.get(forum.discussions[i]).toJSON());
                }

                this.$el.html(this.template({ str: str, forum: forum, discussions: discs, course: this.options.parentView.model.toJSON(),
                    moment: moment, selected_topic: parseInt(this.options.topicId), isPaas : _User.get('isPaas') }));

                LinkHandler.setupView(this);

                if (parseInt(this.options.topicId))
                {
                    this.currentView = new ForumPostView({ id: this.options.topicId, forumId: this.options.id, course: this.options.parentView.model, parentView: this });

                    this.currentView.render();

                    this.$("[data-discid=" + this.options.topicId + "]").parent().addClass("active");
                }
            });

            return this;
        },

        createNewTopic: function (ev, target)
        {
            var topic = new DiscussionModel({ forum: this.options.id, course: this.options.parentView.model.get("id") });
            var edit = new EditPostView({ model: topic, courseCode: this.options.parentView.model.get("code") });

            topic.once("sync", function ()
            {
                this.options.parentView.model.get("forums").reset();

                _Router.navigate("/courses/" + this.options.parentView.model.get("code") + "/discussions/" + this.options.id + "/" + topic.get("id"), { trigger: true });
            }, this);

            _Router.navigate("/courses/" + this.options.parentView.model.get("code") + "/discussions/" + this.options.id);

            edit.render();

            this.$("[data-discid]").parent().removeClass("active");
        },

        editForum: function ()
        {
            this.model.once("sync", function ()
            {
                this.options.parentView.model.get("forums").reset();
                this.render();
            }, this);

            (new ForumDetailsDialog({ model: this.model, header: str.forum_edit })).render();
        },

        removeForum: function ()
        {
            var that = this;

            (new DeleteForumDialog({ message: str.forum_remove_everything, context: this,
                accepted: function ()
                {
                  that.model.destroy({ success: function ()
                  {
                    that.options.parentView.model.get("forums").reset();
                    _Router.navigate("/courses/" + that.options.parentView.model.get("code") + "/discussions", { trigger: true });
                  }});
                },
                rejected: function ()
                {
                    // nothing
                }})).render();
        }
    });
});
