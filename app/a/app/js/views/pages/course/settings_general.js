/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/settings_general.html",
        "text!templates/dice.html", "i18n!nls/strings", "app/tools/utils",
        "app/tools/linkhandler", "app/views/dialogs/yes_no",
        "app/tools/warnings", "app/collections/course/categories",
        "app/tools/dataloader", "app/collections/countries", "datepicker"],
function( tpl, dice_tpl, str, Utils, LinkHandler, YesNoDialog, WarningTool,
          CategoryCollection, DataLoader, CountryCollection )
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        dice_template: _.template( dice_tpl ),
        unsavedSettings: false,
        countries: null,

        events: {
            "click [data-call]": Utils.callMapper,
            "change [data-field]": "settingsChanged",
            "change select#listCategory": "settingsChanged"
        },

        render: function ()
        {
            var data = this.model.toJSON();

            DataLoader.exec({ context: this, collection: CategoryCollection }, function (categories) {
                DataLoader.exec({ context: this, collection: CountryCollection }, function (countries) {
                    this.countries = countries;
                    var teachersList = this.model.get("participants").where({ teacher: true });
                    var managersList = this.model.get("participants").where({ manager: true, teacher: false });
                    $.merge(teachersList, managersList);

                    this.$el.html(this.template({
                        str: str,
                        course: data,
                        startdate: (data.startdate) ? moment(data.startdate / 1000, "X").format("YYYY-MM-DD") : null,
                        enddate: (data.enddate) ? moment(data.enddate / 1000, "X").format("YYYY-MM-DD") : null,
                        unsavedSettings: this.unsavedSettings,
                        categories: categories.toJSON(),
                        countries: countries.toJSON(),  // FIXME: remove after country selection removal
                        teacherList: teachersList,
                        languages: _User.get("langs")
                    }));

                    this.$('[data-toggle="popover"]').popover({ trigger: "hover" });
                    Utils.setupPopups(this.$el);

                    var that = this;

                    $('input#course-start-date-input')
                        .datepicker({ format: "yyyy-mm-dd", weekStart: 1, endDate : (data.enddate) ? new Date(data.enddate) : null, orientation: "auto top", autoclose: true })
                        .on('changeDate', function (ev)
                        {
                            that.model.set('startdate', ev.date.valueOf());
                            that.settingsChanged();
                            // XXX: can use datepair here...
                            $('input#course-end-date-input').datepicker("setStartDate" , new Date(ev.date.valueOf()));
                        });

                    $('input#course-end-date-input')
                        .datepicker({ format: "yyyy-mm-dd", weekStart: 1, startDate : (data.startdate) ? new Date(data.startdate) : null, orientation: "auto top", autoclose: true })
                        .on('changeDate', function (ev)
                        {
                            that.model.set('enddate', ev.date.valueOf());
                            that.settingsChanged();
                            // XXX: can use datepair here...
                            $('input#course-start-date-input').datepicker("setEndDate" , new Date(ev.date.valueOf()));
                        });


                    LinkHandler.setupView(this);

                    //----------------------------------------------------------
                    this.syncCountries();
                    this.model.off('change:course_country');
                    this.model.on('change:course_country',
                                   this.syncCountries, this );
                    this.model.on('change:course_country',
                                   this.settingsChanged, this );
                    // FIXME: all change tracking via the model
                    $('#course-country-select').on('change', function( e ) {
                        that.model.addCountry( this.selectedOptions[0].value );
                    } );
                    //----------------------------------------------------------
                });
            });
        },

        saveCourseSettings: function (ev, target)
        {
            this.options.parentView.options.parentView.saveCourseDetailsClicked();
        },

        deliveryChanged: function (ev, target)
        {
            if ($(target).hasClass("active"))
            {
                return;
            }
            var deliveryMode = parseInt(target.getAttribute("data-type"));
            var completed = this.model.get('completed');
            if(((deliveryMode == 1) || (deliveryMode == 2)) && completed) {
              this.model.toggleCourseStatus(0);
            }
            switch (deliveryMode)
            {
                case 1:
                    this.model.set('startdate', 0);
                    this.repaint(true);
                    break;

                case 2:
                    this.model.set('startdate', (new Date()).valueOf());
                    this.repaint(true);
                    break;

                case 3:
                    (new YesNoDialog({ header: str.course_end_confirmation_header, message: str.course_end_confirmation_body, context: this,
                        accepted: function ()
                        {
                            $('#edit-caption').html('');
                            this.model.toggleCourseStatus(1);
                            _Router.setConfirmLeave(null, null, null);
                            _Router.currentPage().pageRender();
                        },
                        rejected: function ()
                        {
                            this.repaint(false);
                        }})).render();
                    break;
            }
        },

        uploadThumbnail: function (ev, target)
        {
            _Uploader.owner(this).restrictFileType('image/*').exec(function () { }, function () { },
                function (filedata)
                {
                    var extension = filedata.filename.split('.').pop().toLowerCase();

                    if (['jpeg', 'jpg', 'png', 'gif'].indexOf(extension) == -1)
                    {
                        WarningTool.processWarnings({msgtype:'error', message:str.picture_error});
                        return;
                    }

                    this.model.set("coursebackgroundtn", MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + filedata.filename + '&update=' + new Date().getTime());
                    this.options.parentView.options.parentView.uploadedBackgroundtn = filedata.filename;
                    this.repaint(true);
                });
        },

        resetThumbnail: function (ev, target)
        {
            this.model.set("coursebackgroundtn", "");
            this.options.parentView.options.parentView.uploadedBackgroundtn = "default";
            this.repaint(true);
        },

        repaint: function (changes)
        {
            // Save changed fields to model before re-rendering.
            this.options.parentView.options.parentView.inlineEdit.commit({ dry: true });
            this.render();

            if (changes)
            {
                this.settingsChanged();
            }
        },

        settingsChanged: function ()
        {
            if (!this.model.get("wizard_mode") && !this.model.get("from_import") && !this.unsavedSettings)
            {
                this.unsavedSettings = true;
                this.$("#course-general-settings-footer").slideDown();
                $("#course-settings-save-btn").removeClass("disabled");

                _Router.setConfirmLeave(str.edit_cancel,
                    _.bind(this.options.parentView.options.parentView.saveCourseDetails, this.options.parentView.options.parentView),
                    _.bind(this.options.parentView.options.parentView.reloadCourse, this.options.parentView.options.parentView));
            }
        },

        //----------------------------------------------------------------------
        syncCountries: function()
        {
            var that = this,
                $dice = $('#course-country-multiselect .sel-country'),
                $add  = $('#course-country-add'),
                $opt  = $('#course-country-select option'),
                $sel  = $('#course-country-select'),
                $all  = $('#course-country-all'),
                nd = 0, no = 0, num_selected = 0;

            this.countries.each( function( cm ) {
                var id = cm.attributes.key,
                    name = cm.attributes.value,
                    has_die = nd in $dice &&
                                    $dice.eq( nd ).attr('data-id') == id,
                    has_opt = no in $opt &&
                                    $opt.eq( no ).attr('value') == id;

                if( id in that.model.attributes.course_country ) {
                    ++num_selected;
                    if(!has_die ) {
                        var $die = $(that.dice_template( cm.attributes ));
                        (nd in $dice ? $dice.eq( nd ) : $add).before( $die );
                        $die.on('click', '.course-country-remove', function() {
                            that.model.removeCountry(
                                        $(this).parent().attr('data-id') );
                        } );
                    } else
                        ++nd;
                    if( has_opt )
                        $opt.eq( no++ ).remove();
                } else {
                    if( has_die )
                        $dice.eq( nd++ ).remove();
                    if(!has_opt ) {
                        var $i = $('<option>').attr('value', id ).text( name );
                        if( no in $opt )
                            $opt.eq( no ).before( $i );
                        else
                            $sel.append( $i );
                    } else
                        ++no;
                }
            } );
            if( num_selected ) $all.hide(); else $all.show();
        },
    });
});
