/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/progress_grades.html", "i18n!nls/strings", "app/collections/course/grades",
        "app/views/pages/course/progress_grades_cert", "app/tools/dataloader", "app/tools/linkhandler", "app/tools/utils",
        "app/views/dialogs/yes_no", "app/views/dialogs/messagebox", "app/tools/servicecalls"],
    function (tpl, str, GradesCollection, GradesCertView, DataLoader, LinkHandler, Utils, YesNoDialog, MessageBox, SC)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),

        events: {
            "click [data-call]": Utils.callMapper
        },
        
        getPaginationInfo: function (grades, pageid, thisCource)
        {
            var ret = {};
            var maxUsersPerPage = 100; //number of users allowed per page
            var maxPageLinks = 30; //maximum numper of page links to be shown
            var minPageLinks = 2; //minimum number of page links to be shown
            var allUsersInCourse = grades.get("users");
            
            var pagedUsers = function(users,countPerPage)
            {
                var paged = [];
                var page;
                
                var setAdditionalUserStripeClass = function(user)
                {
                    user.cell_stripe_extra_class = "";
                    if(user.all_submitted) {
                        user.cell_stripe_extra_class = "grade-cell-stripe-completed";
                    }
                    return user;
                };
                for (var i = 0; i < users.length; i++)
                {
                    if (i % countPerPage == 0)
                    {
                        page = [];
                        paged.push(page);
                    }
                    page.push(setAdditionalUserStripeClass(users[i]));
                }
                return paged;
            };
            
            //property: pagedUsers is a double dimention array of type users[pageCount][usersInPage]
            ret.pagedUsers = pagedUsers(allUsersInCourse, maxUsersPerPage);
            
            var pageIdInt = parseInt(pageid);
            if (isNaN(pageIdInt) || (pageIdInt < 1) || (pageIdInt > ret.pagedUsers.length))
            {
                pageIdInt = 1;
            }
            
            //property: id of the current page
            ret.pageid = pageIdInt;
            
            var totalPages = ret.pagedUsers.length;

            var pageLinkCount = totalPages;
            if(pageLinkCount > maxPageLinks)
            {
                pageLinkCount = maxPageLinks;
            } 
            else if(pageLinkCount < minPageLinks)
            {
                pageLinkCount = minPageLinks;
            }

            var start = 1;
            var end = pageLinkCount;
            if(maxPageLinks < totalPages)
            {
                var start = ret.pageid - maxPageLinks/2;
                var end = ret.pageid + maxPageLinks/2;
                if(start < 1)
                {
                    start = 1;
                    end = start + maxPageLinks;
                }
                else if(end > totalPages)
                {
                    end = totalPages;
                    start = end - maxPageLinks;
                }
            }
            
            var linksInfo = [];
            var formHref = function(value){
                if( (value < 1) || (value >totalPages ) ){
                    return "#";
                }
                return "/courses/" + thisCource.code + "/progress/page/" + value; 
            };
            var getClass = function(id)
            {
                if( (id < 1) || (id >totalPages ) ){
                    return "disabled";
                }
                else if(id == ret.pageid)
                {
                    return "active";
                }
                return "";
            };
            //bootstrap active style is not visible properly, adding a workaround
            var getStyle = function(id)
            {
                if(id == ret.pageid)
                {
                    return "text-decoration: underline;";
                }
                return "";
            };
            var getLinkObject = function(textValue,idValue)
            {
                return {text:textValue,href:formHref(idValue),class:getClass(idValue),style:getStyle(idValue)};
            };

            linksInfo.push(getLinkObject("<<",ret.pageid-1));
            for(var i=start; i<=end; i++)
            {
                linksInfo.push(getLinkObject(""+i,i));
            }
            linksInfo.push(getLinkObject(">>",ret.pageid+1));
            
            //property: Navigation links info of object type [text(text),value(text),active(boolean),disabled(boolean)]
            ret.linksInfo = linksInfo;

            return ret;
        },

        render: function ()
        {
            DataLoader.exec({ collection: GradesCollection, where: { courseid: this.parentView.mCourseView.model.get("id") }, context: this }, function (grades)
            {
                if (grades.get("users").length > 0)
                {
                    grades.set("users",_.sortBy(grades.get("users"),'all_submitted'));
                    var thisCource = this.parentView.mCourseView.model.toJSON();
                    var paginationInfo = this.getPaginationInfo(grades,this.parentView.mCourseView.currentsubsubsub, thisCource);

                    this.$el.html(this.template({ str: str, 
                                    pagiInfo : paginationInfo,
                                    tasks: grades.get("tasks"),
                                    course: thisCource}));
                                
                    
                    LinkHandler.setupView(this);

                    if (this.parentView.mCourseView.model.get("cert_enabled"))
                    {
                        GradesCertView.setElement(this.$("#progress-grade-cert")).setGrages(grades).setPaginationInfo(paginationInfo).render();
                    }
                }
                else
                {
                    this.$el.html("<h3 class=\"blue-placeholder\">" + str.label_progress_no_students + "<br>" + str.label_progress_send_invitations + "</h3>");
                }
            });

            return this;
        },

        resetProgress: function (ev, target)
        {
            var userid = parseInt(target.getAttribute("data-id"));

            var resetProgress = function ()
            {
              var courseid =  parseInt(this.parentView.mCourseView.model.get("id"));

              SC.call("local_monorailservices_reset_tasks", { courseid: courseid, userid: userid },
                function (data) {
                    location.reload();
                }, this, { errorHandler: function (data) {
                    location.reload();
                }}
              );
            };
          (new YesNoDialog({ header: str.course_reset_progress, message: str.course_reset_progress_msg, 
                             accepted: _.bind(resetProgress, this) })).render(); 
        },

        unenroll: function (ev, target)
        {
            if (this.parentView.mCourseView.model.get("course_privacy") == 1 && this.parentView.mCourseView.model.get("course_access") == 3) {
                (new MessageBox({ message: str.label_cannot_unenroll_from_public_courses })).render();
                return;
            }

            var participant = this.parentView.mCourseView.model.get("participants").get(target.getAttribute("data-id"));

            if (!participant) {
                console.warn("Failed to find participant?!");
                return;
            }

            var thisThis = this;

            (new YesNoDialog({ header: str.remove, message: str.label_confirm_remove_user, button_primary: str.remove, button_secondary: str.button_cancel,
                accepted: function ()
                {
                    participant.destroy({ success: function () {
                        window.location.reload(false);
                    }});
                }})).render();
        }
    }));
});
