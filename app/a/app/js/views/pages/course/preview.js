/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/views/pageview", "text!templates/pages/course/preview.html", "text!templates/pages/course/preview_sections.html",
        "text!templates/pages/course/preview_tasks.html", "i18n!nls/strings", "app/tools/dataloader", "app/tools/linkhandler",
        "app/tools/utils", "app/collections/course/materials", "app/tools/inlinecontent", "app/views/attachment/attachmentlist"],

function (PageView, tpl, tplSect, tplTask, str, DataLoader, LinkHandler, Utils, CourseMaterialCollection, InlineContent, AttachmentList)
{
    return new (PageView.extend(
    {
        template: _.template(tpl),
        templateSections: _.template(tplSect),
        templateTasks: _.template(tplTask),

        mCurrentPage: 0,

        events: {
            "click [data-call]": Utils.callMapper
        },

        actionClicked: function ()
        {
            switch (this.action) {
                case "reuse":
                    _Router.navigate("/reuse-course/" + this.code, { trigger: true });
                    break;

                default:
                    console.warn("Unknown action: " + this.action);
            }
        },

        showContent: function ()
        {
            this.mCurrentPage = 0;
            this.showSection(0);
        },

        showTasks: function ()
        {
            this.mCurrentPage = 1;
            this.$("#preview-course-content-area").html(this.templateTasks({ str: str, tasks: this.tasks.toJSON() }));
        },

        showTaskPreview: function (ev, target)
        {
            _Router.navigate("/reuse-preview-task/" + target.getAttribute("data-id"), { trigger: true });
        },

        openSection: function (ev, target)
        {
            this.showSection(target.getAttribute("data-section"));
        },

        showSection: function (sect)
        {
            sect = parseInt(sect) || 0;

            this.$("#preview-course-content-area").html(this.templateSections({ str: str, course: this.course.toJSON(), sections: this.sections.toJSON() }));

            this.$("[data-section]").parent().removeClass("active");

            if (this.sections.length > sect) {
                this.$("[data-section='" + sect + "']").parent().addClass("active");

                this.$("#course-preview-section-content").html(this.sections.at(sect).get("summary"));
                this.$("#course-preview-section-name").html(sect ? this.sections.at(sect).get("name") : str.overview_content);

                if (sect) {
                    this.$("#course-licence-place").hide();
                } else {
                    this.$("#course-licence-place").show();
                }

                InlineContent.fixTextDirection(this.$el);
                InlineContent.fixAttachmentUrls(this.$el, this.sections.at(sect).get("attachments"), false, false);
                InlineContent.fixMathJaxText(this.$el);

                var att = new AttachmentList({ collection: this.sections.at(sect).get("attachments") });

                att.setElement("#preview-attachment-place").render();
            } else {
                console.warn("Section number out of range.");
            }
        },

        pageRender: function ()
        {
            DataLoader.exec({ collection: CourseMaterialCollection, context: this, where: { code: this.code } }, function (course)
            {
                DataLoader.exec({ collection: course.get("sections"), context: this }, function (sections)
                {
                    DataLoader.exec({ collection: course.get("tasks"), context: this }, function (tasks)
                    {
                        this.course = course;
                        this.sections = sections;
                        this.tasks = tasks;

                        this.$el.html(this.template({ str: str, course: course.toJSON(), currentPage: this.mCurrentPage, action: this.action }));

                        if (this.mCurrentPage == 1) {
                            this.showTasks();
                        } else {
                            this.showContent();
                        }

                        LinkHandler.setupView(this);
                    });
                });
            });
        }
    }));
});
