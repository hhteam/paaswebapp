/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/forums.html", "i18n!nls/strings", "app/tools/dataloader", "app/tools/utils", "app/tools/linkhandler",
        "app/views/dialogs/forumdetails", "app/collections/forum/discussions", "app/models/forum/forum", "app/models/forum/discussion",
        "app/views/pages/forum/editpost", "moment"],
    function (tpl, str, DataLoader, Utils, LinkHandler, ForumDetailsDialog, DiscussionCollection, ForumModel, DiscussionModel,
            EditPostView, moment)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: ".course-content",
        currentView: null,

        bodyClass: "pagelayout-course path-theme path-theme-monorail gecko dir-ltr lang-en pagelayout-incourse jsenabled",
        bodyId: "page-theme-monorail-discussions",

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: this.options.parentView.model.get("forums") }, function (forums)
            {
                this.currentForums = forums;

                var posts = [], foru = [], recent_disc = [];

                this.options.parentView.model.get("recent_posts").each(function (post)
                {
                    var p = post.toJSON();

                    p.modified = moment.unix(p.modified).fromNow();
                    p.discussionname = DiscussionCollection.get(p.discussion).get("name");

                    for (var j=0; j<p.attachments.length; j++)
                    {
                        p.attachments[j].icon = Utils.mimeIcon(p.attachments[j].mimetype);
                    }

                    posts.push(p);
                });
                
                var course = this.options.parentView.model.toJSON();
                recent_disc = course.recent_discussions.toJSON();
                recent_disc = recent_disc.sort(function(a,b){return a.modified < b.modified;});
                _.each(recent_disc, function(disc) { 
                    disc.modified_str = moment.unix(disc.modified).fromNow();
                    disc.link = "/courses/" + course.code + "/discussions/" +  disc.forum + "/" + disc.id;
                });
                
                forums.each(function (fo)
                {
                    var f = fo.toJSON();

                    if (f.last_post_time)
                    {
                        f.last_post_time = moment.unix(f.last_post_time).fromNow();
                    }

                    f.unread = 0;

                    var discs = DiscussionCollection.where({ forum: f.id });

                    _.each(discs, function (d)
                    {
                        f.unread += parseInt(d.get("numunread")) || 0;
                    }, this);

                    foru.push(f);
                }, this);

                this.$el.html(this.template({ str: str, forums: foru, posts: posts,
                    recent_disc: recent_disc,
                    course: course, isPaas : _User.get('isPaas') }));

                LinkHandler.setupView(this);
                Utils.setupPopups(this.$el);
            });

            return this;
        },

        newForum: function (ev, target)
        {
            var course = this.options.parentView.model;

            DataLoader.exec({ collection: course.get("sections"), at: 0, context: this }, function (sec)
            {
                var forum = new ForumModel({ sectionid: sec.get("id"), course: course.get("id") });

                forum.once("sync", function ()
                {
                    this.options.parentView.model.get("forums").reset();
                    _Router.navigate("/courses/" + course.get("code") + "/discussions/" + forum.get("id"), { trigger: true });
                }, this);

                (new ForumDetailsDialog({ model: forum, header: str.forum_new })).render();
            });
        },

        newDiscussion: function ()
        {
            // Start new discussion under first forum...

            var forum = this.currentForums.at(0),
                course = this.options.parentView.model;

            if (forum)
            {
                _Router.navigate("/courses/" + course.get("code") + "/discussions/" + forum.get("id"), { trigger: true });

                var topic = new DiscussionModel({ forum: forum.get("id"), course: course.get("id") });
                var edit = new EditPostView({ model: topic, courseCode: course.get("code") });

                topic.once("sync", function ()
                {
                    this.currentForums.reset();

                    _Router.navigate("/courses/" + course.get("code") + "/discussions/" + forum.get("id") + "/" + topic.get("id"), { trigger: true });
                }, this);

                edit.render();
            }
        },

        goToPost: function (ev, target)
        {
            var post = this.options.parentView.model.get("recent_posts").get($(target).attr("data-postid"));

            _Router.navigate("/courses/" + this.options.parentView.model.get("code") + "/discussions/" + post.get("forum") + "/" + post.get("discussion"), { trigger: true });
        }
    });
});
