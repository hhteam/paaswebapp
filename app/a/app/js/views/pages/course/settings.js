/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/settings.html", "i18n!nls/strings", "app/tools/linkhandler", "app/views/dialogs/yes_no",
        "app/views/dialogs/messagebox", "app/views/dialogs/invite_email", "app/views/dialogs/enroll_users",
        "app/tools/dataloader", "app/collections/admin/cohorts", "app/views/dialogs/delete_course", "app/models/headercontrols",
        "app/tools/utils", "app/tools/tracker", "app/views/pages/course/settings_general", "app/views/pages/course/settings_access"],

    function (tpl, str, LinkHandler, YesNoDialog, MessageBox, InviteEmailDialog, EnrollUsersDialog,
        DataLoader, CohortCollection, DeleteCourseDialog, HeaderControls, Utils, Tracker, SettingsGeneralView, SettingsCourseAccess)
{
    return Backbone.View.extend(
    {
        el: ".course-content",

        template: _.template(tpl),

        events: {
            "click [data-call]": Utils.callMapper,
            "click #button-invite-twitter"         : "inviteTwitter",
            "click #button-invite-facebook"        : "inviteFacebook",
        },

        render: function ()
        {
            this.$el.html(this.template({
                str: str,
                course: this.model.toJSON()
            }));

            LinkHandler.setupView(this);

            (new SettingsGeneralView({ el: this.$("#course-settings-general-view"), model: this.model, parentView: this })).render();
            if (!this.model.get("wizard_mode")) {
              (new SettingsCourseAccess({ el: this.$("#course-settings-access-view"), model: this.model, parentView: this })).render();
            }
            // Show change logo button
            $('#btn-change-course-logo').css('visibility', 'visible');

            // Show Edit backgroun
            $('.edit-course-background').css('visibility', 'visible');

            // Enable teacher selection
            var teachers = this.model.get("participants").where({ teacher: true });
            var managers = this.model.get("participants").where({ manager: true, teacher: false });
            if ((teachers.length+managers.length) > 1) {
                $('#change-teacher').show();
            }

            $("#course-settings").show();
            $("#course-enable-cert-link").hide();

            // Set category ID
            if (this.model.get("categoryid")) {
                $('#listCategory').val(this.model.get("categoryid"));
            }

            var coursebackground = this.model.get("coursebackground");
            if (coursebackground)
            {
                this.options.parentView.currentBackgroundFile = coursebackground.substr(coursebackground.lastIndexOf("/") + 1);

                if (coursebackground.indexOf('/img/stock/') != -1)
                {
                    $('.carousel-inner').show();
                    this.options.parentView.selectCategory();
                }
            }

            // Show the textfield for fullname and shortname
            $('#course_page_header .editable').addClass('edit-mode');

            this.options.parentView.inlineEdit.init(this.options.parentView);
            this.options.parentView.inlineEdit.start();
        },

        saveEdit: function (options)
        {
            this.options.parentView.saveCourseDetails();
        },

        deleteCourse: function ()
        {
            if (this.model.get("course_privacy") == 1 && this.model.get("course_access") == 3 && this.model.get("students") > 0)
            {
                (new MessageBox({ message: str.completed_description_delete_nonempty })).render();

                return;
            }

            var thisThis = this;
            (new DeleteCourseDialog({ coursename: this.model.get("fullname"), context: this,
                accepted: function ()
                {
                    $('#edit-caption').html('');
                    _Router.setConfirmLeave(null, null, null);
                    thisThis.model.destroy({
                        success: function() {
                            _Router.navigate("/", { trigger: false });
                            window.location.reload(false);
                        },
                        error: function() {
                            try {
                                (new MessageBox({ message: str.course_delete_error, backdrop: false })).render();
                            } catch (err) {
                                console.warn("Unable to show warning! " + err);
                            }
                        }
                    });
                },
                rejected: function ()
                {
                    // nothing
                }})).render();
        },

        inviteTwitter: function()
        {
            var urlOfPage = encodeURIComponent(this.model.get("inviteUrl"));
            var text = encodeURIComponent(str.share_join_text + " " + this.model.get("fullname")); //TODO what here?
            Tracker.track("s-twitter-course-invite", { courseid: this.model.get("id")});
            window.open("https://twitter.com/intent/tweet?url="+urlOfPage+"&text="+text);
        },

        inviteFacebook: function(ev)
        {
            var courseName = encodeURIComponent(this.model.get("fullname"));
            var urlOfPage = encodeURIComponent(this.model.get("inviteUrl"));
            var summary = encodeURIComponent(str.share_join_text + " " + this.model.get("fullname")); //TODO what here?
            Tracker.track("s-facebook-course-invite", { courseid: this.model.get("id")});
            window.open("http://www.facebook.com/sharer.php?s=100&p[title]="+courseName+"&p[url]="+urlOfPage+"&p[summary]="+summary);
        },

    });
});
