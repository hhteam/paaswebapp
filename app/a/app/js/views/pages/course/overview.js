/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/course/overview.html", "i18n!nls/strings", "app/models/course/section", "app/tools/inlineedit",
        "app/models/headercontrols", "app/tools/linkhandler", "app/views/pages/course/section", "app/tools/dataloader", "app/tools/utils",
        "app/tools/tracker", "app/views/dialogs/messagebox"],

    function (tpl, str, SectionModel, InlineEdit, HeaderControls, LinkHandler, SectionView, DataLoader, Utils, Tracker, MessageBox)
{

    return Backbone.View.extend(
    {
        el: ".course-content",

        mTaskNotif: null,
        mForumNotif: null,
        mEditMode: false,
        mCurrentSection: null,

        template: _.template(tpl),

        events: {
            "click [data-call]": Utils.callMapper
        },

        bodyClass: function () {
            return "course-layout path-course path-course-view safari dir-ltr lang-en pagelayout-course side-post-only jsenabled";
        },
        
        bodyId: "page-course-view-weeks",

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();
        },

        startEdit: function ()
        {
            var doStartEdit = function ()
            {
                this.mEditMode = true;
                this.render();
                this.mCurrentSection.startEdit();
                this.inlineEdit.start();
            };

            if (!this.mCurrentSection)
            {
                this.once("ready", _.bind(doStartEdit, this));
            }
            else
            {
                doStartEdit.apply(this);
            }
        },

        saveEdit: function (opts)
        {
            if (!this.inlineEdit.commit({ deferred: opts.deferred }))
            {
                if (!this.inlineEdit.triggerValidation())
                {
                    // First, try to show error as tooltip,
                    // if that doesn't work - show the old
                    // message box.
                    (new MessageBox({ message: this.inlineEdit.errorString, backdrop: false })).render();
                }

                return;
            }

            this.mCurrentSection.saveEdit(opts);

            var leaveEditMode = function ()
            {
                this.mEditMode = false;
                _Router.setConfirmLeave(null, null, null);

                if (this.mCurrentSection.mRemoved)
                {
                    _Router.navigate("/courses/" + this.model.get("code"), { trigger: true });
                }
            };

            if (opts instanceof Object && opts.deferred instanceof Object)
            {
                opts.deferred.done(_.bind(leaveEditMode, this));
            }
            else
            {
                leaveEditMode.apply(this);
            }
        },

        cancelEdit: function ()
        {
            this.inlineEdit.rollback();
            this.mCurrentSection.cancelEdit();
            this.mEditMode = false;
            this.render();
        },

        render: function ()
        {
            var idx = parseInt(this.options.sectionIndex),
                sects = this.model.get("sections");

            DataLoader.exec({ collection: sects, at: idx, context: this, error: function ()
            {
                _Router.navigate("/courses/" + this.model.get("code"), { trigger: true });
            }
            }, function (sec)
            {
                var prev = "", next = "";

                if (idx)
                {
                    prev = (idx == 1 ? str.overview_content : sects.at(idx - 1).get("name"));
                }

                if (idx + 1 < sects.length)
                {
                    next = sects.at(idx + 1).get("name");
                }

                this.$el.html(this.template({ str: str, editMode: (this.model.get("wizard_mode") == 1) ? false : this.mEditMode,
                            sections: sects.toJSON(), course: this.model.toJSON(), currentIndex: idx, prev: prev, next: next,
                            wizardMode: this.model.get("wizard_mode"), isPaas : _User.get('isPaas') }));

                LinkHandler.setupView(this);

                this.mCurrentSection = new SectionView(
                        { model: sec, index: idx, inlineEdit: this.inlineEdit,
                          attachmentPath: "sections|" + sec.cid,
                          courseModel: this.model, parentView: this.options.parentView });

                this.mCurrentSection.setElement(this.$("#course-section-list"));
                this.mCurrentSection.render();

                if (this.model.get("can_edit"))
                {
                    this.inlineEdit.init(this);
                }
                else
                {
                    Tracker.track("courseview", { courseid: this.model.get("id"), section: sec.get("section") });
                }

                this.trigger("ready");
            });
        },

        add_section: function (ev)
        {
            ev.preventDefault();

            if (ev.target.hasAttribute("disabled"))
            {
                return;
            }

            var idx = this.model.get("sections").length,
                code = this.model.get("code");

            this.model.get("sections").create(
                { courseid: this.model.get("id"), visible: 1, section: idx },
                { success: function ()
                {
                    _Router.navigate("/courses/" + code + "/" + idx, { trigger: true });

                    HeaderControls.set("state", 2);
                    HeaderControls.get("handler").startEdit();
                } });
        },

        hidePage: function ()
        {
            if (this.mCurrentSection && this.mCurrentSection.hidePage instanceof Function)
            {
                this.mCurrentSection.hidePage();
            }
        }
    });
});
