/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/section.html", "i18n!nls/strings", "app/views/attachment/attachmentlist", "app/tools/inlinecontent",
        "app/models/headercontrols", "app/tools/utils", "app/tools/warnings", "app/views/dialogview", "app/tools/linkhandler",
        "app/collections/task/tasks", "app/tools/dataloader", "app/views/pages/course/section_tasks"],

    function (tpl, str, AttachmentList, InlineContent, HeaderControls, Utils, WarningTool, DialogView, LinkHandler, TaskCollection,
        DataLoader, SectionTasksView)
{
    return Backbone.View.extend(
    {
        mAddButton: null,
        mEditMode: false,
        mRemoved: false,
        mAttachments: null,
        template: _.template(tpl),

        markAsChanged: function()
        {
            this.trigger("valuechanged");
            this.model.set({valuechange:true}, {silent : true});
        },

        initialize: function ()
        {
            this.mAttachments = new AttachmentList(
                { collection: this.model.get("attachments"), inlineEdit: this.options.inlineEdit,
                  attachmentPath: this.options.attachmentPath + "|attachments" , section:this.model, sectionIndex: this.options.index, source: 'section' });

            this.updateAttachmentList();
            this.model.once("newSectionCreated", this.updateAttachmentList, this);

            if (this.options.index) {
                this.mTaskView = new SectionTasksView();
                this.mTaskView.em = false;
            } else {
                this.mTaskView = null;
            }
        },

        updateAttachmentList: function ()
        {
            this.options.inlineEdit.setAttachmentList(
            {
                model: this.model.get("attachments"),
                data: {
                    courseid: this.model.get("courseid"),
                    sectionid: this.model.get("id"),
                    source: "new_item" }
            }, this.options.attachmentPath + "|summary");
        },

        events: {
            "click [data-call]": Utils.callMapper,
            "change [data-call][type=\"checkbox\"]": Utils.callMapper,
            "click .section-remove": "removeSection"
        },

        goToExtCourse : function() {
            $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_papagei_url.php",
                    data: {code: this.options.courseModel instanceof Object ? this.options.courseModel.get('code') : ""},
                    context: this,
                    dataType: 'json',
                    success: function (data)
                    {
                        window.open(data.replace(/&amp;/, "&"));
                    },

                    error: function (data)
                    {
                        WarningTool.processWarnings({ msgtype: 'error', message: str.access_ext_course_error });
                    }
                });

        },

        startEdit: function ()
        {
        	//$('.hide-section').show();
            this.$el.find(".section-remove").show();
        	$('.showhidden').addClass('switchhidden').removeClass('showhidden');

            this.mEditMode = true;
            this.mRemoved = false;
            this.mAttachments.startEdit();
            this.model.on('valuechanged', this.markAsChanged, this);

            $('.hide_in_edit_mode').hide();

            if (!this.model.get("visible"))
            {
                this.$(".section-content").css("opacity", 1);
            }

            if (this.mTaskView) {
                this.mTaskView.em = true;
                this.mTaskView.render();
            }
        },

        saveEdit: function (opts)
        {
            if (this.mRemoved)
            {
                this.mEditMode = false;

                this.model.destroy(opts);

                // A bug in backbone, or something really messed up...
                if (this.model.collection)
                {
                    var stay = [ ];

                    this.model.collection.each(function (i)
                    {
                        if (i.cid != this.model.cid)
                        {
                            stay.push(i);
                        }
                    }, this);

                    this.model.collection.reset(stay, { silent: true });

                    // Topic deleted - go to first topic.
                }
            }
            else
            {
                var leaveEditMode = function ()
                {
                    this.model.off('valuechanged', this.markAsChanged);

                    //$('.hide-section').hide();

                    this.$el.find(".section-remove").hide();

                    this.mEditMode = false;

                    if (this.mTaskView) {
                        this.mTaskView.em = false;
                        this.mTaskView.saveTasks(this.model.get("id"), this.options.courseModel.get("sections").at(0).get("id"));
                    }
                };

                if (opts instanceof Object && opts.deferred instanceof Object)
                {
                    opts.deferred.done(_.bind(leaveEditMode, this));
                }
                else
                {
                    console.warn("Leaving section edit mode without deferred.");
                    leaveEditMode.apply(this);
                }

                var thisThis = this;

                if (this.mTaskView) {
                    this.model.set("tasks", this.mTaskView.tasks.slice());
                }

                this.mAttachments.saveEdit({ deferred: opts.deferred, success: function ()
                {
                    thisThis.model.set({ summary: InlineContent.embedAttachments(thisThis.model.get("summary"), thisThis.model.get("attachments")) });
                    //In update - user has write access
                    InlineContent.flagInlinedAttachments(thisThis.model.get("summary"), thisThis.model.get("attachments"));
                    InlineContent.removeDeletedInlineAttachments(thisThis.model.get("attachments"));

                    thisThis.model.save(undefined, opts);
                }});
            }
        },

        cancelEdit: function ()
        {
            this.model.off('valuechanged', this.markAsChanged);
        	//$('.hide-section').hide();
            this.$el.find(".section-remove").hide();
            this.mEditMode = false;
            this.mAttachments.cancelEdit();

            if (this.mTaskView) {
                this.mTaskView.em = false;
                this.mTaskView.resetTasks(this.model.get("tasks"));
                this.mTaskView.render();
            }

            // If section title is empty, we assume it's a newly created
            // one, and must be destroyed on cancel.
            if (!this.model.get("name"))
            {
                var code = this.options.courseModel instanceof Object ? this.options.courseModel.get("code") : "";

                this.model.destroy({ success: function ()
                {
                    _Router.navigate("/courses/" + code, { trigger: true });
                }});
            }
        },

        render: function ()
        {
            var data = this.model.toJSON();

            //The sections view can be used in cohort catalog or in general
            //course view

            if (this.options.catalog)
            {
                this.$el.html(this.template({
                    data: data, index: this.options.index, cid: this.model.cid, str: str, can_edit: false, /*skills: skills,*/ extcourse:0,
                    course: this.options.courseModel instanceof Object ? this.options.courseModel.toJSON() : null }));

                InlineContent.fixTextDirection(this.$el);
                InlineContent.fixAttachmentUrls(this.$el, this.model.get("attachments"), false, false);
                InlineContent.fixMathJaxText(this.$el);
                Utils.setupPopups(this.$el);
            }
            else
            {
                var can_edit = this.options.courseModel instanceof Object ? (this.options.courseModel.get('can_edit') && !this.options.courseModel.get("cantwrite")) : false;

                var allSections = [];

                if (this.options.courseModel instanceof Object) {
                    this.options.courseModel.get("sections").each(function (sec, idx)
                    {
                        allSections.push({ name: sec.get("name") });
                    });
                }

                this.$el.html(this.template({
                    data: data, index: this.options.index, cid: this.model.cid, str: str,
                    can_edit: can_edit, progress: this.options.courseModel instanceof Object ? this.options.courseModel.get('progress') : 0,
                    allSections: allSections, extcourse: this.options.courseModel instanceof Object ? this.options.courseModel.get('extcourse') : "",
                    extprovider: this.options.courseModel instanceof Object ? this.options.courseModel.get('extprovider') : "",
                    course: this.options.courseModel instanceof Object ? this.options.courseModel.toJSON() : null }));

                InlineContent.fixTextDirection(this.$el);
                InlineContent.fixAttachmentUrls(this.$el, this.model.get("attachments"), false, this.options.courseModel instanceof Object ? this.options.courseModel.get("can_edit") : false);
                InlineContent.fixMathJaxText(this.$el);
                Utils.setupPopups(this.$el);

                if (this.options.index) {
                    var addBtnItems = [ ];
                    addBtnItems.push({ label: str.new_file, handler: this.mAttachments.newFile, owner: this.mAttachments, data: { courseid: data.courseid, sectionid: data.id, source: "new_item", origin: "course_section" } });
                    addBtnItems.push({ label: str.ltitool, handler: this.mAttachments.ltiTool, owner: this.mAttachments, data: { courseid: data.courseid, sectionid: data.id, source: "new_item", origin: "course_section" } });
                    addBtnItems.push({ label: str.googledrive, handler: this.mAttachments.googleDrive, owner: this.mAttachments, data: { courseid: data.courseid, sectionid: data.id, source: "new_item", origin: "course_section" } });
                    if(((typeof videoUploadEnabled !== 'undefined') && videoUploadEnabled) || _User.hasFeature("video")) {
                      addBtnItems.push({ label: str.upload_video, handler: this.mAttachments.videoUpload, owner: this.mAttachments, data: { courseid: data.courseid, sectionid: data.id, source: "new_item", origin: "course_section" } });
                    }
                    this.mAttachments.setAddButton(addBtnItems);
                }
            }

            this.mAttachments.setElement("#section-" + data.id + "-attachments").render();

            if (!data.visible)
            {
                this.$(".section-content").css("opacity", 0.4);
            }

            if (this.mTaskView) {
                this.mTaskView.resetTasks(data.tasks);
                this.mTaskView.render();
            }
        },

        removeSection: function (ev)
        {
            ev.preventDefault();

            this.model.off('valuechanged', this.markAsChanged);
            this.mRemoved = true;

            this.$el.replaceWith("<h3 style=\"text-align: center;\">" + str.press_save_to_confirm_delete + "</h3>" + "<p style=\"text-align: center;\">" + str.press_save_to_confirm_delete_note + "</p>");
            this.trigger('valuechanged');
        },

        editMode: function (ev)
        {
            HeaderControls.set("state", 2);
            HeaderControls.get("handler").startEdit();
        },

        changeSectionIndex: function (ev)
        {
            var thisThis = this,
                idx = ev.target.getAttribute("data-index"),
                sects = this.options.courseModel.get("sections");

            this.model.save({ section: idx }, { success: function ()
            {
                sects.once("reset", function ()
                {
                    _Router.navigate("/courses/" + thisThis.options.courseModel.get("code") + "/" + idx, { trigger: true });
                });

                sects.fetch();
            }});
        },

        togglePublished: function (ev, target)
        {
            var thisThis = this,
                sects = this.options.courseModel.get("sections");

            this.model.save({ visible: $(target).is(":checked") ? 1 : 0 }, { success: function ()
            {
                sects.once("reset", function ()
                {
                    thisThis.options.parentView.render();
                });

                sects.fetch();
            }});
        },

        hidePage: function ()
        {
            // If section title is empty, we assume it's a newly created
            // one, and must be destroyed on cancel.
            if (this.mEditMode && !this.model.get("name"))
            {
                this.model.destroy({ success: function ()
                {
                    if (_Router.sCurrentPage && _Router.sCurrentPage.render instanceof Function)
                    {
                        _Router.sCurrentPage.render();
                    }
                }});
            }
        },

        addTasks: function (ev, target) {
            if (!(this.options.courseModel instanceof Object && this.options.courseModel.get('can_edit') && !this.options.courseModel.get("cantwrite"))) {
                return;
            }

            var allTasks = [];

            this.options.courseModel.get("sections").each(function (sec, idx) {
                if (sec.get("id") != this.model.get("id")) {
                    allTasks = allTasks.concat(sec.get("tasks"));
                }
            }, this);

            var html = "";

            if (allTasks.length > 0) {
                for (var i=0; i<allTasks.length; i++) {
                    html += "<div class=\"mbs\"><a href=\"#\" data-call=\"linkTask\" data-task=\"" + allTasks[i].id + "\" class=\"dialog_reject\">" + allTasks[i].name + "</a></div>";
                }
            } else {
                html += "<div><a href=\"/tasks/new/" + this.options.courseModel.get("code") + "\" class=\"dialog_reject\">" + str.button_start_new_task + "</a></div>";
            }

            var dlg = DialogView.extend({
                dropDownMode: true,
                renderDialog: function () {
                    this.$body.html("<div class=\"modal-body\"><p style=\"max-width: 200px; min-width: 100px;\">" + html + "</p></div>");
                }
            });

            var d = new dlg({moveUnder: target}),
                thisThis = this;

            d.render();

            LinkHandler.setupView(d);

            d.$el.find("[data-call]").each(function () {
                $(this).click(Utils.callMapper.bind(thisThis));
            });
        },

        linkTask: function (e, target) {
            if (!this.mTaskView) {
                return;
            }

            DataLoader.exec({ collection: TaskCollection, where: { instanceid: $(target).attr("data-task") }, context: this }, function (model) {
                this.mTaskView.addTask(model);
                this.mTaskView.render();
            });
        },

        openTask: function (ev, target) {
            _Router.navigate("/" + ($(target).attr("data-taskmod") == "quiz" ? "quizzes" : "tasks") + "/" + $(target).attr("data-taskid"), { trigger: true });
        },

        unlinkTask: function (ev, target) {
            if (!this.mTaskView) {
                return;
            }

            DataLoader.exec({ collection: TaskCollection, where: { instanceid: $(target).attr("data-taskid") }, context: this }, function (model) {
                this.mTaskView.removeTask(model);
                this.mTaskView.render();
            });
        }
    });
});
