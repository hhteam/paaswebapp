/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course/progress_export.html", "i18n!nls/strings", "app/collections/course/grades",
        "app/tools/dataloader", "app/tools/linkhandler", "app/tools/utils", "filesaver"],
    function (tpl, str, GradesCollection, DataLoader, LinkHandler, Utils, FileSaver)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),
        mGrades: null,

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            DataLoader.exec({ collection: GradesCollection, where: { courseid: this.parentView.mCourseView.model.get("id") }, context: this }, function (grades)
            {
                this.mGrades = grades;

                this.$el.html(this.template({ str: str }));
                LinkHandler.setupView(this);
            });

            return this;
        },

        exportGrades: function ()
        {
            if (!this.mGrades) {
                return;
            }

            var feedback = this.$("input[name='include_feedback']:checked").val(),
                hasGroups = false;

            _.each(this.mGrades.get("users"), function (user) {
                if (user.groups) {
                    hasGroups = true;
                }
            });

            switch (this.$("input[name='grade_format']:checked").val())
            {
                case "csv":
                    var data = "\"" + str.name + "\",\"" + str.email + "\",",
                        totalAvgSum = 0, totalAvgCnt = 0;

                    if (hasGroups) {
                        data += "\"" + str.group + "\",";
                    }

                    _.each(this.mGrades.get("tasks"), function (task)
                    {
                        data += "\"" + task.name + "\",";

                        if (feedback)
                        {
                            data += "\"" + str.feedback + "\",";
                        }
                    }, this);

                    data += "\"" + str.average + " (%)\",";
                    data += "\n";

                    _.each(this.mGrades.get("users"), function (user)
                    {
                        data += "\"" + user.firstname + " " + user.lastname + "\",";
                        data += "\"" + user.email + "\",";

                        if (hasGroups) {
                            data += "\"" + user.groups + "\",";
                        }

                        _.each(user.grades, function (grade)
                        {
                            data += "\"" + grade.grade + "\",";

                            if (feedback)
                            {
                                data += "\"" + grade.feedback + "\",";
                            }
                        }, this);

                        data += "\"" + user.average + "\",";
                        data += "\n";

                        if (!isNaN(parseFloat(user.average))) {
                            totalAvgSum += parseFloat(user.average);
                            totalAvgCnt++;
                        }
                    }, this);

                    data += "\"" + str.average + " (%)\",";
                    data += "\"\",";

                    if (hasGroups) {
                        data += "\"\",";
                    }

                    _.each(this.mGrades.get("tasks"), function (task)
                    {
                        data += "\"" + task.average + "\",";

                        if (feedback)
                        {
                            data += "\"\",";
                        }
                    }, this);

                    if (totalAvgCnt) {
                        data += Math.round((totalAvgSum / totalAvgCnt) * 100) / 100;
                    }

                    data += "\n";

                    FileSaver(new Blob([data], { type: "text/csv;charset=utf-8" }), "data.csv");
                    break;

                case "plain":
                    var data = str.name + "\t" + str.email + "\t",
                        totalAvgSum = 0, totalAvgCnt = 0;

                    if (hasGroups) {
                        data += str.group + "\t";
                    }

                    _.each(this.mGrades.get("tasks"), function (task)
                    {
                        data += task.name + "\t";
                    }, this);

                    data += str.average + "(%)";
                    data += "\n";

                    _.each(this.mGrades.get("users"), function (user)
                    {
                        data += user.firstname + " " + user.lastname + "\t" + user.email + "\t";

                        if (hasGroups) {
                            data += user.groups + "\t";
                        }

                        _.each(user.grades, function (grade)
                        {
                            data += grade.grade;

                            if (feedback && grade.feedback)
                            {
                                data += " (" + grade.feedback + ")";
                            }

                            data += "\t";
                        }, this);

                        data +=  user.average + "\n";

                        if (!isNaN(parseFloat(user.average))) {
                            totalAvgSum += parseFloat(user.average);
                            totalAvgCnt++;
                        }
                    }, this);

                    data += str.average + " (%)\t\t";

                    if (hasGroups) {
                        data += "\t";
                    }

                    _.each(this.mGrades.get("tasks"), function (task)
                    {
                        data += task.average + "\t";
                    }, this);

                    if (totalAvgCnt) {
                        data += Math.round((totalAvgSum / totalAvgCnt) * 100) / 100;
                    }

                    data += "\n";

                    FileSaver(new Blob([data], { type: "text/plain;charset=utf-8" }), "data.txt");
                    break;
            }
        }
    }));
});
