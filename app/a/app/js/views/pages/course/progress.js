/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/course/progress.html", "i18n!nls/strings", "app/views/pages/course/progress_grades",
        "app/views/pages/course/progress_export", "app/tools/linkhandler"],
    function (tpl, str, ProgressGradesView, ProgressExportView, LinkHandler)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),

        bodyClass: "pagelayout-course path-theme path-theme-monorail gecko dir-ltr lang-en pagelayout-incourse jsenabled",
        bodyId: "page-theme-monorail-grades",

        mCourseView: null,
        mParticipants: null,

        setCourseView: function (views)
        {
            this.mCourseView = views;

            return this;
        },

        setParticipants: function (participants)
        {
            if (this.mParticipants)
            {
                this.mParticipants.unbind("reset", this.render, this);
            }

            this.mParticipants = participants;
            this.mParticipants.bind("reset", this.render, this);
        },

        initialize: function ()
        {
            ProgressGradesView.parentView = ProgressExportView.parentView = this;

            return this;
        },

        render: function ()
        {
            if (this.mCourseView.currentsub != "progress")
            {
                return;
            }

            this.$el.html(this.template({
                str: str,
                course: this.mCourseView.model.toJSON(),
                subpage: this.mCourseView.currentsubsub
            }));

            LinkHandler.setupView(this);

            switch (this.mCourseView.currentsubsub)
            {
                case "export":
                    ProgressExportView.setElement(this.$("#course-progress-subpage")).render();
                    break;

                default:
                    ProgressGradesView.setElement(this.$("#course-progress-subpage")).render();
            }

            return this;
        }
    }));
});
