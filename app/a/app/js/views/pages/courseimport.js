/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/courseimport.html", "i18n!nls/strings",
        "app/models/headercontrols", "app/collections/course/courses", "app/views/pages/course",
        "app/tools/servicecalls", "app/views/dialogs/importtasks",
        "app/collections/course/sections", "app/collections/task/tasks", "app/tools/dataloader",
        "app/tools/warnings", "app/views/header/wizard", "app/tools/ajaxprogress", "app/tools/dataloader",
        "app/collections/course/coursesshort"],

function (tpl, str, HeaderControls,
            CoursesCollection, CoursePage, SC, ImportTasksDialog,
            SectionsCollection, TasksCollection, DataLoader, WarningTool, HeaderView, AjaxProgress, DataLoader,
            CourseShortCollection)
{
	return new (Backbone.View.extend(
	{
		template: _.template(tpl),

        el: "#page-content",

        customHeader: HeaderView,

        events: {
            "click #course-import-upload-file": "uploadFile"
        },

        upload_process: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-bar .bar').css(
                'width',
                progress + '%'
            );
        },

        uploadFile: function () {
            _Uploader.owner(this).setFileUploadLimit(256*1024*1024).restrictFileExts(["zip", "mbz"]).restrictFileType(null).exec(
                //add
                function(filedata) {
                    _Router.setConfirmLeave(str.edit_cancel, null, null);

                    $('#course-import-upload-file').hide();
                    $('#progress-text').html(str.import_uploading);
                    $('#progress-container').show();
                },
                //error
                function (data) {
                    console.warn("Unable to upload file: " + data.error);
                    $('#progress-container').hide();
                    $('#progress-bar .bar').css('width', '0%');
                    $('#course-import-upload-file').show();
                },
                //done
                function (filedata) {
                    $('#progress-text').html(str.import_processing);
                    $('#progress-bar').addClass('progress-striped');
                    $('#progress-bar').addClass('active');
                    $('#progress-bar .bar').css('width', '100%');

                    var slowPercent = 0;
                    var slowProgress = function () {
                        if (slowPercent < 100) {
                            slowPercent += 1;
                            $('#progress-bar .bar').css('width', slowPercent + '%');
                            setTimeout(slowProgress, slowPercent * 25)
                        }
                    };

                    setTimeout(slowProgress, 500);

                    SC.call("local_monorailservices_import_course",
                        { filename: filedata.filename },
                        function (data) {
                            if ('coursecode' in data && data.coursecode.length > 1) {
                                // Success, let's refresh courses and continue
                                slowPercent = 100;
                                $('#progress-bar .bar').css('width', '100%');
                                this.mCourseCode = data.coursecode;
                                CoursesCollection.reset();
                                CourseShortCollection.TeachingOngoing.reset();
                                CourseShortCollection.TeachingCompleted.reset();

                                this.pretasksDialog();
                            }
                            if ('warnings' in data && data.warnings.length > 0) {
                                WarningTool.processWarnings({ msgtype: 'error', message: str.import_error });
                                $('#page-warnings').addClass('alert-error');
                                $('#progress-container').hide();
                                $('#course-import-upload-file').show();
                                $('#progress-bar').removeClass('progress-striped');
                                $('#progress-bar').removeClass('active');
                            }
                        }, this, { errorHandler: function (data) {
                            WarningTool.processWarnings({ msgtype: 'error', message: str.import_error });
                            $('#page-warnings').addClass('alert-error');
                            $('#progress-container').hide();
                            $('#course-import-upload-file').show();
                            $('#progress-bar').removeClass('progress-striped');
                            $('#progress-bar').removeClass('active');
                        }}
                    );
                }
            );
        },

        pretasksDialog: function () {
            DataLoader.exec({ collection: CoursesCollection, context: this, where: { code: this.mCourseCode } }, function (model)
            {
                model.get("sections").bind("reset", this.tasksDialog, this);
                model.get("sections").fetch();
            });
        },

        tasksDialog: function () {
            var model = CoursesCollection.where({ code: this.mCourseCode })[0];
            model.get("sections").unbind("reset", this.tasksDialog);
            var tasks = model.get("assignments");
            if (tasks.length > 0) {
                (new ImportTasksDialog({
                    mCourseCode: this.mCourseCode,
                    context : this,
                    accepted: function () {
                        var model = CoursesCollection.where({ code: this.mCourseCode })[0];
                        var tasks = model.get("assignments");
                        _.each(tasks, function (task, idx) {
                            if (! window.tempTaskCheckboxes[idx].checked) {
                                tasks.splice(idx, 1);
                                // Make sure we have the task.
                                DataLoader.exec({ collection: TasksCollection, where: { instanceid: task.instanceid }, context: this }, function (model) {
                                    model.destroy({ error: function () {
                                        if (! $('#page-warnings').hasClass('alert-error')) {
                                            WarningTool.processWarnings({ msgtype: 'error', message: str.import_error_tasks });
                                            $('#page-warnings').addClass('alert-error');
                                        }
                                    }});
                                });
                            }
                        });
                        window.tempTaskCheckboxes.remove();
                        model.set('assignments', tasks);
                        model.save();
                        this.nextStep(model);
                    },
                    rejected: function () {
                        this.nextStep(model);
                    }
                })).render();
            } else {
                this.nextStep(model);
            }
        },

        nextStep: function (model) {
            model.set({ can_edit: true, wizard_mode: 2, from_import: true }, { silent: true });
            CoursePage.want_edit_mode = true;
            _Router.setConfirmLeave(null, null, null);
            AjaxProgress.enableAjax();
            _Router.navigate('/courses/'+this.mCourseCode, { trigger: true });
            _User.fetch();
        },

        cancelEdit: function () {
            _Router.setConfirmLeave(null, null, null);
            AjaxProgress.enableAjax();
            _Router.navigate('/', { trigger: true });
        },

		render: function ()
		{
			this.$el.html(this.template({ str: str }));

            HeaderControls.set("state", 3);
            HeaderControls.set("handler", this);

            AjaxProgress.disableAjax();

            _Router.setConfirmLeave(null, null, null);

			return this;
		}
	}));
});
