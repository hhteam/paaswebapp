/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/views/pageview", "text!templates/pages/org_catalog.html", "i18n!nls/strings", "app/tools/dataloader", "app/collections/admin/cohorts",
        "app/tools/linkhandler", "app/collections/course/categories", "app/tools/utils"],

function (PageView, tpl, str, DataLoader, CohortCollection, LinkHandler, CategoryCollection, Utils)
{
    var ITEMS_PER_PAGE = 24;

    return new (PageView.extend(
    {
        template: _.template(tpl),
        mPage: 1,
        mCategory: 0,
        mSearchString: "",

        menuHighlight: "org_catalog",

        pageTitle: function ()
        {
            return str.site_name + ": " + (_User.get("orgname") || "") + " " + str.courses;
        },

        events: {
            "click [data-call]": Utils.callMapper
        },

        setPage: function (ev, target)
        {
            this.mPage = parseInt(target.getAttribute("data-page"));
            this.render();
        },

        openCourse: function (ev, target)
        {
            _Router.navigate("/courses/" + target.getAttribute("data-code"), { trigger: true });
        },

        pageRender: function ()
        {
            var clist = [ ],
                logo = RootDir + "app/img/logo-course-title.png",
                courses = _User.get("enrolledcourses"),
                cohorts = _User.get("cohortparticipant");

            for (var i=0; i<courses.length; i++) {
                if (courses[i].cohortid == this.CurrentId) {
                    clist.push(courses[i]);
                }
            }

            for (var i=0; i<cohorts.length; i++) {
                if (cohorts[i].cohortid == this.CurrentId) {
                    if (cohorts[i].cohorticonurl) {
                        logo = cohorts[i].cohorticonurl;
                    }
                    break;
                }
            }

            this.$el.html(this.template({
                items: clist.slice((this.mPage - 1) * ITEMS_PER_PAGE, Math.min(this.mPage * ITEMS_PER_PAGE, clist.length)),
                page: this.mPage,
                itemCount: clist.length,
                pageCount: Math.ceil(clist.length / ITEMS_PER_PAGE),
                str: str,
                logo: logo
            }));

            LinkHandler.setupView(this);
        }
    }));
});
