/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/dashboard/sidebar.html", "i18n!nls/strings", "app/tools/utils", "app/tools/social_utils",
        "app/collections/settings/social_accounts", "app/tools/dataloader", "app/tools/linkhandler", "backbone"],
        function (tpl, str, Utils, SocialUtils, SocialAccountsCollection, DataLoader, LinkHandler)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: SocialAccountsCollection }, function (accounts)
            {
                var social = { },
                    accountNag = (!_User.get("description") || _User.get("profilepichash") == "default") ? 1 : 0;

                accounts.each(function (item)
                {
                    social[item.get("type")] = item.toJSON();
                }, this);

                this.$el.html(this.template({ social: social, accountNag: accountNag, str: str }));

                LinkHandler.setupView(this);
            });
        },

        addFacebookAccount: function ()
        {
            SocialUtils.addFacebookAccount(_.bind(this.render, this));
        },

        addLinkedinAccount: function ()
        {
            SocialUtils.addLinkedinAccount(_.bind(this.render, this));
        },

        addTwitterAccount: function ()
        {
            SocialUtils.addTwitterAccount(_.bind(this.render, this));
        },

        share: function (ev, target)
        {
            // TODO: Share message text?
            SocialUtils.sharePopup(target.getAttribute("data-service"), "https://eliademy.com/", "Eliademy",
                "Eliademy", "https://eliademy.com/img/logo-eliademy-square.png");
        }
    }));
});
