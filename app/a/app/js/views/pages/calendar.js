/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["i18n!nls/strings", "text!templates/pages/calendar.html", "app/views/pageview", "app/collections/calendar/months",
        "app/tools/dataloader", "app/tools/utils", "app/tools/linkhandler", "app/views/pages/calendar/upcomingevents",
        "app/views/dialogs/event", "app/models/calendar/event", "app/collections/calendar/upcomingevents",
        "app/views/dialogs/eventview", "app/views/pages/calendar/calexport", "app/collections/course/coursesshort", "moment"],
    function (str, tpl, PageView, MonthCollection, DataLoader, Utils, LinkHandler, UpcomingEventsView, EventDialog,
        EventModel, UpcomingEventCollection, EventViewDialog, CalExportDialog, CourseShortCollection, moment)
{
    return new (PageView.extend(
    {
        template: _.template(tpl),
        menuHighlight: "calendar",
        mAddEventState: false,
        mOpenWeek: null,

        pageTitle: function ()
        {
            return str.site_name + ": " + str.calendar;
        },

        events: {
            "click [data-call]": Utils.callMapper
        },

        initialize: function ()
        {
            this.mUpcomingEvents = new UpcomingEventsView();
        },

        pageRender: function ()
        {
            moment.locale(_User.get("lang"));

            DataLoader.exec({ context: this, collection: MonthCollection, id: this.date }, function (month)
            {
                this.currentMonth = month;

                var startDay = moment(this.date + "0112", "YYYYMMDDHH").format("X"),
                    day = parseInt(moment(startDay, "X").startOf("week").add(12, "hours").format("X")),
                    weekDays = [], days = [], week, items, i, j, dayStart, dayEnd;

                do
                {
                    week = [];

                    for (i=0; i<7; i++)
                    {
                        items = [ ];
                        dayStart = parseInt(moment(day, "X").startOf("day").format("X"));
                        dayEnd = parseInt(moment(day, "X").endOf("day").format("X"));

                        month.get("events").each(function (ev)
                        {
                            if (!(ev.get("timestart") > dayEnd || ev.get("timeend") < dayStart) && ev.get("visible"))
                            {
                                items.push(ev.toJSON());
                            }
                        });

                        week.push(
                        {
                            timestamp: day,
                            weeknum: moment(day, "X").format("w"),
                            name: moment(day, "X").format("D"),
                            items: items,
                            this_month: (moment(day, "X").format("YYYYMM") == this.date) ? 1 : 0,
                            this_week: (moment(day, "X").isSame(moment(), "week")) ? 1 : 0,
                            is_weekend: (parseInt(moment(day, "X").format("d")) % 6 == 0) ? 1 : 0,
                            is_today: (moment(day, "X").isSame(moment(), "day")) ? 1 : 0
                        });

                        day += 86400;
                    }

                    days.push(week);
                }
                while (moment(day, "X").format("YYYYMM") == this.date);

                day = parseInt(moment().startOf("week").add(12, "hours").format("X"));

                for (i=0; i<7; i++)
                {
                    weekDays.push(moment(day, "X").format("ddd"));
                    day += 86400;
                }

                this.$el.html(this.template(
                {
                    str: str,
                    month: month.toJSON(),
                    days: days,
                    week_days: weekDays,
                    prev_month: moment(startDay, "X").subtract(1, "month").format("YYYYMM"),
                    next_month: moment(startDay, "X").add(1, "month").format("YYYYMM"),
                    add_event: this.mAddEventState,
                    open_week: this.mOpenWeek
                }));

                this.mUpcomingEvents.setElement(this.$("#calendar-upcoming-events"));
                this.mUpcomingEvents.render();

                LinkHandler.setupView(this);
            });
        },

        toggleAddEvent: function ()
        {
            this.mAddEventState = !this.mAddEventState;

            this.render();
        },

        addEvent: function (ev, target)
        {
            var em = new EventModel({ timestart: moment(target.getAttribute("data-day"), "X").format(), timeduration: 3600 });

            em.once("sync", function ()
            {
                MonthCollection.remove(this.currentMonth);
                UpcomingEventCollection.reset();

                this.mAddEventState = 0;
                this.render();
            }, this);

            var opt = { model: em };

            if ($(target).offset().top * 2 < $(document).height())
            {
                opt.moveUnder = target;
            }
            else
            {
                opt.moveAbove = target;
            }

            DataLoader.exec({ context: this, collection: CourseShortCollection.TeachingOngoing }, function (courses)
            {
                opt.courses = courses;

                (new EventDialog(opt)).render();

                if (this.mAddEventState)
                {
                    this.mAddEventState = false;
                    this.render();
                }
            });
        },

        eventClicked: function (ev, target)
        {
            var m = this.currentMonth.get("events").get(target.getAttribute("data-evid"));

            if (!m)
            {
                m = UpcomingEventCollection.get(target.getAttribute("data-evid"));

                if (!m)
                {
                    console.warn("Invalid event clicked!");
                    return;
                }
            }

            var opt = { model: m };

            if ($(target).offset().top * 2 < $(document).height())
            {
                opt.moveUnder = target;
            }
            else
            {
                opt.moveAbove = target;
            }

            if (m.get("userid") == _User.get("id"))
            {
                m.once("sync", function ()
                {
                    MonthCollection.remove(this.currentMonth);
                    UpcomingEventCollection.reset();

                    this.render();
                }, this);

                DataLoader.exec({ context: this, collection: CourseShortCollection.TeachingOngoing }, function (courses)
                {
                    opt.courses = courses;

                    (new EventDialog(opt)).render();
                });
            }
            else
            {
                (new EventViewDialog(opt)).render();
            }
        },

        dueEventClicked: function (ev, target)
        {
            _Router.navigate("/tasks/" + target.getAttribute("data-taskid"), { trigger: true });
        },

        showMoreClicked: function (ev, target)
        {
            this.mOpenWeek = target.getAttribute("data-week");
            this.render();
        },

        subscribeClicked: function (ev, target)
        {
            (new CalExportDialog()).render();
        }
    }));
});
