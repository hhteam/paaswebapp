/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/genericnotice.html", "i18n!nls/strings", "app/tools/linkhandler"], 
function (tpl, str, LinkHandler)
{
	return new (Backbone.View.extend(
	{
		template: _.template(tpl),
        
        el: "#page-content",
        
        notices: {
            'notenrolled': {
                text: str.error_notenrolled,
                img: "large-x.png",
                link: ["/", str.join_home]
            },
            'unknown': {
                text: str.error_unknown,
                img: "large-x.png",
                link: ["/", str.join_home]
            }
        },
        
        notice: 'unknown',

		render: function ()
		{
            
			this.$el.html(this.template({ notice: this.notices[this.notice] }));

            LinkHandler.setupView(this);

			return this;
		},
        
        hide: function ()
        {
            // nothing
        }
	}));
});
