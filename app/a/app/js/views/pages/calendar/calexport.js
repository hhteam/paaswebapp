/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "text!templates/pages/calendar/calexport.html", "app/views/dialogview", "app/models/calexport"],

    function (str, tpl, DialogView, CalExportModel)
{
    return DialogView.extend(
    {
        template: _.template(tpl),
        model: null,

        renderDialog: function ()
        {
            var thisThis = this;
            if(!this.model) {
                this.model = new CalExportModel();
                this.model.fetch({
                    success:function(method,model,option)
                    {
                        thisThis.$body.html(thisThis.template({ calurl: thisThis.model.get('calurl'), str: str }));
                    },
               });
            } else {
                this.$body.html(this.template({ calurl: this.model.get('calurl'), str: str }));
            }
        }
    });
});
