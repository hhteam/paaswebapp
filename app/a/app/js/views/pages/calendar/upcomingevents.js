/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/calendar/upcomingevents.html", "i18n!nls/strings", "app/collections/calendar/upcomingevents",
        "app/tools/dataloader", "app/tools/utils", "moment"],
    function (tpl, str, UpcomingEventCollection, DataLoader, Utils, moment)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        mMaxUpcoming: 10,

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: UpcomingEventCollection }, function (upcoming)
            {
                var upcomingEvents = [];

                upcoming.each(function (up, idx)
                {
                    if (idx >= this.mMaxUpcoming)
                    {
                        return;
                    }

                    var u = up.toJSON();

                    if (moment(u.timestart, "X").isAfter(moment()) && u.visible)
                    {
                        u.time = moment(u.timestart, "X").fromNow();

                        if (moment(u.timestart, "X").isSame(moment(), "day"))
                        {
                            u.label = "label label-important";
                        }
                        else if (moment(u.timestart, "X").isBefore(moment().add(6, "days")))
                        {
                            u.label = "label label-warning";
                        }
                        else
                        {
                            u.label = "label label-info";
                        }

                        upcomingEvents.push(u);
                    }
                }, this);

                this.$el.html(this.template({ str: str, upcoming: upcomingEvents, maxUpcoming: this.mMaxUpcoming, totalUpcoming: upcoming.length }));
            });
        },

        loadMoreEvents: function (ev, target)
        {
            this.mMaxUpcoming += 10;
            this.render();
        }
    });
});
