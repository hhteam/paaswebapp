/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/views/pageview", "text!templates/pages/reuse/public_catalog.html", "i18n!nls/strings", "app/tools/dataloader",
        "app/tools/linkhandler", "app/collections/course/categories", "app/tools/utils", "app/collections/course/coursesfree",
        "app/models/headercontrols"],

function (PageView, tpl, str, DataLoader, LinkHandler, CategoryCollection, Utils, FreeCourseCollection, HeaderControls)
{
    var ITEMS_PER_PAGE = 24;

    return new (PageView.extend(
    {
        template: _.template(tpl),
        mPage: 1,
        mCategory: 0,
        mSearchString: "",

        //menuHighlight: "catalog",

        pageTitle: str.label_choose_public_course,

        events: {
            "click [data-call]": Utils.callMapper,
            "change #catalog-search-input": "search",
            "keypress #catalog-search-input": "searchKeyPressed"
        },

        setPage: function (ev, target)
        {
            this.mPage = parseInt(target.getAttribute("data-page"));
            this.render();
        },

        setCategory: function (ev, target)
        {
            this.mCategory = parseInt(target.getAttribute("data-cat") || 0);
            this.mPage = 1;
            this.mSearchString = "";
            this.render();
        },

        searchKeyPressed: function (ev)
        {
            switch (ev.keyCode)
            {
                case 13:
                    this.search();
                    break;
            }
        },

        search: function ()
        {
            this.mPage = 1;
            this.mCategory = 0;
            this.mSearchString = $("#catalog-search-input").val();
            this.render();
        },

        clearSearch: function ()
        {
            this.mPage = 1;
            this.mCategory = 0;
            this.mSearchString = "";
            this.render();
        },

        openCourse: function (ev, target)
        {
            var id = parseInt(target.getAttribute("data-id")),
                item = this.currentCourseCollection.get(id);

            _Router.navigate("/reuse-preview/" + item.get("code"), { trigger: true });
        },

        cancelEdit: function ()
        {
            _Router.navigate("/teaching", { trigger: true });
        },

        pageRender: function ()
        {
            HeaderControls.set("state", 7);
            HeaderControls.set("handler", this);

            DataLoader.exec({ collection: FreeCourseCollection, context: this }, function (courses)
            {
                var clist = [ ];

                this.currentCourseCollection = courses;

                // In order for pagination to work normally, we
                // must create a filtered list of all items in collection.
                courses.each(function (c)
                {
                    var crs = c.toJSON();

                    // Category
                    if (this.mCategory && this.mCategory != crs.category)
                    {
                        return;
                    }

                    // Search
                    if (this.mSearchString)
                    {
                        var s = this.mSearchString.toLowerCase();

                        if (crs.fullname.toLowerCase().indexOf(s) == -1 &&
                            crs.teacher_name.toLowerCase().indexOf(s) == -1) {
                            return;
                        }
                    }

                    clist.push(crs);
                }, this);

                DataLoader.exec({ context: this, collection: CategoryCollection }, function (categories)
                {
                    var categoryList = [ ],
                        currentCategory = str.all;

                    categories.each(function (cat)
                    {
                        var id = cat.get("idnumber") || 0;

                        if (id)
                        {
                            categoryList.push(cat.toJSON());

                            if (this.mCategory == id)
                            {
                                currentCategory = cat.get("name");
                            }
                        }
                    }, this);

                    this.$el.html(this.template({
                        items: clist.slice((this.mPage - 1) * ITEMS_PER_PAGE, Math.min(this.mPage * ITEMS_PER_PAGE, clist.length)),
                        page: this.mPage,
                        itemCount: clist.length,
                        pageCount: Math.ceil(clist.length / ITEMS_PER_PAGE),
                        str: str,
                        categories: categoryList,
                        current_category_name: currentCategory,
                        search_string: this.mSearchString
                    }));

                    LinkHandler.setupView(this);
                });
            });
        }
    }));
});
