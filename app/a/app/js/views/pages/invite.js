/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/invite.html", "i18n!nls/strings", "app/views/pageview", "app/tools/linkhandler"],
		function (tpl, str, PageView, LinkHandler)
		{
	return new (PageView.extend(
			{
				template: _.template(tpl),

				el: "#page-content",

				render: function ()
				{

					this.$el.html(this.template({ str: str }));

					LinkHandler.setupView(this);

					return this;
				},

				hide: function ()
				{
					// nothing
				}
			}));
		});
