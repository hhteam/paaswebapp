/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/task/quiz/edit_quiz.html", "text!templates/pages/task/quiz_student_result.html", "i18n!nls/strings",
        "text!templates/pages/task/quiz/answer_mc.html", "text!templates/pages/task/quiz/answer_tf.html", "text!templates/pages/task/quiz/answer_sa.html",
        "app/views/pages/task/quiz_teacher_results", "app/models/task/question", "app/views/attachment/attachmentlist", "app/collections/attachments",
        "app/tools/utils", "app/tools/linkhandler", "app/views/pages/task/quiz/edit_quiz_question", "app/tools/quiz_utils", "app/tools/inlinecontent"],

    function (editTpl, tpl2, str, aMcTpl, aTfTpl, aSaTpl, ResultView, QuestionModel, AttachmentList, AttachmentCollection, Utils,
        LinkHandler, EditQuizQuestionView, QuizUtils, InlineContent)
{
    return Backbone.View.extend(
    {
        className: "QuizTeacherSub",
        el: "#content-box",

        template2: _.template(tpl2),
        editTemplate: _.template(editTpl),

        aMcTemplate: _.template(aMcTpl),
        aTfTemplate: _.template(aTfTpl),
        aSaTemplate: _.template(aSaTpl),

        mQuestionViews: { },
        mQuestionViewsToRemove: [ ],

        initialize: function ()
        {
            this.returnedView = new ResultView({ parentView: this.options.parentView });

            if (!this.options.parentView.model.isNew()) {
                this.options.parentView.courseModel.get("participants").bind("reset", this.render, this);      
            }
        },

        startEdit: function ()
        {
            this.renderEdit();
        },

        saveEdit: function (deferred, success, taskid)
        {
            this.options.parentView.model.get("attachments").each(function (att) {
                att.save();
            });

            var questions = this.options.parentView.model.get("questions");

            var listsToSave = questions.length, listsSaved = 0;

            var checkSavedLists = function () {
                if (listsSaved >= listsToSave) {
                    success();
                }
            };

            checkSavedLists();

            questions.each(function (q, idx) {
                // Each attachment in each question must know its' task id
                // and question index.
                this.mQuestionViews[q.cid].mAttachmentCollection.each(function (att) {
                    att.set({ options: idx + "", taskid: taskid }, { silent: true });
                }, this);

                this.mQuestionViews[q.cid].mAttachmentList.saveEdit({ deferred: deferred, success: function () {
                    listsSaved++;
                    checkSavedLists();
                } });
            }, this);

            for (var i=0; i<this.mQuestionViewsToRemove.length; i++) {
                this.mQuestionViewsToRemove[i].mAttachmentList.removeAll();
                this.mQuestionViewsToRemove[i].mAttachmentList.saveEdit({ deferred: deferred });
            }
        },

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            var taskData = this.options.parentView.model.toJSON(true);

            if (this.options.parentView.subid) {
                // Showing results of a single student
                var numAnswered = 0, total = 0, correct = 0;

                this.options.parentView.model.get("questions").each(function (q, num) {
                    var res = QuizUtils.getResult(this.options.parentView.model, q, this.options.parentView.subid);

                    total++;

                    if (res.correct) {
                        correct++;
                    }

                    if (res.hasAnswer) {
                        numAnswered++;
                    }
                }, this);

                this.$el.html(this.template2({ str: str, data: taskData, correct: correct, total: total }));

                var student = this.options.parentView.courseModel.get("participants").get(this.options.parentView.subid);

                if (student) {
                    sdata = student.toJSON();
                    $("#task-view-subheader-left").html("<img src=\"" + sdata.picurl.s + "\" style=\"width: 24px; height: 24px; border-radius: 15px; margin-right: 8px;\"> <span>" + sdata.fullname + "</span>");
                }

                $("#task-view-back-link span").html(str.button_back_to_results);
                $("#task-view-back-link").attr("href", "/quizzes/" + taskData.instanceid);

                this.options.parentView.model.get("questions").each(function (q, num) {
                    var result = QuizUtils.getResult(this.options.parentView.model, q, this.options.parentView.subid),
                        col = new AttachmentCollection();

                    this.options.parentView.model.get("attachments").each(function (att) {
                        if (parseInt(att.get("options")) == num) {
                            col.add(att);
                        }
                    }, this);

                    switch (q.get("type")) {
                        case 0:
                            this.$("#student-quiz-rezult-preview-block").append(this.aMcTemplate({ str: str, question: q.toJSON(true), result: result, num: num + 1, count: total, nonext: 1 }));
                            break;
                    
                        case 1:
                            this.$("#student-quiz-rezult-preview-block").append(this.aTfTemplate({ str: str, question: q.toJSON(true), result: result, num: num + 1, count: total, nonext: 1 }));
                            break;

                        case 2:
                            this.$("#student-quiz-rezult-preview-block").append(this.aSaTemplate({ str: str, question: q.toJSON(true), result: result, num: num + 1, count: total, nonext: 1 }));
                            break;

                        default:
                            console.warn("Invalid question type.");
                    }

                    (new AttachmentList({ collection: col, readOnly: true, source: 'task' })).setElement("#quiz-attachment-block-" + q.cid).render();
                }, this);

                InlineContent.fixTextDirection(this.$("#student-quiz-rezult-preview-block"));
                InlineContent.fixAttachmentUrls(this.$("#student-quiz-rezult-preview-block"), this.options.parentView.model.get("attachments"), false, false);
                InlineContent.fixMathJaxText(this.$("#student-quiz-rezult-preview-block"));
                LinkHandler.setupView(this);

            } else if (!this.options.parentView.isnew) {
                // Showing results of all students

                this.returnedView.setElement(this.$el).render();
            } else {
                // Is new - showing nothing, waiting for startEdit.
            }
        },

        renderEdit: function () {
            var data = this.options.parentView.model.toJSON();

            this.$el.html(this.editTemplate({ str: str, data: data }));

            var tModel = this.options.parentView.model,
                atts = tModel.get("attachments");

            this.options.parentView.model.get("questions").each(function (q, idx) {
                var view = new EditQuizQuestionView({ model: q, taskModel: tModel, num: idx+1 }),
                    i = 0;

                while (i < atts.models.length) {
                    if (parseInt(atts.models[i].get("options")) == parseInt(idx)) {
                        view.addAttachment(atts.models.splice(i, 1)[0]);
                    } else {
                        i++;
                    }
                }

                view.showOn(this.$("#quiz-edit-question-container"), this.options.parentView);

                this.mQuestionViews[q.cid] = view;
            }, this);

            atts.reset(atts.models, {silent: true});
        },

        addQuestion: function (ev, target) {
            var nqm;

            switch (parseInt($(target).attr("data-qtype"))) {
                case 0:
                    nqm = new QuestionModel({ answers: [{}, {}], type: 0 });
                    break;

                case 1:
                    nqm = new QuestionModel({ answers: [{text: "1"}, {text: "2"}], type: 1 });
                    break;

                case 2:
                    nqm = new QuestionModel({ answers: [{correct: 1, text: "-NONE-"}, {}], type: 2 });
                    break;

                default:
                    console.warn("Invalid question type.");
                    return;
            }

            var tModel = this.options.parentView.model;

            tModel.get("questions").add(nqm);

            var view = new EditQuizQuestionView({ model: nqm, taskModel: tModel, num: tModel.get("questions").length });

            view.showOn(this.$("#quiz-edit-question-container"), this.options.parentView);

            this.mQuestionViews[nqm.cid] = view;

            this.options.parentView.markAsChanged();
        },

        removeQuestion: function (ev, target) {
            var qc = this.options.parentView.model.get("questions"),
                cid = $(target).attr("data-question"),
                qv = this.mQuestionViews[cid];

            this.mQuestionViewsToRemove.push(qv);

            qc.remove(qc.get(cid), { silent: true });
            delete this.mQuestionViews[cid];
            this.$("[data-question-container=\"" + cid + "\"]").remove();

            this.options.parentView.inlineEdit.commit({ dry: true });

            qc.each(function (q, idx) {
                if (idx >= qv.mNum - 1) {
                    this.mQuestionViews[q.cid].setNum(idx + 1);
                    this.mQuestionViews[q.cid].render();
                    this.options.parentView.inlineEdit.reInit(this.mQuestionViews[q.cid].$el);
                }
            }, this);

            this.options.parentView.markAsChanged();
        },

        moveQuestionUp: function (ev, target) {
            var idx = parseInt($(target).attr("data-idx"));

            if (!idx) {
                return;
            }

            this.options.parentView.inlineEdit.commit({ dry: true });

            var qc = this.options.parentView.model.get("questions"),
                qm1 = qc.at(idx),
                qm2 = qc.at(idx - 1);

            qc.models.splice(idx - 1, 2, qm1, qm2);
            this.$("[data-question-container=\"" + qm2.cid + "\"]").before(this.$("[data-question-container=\"" + qm1.cid + "\"]"));

            this.mQuestionViews[qm1.cid].setNum(idx);
            this.mQuestionViews[qm1.cid].render();
            this.options.parentView.inlineEdit.reInit(this.mQuestionViews[qm1.cid].$el);

            this.mQuestionViews[qm2.cid].setNum(idx + 1);
            this.mQuestionViews[qm2.cid].render();
            this.options.parentView.inlineEdit.reInit(this.mQuestionViews[qm2.cid].$el);
        },

        moveQuestionDown: function (ev, target) {
            var idx = parseInt($(target).attr("data-idx")),
                qc = this.options.parentView.model.get("questions");

            if (idx >= qc.length - 1) {
                return;
            }

            this.options.parentView.inlineEdit.commit({ dry: true });

            var qm1 = qc.at(idx + 1),
                qm2 = qc.at(idx);

            qc.models.splice(idx, 2, qm1, qm2);
            this.$("[data-question-container=\"" + qm2.cid + "\"]").before(this.$("[data-question-container=\"" + qm1.cid + "\"]"));

            this.mQuestionViews[qm1.cid].setNum(idx + 1);
            this.mQuestionViews[qm1.cid].render();
            this.options.parentView.inlineEdit.reInit(this.mQuestionViews[qm1.cid].$el);

            this.mQuestionViews[qm2.cid].setNum(idx + 2);
            this.mQuestionViews[qm2.cid].render();
            this.options.parentView.inlineEdit.reInit(this.mQuestionViews[qm2.cid].$el);
        }
    });
});
