/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/task/returned_list.html", "i18n!nls/strings", "app/tools/utils", "app/collections/task/returned", "moment", "backbone"],
    function (tpl, str, Utils, ReturnedCollection)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        ITEMS_PER_PAGE: 10,

        el: "#returned-list-container",

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function () {
            var data = [];

            _.each(ReturnedCollection.where({ assignid: this.assignid }), function (item) {
                data.push(item.toJSON());
            }, this);

            var pages = Math.ceil(data.length / this.ITEMS_PER_PAGE);

            this.$el.html(this.template({ data: data, pages: pages, page: this.page, ipp: this.ITEMS_PER_PAGE, str: str }));
        },

        changePage: function (ev, target) {
            this.page = parseInt($(target).attr("data-page"));
            this.render();
        }
    });
});
