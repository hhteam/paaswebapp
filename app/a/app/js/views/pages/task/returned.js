/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/task/returned.html","text!templates/pages/task/returnedtext.html", "i18n!nls/strings",
        "app/collections/task/returned", "app/collections/course/grades", "app/tools/utils", "app/views/pages/task/grade",
        "app/views/pages/task/returned_list"],
    function (tpl, tplText, str, ReturnedCollection, GradesCollection, Utils, GradeView, ListView)
{
    return Backbone.View.extend(
    {
        mTaskId: -1,
        template: _.template(tpl),
        templateText: _.template(tplText),

        events: {
            "click [data-call]": Utils.callMapper
        },

        setTaskId: function (taskid, courseid, course)
        {
            this.mTaskId = taskid;
            this.mCourseId = courseid;
            this.mCourse = course;

            ReturnedCollection.fetchForTasks(taskid);

            return this;
        },

        initialize: function ()
        {
            ReturnedCollection.bind("updated", this.render, this);

            return this;
        },

        render: function ()
        {
            this.$el.html(this.template({ str: str }));

            if (ReturnedCollection.length > 0) {
                this.mCurrentList = new ListView();

                this.mCurrentList.page = 0;
                this.mCurrentList.assignid = this.mTaskId;
                this.mCurrentList.render();
            } else {
                // No results (yet).
            }

            return this;
        },

        openGradeBox: function (ev, target) {
            var studentid = $(target).attr("data-studentid"),
                lastId = null,
                nextId = null;

            this.mCurrentGrade = null;

            _.each(ReturnedCollection.where({ assignid: this.mTaskId }), function (item, idx, list) {
                var sid = item.get("studentid");

                if (sid == studentid) {
                    this.mCurrentGrade = item;
                } else {
                    if (this.mCurrentGrade == null) {
                        lastId = sid;
                    } else if (nextId == null) {
                        nextId = sid;
                    }
                }
            }, this);

            if (this.mCurrentGrade != null) {
                this.mCurrentView = new GradeView();
                this.mCurrentView.data = this.mCurrentGrade;
                this.mCurrentView.previous = lastId;
                this.mCurrentView.next = nextId;
                this.mCurrentView.cantwrite = this.mCourse.get("cantwrite");
                this.mCurrentView.render();

                // XXX: only do this if element isn't visible...
                //$('html,body').animate({ scrollTop: view.$el.offset().top - 20 }, 'fast');
            } else {
                console.warn("ERROR: student lost.");
            }
        },

        gradeTask: function () {
            if (this.mCourse.get("cantwrite")) {
                return;
            }

            var thisThis = this,
                data = {
                    grade: "" + parseInt(this.$("#grade-input-field").val()),
                    feedback: this.$("#feedback-input-field").val(),
                    gradestring: parseInt(this.$("#grade-input-field").val()) + "/" + this.mCurrentGrade.get("grademax")
                };

            if (parseInt(data.grade) >= 0 && parseInt(data.grade) <= parseInt(this.mCurrentGrade.get("grademax"))) {
                this.mCurrentGrade.save(data, { success: function (model, response, options) {
                    thisThis.mCurrentList.render();
                    thisThis.mCurrentView.render();

                    var grades = GradesCollection.where({ courseid: thisThis.mCourseId });

                    _.each(grades, function (gr) {
                        GradesCollection.remove(gr);
                    });
                }});
            } else {
                this.$("#grade-input-field").css("border-color", "red").focus();
            }
        },

        draftTask: function () {
            if (this.mCourse.get("cantwrite")) {
                return;
            }

            var thisThis = this,
                data = {
                    feedback: this.$("#feedback-input-field").val(),
                    grade: null,
                    gradestring: null,
                    revert_to_draft: 1,
                    lastsubmissiondate: 0
                };

            this.mCurrentGrade.save(data, { success: function (model, response, options) {
                thisThis.mCurrentList.render();
                thisThis.mCurrentView.render();
            }});
        }
    });
});
