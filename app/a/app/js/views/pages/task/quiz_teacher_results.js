/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/task/quiz_teacher_results.html", "text!templates/pages/task/quiz_teacher_results_empty.html",
        "i18n!nls/strings", "app/tools/linkhandler", "app/tools/quiz_utils", "app/tools/utils", "app/tools/servicecalls",
        "app/collections/task/tasks", "app/views/dialogs/yes_no", "moment", "backbone"],
    function (tpl, tpl2, str, LinkHandler, QuizUtils, Utils, SC, TaskCollection, YesNoDialog)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        template2: _.template(tpl2),

        initialize: function ()
        {
            this.options.parentView.courseModel.get("participants").bind("reset", this.render, this);      

            if (this.options.parentView.courseModel.get("participants").length < 1)
            {
                this.options.parentView.courseModel.get("participants").fetch();
            }
        },

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            if (this.options.parentView.model.isNew())
            {
                return;
            }

            var task = this.options.parentView.model.toJSON(), data = [], ret_count = 0, percent_sum = 0, correct_sum = 0;

            this.options.parentView.courseModel.get("participants").each(function (user) {
                var usr = user.toJSON();

                if (!usr.teacher && !usr.manager) {
                    _.each(task.answers, function (ans) {
                        if (ans.student == usr.id) {
                            usr.finished = 1;
                            usr.datetaken = ans.datetaken;

                            return;
                        }
                    });

                    if (usr.finished) {
                        var total = 0, correct = 0;

                        this.options.parentView.model.get("questions").each(function (q) {
                            var res = QuizUtils.getResult(this.options.parentView.model, q, usr.id);

                            total++;

                            if (res.correct) {
                                correct++;
                            }
                        }, this);

                        usr.correct = correct;
                        usr.total = total;
                        usr.result = Math.round(correct / total * 100);

                        percent_sum += usr.result;
                        correct_sum += correct;
                        ret_count++;
                    }

                    data.push(usr);
                }
            }, this);

            // !data.length - no students at all
            // !ret_count - no student results
            if (!data.length || !ret_count) {
                this.$el.html(this.template2({ str: str }));
            } else {
                this.$el.html(this.template({
                    str: str,
                    data: data,
                    task: task,
                    ret_count: ret_count,
                    average_percent: (ret_count > 0 ? Math.round(percent_sum / ret_count) + "%" : "-"),
                    average_answer: (ret_count > 0 ? Math.round(correct_sum / ret_count) : "-")
                }));
            }

            LinkHandler.setupView(this);

            return this;
        },

        resetResult: function (ev, target) {
            var thisThis = this;

            (new YesNoDialog({ header: str.quiz_reset_progress, message: str.quiz_reset_progress_msg,
                accepted: function () {
                    var data = {
                        userid: parseInt($(target).attr("data-studentid")),
                        courseid: parseInt(thisThis.options.parentView.courseModel.get("id")),
                        quizid: parseInt(thisThis.options.parentView.model.get("id"))
                    };

                    SC.call("local_monorailservices_reset_tasks", data, function () {
                        TaskCollection.reset();
                        thisThis.options.parentView.render();
                    });
                }})).render();
        }
    });
});
