/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/task/task_teacher.html", "i18n!nls/strings", "app/views/pages/task/returned"],
    function (tpl, str, ReturnedView)
{
    return Backbone.View.extend(
    {
        className: "TaskTeacherSub",
        el: "#content-box",

        template: _.template(tpl),

        initialize: function ()
        {
            this.returnedView = new ReturnedView();
        },

        render: function ()
        {
            var taskData = this.options.parentView.model.toJSON();

            this.$el.html(this.template({ data: taskData, str: str }));
            var addBtnItems = [ ];
            var att = this.options.parentView.mAttachments;

            if (att instanceof Object) {
                addBtnItems.push({ label: str.new_file, handler: att.newFile, owner: att, data: { taskid: taskData.instanceid, source: "new_item"  } });
                if(((typeof videoUploadEnabled !== 'undefined') && videoUploadEnabled) || _User.hasFeature("video")) {
                  addBtnItems.push({ label: str.upload_video, handler: att.videoUpload, owner: att, data: { taskid: taskData.instanceid, source: "new_item", origin: "tasks" } }); 
                }
                att.setAddButton(addBtnItems).setElement("#assignment-attachment-view").render();
            }

            if (this.options.parentView._preview !== true && !this.options.parentView.isnew && !taskData.nosubmissions)  // TODO: enable returned view for no-submission tasks (#889).
            {
                this.returnedView.setTaskId(taskData.id, taskData.courseid, this.options.parentView.courseModel).setElement("#assignment-returned-view").render();
            }
        }
    });
});
