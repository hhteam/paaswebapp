/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/views/pageview", "text!templates/pages/task/preview.html", "i18n!nls/strings", "app/tools/dataloader", "app/tools/linkhandler",
        "app/tools/utils", "app/collections/task/tasks", "app/views/pages/task/task_teacher", "app/views/pages/task/quiz_student",
        "app/tools/inlinecontent"],

function (PageView, tpl, str, DataLoader, LinkHandler, Utils, TaskCollection, TaskTeacherSub, QuizStudentSub, InlineContent)
{
    return new (PageView.extend(
    {
        template: _.template(tpl),

        pageRender: function ()
        {
            DataLoader.exec({ collection: TaskCollection, where: { instanceid: this.instance }, context: this }, function (model)
            {
                this.model = model;
                this._preview = true;

                this.$el.html(this.template({ str: str, task: model.toJSON() }));

                switch (model.get("modulename")) {
                    case "assign":
                        (new TaskTeacherSub({ parentView: this })).render();
                        break;

                    case "quiz":
                        (new QuizStudentSub({ parentView: this })).render();
                        break;

                    default:
                        console.warn("Invalid task type.");
                }

                InlineContent.fixTextDirection(this.$el);
                InlineContent.fixMathJaxText(this.$el);
                InlineContent.fixAttachmentUrls(this.$el, this.model.get("attachments"), false, false);

                LinkHandler.setupView(this);
            });
        }
    }));
});
