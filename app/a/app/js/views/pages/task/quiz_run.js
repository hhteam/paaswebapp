/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/proupgrade.html", "i18n!nls/strings", "app/views/pageview", "app/tools/linkhandler", "app/tools/dataloader",
        "text!templates/pages/task/quiz/question_mc.html", "text!templates/pages/task/quiz/question_tf.html", "text!templates/pages/task/quiz/question_sa.html",
        "text!templates/pages/task/quiz/answer_mc.html", "text!templates/pages/task/quiz/answer_tf.html", "text!templates/pages/task/quiz/answer_sa.html",
        "app/tools/utils", "app/models/headercontrols", "app/collections/task/tasks", "app/tools/inlinecontent", "app/collections/attachments",
        "app/views/attachment/attachmentlist", "app/tools/inlineedit", "app/tools/quiz_utils", "app/collections/course/courses",
        "app/collections/course/coursesshort"],

    function (tpl, str, PageView, LinkHandler, DataLoader, qMcTpl, qTfTpl, qSaTpl, aMcTpl, aTfTpl, aSaTpl, Utils, HeaderControls, TaskCollection, InlineContent,
        AttachmentCollection, AttachmentList, InlineEdit, QuizUtils, CourseCollection, CourseShortCollection)
{
    return new (PageView.extend(
    {
        template: _.template(tpl),

        qMcTemplate: _.template(qMcTpl),
        qTfTemplate: _.template(qTfTpl),
        qSaTemplate: _.template(qSaTpl),
        aMcTemplate: _.template(aMcTpl),
        aTfTemplate: _.template(aTfTpl),
        aSaTemplate: _.template(aSaTpl),

        mCurrentQuestion: 0,
        mCurrentStep: 0,
        mAttachmentCollections: { },

        events: {
            "click [data-call]": Utils.callMapper
        },

        initialize: function () {
            this.inlineEdit = new InlineEdit();
        },

        render: function () {
            DataLoader.exec({ collection: TaskCollection, where: { instanceid: this.instanceid }, context: this }, function (model) {
                if (model.isNew()) {
                    console.warn("Quiz not found.");
                    _Router.navigate("/", { trigger: true, replace: true });
                } else {
                    this.model = model;

                    this.model.get("questions").each(function (q, idx) {
                        var col = new AttachmentCollection();

                        this.model.get("attachments").each(function (att) {
                            if (parseInt(att.get("options")) == idx) {
                                col.add(att);
                            }
                        }, this);

                        this.mAttachmentCollections[q.cid] = col;
                    }, this);

                    if (!this.selectNextQuestion()) {
                        // All questions have answers. Go away.
                        _Router.navigate("/quizzes/" + this.instanceid, { trigger: true, replace: true });
                        return;
                    }

                    HeaderControls.set({ handler: this, state: 8 });
                    this.showQuestion();
                }
            });
        },

        finishLater: function () {
            _Router.navigate("/quizzes/" + this.instanceid, { trigger: true, replace: true });
        },

        showQuestion: function () {
            var qc = this.model.get("questions"),
                q = qc.at(this.mCurrentQuestion);

            switch (q.get("type")) {
                case 0:
                    this.$el.html(this.qMcTemplate({ str: str, question: q.toJSON(true), num: this.mCurrentStep }));
                    break;

                case 1:
                    this.$el.html(this.qTfTemplate({ str: str, question: q.toJSON(true), num: this.mCurrentStep }));
                    break;

                case 2:
                    this.$el.html(this.qSaTemplate({ str: str, question: q.toJSON(true), num: this.mCurrentStep }));
                    break;

                default:
                    console.warn("Invalid question type.");
            }

            (new AttachmentList({ collection: this.mAttachmentCollections[q.cid], readOnly: true, source: 'task' })).setElement("#quiz-attachment-block-" + q.cid).render();

            InlineContent.fixTextDirection(this.$el);
            InlineContent.fixAttachmentUrls(this.$el, this.model.get("attachments"), false, false);
            InlineContent.fixMathJaxText(this.$el);

            this.inlineEdit.init(this);
            this.inlineEdit.start();

            $("#edit-caption").html("<div class=\"blue-header small\" style=\"text-align: center;\">" + str.quiz_progress.replace("$1", this.mCurrentStep + "/" + qc.length) + "</div>");
        },

        showAnswer: function () {
            var qc = this.model.get("questions"),
                q = qc.at(this.mCurrentQuestion),
                result = QuizUtils.getResult(this.model, q),
                singleTry = (parseInt(this.model.get("attempts")) == 1);

            switch (q.get("type")) {
                case 0:
                    this.$el.html(this.aMcTemplate({ str: str, question: q.toJSON(true), result: result, num: this.mCurrentStep, hasNext: qc.length > this.mCurrentStep, singleTry: singleTry }));
                    break;
            
                case 1:
                    this.$el.html(this.aTfTemplate({ str: str, question: q.toJSON(true), result: result, num: this.mCurrentStep, hasNext: qc.length > this.mCurrentStep, singleTry: singleTry }));
                    break;

                case 2:
                    this.$el.html(this.aSaTemplate({ str: str, question: q.toJSON(true), result: result, num: this.mCurrentStep, hasNext: qc.length > this.mCurrentStep, singleTry: singleTry }));
                    break;

                default:
                    console.warn("Invalid question type.");
            }

            (new AttachmentList({ collection: this.mAttachmentCollections[q.cid], readOnly: true, source: 'task' })).setElement("#quiz-attachment-block-" + q.cid).render();

            InlineContent.fixTextDirection(this.$el);
            InlineContent.fixAttachmentUrls(this.$el, this.model.get("attachments"), false, false);
            InlineContent.fixMathJaxText(this.$el);

            $("#edit-caption").html("<div class=\"blue-header small\" style=\"text-align: center;\">" + str.quiz_progress.replace("$1", this.mCurrentStep + "/" + qc.length) + "</div>");
        },

        submitAnswer: function () {
            var that = this;

            this.inlineEdit.commit({ dry: true });

            this.model.save({ attempt: 1, current_question: this.mCurrentQuestion }, { validate: false, success: function ()
            {
                that.showAnswer();
            }});
        },

        nextQuestion: function () {
            if (!this.selectNextQuestion()) {
                // Last question...

                var course = CourseCollection.get(this.model.get("courseid"));

                if (course) {
                    course.get("assignments").reset();
                }

                TaskCollection.reset();
                CourseCollection.reset();
                CourseShortCollection.LearningOngoing.reset();
                CourseShortCollection.LearningCompleted.reset();
                CourseShortCollection.TeachingOngoing.reset();
                CourseShortCollection.TeachingCompleted.reset();

                _Router.navigate("/quizzes/" + this.instanceid, { trigger: true, replace: true });
            } else {
                this.showQuestion();
            }
        },

        selectNextQuestion: function () {
            var unanswered = [ ];

            this.model.get("questions").each(function (q, idx) {
                var result = QuizUtils.getResult(this.model, q);

                if (!result.hasAnswer) {
                    unanswered.push(idx);
                }
            }, this);

            if (unanswered.length > 0) {
                if (parseInt(this.model.get("question_order"))) {
                    this.mCurrentQuestion = unanswered[Math.floor(Math.random() * unanswered.length)];
                } else {
                    this.mCurrentQuestion = unanswered[0];
                }

                this.mCurrentStep = this.model.get("questions").length - unanswered.length + 1;

                return true;
            } else {
                return false;
            }
        }
    }));
});
