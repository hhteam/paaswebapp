/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define([/*"text!templates/pages/task/quiz_student.html", "text!templates/pages/task/quiz_student_result.html",*/ "text!templates/pages/task/quiz/start_quiz.html",
        "text!templates/pages/task/quiz/end_quiz.html", "text!templates/pages/task/quiz/answer_mc.html", "text!templates/pages/task/quiz/answer_tf.html",
        "text!templates/pages/task/quiz/answer_sa.html", "i18n!nls/strings", "app/collections/course/courses", "app/collections/attachments",
        "app/views/attachment/attachmentlist", "app/tools/inlinecontent", "app/collections/task/tasks", "app/tools/dataloader", "app/collections/course/coursesshort",
        "app/tools/utils", "app/tools/linkhandler", "app/tools/quiz_utils"],
    function (/*tpl, tpl2,*/ startTpl, endTpl, aMcTpl, aTfTpl, aSaTpl, str, CourseCollection, AttachmentCollection, AttachmentList, InlineContent, TaskCollection,
        DataLoader, CourseShortCollection, Utils, LinkHandler, QuizUtils)
{
    return Backbone.View.extend(
    {
        className: "QuizStudentSub",
        el: "#content-box",

        //template: _.template(tpl),
        //template2: _.template(tpl2),
        startTemplate: _.template(startTpl),
        endTemplate: _.template(endTpl),
        aMcTemplate: _.template(aMcTpl),
        aTfTemplate: _.template(aTfTpl),
        aSaTemplate: _.template(aSaTpl),

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            var taskData = this.options.parentView.model.toJSON(true);
            var numAnswered = 0, total = 0, correct = 0;

            this.options.parentView.model.get("questions").each(function (q) {
                var res = QuizUtils.getResult(this.options.parentView.model, q);

                total++;

                if (res.correct) {
                    correct++;
                }

                if (res.hasAnswer) {
                    numAnswered++;
                }
            }, this);

            if (taskData.endquiz) {
                // Quiz finished?
                this.$el.html(this.endTemplate({ str: str, data: taskData, total: total, correct: correct }));
            } else {
                // Start/restart/continue
                if (taskData.nolatesubmissions && taskData.duedate && taskData.duedate < ((new Date()).getTime() / 1000)) {
                    this.$el.html("<div class=\"blue-header\" style=\"text-align: center; padding: 40px 0px;\">" + str.late_submissions_too_late + "</div>");
                } else {
                    this.$el.html(this.startTemplate({ str: str, data: taskData, answered: numAnswered, total: total, correct: correct, pastDue: (taskData.duedate && taskData.duedate < ((new Date()).getTime() / 1000) && taskData.nolatesubmissions) }));
                }
            }

            LinkHandler.setupView(this);
        },

        startQuiz: function () {
            _Router.navigate("/quiz-take/" + this.options.parentView.model.get("instanceid"), { trigger: true, replace: true });
        },

        submitQuiz: function () {
            var that = this;

            this.options.parentView.model.save({ endquiz: 1 }, { validate: false, success: function () {
                var course = CourseCollection.get(that.options.parentView.model.get("courseid"));

                if (course) {
                    course.get("assignments").reset();
                }

                TaskCollection.reset();
                CourseCollection.reset();
                CourseShortCollection.LearningOngoing.reset();
                CourseShortCollection.LearningCompleted.reset();
                CourseShortCollection.TeachingOngoing.reset();
                CourseShortCollection.TeachingCompleted.reset();

                that.options.parentView.render();
            }});
        },

        restartQuiz: function () {
            var that = this;

            this.options.parentView.model.save({ attempt: 2 }, { validate: false, success: function () {
                var course = CourseCollection.get(that.options.parentView.model.get("courseid"));

                if (course) {
                    course.get("assignments").reset();
                }

                TaskCollection.reset();
                CourseCollection.reset();
                CourseShortCollection.LearningOngoing.reset();
                CourseShortCollection.LearningCompleted.reset();
                CourseShortCollection.TeachingOngoing.reset();
                CourseShortCollection.TeachingCompleted.reset();

                _Router.navigate("/quiz-take/" + that.options.parentView.model.get("instanceid"), { trigger: true, replace: true });
            }});
        },

        showResults: function () {
            if (this.$("#quiz-answer-review-block").is(":empty")) {
                var count = this.options.parentView.model.get("questions").length,
                    endquiz = this.options.parentView.model.get("endquiz") ? 1 : 0;

                this.options.parentView.model.get("questions").each(function (q, num) {
                    var result = QuizUtils.getResult(this.options.parentView.model, q),
                        col = new AttachmentCollection();

                    this.options.parentView.model.get("attachments").each(function (att) {
                        if (parseInt(att.get("options")) == num) {
                            col.add(att);
                        }
                    }, this);

                    switch (q.get("type")) {
                        case 0:
                            this.$("#quiz-answer-review-block").append(this.aMcTemplate({ str: str, question: q.toJSON(true), result: result, num: num + 1, count: count, nonext: 1, singleTry: endquiz }));
                            break;
                    
                        case 1:
                            this.$("#quiz-answer-review-block").append(this.aTfTemplate({ str: str, question: q.toJSON(true), result: result, num: num + 1, count: count, nonext: 1, singleTry: endquiz }));
                            break;

                        case 2:
                            this.$("#quiz-answer-review-block").append(this.aSaTemplate({ str: str, question: q.toJSON(true), result: result, num: num + 1, count: count, nonext: 1, singleTry: endquiz }));
                            break;

                        default:
                            console.warn("Invalid question type.");
                    }

                    (new AttachmentList({ collection: col, readOnly: true, source: 'task' })).setElement("#quiz-attachment-block-" + q.cid).render();
                }, this);

                InlineContent.fixTextDirection(this.$("#quiz-answer-review-block"));
                InlineContent.fixAttachmentUrls(this.$("#quiz-answer-review-block"), this.options.parentView.model.get("attachments"), false, false);
                InlineContent.fixMathJaxText(this.$("#quiz-answer-review-block"));
            } else {
                this.$("#quiz-answer-review-block").html("");
            }
        }
    });
});
