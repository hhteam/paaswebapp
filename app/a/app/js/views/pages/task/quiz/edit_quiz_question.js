/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/task/quiz/edit_quiz_mc.html", "text!templates/pages/task/quiz/edit_quiz_tf.html", "text!templates/pages/task/quiz/edit_quiz_sa.html",
        "i18n!nls/strings", "app/tools/utils", "app/tools/dataloader", "app/models/task/answer", "app/collections/attachments", "app/views/attachment/attachmentlist",
        "backbone"],
    function (tplMs, tplTf, tplSa, str, Utils, DataLoader, AnswerModel, AttachmentCollection, AttachmentList)
{
    return Backbone.View.extend(
    {
        templateMs: _.template(tplMs),
        templateTf: _.template(tplTf),
        templateSa: _.template(tplSa),

        mAttachmentList: null,
        mAttachmentCollection: null,
        mShowing: false,

        events: {
            "click [data-call]": Utils.callMapper
        },

        initialize: function () {
            this.mNum = this.options.num;
            this.mAttachmentCollection = new AttachmentCollection();
        },

        setNum: function (num) {
            this.mNum = num;
        },

        addAttachment: function (att) {
            this.mAttachmentCollection.add(att);
        },

        render: function () {
            switch (this.model.get("type")) {
                case 0:
                    this.$el.html(this.templateMs({ str: str, num: this.mNum, task: this.options.taskModel.toJSON(true), question: this.model.toJSON(true) }));
                    break;

                case 1:
                    this.$el.html(this.templateTf({ str: str, num: this.mNum, task: this.options.taskModel.toJSON(true), question: this.model.toJSON(true) }));
                    break;

                case 2:
                    this.$el.html(this.templateSa({ str: str, num: this.mNum, task: this.options.taskModel.toJSON(true), question: this.model.toJSON(true) }));
                    break;

                default:
                    console.warn("Invalid question type: " + this.model.get("type"));
            }

            if (this.mAttachmentList) {
                this.mAttachmentList.setElement("#assignment-attachment-view-" + this.model.cid);
                this.mAttachmentList.render();
                this.mAttachmentList.startEdit();
            }
        },

        showOn: function (par, parentView) {
            if (this.mShowing) {
                console.warn("Quiz question editor already assigned to view!");
            }

            this.mShowing = true;

            this.mAttachmentList = new AttachmentList({
                collection: this.mAttachmentCollection,
                inlineEdit: parentView.inlineEdit,
                //attachmentPath: "attachments",    No model - no problems
                section: {id: this.mNum},
                source: 'task'
            });

            if (((typeof videoUploadEnabled !== 'undefined') && videoUploadEnabled)  || _User.hasFeature("video")) {
                this.mAttachmentList.setAddButton([
                    { label: str.new_file, handler: this.mAttachmentList.newFile, owner: this.mAttachmentList, data: { taskid: this.options.taskModel.get("instanceid"), source: "new_item"  }},
                    { label: str.upload_video, handler: this.mAttachmentList.videoUpload, owner: this.mAttachmentList, data: { taskid: this.options.taskModel.get("instanceid"), source: "new_item", origin: 'task_quiz' }}
                ])
            } else {
                this.mAttachmentList.setAddButton([
                    { label: str.new_file, handler: this.mAttachmentList.newFile, owner: this.mAttachmentList, data: { taskid: this.options.taskModel.get("instanceid"), source: "new_item"  }}
                ])
            }

            this.parentView = parentView;

            var node = $("<div/>").attr("data-question-container", this.model.cid);
            par.append(node);

            this.setElement(node);
            this.render();
            this.parentView.inlineEdit.reInit(this.$el);
        },

        addAnswer: function (ev, target) {
            this.parentView.inlineEdit.commit({ dry: true });
            this.model.get("answers").add(new AnswerModel());
            this.render();
            this.parentView.inlineEdit.reInit(this.$el);
            this.parentView.markAsChanged();
        },

        removeAnswer: function (ev, target) {
            var qm = this.model.get("answers");
            this.parentView.inlineEdit.commit({ dry: true });
            qm.remove(qm.get($(target).attr("data-answer")), { silent: true });
            this.render();
            this.parentView.inlineEdit.reInit(this.$el);
            this.parentView.markAsChanged();
        }
    });
});
