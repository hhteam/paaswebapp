/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/task/task_student.html", "i18n!nls/strings", "app/views/pages/task/submission",
        "app/collections/task/submissions", "app/tools/inlinecontent", "app/tools/dataloader"],
    function (tpl, str, SubmissionView, SubmissionsCollection, InlineContent, DataLoader)
{
    return Backbone.View.extend(
    {
        className: "TaskStudentSub",
        el: "#content-box",

        template: _.template(tpl),

        initialize: function ()
        {
            this.submissionView = new SubmissionView();
        },

        render: function ()
        {
            var taskData = this.options.parentView.model.toJSON();
            var submissions = new SubmissionsCollection();
            submissions.setTaskId(taskData.id);

            DataLoader.exec({ context: this, collection: submissions }, function (submissions)
            {
                // TODO: serious refactoring needed...
                this.submissionView.model = submissions.get(taskData.id);
                this.submissionView.taskModel = this.options.parentView.model;
                this.submissionView.parentView = this;

                if (this.submissionView.model.get("status") === 1) {
                    if (this.submissionView.model.get("grade").grade === null) {
                        $("#due-date-view").html(str.submitted + ". " + str.waiting_for_grade);
                    } else {
                        $("#due-date-view").html('');
                    }
                }

                this.$el.html(this.template({ data: taskData, str: str, grade: this.submissionView.model.get("grade") }));

                InlineContent.fixTextDirection(this.$el);
                InlineContent.fixAttachmentUrls(this.$el, this.options.parentView.model.get("attachments"), false, false);
                InlineContent.fixMathJaxText(this.$el);
                this.options.parentView.mAttachments.setElement("#assignment-attachment-view").render();
                this.submissionView.setTaskId(taskData.id);

                if (!taskData.nosubmissions)
                {
                    // check if studen has already submitted a task before the due date
                    if (this.submissionView.model.get("timesubmitted") !=0 && this.submissionView.model.get("timesubmitted") < taskData.duedate) {
                        this.submissionView.setElement("#assignment-submission-view").render();
                    } else if (!taskData.duedate || taskData.duedate > ((new Date()).getTime() / 1000) || !taskData.nolatesubmissions) {
                        this.submissionView.setElement("#assignment-submission-view").render();
                    } else {
                        this.$("#assignment-submission-view").html("<div class=\"block\" style=\"padding: 10px 30px;\"><h3 class=\"block-title\">" + str.task_your_answer + "</h3><div class=\"blue-header\" style=\"text-align: center; padding: 40px 0px;\">" + str.late_submissions_too_late + "</div></div>");
                    }
                }
                else
                {
                    this.$("#assignment-submission-view").html("<div style=\"text-align: center;\"><div class=\"blue-header\">" + str.task_no_submission + "</div><div class=\"blue-header small\">" + str.task_no_submission_student_info + "</div></div>");
                }
            });
        }
    });
});
