/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/task/grade.html", "i18n!nls/strings", "app/tools/utils", "backbone"],

    function (tpl, str, Utils)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),

        el: "#returned-grade-container",

        events: {
            "click [data-call]": Utils.callMapper,
            "change #grade-input-field": "gradeChanged",
            "change #feedback-input-field": "gradeChanged"
        },

        render: function () {
            this.$el.html(this.template({ data: this.data.toJSON(), str: str, Utils: Utils, next: this.next, previous: this.previous, cantwrite: this.cantwrite }));

            Utils.setupPopups(this.$el.get(0));
        },

        gradeChanged: function () {
            this.$("[data-call='gradeTask']").removeClass("disabled");
        }
    });
});
