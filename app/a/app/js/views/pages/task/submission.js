/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/task/submission.html", "text!templates/pages/task/submissionfiles.html", "i18n!nls/strings",
        "app/models/headercontrols", "app/views/dialogs/yes_no", "app/views/dialogs/messagebox", "app/models/task/submission",
        "app/tools/inlineedit", "app/tools/servicecalls", "app/tools/linkhandler", "app/tools/inlinecontent",
        "app/tools/utils", "app/collections/task/tasks", "app/collections/course/courses"],

    function (tpl, tplFiles, str, HeaderControls, YesNoDialog, MessageBox, SubmissionModel, InlineEdit, SC, LinkHandler,
            InlineContent, Utils, TaskCollection, CourseCollection)
{
    return Backbone.View.extend({
        template: _.template(tpl),
        templateFiles: _.template(tplFiles),

        mFinalizing: false,

        events: {
            "click [data-call]": Utils.callMapper,
            "click .attachment-remove-button": "removeFile"
        },

        saveFinal: function ()
        {
            if (this.model.get("status") === 0) {
                // tried hooking to model.previousAttributes at model.save but previous
                // not available any more there
                this.mFinalizing = true;
            }

            this.model.set("status", 1);
            this.saveEdit();
        },

        saveEdit: function () {
            var deferred = $.Deferred(),
                thisThis = this;

            deferred.done(function () {
                _Router.setConfirmLeave(null, null, null);
            });

            this.model.save({ text: this.filteredText() }, { deferred: deferred, finalizing: this.mFinalizing, success: function ()
            {
                var course = CourseCollection.get(thisThis.taskModel.get("courseid"));

                if (course) {
                    course.get("assignments").reset();
                }

                TaskCollection.reset();
                thisThis.parentView.render();
            }});

            return deferred;
        },

        setTaskId: function (id)
        {
            this.model.set('assignid', id);
            this.model.get("files").bind("add", this.renderFiles, this);
            this.model.get("files").bind("change", this.renderFiles, this);
            this.model.get("files").bind("reset", this.renderFiles, this);
            this.model.get("files").setTaskId(id);
            this.model.get("files").reset();
            this.model.get("files").fetch();

            return this;
        },

        render: function ()
        {
            if (! this.model) {
                return;
            }

            this.$el.html(this.template({ str: str, data: this.model.toJSON(), task: this.taskModel.toJSON() }));

            LinkHandler.setupView(this);

            this.renderFiles();

            if (!parseInt(this.model.get("status"))) {
                var that = this;

                setTimeout(function () {
                    require(["ckeditor"], function () {
                        CKEDITOR.disableAutoInline = true;

                        try {
                            that.ckeditor = CKEDITOR.replace($("#task-submission-text-input").get(0), {
                                toolbarGroups: [
                                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                                    { name: 'clipboard', groups: [ /*'undo',*/ 'clipboard' ] },
                                    { name: 'links' },
                                    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] }
                                ],
                                language: _User.get("lang"),
                                contentsLangDirection: (CKEDITOR.config.language == "ar" ? "rtl" : "ui"),
                                removePlugins: 'elementspath,resize,magicline',
                                removeButtons: "Subscript,Superscript,Paste,Anchor,CreateDiv",
                                height: "150px",
                                contentsCss: RootDir + "app/css/basic.css",
                                //mathJaxLib: 'https://c328740.ssl.cf1.rackcdn.com/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
                            });
                        } catch (err) {
                            that.ckeditor = CKEDITOR.instances["task-submission-text-input"];
                        }

                        that.ckeditor.setData(that.model.get("text"));

                        that.ckeditor.on("change", function () {
                            that.thingsChanged();
                        });
                    });
                }, 100);
            }
        },

        renderFiles: function () {
            var files = this.model.get("files").toJSON();

            $('#submission-files-container').html(this.templateFiles({ submissionfiles: files, wstoken: SC.getToken(), editmode: !parseInt(this.model.get("status")) }));

            Utils.setupPopups($('#submission-files-container').get(0));
        },

        submitFile: function () {
            _Uploader.owner(this).restrictFileType(null).exec(null,null,function (filedata) {
                this.model.get("files").add({ name: filedata.filename, filename: filedata.origname, assignid: this.model.get('assignid') });
                this.thingsChanged();
            });
        },

        removeFile: function (ev) {
            var elem = $(ev.currentTarget);
            var files = this.model.get("files");
            files.get($(elem).data('cid')).set("remove", true);
            $(elem).parent().remove();
            this.thingsChanged();
        },

        filteredText: function () {
            var txt = this.ckeditor.getData();

            return txt;
        },

        thingsChanged: function () {
            this.$("[data-call=\"saveEdit\"]").prop("disabled", false);
            _Router.setConfirmLeave(str.task_cancel, _.bind(this.saveEdit, this), null);
        },

        model: SubmissionModel
    });
});
