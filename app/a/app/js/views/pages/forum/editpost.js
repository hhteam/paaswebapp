/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/forum/editpost.html", "i18n!nls/strings", "app/tools/utils", "app/tools/inlineedit", "app/views/dialogs/messagebox"],
    function (tpl, str, Utils, InlineEdit, MessageBox)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: "#forum-content",
        currentReply: null,

        events: {
            "click [data-call]": Utils.callMapper
        },

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();
        },

        render: function ()
        {
            var that = this;

            // Discussion message is actually first post message
            if (this.model.get("posts").length > 0)
            {
                this.model.set("message", this.model.get("posts").at(0).get("message"));
            }

            this.$el.html(this.template({ str: str, data: this.model.toJSON() }));

            require(["ckeditor"], function ()
            {
                // Intrusive "features" should not be enabled by default X(
                CKEDITOR.disableAutoInline = true;

                // Must use custom ckeditor.
                that.ckeditor = CKEDITOR.replace(that.$("#post-text-field").get(0),
                {
                    toolbarGroups: [
                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                        { name: 'clipboard', groups: [ /*'undo',*/ 'clipboard' ] },
                        { name: 'links' },
                        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] }
                    ],
                    language: _User.get("lang"),
                    contentsLangDirection: (CKEDITOR.config.language == "ar" ? "rtl" : "ui"),
                    removePlugins: 'elementspath,resize,magicline',
                    removeButtons: "Subscript,Superscript,Paste,Anchor,CreateDiv",
                    height: "150px",
                    contentsCss: RootDir + "app/css/basic.css",
                    mathJaxLib: 'https://c328740.ssl.cf1.rackcdn.com/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
                });

                that.inlineEdit.init(that);
                that.inlineEdit.start();
                that.updateAttachments();

                _Router.setConfirmLeave(str.forum_confirm_cancel_post_message, _.bind(that.saveComment, that));
            });

            return this;
        },

        addAttachment: function (ev, target)
        {
            _Uploader.owner(this).restrictFileType(null).exec(
                //add
                function (filedata) { },
                //error
                function (data) { },
                //done
                function (filedata)
                {
                    var att = this.model.get("new_attachments");

                    if (!att)
                    {
                        att = [ ];
                    }

                    att.push({ name: filedata.origname, id: 0 });
                    this.model.set("new_attachments", att);
                    this.updateAttachments();
                });
        },

        saveComment: function ()
        {
            var deferred = $.Deferred();

            if (this.inlineEdit.commit({ deferred: deferred }))
            {
                // Custom ckeditor...
                this.model.set({ message: this.ckeditor.getData() });

                this.model.save(undefined, { deferred: deferred, success: function ()
                {
                    _Router.setConfirmLeave(null, null, null);
                }});

                return true;
            }
            else
            {
                if (!this.inlineEdit.triggerValidation())
                {
                    (new MessageBox({ message: this.inlineEdit.errorString, backdrop: false })).render();
                }
            }

            return deferred;
        },

        updateAttachments: function ()
        {
            var txt = "", att = this.model.get("new_attachments");

            if (att instanceof Array)
            {
                for (var i=0; i<att.length; i++)
                {
                    txt += " <span class=\"block\" style=\"padding: 8px 10px; margin: 0px 5px 10px 5px; position: relative;\"><img src=\"" + RootDir + "/app/img/icon-download.png\"> "
                        + att[i].name
                        + " <span data-id=\"" + att[i].id + "\" data-idx=\"" + i + "\" data-call=\"removeAttachment\" class=\"attachment-remove-button small-remove-button\"></span>"
                        + "</span>";
                }
            }

            this.$("#attachment-space").html(txt);
        },

        cancelComment: function (ev, target)
        {
            _Router.setConfirmLeave(null, null, null);

            _Router.navigate("/courses/" + this.options.courseCode + "/discussions/" + this.model.get("forum") + "/temp", { trigger: false, replace: true });
            _Router.navigate("/courses/" + this.options.courseCode + "/discussions/" + this.model.get("forum"), { trigger: true });
        },

        removeAttachment: function (ev, target)
        {
            var id = parseInt($(target).attr("data-id")),
                idx = parseInt($(target).attr("data-idx")),
                att = this.model.get("new_attachments");

            if (att instanceof Array)
            {
                att.splice(idx, 1);

                this.model.set("new_attachments", att);
            }

            if (id)
            {
                var datt = this.model.get("deleted_attachments");

                if (!datt) {
                    datt = [ ];
                }

                datt.push(id);
                this.model.set("deleted_attachments", datt);
            }

            this.updateAttachments();
        }
    });
});
