/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/forum/posts.html", "i18n!nls/strings", "app/tools/dataloader", "app/tools/utils",
        "app/collections/forum/discussions", "app/views/pages/forum/reply", "app/views/dialogs/delete_forum",
        "app/views/pages/forum/editpost", "app/models/forum/post", "app/tools/linkhandler", "moment"],
    function (tpl, str, DataLoader, Utils, DiscussionCollection, ReplyView, DeleteForumDialog, EditPostView, PostModel,
            LinkHandler, moment)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: "#forum-content",
        currentReply: null,

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            var isTeacher = this.options.course.get("can_edit");

            DataLoader.exec({ context: this, collection: this.options.course.get("forums") }, function (forums)
            {
                DataLoader.exec({ context: this, collection: DiscussionCollection, id: this.options.id }, function (disc)
                {
                    this.currentDiscussion = disc;

                    DataLoader.exec({ context: this, collection: disc.get("posts") }, function (posts)
                    {
                        this.currentPosts = posts;
                        this.firstPost = null;

                        var tar = [];

                        var makePostTree = function (par, level)
                        {
                            posts.each(function (post)
                            {
                                var p = post.toJSON();

                                if (p["parent"] == par)
                                {
                                    p.level = level > 1 ? level - 1 : 0;
                                    p.modified = moment.unix(p.modified).fromNow();

                                    p.can_edit = (_User.get("id") == p.userid);
                                    p.can_delete = isTeacher;

                                    for (var j=0; j<p.attachments.length; j++)
                                    {
                                        p.attachments[j].icon = Utils.mimeIcon(p.attachments[j].mimetype);
                                    }

                                    tar.push(p);

                                    if (!this.firstPost)
                                    {
                                        this.firstPost = p.id;
                                    }

                                    post.set({ is_new: 0 });

                                    makePostTree.call(this, p.id, level + 1)
                                }
                            }, this);
                        };

                        makePostTree.call(this, 0, 0);

                        this.$el.html(this.template({ str: str, posts: tar, discussion: disc.toJSON(), course: this.options.course.toJSON() }));

                        LinkHandler.setupView(this);
                        Utils.setupPopups(this.$el);

                        var postModel = new PostModel({ discussion: this.options.id, "parent": this.firstPost });

                        postModel.once("sync", function ()
                        {
                            this.options.course.get("forums").reset();
                            this.currentPosts.reset();
                            this.options.parentView.render();
                        }, this);

                        if (this.currentReply)
                        {
                            this.currentReply.hide();
                        }

                        if (!this.options.course.get("cantwrite")) {
                            this.currentReply = new ReplyView({ el: "#reply-box", parentPost: this.firstPost, model: postModel, cancel: _.bind(this.render, this) });
                            this.currentReply.render();
                        }

                        $("#forum-disc-unread-badge-" + this.options.id).fadeOut(2000);
                        disc.set({ numunread: 0 });
                        DiscussionCollection.set([ disc ], { remove: false });
                    });
                });
            });

            return this;
        },

        replyToPost: function (ev, target)
        {
            var postid = $(target).attr("data-postid");

            if (this.currentReply)
            {
                if (this.currentReply.options.parentPost == postid)
                {
                    this.currentReply.focus();
                    return;
                }
                else
                {
                    this.currentReply.hide();
                }
            }

            var postModel = new PostModel({ discussion: this.options.id, "parent": postid });

            postModel.once("sync", function ()
            {
                this.options.course.get("forums").reset();
                this.currentPosts.reset();
                this.options.parentView.render();
            }, this);

            this.currentReply = new ReplyView({ el: this.$("#reply-box" + (postid != this.firstPost ? "-" + postid : "")),
                    parentPost: postid, model: postModel, cancel: _.bind(this.render, this) });

            this.currentReply.render(true);
        },

        editPost: function (ev, target)
        {
            var postid = $(target).attr("data-postid");

            if (this.currentReply)
            {
                this.currentReply.hide();
                this.currentReply = null;
            }

            var postModel = this.currentPosts.get(postid);

            postModel.once("sync", function ()
            {
                this.options.course.get("forums").reset();
                this.currentPosts.reset();
                this.options.parentView.render();
            }, this);

            this.currentReply = new ReplyView({ el: this.$("#post-message-" + postid), model: postModel, cancel: _.bind(this.render, this) });

            this.currentReply.render(true);
        },

        removePost: function (ev, target)
        {
            var that = this;
            var postid = $(target).attr("data-postid");

            (new DeleteForumDialog({ message: str.forum_remove_post, context: this,
                accepted: function ()
                {
                  that.currentPosts.get(postid).destroy({ success: function ()
                  {
                    that.options.course.get("forums").reset();
                    that.currentPosts.reset();
                    that.options.parentView.render();
                  }});
                },
                rejected: function ()
                {
                    // nothing
                }})).render();
        },

        editDiscussion: function (ev, target)
        {
            this.currentDiscussion.set("new_attachments", this.currentDiscussion.get("posts").at(0).get("new_attachments"));

            var edit = new EditPostView({ model: this.currentDiscussion, courseCode: this.options.course.get("code") });

            this.currentDiscussion.once("sync", function ()
            {
                this.options.course.get("forums").reset();
                this.currentPosts.reset();
                this.options.parentView.render();
            }, this);

            edit.render();
        },

        removeDiscussion: function (ev, target)
        {
            var that = this;

            (new DeleteForumDialog({ message: str.forum_remove_discussion, context: this,
                accepted: function ()
                {
                  that.currentDiscussion.destroy({ success: function ()
                  {
                    that.options.course.get("forums").reset();

                    _Router.navigate("/courses/" + that.options.course.get("code") + "/discussions/" + that.options.forumId, { trigger: true });
                  }});
                },
                rejected: function ()
                {
                    // nothing
                }})).render();
        }
    });
});
