/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/forum/reply.html", "i18n!nls/strings", "app/tools/dataloader", "app/tools/utils", "backbone"],
    function (tpl, str, DataLoader, Utils)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function (autofocus)
        {
            var that = this;

            this.$el.html(this.template({ str: str, data: this.model.toJSON() }));

            require(["ckeditor"], function ()
            {
                CKEDITOR.disableAutoInline = true;

                try
                {
                    that.ckeditor = CKEDITOR.replace($("#reply-post-text-field").get(0),
                    {
                        toolbarGroups: [
                            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                            { name: 'clipboard', groups: [ /*'undo',*/ 'clipboard' ] },
                            { name: 'links' },
                            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] }
                        ],
                        language: _User.get("lang"),
                        contentsLangDirection: (CKEDITOR.config.language == "ar" ? "rtl" : "ui"),
                        removePlugins: 'elementspath,resize,magicline',
                        removeButtons: "Subscript,Superscript,Paste,Anchor,CreateDiv",
                        height: "150px",
                        contentsCss: RootDir + "app/css/basic.css",
                        mathJaxLib: 'https://c328740.ssl.cf1.rackcdn.com/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
                    });
                }
                catch (err)
                {
                    that.ckeditor = CKEDITOR.instances["reply-post-text-field"];
                }

                that.ckeditor.setData(that.model.get("message"));

                if (autofocus)
                {
                    that.focus();
                }

                that.ckeditor.on("change", function ()
                {
                    if (that.ckeditor.getData())
                    {
                        that.$(".btn-primary").removeClass("disabled").removeAttr("disabled");
                        _Router.setConfirmLeave(str.forum_confirm_cancel_post_message, _.bind(that.saveComment, that));
                    }
                    else
                    {
                        that.$(".btn-primary").addClass("disabled").attr("disabled", "disabled");
                        _Router.setConfirmLeave(null, null, null);
                    }
                });

            });

            this.updateAttachments();

            return this;
        },

        hide: function ()
        {
            this.$el.html("");
            this.undelegateEvents();
        },

        focus: function ()
        {
            try
            {
                this.ckeditor.focus();
                $('html,body').animate({ scrollTop: this.$el.offset().top - 50}, 'fast');
            }
            catch (err)
            { }
        },

        addAttachment: function (ev, target)
        {
            _Uploader.owner(this).restrictFileType(null).exec(
                //add 
                function (filedata) { },
                //error 
                function (data) { },
                //done
                function (filedata)
                {
                    var att = this.model.get("new_attachments");

                    if (!att)
                    {
                        att = [ ];
                    }

                    att.push({ name: filedata.origname, id: 0 });
                    this.model.set("new_attachments", att);
                    this.updateAttachments();
                    this.$(".btn-primary").removeClass("disabled").removeAttr("disabled");
                    _Router.setConfirmLeave(str.forum_confirm_cancel_post_message, _.bind(this.saveComment, this));
                });
        },

        saveComment: function (ev, target)
        {
            var deferred = $.Deferred();

            this.model.save({ message: this.ckeditor.getData() }, { deferred: deferred, success: function ()
            {
                _Router.setConfirmLeave(null, null, null);
            }});

            return deferred;
        },

        updateAttachments: function ()
        {
            var txt = "", att = this.model.get("new_attachments");

            if (att instanceof Array)
            {
                for (var i=0; i<att.length; i++)
                {
                    txt += " <span class=\"block\" style=\"padding: 8px 10px; margin: 0px 5px 10px 5px; position: relative;\"><img src=\"" + RootDir + "/app/img/icon-download.png\"> "
                        + att[i].name
                        + " <span data-id=\"" + att[i].id + "\" data-idx=\"" + i + "\" data-call=\"removeAttachment\" class=\"attachment-remove-button small-remove-button\"></span>"
                        + "</span>";
                }
            }

            this.$("#attachment-space").html(txt);
        },

        cancelComment: function (ev, target)
        {
            _Router.setConfirmLeave(null, null, null);

            var that = this;

            setTimeout(function () { that.options.cancel(); }, 100);
        },

        removeAttachment: function (ev, target)
        {
            var id = parseInt($(target).attr("data-id")),
                idx = parseInt($(target).attr("data-idx")),
                att = this.model.get("new_attachments");

            if (att instanceof Array)
            {
                att.splice(idx, 1);

                this.model.set("new_attachments", att);
            }

            if (id)
            {
                var datt = this.model.get("deleted_attachments");

                if (!datt) {
                    datt = [ ];
                }

                datt.push(id);
                this.model.set("deleted_attachments", datt);

                this.$(".btn-primary").removeClass("disabled").removeAttr("disabled");
                _Router.setConfirmLeave(str.forum_confirm_cancel_post_message, _.bind(this.saveComment, this));
            }

            this.updateAttachments();
        }
    });
});
