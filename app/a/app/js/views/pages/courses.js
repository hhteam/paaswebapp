/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/courses.html", "i18n!nls/strings", "app/tools/linkhandler", "app/views/pageview",
        "app/tools/dataloader", "app/tools/utils", "app/views/pages/dashboard/sidebar", "app/collections/admin/cohorts"],
	function (tpl, str, LinkHandler, PageView, DataLoader, Utils, SidebarView, CohortCollection)
{
	return new (PageView.extend(
	{
		template: _.template(tpl),

        mSearchString: "",

        pageTitle: function()
        {
            return str.site_name + ": " + str.courses;
        },

        events: {
            "click [data-call]": Utils.callMapper,
            "change #catalog-search-input": "searchUpdate",
            "keypress #catalog-search-input": "searchKeyPress"
        },

		pageRender: function ()
		{
            DataLoader.exec({ context: this, collection: this.collection }, function (courses)
            {
                var courseList = [], teachingCount = 0, learningCount = 0, totalComleted = 0, totalOngoing = 0;
                var allCourses = _User.get("enrolledcourses"), i;

                for (i=0; i<allCourses.length; i++)
                {
                    switch (allCourses[i].userrole)
                    {
                        case "teacher": case "editingteacher": case "coursecreator":
                            if (this.collection.isActive == allCourses[i].coursestatus)
                            {
                                teachingCount++;
                            }
                            break;

                        case "student":
                            if (this.collection.isActive == allCourses[i].coursestatus)
                            {
                                learningCount++;
                            }
                            break;

                        case "":
                            if (_User.get("cohortusertype") != 1 && _User.get("cohortusertype") != 2) {
                                teachingCount++;
                                break;
                            }

                        default:
                            continue;
                    }

                    if (allCourses[i].coursestatus == 1)
                    {
                        totalOngoing++;
                    }
                    else
                    {
                        totalComleted++;
                    }
                }

                if (this.mSearchString)
                {
                    var s = this.mSearchString.toLowerCase();

                    courses.each(function (c)
                    {
                        if (c.get("fullname").toLowerCase().indexOf(s) != -1)
                        {
                            courseList.push(c.toJSON());
                        }
                    }, this);
                }
                else
                {
                    courseList = courses.toJSON();
                }

                var cohortid = parseInt(_User.get("adminCohort"));

                if (!cohortid) {
                    cohortid = _User.get("cohorts");

                    if (typeof cohortid == "string" && cohortid.indexOf(",") != -1)
                    {
                        cohortid = cohortid.split(",")[0];
                    }

                    cohortid = parseInt(cohortid);
                }

                if (!cohortid) {
                    this.$el.html(this.template({ str: str, courses: courseList, noCoursesAtAll: courses.length > 0 ? 0 : 1,
                        isTeacher: this.collection.isTeacher, isActive: this.collection.isActive, searchString: this.mSearchString,
                        teachingCount: teachingCount, learningCount: learningCount, totalOngoing: totalOngoing, totalComleted: totalComleted, logo: null }));

                    LinkHandler.setupView(this);

                    SidebarView.setElement(this.$("#dashboard-sidebar"));
                    SidebarView.render();
                } else {
                    DataLoader.exec({ collection: CohortCollection, id: cohortid, context: this }, function (cohort)
                    {
                        this.$el.html(this.template({ str: str, courses: courseList, noCoursesAtAll: courses.length > 0 ? 0 : 1,
                            isTeacher: this.collection.isTeacher, isActive: this.collection.isActive, searchString: this.mSearchString,
                            teachingCount: teachingCount, learningCount: learningCount, totalOngoing: totalOngoing, totalComleted: totalComleted,
                            logo: cohort.get("cohorticonurl") }));

                        LinkHandler.setupView(this);

                        SidebarView.setElement(this.$("#dashboard-sidebar"));
                        SidebarView.render();
                    });
                }
            });
		},

        openCourse: function (ev, target)
        {
            _Router.navigate("/courses/" + target.getAttribute("data-code"), { trigger: true });
        },

        createCourse: function ()
        {
            _Router.navigate("/courses/wizard", { trigger: true });
        },

        reuseCourse: function ()
        {
            _Router.navigate("/courses/reuse", { trigger: true });
        },

        searchUpdate: function (ev)
        {
            this.mSearchString = $("#catalog-search-input").val();
            this.render();
        },

        searchKeyPress: function (ev)
        {
            switch (ev.keyCode)
            {
                case 13:
                    this.mSearchString = $("#catalog-search-input").val();
                    this.render();
                    break;

                case 27:
                    this.mSearchString = "";
                    this.render();
                    break;
            }
        },

        clearSearch: function ()
        {
            this.mSearchString = "";
            this.render();
        }
	}));
});
