/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/course.html", "i18n!nls/strings", "app/collections/course/courses", "app/tools/linkhandler",
        "app/views/pageview", "app/models/headercontrols", "app/views/warnings", "app/views/pages/course/overview",
        "app/views/pages/course/participants", "app/views/pages/course/progress", "app/views/pages/course/teacher_tasks",
        "app/collections/task/returned", "app/views/pages/course/student_tasks", "app/views/dialogs/yes_no",
        "app/views/dialogs/messagebox", "app/collections/course/categories", "app/views/dialogs/change_logo", "app/views/header/wizard",
        "app/views/header/header", "app/views/header/wizard2", "app/tools/dataloader", "app/collections/admin/cohorts",
        "app/views/pages/course/settings", "app/tools/inlineedit", "app/tools/utils", "app/views/dialogs/share_course",
        "app/collections/sharereminders", "app/tools/tracker", "app/views/pages/course/forums", "app/views/pages/course/forumtopics",
        "app/collections/course/coursesshort", "app/tools/social_utils", "app/views/pages/course/liveevent", "app/tools/servicecalls",
        "app/views/dialogs/error_message", "app/collections/course/coursesfree", "app/views/pages/course/certificate",
        "app/views/dialogs/share_certificate", "app/views/pages/student_certificate", "datepicker", "progressbar", "moment"],

    function (tpl, str, CourseCollection, LinkHandler, PageView, HeaderControls, WarningsView, CourseOverviewView, CourseParticipantsView,
             CourseProgressTabView, CourseTeacherTasksTabView, ReturnedCollection, CourseStudentTasksTabView, YesNoDialog,
             MessageBox, CategoryCollection, ChangeLogoDialog, HeaderView1, HeaderView2, HeaderView3, DataLoader, CohortCollection,
             CourseSettingsTabView, InlineEdit, Utils, ShareCourseDialog, ShareReminderCollection, Tracker, ForumsView, ForumTopicsView,
             CourseShortCollection, SocialUtils, LiveEventView, SC, ErrorDialog, FreeCourseCollection, CourseCertificateView,
             ShareCertificateDialog, StudentCertificatePage)
    {

    return new (PageView.extend(
    {
        code : null,
        model : null,
        uploadedBackground : null,
        uploadedBackgroundtn : null,
        changedBackground : null,
        changedLogo: null,
        mOrigSkills: [],
        template : _.template(tpl),

        pageTitle : function() {
            return str.site_name
                    + (this.model ? ": " + this.model.get("fullname") : "");
        },

        customHeader : function() {
            return (this.model && this.model.get("wizard_mode") == 1) ? HeaderView1 :
                (this.model && this.model.get("wizard_mode") == 2) ? HeaderView3 : HeaderView2;
        },

        initialize : function()
        {
            CourseProgressTabView.setCourseView(this);
            CourseTeacherTasksTabView.setCourseView(this);
            CourseStudentTasksTabView.setCourseView(this);

            this.inlineEdit = new InlineEdit();

            if (typeof FB != "undefined")
            {
                try
                {
                    FB.init({
                        appId : FBAppID,
                    });
                }
                catch (err)
                { }
            }

            return this;
        },

        startEdit: function()
        {
            this.mOrigSkills = this.model.get('skills').slice();
            if (this.subPage instanceof Object && this.subPage.startEdit instanceof Function)
            {
                this.subPage.startEdit();
            }

            // Change Edit caption (do we need this label?)
            //if (!this.model || this.model.get("wizard_mode") != 3) {
            //    $('#edit-caption').html(str.edit_course_caption);
            //}

            // Hide back button
            $('.back-button').css('visibility', 'hidden');

            // Hide the menu
            $('#course-menu').hide();
            $("#course-enable-cert-link").hide();

            _Router.setConfirmLeave(str.edit_cancel, _.bind(this.saveEdit, this), _.bind(this.leaveConfirm, this));
        },

        leaveConfirm: function() {
           var wmode = this.model.get("wizard_mode");
             if ((wmode > 2)  || this.model.get("from_import"))
             {
               this.model.set({wizard_mode: 0,from_import: false}, {silent: true});
               this.customHeader().render();
               this.render();
             }
        },

        saveEdit : function()
        {
            var thisThis = this,
                deferred = $.Deferred();

            $('#edit-caption').html('');

            if (this.subPage instanceof Object && this.subPage.saveEdit instanceof Function)
            {
                this.subPage.saveEdit({ deferred: deferred, success: function ()
                {
                    var wmode = thisThis.model.get("wizard_mode");
                    if (wmode)
                    {
                        thisThis.model.set({wizard_mode: 0}, {silent: true});

                        // XXX: where is it?
                        CourseShortCollection.LearningOngoing.reset();
                        CourseShortCollection.LearningCompleted.reset();
                        CourseShortCollection.TeachingOngoing.reset();
                        CourseShortCollection.TeachingCompleted.reset();

                        FreeCourseCollection.reset();
                    }

                    if((thisThis.subPage.bodyId =="page-course-view-weeks") && !wmode) {
                        if(thisThis.model.sectionsHaveChanged) {
                            thisThis.model.get("sections").reset();
                        };
                    }
                    thisThis.customHeader().render();
                    thisThis.render();
                }});
            }
            else
            {
                this.render();
            }

            return deferred;
        },

        cancelEdit: function()
        {
            (new YesNoDialog({
                message : this.model.get("wizard_mode") || this.model.get("from_import") ? str.edit_cancel2
                        : str.edit_cancel,
                context : this,
                button_primary : str.button_cancel_editing,
                button_secondary : str.button_save_changes,
                accepted : function()
                {
                    this.model.set({skills : this.mOrigSkills}, {silent : true});
                    _Router.setConfirmLeave(null, null,null);

                    this.changedLogo = null;

                    // Change Edit caption
                    $('#edit-caption').html('');

                    if (this.subPage instanceof Object
                            && this.subPage.cancelEdit instanceof Function) {
                        this.subPage.cancelEdit();
                    }

                    if (this.model.get("wizard_mode") === 2 || this.model.get("from_import") ||
                        ( (typeof this.model.get("cid") === 'undefined') && (this.model.get("wizard_mode") === 1) )) {
                        this.model.destroy({
                            success: function() {
                                _Router.navigate("/", {
                                    trigger : true
                                });
                            },
                            error: function() {
                                _Router.navigate("/", {
                                    trigger : true
                                });
                            }
                        });
                    } else if (this.model.get("wizard_mode") == 3) {
                        _Router.navigate("/teaching", { trigger: true });
                    } else {
                        if (this.model.get("wizard_mode")) {
                            this.model.set("wizard_mode", 0);
                        }
                        this.customHeader().render();
                        this.render();
                    }
                },
                rejected : function() {
                    this.saveEdit();
                }
            })).render();
        },

        events : {
            "click [data-call]": Utils.callMapper,

            "change select#listCategory" : function (ev)
            {
                this.selectCategory(true);
            },

            "click a#btn-change-course-logo" : "changeCourseLogo",
            "click .edit-course-background" : "changeCourseBackground",
            "keydown #input-course-fullname": "courseDetailsChanged"
        },

        changeCourseBackground: function (ev)
        {
            var thisThis = this;

            (new ChangeLogoDialog(
                { model: this.model,
                  moveUnder: $('.edit-course-background'),
                  context: 'coursebackground',
                  success: function (new_background) {
                      if (new_background == '/') {
                          $('.carousel-inner').show();
                          thisThis.selectCategory(true);
                          thisThis.uploadedBackground = null;
                      } else {
                          thisThis.uploadedBackground = new_background;
                      }
                      thisThis.courseDetailsChanged();
                  }
                })).render();

            ev.preventDefault();
        },

        changeCourseLogo: function (ev)
        {
            var thisThis = this;

            (new ChangeLogoDialog(
                { model: this.model,
                  moveUnder: $('.course-logo'),
                  context: 'courselogo',
                  success: function (new_logo) {
                      thisThis.changedLogo = new_logo;
                      thisThis.courseDetailsChanged();
                  }
                })).render();

            ev.preventDefault();
        },

        selectCategory: function (defaultBackground)
        {
            DataLoader.exec({ context: this, collection: CategoryCollection }, function (categories)
            {
                var html = "", activeIdx = -1, i, c, f,
                    catId = $('#listCategory').val(),
                    images = [ ];

                this.currentCategoryId = catId;

                if (catId && this.model.get("categoryid") != catId)
                {
                    this.model.set({ categoryid: catId }, { silent: true });
                    this.courseDetailsChanged();
                }

                // Update background images.
                categories.each(function (cat)
                {
                    _.each(cat.get("stock_backgrounds"), function (bgr)
                    {
                        if (activeIdx == -1)
                        {
                            if (cat.get("id") == catId && defaultBackground)
                            {
                                activeIdx = images.length;
                            }
                        }

                        images.push(RootDir + "app/img/stock/" + bgr.file);
                    }, this);
                }, this);

                if (images.length > 1 && $('.carousel-inner').css('display') != 'none')
                {
                    $('.carousel-control').show();
                }
                else
                {
                    $('.carousel-control').hide();
                }

                for (i=0, c=images.length; i<c; i++)
                {
                    f = images[i].substr(images[i].lastIndexOf("/") + 1);

                    html += "<div data-file=\"" + f + "\" data-url=\"" + images[i] + "\" class=\"item" + (activeIdx == i ? " active" : "") + "\" style=\"background: url('" + images[i] + "') no-repeat center;\"></div>";

                    if (activeIdx == i)
                    {
                        this.currentBackgroundFile = f;
                    }
                }

                $("#course-background-carousel").html(html);
            });
        },

        courseDetailsChanged: function ()
        {
            _Router.setConfirmLeave(str.edit_cancel, _.bind(this.saveCourseDetails, this), _.bind(this.reloadCourse, this));

            this.$("#course-settings-save-btn").removeClass("disabled");
        },

        saveCourseDetailsClicked: function ()
        {
            // Save and re-render
            this.saveCourseDetails().done(_.bind(this.render, this));
        },

        saveCourseDetails: function ()
        {
            var deferred = $.Deferred();

            if (!this.inlineEdit.commit({ deferred: deferred }))
            {
                if (!this.inlineEdit.triggerValidation())
                {
                    (new MessageBox({ message: this.inlineEdit.errorString, backdrop: false })).render();
                }

                HeaderControls.set("state", HeaderControls.get("oldstate"));

                deferred.rejectWith(this);

                return deferred;
            }

            // Declare what to wait
            // TODO: http://cdn.meme.am/instances/500x/59849038.jpg
            if (this.uploadedBackground !== null)
            {
                this.saveWaitForBackground = true
            } else
            {
                this.saveWaitForBackground = false;
            }
            if (this.changedLogo && this.changedLogo !== '/')
            {
                this.saveWaitForLogo = true
            } else
            {
                this.saveWaitForLogo = false;
            }
            if (this.uploadedBackgroundtn !== null)
            {
                this.saveWaitForBackgroundtn = true
            } else
            {
                this.saveWaitForBackgroundtn = false;
            }

            // Save background
            if (this.uploadedBackground == null) {
                if ($('.carousel-inner').is(":visible")) {
                    this.model.set({ coursebackground: $("#course-background .active").attr("data-url") }, { silent: true });
                }
                this.model.set({ categoryid: this.currentCategoryId }, { silent: true });
                // Check if we can save immediately
                this.saveCourse(deferred);
            }

            if(this.uploadedBackgroundtn) {
                $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_save_customized_course_background_tn.php",
                    data: {filename: this.uploadedBackgroundtn, course: this.model.get('id'), top: top},
                    context: this,
                    dataType: 'json',

                    success: function (data)
                    {
                        if(this.uploadedBackgroundtn != 'default') {
                          this.model.set({ coursebackgroundtn: data + "?v=" + (new Date()).getTime(),
                            }, { silent: true });
                        }

                        // Tell browser to reload this file (you saw it here first :-))
                        $.ajax({ url: data, method: "POST" });

                        this.uploadedBackgroundtn = null;
                        this.saveWaitForBackgroundtn = false;
                        this.saveCourse(deferred);
                    },

                    error: function (data)
                    {
                        console.log('Cannot save course TN background');
                        // Save course anyway
                        this.uploadedBackgroundtn = null;
                        this.saveWaitForBackgroundtn = false;
                        this.saveCourse(deferred);
                    }
                });
            }
            if(this.uploadedBackground) {
                var top = null;

                if ($('#uploaded-background').height() <= 170) {
                    top = 0;
                } else {
                    top = $('#uploaded-background').height() - 170 - parseInt($('#uploaded-background').css('top'), 10);
                }
                $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_save_customized_course_background.php",
                    data: {filename: this.uploadedBackground, course: this.model.get('id'), top: top},
                    context: this,
                    dataType: 'json',

                    success: function (data)
                    {
                        this.model.set({ coursebackground: data + "?v=" + (new Date()).getTime(),
                            categoryid: this.currentCategoryId }, { silent: true });
                        if(!this.model.get(('coursebackgroundtn'))) {
                          this.model.set({coursebackgroundtn : data.replace('.jpeg', '-tn.jpeg') + "?v=" + (new Date()).getTime() }, { silent: true });
                        }
                        this.saveWaitForBackground = false;
                        this.uploadedBackground = null;
                        this.saveCourse(deferred);
                    },

                    error: function (data)
                    {
                        console.log('Cannot save course background');
                        // Save course anyway
                        this.uploadedBackground = null;
                        this.saveWaitForBackground = false;
                        this.saveCourse(deferred);
                    }
                });
            }

            // Save course logo
            if (this.changedLogo == '/') {
                this.model.set('course_logo', '/');
                // Check if we can save immediately
                this.saveCourse(deferred);
            } else if (this.changedLogo) {
                $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_save_course_logo.php",
                         data: {filename: this.changedLogo, course: this.model.get('id')},
                         context: this,
                         dataType: 'json',
                    success: function (data)
                    {
                        this.model.set('course_logo', data + "?v=" + (new Date()).getTime());
                        this.saveWaitForLogo = false;
                        this.saveCourse(deferred);
                    },
                    error: function (data)
                    {
                        console.log('Cannot save course logo');
                        // Save course anyway
                        this.saveWaitForLogo = false;
                        this.saveCourse(deferred);
                    }
                });
            }
            this.changedLogo = null;
            if (this.model.get("wizard_mode") !== 1) {
                this.uploadedBackground = null;
            }

            return deferred;
        },

        saveCourse: function (deferred)
        {
            if (!deferred)
            {
                deferred = $.Deferred();
            }

            if (! this.saveWaitForLogo && ! this.saveWaitForBackground && ! this.saveWaitForBackgroundtn)
            {
                thisThis = this;

                if (this.model.get("wizard_mode") == 3)
                {
                    var courseData = {
                        'fullname': this.model.get("fullname"),
                        'category': this.model.get("category"),
                        'background': this.model.get("coursebackground"),
                        'course_logo': this.model.get("course_logo"),
                        'coursecode': this.model.get("code"),
                        'startdate': (parseInt(this.model.get("startdate")) / 1000) || 0,
                        'enddate': (parseInt(this.model.get("enddate")) / 1000) || 0,
                        'cert_enabled': this.model.get("cert_enabled") ? true : false,
                        "language": this.model.get("language"),
                        "keywords": this.model.get("keywords")
                    };

                    if (!courseData.startdate && courseData.enddate) {
                        // If no start date, end date should not be given
                        // (the back-end gets very confused)
                        courseData.enddate = 0;
                    }

                    SC.call("local_monorailservices_clone_course", { course : courseData}, function (data)
                    {
                        if ('coursecode' in data && data.coursecode.length > 1) {
                            _User.fetch();
                            this.reloadCourse();
                            _Router.navigate("/courses/" + data.coursecode, { trigger: true });
                        } else {
                            (new ErrorDialog()).render();
                        }
                    }, this);
                }
                else
                {
                    this.model.save(undefined, {
                        success : function()
                        {
                            if (thisThis.model)
                            {
                                thisThis.code = thisThis.model.get("code");
                                if (thisThis.model.get("wizard_mode") == 1)
                                {
                                    thisThis.model.set("wizard_mode", 2);
                                    thisThis.want_edit_mode = true;
                                    _Router.setConfirmLeave(null, null, null);
                                    _Router.navigate('courses/' + thisThis.code, { trigger: true, replace: true });
                                    _User.fetch();
                                    thisThis.customHeader().render();
                                }
                                else
                                {
                                    thisThis.reloadCourse();
                                }
                            }
                        },
                        deferred: deferred
                    });
                }
            } else {
                // ignore and wait
            }

            return deferred;
        },

        reloadCourse: function (rerender)
        {
            //CourseCollection.remove(this.model);
            CourseCollection.reset();

            // XXX: where is it?
            CourseShortCollection.LearningOngoing.reset();
            CourseShortCollection.LearningCompleted.reset();
            CourseShortCollection.TeachingOngoing.reset();
            CourseShortCollection.TeachingCompleted.reset();

            FreeCourseCollection.reset();
            CohortCollection.reset();

            this.model = null;
            _Router.setConfirmLeave(null, null, null);
        },

        setPageAttrs : function() {
            $(document.body).attr({
                id : this.subPage.bodyId,
                class : this.subPage.bodyClass
            });
        },

        getSubPage : function()
        {
            switch (this.currentsub)
            {
                case "overview":
                    return new CourseOverviewView({
                        model : this.model,
                        courseId : this.model.get("id"),
                        sectionIndex: 0,
                        parentView: this
                    });

                case "participants":
                    return new CourseParticipantsView({
                        model : this.model,
                        courseId : this.model.get("id"),
                        parentView: this
                    });

                case "progress":
                    CourseProgressTabView.setElement(this.$el.find(".course-content").get(0));
                    CourseProgressTabView.setParticipants(this.model.get("participants"));
                    return CourseProgressTabView;

                case "tasks":
                    if (this.model.get("can_edit")) {
                        CourseTeacherTasksTabView.setElement(this.$el.find(
                                ".course-content").get(0));
                        return CourseTeacherTasksTabView;
                    } else {
                        CourseStudentTasksTabView.setElement(this.$el.find(
                                ".course-content").get(0));
                        return CourseStudentTasksTabView;
                    }

                case "settings":
                    return new CourseSettingsTabView({ model: this.model, parentView: this });

                case "discussions":
                    if (parseInt(this.currentsubsub))
                    {
                        return new ForumTopicsView({ model: this.model, parentView: this,
                                id: this.currentsubsub, topicId: this.currentsubsubsub });
                    }
                    else
                    {
                        return new ForumsView({ model: this.model, parentView: this });
                    }

                case "liveevent":
                    return new LiveEventView({ model : this.model, parentView: this });

                case "certificate":
                    if (this.model.get("can_edit")) {
                        return new CourseCertificateView({ model: this.model, parentView: this });
                    } else {
                        return new StudentCertificatePage({ model: this.model, parentView: this });
                    }

                default:
                    var idx = parseInt(this.currentsub) || 0;

                    if (idx > 0)
                    {
                        return new CourseOverviewView({
                            model : this.model,
                            courseId : this.model.get("id"),
                            sectionIndex: idx,
                            parentView: this
                        });
                    }
            }

            return null;
        },

        pageRender: function()
        {
            moment.locale(_User.get("lang"));

            var doRender = function (model)
            {
                if (model.isNew() && !(model.get("wizard_mode") || model.get("from_import")))
                {
                    // User not enrolled!
                    window.location.href = EliademyUrl + "/" + this.code;
                    return;
                }

                if (!model.get("can_edit") && model.get("course_privacy") == 5)
                {
                    window.location.href = RootDir;
                    return;
                }

                this.model = model;

                DataLoader.exec({ context: this, collection: this.model.get("participants") }, function ()
                {
                    var data = this.model.toJSON();

                    // Collect teacher information
                    try
                    {
                        if (this.model.get("wizard_mode"))
                        {
                            data.teacher = {
                                url: "/profile",
                                fullname: _User.get("fullname"),
                                picurl: { s: _User.picurl("s") }
                            };
                        }
                        else
                        {
                            data.teacher = this.model.get("participants").get(this.model.get("mainteacher")).toJSON();
                        }
                    }
                    catch (err)
                    {
                        data.teacher = null;
                    }

                    DataLoader.exec({ context: this, collection: CategoryCollection }, function (categories)
                    {
                        if ($('#course-menu-right .bar').attr('aria-valuenow')) { // make sure we do not redraw
                            var progressbarRendered = $('#course-menu-right').html();
                        } else {
                            var progressbarRendered = false;
                        }

                        this.$el.html(this.template({
                            str : str,
                            course : data
                        }));

                        if (! this.model.get("can_edit") && !this.model.get("cert_hashurl")) {
                            // course progress
                            if (! progressbarRendered) {
                                $('#course-menu-right .bar').progressbar({ display_text: 'center' });
                            } else {
                                $('#course-menu-right').html(progressbarRendered);
                            }
                        }

                        var thisThis = this;

                        if (this.model.get("wizard_mode") !== 1) {
                            if (this.model.get("can_edit")) {
                                HeaderControls.set("state", 1);
                                HeaderControls.set("handler", this);
                            } else {
                                HeaderControls.set("state", 0);
                            }
                        }

                        LinkHandler.setupView(this);
                        // Show course logo
                        if (data.course_logo && data.course_logo != '/')
                        {
                            // Course logo set in model.
                            $('#course-logo-img').attr('src', data.course_logo);
                        }
                        else
                        {
                            if(!data.cohortid &&  (data.wizard_mode == 1)) {
                                //User may belong to cohort
                                 data.cohortid = _User.get("adminCohort");
                                 if (!data.cohortid)
                                 {
                                     data.cohortid = _User.get("cohorts");
                                     if (typeof cohortid == "string" && cohortid.indexOf(",") != -1)
                                     {
                                       data.cohortid = cohortid.split(",")[0];
                                     }
                                 }
                            }
                            if (data.cohortid)
                            {
                                // Cohort logo.
                                DataLoader.exec({ context: this, collection: CohortCollection, id: data.cohortid }, function (coh)
                                {
                                    $('#course-logo-img').attr('src', coh.get("cohorticonurl"));

                                    this.model.set({ course_logo: coh.get("cohorticonurl") }, { silent: true });
                                });
                            }
                            else
                            {
                                // Non-cohort logo.
                                $('#course-logo-img').attr('src', RootDir + "app/img/logo-course-title.png");
                            }
                        }

                        // Show background picture
                        var backgroundFile = "",
                            backgroundUrl = RootDir + "app/img/stock/other.jpg";

                        if (data.coursebackground)
                        {
                            backgroundFile = data.coursebackground.substr(data.coursebackground.lastIndexOf("/") + 1);
                        }

                        if (/^\d+$/.test(backgroundFile))
                        {
                            // Id from old data
                            _.each(categories.toJSON(), function (cat)
                            {
                                _.each(cat.stock_backgrounds, function (bgr)
                                {
                                    if (bgr.id == backgroundFile)
                                    {
                                        backgroundUrl = RootDir + "app/img/stock/" + bgr.file;
                                        return;
                                    }
                                });
                            });
                        }
                        else if (backgroundFile)
                        {
                            backgroundUrl = data.coursebackground;
                            if (this.changedBackground) {
                                backgroundUrl = backgroundUrl + '?update=' + new Date().getTime();
                            }
                        }

                        $("#course-background").css("background", "url('" + backgroundUrl + "') no-repeat 50% 50%");

                        // Hide Edit button if in Wizard mode
                        if (this.model.get("wizard_mode") == 1) {
                            $('#btn-group-edit').hide();

                            // Show carousel if in Wizard mode
                            $('.carousel-inner').show();
                            this.selectCategory(true);
                        }

                        $("#course-background").on("slid", function ()
                        {
                            thisThis.currentBackgroundFile = $("#course-background .active").attr("data-file");
                            var bgt = $('#course-thumbnail').attr('src');
                            var url = $("#course-background .active").attr("data-url");
                            if(bgt.indexOf("-tncustom.jpg") == -1 && url.indexOf("/app/img/stock")) {
                              $('#course-thumbnail').attr('src', url.replace('.jpg', '-tn.jpg'));
                            }
                            thisThis.courseDetailsChanged();
                        });

                        // Fade out when changing
                        $('.course-button').on('click', function(ev) {
                            if (!$(ev.target).closest(".course-button").hasClass('selected')) {
                                $('#content-box').removeClass('in');
                            }
                        });
                        this.subPage = this.getSubPage();
                        this.setPageAttrs();
                        $('#course-button-' + (parseInt(this.currentsub) > 0 ? "overview" : this.currentsub)).addClass('active');
                        this.subPage.render();

                        $('#content-box').addClass('in');

                        // Set want_edit_mode to true before navigating to
                        // this page to get edit mode on.
                        if (this.want_edit_mode) {
                            delete this.want_edit_mode;

                            if (this.model.get("can_edit")) {
                                if (this.model.get("wizard_mode") === 1) {
                                    HeaderControls.set({ state: 6, handler: this });
                                } else {
                                    HeaderControls.set({ state: 2, handler: this });
                                }
                                this.startEdit();
                            }
                        }
                        else if ((this.model.get("course_privacy") == 1) && !this.model.get("can_edit"))
                        {
                            if (this.model.get("cert_enabled") && this.model.get("cert_hashurl"))
                            {
                                // Have certificate - remind to share it.

                                DataLoader.exec({ context: this, collection: ShareReminderCollection,
                                        where: { entryid: parseInt(this.model.get("id") || 0), entrytype: "certificate" } },
                                    function (reminder)
                                    {
                                        if (reminder.get("show"))
                                        {
                                            reminder.set("show", 0);

                                            (new ShareCertificateDialog({ reminder: reminder, course: this.model })).render();
                                        }
                                    });
                            }
                            else if (this.model.get("progress") == 100)
                            {
                                // Course completed - remind to share it.

                                DataLoader.exec({ context: this, collection: ShareReminderCollection,
                                        where: { entryid: parseInt(this.model.get("id") || 0), entrytype: "completion" } },
                                    function (reminder)
                                    {
                                        if (reminder.get("show"))
                                        {
                                            reminder.set("show", 0);

                                            (new ShareCourseDialog({
                                                courseid: this.model.get("id"),
                                                header: str.congratulations,
                                                message: str.share_completed_course_label.replace("%COURSENAME", this.model.get("fullname")),
                                                review: true,
                                                rate: true,
                                                share_message: str.share_completed_course_msg
                                                    .replace("%COURSENAME", this.model.get("fullname")),
                                                reminder: reminder })).render();
                                        }
                                    });
                            }
                            else
                            {
                                // Course ongoing - remind to share it.

                                DataLoader.exec({ context: this, collection: ShareReminderCollection,
                                        where: { entryid: parseInt(this.model.get("id") || 0), entrytype: "course" } },
                                    function (reminder)
                                    {
                                        if (reminder.get("show"))
                                        {
                                            reminder.set("show", 0);

                                            (new ShareCourseDialog({
                                                courseid: this.model.get("id"),
                                                header: str.share_course_title,
                                                message: str.rate_course_msg.replace("%COURSENAME", this.model.get("fullname")),
                                                review: false,
                                                rate: true,
                                                reminder: reminder })).render();
                                        }
                                    });
                            }
                        }
                    });
                });
            };

            if (this.model && (this.model.get("wizard_mode") || this.model.get("from_import")))
            {
                doRender.apply(this, [ this.model ]);
            }
            else
            {
                DataLoader.exec({ context: this, collection: CourseCollection, where: { code: this.code } }, doRender);
            }
        },

        addToProfile: function()
        {
            // pfAuthorityId is Eliademy page on LinkedIn eg http://www.linkedin.com/company/5016192

            Tracker.track("s-linkedin-certificate", { courseid: this.model.get("id")});
            if (!this.model.get("can_edit") && this.model.get("cert_hashurl"))
            {
                var certUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".html";

                // Commented out old deprecated API
                //window.open("https://www.linkedin.com/profile/guided?startTask=CERTIFICATION_NAME" +
                window.open("https://www.linkedin.com/profile/add?_ed=0_7nTFLiuDkkQkdELSpruCwKiOYJGHNsAonQIWT1kHI6TXWMlneN2rrjbx8g5GDDYZaSgvthvZk7wTBMS3S-m0L6A6mLjErM6PJiwMkk6nYZylU7__75hCVwJdOTZCAkdv" +
                    //"&force=true" +
                    "&pfCertificationName=" + encodeURIComponent(this.model.get("fullname")) +
                    "&pfCertificationUrl=" + encodeURIComponent(certUrl) +
                    "&pfLicenseNo=" + encodeURIComponent(this.model.get("cert_hashurl")) +
                    "&pfCertStartDate=" + encodeURIComponent(moment(this.model.get("cert_issuetime"), "X").format("YYYYMM"))
                    // + "&pfAuthorityId=5016192"
                    );

            }
            else
            {
                window.open("https://www.linkedin.com/profile/add?_ed=0_7nTFLiuDkkQkdELSpruCwKiOYJGHNsAonQIWT1kHI6TXWMlneN2rrjbx8g5GDDYZaSgvthvZk7wTBMS3S-m0L6A6mLjErM6PJiwMkk6nYZylU7__75hCVwJdOTZCAkdv" +
                    //"&force=true&" +
                    "&pfCertificationName=" + encodeURIComponent(this.model.get("fullname")) +
                    "&pfCertificationUrl=" + encodeURIComponent(this.model.courseUrl()) 
                    // + "&pfLicenseNo=" + encodeURIComponent(this.model.get("cert_hashurl"))
                    // + "&pfCertStartDate=" + encodeURIComponent(moment(this.model.get("cert_issuetime"), "X").format("YYYYMM"))
                    // + "&pfAuthorityId=5016192"
                    );
            }
        },

        shareCertificateFacebook: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".html",
                certImgUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".jpg";

            Tracker.track("s-facebook-certificate", { courseid: this.model.get("id")});
            window.open("https://www.facebook.com/dialog/feed?app_id=" + FBAppID + "&display=page" +
                "&name=" + encodeURIComponent(this.model.get("fullname")) +
                "&description=" + encodeURIComponent(str.certificate_share_text.replace("%COURSENAME", this.model.get("fullname"))) +
                "&link=" + encodeURIComponent(certUrl) +
                "&picture=" + encodeURIComponent(certImgUrl) +
                "&redirect_uri=" + encodeURIComponent("https://facebook.com"));
        },

        shareCertificateTwitter: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".html";
            Tracker.track("s-twitter-certificate", { courseid: this.model.get("id")});
            window.open("https://twitter.com/intent/tweet?url=" + encodeURIComponent(certUrl) +
                "&text=" + encodeURIComponent(str.certificate_share_text.replace("%COURSENAME", this.model.get("fullname"))));
        },

        shareCertificateLinkedin: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".html";

            Tracker.track("s-linkedin-certificate", { courseid: this.model.get("id")});
            window.open("https://www.linkedin.com/profile/add?_ed=0_7nTFLiuDkkQkdELSpruCwKiOYJGHNsAonQIWT1kHI6TXWMlneN2rrjbx8g5GDDYZaSgvthvZk7wTBMS3S-m0L6A6mLjErM6PJiwMkk6nYZylU7__75hCVwJdOTZCAkdv" +
                //"&force=true" +
                "&pfCertificationName=" + encodeURIComponent(this.model.get("fullname")) +
                "&pfCertificationUrl=" + encodeURIComponent(certUrl) +
                "&pfLicenseNo=" + encodeURIComponent(this.model.get("cert_hashurl")) +
                "&pfCertStartDate=" + encodeURIComponent(moment(this.model.get("cert_issuetime"), "X").format("YYYYMM"))
                // + "&pfAuthorityId=5016192"
                );
        },

        hidePage: function ()
        {
            $('#edit-caption').html('');

            if (this.subPage && this.subPage.hidePage instanceof Function)
            {
                this.subPage.hidePage();
            }
        },

        rateCourse: function ()
        {
            if (!this.model.get("can_edit"))
            {
                (new ShareCourseDialog({
                    courseid: this.model.get("id"),
                    header: str.review,
                    message: str.rate_course_msg.replace("%COURSENAME", this.model.get("fullname")),
                    review: true,
                    rate: true })).render();
            }
        },

        shareCourse: function (ev, target)
        {
            var url = CatalogUrl + "/catalog/product/view/sku/" + this.model.get("code"),
                message = (this.model.get("can_edit") ? str.share_course_teacher_msg : str.share_course_student_msg)
                                .replace("%COURSENAME", this.model.get("fullname")),
                img = EliademyUrl + "/extimg/" + this.model.get("code") + "_course_background.jpg",
                service = target.getAttribute("data-service");

           Tracker.track("s-" + service + "-course", { courseid: this.model.get("id")});

           SocialUtils.sharePopup(service, url, this.model.get("fullname"), message, img);
        },

        openCertificate: function ()
        {
            _Router.navigate("/courses/" + this.model.get("code") + "/certificate", { trigger: true });
        }
    }));
});
