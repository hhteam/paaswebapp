/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license All rights reserved
 */

define(["text!templates/pages/coursereuse.html", "i18n!nls/strings", "app/models/headercontrols", "app/views/pages/course",
        "app/tools/dataloader", "app/collections/course/coursesshort", "app/tools/utils", "app/tools/linkhandler"],

function (tpl, str, HeaderControls, CoursePage, DataLoader, CourseShortCollection, Utils, LinkHandler)
{
   //var ITEMS_PER_PAGE = 24;
   var ITEMS_PER_PAGE = 12;
   return new (Backbone.View.extend(
   {
         template: _.template(tpl),

         el: "#page-content",
         mPage: 1,
        pageTitle: str.reuse_course,

         events: {
            "click [data-call]": Utils.callMapper,
         },

         setPage: function (ev, target)
         {
           this.mPage = parseInt(target.getAttribute("data-page")) || 0;
           this.render();
         },

         cancelEdit: function () {
           _Router.navigate('/teaching', { trigger: true });
         },

        courseSelected: function (ev, target)
        {
            _Router.navigate("/reuse-course/" + target.getAttribute("data-code"), { trigger: true });
        },

        render: function ()
        {
            DataLoader.exec({ collection: CourseShortCollection.TeachingOngoing, context: this }, function (collection1)
            {
                DataLoader.exec({ collection: CourseShortCollection.TeachingCompleted, context: this }, function (collection2)
                {
                    var clist = [ ];

                    collection1.each(function (c)
                    {
                        clist.push(_.extend({ completed: false }, c.toJSON()));
                    }, this);

                    collection2.each(function (c)
                    {
                        clist.push(_.extend({ completed: true }, c.toJSON()));
                    }, this);

                    this.$el.html(this.template({
                        items: clist.slice((this.mPage - 1) * ITEMS_PER_PAGE, Math.min(this.mPage * ITEMS_PER_PAGE, clist.length)),
                        page: this.mPage,
                        pageCount: Math.ceil(clist.length / ITEMS_PER_PAGE),
                        str: str
                    }));

                    LinkHandler.setupView(this);

                    HeaderControls.set("state", 7);
                    HeaderControls.set("handler", this);
                });
            });
        }
  }));
});
