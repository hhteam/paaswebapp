/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "text!templates/pages/settings/settings.html", "text!templates/pages/settings/settings_base.html",
        "app/views/pageview", "app/tools/servicecalls", "app/tools/utils", "app/tools/linkhandler", "app/views/pages/settings/settings_social",
        "app/collections/settings/social_accounts", "app/views/dialogs/timezone"],

    function (str, tpl, baseTpl, PageView, SC, Utils, LinkHandler, SocialSettingsView, SocialAccountsCollection, TimeZoneDialog)
{
    return new (PageView.extend(
    {
        template: _.template(tpl),
        baseTemplate: _.template(baseTpl),

        menuHighlight: null,

        events: {
            "click [data-call]": Utils.callMapper
        },

        saveEdit: function ()
        {
            $('#password-success').hide();
            var notificationsettings = [];
            this.$("input:radio:checked.radio_opt").each(function () {
                notificationsettings.push({ type: $(this).attr('name'), value: $(this).val() });
            });
            if($('#platform_updates').prop('checked')) {
                notificationsettings.push({ type: 'platform_updates', value: 1 });
            } else {
                notificationsettings.push({ type: 'platform_updates', value: 0 });
            }
            _User.set('notificationsettings', notificationsettings);
            _User.set({ lang:$('#languageselect').val() });
            //_User.set('timezone', $('#timezone-input').val());
            _User.set('maildisplay', parseInt($('#emailselect').val()));
            _User.once("updated", function ()
            {
                if (!localStorage.getItem('locale') || (localStorage.getItem('locale') != _User.get('lang')))
                {
                    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_save_user_lang.php", data: {language: $('#languageselect').val()}, context: this,
                            success: function ()
                            {
                                localStorage.setItem('locale', _User.get('lang'));
                                location.reload(true);
                            },
                            error: function ()
                            {
                                // Does this work?
                                this.render();
                            }});
                }
                else
                {
                    location.reload(true);
                }
            });
            _User.save();

            SocialAccountsCollection.each(function (account)
            {
                account.save({
                    share_enroll: this.$("input[name='" + account.get("type") + "_share_enroll']:checked").val() || 0,
                    share_complete: this.$("input[name='" + account.get("type") + "_share_complete']:checked").val() || 0,
                    share_certificate: this.$("input[name='" + account.get("type") + "_share_certificate']:checked").val() || 0});
            }, this);
        },

        changePassword: function()
        {
          $('#password-success').hide();
          $('#password-error').hide();
          if($('#passwordpanel').is(':visible')) {
               $('#passwordpanel').hide();
               $('#changepassword').show(); 
          } else {
               $('#password1').val('') 
               $('#password2').val('') 
               $('#origpassword').val('') 
               $('#passwordpanel').show();
               $('#changepassword').hide();
          }
        },

        updatePassword: function()
        {
           $('#password-error').hide();
           var ulogin = localStorage.getItem('user_login');
           if(((typeof ulogin !== undefined) && (ulogin == 'login_form'))  && !$('#origpassword').val().trim()) { 
                $('#password-error').text(str.orig_password_missing);
                $('#password-error').show();
           } 
           else if(!$('#password1').val() || !$('#password2').val()) {
               $('#password-error').text(str.password_empty);
               $('#password-error').show();
           } 
           else if($('#password1').val().trim() != $('#password2').val().trim()) {
             $('#password-error').text(str.password_mismatch);
             $('#password-error').show();
           }
           else if(Utils.validatePassword($('#password1').val())) { 
               $('#password-error').text(str.password_condfail);
               $('#password-error').show();
           }else {
                var thisThis = this;
                $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_change_password.php", data: {p: $('#origpassword').val(), np: $('#password1').val()}, context: this, dataType: "json",
                  success: function (data)
                  {
                    if(data.success) {
                      $('#password-success').text(str.password_change_success);
                      $('#password-success').show();
                      setTimeout(thisThis.changePassword, 1500);
                    } else {
                      $('#password-error').text(str.generic_passwd_error);
                      $('#password-error').show();
                    }
                  },
                  error: function ()
                  {
                    $('#password-error').text(str.generic_passwd_error);
                    $('#password-error').show();
                  }});
           }
        },

        pageRender: function ()
        {
            var ulogin = localStorage.getItem('user_login');
            var email_login = false;
            if((typeof ulogin !== undefined) && (ulogin == 'login_form')) {
                email_login = true;
            }
            if (_User.get("sessionkey")) { // we have user data to render
                this.$el.html(this.baseTemplate({ str: str, page: 1 }));
                this.$("#settings-page-content").html(this.template({ data: _User.toJSON(), str: str, email_login:email_login }));

                (new SocialSettingsView()).render();

                LinkHandler.setupView(this);

                /*
                $('#timezone-input').typeahead({
                    name: 'timezone',
                    prefetch: MoodleDir + "webservice/rest/server.php?wstoken="+SC.getToken()+"&wsfunction=local_monorailservices_get_timezones&moodlewsrestformat=json",
                    limit: 10
                });
                */
            }
            else
            {
                // Data not here yet. Try once again when it's here (or
                // use dataloader...).
                _User.once("updated", this.render, this);
            }
        },

        editTimezone: function ()
        {
            (new TimeZoneDialog({ timezone: _User.get("timezone"), model: _User, finished: function ()
            {
                $('#timezone-input').val(_User.get('timezone'));
            }})).render();
        }
    }));
});
