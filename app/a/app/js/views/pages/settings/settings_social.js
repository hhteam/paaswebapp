/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/settings/settings_social.html", "i18n!nls/strings", "app/tools/utils", "app/tools/social_utils",
        "app/collections/settings/social_accounts", "app/tools/dataloader", "backbone"],
        function (tpl, str, Utils, SocialUtils, SocialAccountsCollection, DataLoader)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: "#settins-social-buttons",

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: SocialAccountsCollection }, function (accounts)
            {
                var social = { };

                accounts.each(function (item)
                {
                    social[item.get("type")] = item.toJSON();
                }, this);

                this.$el.html(this.template({ social: social, str: str }));
            });
        },

        addFacebookAccount: function ()
        {
            SocialUtils.addFacebookAccount(_.bind(this.render, this));
        },

        removeFacebookAccount: function ()
        {
            SocialUtils.removeFacebookAccount(_.bind(this.render, this));
        },

        addGoogleAccount: function ()
        {
            SocialUtils.addGoogleAccount(_.bind(this.render, this));
        },

        removeGoogleAccount: function ()
        {
            SocialUtils.removeGoogleAccount(_.bind(this.render, this));
        },

        addTwitterAccount: function ()
        {
            SocialUtils.addTwitterAccount(_.bind(this.render, this));
        },

        removeTwitterAccount: function ()
        {
            SocialUtils.removeTwitterAccount(_.bind(this.render, this));
        },

        addLinkedinAccount: function ()
        {
            SocialUtils.addLinkedinAccount(_.bind(this.render, this));
        },

        removeLinkedinAccount: function ()
        {
            SocialUtils.removeLinkedinAccount(_.bind(this.render, this));
        },

        addLiveAccount: function ()
        {
            SocialUtils.addLiveAccount(_.bind(this.render, this));
        },

        removeLiveAccount: function()
        {
            SocialUtils.removeLiveAccount(_.bind(this.render, this));
        }
    });
});
