/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/settings/profile_social.html", "i18n!nls/strings", "app/tools/utils", "app/tools/social_utils",
        "app/tools/dataloader", "app/collections/settings/social_accounts", "backbone"],
    function (tpl, str, Utils, SocialUtils, DataLoader, SocialAccountsCollection)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: "#profile-social-buttons",

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: SocialAccountsCollection }, function (accounts)
            {
                var data = this.model.toJSON();
                var social = { };

                accounts.each(function (item)
                {
                    social[item.get("type")] = item.toJSON();
                }, this);

                this.$el.html(this.template({ data: data, social: social, str: str }));
            });
        },

        addFacebookAccount: function ()
        {
            SocialUtils.addFacebookAccount(_.bind(this.render, this));
        },

        addGoogleAccount: function ()
        {
            SocialUtils.addGoogleAccount(_.bind(this.render, this));
        },

        addTwitterAccount: function ()
        {
            SocialUtils.addTwitterAccount(_.bind(this.render, this));
        },

        addLinkedinAccount: function ()
        {
            SocialUtils.addLinkedinAccount(_.bind(this.render, this));
        },

        addLiveAccount: function ()
        {
            SocialUtils.addLiveAccount(_.bind(this.render, this));
        }
    });
});
