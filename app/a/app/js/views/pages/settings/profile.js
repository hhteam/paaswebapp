/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "text!templates/pages/settings/profile.html", "text!templates/pages/settings/profile_edit.html",
        "text!templates/pages/settings/settings_base.html", "text!templates/pages/settings/profile_analytics.html", "app/views/pageview",
        "app/tools/inlineedit", "app/tools/linkhandler", "app/collections/countries", "app/tools/dataloader", "app/tools/utils",
        "app/collections/admin/user_analytics", "app/collections/admin/cohorts", "app/views/pages/settings/profile_social",
        "app/collections/settings/social_accounts", "app/tools/social_utils", "app/collections/settings/participant_social_accounts"],

    function (str, tpl, editTpl, baseTpl, analyticsTpl, PageView, InlineEdit, LinkHandler, CountryCollection, DataLoader, Utils,
              UserAnalyticsCollection, CohortCollection, SocialEditView, SocialAccountsCollection, SocialUtils, ParticipantSocialAccountCollection)
{
    return new (PageView.extend(
    {
        template: _.template(tpl),
        editTemplate: _.template(editTpl),
        baseTemplate: _.template(baseTpl),
        analyticsTemplate: _.template(analyticsTpl),
        editMode: false,
        model: null,
        mPicture: null,

        menuHighlight: null,

        events: {
            "click [data-call]": Utils.callMapper,

            "keyup *[data-field]": "fieldEdited",
            "change *[data-field]": "fieldEdited"
        },

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();
        },

        save: function ()
        {
            if (this.inlineEdit.commit())
            {
                var names = this.model.get("fullname").split(/\s+/);

                if (names.length > 1)
                {
                    this.model.set({ firstname: names.slice(0, names.length-1 ).join(" "),
                            lastname: names[names.length - 1] }, { silent: true });
                }
                else
                {
                    this.model.set({ firstname: names[0], lastname: ' ' }, { silent: true });
                }

                this.model.save();

                if (this.mPicture)
                {
                    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_save_user_avatar.php", data: {uploaded_file: this.mPicture}, context: this,
                        success: function ()
                        {
                            this.model.once("updated", function ()
                            {
                                window.history.go(-1);
                            });

                            this.model.fetch();
                        }
                    });
                }
                else
                {
                    window.history.go(-1);
                }
            }
        },

        fieldEdited: function ()
        {
            this.$("#profile-save-btn").removeAttr("disabled");
        },

        uploadPicture: function ()
        {
            var that = this, progress;

            _Uploader.owner({ }).restrictFileType('image/*').setFileUploadLimit(0.5 * 1024 * 1024).exec(
                function ()
                {
                    progress = $("<div>").attr({ id: "progress-bar" }).addClass("progress").append($("<div>").addClass("bar").css({width: "0%"}));
                    $("#user-profile-image").parent().append(progress);
                },
                null,
                function (filedata)
                {
                    progress.remove();
                    $("#user-profile-image").attr("src", MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + filedata.filename);                
                    $("#user-profile-image").height($("#user-profile-image").width());

                    that.mPicture = filedata.filename;
                    that.fieldEdited();
                });
        },

        facebookPicture: function ()
        {
            var win = window.open(
                "https://www.facebook.com/dialog/oauth/?client_id=" + this.model.get("fcid") + "&redirect_uri=" + window.location.protocol + "//"
                  + window.location.host + MoodleDir + "auth/googleoauth2/facebook.php&scope=email,user_birthday,user_location,publish_actions&state=p_"+new Date().getTime() + "&response_type=code",  null, "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");

            win.focus();
            SocialAccountsCollection.reset();
            this.mPicture = _User.get("id")+'socialprofilepicture';
            this.fieldEdited();
        },

        linkedinPicture: function ()
        {
            var win = window.open("https://www.linkedin.com/uas/oauth2/authorization?client_id="+ LINAppID + "&redirect_uri="  + window.location.protocol + "//" + window.location.host + MoodleDir + "auth/googleoauth2/linkedin.php&scope=r_emailaddress rw_nus&state=p_" + new Date().getTime() + "&response_type=code", null, "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");

            win.focus();
            SocialAccountsCollection.reset();
            this.mPicture = _User.get("id")+'socialprofilepicture';
            this.fieldEdited();
        },

        twitterPicture: function ()
        {
            var win = window.open(MoodleDir + "theme/monorail/twitter.php?p=true", null, "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");
            win.focus();
            SocialAccountsCollection.reset();
            this.mPicture = _User.get("id")+'socialprofilepicture';
            this.fieldEdited();
        },

        pageRender: function ()
        {
            moment.locale(_User.get("lang"));

            DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
            {
                if (this.editMode)
                {
                    this.$el.html(this.baseTemplate({ str: str, page: 0 }));
                    LinkHandler.setupView(this);
                    this.$("#settings-page-content").html(this.editTemplate({ data: this.model.toJSON(), countries: countries.toJSON(), str: str }));

                    (new SocialEditView({ model: this.model })).render();

                    this.inlineEdit.init(this);
                    this.inlineEdit.start();
                }
                else
                {
                    var country = countries.get(this.model.get("country"));

                    var doRenderView = function (accounts, owner)
                    {
                        var social = { }, data = this.model.toJSON(),
                            haveSocial = false, haveUnconnected = true, i;

                        for (i=0; i<accounts.length; i++)
                        {
                            social[accounts[i].type] = accounts[i];
                        }

                        if (social.facebook || social.linkedin || social.twitter || data.customfields_googleplus || data.customfields_vk ||
                            data.customfields_weibo || data.customfields_pinterest || data.customfields_instagram)
                        {
                            haveSocial = true;
                        }

                        if (social.facebook && social.linkedin && social.twitter && data.customfields_googleplus && data.customfields_vk &&
                            data.customfields_weibo && data.customfields_pinterest && data.customfields_instagram)
                        {
                            haveUnconnected = false;
                        }
                        
                        var courseProfilesToDisp = (function(){
                            var ret = [];
                            var teacher = {title:str.teaching,courses:[]};
                            var student = {title:str.learning,courses:[]};
                            if (!data.enrolledcourses instanceof Array) {
                                return ret;
                            }
                            var courses = _.sortBy(data.enrolledcourses,'coursestatus').reverse();
                            for (var i=0; i<courses.length; i++) {
                                if (courses[i].isteacher) {
                                    teacher.courses.push(courses[i]);
                                }
                                else {
                                    student.courses.push(courses[i]);
                                }
                            }
                            ret.push(teacher);
                            ret.push(student);
                            return ret;
                        })();
                        
                        var contentHtml = this.template({
                                data: data,
                                countryName: country ? country.get("value") : "",
                                canedit: true,
                                social: social,
                                haveSocial: haveSocial,
                                haveUnconnected: haveUnconnected,
                                courseProfilesToDisp: courseProfilesToDisp,
                                str: str });

                        if (owner)
                        {
                            this.$el.html(this.baseTemplate({ str: str, page: 0 }));
                            this.$("#settings-page-content").html(contentHtml);
                        }
                        else
                        {
                            this.$el.html(contentHtml);
                            var cohort = parseInt(_User.get("adminCohort"));

                            //if (cohort && this.model.get("cohortid") == cohort)
                            if (cohort)
                            {
                                DataLoader.exec({ context: this, collection: UserAnalyticsCollection, where: { userid: this.model.get("id") } }, function (stat)
                                {
                                    this.$("#profile-extras-placeholder").html(this.analyticsTemplate({ str: str, user: this.model.toJSON(), stats: stat.toJSON() }));

                                    LinkHandler.setupView(this);
                                });
                            }
                        }

                        //$('#skills-progress .label-success').popover();
                        LinkHandler.setupView(this);
                    };

                    if (this.model.get("id") == _User.get("id"))
                    {
                        // Viewing own profile
                        DataLoader.exec({ context: this, collection: SocialAccountsCollection }, function (accounts)
                        {
                            doRenderView.call(this, accounts.toJSON(), true);
                        });
                    }
                    else
                    {
                        // Viewing profile of someone else
                        DataLoader.exec({ context: this, collection: ParticipantSocialAccountCollection, where: { userid: this.model.get("id") } }, function (accounts)
                        {
                            doRenderView.call(this, accounts.get("accounts"), false);
                        });
                    }
                }

                LinkHandler.setupView(this);
            });
        },

        refreshSocialButtons: function ()
        {
            SocialAccountsCollection.reset();
            this.pageRender();
        },

        linkAccount: function (ev, target)
        {
            switch ($(target).attr("data-social"))
            {
                case "facebook":
                    SocialUtils.addFacebookAccount(_.bind(this.refreshSocialButtons, this));
                    break;

                case "linkedin":
                    SocialUtils.addLinkedinAccount(_.bind(this.refreshSocialButtons, this));
                    break;

                case "twitter":
                    SocialUtils.addTwitterAccount(_.bind(this.refreshSocialButtons, this));
                    break;
            }
        },

        addAccountLink: function (ev, target)
        {
            _Router.navigate("profile/edit", { trigger: true });
            setTimeout(function () { $("[data-field='customfields_" + $(target).attr("data-social") + "']" ).focus(); }, 500);
        },

        openCourse: function (ev, target)
        {
            _Router.navigate("/courses/" + target.getAttribute("data-code"), { trigger: true });
        }
    }));
});
