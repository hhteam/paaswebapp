/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/student_certificate.html", "text!templates/pages/course/student_cert_waiting.html", "i18n!nls/strings",
        "app/tools/linkhandler", "app/tools/dataloader", "app/tools/utils", "app/collections/countries", "app/collections/course/cert_orders",
        "app/models/course/cert_order", "app/tools/tracker", "app/views/dialogs/error_message", "moment"],

    function (tpl, tplWaiting, str, LinkHandler, DataLoader, Utils, CountryCollection, CertOrderCollection, CertOrderModel, Tracker,
        ErrorDialog)
{
    return Backbone.View.extend(
    {
        el: ".course-content",
        template: _.template(tpl),
        templateWaiting: _.template(tplWaiting),
        country_vat: { },
        currentOrder: null,
        orderQuantity: 1,
        reordering: false,

        events: {
            "click [data-call]": Utils.callMapper,
            "change #billing_details [name=\"country\"]": "updatePrice",
            "change #billing_details [name=\"vat_id\"]": "updatePrice",
            "submit #billing_details": "placeOrder"
        },

        render: function ()
        {
            if (this.model.get("cert_hashurl"))
            {
                // Certificate is issued
                _Router.navigate("courses/" + this.model.get("code") + "/certificate/" + this.model.get("cert_hashurl"), { trigger: false, replace: true });

                if (this.model.get("cert_enabled") == 2)
                {
                    // Can order printed version
                    DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
                    {
                        countries.each(function (c) {
                            this.country_vat[c.get("key")] = c.get("vat");
                        }, this);

                        DataLoader.exec({ collection: CertOrderCollection, context: this }, function (orders)
                        {
                            var addr = {
                                name: _User.get("fullname"),
                                country: _User.get("country")
                            };

                            if (orders.length > 0) {
                                try {
                                    addr = JSON.parse(orders.at(0).get("address"));
                                } catch (err)
                                { }
                            }

                            this.$el.html(this.template({
                                str: str,
                                course: this.model.toJSON(),
                                user: _User.toJSON(),
                                countries: countries.toJSON(),
                                orders: orders.toJSON(),
                                address: addr,
                                reordering: this.reordering
                            }));

                            LinkHandler.setupView(this);

                            if (!orders.length || this.reordering)
                            {
                                this.updatePrice();

                                $.ajax({ method: "GET", url: MoodleDir + "theme/monorail/ext/ajax_bt_token.php", dataType: "json", context: this }).done(function (token)
                                {
                                    var thisThis = this;

                                    BraintreeData.setup(token.merchant, 'payment_form', BraintreeData.environments.production);

                                    braintree.setup(token.token, 'dropin', {
                                        container: 'payment_form',
                                        paymentMethodNonceReceived: function (ev, nonce)
                                        {
                                            thisThis.currentOrder.save(undefined, { success: function ()
                                            {
                                                $.ajax({ method: "POST",
                                                         url: MoodleDir + "theme/monorail/ext/ajax_bt_cert.php",
                                                         data: {
                                                            nonce: nonce,
                                                            device_data: thisThis.$("#payment_form [name=\"device_data\"]").val(),
                                                            courseid: thisThis.model.get("id"),
                                                            amount: (thisThis.vat_amount + (_Config.cert_price * thisThis.orderQuantity)),
                                                            tax: thisThis.vat_amount,
                                                            quantity: thisThis.orderQuantity
                                                         },
                                                         context: thisThis,
                                                         dataType: "json" }).done(function (data)
                                                {
                                                    if (data.state == "success")
                                                    {
                                                        this.processOrderSuccess();
                                                    }
                                                    else
                                                    {
                                                        (new ErrorDialog()).render();
                                                    }
                                                });
                                            }});
                                        }
                                    });
                                });

                                this.reordering = false;
                            }
                        });
                    });
                }
                else
                {
                    // No printed certificates
                    this.$el.html(this.template({ str: str, course: this.model.toJSON(), user: _User.toJSON() }));
                    LinkHandler.setupView(this);
                }
            }
            else
            {
                // Certificate not issued
                this.$el.html(this.templateWaiting({ str: str }));
                LinkHandler.setupView(this);
            }
        },

        openCourse: function ()
        {
            _Router.navigate("/courses/" + this.model.get("code"), { trigger: true });
        },

        updatePrice: function ()
        {
            var country = this.$("#billing_details [name=\"country\"]").val(),
                vat_id = this.$("#billing_details [name=\"vat_id\"]").val(),
                vat = parseFloat(this.country_vat[country]) || 0;

            var doUpdatePrice = function (vat) {
                this.vat_amount = (_Config.cert_price * this.orderQuantity) * vat / 100;

                this.$("#order_vat").text(this.vat_amount.toFixed(2));
                this.$("#order_total").text(((_Config.cert_price * this.orderQuantity) + this.vat_amount).toFixed(2));
                this.$("#order_subtotal").text((_Config.cert_price * this.orderQuantity).toFixed(2));
                this.$("#cert-order-quantity").text(this.orderQuantity);
            };

            if (vat && vat_id && country != "FI") {
                $.ajax({ method: "GET", url: MoodleDir + "theme/monorail/ext/ajax_vatcheck.php", data: { country: country, vat: vat_id }, context: this, dataType: "json" }).done(function (data)
                {
                    if (data instanceof Object && data.valid) {
                        doUpdatePrice.call(this, 0);
                    } else {
                        doUpdatePrice.call(this, vat);
                    }
                }).fail(function ()
                {
                    doUpdatePrice.call(this, vat);
                });
            } else {
                doUpdatePrice.call(this, vat);
            }
        },

        completePurchase: function ()
        {
            $("#billing_details input[type=\"submit\"]").click();
        },

        placeOrder: function (ev)
        {
            ev.preventDefault();

            var data = { };

            this.$("#billing_details").find("[name]").each(function ()
            {
                data[$(this).attr("name")] = $(this).val();
            });

            this.currentOrder = new CertOrderModel({ courseid: this.model.get("id"), "status": 0, address: JSON.stringify(data) });

            $("#payment_method_form input[type=\"submit\"]").click();

            return false;
        },

        orderAgain: function ()
        {
            this.reordering = true;
            this.render();
        },

        processOrderSuccess: function ()
        {
            document.location.reload(true);
        },

        incCertCount: function ()
        {
            if (this.orderQuantity < 100)
            {
                this.orderQuantity++;
                this.updatePrice();
            }
        },

        decCertCount: function ()
        {
            if (this.orderQuantity > 1)
            {
                this.orderQuantity--;
                this.updatePrice();
            }
        },

        shareCertificateFacebook: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".html",
                certImgUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".jpg";

            Tracker.track("s-facebook-certificate", { courseid: this.model.get("id")});
            window.open("https://www.facebook.com/dialog/feed?app_id=" + FBAppID + "&display=page" +
                "&name=" + encodeURIComponent(this.model.get("fullname")) +
                "&description=" + encodeURIComponent(str.certificate_share_text.replace("%COURSENAME", this.model.get("fullname"))) +
                "&link=" + encodeURIComponent(certUrl) +
                "&picture=" + encodeURIComponent(certImgUrl) +
                "&redirect_uri=" + encodeURIComponent("https://facebook.com"));
        },

        shareCertificateTwitter: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".html";
            Tracker.track("s-twitter-certificate", { courseid: this.model.get("id")});
            window.open("https://twitter.com/intent/tweet?url=" + encodeURIComponent(certUrl) +
                "&text=" + encodeURIComponent(str.certificate_share_text.replace("%COURSENAME", this.model.get("fullname"))));
        },

        shareCertificateLinkedin: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.model.get("cert_hashurl") + ".html";

            Tracker.track("s-linkedin-certificate", { courseid: this.model.get("id")});
            
            //alert("https://www.linkedin.com/profile/guided?startTask=CERTIFICATION_NAME" +
            alert("https://www.linkedin.com/profile/add?_ed=0_7nTFLiuDkkQkdELSpruCwKiOYJGHNsAonQIWT1kHI6TXWMlneN2rrjbx8g5GDDYZaSgvthvZk7wTBMS3S-m0L6A6mLjErM6PJiwMkk6nYZylU7__75hCVwJdOTZCAkdv" +
                //"&force=true" +
                "&pfCertificationName=" + encodeURIComponent(this.model.get("fullname")) +
                "&pfCertificationUrl=" + encodeURIComponent(certUrl) +
                "&pfLicenseNo=" + encodeURIComponent(this.model.get("cert_hashurl")) +
                "&pfCertStartDate=" + encodeURIComponent(moment(this.model.get("cert_issuetime"), "X").format("YYYYMM"))
                // + "&pfAuthorityId=5016192"
                );
        }
    });
});
