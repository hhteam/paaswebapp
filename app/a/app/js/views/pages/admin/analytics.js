/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/analytics.html", "text!templates/pages/admin/analytics_engagement.html",
        "text!templates/pages/admin/analytics_timed_engagement.html", "text!templates/pages/admin/analytics_achievements.html",
        "text!templates/pages/admin/analytics_task_progress.html", "text!templates/pages/admin/analytics_enrollments.html",
        "text!templates/pages/admin/analytics_engagement_vert.html", "i18n!nls/strings", "app/views/pageview", "app/tools/dataloader",
        "app/collections/admin/cohorts", "app/tools/linkhandler", "app/models/admin/analytics", "app/collections/admin/activity_analytics",
        "app/collections/admin/enrollment_analytics", "app/tools/utils", "progressbar", "jqueryflot", "jqueryflotpie", "jqueryflottime",
        "jqueryflotcategories", "jqueryflottooltip", "jqueryflotaxislabels", "jqueryflotstack", "datepicker"],

    function (baseTpl, tpl, engagementTpl, timedEngagementTpl, achievementsTpl, taskProgressTpl, enrollmentsTpl, engagementVertTpl,
              str, PageView, DataLoader, CohortCollection, LinkHandler, AnalyticsModel, ActivityAnalyticsCollection, EnrollmentAnalyticsCollection,
              Utils)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        engagementTemplate: _.template(engagementTpl),
        timedEngagementTemplate: _.template(timedEngagementTpl),
        achievementsTemplate: _.template(achievementsTpl),
        taskProgressTemplate: _.template(taskProgressTpl),
        enrollmentsTemplate: _.template(enrollmentsTpl),
        engagementVertTemplate: _.template(engagementVertTpl),

        menuHighlight: "administration",

        subViews: {
            engagement: str.content_engagement,
            timedEngagement: str.timed_content_engagement,
            studentActivity: str.student_activity,
            submission: str.task_submission_progress,
            enrollment: str.enrollment_history,
            grade: str.grade_distribution
        },

        pageRender: function ()
        {
            var cohortid = _User.get("adminCohort");

            if (!cohortid)
            {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (cohort)
            {
                this.$el.html(this.baseTemplate({ str: str, page: 5, cohort: cohort.toJSON() }));

                DataLoader.exec({ context: this, model: AnalyticsModel }, function (stat)
                {
                    var courses = stat.get("courses"),
                        users = stat.get("users");

                    var data = {
                        course_count: courses.length,
                        user_count: users.length,
                        section_count: 0,
                        task_count: 0,
                        quiz_count: 0
                    };

                    var i, j, cc;

                    for (i=0; i<courses.length; i++)
                    {
                        data.section_count += courses[i].sections.length;

                        cc = courses[i].tasks.length;

                        for (j=0; j<cc; j++)
                        {
                            switch (courses[i].tasks[j].tasktype)
                            {
                                case "assign":
                                    data.task_count++;
                                    break;

                                case "quiz":
                                    data.quiz_count++;
                                    break;
                            }
                        }
                    }

                    this.$("#admin-page-base").html(this.template({
                        str: str,
                        data: data,
                        disabled_message: Utils.premiumDisabledMessage(cohort.get("company_status")),
                        views: this.subViews }));

                    LinkHandler.setupView(this);

                    var thisThis = this;

                    this.$("#view-type-selector").change(function (ev)
                    {
                        thisThis[$(this).val() + "Page"](stat);
                    });

                    for (i in this.subViews)
                    {
                        this[i + "Page"](stat);

                        break;
                    }
                });
            });
        },

        engagementPage: function (stat)
        {
            var courses = stat.get("courses"), sections;

            var data = { courses: [ ] }, visitData = [ ];

            var i, j, sum;

            for (i=0; i<courses.length; i++)
            {
                sum = 0;
                sections = courses[i].sections;

                for (j=0; j<sections.length; j++)
                {
                    sum += sections[j].visits;
                }

                data.courses.push(courses[i].fullname);
                visitData.push([ i, sum ]);
            }

            this.$("#analytics-content-view").html(this.engagementTemplate({ str: str, data: data }));

            var graph1 = this.$("#graph1");

            var plotAll = function ()
            {
                graph1.plot([ { data: visitData } ], {
                    grid: { hoverable: true },
                    bars: { show: true },
                    xaxis: { axisLabel: str.courses + " (" + str.hover_to_see_more + ")", ticks: false },
                    yaxis: { axisLabel: str.number_of_views, min: 0 },
                    tooltip: true,
                    tooltipOpts: { content: function (a, x, y) { return str.course + ": " + courses[x].fullname + "<br>" + str.number_of_views + ": " + y; } }
                });
            };

            this.$("#activity-course-selector").change(function ()
            {
                var idx = parseInt($(this).val()), visDat = [ ];

                if (idx)
                {
                    sections = courses[idx - 1].sections;

                    for (i=0; i<sections.length; i++)
                    {
                        visDat.push([ i, sections[i].visits ]);
                    }

                    graph1.plot([ { data: visDat } ], {
                        grid: { hoverable: true },
                        bars: { show: true },
                        xaxis: { axisLabel: str.topics + " (" + str.hover_to_see_more + ")", ticks: false },
                        yaxis: { axisLabel: str.number_of_views, min: 0 },
                        tooltip: true,
                        tooltipOpts: { content: function (a, x, y) { return str.topic + ": " + sections[x].name + "<br>" + str.number_of_views + ": " + y; } }
                    });
                }
                else
                {
                    plotAll();
                }
            });

            plotAll();
        },

        timedEngagementPage: function (stat)
        {
            var courses = stat.get("courses");

            var data = { courses: [ ] },
                courseIndices = { };

            var i, j, sections, secIdx;

            for (i=0; i<courses.length; i++)
            {
                data.courses.push(courses[i].fullname);

                sections = courses[i].sections;
                secIdx = { };

                for (j=0; j<sections.length; j++)
                {
                    secIdx[sections[j].index] = j;
                }

                courseIndices[courses[i].courseid] = { index: i, sections: secIdx };
            }

            var now = new Date();

            data.current_month = Utils.padLeft(now.getMonth() + 1, '0', 2) + "/" + now.getFullYear();

            this.$("#analytics-content-view").html(this.timedEngagementTemplate({ str: str, data: data }));

            var graph1 = this.$("#graph1");

            var updateData = function ()
            {
                var courseIdx = parseInt($("#activity-course-selector").val()),
                    month = parseInt($("#activity-period-selector").val().substr(0, 2)) - 1,
                    year = parseInt($("#activity-period-selector").val().substr(3)),
                    endMonth = month + 1, endYear = year;

                if (endMonth > 11)
                {
                    endMonth = 0;
                    endYear++;
                }

                if (!year || !endYear)
                {
                    return;
                }

                var startTime = (new Date(year, month, 1, 0, 0, 0, 0)).getTime(),
                    endTime = (new Date(endYear, endMonth, 1, 0, 0, 0, 0)).getTime();

                DataLoader.exec({ collection: ActivityAnalyticsCollection,
                    where: { period_start: Math.ceil(startTime / 1000),
                             period_end: Math.ceil(endTime / 1000) } }, function (st)
                {
                    var stCourses = st.get("courses"), stVisits, stSections, stId,
                        visitData = [ ], visDat, vd, i, j, keys;

                    if (!courseIdx)
                    {
                        for (i=0; i<stCourses.length; i++)
                        {
                            stVisits = stCourses[i].visits;
                            vd = { };
                            keys = [ ];

                            for (j=0; j<stVisits.length; j++)
                            {
                                if (typeof vd[stVisits[j].date_from] == "undefined")
                                {
                                    vd[stVisits[j].date_from] = stVisits[j].count;
                                }
                                else
                                {
                                    vd[stVisits[j].date_from] += stVisits[j].count;
                                }

                                keys.push(stVisits[j].date_from);
                            }

                            visDat = [ ];
                            keys.sort();

                            for (j=0; j<keys.length; j++)
                            {
                                visDat.push([ keys[j] * 1000, vd[keys[j]] ]);
                            }

                            if (visDat.length)
                            {
                                visitData.push({ label: courses[courseIndices[stCourses[i].courseid].index].fullname, data: visDat, stack: true });
                            }
                        }
                    }
                    else
                    {
                        stId = courses[courseIdx - 1].courseid;
                        stVisits = null;

                        for (i=0; i<stCourses.length; i++)
                        {
                            if (stCourses[i].courseid == stId)
                            {
                                stVisits = stCourses[i].visits;
                                break;
                            }
                        }

                        if (stVisits !== null)
                        {
                            stSections = courses[courseIdx - 1].sections;

                            for (i=0; i<stSections.length; i++)
                            {
                                vd = { };
                                keys = [ ];

                                for (j=0; j<stVisits.length; j++)
                                {
                                    if (stSections[i].index == stVisits[j].section)
                                    {
                                        vd[stVisits[j].date_from] = stVisits[j].count;

                                        keys.push(stVisits[j].date_from);
                                    }
                                }

                                if (keys.length > 0)
                                {
                                    keys.sort();
                                    visDat = [ ];

                                    for (j=0; j<keys.length; j++)
                                    {
                                        visDat.push([ keys[j] * 1000, vd[keys[j]] ]);
                                    }

                                    visitData.push({ label: stSections[i].name, data: visDat, stack: true });
                                }
                            }
                        }
                    }

                    graph1.plot(visitData, {
                        grid: { hoverable: true },
                        bars: { show: true, barWidth: 86400000 },
                        xaxis: { mode: "time", timeformat: "%d.%m.%y", min: startTime, max: endTime },
                        yaxis: { axisLabel: str.number_of_views, min: 0 },
                        tooltip: true,
                        tooltipOpts: { content: function (a, x, y) { return str.date + ": " + (new Date(x)).toDateString() + "<br>" + str.number_of_views + ": " + y; } }
                    });
                });
            };

            var picker = this.$("#activity-period-selector")
                .datepicker({ format: "mm/yyyy", viewMode: 1, minViewMode: 1, startDate: new Date(2014, 4, 1) })
                .on("changeDate", function (ev)
                {
                    picker.hide();
                    updateData();
                })
                .data("datepicker");

            this.$("#activity-course-selector").change(updateData);

            updateData();
        },

        studentActivityPage: function (stat)
        {
            var courses = stat.get("courses"),
                users = stat.get("users"),
                sections, sectionUsers;

            var data = { courses: [ ] },
                visitData = [ ],
                userTicks = [ ],
                userVisits = { };

            var i, j, k, sum;

            for (i=0; i<courses.length; i++)
            {
                data.courses.push(courses[i].fullname);
                sections = courses[i].sections;

                for (j=0; j<sections.length; j++)
                {
                    sectionUsers = sections[j].users;

                    for (k=0; k<sectionUsers.length; k++)
                    {
                        userVisits[sectionUsers[k].userid] = (parseInt(userVisits[sectionUsers[k].userid]) || 0) + sectionUsers[k].visits;
                    }
                }
            }

            for (i=0; i<users.length; i++)
            {
                userTicks.push([ i, users[i].fullname ]);
                visitData.push([ parseInt(userVisits[users[i].userid]) || 0, i ]);
            }

            this.$("#analytics-content-view").html(this.engagementVertTemplate({ str: str, data: data, height: users.length * 24 }));

            var graph1 = this.$("#graph1");

            var plotAll = function ()
            {
                graph1.plot([ { data: visitData } ], {
                    grid: { hoverable: true },
                    bars: { show: true, horizontal: true, barWidth: 0.7, align: "center" },
                    xaxis: { axisLabel: str.number_of_views + " (" + str.hover_to_see_more + ")", min: 0 },
                    yaxis: { axisLabel: str.students, ticks: userTicks },
                    tooltip: true,
                    tooltipOpts: { content: function (a, x, y) { return str.role_student + ": " + users[y].fullname + "<br>" + str.number_of_views + ": " + x; } }
                });
            };

            this.$("#activity-course-selector").change(function ()
            {
                var idx = parseInt($(this).val()), visDat = [ ], uv = { };

                if (idx)
                {
                    sections = courses[idx - 1].sections;

                    for (j=0; j<sections.length; j++)
                    {
                        sectionUsers = sections[j].users;

                        for (k=0; k<sectionUsers.length; k++)
                        {
                            uv[sectionUsers[k].userid] = (parseInt(uv[sectionUsers[k].userid]) || 0) + sectionUsers[k].visits;
                        }
                    }

                    for (i=0; i<users.length; i++)
                    {
                        visDat.push([ parseInt(uv[users[i].userid]) || 0, i ]);
                    }

                    graph1.plot([ { data: visDat } ], {
                        grid: { hoverable: true },
                        bars: { show: true, horizontal: true, barWidth: 0.7, align: "center" },
                        xaxis: { axisLabel: str.number_of_views + " (" + str.hover_to_see_more + ")", min: 0 },
                        yaxis: { axisLabel: str.students, ticks: userTicks },
                        tooltip: true,
                        tooltipOpts: { content: function (a, x, y) { return str.role_student + ": " + users[y].fullname + "<br>" + str.number_of_views + ": " + x; } }
                    });
                }
                else
                {
                    plotAll();
                }
            });

            plotAll();
        },

        submissionPage: function (stat)
        {
            var courses = stat.get("courses"), tasks, users,
                i, j, k,
                data = { courses: [] };

            for (i=0; i<courses.length; i++)
            {
                data.courses.push(courses[i].fullname);
            }

            this.$("#analytics-content-view").html(this.taskProgressTemplate({ str: str, data: data }));
            var graph1 = this.$("#graph1");

            var updatePage = function ()
            {
                var stIdx = parseInt($("#activity-course-selector").val()),
                    visitData = [ ], taskData = [ ], ymax, sub, total;

                if (stIdx)
                {
                    ymax = 0;
                    tasks = courses[stIdx - 1].tasks;

                    for (i=0; i<tasks.length; i++)
                    {
                        sub = 0;
                        total = 0;

                        users = tasks[i].users;

                        for (j=0; j<users.length; j++)
                        {
                            total++;

                            if (users[j].submitted)
                            {
                                sub++;
                            }
                        }

                        if (total > 0)
                        {
                            taskData.push(str.task + ": " + tasks[i].name + "<br>" + str.task_submissions + ": " + sub + "/" + total);
                            visitData.push([ visitData.length, sub ]);

                            if (total > ymax)
                            {
                                ymax = total;
                            }
                        }
                    }
                }
                else
                {
                    ymax = 100;

                    for (i=0; i<courses.length; i++)
                    {
                        tasks = courses[i].tasks;

                        sub = 0;
                        total = 0;

                        for (j=0; j<tasks.length; j++)
                        {
                            users = tasks[j].users;

                            for (k=0; k<users.length; k++)
                            {
                                total++;

                                if (users[k].submitted)
                                {
                                    sub++;
                                }
                            }
                        }

                        if (total > 0)
                        {
                            taskData.push(str.course + ": " + courses[i].fullname + "<br>" + str.task_submissions + ": " + Math.round((sub / total) * 100) + "%");
                            visitData.push([ visitData.length, Math.round((sub / total) * 100) ]);
                        }
                    }
                }

                graph1.plot([ { data: visitData } ], {
                    grid: { hoverable: true },
                    bars: { show: true },
                    xaxis: { ticks: false, axisLabel: (stIdx ? str.tasks : str.courses) + " (" + str.hover_to_see_more + ")" },
                    yaxis: { min: 0, max: ymax, axisLabel: stIdx ? str.number_of_submissions : str.average_submissions },
                    tooltip: true,
                    tooltipOpts: { content: function (a, x, y) { return taskData[x]; } }
                });
            };

            this.$("#activity-course-selector").change(updatePage);

            updatePage();
        },

        enrollmentPage: function (stat)
        {
            var now = new Date(), data = { };

            data.current_month = Utils.padLeft(now.getMonth() + 1, '0', 2) + "/" + now.getFullYear();

            this.$("#analytics-content-view").html(this.enrollmentsTemplate({ str: str, data: data }));

            var graph1 = this.$("#graph1");

            var updateData = function ()
            {
                var month = parseInt($("#activity-period-selector").val().substr(0, 2)) - 1,
                    year = parseInt($("#activity-period-selector").val().substr(3)),
                    endMonth = month + 1, endYear = year;

                if (endMonth > 11)
                {
                    endMonth = 0;
                    endYear++;
                }

                if (!year || !endYear)
                {
                    return;
                }

                var startTime = (new Date(year, month, 1, 0, 0, 0, 0)).getTime(),
                    endTime = (new Date(endYear, endMonth, 1, 0, 0, 0, 0)).getTime();

                DataLoader.exec({ collection: EnrollmentAnalyticsCollection,
                    where: { period_start: Math.ceil(startTime / 1000),
                             period_end: Math.ceil(endTime / 1000) } }, function (st)
                {
                    var stCourses = st.get("courses"), stEnrollments,
                        enrolData = [ ], enDat = { }, keys = [ ],
                        i, j;

                    for (i=0; i<stCourses.length; i++)
                    {
                        stEnrollments = stCourses[i].enrollments;

                        for (j=0; j<stEnrollments.length; j++)
                        {
                            if (typeof enDat[stEnrollments[j].date_from] == "undefined")
                            {
                                enDat[stEnrollments[j].date_from] = stEnrollments[j].count;
                            }
                            else
                            {
                                enDat[stEnrollments[j].date_from] += stEnrollments[j].count;
                            }

                            if (keys.indexOf(stEnrollments[j].date_from) == -1)
                            {
                                keys.push(stEnrollments[j].date_from);
                            }
                        }
                    }

                    keys.sort();

                    for (i=0; i<keys.length; i++)
                    {
                        enrolData.push([ keys[i] * 1000, enDat[keys[i]] ]);
                    }

                    graph1.plot([ { data: enrolData } ], {
                        grid: { hoverable: true },
                        bars: { show: true, barWidth: 86400000 },
                        xaxis: { mode: "time", timeformat: "%d.%m.%y", min: startTime, max: endTime },
                        yaxis: { axisLabel: str.number_of_enrollments, min: 0 },
                        tooltip: true,
                        tooltipOpts: { content: function (a, x, y) { return str.date + ": " + (new Date(x)).toDateString() + "<br>" + str.number_of_enrollments + ": " + y; } }
                    });
                });
            };

            var picker = this.$("#activity-period-selector")
                .datepicker({ format: "mm/yyyy", viewMode: 1, minViewMode: 1 })
                .on("changeDate", function (ev)
                {
                    picker.hide();
                    updateData();
                })
                .data("datepicker");

            updateData();
        },

        gradePage: function (stat)
        {
            var courses = stat.get("courses"), tasks, users,
                i, data = { courses: [] };

            for (i=0; i<courses.length; i++)
            {
                data.courses.push(courses[i].fullname);
            }

            this.$("#analytics-content-view").html(this.achievementsTemplate({ str: str, data: data }));
            var graph1 = this.$("#graph1");

            var updatePage = function ()
            {
                var stIdx = parseInt($("#activity-course-selector").val()),
                    allGrades = { 0: 0, 10: 0, 20: 0, 30: 0, 40: 0, 50: 0, 60: 0, 70: 0, 80: 0, 90: 0 },
                    gradeData = [ ],
                    i, j, k;

                if (stIdx)
                {
                    tasks = courses[stIdx - 1].tasks;

                    for (j=0; j<tasks.length; j++)
                    {
                        users = tasks[j].users;

                        for (k=0; k<users.length; k++)
                        {
                            grade = parseInt(users[k].gradepercent);

                            if (grade >= 100)
                            {
                                allGrades[90]++;
                            }
                            else if (grade != -1)
                            {
                                allGrades[Math.floor(grade / 10) * 10]++;
                            }
                        }
                    }
                }
                else
                {
                    for (i=0; i<courses.length; i++)
                    {
                        tasks = courses[i].tasks;

                        for (j=0; j<tasks.length; j++)
                        {
                            users = tasks[j].users;

                            for (k=0; k<users.length; k++)
                            {
                                grade = parseInt(users[k].gradepercent);

                                if (grade >= 100)
                                {
                                    allGrades[90]++;
                                }
                                else if (grade != -1)
                                {
                                    allGrades[Math.floor(grade / 10) * 10]++;
                                }
                            }
                        }
                    }
                }

                for (i in allGrades)
                {
                    gradeData.push([ i, allGrades[i] ]);
                }

                graph1.plot([ { data: gradeData } ], {
                    bars: { barWidth: 10, show: true, fill: true },
                    grid: { hoverable: true },
                    xaxis: { axisLabel: str.grade_range },
                    yaxis: { axisLabel: str.students, min: 0 },
                    tooltip: true,
                    tooltipOpts: { content: function (a, x, y) { return str.grade_range + ": " + x + "-" + (parseInt(x) + 10) + "<br>" + str.students + ": " + y; } }
                });
            };

            $("#activity-course-selector").change(updatePage);

            updatePage();
        }
    }));
});
