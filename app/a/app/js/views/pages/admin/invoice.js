/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/invoice.html", "i18n!nls/strings", "app/views/pageview",
        "app/tools/dataloader", "app/tools/linkhandler", "app/collections/admin/cohorts", "app/tools/utils", "app/views/pages/admin/invoices",
        "app/collections/countries", "moment"],
    function (baseTpl, tpl, str, PageView, DataLoader, LinkHandler, CohortCollection, Utils, InvoiceListView, CountryCollection)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        menuHighlight: "administration",

        events: {
            "click [data-call]": Utils.callMapper
        },

        pageRender: function ()
        {
            // TODO: multiple cohorts?
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (cohort)
            {
                DataLoader.exec({ context: this, collection: InvoiceListView.getInvoiceCollection(cohortid), id: this.id }, function (invoice)
                {
                    DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
                    {
                        var countr = { };

                        countries.each(function (c) {
                            countr[c.get("key")] = c.get("value");
                        });

                        var billing = { };

                        try {
                            billing = JSON.parse(cohort.get("billing_details"));
                        } catch (err) { }

                        var description = "";

                        switch (parseInt(invoice.get("invoicetype"))) {
                            case 1:
                                description = str.invoice_user_item_description
                                    .replace(/%PERIOD/, moment.unix(invoice.get("timepaid")).format("LL") + " - " + moment.unix(invoice.get("timepaid")).subtract(1, "month").format("LL"));
                                break;

                            case 2:
                                description = str.invoice_prepaid_item_description
                                    .replace(/%PERIOD/, moment.unix(invoice.get("timepaid")).format("LL") + " - " + moment.unix(invoice.get("timepaid")).add(1, "year").format("LL"))
                                    .replace(/%COUNT/, 500);
                                break;
                        }

                        this.$el.html(this.baseTemplate({ str: str, page: 8, cohort: cohort.toJSON() }));

                        this.$("#admin-page-base").html(this.template({
                            str: str,
                            invoice: invoice.toJSON(),
                            cohort: cohort.toJSON(),
                            billing_details: billing,
                            countries: countr,
                            description: description }));

                        LinkHandler.setupView(this);

                        if (this._print === true) {
                            delete this._print;
                            window.print();
                        }
                    });
                });
            });
        }
    }));
});
