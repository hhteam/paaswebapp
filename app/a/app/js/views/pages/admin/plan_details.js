/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/plan_details.html", "i18n!nls/strings",
        "app/views/pageview", "app/tools/dataloader", "app/tools/linkhandler", "app/collections/landingstrings",
        "app/collections/admin/cohorts", "app/views/dialogs/textinput", "app/tools/utils", "app/collections/countries",
        "moment"],
    function (baseTpl, tpl, str, PageView, DataLoader, LinkHandler, LandingStringsCollection, CohortCollection,
              InputDialog, Utils, CountryCollection)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        menuHighlight: "administration",
        mode: 1,
        paymentDetails: null,

        events: {
            "click [data-call]": Utils.callMapper
        },

        pageRender: function ()
        {
            // TODO: multiple cohorts?
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (model)
            {
                if (!model.get("isowner")) {
                    // Not an admin, go away.
                    _Router.navigate("", { trigger: true });
                    return;
                }

                DataLoader.exec({ collection: LandingStringsCollection, context: this, where: { lang: _User.get("lang") } }, function (lstr)
                {
                    DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
                    {
                        this.$el.html(this.baseTemplate({ str: str, page: 6, cohort: model.toJSON() }));

                        var billingDetails;

                        try {
                            billingDetails = JSON.parse(model.get("billing_details"));
                        } catch (err) {
                            billingDetails = { };
                        }

                        if (!billingDetails.company_name) {
                            billingDetails.company_name = model.get("name");
                        }

                        if (billingDetails.country) {
                            for (var i=0; i<countries.length; i++) {
                                if (countries.at(i).get("key") == billingDetails.country) {
                                    billingDetails.country = countries.at(i).get("value");
                                    break;
                                }
                            }
                        }

                        this.$("#admin-page-base").html(this.template({ str: str, lstr: lstr.get("str"),
                                    cohort: model.toJSON(), billingDetails: billingDetails }));

                        LinkHandler.setupView(this);

                        if (model.get("company_status") == 2) {
                            if (!this.paymentDetails) {
                                $.ajax({ method: "GET", url: MoodleDir + "theme/monorail/ext/ajax_bt_defmeth.php", dataType: "json", context: this }).done(function (data)
                                {
                                    if (data instanceof Object && data.name) {
                                        this.paymentDetails = data;
                                        $("#payment_method_info").html("<img style=\"height: 20px; width: auto; vertical-align: middle;\" src=\"" + this.paymentDetails.image + "\"> " + this.paymentDetails.name);
                                    }
                                    $("#payment_method_info").show();
                                });
                            } else {
                                $("#payment_method_info").html("<img style=\"height: 20px; width: auto; vertical-align: middle;\" src=\"" + this.paymentDetails.image + "\"> " + this.paymentDetails.name);
                                $("#payment_method_info").show();
                            }
                        }
                    });
                });
            });
        },

        // applyNgo: function ()
        // {
        //     var thisThis = this;

        //     (new InputDialog({ header: str.apply_for_ngo_title, label: str.apply_for_ngo_alertbox, placeholder: str.apply_for_ngo_label, noempty: true, button: str.button_apply_ngo, accepted: function ()
        //     {
        //         $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_ngo_request.php", method: "POST",
        //                data: { cohortid: _User.get("adminCohort"), message: this.value } }).done(function ()
        //                {
        //                     CohortCollection.reset();
        //                     thisThis.render();
        //                });
        //     }})).render();
        // }
    }));
});
