/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/invoices.html", "i18n!nls/strings",
        "app/views/pageview", "app/tools/dataloader", "app/tools/linkhandler", "app/collections/landingstrings",
        "app/collections/admin/cohorts", "app/tools/utils", "app/collections/admin/invoices",
        "moment"],
    function (baseTpl, tpl, str, PageView, DataLoader, LinkHandler, LandingStringsCollection, CohortCollection,
              Utils, InvoiceCollection)
{
    return new (PageView.extend(
    {
        invoiceCollections: { },
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        menuHighlight: "administration",

        events: {
            "click [data-call]": Utils.callMapper
        },

        pageRender: function ()
        {
            // TODO: multiple cohorts?
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (cohort)
            {
                if (!cohort.get("isowner")) {
                    // Not an admin, go away.
                    _Router.navigate("", { trigger: true });
                    return;
                }

                DataLoader.exec({ context: this, collection: this.getInvoiceCollection(cohortid) }, function (invoices)
                {
                    this.$el.html(this.baseTemplate({ str: str, page: 8, cohort: cohort.toJSON() }));
                    this.$("#admin-page-base").html(this.template({ str: str, invoices: invoices.toJSON() }));

                    LinkHandler.setupView(this);
                });
            });
        },

        printInvoice: function (ev, target)
        {
            
        },

        getInvoiceCollection: function (id)
        {
            var c = this.invoiceCollections[id];

            if (!c) {
                c = new InvoiceCollection();
                c.setCohort(id);

                this.invoiceCollections[id] = c;
            }

            return c;
        },

        /* call this when invoices change... */
        resetInvoiceCollections: function ()
        {
            this.invoiceCollections = { };
        }
    }));
});
