/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/public_page.html",
        "text!templates/pages/admin/public_page_ads.html",
        "i18n!nls/strings", "app/views/pageview", "app/tools/dataloader", "app/collections/admin/cohorts",
        "app/tools/linkhandler", "app/tools/utils", "app/tools/warnings", "app/views/dialogs/yes_no",
        "app/views/dialogs/admin_new_ad", "app/collections/countries"],
    function (baseTpl, tpl, adsTpl, str, PageView, DataLoader, CohortCollection, LinkHandler, Utils,
        WarningTool, YesNoDialog, AdminNewAdDialog, CountryCollection)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        adsTemplate: _.template(adsTpl),

        menuHighlight: "administration",

        events: {
            "click [data-call]": Utils.callMapper,
            "change [data-call][data-toggle=\"toggle\"]": Utils.callMapper,
            "change [data-course-all-cats]": "updateSelection",
            "change [data-group-ann]": "announcementChanged",
            "change [data-group-ad]": "advertisementChanged",
            "change [data-course-cat]": "updateCatSelection",
            "click [data-ad-checkbox]": "updateAdSelection"
        },

        pageRender: function ()
        {
            // TODO: multiple cohorts?
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            this.mCoursePub = { }, that = this;

            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (model)
            {
                this.model = model;
                var pub = [];

                try {
                    pub = JSON.parse(model.get("published_courses"));
                } catch (err) { }

                DataLoader.exec({ collection: model.get("courses"), context: this }, function (courses)
                {
                    DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
                    {
                        var data = model.toJSON(),
                            cList = [], ann_info = {}, cats = [];

                        try {
                            ann_info = JSON.parse(data.public_page_ann_info);
                        } catch (err) {
                            ann_info = {};
                        }

                        try {
                            this.ad_info = JSON.parse(data.public_page_ad_info);
                        } catch (err) {
                            this.ad_info = { ads: [] };
                        }

                        try {
                            cats = JSON.parse(data.public_page_cats);
                        } catch (err) {
                            cats = [];
                        }

                        courses.each(function (c)
                        {
                            var course = c.toJSON();

                            if (course.course_privacy == 1 && course.course_access != 1)
                            {
                                if (pub.indexOf(course.id) != -1) {
                                    course.org_published = 1;
                                    that.mCoursePub[parseInt(course.id)] = true;
                                }

                                cList.push(course);
                            }
                        });

                        this.$el.html(this.baseTemplate({ str: str, page: 9, cohort: data }));

                        this.$("#admin-page-base").html(this.template({
                            str: str,
                            cohort: data,
                            courses: cList,
                            ann_info: ann_info,
                            ad_info: this.ad_info,
                            cats: cats,
                            countries: countries.toJSON(),
                            disabled_message: Utils.premiumDisabledMessage(data.company_status)}));

                        if (data.public_page_ad) {
                            this.renderAdsTable();
                        }

                        LinkHandler.setupView(this);
                        this.$('[data-toggle="popover"]').popover({ trigger: "hover", placement: "top" });
                        Utils.setupPopups(this.$el);
                    });
                });
            });
        },

        togglePublished: function (ev, target) {
            var thisThis = this;

            this.model.save({ public_page: $(target).is(":checked") ? 1 : 0 }, { patch: true, success: function (model, method, options)
            {
                CohortCollection.reset();
                thisThis.render();
            }});
        },

        toggleAnnouncement: function (ev, target) {
            var thisThis = this;

            this.model.save({ public_page_ann: $(target).is(":checked") ? 1 : 0 }, { patch: true, success: function (model, method, options)
            {
                CohortCollection.reset();
                thisThis.render();
            }});
        },

        changeAnnCover: function () {
            this.uploadImage("ancov", function (file) {
                this.$("#public-ann-cover").attr("src", file + "?d=" + (new Date()).getTime());

                this.announcementChanged();
            });
        },

        fetchPostInfo: function () {
            $.ajax({
                "url": MoodleDir + "theme/monorail/ext/ajax_get_page_info.php",
                "data": { "url": this.$('[name="ann_info_url"]').val() },
                "context": this,
                "success": function (data) {
                    var obj = {};

                    try {
                        obj = JSON.parse(data);
                    } catch (err) {
                        obj = {};
                    }

                    if (obj["og:title"]) {
                         this.$('[name="ann_info_title"]').val(obj["og:title"]);
                    }

                    if (obj["og:description"]) {
                        this.$('[name="ann_info_description"]').val(obj["og:description"]);
                    }

                    if (obj["og:image"]) {
                        this.$("#public-ann-cover").attr("src", obj["og:image"]);
                    }
                }
            });
        },

        announcementChanged: function () {
            if (!this.$("#public-page-update-announcement").is(":visible")) {
                this.$("#public-page-update-announcement").slideDown();
            }
        },

        updateAnnouncement: function () {
            var data = {
                cover: this.$("#public-ann-cover").attr("src"),
                title: this.$('[name="ann_info_title"]').val(),
                description: this.$('[name="ann_info_description"]').val(),
                url: this.$('[name="ann_info_url"]').val()
            };

            this.model.save({ public_page_ann_info: JSON.stringify(data) }, { patch: true, success: function (model, method, options) {
                $("#public-page-update-announcement").slideUp();
            }});
        },

        toggleAdvertisements: function (ev, target) {
            var thisThis = this;

            this.model.save({ public_page_ad: $(target).is(":checked") ? 1 : 0 }, { patch: true, success: function (model, method, options)
            {
                CohortCollection.reset();
                thisThis.render();
            }});
        },

        newAd: function () {
            var thisThis = this;

            if (!(this.ad_info instanceof Object)) {
                this.ad_info = { ads: [] };
            }

            if (!(this.ad_info.ads instanceof Array)) {
                this.ad_info.ads = [];
            }

            (new AdminNewAdDialog({adnum: (this.ad_info.ads.length + 1) + "_" + (new Date()).getTime(), cohortid: this.model.get("id"), accepted: function () {
                thisThis.ad_info.ads.push(this.data);
                thisThis.renderAdsTable();
                thisThis.advertisementChanged();
            }})).render();
        },

        deleteAds: function () {
            var idxs = [];

            this.$("[data-ad-checkbox]").each(function () {
                if (this.checked) {
                    idxs.unshift(parseInt($(this).attr("data-idx")));
                }
            });

            if (idxs.length > 0 && this.ad_info instanceof Object && this.ad_info.ads instanceof Array) {
                for (var i=0; i<idxs.length; i++) {
                    this.ad_info.ads.splice(idxs[i], 1);
                }

                this.renderAdsTable();
                this.advertisementChanged();
            }
        },

        renderAdsTable: function () {
            DataLoader.exec({ context: this, collection: CountryCollection }, function (countries) {
                var ctr = {};

                countries.each(function (c) {
                    ctr[c.get("key")] = c.get("value");
                });

                var grp = {
                    1: str.org_public_page_banner1,
                    2: str.org_public_page_banner2,
                    3: str.org_public_page_banner3
                };

                this.$("#public-page-ad-list").html(this.adsTemplate({ str: str, ad_info: this.ad_info, countries: ctr, groups: grp }));
                this.updateAdSelection();
            });
        },

        updateAdSelection: function () {
            var empty = true;

            this.$("[data-ad-checkbox]").each(function () {
                if (this.checked) {
                    empty = false;
                }
            });

            if (empty) {
                this.$("[data-ad-actions]").attr("disabled", "disabled").addClass("disabled");
            } else {
                this.$("[data-ad-actions]").removeAttr("disabled").removeClass("disabled");
            }
        },

        changeAdGroup: function (ev, target) {
            if (this.ad_info instanceof Object && this.ad_info.ads instanceof Array) {
                var grp = $(target).attr("data-group"),
                    thisThis = this;

                this.$("[data-ad-checkbox]").each(function () {
                    if (this.checked) {
                        thisThis.ad_info.ads[parseInt($(this).attr("data-idx"))].group = grp;
                    }
                });

                this.renderAdsTable();
                this.advertisementChanged();
            }
        },

        changeAdCountry: function (ev, target) {
            if (this.ad_info instanceof Object && this.ad_info.ads instanceof Array) {
                var country = $(target).attr("data-country"),
                    thisThis = this;

                this.$("[data-ad-checkbox]").each(function () {
                    if (this.checked) {
                        thisThis.ad_info.ads[parseInt($(this).attr("data-idx"))].country = country;
                    }
                });

                this.renderAdsTable();
                this.advertisementChanged();
            }
        },

        advertisementChanged: function () {
            if (!(this.ad_info instanceof Object)) {
                this.ad_info = { ads: [] };
            }

            this.ad_info.title1 = this.$('[name="ad_info_title1"]').val();
            this.ad_info.title2 = this.$('[name="ad_info_title2"]').val();
            this.ad_info.title3 = this.$('[name="ad_info_title3"]').val();

            this.model.save({ public_page_ad_info: JSON.stringify(this.ad_info) }, { patch: true });
        },

        updateSelection: function ()
        {
            var that = this, canAdd = false, canRemove = false;

            this.$("[data-course-all-cats]").each(function () {
                if (this.checked) {
                    if (that.mCoursePub[parseInt(this.getAttribute("data-courseid"))]) {
                        canRemove = true;
                    } else {
                        canAdd = true;
                    }
                }
            });

            this.$("#public-page-add-courses-btn").prop("disabled", !canAdd);
            this.$("#public-page-remove-courses-btn").prop("disabled", !canRemove);
        },

        addCourses: function ()
        {
            var pub = [];

            try {
                pub = JSON.parse(this.model.get("published_courses"));
            } catch (err) { }

            this.$("[data-course-all-cats]").each(function () {
                if (this.checked) {
                    var id = parseInt(this.getAttribute("data-courseid"));

                    if (pub.indexOf(id) == -1) {
                        pub.push(id);
                    }
                }
            });

            var thisThis = this;

            this.model.save({ published_courses: JSON.stringify(pub) }, { patch: true, success: function (model, method, options)
            {
                CohortCollection.reset();
                thisThis.render();
            }});
        },

        removeCourses: function () {
            var pub = [];

            try {
                pub = JSON.parse(this.model.get("published_courses"));
            } catch (err) { }

            this.$("[data-course-all-cats]").each(function () {
                if (this.checked) {
                    var id = parseInt(this.getAttribute("data-courseid")),
                        idx = pub.indexOf(id);

                    if (idx != -1) {
                        pub.splice(idx, 1);
                    }
                }
            });

            var thisThis = this;

            this.model.save({ published_courses: JSON.stringify(pub) }, { patch: true, success: function (model, method, options) {
                CohortCollection.reset();
                thisThis.render();
            }});
        },

        uploadImage: function (prefix, callback) {
            var thisThis = this;

            _Uploader.owner(this).restrictFileType('image/*').setFileUploadLimit(0.5 * 1024 * 1024).exec(
                function (filedata)     //add
                { },
                function (data)         //error
                { },
                function (filedata)     //done
                {
                    var extension = filedata.filename.split('.').pop().toLowerCase();

                    if (['jpeg', 'jpg', 'png', 'gif'].indexOf(extension) == -1)
                    {
                        WarningTool.processWarnings({msgtype:'error', message:str.picture_error});
                        return;
                    }

                    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_set_public_file.php",
                            data: { filename: filedata.filename, prefix: prefix, cohortid: thisThis.model.get("id") },
                            success: function (data) {
                                if (callback instanceof Function) {
                                    callback.call(thisThis, data);
                                }
                            },
                            error: function (data) {
                                console.warn("Network error");
                            }
                    });
                });
        },

        addCategory: function () {
            this.$("#public-page-new-category-block").slideDown();
            this.$("#public-page-new-category-edit").focus();
        },

        createNewCategory: function () {
            var cats = [];

            try {
                cats = JSON.parse(this.model.get("public_page_cats"));
            } catch (err) {
                cats = [];
            }

            if (!(cats instanceof Array)) {
                cats = [];
            }

            cats.push({ title: this.$("#public-page-new-category-edit").val(), courses: [] });

            var thisThis = this;

            this.model.save({ public_page_cats: JSON.stringify(cats) }, { patch: true, success: function (model, method, options)
            {
                CohortCollection.reset();
                thisThis.render();
            }});
        },

        updateCategory: function (ev, target) {
            var idx = $(target).attr("data-idx");

            this.$("#public-page-cat-title-" + idx).addClass("editable inputfield").attr("contenteditable", "true").focus();
            this.$("#public-page-cat-edit-" + idx).hide();
            this.$("#public-page-cat-edit-save-" + idx).show();
        },

        updateSaveCategory: function (ev, target) {
            var idx = parseInt($(target).attr("data-idx"));

            var cats = [];

            try {
                cats = JSON.parse(this.model.get("public_page_cats"));
            } catch (err) {
                cats = [];
            }

            if (cats instanceof Array && cats.length > idx) {
                var thisThis = this;

                cats[idx].title = this.$("#public-page-cat-title-" + idx).text();

                this.model.save({ public_page_cats: JSON.stringify(cats) }, { patch: true, success: function (model, method, options)
                {
                    CohortCollection.reset();
                    thisThis.render();
                }});
            }
        },

        deleteCategory: function (ev, target) {
            var idx = parseInt($(target).attr("data-idx"));

            (new YesNoDialog({
                message : str.org_public_page_del_cat_confirm,
                context : this,
                button_primary : str.button_yes,
                button_secondary : str.button_no,
                accepted: function () {
                    var cats = [];

                    try {
                        cats = JSON.parse(this.model.get("public_page_cats"));
                    } catch (err) {
                        cats = [];
                    }

                    if (cats instanceof Array && cats.length > idx) {
                        var thisThis = this;

                        cats.splice(idx, 1);

                        this.model.save({ public_page_cats: JSON.stringify(cats) }, { patch: true, success: function (model, method, options)
                        {
                            CohortCollection.reset();
                            thisThis.render();
                        }});
                    }
                }
            })).render();
        },

        addCatCourses: function (ev, target) {
            var idx = parseInt($(ev.target).attr("data-idx")),
                pubs = [], cats = [];

            try {
                pubs = JSON.parse(this.model.get("published_courses"));
            } catch (err) {
                pubs = [];
            }

            try {
                cats = JSON.parse(this.model.get("public_page_cats"));
            } catch (err) {
                cats = [];
            }

            if (cats instanceof Array && cats.length > idx) {
                this.$('[data-catidx="' + idx + '"]').each(function () {
                    if (this.checked) {
                        var id = parseInt(this.getAttribute("data-courseid"));

                        if (pubs.indexOf(id) == -1) {
                            pubs.push(id);
                        }

                        if (cats[idx].courses.indexOf(id) == -1) {
                            cats[idx].courses.push(id);
                        }
                    }
                });

                var thisThis = this;

                this.model.save({ published_courses: JSON.stringify(pubs), public_page_cats: JSON.stringify(cats) }, { patch: true, success: function (model, method, options)
                {
                    CohortCollection.reset();
                    thisThis.render();
                }});
            }
        },

        removeCatCourses: function (ev, target) {
            var idx = parseInt($(ev.target).attr("data-idx")),
                cats = [];

            try {
                cats = JSON.parse(this.model.get("public_page_cats"));
            } catch (err) {
                cats = [];
            }

            if (cats instanceof Array && cats.length > idx) {
                this.$('[data-catidx="' + idx + '"]').each(function () {
                    if (this.checked) {
                        var id = parseInt(this.getAttribute("data-courseid")),
                            cidx = cats[idx].courses.indexOf(id);

                        if (cidx != -1) {
                            cats[idx].courses.splice(cidx, 1);
                        }
                    }
                });

                var thisThis = this;

                this.model.save({ public_page_cats: JSON.stringify(cats) }, { patch: true, success: function (model, method, options)
                {
                    CohortCollection.reset();
                    thisThis.render();
                }});
            }
        },

        updateCatSelection: function (ev) {
            var idx = parseInt($(ev.target).attr("data-catidx")),
                pubs = {}, canAdd = false, canRemove = false, cats = [];

            try {
                cats = JSON.parse(this.model.get("public_page_cats"));
            } catch (err) {
                cats = [];
            }

            if (cats instanceof Array && cats.length > idx) {
                for (var i=0; i<cats[idx].courses.length; i++) {
                    pubs[cats[idx].courses[i]] = 1;
                }

                this.$('[data-catidx="' + idx + '"]').each(function () {
                    if (this.checked) {
                        if (pubs[parseInt(this.getAttribute("data-courseid"))]) {
                            canRemove = true;
                        } else {
                            canAdd = true;
                        }
                    }
                });

                this.$("#public-page-add-cat-courses-btn-" + idx).prop("disabled", !canAdd);
                this.$("#public-page-remove-cat-courses-btn-" + idx).prop("disabled", !canRemove);
            }
        }
    }));
});
