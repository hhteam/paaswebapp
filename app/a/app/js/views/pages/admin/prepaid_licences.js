/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/prepaid_licences.html", "i18n!nls/strings",
        "app/views/pageview", "app/tools/dataloader", "app/tools/linkhandler", "app/collections/countries", "app/tools/utils",
        "app/collections/admin/cohorts", "app/views/dialogs/error_message", "moment"],
    function (baseTpl, tpl, str, PageView, DataLoader, LinkHandler, CountryCollection, Utils, CohortCollection, ErrorDialog)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        menuHighlight: "administration",

        //This page is hidden and can be accessed by visiting eliademy.com/offer-annual
        offer_price: 1000,
        offer_amount: 100,
        country_vat: { },

        events: {
            "click [data-call]": Utils.callMapper,
            "change #pro_billing_details [name=\"country\"]": "updatePrice",
            "change #pro_billing_details [name=\"vat_id\"]": "updatePrice"
        },

        pageRender: function ()
        {
            // TODO: multiple cohorts?
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            //This page is hidden and can be accessed by visiting eliademy.com/offer-annual/100
            if (this.code == "25") {
                this.offer_amount = 25;
                this.offer_price = 150;
            } else if (this.code == "50") {
                this.offer_amount = 50;
                this.offer_price = 250;
            } else if (this.code == "100") {
                this.offer_amount = 100;
                this.offer_price = 500;
            } else if (this.code == "unlimited") {
                this.offer_amount = 999;
                this.offer_price = 100;
            } else if (this.code == "unlim1k") {
                this.offer_amount = 999;
                this.offer_price = 1000;
            } else if (this.code == "unlim2k") {
                this.offer_amount = 999;
                this.offer_price = 2000;
            } else if (this.code == "academic") {
                this.offer_amount = 500;
                this.offer_price = 50;
            }

            DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
            {
                countries.each(function (c) {
                    this.country_vat[c.get("key")] = c.get("vat");
                }, this);

                DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (cohort)
                {
                    this.cohortModel = cohort;

                    this.$el.html(this.baseTemplate({ str: str, page: null, cohort: cohort.toJSON() }));

                    var billing_details = { };

                    try {
                        billing_details = JSON.parse(cohort.get("billing_details"));
                    } catch (err) {
                        billing_details = { };
                    }

                    if (!billing_details.company_name) {
                        billing_details.company_name = cohort.get("name");
                    }

                    this.$("#admin-page-base").html(this.template({
                        str: str,
                        countries: countries.toJSON(),
                        details: billing_details,
                        validity: moment().add(1, "year").format("LL"),
                        numlicences: this.offer_amount,
                        price: this.offer_price }));

                    LinkHandler.setupView(this);

                    $.ajax({ method: "GET", url: MoodleDir + "theme/monorail/ext/ajax_bt_token.php", dataType: "json", context: this }).done(function (token)
                    {
                        var thisThis = this;

                        BraintreeData.setup(token.merchant, 'payment_form', BraintreeData.environments.production);

                        braintree.setup(token.token, 'dropin', {
                            container: 'payment_form',
                            paymentMethodNonceReceived: function (ev, nonce) {
                                $.ajax({ method: "POST",
                                         url: MoodleDir + "theme/monorail/ext/ajax_bt_proprepaid.php",
                                         data: { nonce: nonce,
                                                 amount: thisThis.offer_price + thisThis.vat_amount,
                                                 numlicences: thisThis.offer_amount,
                                                 tax: thisThis.vat_amount,
                                                 device_data: thisThis.$("#payment_form [name=\"device_data\"]").val(),
                                                 cohort: cohortid },
                                         context: thisThis,
                                         dataType: "json" }).done(function (data)
                                {
                                    if (data.state == "success") {
                                        CohortCollection.reset();
                                        _Router.navigate("/plan-details", { trigger: true });
                                    } else {
                                        (new ErrorDialog({ message: str.error_payment })).render();
                                    }
                                });
                            }
                        });
                    });

                    this.updatePrice();
                });
            });
        },

        completePurchase: function ()
        {
            var data = { };

            this.$("#pro_billing_details").find("[name]").each(function () {
                data[$(this).attr("name")] = $(this).val();
            });

            this.cohortModel.save({ billing_details: JSON.stringify(data) }, { patch: true, success: function () {
                $("#pro_payment_form_submit").click();
            }});
        },

        updatePrice: function ()
        {
            var country = this.$("#pro_billing_details [name=\"country\"]").val(),
                vat_id = this.$("#pro_billing_details [name=\"vat_id\"]").val(),
                vat = parseFloat(this.country_vat[country]) || 0;

            var doUpdatePrice = function (vat) {
                this.vat_amount = this.offer_price * 0.01 * vat;

                this.$("#pro_vat_amount").text(this.vat_amount);
                this.$("#pro_total_price").text(this.offer_price + this.vat_amount);
            };

            if (vat && vat_id && country != "FI") {
                $.ajax({ method: "GET", url: MoodleDir + "theme/monorail/ext/ajax_vatcheck.php", data: { country: country, vat: vat_id }, context: this, dataType: "json" }).done(function (data)
                {
                    if (data instanceof Object && data.valid) {
                        doUpdatePrice.call(this, 0);
                    } else {
                        doUpdatePrice.call(this, vat);
                    }
                }).fail(function ()
                {
                    doUpdatePrice.call(this, vat);
                });
            } else {
                doUpdatePrice.call(this, vat);
            }
        }
    }));
});
