/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/skills.html",
        "i18n!nls/strings", "app/views/pageview", "app/tools/dataloader", "app/collections/admin/cohorts",
        "app/tools/linkhandler", "app/tools/utils", "progressbar"],
    function (baseTpl, tpl, str, PageView, DataLoader, CohortCollection, LinkHandler, Utils)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),

        menuHighlight: "administration",

        pageRender: function ()
        {
            var cohortid = _User.get("adminCohort");

            if (!cohortid)
            {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }
            
            // Load cohort data
            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (model)
            {
                this.model = model;

                this.$el.html(this.baseTemplate({ str: str, page: 4, cohort: model.toJSON() }));

                var data = this.model.toJSON();
                this.$("#admin-page-base").html(this.template({
                    str: str,
                    cohort: data,
                    disabled_message: Utils.premiumDisabledMessage(model.get("company_status")),
                    skillreport: (data.skillreport !== '[]') ? JSON.parse(data.skillreport) : null }));

                $('.bar').progressbar();
                $('.progress').popover();
                
                LinkHandler.setupView(this);
            });
        },

    }));
});
