/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/course_manager.html",
        "i18n!nls/strings", "app/views/pageview", "app/tools/dataloader", "app/tools/linkhandler",
        "app/views/dialogs/invite_email", "app/collections/admin/cohorts", "app/models/admin/cohortcourseuser",
        "app/collections/admin/usertags", "app/models/admin/usertag", "app/views/dialogs/admin_unenroll_confirm",
        "app/tools/utils", "app/views/dialogs/messagebox"],
    function (baseTpl, tpl, str, PageView, DataLoader, LinkHandler, InviteEmailDialog, CohortCollection,
              CohortCourseUserModel, UserTagCollection, UserTagModel, UnenrollConfirmDialog, Utils, MessageBox)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),

        menuHighlight: "administration",
        mCohortId: 0,
        mDialog: false,
        currentCourse: null,
        courseFilter: "",

        events: {
            "click [data-call]": Utils.callMapper,
            //"click #admin-add-cusers-btn": "addUsers",
            "change #admin-course-list": "courseSelected",
            "change #admin-course-user-list": "courseUsersSelected",
            "change #admin-user-list": "usersSelected",
            "change #admin-course-user-role": "updateStudentList",
            "change #admin-course-user-group-select": "updateUserList",
            "change #admin-course-type-select": "updateCourseList",
            "click #admin-remove-course-user-btn": "removeCourseUsers",
            "click #admin-add-course-user-btn": "addCourseUsers",
            "keyup #admin-course-filter-input": "updateCourseFilter",
            "keyup #admin-student-filter-input": "updateStudentFilter",
            "keyup #admin-user-filter-input": "updateUserFilter"
        },

        pageRender: function ()
        {
            var cohortid = (this.mCohortId > 0) ? this.mCohortId : _User.get("adminCohort");

            if (!cohortid)
            {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            this.currentCourse = null;

            // Load cohort data
            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (model)
            {
                this.model = model;

                // Load cohort user list
                DataLoader.exec({ context: this, collection: model.get("extusers") }, function (users)
                {
                    this.userList = users;

                    // Load cohort course list
                    DataLoader.exec({ context: this, collection: model.get("courses") }, function (courses)
                    {
                        this.courseList = courses;

                        // Load cohort user tags (groups).
                        DataLoader.exec({ context: this, collection: UserTagCollection }, function (userTags)
                        {
                            this.tagList = userTags;

                            if (this.mDialog) {
                                this.$body.html(this.baseTemplate({ str: str, cohort: model.toJSON() }));
                                this.currentCourse = this.model.get("courses").get(this.mCourseId);
                                // Load users of selected course.
                                DataLoader.exec({ context: this, collection: this.currentCourse.get("users") }, function ()
                                {
                                    this.updateUserLists();
                                });
                            } else {
                                this.$el.html(this.baseTemplate({ str: str, page: 3, cohort: model.toJSON() }));
                            }

                            $("#admin-page-base").html(this.template({
                                str: str,
                                cohort: model.toJSON(),
                                courses: courses.toJSON(),
                                users: users.toJSON(),
                                userTags: userTags.toJSON(),
                                disabled_message: Utils.premiumDisabledMessage(model.get("company_status")),
                                fromDialog: this.mDialog }));

                            this.updateUserLists();
                            LinkHandler.setupView(this);
                        });
                    });
                });
            });
        },

        /*
        addUsers: function ()
        {
            // TODO: get invited list.
            (new InviteEmailDialog({ cid: this.model.get("id"), code: this.model.get("inviteCode"), invited: [] })).render();
        },
        */

        courseSelected: function (ev)
        {
            if ($(ev.target).val())
            {
                $("#admin-course-user-list").html("<option disabled=\"disabled\">...</option>");

                this.currentCourse = this.model.get("courses").get($(ev.target).val());

                // Load users of selected course.
                DataLoader.exec({ context: this, collection: this.currentCourse.get("users") }, function ()
                {
                    // Load tags of selected course.
                    DataLoader.exec({ context: this, collection: this.currentCourse.get("tags") }, function ()
                    {
                        this.updateUserLists();
                    });
                });
            }
            else
            {
                this.currentCourse = null;
                this.updateUserLists();
            }
        },

        courseUsersSelected: function (ev)
        {
            if ($(ev.target).val() && $(ev.target).val().length > 0)
            {
                $("#admin-remove-course-user-btn").removeAttr("disabled");
            }
            else
            {
                $("#admin-remove-course-user-btn").attr("disabled", "disabled");
            }
        },

        usersSelected: function (ev)
        {
            if (this.currentCourse && $(ev.target).val() && $(ev.target).val().length > 0)
            {
                $("#admin-add-course-user-btn").removeAttr("disabled");
            }
            else
            {
                $("#admin-add-course-user-btn").attr("disabled", "disabled");
            }
        },

        addCourseUsers: function ()
        {
            var users = this.currentCourse.get("users"),
                tags = this.currentCourse.get("tags"),
                courseid = this.currentCourse.get("id"),
                rolename = $("#admin-course-user-role").val(),
                tagsAdded = 0, tagsCompleted = 0, usersCompleted = 0,
                thisThis = this;

            var checkListUpdate = function ()
            {
                if (tagsAdded == tagsCompleted && usersCompleted)
                {
                    users.once("reset", thisThis.updateUserLists, thisThis);
                    users.fetch();
                }
            };

            _.each($("#admin-user-list").val(), function (uid)
            {
                if (uid.substr(0, 1) == "t")
                {
                    var tid = parseInt(uid.substr(1));

                    tagsAdded++;
                    tags.create({ tagid: tid, courseid: courseid, rolename: rolename, name: UserTagCollection.get(tid).get("name") }, { success: function ()
                    {
                        tagsCompleted++;
                        checkListUpdate();
                    }});
                }
                else
                {
                    var user = this.model.get("users").get(parseInt(uid));

                    if (!user) {
                        user = this.model.get("extusers").get(parseInt(uid));
                    }

                    if (user) {
                        user = user.toJSON();

                        var usr = new CohortCourseUserModel(
                        {
                            id: parseInt(uid),
                            courseid: courseid,
                            cohortid: null,     // XXX: this is to trick collection into saving enrollment.
                            inviteid: this.currentCourse.get("invitecode"),
                            username: user.username,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            email: user.email,
                            rolename: rolename
                        });

                        users.add(usr, { silent: true, merge: true });
                    } else {
                        console.warn("Failed to find the user!");
                    }
                }
            }, this);

            users.save({ success: function ()
            {
                usersCompleted = 1;
                checkListUpdate();
            }});

            this.$("#admin-add-course-user-btn").attr("disabled", "disabled");
        },

        removeCourseUsers: function ()
        {
            if (this.currentCourse instanceof Object && this.currentCourse.get("course_access") == 3 && this.currentCourse.get("course_privacy") == 1 && this.currentCourse.get("price") > 0) {
                (new MessageBox({ message: str.label_cannot_unenroll_from_public_courses, backdrop: false })).render();
                return;
            }

            var users = this.currentCourse.get("users"),
                courseid = parseInt(this.currentCourse.get("id")),
                list = [ ],
                tagsDeleted = 0, tagsCompleted = 0, usersCompleted = 0,
                thisThis = this;

            var checkListUpdate = function ()
            {
                if (tagsDeleted == tagsCompleted && usersCompleted)
                {
                    users.once("reset", thisThis.updateUserLists, thisThis);
                    users.fetch();
                }
            };

            _.each($("#admin-course-user-list").val(), function (uid)
            {
                if (uid.substr(0, 1) == "t")
                {
                    var tid = parseInt(uid.substr(1));

                    this.currentCourse.get("tags").each(function (rel)
                    {
                        if (parseInt(rel.get("tagid")) == tid)
                        {
                            tagsDeleted++;
                            rel.destroy({ success: function ()
                            {
                                tagsCompleted++;
                                checkListUpdate();
                            }});
                        }
                    });
                }
                else
                {
                    var user = users.findWhere({ courseid: courseid, id: parseInt(uid) });

                    if (user)
                    {
                        list.push(user);
                    }
                    else
                    {
                        console.warn("USER NOT FOUND! course: " + courseid + ", uid: " + uid);
                    }
                }
            }, this);

            (new UnenrollConfirmDialog({ context: this, accepted: function ()
            {
                users.removeAndSave(list, { success: function ()
                {
                    usersCompleted = 1;
                    checkListUpdate();
                }});

                this.$("#admin-remove-course-user-btn").attr("disabled", "disabled");
            }})).render();
        },

        /* Fill enrolled and non-enrolled user/tag boxes.
         * */
        updateUserLists: function ()
        {
            this.updateStudentList();
            this.updateUserList();

            $("#admin-remove-course-user-btn").attr("disabled", "disabled");
            $("#admin-add-course-user-btn").attr("disabled", "disabled");

            if (this.currentCourse) {
                this.$("[data-course-action-btn]").removeAttr("disabled");
            } else {
                this.$("[data-course-action-btn]").attr("disabled", "true");
            }
        },

        /* Fill the user box with groups/users, not enrolled to current
         * course.
         * */
        updateUserList: function ()
        {
            var html = "",
                selectedUsers = [],
                selectedTags = this.currentCourse ? this.currentCourse.get("tags").pluck("tagid") : [],
                userTagId = parseInt(this.$("#admin-course-user-group-select").val());

            if (this.currentCourse) {
                this.currentCourse.get("users").each(function (user) {
                    var roles = user.get("roles"), roleOk = false;

                    for (var i=0; i<roles.length; i++) {
                        if (roles[i].shortname != "manager") {
                            roleOk = true;
                            break;
                        }
                    }

                    if (!roleOk && user instanceof Object && user.get instanceof Function) {
                        var _uid = user.get("id");

                        if (_uid && this.userList instanceof Object && this.userList.get instanceof Function) {
                            var _ulObj = this.userList.get(_uid);

                            if (_ulObj instanceof Object && _ulObj.get instanceof Function) {
                                var utype = _ulObj.get("usertype");

                                if (utype != 1 && utype != 2) {
                                    roleOk = true;
                                }
                            }
                        }
                    }

                    if (roleOk) {
                        selectedUsers.push(user.get("id"));
                    }
                }, this);
            }

            this.tagList.each(function (tag)
            {
                var name = tag.get("name");

                if (selectedTags.indexOf(tag.get("id")) == -1 &&
                    (!this.userFilter || name.toLowerCase().indexOf(this.userFilter) != -1) &&
                    (userTagId < 1 || tag.get("id") == userTagId))
                {
                    html += "<option value=\"t" + tag.get("id") + "\">" + str.group + ": " + name + "</option>\n";
                }
            }, this);

            if (html)
            {
                html += "<optgroup label=\"-----------------\"></optgroup>\n";
            }

            this.userList.each(function (user)
            {
                var fullname = user.get("firstname") + " " + user.get("lastname"), ok = true;

                if (selectedUsers.indexOf(user.get("id")) != -1) {
                    ok = false;
                } else if (this.userFilter && fullname.toLowerCase().indexOf(this.userFilter) == -1) {
                    ok = false;
                } else if (userTagId > 0 && !user.get("tags").some(function (tag) { return tag.get("tagid") == userTagId; }, this)) {
                    ok = false;
                } else if (userTagId == -1 && !user.get("ispaid")) {
                    ok = false;
                } else if (userTagId == -2 && user.get("tags").length > 0) {
                    ok = false;
                } else if (userTagId == -3 && !user.get("isteammember")) {
                    ok = false;
                }

                if (ok) {
                    html += "<option value=\"" + user.get("id") + "\">" + fullname + "</option>\n";
                }
            }, this);

            $("#admin-user-list").html(html);
        },

        /* Fill the course participant box with already enrolled
         * users/groups.
         * */
        updateStudentList: function ()
        {
            var html = "",
                currentRole = $("#admin-course-user-role").val();

            // If no course selected, the box will be empty.
            if (this.currentCourse)
            {
                // Add user tags (groups)
                this.currentCourse.get("tags").each(function (tag)
                {
                    if ((!this.studentFilter || tag.get("name").toLowerCase().indexOf(this.studentFilter) != -1)
                        && (!currentRole || currentRole == tag.get("rolename")))
                    {
                        html += "<option value=\"t" + tag.get("tagid") + "\">" + str.group + ": " + tag.get("name") + "</option>";
                    }
                }, this);

                if (html)
                {
                    html += "<optgroup label=\"-----------------\"></optgroup>\n";
                }

                // Add individual users
                this.currentCourse.get("users").each(function (user)
                {
                    var fullname = user.get("firstname") + " " + user.get("lastname"),
                        roleOk = true;

                    if (currentRole)
                    {
                        var roles = user.get("roles");
                        roleOk = false;

                        if (roles)
                        {
                            for (var i = roles.length-1; i >= 0; i--)
                            {
                                if (roles[i].shortname == currentRole)
                                {
                                    roleOk = true;
                                    break;
                                }
                            }
                        }
                        else if (user.get("rolename") == currentRole)
                        {
                            roleOk = true;
                        }

                        if (roleOk && currentRole == "manager") {
                            var utype = this.userList.get(user.get("id")).get("usertype");

                            if (utype == 1 || utype == 2) {
                                // Managers that are also cohort
                                // owners/admins are not observers
                                roleOk = false;
                            }
                        }
                    }

                    if ((!this.studentFilter || fullname.toLowerCase().indexOf(this.studentFilter) != -1)
                        && roleOk)
                    {
                        html += "<option value=\"" + user.get("id") + "\">" + fullname + "</option>";
                    }
                }, this);
            }

            $("#admin-course-user-list").html(html);
        },

        updateCourseFilter: function (ev)
        {
            this.courseFilter = $(ev.target).val().toLowerCase();
            this.updateCourseList();
        },

        updateCourseList: function () {
            var html = "",
                filter = this.courseFilter,
                privacy = parseInt($("#admin-course-type-select").val());

            this.courseList.each(function (course)
            {
                if ((!filter || course.get("fullname").toLowerCase().indexOf(filter) != -1) &&
                    (!privacy || course.get("course_privacy") == privacy))
                {
                    html += "<option value=\"" + course.get("id") + "\">" + course.get("fullname") + "</option>";
                }
            }, this);

            $("#admin-course-list").html(html);
        },

        updateStudentFilter: function (ev)
        {
            this.studentFilter = $(ev.target).val().toLowerCase();
            this.updateStudentList();
        },

        updateUserFilter: function (ev)
        {
            this.userFilter = $(ev.target).val().toLowerCase();
            this.updateUserList();
        },

        settingsChanged: function (ev, target)
        {
            var settings = this.model.get('settings'),
                toggle = null;

            for (var i=0; i<settings.length; i++)
            {
                if (settings[i].name == target.id) {
                    settings[i].value = 1 - settings[i].value;
                    toggle = settings[i].value;
                    break;
                }
            }

            setTimeout(function () {
                target.checked = toggle ? true : false;
            }, 1);

            this.model.set({ settings: settings }, { silent: true });
            this.model.save();
        },

        openCurrentCourse: function () {
            if (this.currentCourse) {
                _Router.navigate("/courses/" + this.currentCourse.get("code"), { trigger: true });
            }
        },

        reuseCurrentCourse: function () {
            if (this.currentCourse) {
                _Router.navigate("/reuse-course/" + this.currentCourse.get("code"), { trigger: true });
            }
        }
    }));
});
