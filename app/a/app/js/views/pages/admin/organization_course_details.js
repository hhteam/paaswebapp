/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/pages/admin/organization_course_details.html","i18n!nls/strings", "app/views/pageview", "app/tools/dataloader",
        "app/collections/admin/cohorts","app/tools/linkhandler", "app/tools/inlineedit", "app/views/pages/course/section",
        "app/collections/course/categories", "app/tools/servicecalls", "app/models/course/section"],
        function (tpl, str, PageView, DataLoader, CohortCollection, LinkHandler, InlineEdit, SectionView, CategoryCollection, SC,
            SectionModel)
            {
  return new (PageView.extend(
      {
        template: _.template(tpl),
        code : '',
        model : null,

        setPageAttrs : function() {
          $(document.body).attr({
            id : this.subPage.bodyId,
            class : this.subPage.bodyClass
          });
        },

        initialize : function()
        {
        },          

        setOverview : function(course_id, cohort_id)
        {
          var thisThis = this;
          // http://cdn.meme.am/instances/500x/60408157.jpg
          SC.call("local_monorailservices_course_get_cont",
              { courseid:  course_id, cohortid: cohort_id},
              function (data) {
                if(data.length > 0) 
                {
                  var sec = data[0]; 
                  var skills = ('skills' in sec) ? sec.skills : []; 
                  var m = new SectionModel({ id: sec.id, courseid: course_id, name: sec.name, summary: sec.summary, visible: sec.visible, skills: skills }),
                  attach = m.get("attachments");

                  _.each(sec.modules, function (mod)
                  {
                    // XXX: ignoring certain types of modules.
                    if (mod.modname != "forum" && mod.modname != "assign" && mod.modname != "quiz")
                    {
                      mod.source = "course_modules";
                      attach.add(mod);
                    }
                  });
                  if(m) {
                    //Add first section info now
                    thisThis.subPage  = new SectionView(
                        { model: m, index: 0, inlineEdit: new InlineEdit(),
                          attachmentPath: "sections|" + m.cid,
                          catalog: true });
                    thisThis.subPage.setElement("#section-overview");
                    thisThis.setPageAttrs();
                    thisThis.subPage.render();
                    $('#course-button-overview').addClass('active');
                    $('#content-box').addClass('in');
                  }
                } else {
                  console.log("could not fetch course content");
                }
              }, this);
        },

        initialize: function() {
        },

        pageRender : function()
        {
          // Using default logo.
          var cohortid = _User.get("adminCohort");
          if (!cohortid)
          {
            cohortid = _User.get("cohorts");
            if (typeof cohortid == "string" && cohortid.indexOf(",") != -1)
            {
              cohortid = cohortid.split(",")[0];
            }
          }
          if(!cohortid) {
            return;
          }
          var thisThis = this; 

          DataLoader.exec({ collection: CohortCollection, id: cohortid, context: this }, function (cohort)
              {
            DataLoader.exec({ collection: cohort.get("courses"), context: this }, function (courses)
                {
              thisThis.model = courses.where({ code: thisThis.code })[0];;
              if (!thisThis.model) {
                return;
              }
              var data = thisThis.model.toJSON();
              DataLoader.exec({ context: this, collection: CategoryCollection }, function (categories)
              {

              // Collect teacher information

              this.$el.html(this.template({
                str : str,
                course : data,
                categories: categories.toJSON(),
              }));  

              LinkHandler.setupView(thisThis);

              // Show course logo
              if (data.course_logo && data.course_logo != '/')
              {
                // Course logo set in model.
                $('#course-logo-img').attr('src', data.course_logo);
              }
              else
              {

                if (cohortid)
                {
                  // Cohort logo.
                  DataLoader.exec({ context: thisThis, collection: CohortCollection, id: cohortid }, function (coh)
                      {
                    $('#course-logo-img').attr('src', coh.get("cohorticonurl"));

                    this.model.set({ course_logo: coh.get("cohorticonurl") }, { silent: true });
                      });
                }
                else
                {
                  // Non-cohort logo.
                  $('#course-logo-img').attr('src', RootDir + "app/img/logo-course-title.png");
                }
              }

              // Show background picture
              var backgroundFile = "",
              backgroundUrl = RootDir + "app/img/stock/other.jpg";

              if (data.course_background)
              {
                backgroundFile = data.course_background.substr(data.course_background.lastIndexOf("/") + 1);
              }

              if (/^\d+$/.test(backgroundFile))
              {
                // Id from old data
                _.each(categories.toJSON(), function (cat)
                    {
                  _.each(cat.stock_backgrounds, function (bgr)
                      {
                    if (bgr.id == backgroundFile)
                    {
                      backgroundUrl = RootDir + "app/img/stock/" + bgr.file;
                      return;
                    }
                      });
                    });
              }
              else if (backgroundFile)
              {
                backgroundUrl = data.course_background;
                if (thisThis.changedBackground) {
                  backgroundUrl = backgroundUrl + '?update=' + new Date().getTime();
                }
              }

              $("#course-background").css("background", "url('" + backgroundUrl + "') no-repeat 50% 50%");

                  thisThis.setOverview(data.id, cohortid);
                });
               });
              });
        }         
      }));
 });
