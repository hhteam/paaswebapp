/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/organization_details.html",
        "i18n!nls/strings", "app/views/pageview", "app/tools/dataloader", "app/collections/admin/cohorts",
        "app/tools/linkhandler", "app/tools/inlineedit", "app/tools/utils", "app/collections/countries"],
    function (baseTpl, tpl, str, PageView, DataLoader, CohortCollection, LinkHandler, InlineEdit, Utils, CountryCollection)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),

        menuHighlight: "administration",

        events: {
            "click [data-call]": Utils.callMapper,
            "click #save-org-settings-btn": "saveDetails"
        },

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();
            this.mCover = null;
        },

        pageRender: function ()
        {
            // TODO: multiple cohorts?
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (model)
            {
                DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
                {
                    this.model = model;
                    var details = { };

                    try {
                        details = JSON.parse(model.get("organization_details"));
                        this.mCover = details.cover;
                    } catch (err) {}

                    this.$el.html(this.baseTemplate({ str: str, page: 1, cohort: model.toJSON() }));
                    this.$("#admin-page-base").html(this.template({
                        str: str,
                        cohort: model.toJSON(),
                        details: details,
                        countries: countries.toJSON(),
                        disabled_message: Utils.premiumDisabledMessage(model.get("company_status"))}));

                    this.inlineEdit.init(this);
                    this.inlineEdit.start();

                    LinkHandler.setupView(this);

                    this.$("*[data-field]").keydown(function ()
                    {
                        $("#save-org-settings-btn").removeAttr("disabled");
                    });
                });
            });
        },

        saveDetails: function ()
        {
            if (this.inlineEdit.commit())
            {
                var thisThis = this;

                this.model.set("organization_details", JSON.stringify({
                    about: this.model.get("about"),
                    location: this.model.get("location"),
                    country: this.model.get("country"),
                    link_fb: this.model.get("link_fb"),
                    link_li: this.model.get("link_li"),
                    link_tw: this.model.get("link_tw"),
                    link_gp: this.model.get("link_gp"),
                    link_web: this.model.get("link_web"),
                    cover: this.mCover
                }));

                $("#save-org-settings-btn").prop("disabled", true);
                this.model.save(undefined, { success: function ()
                {
                    _User.set({institution: thisThis.model.get('name')},{silent:true});
                    CohortCollection.remove(thisThis.model);
                    thisThis.render();
                }});
            }
        },

        changeLogo: function(ev)
        {
            var thisThis = this;

            _Uploader.owner({ }).restrictFileType('image/*').setFileUploadLimit(0.5 * 1024 * 1024).exec(null, null,
                function (filedata)
                {
                    thisThis.model.set({ cohorticonurl: filedata.origname, cohorticonchanged: true });

                    $("#details-org-logo").attr({ src: MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + filedata.origname });
                    $("#save-org-settings-btn").removeAttr("disabled");
                });
        },

        changeCover: function(ev)
        {
            var thisThis = this;

            _Uploader.owner({ }).restrictFileType('image/*').setFileUploadLimit(2 * 1024 * 1024).exec(null, null,
                function (filedata)
                {
                    thisThis.model.set({ cohortcoverurl: filedata.origname, cohortcoverchanged: true });

                    $("#details-org-cover").attr({ src: MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + filedata.origname + "&t=" + (new Date()).getTime() });
                    $("#save-org-settings-btn").removeAttr("disabled");

                    thisThis.mCover = "/public_images/org/cover" + thisThis.model.get("id") + ".jpg?t=" + (new Date()).getTime();
                });
        }
    }));
});
