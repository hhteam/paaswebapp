/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/user_list_subext.html", "i18n!nls/strings", "app/tools/linkhandler",
        "app/collections/admin/usertags", "app/tools/dataloader", "app/tools/utils", "app/views/dialogs/lineinput",
        "app/models/admin/usertag", "app/views/dialogs/yes_no", "app/views/dialogs/error_message",
        "app/views/dialogs/delete_cohort_member", "filesaver", "backbone"],
    function (tpl, str, LinkHandler, UserTagsCollection, DataLoader, Utils, LineInputDialog, UserTagModel,
              YesNoDialog, ErrorDialog, DeleteCohortMemberDialog, FileSaver)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: "#admin-user-extlist",
        mPage: 1,
        mUsersPerPage: 20,
        mUserFilter: "",
        mTagFilter: null,
        mUsers: [],

        events: {
            "click [data-call]": Utils.callMapper,
            "click a[data-page]": "selectPage",
            "change select[name='usersperpage']": "changeItemCount",
            "change #admin-user-filter-input": "updateUserFilter",
            "click input[name='selectall']": "selectAll",
            "click input[type='checkbox']": "updateUserSelection",
            "click a[data-filter-tagid]": "filterByTag",
            "click #admin-add-user-tag": "addTag",
            "click #admin-apply-user-tags": "applyTags",
            "click #admin-show-tag-menu": "showTagMenu",
            "click a[data-action='edit-tag']": "editTag",
            "click a[data-action='delete-tag']": "deleteTag",
            "click label[data-action='toggle-tag']": "toggleTag"
        },

        setItemsPerPage: function(count)
        {
           this.mUsersPerPage = 20;
        },

        resetPage: function ()
        {
            this.mPage = 1;
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: UserTagsCollection }, function (tagCollection)
            {
                moment.locale(_User.get("lang"));

                var users = [ ], idx = 0;

                this.options.extuserCollection.each(function (user)
                {
                    var usr = user.toJSON();

                    if ((!this.mUserFilter || this.searchMatch(this.mUserFilter, usr)) &&
                        (!this.mTagFilter ||
                         (this.mTagFilter == -1 && usr.ispaid) ||
                         (this.mTagFilter == -2 && !usr.tags.length) ||
                         (this.mTagFilter == -3 && usr.isteammember) ||
                          user.get("tags").some(function (tag) { return tag.get("tagid") == this.mTagFilter; }, this)))
                    {
                        if (!this.mUsersPerPage || (idx >= (this.mPage - 1) * this.mUsersPerPage && idx < this.mPage * this.mUsersPerPage))
                        {
                            users.push(_.extend({ cid: user.cid }, usr));
                        }

                        idx++;
                    }
                }, this);

                var currentFilter;

                if (this.mTagFilter) {
                    switch (this.mTagFilter) {
                        case -1: currentFilter = str.users_with_licence; break;
                        case -2: currentFilter = str.users_without_group; break;
                        case -3: currentFilter = str.user_directory_int; break;
                        default: currentFilter = UserTagsCollection.get(this.mTagFilter).get("name");
                    }
                } else {
                    currentFilter = str.all_users;
                }

                this.$el.html(this.template({ str: str, users: users, usercount: this.options.extuserCollection.length, mode: this.mMode, page: this.mPage,
                    pages: this.mUsersPerPage ? Math.ceil(idx / this.mUsersPerPage) : 0, currUserId: _User.get('id'),
                    itemsPerPage: this.mUsersPerPage,
                    tags: tagCollection.toJSON(),
                    currentFilter: currentFilter,
                    userFilter: this.mUserFilter }));

                LinkHandler.setupView(this);
            });
        },

        selectPage: function (ev)
        {
            ev.preventDefault();

            this.mPage = parseInt($(ev.currentTarget).attr("data-page"));
            this.render();
        },

        selectAll: function (ev)
        {
            this.$("input[name='selectrow']").prop("checked", $(ev.currentTarget).is(":checked"));
        },

        changeItemCount: function (ev)
        {
            this.mUsersPerPage = parseInt($(ev.currentTarget).val());
            this.mPage = 1;
            this.render();
        },

        updateUserFilter: function () {
            this.mUserFilter = $("#admin-user-filter-input").val();
            this.mPage = 1;
            this.render();
        },

        clearFilter: function () {
            this.mUserFilter = "";
            this.mPage = 1;
            this.render();
        },

        updateUserSelection: function (ev)
        {
            this.mUsers = [];

            var thisThis = this;

            this.$("input[data-userid]").each(function ()
            {
                if ($(this).is(":checked"))
                {
                    var u = thisThis.options.extuserCollection.get($(this).attr("data-userid"));

                    thisThis.mUsers.push(u);
                }
            });

            if (this.mUsers.length > 0) {
                this.$(".user_actions").removeAttr("disabled");
            } else {
                this.$(".user_actions").attr("disabled", "true");
            }
        },

        filterByTag: function (ev)
        {
            ev.preventDefault();

            this.mTagFilter = parseInt($(ev.currentTarget).attr("data-filter-tagid"));

            this.render();
        },

        addTag: function (ev)
        {
            ev.preventDefault();

            var thisThis = this;

            (new LineInputDialog({ header: str.new_group, label: str.name_of_group, value: "", noempty: true, accepted: function ()
            {
                var tag = new UserTagModel({ name: this.value });

                tag.save(undefined, { success: function ()
                {
                    UserTagsCollection.reset();

                    if (thisThis.mUsers.length > 0) {
                        var tagid = tag.get("id"), tagname = tag.get("name"),
                            count = thisThis.mUsers.length;

                        for (var i=0; i<thisThis.mUsers.length; i++)
                        {
                            thisThis.mUsers[i].get("tags").create({ tagid: tagid, tagname: tagname, userid: thisThis.mUsers[i].get("id") }, { success: function ()
                            {
                                if (!(--count))
                                {
                                    thisThis.render();
                                }
                            }});
                        }
                    } else {
                        thisThis.render();
                    }
                }});
            }
            })).render();
        },

        applyTags: function (ev)
        {
            ev.preventDefault();

            DataLoader.exec({ context: this, collection: UserTagsCollection }, function (tagCollection)
            {
                var remove = [], add = [], updateView = false;

                // Collect selected/unselected tags from menu
                this.$("input[data-tagid]").each(function ()
                {
                    if (!this.indeterminate)
                    {
                        if (this.checked)
                        {
                            add.push(parseInt(this.getAttribute("data-tagid")));
                        }
                        else
                        {
                            remove.push(parseInt(this.getAttribute("data-tagid")));
                        }
                    }
                });

                // Create/delete tag relations for selected users
                for (var i=0; i<this.mUsers.length; i++)
                {
                    var oldTags = [],
                        tags = this.mUsers[i].get("tags"),
                        userId = parseInt(this.mUsers[i].get("id")),
                        j = 0;

                    while (j < tags.length)
                    {
                        var id = parseInt(tags.at(j).get("tagid"));

                        if (remove.indexOf(id) != -1)
                        {
                            tags.at(j).destroy();

                            updateView = true;
                        }
                        else
                        {
                            oldTags.push(id);
                            j++;
                        }
                    }

                    _.each(add, function (tag)
                    {
                        if (oldTags.indexOf(tag) == -1)
                        {
                            tags.create({ tagid: tag, tagname: tagCollection.get(tag).get("name"), userid: userId });

                            updateView = true;
                        }
                    }, this);
                }

                // There were relation changes - must update view.
                if (updateView)
                {
                    this.render();
                }
            });
        },

        toggleTag: function (ev)
        {
            ev.preventDefault();
            ev.stopPropagation();

            var input = $(ev.target).closest("label").find("input").get(0);

            setTimeout(function ()
            {
                input.checked = !input.checked;
                input.indeterminate = false;
            }, 10);

            this.$("#admin-apply-user-tags").show();
        },

        showTagMenu: function (ev)
        {
            // Before showing tag menu, must reset view.

            ev.preventDefault();

            var tagIds = { }, totalUsers = this.mUsers.length;

            for (var i=0; i<totalUsers; i++)
            {
                this.mUsers[i].get("tags").each(function (tag)
                {
                    var id = parseInt(tag.get("tagid"));

                    if (tagIds[id])
                    {
                        tagIds[id]++;
                    }
                    else
                    {
                        tagIds[id] = 1;
                    }
                });
            }

            this.$("#admin-apply-user-tags").hide();
            this.$("input[data-tagid]").prop({ checked: false, indeterminate: false });

            _.each(tagIds, function (num, id)
            {
                if (num == totalUsers)
                {
                    this.$("input[data-tagid='" + id + "']").prop("checked", true);
                }
                else
                {
                    this.$("input[data-tagid='" + id + "']").prop("indeterminate", true);
                }
            }, this);
        },

        editTag: function (ev)
        {
            ev.preventDefault();

            var thisThis = this,
                model = UserTagsCollection.get($(ev.currentTarget).attr("data-tagid"));

            (new LineInputDialog({ header: str.edit_group, label: str.name_of_group, value: model.get("name"), noempty: true, accepted: function ()
            {
                model.save({ name: this.value }, { success: function ()
                {
                    UserTagsCollection.reset();

                    thisThis.trigger("resetView");
                    thisThis.render();
                }});
            }
            })).render();
        },

        deleteTag: function (ev)
        {
            ev.preventDefault();

            var thisThis = this,
                model = UserTagsCollection.get($(ev.currentTarget).attr("data-tagid"));

            (new YesNoDialog({ context: this, message: str.delete_group_message.replace("{%group%}", model.get("name")), accepted: function ()
            {
                model.destroy({ success: function ()
                {
                    UserTagsCollection.reset();

                    thisThis.trigger("resetView");
                    thisThis.render();
                }});

            }})).render();
        },

        makeAdmins: function (ev, target) {
            this.changeUsertype(2, (this.mUsers.length > 0) ? this.mUsers : [this.options.extuserCollection.get($(target).attr("data-cid"))]);
        },

        makeObservers: function (ev, target) {
            this.changeUsertype(3, (this.mUsers.length > 0) ? this.mUsers : [this.options.extuserCollection.get($(target).attr("data-cid"))]);
        },

        makeTeachers: function (ev, target) {
            this.changeUsertype(4, (this.mUsers.length > 0) ? this.mUsers : [this.options.extuserCollection.get($(target).attr("data-cid"))]);
        },

        makeUsers: function (ev, target) {
            this.changeUsertype(5, (this.mUsers.length > 0) ? this.mUsers : [this.options.extuserCollection.get($(target).attr("data-cid"))]);
        },

        changeUsertype: function (utype, users) {
            var count = 0, thisThis = this;

            for (var i=0; i<users.length; i++) {
                if (users[i].get("usertype") != utype) {
                    count++;

                    users[i].save({ state: 4, usertype: utype }, { success: function (model, method, options) {
                        if (options.failed) {
                            (new ErrorDialog({ header: str.addasteacher, message: str.make_cohort_member_failed.replace("$USER", model.get("firstname")) })).render();
                        }

                        if (!(--count)) {
                            thisThis.mUsers = [];

                            thisThis.options.parentView.model.get("users").reset();
                            thisThis.options.parentView.model.get("extusers").reset();
                            thisThis.options.parentView.render();
                        }
                    }});
                }
            }
        },

        deleteUsers: function (ev, target)
        {
            var thisThis = this;

            (new DeleteCohortMemberDialog({ context: this,
                accepted: function ()
                {
                    var count = this.mUsers.length, i;

                    if (count) {
                        for (i=0; i<this.mUsers.length; i++)
                        {
                            this.mUsers[i].destroy({ success: function ()
                            {
                                if (!(--count))
                                {
                                    thisThis.mUsers = [];

                                    thisThis.options.parentView.model.get("users").reset();
                                    thisThis.options.parentView.model.get("extusers").reset();
                                    thisThis.options.parentView.model.get("courses").reset();
                                    thisThis.options.parentView.render();
                                }
                            }});
                        }
                    } else {
                        var u = this.options.userCollection.get($(target).attr("data-cid"));

                        if (u) {
                            u.destroy({ success: function ()
                            {
                                    thisThis.options.parentView.model.get("users").reset();
                                    thisThis.options.parentView.model.get("extusers").reset();
                                    thisThis.options.parentView.model.get("courses").reset();
                                    thisThis.options.parentView.render();
                            }});
                        }
                    }
                },
                rejected: function () { }})).render();
        },
        
        exportUsers: function (ev, target)
        {
            var fieldsToExport = [str.name,str.label_role,str.group,str.last_access];
            var getFormattedField = function(fld) { return "\"" + fld + "\""; };
            var getUserTypeToString = function(userType) {
                var types = [str.label_org_user,str.label_account_owner,str.label_admin,str.label_org_observer,str.label_org_teacher,str.label_org_user];
                var type = ((userType > 0) && (userType < 6))?userType:0;
                return types[type];
            }
            var getGroupName = function(usr) {
                var tags = usr.get("tags");
                return tags.map(function (ut) { return ut.get("tagname") }).join(", ");
            }
            var getRow = function(rowData)
            {
                var ret = "";
                for(i=0;i<rowData.length;i++) {
                    ret += getFormattedField(rowData[i]);
                    if(i<rowData.length-1) {
                        ret += ",";
                    } else {
                        ret += "\n";
                    }
                }
                return ret;
            }
            var getDateToFormattedString = function(dt) {
                var date = new Date(dt*1000);
                var dte = [date.getDate(),date.getMonth() +1, date.getFullYear()];
                var hh = [date.getHours(), date.getMinutes(), date.getSeconds()];
                return dte.join('-') + " " + hh.join(':');
            }

            var dataForCsv = [];
            dataForCsv.push(fieldsToExport);
            this.options.extuserCollection.each(function (user) {
                fields = [user.get("firstname") + " " + user.get("lastname"), getUserTypeToString(user.get("usertype")), 
                        getGroupName(user),getDateToFormattedString(user.get("lastaccess"))];
                dataForCsv.push(fields);
            });
            
            var csvData = "";
            _.each(dataForCsv,function(data){csvData+=getRow(data);});
            FileSaver(new Blob([csvData], { type: "text/csv;charset=utf-8" }), "users.csv");
        },

        searchMatch: function (str, usr) {
            var parts = str.toLowerCase().split(/[\s;,\.]+/),
                m1 = usr.firstname.toLowerCase(),
                m2 = usr.lastname.toLowerCase(),
                m3 = usr.email.toLowerCase();

            // Assuming user wants to search for <word1> AND <word2> AND <word3>

            for (var i=0; i<parts.length; i++) {
                if (m1.indexOf(parts[i]) == -1 && m2.indexOf(parts[i]) == -1 && m3.indexOf(parts[i]) == -1) {
                    return false;
                }
            }

            return true;
        }
    });
});
