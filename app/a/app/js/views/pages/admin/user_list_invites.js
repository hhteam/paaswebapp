/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/user_list_invites.html", "i18n!nls/strings", "app/tools/linkhandler", "app/tools/utils",
        "app/views/dialogs/invite_email", "app/tools/dataloader", "app/collections/admin/usertags", "backbone"],
    function (tpl, str, LinkHandler, Utils, InviteEmailDialog, DataLoader, UserTagsCollection)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        el: "#admin-user-invitationlist",
        mPage: 1,
        mItemsPerPage: 20,

        menuHighlight: "administration",

        events: {
            "click [data-call]": Utils.callMapper,
            "click a[data-page]": "selectPage",
            "change select[name='itemsperpage']": "changeItemCount"
        },

        pageReset: function ()
        {
            this.options.userCollection.reset();
            this.mPage = 1;
            this.render();
        },

        render: function ()
        {
            DataLoader.exec({ context: this, collection: this.options.userCollection }, function (collection)
            {
                var users = [ ], idx = 0;

                collection.each(function (user)
                {
                    var usr = user.toJSON();

                    if (usr.state == 3)
                    {
                        if (!this.mItemsPerPage || (idx >= (this.mPage - 1) * this.mItemsPerPage && idx < this.mPage * this.mItemsPerPage))
                        {
                            users.push(_.extend({ cid: user.cid }, usr));
                        }

                        idx++;
                    }
                }, this);

                this.$el.html(this.template({ str: str, users: users, userCount: users.length, page: this.mPage,
                    pages: this.mItemsPerPage ? Math.ceil(idx / this.mItemsPerPage) : 0, itemsPerPage: this.mItemsPerPage,
                    inviteCode: this.options.cohort.get("inviteCode") }));

                LinkHandler.setupView(this);
            });
        },

        selectPage: function (ev)
        {
            ev.preventDefault();

            this.mPage = parseInt($(ev.currentTarget).attr("data-page"));
            this.render();
        },

        changeItemCount: function (ev)
        {
            this.mItemsPerPage = parseInt($(ev.currentTarget).val());
            this.mPage = 1;
            this.render();
        },

        addUsers: function ()
        {
            var settings = this.options.cohort.get('settings');
            var cohortselfregister = 0;

            for (var i in settings)
            {
                if (settings[i].name == 'cohort_enroll_insecure')
                {
                   cohortselfregister = settings[i].value;
                   break;
                }
            }

            DataLoader.exec({ context: this, collection: UserTagsCollection }, function (userTags)
            {
                // TODO: get invited list?
                (new InviteEmailDialog({ cid: this.options.cohort.get("id"), code: this.options.cohort.get("inviteCode"), invited: [],
                             gapp: this.options.cohort.get('googleapp'), cohort: 1,
                             cohortselfregister: cohortselfregister , cohortinvurl: this.options.cohort.get("inviteurl"), parent: this,
                             userRoles: [{id: 5, name: str.label_org_user}, {id: 4, name: str.label_org_teacher}, {id: 3, name: str.label_org_observer}, {id: 2, name: str.label_admin}],
                             userTags: userTags })).render();
            });
        },

        deleteUsers: function (ev, target)
        {
            var thisThis = this,
                u = this.options.userCollection.get($(target).attr("data-cid"));

            if (u) {
                u.destroy({ success: function ()
                {
                    thisThis.options.parentView.model.get("users").reset();
                    thisThis.options.parentView.model.get("extusers").reset();
                    thisThis.options.parentView.model.get("courses").reset();
                    thisThis.options.parentView.render();
                }});
            }
        }

        /*
        resendInvite: function (ev, target)
        {
            var thisThis = this,
                u = this.options.userCollection.get($(target).attr("data-cid"));

            if (u) {
                u.save({ state: 3 }, { success: function ()
                {
                    thisThis.render();
                }});
            }
        }
        */
    });
});

