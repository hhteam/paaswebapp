/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/payment_method.html", "i18n!nls/strings",
        "app/views/pageview", "app/tools/dataloader", "app/tools/linkhandler", "app/collections/countries", "app/tools/utils",
        "app/collections/admin/cohorts", "app/views/dialogs/error_message"],
    function (baseTpl, tpl, str, PageView, DataLoader, LinkHandler, CountryCollection, Utils, CohortCollection, ErrorDialog)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        menuHighlight: "administration",
        mode: 1,

        events: {
           "click [data-call]": Utils.callMapper,
           "change #billing_details [name]": "enableSave",
           "keyup #billing_details [name]": "enableSave"
        },

        pageRender: function ()
        {
            // TODO: multiple cohorts?
            var cohortid = _User.get("adminCohort");

            if (!cohortid) {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            DataLoader.exec({ context: this, collection: CountryCollection }, function (countries)
            {
                DataLoader.exec({ context: this, collection: CohortCollection, id: cohortid }, function (cohort)
                {
                    if (!cohort.get("isowner")) {
                        // Not an admin, go away.
                        _Router.navigate("", { trigger: true });
                        return;
                    }

                    this.cohortModel = cohort;
                    this.$el.html(this.baseTemplate({ str: str, page: 7, cohort: cohort.toJSON() }));

                    var billing_details = { };

                    try {
                        billing_details = JSON.parse(cohort.get("billing_details"));
                    } catch (err) {
                        billing_details = { };
                    }

                    if (!billing_details.company_name) {
                        billing_details.company_name = cohort.get("name");
                    }

                    this.$("#admin-page-base").html(this.template({ str: str, countries: countries.toJSON(), details: billing_details }));
                    LinkHandler.setupView(this);

                    $.ajax({ method: "GET", url: MoodleDir + "theme/monorail/ext/ajax_bt_token.php", dataType: "json", context: this }).done(function (token)
                    {
                        var thisThis = this;

                        BraintreeData.setup(token.merchant, 'payment_form', BraintreeData.environments.production);

                        braintree.setup(token.token, 'dropin', {
                            container: 'payment_form',
                            paymentMethodNonceReceived: function (ev, nonce) {
                                $.ajax({ method: "POST",
                                         url: MoodleDir + "theme/monorail/ext/ajax_bt_enter.php",
                                         data: { nonce: nonce, device_data: thisThis.$("#payment_form [name=\"device_data\"]").val(), cohort: cohortid },
                                         context: thisThis,
                                         dataType: "json" }).done(function (data)
                                {
                                    if (data.state == "success") {
                                        CohortCollection.reset();
                                        _Router.navigate("/plan-details", { trigger: true });
                                    } else {
                                        (new ErrorDialog()).render();
                                    }
                                });
                            }
                        });

                        $("#payment_method_form").on("mouseleave", this.enableSave);
                    });
                });
            });
        },

        updateDetailsClicked: function ()
        {
            var data = { };

            this.$("#billing_details").find("[name]").each(function ()
            {
                data[$(this).attr("name")] = $(this).val();
            });

            this.cohortModel.save({ billing_details: JSON.stringify(data) }, { patch: true, success: function ()
            {
                $("#payment_form_submit").click();
                $("#payment_method_save_button").addClass("disabled");
            }});
        },

        enableSave: function ()
        {
            $("#payment_method_save_button").removeClass("disabled");
        }
    }));
});
