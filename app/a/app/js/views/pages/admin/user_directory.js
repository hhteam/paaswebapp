/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/admin/admin_base.html", "text!templates/pages/admin/user_directory.html",
        "i18n!nls/strings", "app/views/pageview", "app/tools/dataloader", "app/collections/admin/cohorts",
        "app/tools/linkhandler", "app/views/pages/admin/user_list_subext", "app/tools/utils",
        "app/views/pages/admin/user_list_invites", "moment"],
    function (baseTpl, tpl, str, PageView, DataLoader, CohortCollection, LinkHandler, ExtUsersSubview, Utils,
        InvitedUsersSubview)
{
    return new (PageView.extend(
    {
        baseTemplate: _.template(baseTpl),
        template: _.template(tpl),
        menuHighlight: "administration",
        mode: 1,

        events: {
            "input input[name='searchbox']": "filterUsers"
        },

        pageRender: function ()
        {
            var cohortid = _User.get("adminCohort");
            var thisThis = this;
            if (!cohortid)
            {
                // Not an admin, go away.
                _Router.navigate("", { trigger: true });
                return;
            }

            moment.locale(_User.get("lang"));

            // Load cohort data
            DataLoader.exec({ context: thisThis, collection: CohortCollection, id: cohortid }, function (model)
            {
                thisThis.model = model;

                // Load cohort user list
                DataLoader.exec({ context: thisThis, collection: model.get("users") }, function (users)
                {
                  thisThis.users = users;
                  DataLoader.exec({ context: thisThis, collection: thisThis.model.get("extusers") }, function (extusers)
                  {
                    thisThis.$el.html(thisThis.baseTemplate({ str: str, page: 2, cohort: model.toJSON() }));

                    thisThis.$("#admin-page-base").html(thisThis.template({
                        str: str,
                        cohort: thisThis.model.toJSON(),
                        users: thisThis.users.length,
                        mode: thisThis.mode,
                        disabled_message: Utils.premiumDisabledMessage(thisThis.model.get("company_status"))
                    }));

                    LinkHandler.setupView(this);

                    thisThis.extusersView = new ExtUsersSubview({ userCollection: thisThis.users, extuserCollection: extusers, extusers: extusers.length, parentView: thisThis });
                    thisThis.invitedusersView = new InvitedUsersSubview({ userCollection: thisThis.users, cohort: thisThis.model, parentView: thisThis });

                    thisThis.extusersView.render();
                    thisThis.invitedusersView.render();
                 });
                });
            });
        },

        userDeleted: function(options) {
          this.extusersView.off('userdeleted', this.userDeleted);
          this.model.get("users").reset();
          this.mode = options.mode;
          this.render();
        },

        pageReset: function() {
          this.extusersView.off('userdeleted', this.userDeleted);
          this.model.get("users").reset();
          this.render();
        }
    }));
});
