/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/views/pageview", "text!templates/pages/catalog.html", "i18n!nls/strings", "app/tools/dataloader", "app/collections/admin/cohorts",
        "app/tools/linkhandler", "app/collections/course/categories", "app/tools/utils", "app/views/dialogs/showvideo"],

function (PageView, tpl, str, DataLoader, CohortCollection, LinkHandler, CategoryCollection, Utils, ShowVideoDialog)
{
    var ITEMS_PER_PAGE = 24;

    return new (PageView.extend(
    {
        template: _.template(tpl),
        mPage: 1,
        mCategory: 0,
        mSearchString: "",

        menuHighlight: "catalog",

        pageTitle: function ()
        {
            return str.site_name + ": " + (_User.get("orgname") || "") + " " + str.courses;
        },

        events: {
            "click [data-call]": Utils.callMapper,
            //"click .enrollable": "courseDetails",
            "change #catalog-search-input": "search",
            "keypress #catalog-search-input": "searchKeyPressed"
        },

        setPage: function (ev, target)
        {
            this.mPage = parseInt(target.getAttribute("data-page"));
            this.render();
        },

        setCategory: function (ev, target)
        {
            this.mCategory = parseInt(target.getAttribute("data-cat") || 0);
            this.mPage = 1;
            this.mSearchString = "";
            this.render();
        },

        searchKeyPressed: function (ev)
        {
            switch (ev.keyCode)
            {
                case 13:
                    this.search();
                    break;
            }
        },

        search: function ()
        {
            this.mPage = 1;
            this.mCategory = 0;
            this.mSearchString = $("#catalog-search-input").val();
            this.render();
        },

        clearSearch: function ()
        {
            this.mPage = 1;
            this.mCategory = 0;
            this.mSearchString = "";
            this.render();
        },

        /*
        courseDetails: function (ev) {
            var t = $(ev.currentTarget);
            var courseId = t.data('courseid');
            t.find('.course-intro').toggle(300);
            ev.preventDefault();
        },
        */

        openCourse: function (ev, target)
        {
            var id = parseInt(target.getAttribute("data-id")),
                item = this.currentCourseCollection.get(id).toJSON();

            if (_User.get("enrolledcourseslist").indexOf(id) == -1)
            {
                //If in public in catalog
                if ((item.course_privacy == 1) && (item.course_access > 1))
                {
                    window.location.href = EliademyUrl + "/" + item.code;
                }
                else
                {
                    // This page shouldn't exist anymore.
                    //_Router.navigate("/organization-course-details/" + item.code, { trigger: true });
                }
            }
            else
            {
                _Router.navigate("/courses/" + item.code, { trigger: true });
            }
        },

        pageRender: function ()
        {
            var cohortid = _User.get("adminCohort"), isAdmin = false;

            if (!cohortid)
            {
                cohortid = _User.get("cohorts");

                if (typeof cohortid == "string" && cohortid.indexOf(",") != -1)
                {
                    cohortid = cohortid.split(",")[0];
                }

                if (!cohortid)
                {
                    console.warn("Invalid url");
                    return;
                }
            }
            else
            {
                isAdmin = true;

                // Whether to show tutorial video dialog
                var prefs = _User.get("customfields_options");
                if(prefs === undefined || prefs.indexOf("ebvid") < 0) {
                    if (prefs) {
                        prefs = JSON.parse(prefs);
                    } else {
                        prefs = { };
                    }
                    prefs.ebvid = true; //E4B Video loink shown
                    _User.set({ customfields_options: JSON.stringify(prefs) }, { silent: true });
                    _User.save(undefined, { silent: true });
                    //Show video to the user
                    (new ShowVideoDialog({ link: "//www.youtube-nocookie.com/embed/VMOkSqi1juQ?rel=0", backdrop: false})).render();
                }
            }

            DataLoader.exec({ collection: CohortCollection, id: cohortid, context: this }, function (cohort)
            {
                DataLoader.exec({ collection: cohort.get("courses"), context: this }, function (courses)
                {
                    var clist = [ ];

                    this.currentCourseCollection = courses;

                    // In order for pagination to work normally, we
                    // must create a filtered list of all items in collection.
                    courses.each(function (c)
                    {
                        var crs = c.toJSON();

                        /*
                        // Not published in business/public catalog
                        if ((crs.course_access != 3))
                        {
                            return;
                        }
                        */

                        // Category
                        if (this.mCategory && this.mCategory != crs.category)
                        {
                            return;
                        }

                        // Search
                        if (this.mSearchString)
                        {
                            var s = this.mSearchString.toLowerCase();

                            if (crs.fullname.toLowerCase().indexOf(s) == -1 &&
                                crs.shortname.toLowerCase().indexOf(s) == -1 &&
                                crs.teacher_name.toLowerCase().indexOf(s) == -1)
                            {
                                return;
                            }
                        }

                        clist.push(crs);
                    }, this);

                    DataLoader.exec({ context: this, collection: CategoryCollection }, function (categories)
                    {
                        var categoryList = [ ],
                            currentCategory = str.all;

                        categories.each(function (cat)
                        {
                            var id = cat.get("idnumber") || 0;

                            if (id)
                            {
                                categoryList.push(cat.toJSON());

                                if (this.mCategory == id)
                                {
                                    currentCategory = cat.get("name");
                                }
                            }
                        }, this);

                        this.$el.html(this.template({
                            items: clist.slice((this.mPage - 1) * ITEMS_PER_PAGE, Math.min(this.mPage * ITEMS_PER_PAGE, clist.length)),
                            page: this.mPage,
                            itemCount: clist.length,
                            pageCount: Math.ceil(clist.length / ITEMS_PER_PAGE),
                            str: str,
                            logo: cohort.get("cohorticonurl"),
                            categories: categoryList,
                            current_category_name: currentCategory,
                            search_string: this.mSearchString,
                            isAdmin: isAdmin,
                            disabled_message: Utils.premiumDisabledMessage(cohort.get("company_status")),
                        }));

                        LinkHandler.setupView(this);
                    });
                });
            });
        }
    }));
});
