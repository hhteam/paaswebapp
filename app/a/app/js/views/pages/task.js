/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/pages/task.html", "text!templates/pages/task/task_options_block.html", "i18n!nls/strings", "app/tools/inlineedit",
        "app/tools/linkhandler", "app/views/pageview", "app/collections/task/tasks", "app/models/headercontrols", "app/models/task/task",
        "app/collections/course/courses", "app/views/attachment/attachmentlist", "app/views/dialogs/messagebox", "app/views/dialogs/yes_no",
        "app/tools/dataloader", "app/views/pages/task/task_student", "app/views/pages/task/task_teacher", "app/tools/inlinecontent",
        "app/views/header/wizard2", "app/views/header/header", "app/tools/utils", "app/tools/tracker", "app/tools/warnings", "datepicker", "timepicker"],

	function (taskTpl, optionsTpl, str, InlineEdit, LinkHandler, PageView, TaskCollection, HeaderControls, TaskModel, CourseCollection,
              AttachmentList, MessageBox, YesNoDialog, DataLoader, TaskStudentSub, TaskTeacherSub, InlineContent, HeaderView1, HeaderView2,
              Utils, Tracker, WarningTool)
{
	return new (PageView.extend(
	{
        template: _.template(taskTpl),
        optionsTemplate: _.template(optionsTpl),

        el: "#page-content",

        courseModel: null,
        mSubView: null,
        mAttachments: null,
        hasChanged: false,
        mEditMode: false,

        pageTitle: function ()
        {
            return str.site_name + ": " + (this.model ? ": " + this.model.get("name") : "");
        },

        customHeader: function ()
        {
            var course = this.isnew ?
                CourseCollection.where({code: this.coursecode})[0] :
                (this.model ? CourseCollection.get(this.model.get("courseid")) : null);

            return (course && course.get("wizard_mode") == 1) ? HeaderView1 : HeaderView2;
        },

        markAsChanged: function()
        {
            this.hasChanged = true;
        },

        startEdit: function ()
        {
            this.mEditMode = true;
            this.hasChanged = false;
            this.model.on('valuechanged', this.markAsChanged, this);

            this.inlineEdit.start();

            if (this.mSubView && this.mSubView.startEdit instanceof Function)
            {
                this.mSubView.startEdit();
            }

            this.mAttachments.startEdit();

            // Hide back button
            $('.back-button').css('visibility', 'hidden');

            if (this.isnew) {
                $('#edit-caption').html(str.create_task_caption.replace("$1", "<b>" + this.courseModel.get("fullname") + "</b>"));
            } else {
                $('#edit-caption').html(str.edit_task_caption.replace("$1", "<b>" + this.courseModel.get("fullname") + "</b>"));
            }

            // Hide edit button
            Utils.hide($('#btn-group-edit').get(0));

            // Hide view mode
            Utils.hide($('#task-view-subblock').get(0));

            // Show edit mode
            this.setupEditBlock();

            if (this.model.get("isgraded"))
            {
                WarningTool.processWarnings({ msgtype: 'info',
                    message: this.model.get('modulename') == 'quiz' ? str.task_page_cant_edit_submitted_quiz : str.task_page_cant_edit_submitted });
            }

            this.$('[data-toggle="popover"]').popover({ trigger: "hover", placement: "top" });

            _Router.setConfirmLeave(str.edit_cancel, _.bind(this.saveEdit, this), null);
        },

        saveEdit: function ()
        {
            var deferred = $.Deferred();

            if (this.model.isNew() && this.inlineEdit.placeholderActive('#full-name'))
            {
                // Use default placeholder value if user didn't edit name
                $('#full-name').html($('#full-name').attr("data-placeholder"));
            }

            var date = $("#deadline-date").datepicker("getDate");
            var sdate = $("#start-date").datepicker("getDate");

            if (date) {
                var time = $("#deadline-time-input").timepicker("getTime");

                if (time instanceof Object) {
                    date.setHours(time.getHours());
                    date.setMinutes(time.getMinutes());
                } else {
                    date.setHours(0);
                    date.setMinutes(0);
                }

                date.setSeconds(0);

                this.model.set('duedate', Math.round(date.getTime() / 1000));
            } else {
                this.model.set("duedate", 0);
            }

            if (sdate) {
                var stime = $("#start-time-input").timepicker("getTime");

                if (stime instanceof Object) {
                    sdate.setHours(stime.getHours());
                    sdate.setMinutes(stime.getMinutes());
                } else {
                    sdate.setHours(0);
                    sdate.setMinutes(0);
                }

                sdate.setSeconds(0);

                this.model.set('startdate', Math.round(sdate.getTime() / 1000));
            } else {
                this.model.set('startdate', 0);
            }

            this.model.set("nolatesubmissions", $("#allow-late-submissions").prop("checked") ? 1 : 0);

            if (!this.inlineEdit.commit({ deferred: deferred }))
            {
                if (!this.inlineEdit.triggerValidation())
                {
                    // If error cannot be shown as tool tip, it must be a
                    // missed right answer.

                    (new MessageBox({ message: this.inlineEdit.errorString })).render();
                }

                return deferred;
            }

            var thisThis = this;

            deferred.done(function ()
            {
                thisThis.model.off('valuechanged', thisThis.markAsChanged);
                thisThis.mEditMode = false;

                _Router.setConfirmLeave(null, null, null);

                // Change Edit caption
                $('#edit-caption').html('');

                // Show Edit button
                Utils.unhide($('#btn-group-edit').get(0));

                // Show back button
                $('.back-button').css('visibility', 'visible');

                // Hide due date edit mode
                $("#task-edit-subblock").empty();

                // Show view mode
                Utils.unhide($('#task-view-subblock').get(0));

                if (thisThis.model.get('modulename') == 'quiz') {
                    Utils.hide($('#quiz-attempt-edit').get(0));
                }

                if (!thisThis.isnew) {
                    thisThis.model.fetch({ success: _.bind(thisThis.render, thisThis) });
                } else {
                    delete thisThis.isnew;

                    _Router.navigate("/tasks/" + thisThis.model.get("instanceid"), {trigger: true});
                }
            });

            if (this.isnew)
            {
                this.model.save(undefined, { success: function (model, method, options)
                {
                    // In order to save attachments, task id must be known,
                    // therefore attachments are saved after task.
                    var taskid = model.get("instanceid");

                    thisThis.model.get("attachments").each(function (attach)
                    {
                        attach.set({ taskid: taskid }, { silent: true });
                    });

                    var completeSave = function ()
                    {
                        thisThis.mAttachments.saveEdit({ deferred: deferred, success: function ()
                        {
                            // Then, task is saved again, with real attachment ids
                            thisThis.model.save(undefined, { deferred: deferred, quietsave: true, success: function (model, method, options)
                            {
                                thisThis.courseModel.get("assignments").reset();
                                thisThis.courseModel.get("sections").reset();
                            }});
                        }});
                    };

                    if (thisThis.mSubView && thisThis.mSubView.saveEdit instanceof Function)
                    {
                        thisThis.mSubView.saveEdit(deferred, completeSave, taskid);
                    }
                    else
                    {
                        completeSave();
                    }
                }});
            }
            else
            {
                var completeSave = function ()
                {
                    thisThis.mAttachments.saveEdit({ deferred: deferred, success: function()
                    {
                        if (thisThis.hasChanged || thisThis.mAttachments.hasChanged)
                        {
                            thisThis.model.save(undefined, { deferred: deferred, success: function (model, method, options)
                            {
                                thisThis.courseModel.get("assignments").reset();
                                thisThis.courseModel.get("sections").reset();
                            }});
                        }
                        else
                        {
                            // Don't save if nothing changed?
                            deferred.resolveWith(thisThis);
                        }
                    }});
                };

                if (this.mSubView && this.mSubView.saveEdit instanceof Function)
                {
                    this.mSubView.saveEdit(deferred, completeSave, this.model.get("instanceid"));
                }
                else
                {
                    completeSave();
                }
            }

            return deferred;
        },

        cancelEdit: function ()
        {
            (new YesNoDialog({
                message : str.edit_cancel,
                context : this,
                button_primary : str.button_cancel_editing,
                button_secondary : str.button_save_changes,
                accepted : function ()
                {
                    this.model.off('valuechanged', this.markAsChanged);
                    this.inlineEdit.rollback();
                    this.mAttachments.cancelEdit();
                    this.mEditMode = false;
                    _Router.setConfirmLeave(null, null, null);

                    if (this.mSubView && this.mSubView.cancelEdit instanceof Function)
                    {
                        this.mSubView.cancelEdit();
                    }

                    // Change Edit caption
                    $('#edit-caption').html('');

                    // Show Edit button
                    Utils.unhide($('#btn-group-edit').get(0));

                    // Show back button
                    $('.back-button').css('visibility', 'visible');

                    // Hide due date edit mode
                    $("#task-edit-subblock").empty();

                    // Show view mode
                    Utils.unhide($('#task-view-subblock').get(0));

                   if(this.model.get('modulename') == 'quiz') {
                        Utils.hide($('#quiz-attempt-edit').get(0));
                    }

                    if (this.isnew)
                    {
                        delete this.isnew;

                        _Router.navigate("/courses/" + this.coursecode + "/tasks", { trigger: true });
                    }
                    else
                    {
                        this.model.fetch({ success: _.bind(this.render, this) });
                    }
                },
                rejected: function ()
                {
                    this.saveEdit();
                }})).render();
        },

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();

            return this;
        },

        events: {
            "click [data-call]": Utils.callMapper,
            "change [data-call][data-toggle=\"toggle\"]": Utils.callMapper,
            "click #btn_edit.edit_task" : function(ev)
            {
                HeaderControls.set("state", 2);
                HeaderControls.get("handler").startEdit();

                // Hide Edit button
                Utils.hide($('#btn-group-edit').get(0));
            },
            "click #btn_delete_task" : function(ev) {
                (new YesNoDialog({
                    message : str.delete_task_message,
                    context : this,
                    button_primary : str.button_delete,
                    button_secondary : str.button_cancel,
                    accepted: function ()
                    {
                        var that = this;
                        TaskCollection.remove(this.model);

                        this.model.destroy({ success: function ()
                        {
                            that.courseModel.get("assignments").reset();
                            that.courseModel.get("sections").reset();
                            _Router.navigate("/courses/" + that.courseModel.get("code") + "/tasks", { trigger: true });
                        }});
                    }})).render();
            },

            "click #allow-late-submissions": "markAsChanged"
        },

        pageRender: function ()
        {
            this.mEditMode = false;

            moment.locale(_User.get("lang"));

            if (this.isnew)
            {
                // start a new task. Make sure we have the course first.
                DataLoader.exec({ collection: CourseCollection, where: {code: this.coursecode}, context: this }, function (cmodel)
                {
                    this.courseModel = cmodel;

                    this.model = new TaskModel({
                        courseid: cmodel.get("id"),
                        can_edit: true,
                        modulename: "assign",
                        visible : 1 ,
                        nosubmissions: 0
                    });

                    this.renderTask();
                });
            }
            else
            {
                // Make sure we have the task.
                DataLoader.exec({ collection: TaskCollection, where: { instanceid: this.instanceid }, context: this }, function (model)
                {
                    if (model.isNew())
                    {
                        _Router.navigate("/", { trigger: true });
                    }
                    else
                    {
                        this.model = model;

                        // Make sure we have the course.
                        DataLoader.exec({ collection: CourseCollection, id: model.get("courseid"), context: this }, function (cmodel)
                        {
                            this.courseModel = cmodel;

                            this.renderTask();
                        });
                    }
                });
            }
        },

        renderTask: function () {
            var data = this.model.toJSON();

            this.$el.html(this.template({ str: str, data: data, course: this.courseModel.toJSON(), placeholder: str.new_task }));

            switch (data.modulename) {
                case "assign":
                    if (data.can_edit) {
                        this.mSubView = new TaskTeacherSub({ parentView: this });
                        this.$("#task-type-description").text(str.task_options_1);
                    } else {
                        Tracker.track("taskview", { courseid: this.courseModel.get("id"), taskid: this.model.get("id") });

                        this.mSubView = new TaskStudentSub({ parentView: this });
                    }
                    break;

                case "quiz":
                    console.warn("Invalid quiz link");
                    _Router.navigate("/quizzes/" + this.instanceid, { trigger: true, replace: true });
                    return;

                default:
                    console.warn("Invalid task type: " + data.modulename);
            }

            //Set visibility false to attachments in ckeditor
            var attachments = this.model.get("attachments");
            $('<div/>').html(data.intro).contents().find("*[data-attachment]").each(function ()
            {
                var ea = attachments.get(this.getAttribute("data-attachment"));
                if(ea) {
                    ea.set('visible', 0);
                }
            });

            this.mAttachments = new AttachmentList(
                { collection: this.model.get("attachments"), inlineEdit: this.inlineEdit, attachmentPath: "attachments", source: 'task' });

            this.mSubView.render();

            InlineContent.fixTextDirection(this.$el);
            InlineContent.fixMathJaxText(this.$el);
            InlineContent.fixAttachmentUrls(this.$el, this.model.get("attachments"), false, data.can_edit);

            this.inlineEdit.setAttachmentList({ model: this.model.get("attachments"), data: { taskid: data.instanceid, source: "new_item" }});
            this.inlineEdit.init(this);

            if (data.can_edit)
            {
                HeaderControls.set("handler", this);

                if (this.isnew)
                {
                    HeaderControls.set("state", 2);
                    this.startEdit();
                }
                else
                {
                    HeaderControls.set("state", 1);
                }
            }
            else
            {
                HeaderControls.set("state", 0);
            }

            LinkHandler.setupView(this);
        },

        setupEditBlock: function () {
            var data = this.model.toJSON(),
                thisThis = this;

            $("#task-edit-subblock").html(this.optionsTemplate({ str: str, data: data, isnew: this.isnew }));

            Utils.setupPopups($("#task-edit-subblock"));

            $("#deadline-date").datepicker({ orientation: "auto top", autoclose: true, format: "yyyy-mm-dd" }).on("change", function (ev) {
                thisThis.markAsChanged();
            });

            $("#deadline-time-input").timepicker({ timeFormat: "H:i", step: 15 }).on("changeTime", function (ev) {
                thisThis.markAsChanged();
            });

            $("#start-date").datepicker({ orientation: "auto top", autoclose: true, format: "yyyy-mm-dd" }).on("change", function (ev) {
               thisThis.markAsChanged();
            });

            $("#start-time-input").timepicker({ timeFormat: "H:i", step: 15 }).on("changeTime", function (ev) {
                thisThis.markAsChanged();
            });

            if (data.duedate > 0) {
                $("#deadline-date").datepicker("setDate", new Date(data.duedate * 1000));
                $("#deadline-time-input").timepicker("setTime", new Date(data.duedate * 1000));
            }

            if (data.startdate > 0) {
                $("#start-date").datepicker("setDate", new Date(data.startdate * 1000));
                $("#start-time-input").timepicker("setTime", new Date(data.startdate * 1000));
            }

            if (parseInt(data.nosubmissions)) {
                this.$("#btn-task-type-nosubmissions").addClass("active");
                this.$("#task-type-description").text(str.task_options_2);
            } else {
                this.$("#btn-task-type-upload").addClass("active");
                this.$("#task-type-description").text(str.task_options_1);
            }
        },

        changeTaskType: function (ev)
        {
            this.inlineEdit.commit({ dry: true });

            switch (ev.target.id)
            {
                case "btn-task-type-upload":
                    this.model.set({ modulename: "assign", nosubmissions: 0 }, { silent: true });
                    this.$("#task-type-description").text(str.task_options_1);

                    if (this.mSubView.className != "TaskTeacherSub")
                    {
                        this.mSubView.undelegateEvents();
                        this.mSubView = new TaskTeacherSub({ parentView: this });
                        this.mSubView.render();
                        this.inlineEdit.reInit(this.mSubView.$el);

                        if (this.mSubView.startEdit instanceof Function)
                        {
                            this.mSubView.startEdit();
                        }
                    }
                    break;

                case "btn-task-type-nosubmissions":
                    this.model.set({ modulename: "assign", nosubmissions: 1 }, { silent: true });
                    this.$("#task-type-description").text(str.task_options_2);

                    if (this.mSubView.className != "TaskTeacherSub")
                    {
                        this.mSubView.undelegateEvents();
                        this.mSubView = new TaskTeacherSub({ parentView: this });
                        this.mSubView.render();
                        this.inlineEdit.reInit(this.mSubView.$el);

                        if (this.mSubView.startEdit instanceof Function)
                        {
                            this.mSubView.startEdit();
                        }
                    }
                    break;
            }
        }
    }));
});
