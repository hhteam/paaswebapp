/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/header/wizard.html", "i18n!nls/strings", "app/tools/linkhandler", "app/models/headercontrols",
        "app/views/header/button", "backbone"],
	function (tpl, str, LinkHandler, HeaderControls, ButtonView)
{

	return new (Backbone.View.extend(
	{
		el: "#page-header",

		template: _.template(tpl),

		render: function ()
		{
			this.$el.html(this.template({ str: str }));

            $('#wizard-step-one').css('color', '#666666');
            $('#wizard-step-two').css('color', '#4189dd');

            ButtonView.setElement(this.$el.find("#edit-button-block")).render();

			LinkHandler.setupView(this);

			return this;
		}
	}));
});
