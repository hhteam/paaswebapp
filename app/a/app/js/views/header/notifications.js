/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/header/notifications.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/utils",
        "app/tools/linkhandler", "app/collections/feed", "moment"],

    function (tpl, str, DialogView, Utils, LinkHandler, FeedCollection, moment)
{
    return DialogView.extend(
    {
        template: _.template(tpl),
        dropDownMode: true,
        fixedPosition: true,

        dialogEvents: {
            "click [data-call]": Utils.callMapper
        },

        renderDialog: function ()
        {
            var data = [ ];

            FeedCollection.each(function (item)
            {
                var i = item.toJSON();

                i.time = moment(i.timestamp, "X").fromNow();

                data.push(i);
            });

            this.$body.html(this.template({ str: str, data: data, moreAvailable: FeedCollection.moreAvailable }));

            LinkHandler.setupView(this);
        },

        openNotification: function (ev, target)
        {
            $.ajax({ url: MoodleDir + "local/monorailfeed/ext/ajax_mark_read.php?", data: { i: target.getAttribute("data-nid") }, success: function()
            {
                FeedCollection.fetch({ always: true, increment: false });
            }});

            _Router.navigate(target.getAttribute("data-link"), { trigger: true });
            this.closeDialog();
        },

        loadMore: function (ev, target)
        {
            FeedCollection.once("reset", _.bind(this.render, this));
            FeedCollection.fetch({ always: true, increment: 5 });
        },

        clearAll: function (ev, target)
        {
            FeedCollection.markAllRead();
            this.closeDialog();
        }
    });
});
