/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/header/button.html", "app/models/headercontrols", "app/tools/utils", "backbone"],
    function(tpl, HeaderControls, Utils)
{
    return new (Backbone.View.extend(
    {
        template: _.template(tpl),

        events: {
            "click [data-call]": Utils.callMapper
        },

        initialize: function()
        {
            this.model.bind("change", this.render, this);
            return this;
        },

        buttonClicked: function (ev, target)
        {
            var fun = this.model.get("editButtons")[parseInt(target.getAttribute("data-idx"))].handler;

            if (fun instanceof Function)
            {
                fun();
            }
            else
            {
                console.warn("NO FUN!");
            }
        },

        render: function()
        {
            if (this.model.get('state') == 1 || this.model.get('state') == 0)
            {
                // Show mainmenu
                Utils.unhide($('#mainmenu').get(0));
                Utils.hide($('#header-button-box').get(0));

                this.$el.html("");
            }
            else
            {
                // Hide mainmenu
                Utils.hide($('#mainmenu').get(0));
                Utils.unhide($('#header-button-box').get(0));

                this.$el.html(this.template({ buttons: this.model.get("editButtons") }));
            }

            return this;
        },

        model: HeaderControls
    }));
});
