/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/header/header.html", "i18n!nls/strings", "app/tools/linkhandler", "app/collections/feed",
        "app/views/header/button", "app/models/user", "app/definitions/notifications", "app/tools/utils",
        "app/views/header/shortcourselist", "app/views/header/notifications", "app/collections/course/coursesshort",
        "app/views/dialogs/invite_email", "app/tools/dataloader"],
	function (tpl, str, LinkHandler, FeedCollection, ButtonView, UserModel, NotifMessages, Utils,
            ShortCourseListDialog, NotificationDialog, CourseShortCollection, InviteEmailDialog, DataLoader)
{
    var $nag;

    function displayNag( msg ) {
        $nag.find('span')
            .html('<i class="fa fa-exclamation-circle"></i> ' + msg );
        $nag.show();
        $('#page').css('padding-top', $nag.height() + 'px');
    }

    function slideUpNag() {
        $nag.slideUp({
            duration: 300,
            complete: function ()
            {
                $('#page').css('padding-top', '0px');
            },
            progress: function ()
            {
                $('#page').css('padding-top', $nag.height() + 'px');
            }});
    }

	return new (Backbone.View.extend(
	{
		el: "#page-header",

		template: _.template(tpl),

		initialize: function ()
		{
			UserModel.bind("updated", this.render, this);

            return this;
		},

		events: {
            "click [data-call]": Utils.callMapper,
            "click #header-logout-link": "logout",
            "click #header-invite-friends": "inviteFriends",
            "click #header-close-nag": "closeNag"
		},

		render: function ()
		{
			var data = { data: {}, str: str, RootDir: RootDir };

			_.extend(data.data, UserModel.toJSON());

            var activeLink = this.$("#mainmenu .menubutton.active").attr("id"),
                trial_expired = data.data.trial_expired;

            data.is_teacher = false;

            for (var i=0; i<data.data.enrolledcourses.length; i++) {
                if (data.data.enrolledcourses[i].userrole != "student") {
                    data.is_teacher = true;
                    break;
                }
            }

            data.CurrentCatalogIdx = 0;

            for (var i=0; i<data.data.cohortparticipant.length; i++) {
                if (this.CurrentCatalog == data.data.cohortparticipant[i].cohortid) {
                    data.CurrentCatalogIdx = i;
                    break;
                }
            }

			this.$el.html(this.template(data));
            $nag = $('#header-nag-block');

            // Retain highlighted state after re-render.
            if (activeLink)
            {
                this.$("#" + activeLink).addClass("active");
            }

            ButtonView.setElement(this.$el.find("#edit-button-block")).render();

            if ((document.body.style.webkitTransform === undefined || typeof FileReader == "undefined") &&
                document.body.mozRequestFullScreen === undefined &&
                !("onpropertychange" in document && !!window.matchMedia) && !(!!window.MSStream))
            {
                displayNag( str.browser_warning1 +
                    ' <a target="_blank" href="http://eliademy.uservoice.com/knowledgebase/articles/236003">' + str.learn_more + '</a>' );
            }
            else if (data.data.admin_msg)
            {
                displayNag(data.data.admin_msg);
                setTimeout( slideUpNag, 30000 );
            }
            else if( trial_expired )
            {
                displayNag( str.banner_trial_expired );
            }
            else if (_User.get("sessionkey"))
            {
                var userCreated = new Date(_User.get("firstaccess") * 1000);
                var lastAccess = new Date(_User.get("lastaccess") * 1000);
                var prefs = _User.get("customfields_options");

                if (prefs)
                {
                    prefs = JSON.parse(prefs);
                }
                else
                {
                    prefs = { };
                }

                for (var i=0; i<NotifMessages.length; i++)
                {
                    var msg = NotifMessages[i];

                    // Message from the future.
                    if (msg.since.getTime() > (new Date()).getTime())
                    {
                        continue;
                    }

                    if (msg.type == "action")
                    {
                        // Action messages can be shown for users who
                        // registered after the message (but not before 24h
                        // after user registration).
                        if (userCreated.getTime() > (new Date()).getTime() - 86400000)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        // Other types of messages are not shown if user
                        // registered or visited after the message was created.
                        if (msg.since.getTime() < userCreated.getTime() || (lastAccess && msg.since.getTime() < lastAccess.getTime()))
                        {
                            continue;
                        }

                        // Or user has already seen it.
                        if (prefs.lastnag && msg.since.getTime() < (new Date(prefs.lastnag * 1000)).getTime())
                        {
                            continue;
                        }
                    }

                    if (msg.condition instanceof Function && !msg.condition())
                    {
                        // Condition not met.
                        continue;
                    }

                    displayNag( msg.message );
                    $nag.find('a').click(function () { $nag.hide(); });
                    setTimeout( slideUpNag, 9000 );

                    break;
                }
            }

			LinkHandler.setupView(this);

			return this;
		},

        logout: function (ev)
        {
            ev.preventDefault();

            require(["app/tools/servicecalls", "app/collections/course/courses"], function (SC, CoursesCollection)
            {
                var doLogout = function ()
                {
                    // Close relay socket
                    SC.closeSocket();
                    // Clear data
                    UserModel.clear({ silent: true });
                    CoursesCollection.reset({ silent: true });
                    FeedCollection.reset();
                    //TODO: clear any other data when logging out

                    localStorage.removeItem('login.wants-url');
                    localStorage.removeItem('user_login');
                    window.location = ExitUrl;
                };

                var postCleanup = function ()
                {
                    $.ajax({ url: MoodleDir + "login/logout.php?sesskey=" + SC.getSesskey(),
                         type: "POST", success: doLogout, error: doLogout });
                };
                $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_logout_cleanup.php",
                     type: "POST", success: postCleanup, error: postCleanup });

            });
        },

        inviteFriends: function (ev)
        {

            ev.preventDefault();
            (new InviteEmailDialog({ cid: _User.get("id") , code: 'eliademy', invited: [] })).render();

        },

        closeNag: function (ev)
        {
            ev.preventDefault();

            var prefs = _User.get("customfields_options");

            if (prefs)
            {
                prefs = JSON.parse(prefs);
            }
            else
            {
                prefs = { };
            }

            prefs.lastnag = Math.ceil((new Date()).getTime() / 1000);

            _User.set({ customfields_options: JSON.stringify(prefs) }, { silent: true });
            _User.save(undefined, { silent: true });

            slideUpNag();
        },

        quickCourses: function (ev, target)
        {
            DataLoader.exec({ context: this, collection: CourseShortCollection.TeachingOngoing }, function (teaching)
            {
                DataLoader.exec({ context: this, collection: CourseShortCollection.LearningOngoing }, function (learning)
                {
                    (new ShortCourseListDialog({ moveUnder: target, learning: learning, teaching: teaching })).render();
                });
            });
        },

        showNotifications: function (ev, target)
        {
            (new NotificationDialog({ moveUnder: target })).render();
        }
	}));
});
