/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/header/shortcourselist.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/utils",
        "app/tools/linkhandler"],

    function (tpl, str, DialogView, Utils, LinkHandler)
{
    return DialogView.extend(
    {
        template: _.template(tpl),
        dropDownMode: true,
        fixedPosition: true,

        dialogEvents: {
            "click [data-call]": Utils.callMapper
        },

        renderDialog: function ()
        {
            // First few courses only...
            var crs1 = [], crs2 = [], i;

            for (i=0; i<8 && i<this.options.learning.length; i++)
            {
                crs1.push(this.options.learning.at(i).toJSON());
            }

            for (i=0; i<8 && i<this.options.teaching.length; i++)
            {
                crs2.push(this.options.teaching.at(i).toJSON());
            }

            this.$body.html(this.template({ str: str, learning: crs1, teaching: crs2 }));

            LinkHandler.setupView(this);
        },

        gotoCourse: function (ev, target)
        {
            _Router.navigate("/courses/" + target.getAttribute("data-code"), { trigger: true });

            this.closeDialog();
        },

        showAllCourses: function ()
        {
            _Router.navigate("/", { trigger: true });

            this.closeDialog();
        },

        startCourse: function ()
        {
            _Router.navigate("/courses/wizard", { trigger: true });

            this.closeDialog();
        }
    });
});
