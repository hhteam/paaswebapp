/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["backbone"], function ()
{
    /* Extend this to create dialog views.
     *
     * Example:
     *
     * var MyDialog = DialogView.extend(
     * {
     *     dialogEvents: { "click .abc": "hello" },
     *
     *     dropDownMode: true/false
     *     fixedPosition: true/false
     *
     *     renderDialog: function () { this.$body.html("Hello! <a href=\"#\" class=\"btn btn-danger\">Close</a>") },
     *
     *     saveClicked: function () { alert("Saved!") },
     *     cancelClicked: function () { alert("Cancelled!") },
     *     validate: function () { return inputsValid(); }
     * })
     *
     * var myDialog = new MyDialog({ moveUnder/moveAbove: element, finished: function (status) { alert(status ? "Ok" : "Cancel") } })
     * myDialog.render()
     *
     * WARNING: DO NOT USE data-dismiss or any other bootstrap dialog
     *          control - they override the normal control flow. Use
     *          only dialog_accept and dialog_reject classes for
     *          buttons that close the dialog.
     *          DO NOT USE dialog_reject or dialog_reject for anything
     *          else (no styles, custom actions or state checks).
     */
    return Backbone.View.extend(
    {
        el: "#dialog-space",

        render: function ()
        {
            var thisThis = this, mUnder, pos, dialog_width, ptrLeft;

            $("#page-warnings-container").empty();

            this.$el.html("<div class=\"modal hide mui-dialog\" data-backdrop=\"static\" style=\"z-index: 990;\"><div class=\"ptr\"></div><div class=\"mui-dialog-body\"></div></div>");

            this.$body = this.$el.find(".mui-dialog-body");

            if (this.renderDialog instanceof Function)
            {
                this.renderDialog();
            }
            else
            {
                console.warn("Dialog has no renderDialog method.");
            }

            if ("moveUnder" in this.options || "moveAbove" in this.options)
            {
                this.$el.children().css('width', "auto");
                mUnder = $(this.options.moveUnder ? this.options.moveUnder : this.options.moveAbove);
                pos = mUnder.offset();

                if (this.fixedPosition)
                {
                    pos.top -= $(document).scrollTop();
                    pos.left -= $(document).scrollLeft();
                }

                dialog_width = this.$el.children().width();
                ptrLeft = pos.left + (mUnder.width() / 2);

                if (this.options.moveUnder)
                {
                    pos.top += mUnder.height() + 15;
                }
                else
                {
                    pos.top -= this.$el.children().height() + 13;
                }

                pos.width = dialog_width;
                pos.left += (mUnder.width() - dialog_width) / 2;

                if (pos.left < 0)
                {
                    pos.left = 20;
                }

                if (pos.left + dialog_width > $(window).width() - 20)
                {
                    pos.left = $(window).width() - 20 - dialog_width;
                }

                ptrLeft -= pos.left + 10;

                if (ptrLeft < 10)
                {
                    ptrLeft = 10;
                }

                if (ptrLeft > dialog_width - 10)
                {
                    ptrLeft = dialog_width - 10;
                }

                this.$el.children().find(".ptr").css('left', ptrLeft);

                if (this.options.moveAbove)
                {
                    this.$el.children().find(".ptr").css({'top': this.$el.children().height() + 16, "transform": "rotate(180deg)"});
                }

                this.$el.children().css(pos);

                this.$el.children().css('position', this.fixedPosition ? 'fixed' : 'absolute');
                this.$el.children().css('margin-left', 0);
                $("#dialog-overlay").show();

                if (this.dropDownMode)
                {
                    $("#dialog-overlay").bind("click", function (ev) { thisThis.reject.apply(thisThis, [ ev ]); });
                }

                if ("focusTo" in this.options)
                {
                    $(this.options.focusTo).focus();
                }

                this.$el.children().modal({ backdrop: false });
            }
            else if ("moveXY" in this.options)
            {
                this.$el.children().css('width', "auto");
                dialog_width = this.$el.children().width();
                pos = { left: this.options.moveXY[0] - dialog_width / 2, top: this.options.moveXY[1] };

                this.$el.children().css(pos);
                this.$el.children().css('position', this.fixedPosition ? 'fixed' : 'absolute');
                this.$el.children().css('margin-left', 0);
                $("#dialog-overlay").show();

                if (this.dropDownMode)
                {
                    $("dialog-overlay").bind("click", function (ev) { thisThis.reject.apply(thisThis, [ ev ]); });
                }

                this.$el.children().modal({ backdrop: false });
            }
            else if ("fromTop" in this.options)
            {
                this.$el.children().find(".ptr").hide();
                this.$el.children().modal("show");
                this.$el.children().css('position', this.fixedPosition ? 'fixed' : 'absolute');
                this.$el.children().css('top', $(window).scrollTop() + this.options.fromTop);

                if (this.dropDownMode)
                {
                    $(".modal-backdrop").bind("click", function (ev) { thisThis.reject.apply(thisThis, [ ev ]); });
                }
            }
            else
            {
                this.$el.children().find(".ptr").hide();
                this.$el.children().modal("show");

                if (this.dropDownMode)
                {
                    $(".modal-backdrop").bind("click", function (ev) { thisThis.reject.apply(thisThis, [ ev ]); });
                }
            }

            if ("focusTo" in this.options && ! "moveUnder" in this.options && ! "moveAbove" in this.options) {
                $(this.options.focusTo).focus();
            }

            this.delegateEvents(_.extend(
            {
                "click .dialog_accept": "accept",
                "click .dialog_reject": "reject"
            }, (this.dialogEvents instanceof Object) ? this.dialogEvents : { } ));
        },

        reject: function (ev)
        {
            if (ev instanceof Object && ev.preventDefault instanceof Function)
            {
                ev.preventDefault();
                ev.stopPropagation();
            }

            if (this.acceptCancel instanceof Function)
            {
                if(!this.acceptCancel(ev)) {
                  return;
                }
            }

            if ($(ev.target).hasClass("disabled") || $(ev.target).is(":disabled"))
            {
                return;
            }

            this.closeDialog();

            if (this.cancelClicked instanceof Function)
            {
                this.cancelClicked();
            }

            if (this.options.rejected instanceof Function)
            {
                this.options.rejected.apply(this.options.context ? this.options.context : this, [ ]);
            }

            if (this.options.finished instanceof Function)
            {
                this.options.finished.apply(this.options.context ? this.options.context : this, [ false ]);
            }
        },

        accept: function (ev)
        {
            if (ev instanceof Object && ev.preventDefault instanceof Function)
            {
                ev.preventDefault();
                ev.stopPropagation();
            }

            if ($(ev.target).hasClass("disabled") || $(ev.target).is(":disabled"))
            {
                return;
            }

            if (this.validate instanceof Function)
            {
                if (!this.validate())
                {
                    return;
                }
            }
            
            if (this.verifyCaptcha instanceof Function)
            {
                if (!this.verifyCaptcha())
                {
                    return;
                }
            }

            if (this.saveClicked instanceof Function)
            {
                this.saveClicked(ev);
            }

            this.closeDialog();

            if (this.options.accepted instanceof Function)
            {
                this.options.accepted.apply(this.options.context ? this.options.context : this, [ ]);
            }

            if (this.options.finished instanceof Function)
            {
                this.options.finished.apply(this.options.context ? this.options.context : this, [ true ]);
            }
        },

        closeDialog: function ()
        {
            this.$el.empty();
            this.undelegateEvents();

            $("#dialog-overlay").hide().unbind("click");
            $(".modal-backdrop").unbind("click").remove();
        }
    });
});
