/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/warnings.html", "app/models/warnings"],

    function (tpl, WarningsModel)
{
    return Backbone.View.extend(
    {
        el: "#page-warnings-container",
        template: _.template(tpl),
        initialize: function(){

            this.render();
        },
		render: function () {
            var message = $(this.template({ type: this.model.get('type'), message: this.model.get('message')}));
            this.$el.append(message);
            
            if (this.options.timeout) {
                setTimeout(function () {
                    $(message).fadeOut(400);
                }, this.options.timeout);
            }
			
			return this;
		},

    });

});
