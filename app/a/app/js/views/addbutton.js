/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/addbutton.html", "text!templates/addbutton_str.html", "i18n!nls/strings", "app/tools/linkhandler", "backbone"],
    function (tpl, tpl2, str, LinkHandler)
{
    var AddButton = Backbone.View.extend(
    {
        template: _.template(tpl),
        template2: _.template(tpl2),

        events: {
            "click .add-item": "add_item"
        },

        initialize: function ()
        {
            this.mItems = ("items" in this.options ? this.options.items : [ ]);
        },

        setItems: function (items)
        {
            this.mItems = items;

            return this;
        },

        isEuropeanaSearchNeeded: function ()
        {
            needed = false;
            //Europeana seach is needed only in course editing, not on quizzes
            //we need to find if we are in course page or in quizz page
            //easiest way to find is 'count the number of items in items'
            needed = this.mItems && (this.mItems.length > 2);
            return needed;
        },
        
        render: function ()
        {
            var segments = str.label_add_attachments.split("|");

            if (segments.length != 5) {
                // Invalid string (translators forgot to add '|'?) fall
                // back to old button.

                this.$el.html(this.template({ str: str, items: this.mItems }));
                console.warn("Invalid translation: label_add_attachments. Falling back to simple button.");
            } else {
                // Have 5 segments. Order is important.

                var labelorder = [ str.new_file, str.googledrive, str.ltitool, str.upload_video ],
                    line = "";

                for (var i=0; i<4; i++) {
                    for (var j in this.mItems)  {
                        if (labelorder[i] == this.mItems[j].label) {
                            if ("link" in this.mItems[j]) {
                                line += segments[i].replace("{", "<a href=\"" + this.mItems[j].link + "\">").replace("}", "</a>");
                            } else {
                                line += segments[i].replace("{", "<a href=\"#\" class=\"ignored_link add-item\" data-itemid=\"" + j + "\">").replace("}", "</a>");
                            }
                        }
                    }
                }

                line += segments[4];
                europeana_needed = this.isEuropeanaSearchNeeded();
                this.$el.html(this.template2({ line: line, str: str, addtasks: (typeof this.options.notasks != "undefined") ? 0 : 1, europeanaNeeded: europeana_needed }));
            }


            $('.add-button-place').css('display', 'inline-block');

            LinkHandler.setupView(this);

            return this;
        },

        add_item: function (ev)
        {
            var it = this.mItems[parseInt($(ev.target).closest("[data-itemid]").attr("data-itemid"))];

            if (it)
            {
                it.handler.apply((it.owner ? it.owner : this), [ it.data ]);
            }
            else
            {
                console.warn("Invalid action.");
            }

            ev.preventDefault();
        }
    });

    return AddButton;
});
