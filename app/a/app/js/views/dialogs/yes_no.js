/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define([ "text!templates/dialogs/yes_no.html", "i18n!nls/strings", "app/views/dialogview" ],
        function(tpl, str, DialogView) {
            return DialogView.extend({
                template : _.template(tpl),

                renderDialog : function() {
                    if (!this.options.button_primary) {
                        this.options.button_primary = str.button_yes;
                    }
                    if (!this.options.button_secondary) {
                        this.options.button_secondary = str.button_no;
                    }

                    this.$body.html(this.template({
                        str : str,
                        message : this.options.message,
                        header : this.options.header,
                        button_primary: this.options.button_primary,
                        button_secondary: this.options.button_secondary,
                        invert_danger: this.options.invert_danger
                    }));
                }
            });
        });
