/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/share_course.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/utils",
        "app/collections/settings/social_accounts", "app/tools/dataloader", "app/tools/social_utils", "app/models/course/review"],
    function (tpl, str, DialogView, Utils, SocialAccountsCollection, DataLoader, SocialUtils, ReviewModel)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        dialogEvents: {
            "click [data-call]": Utils.callMapper
        },

        mStars: null,
        mAccounts: { },
        mShareOn: [ ],

        renderDialog: function ()
        {
            // TODO: different types of sharing...

            DataLoader.exec({ context: this, collection: SocialAccountsCollection }, function (accounts)
            {
                this.mAccounts = { };
                accounts.each(function (item)
                {
                    this.mAccounts[item.get("type")] = item.toJSON();
                    this.mShareOn.push(item.get("type"));
                }, this);

                this.$body.html(this.template({
                    str: str,
                    accounts: this.mAccounts,
                    header: this.options.header,
                    message: this.options.message,
                    review: this.options.review,
                    rate: this.options.rate,
                    image: this.options.image }));

                this.updateSocialButtons();
            });
        },

        rate: function (ev)
        {
            var stars = this.mStars = parseInt(ev.target.getAttribute("data-item"));

            this.$body.find("[data-item]").each(function ()
            {
                if (this.getAttribute("data-item") <= stars)
                {
                    $(this).addClass("fa fa-star").removeClass("fa-star-o").css("color", "gold");
                }
                else
                {
                    $(this).addClass("fa fa-star-o").removeClass("fa-star").css("color", "gold");
                }
            });
        },

        updateSocialButtons: function ()
        {
            DataLoader.exec({ context: this, collection: SocialAccountsCollection }, function (accounts)
            {
                var _this = this, showAutoshares = false;
                this.mAccounts = { };
                accounts.each(function (item)
                {
                    this.mAccounts[item.get("type")] = item.toJSON();

                    if (!item.get("share_enroll"))
                    {
                        // If at least one account has automatic shares
                        // disabled, offering to re-enable them...
                        showAutoshares = true;
                    }
                }, this);

                if (showAutoshares)
                {
                    this.$body.find("#share_enrollments_box").show();
                }
                else
                {
                    this.$body.find("#share_enrollments_box").hide();
                }

                this.$body.find("[data-social]").each(function ()
                {
                    var type = this.getAttribute("data-social");

                    if (_this.mAccounts[type])
                    {
                        $(this).removeClass("inactive");

                        if (_this.mShareOn.indexOf(type) != -1)
                        {
                            $(this).addClass("active");
                        }
                        else
                        {
                            $(this).removeClass("active");
                        }
                    }
                    else
                    {
                        $(this).addClass("inactive").removeClass("active");
                    }
                });
            });
        },

        socialButtonToggle: function (ev, target)
        {
            var type = target.getAttribute("data-social");

            if (this.mAccounts[type])
            {
                var idx = this.mShareOn.indexOf(type);

                if (idx == -1)
                {
                    this.mShareOn.push(type);
                }
                else
                {
                    this.mShareOn.splice(idx, 1);
                }

                this.updateSocialButtons();
            }
            else
            {
                this.mShareOn.push(type);
                SocialUtils["add" + type.charAt(0).toUpperCase() + type.slice(1) + "Account"](_.bind(this.updateSocialButtons, this));
            }
        },

        validate: function ()
        {
            if (this.options.rate && this.mStars === null)
            {
                var box = this.$body.find("#share-rating-block");

                box.css("border-color", "#F00");
                setTimeout(function () { box.css("border-color", "#FFF"); }, 300);

                return false;
            }

            return true;
        },

        saveClicked: function ()
        {
            (new ReviewModel({
                courseid: this.options.courseid,
                stars: this.mStars,
                review: this.options.review ?
                    this.$body.find("textarea[name='review']").val() : "",
                shareon: this.mShareOn.join(","),
                enable_autoshare: this.$body.find("input[name='enable_autoshare']:visible").is(":checked") ?
                    1 : 0,
                image: this.options.image,
                share_message: this.options.share_message })).save();
        },

        neverRemind: function (ev)
        {
            if (this.options.reminder)
            {
                this.options.reminder.save({ action: "N" });
            }

            this.reject(ev);
        }
    });
});
