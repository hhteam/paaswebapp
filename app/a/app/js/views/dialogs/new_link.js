/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/dialogs/new_link.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/utils"], function (tpl, str, DialogView, Utils)
{
    return  DialogView.extend(
    {   
        //el:'#dialog-space',
        template: _.template(tpl),
        data: null,
        initialize : function()
        {
            this.data = this.options.data;           
        },
        dialogEvents:
        {   
            "keyup  input.url_input": "showPreview",
            
        },
        renderDialog: function ()
        {
            this.$body.html(this.template({ str: str, data: this.data }));
        },
        showPreview: function(ev)
        {
            var attrid = { },
                myurl = this.$body.find(".url_input").val().trim();
                target_el= $("#link-preview");
            //parse type & video id
            attrid = Utils.externalUrlData(myurl, attrid);
            
            switch (attrid.type)
            {

                case "youtube":
                    if (attrid.videoid) 
                    {
                        $("#error_msg").hide();
                        target_el.show().find(".media-thumbnail").attr('src','https://img.youtube.com/vi/'+attrid.videoid+'/2.jpg');
                    }
                    else
                    {
                        target_el.hide();
                        $("#error_msg").show();                        
                    }
                break;
                case "vimeo":
                    $.ajax({
                        type: 'GET',
                        url: 'https://vimeo.com/api/v2/video/' + attrid.videoid + '.json',
                        dataType: 'jsonp',
                        success: function(data) {
                            $("#error_msg").hide();
                            if (data[0].thumbnail_large) 
                                target_el.show().find(".media-thumbnail").attr('src', data[0].thumbnail_large);
                            
                        },
                        error:function(data){
                            target_el.hide();
                            $("#error_msg").show();
                        }  
                    });
                break;
                case "slideshare":
                    $.ajax({
                        type: 'GET',
                        url: 'https://www.slideshare.net/api/oembed/2',
                        data: {format: 'json', url:myurl},
                        //slideshare not allowed by Access-Control-Allow-Origin
                        dataType: 'jsonp',
                        success: function(data) {
                            $("#error_msg").hide();
                            if(data["thumbnail"])
                                target_el.show().find(".media-thumbnail").attr('src', data["thumbnail"]);
                            
                        },
                        error:function(data){
                            target_el.hide();
                            $("#error_msg").show();
                        }                    
                    });

                break;
                default:
                    target_el.hide();
                    $("#error_msg").show();
            }
          
        },
        saveClicked: function (ev)
        {
                ev.preventDefault();
        },

        validate: function ()
        {
            var urlinput = $.trim(this.$body.find(".url_input").val());
            if(Utils.validateUrl(urlinput)){
                this.url = urlinput;
                return true;
            }
            return false;
        }
    });
});
