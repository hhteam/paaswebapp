/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/error_message.html", "i18n!nls/strings", "app/views/dialogview"],
    function (tpl, str, DialogView)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        renderDialog: function ()
        {
            this.$body.html(this.template({
                str: str,
                header: this.options.header ? this.options.header : str.error_unknown,
                message: this.options.message ? this.options.message : str.error_service }));
        }
    });
});
