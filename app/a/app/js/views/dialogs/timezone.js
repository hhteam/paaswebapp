/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/timezone.html", "i18n!nls/strings", "app/views/dialogview", "jquerytimezone", "jquerymaphilight"],
    function (tpl, str, DialogView)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        renderDialog: function ()
        {
            this.$body.html(this.template({ str: str, timezone: this.options.timezone }));

            $('#timezone-image').timezonePicker({ target: "#timezone-dialog-zone-name" });

            // bootstrap override...
            $(".mui-dialog").css({ width: "630px", "margin-left": "-315px" });
        },

        saveClicked: function ()
        {
            if (this.model)
            {
                this.model.set("timezone", this.$("#timezone-dialog-zone-name").val());
            }
        }
    });
});
