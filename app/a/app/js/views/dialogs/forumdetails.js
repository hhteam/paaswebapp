/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/dialogs/forumdetails.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/inlineedit"],
    function (tpl, str, DialogView, InlineEdit)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();
        },

        renderDialog: function ()
        {
            this.$body.html(this.template({ str: str, forum: this.model.toJSON(), header: this.options.header }));

            this.inlineEdit.init(this);
            this.inlineEdit.start();
        },

        validate: function ()
        {
            if (this.inlineEdit.commit())
            {
                return true;
            }
            else
            {
                if (!this.inlineEdit.triggerValidation())
                {
                    (new MessageBox({ message: this.inlineEdit.errorString, backdrop: false })).render();
                }

                return false;
            }
        },

        saveClicked: function ()
        {
            this.model.save();
        }
    });
});
