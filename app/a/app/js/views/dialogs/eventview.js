/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/eventview.html", "i18n!nls/strings", "app/views/dialogview", "moment"],
    function(tpl, str, DialogView, moment)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        renderDialog: function()
        {
            var data = this.model.toJSON();

            this.$body.html(this.template(
            {
                str: str,
                data: data,
                from_date: moment(parseInt(data.timestart || 0), "X").format("DD.MM.YYYY"),
                from_time: moment(parseInt(data.timestart || 0), "X").format("HH:mm"),
                to_date: moment(parseInt(data.timestart || 0) + parseInt(data.timeduration || 0), "X").format("DD.MM.YYYY"),
                to_time: moment(parseInt(data.timestart || 0) + parseInt(data.timeduration || 0), "X").format("HH:mm")
            }));
        }
    });
});
