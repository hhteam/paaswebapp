/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/share_certificate.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/utils",
        "app/tools/tracker", "moment"],

    function (tpl, str, DialogView, Utils, Tracker)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        dialogEvents: {
            "click [data-call]": Utils.callMapper
        },

        renderDialog: function ()
        {
            this.$body.html(this.template({ str: str, course: this.options.course.toJSON() }));
        },

        gotoCourse: function ()
        {
            this.closeDialog();
            _Router.navigate("/courses/" + this.options.course.get("code") + "/certificate", { trigger: true });
        },

        shareCertificateFacebook: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.options.course.get("cert_hashurl") + ".html",
                certImgUrl = EliademyUrl + "/cert/" + this.options.course.get("cert_hashurl") + ".jpg";

            Tracker.track("s-facebook-certificate", { courseid: this.options.course.get("id")});
            window.open("https://www.facebook.com/dialog/feed?app_id=" + FBAppID + "&display=page" +
                "&name=" + encodeURIComponent(this.options.course.get("fullname")) +
                "&description=" + encodeURIComponent(str.certificate_share_text.replace("%COURSENAME", this.options.course.get("fullname"))) +
                "&link=" + encodeURIComponent(certUrl) +
                "&picture=" + encodeURIComponent(certImgUrl) +
                "&redirect_uri=" + encodeURIComponent("https://facebook.com"));
        },

        shareCertificateTwitter: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.options.course.get("cert_hashurl") + ".html";
            Tracker.track("s-twitter-certificate", { courseid: this.options.course.get("id")});
            window.open("https://twitter.com/intent/tweet?url=" + encodeURIComponent(certUrl) +
                "&text=" + encodeURIComponent(str.certificate_share_text.replace("%COURSENAME", this.options.course.get("fullname"))));
        },

        shareCertificateLinkedin: function ()
        {
            var certUrl = EliademyUrl + "/cert/" + this.options.course.get("cert_hashurl") + ".html";

            Tracker.track("s-linkedin-certificate", { courseid: this.options.course.get("id")});
            window.open("https://www.linkedin.com/profile/add?_ed=0_7nTFLiuDkkQkdELSpruCwKiOYJGHNsAonQIWT1kHI6TXWMlneN2rrjbx8g5GDDYZaSgvthvZk7wTBMS3S-m0L6A6mLjErM6PJiwMkk6nYZylU7__75hCVwJdOTZCAkdv" +
                //"&force=true" +
                "&pfCertificationName=" + encodeURIComponent(this.options.course.get("fullname")) +
                "&pfCertificationUrl=" + encodeURIComponent(certUrl) +
                "&pfLicenseNo=" + encodeURIComponent(this.options.course.get("cert_hashurl")) +
                "&pfCertStartDate=" + encodeURIComponent(moment(this.options.course.get("cert_issuetime"), "X").format("YYYYMM"))
                // + "&pfAuthorityId=5016192"
                );
        }
    });
});
