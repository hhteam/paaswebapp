/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/dialogs/showvideo.html", "i18n!nls/strings", "app/views/dialogview"], function (tpl, str, DialogView)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        renderDialog: function ()
        {
            this.$body.html(this.template({ str: str, link: this.options.link }));
        }
    });
});
