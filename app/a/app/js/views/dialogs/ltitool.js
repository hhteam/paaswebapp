/**
 * Eliademy.com
 * 
 * @copyright CBTec Oy
 * @license All rights reserved
 */

define([ "text!templates/dialogs/ltitool.html", "i18n!nls/strings",
         "app/views/dialogview", "app/models/ltitool",
         "app/collections/ltitools", "app/models/ltimod",
         "app/tools/dataloader", "app/tools/utils",
         "app/collections/course/courses", "app/tools/warnings" ], function(tpl, str, DialogView,
             LtiToolModel, LtiToolsCollection, LtiModuleModel, DataLoader, Utils,
             CoursesCollection, WarningTool) {
  return DialogView
  .extend({
    template : _.template(tpl),
    data : null,
    typeid : -1,
    iconurl : '',
    launchurl : '',
    moduleid : 0,
    state : 0,
    name : '',
    model : null,
    toolEdit : false,
    newtool : false,

    dialogEvents : {
      "keyup input[name=name]" : function(ev) {
        $(ev.target).parent().removeClass("error");
        this.$("#errorstr").text('');
        this.$("#errorstr").hide();
      },
      "click a#ltiopennewwindow" : "openInNewWindow",
      "click a#ltiedit" : "editClicked",
      "click a#ltidelete" : "deleteClicked",
      "click #toolslist a" : "listItemClicked",
    },

    listItemClicked : function(ev) {
      var item = $(ev.target);
      if (this.typeid < 0) {
        this.typeid = parseInt(item.parent().attr("id").substr(
            8));
      } else {
        var id = "#radio-" + this.typeid;
        $(id).prop('checked', false);
        this.typeid = parseInt(item.parent().attr("id").substr(
            8));
      }
      $("#radio-" + this.typeid).prop('checked', true);
    },

    initialize : function() {
      this.data = this.options.data;
    },

    enableDisableInput : function(enable) {
      $("#ctoolname").prop('disabled', enable);
      $("#ctoolurl").prop('disabled', enable);
      $("#ciconurl").prop('disabled', enable);
      $("#ckey").prop('disabled', enable);
      $("#csecret").prop('disabled', enable);
      $("#cemail").prop('disabled', enable);
      $("#cusername").prop('disabled', enable);
    },

    validateToolFields : function() {
      var cr = /[{}<>=%]/; // not allowed characters;
      if (!Utils.validateUrl($("#ctoolurl").val())) {
        return false;
      }
      if (!($("#ctoolname").val())
          || cr.test($("#ctoolname").val())) {
        if (!($("#ctoolname").val())) {
          this.$("#errorstr").text(str.ltitool_nameerror);
        } else {
          this.$("#errorstr").text(str.valid_text_input);
        }
        this.$("#name").focus().parent().addClass("error");
        this.$("#errorstr").show();
        return false;
      }
      if (($("#csecret").val() && !$("#ckey").val())
          || (!$("#csecret").val() && $("#ckey").val())) {
        this.$("#errorstr").text(str.ltitool_keyerror);
        this.$("#name").focus().parent().addClass("error");
        this.$("#errorstr").show();
        return false;
      }
      return true;
    },

    editClicked : function(ev) {
      ev.preventDefault();
      $(ev.target).parent().removeClass("error");
      this.$("#errorstr").text('');
      this.$("#errorstr").hide();
      if (this.toolEdit) {
        // Validate parameters
        if (!this.validateToolFields()) {
          return;
        }
        this.toolEdit = false;
        this.enableDisableInput(true);
        $("#ltiedit").text(str.edit);
        var m = LtiToolsCollection.get(this.typeid);
        if (m) {
          m.set({
            toolurl : $("#ctoolurl").val(),
            instructorchoicesendname : $('#cusername')
            .prop('checked'),
            instructorchoicesendemailaddr : $('#cemail')
            .prop('checked'),
            resourcekey : $("#ckey").val(),
            password : $("#csecret").val(),
            toolname : $("#ctoolname").val(),
            icon : $("#ciconurl").val()
          }, {
            silent : true
          });
          m.save(undefined, {
            success : function(model, method, options) {
              LtiToolsCollection.reset({
                silent : true
              });
              LtiToolsCollection.fetch();
            }
          });

        }
        return;
      }
      this.toolEdit = true;
      this.enableDisableInput(false);
      $("#ltiedit").text(str.button_save);
    },

    deleteClicked : function(ev) {
      ev.preventDefault();
      var m = LtiToolsCollection.get(this.typeid);
      if (m) {
        m.set({
          deletetool : true
        }, {
          silent : true
        });
        var thisThis = this;
        m.destroy({
          success : function() {
            thisThis.reject(ev);
          },
          error : function() {
            thisThis.reject(ev);
          }
        });
      }
    },

    openInNewWindow : function(ev) {
      ev.preventDefault();
      window.open(this.launchurl);
    },

    renderDialog : function() {

      DataLoader.exec({
        collection : LtiToolsCollection,
        context : this
      }, function(ltitools) {
        this.$body.html(this.template({
          str : str,
          ltitools : ltitools.toJSON()
        }));
      });
    },

    setState : function(state) {
      if (this.state == state) {
        return;
      }
      this.state = state;
      if (state == 1) {
        $("#ltiselector").hide();
        this.$body.find(".modal-body").height(600);
        $("#ltidelete").hide();
        $("#ltiedit").hide();
        $("#lticustomtool").hide();
        $("#lticonfigurl").attr("src", this.launchurl);
        $("#ltiopennewwindow").show();
        $("#lticonfigtool").show();
        $("#ltiheader").text(str.ltitool_config);
        $("#ltisave").text(str.button_done);
      } else if (state == 2) {
        $("#ltiselector").hide();
        $("#lticustomtool").show();
        $("#ltiheader").text(str.custom_add_lti_tool);
      } else if (state == 3) {
        $("#ltiselector").hide();
        $("#lticustomtool").show();
        $("#ltiheader").text(str.custom_edit_lti_tool);
        $("#ltidelete").show();
        $("#ltiedit").show();
        this.enableDisableInput(true);
      }
    },

    setToolFieldsData : function(ltimodel) {
      $("#ctoolname").val(ltimodel.get('name'));
      $("#ctoolurl").val(ltimodel.get('url'));
      $("#ciconurl").val(ltimodel.get('iconurl'));
      $config = ltimodel.get('config');
      $("#ckey").val($config['resourcekey']);
      $("#csecret").val($config['password']);
      $('#cemail').prop('checked',
          ($config['sendemailaddr'] == 1) ? true : false);
      $('#cusername').prop('checked',
          ($config['sendname'] == 1) ? true : false);
    },

    validate : function() {
      if (this.typeid < 0) // Max
      {
        this.$("#errorstr").text(str.select_ltitool);
        this.$("#errorstr").show();
        return false;
      }
      var cr = /[{}<>=%]/; // not allowed characters;
      this.$("#errorstr").hide();
      if (this.state != 1) {
        var thisThis = this;
        if ((this.typeid == 0 && this.state != 2)) {
          this.setState(2);
          return false;
        }
        if (this.typeid == 0) {
          if (!this.validateToolFields()) {
            return false;
          }
          var tmodel = new LtiToolModel({
            course : this.data.courseid,
            toolurl : $("#ctoolurl").val(),
            instructorchoicesendname : $('#cusername')
            .prop('checked'),
            instructorchoicesendemailaddr : $('#cemail')
            .prop('checked'),
            resourcekey : $("#ckey").val(),
            password : $("#csecret").val(),
            toolname : $("#ctoolname").val(),
            icon : $("#ciconurl").val()
          });
          tmodel.save(undefined, {
            success : function(model, method, options) {
              thisThis.newtool = true;
              thisThis.typeid = model.get('id');
              LtiToolsCollection.reset({
                silent : true
              });
              LtiToolsCollection.fetch();
              thisThis.createLtiModule(model.get('id'),
                  model.get('name'));
            }
          });
        } else {
          var ltimodel = LtiToolsCollection.get(this.typeid);
          if (typeof ltimodel !== 'undefined'
            && (ltimodel.get('custom') == 1)
            && thisThis.state != 3) {
            this.setToolFieldsData(ltimodel);
            this.setState(3);
            return false;
          }
          this.createLtiModule(ltimodel.get('id'), ltimodel
              .get('name'));
        }
        return false;
      }
      if (this.typeid == 0) {
        LtiToolsCollection.reset({
          silent : true
        });
        LtiToolsCollection.fetch();
      }
      return true;
    },

    createLtiModule : function(typeid, name) {
      var thisThis = this;
      thisThis.model = new LtiModuleModel({
        name : name,
        typeid : typeid,
        course : thisThis.data.courseid,
        sectionid : thisThis.data.sectionid
      });
      thisThis.model.save(undefined, {
        success : function(model, method, options) {
          thisThis.moduleid = model.get('cmid');
          thisThis.launchurl = model.get('launchurl');
          thisThis.name = model.get('name');
          thisThis.iconurl = model.get('iconurl');
          thisThis.setState(1);
        },
        error : function(model, method, options) {
          thisThis.moduleid = model.get('cmid');
          thisThis.launchurl = model.get('launchurl');
          thisThis.name = model.get('name');
          thisThis.iconurl = model.get('iconurl');
          thisThis.setState(1);
          WarningTool.processWarnings({msgtype:'error', message:str.ltitool_configerror});
        }
      });
    },

    toolError : function() {
      if (this.newtool) {
        var ltimodel = LtiToolsCollection.get(this.typeid);
        if (typeof ltimodel !== 'undefined'
          && (ltimodel.get('custom') == 1)
          && this.state != 3) {
          this.setToolFieldsData(ltimodel);
          this.setState(3);
        }
      }
      this.$("#errorstr").text(str.ltitool_configerror);
      this.$("#errorstr").show();
    },

    cancelClicked : function(ev) {
      if (this.state == 1) {
        this.model.destroy({
          success : function() {
          },
          error : function() {
          }
        });
      }
    },

    saveClicked : function(ev) {
      var thisThis = this;
    }
  });
});
