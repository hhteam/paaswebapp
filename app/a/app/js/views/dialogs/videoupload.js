/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/dialogs/video_upload.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/warnings",
        "app/collections/admin/cohorts", "app/tools/dataloader", "app/tools/servicecalls", "app/views/dialogs/messagebox", "jqueryui"],
    function (tpl, str, DialogView, WarningTool, CohortCollection, DataLoader, SC, MessageBox)
{
    return DialogView.extend(
    {
        template: _.template(tpl),
        vdata: null,
        uploadRequest: null,
        cancelRequest: false,
        filename: null,
        origname: null,
        filesize: 0 , 
        thumbnail: null,
        vresid: null, 
        vjqXHR: null, 
        availablespace: 0,
        currentUsage: 0,
        uploadinit: false,
 
        dialogEvents:
        {
            "click #btn-upload": "uploadVideo",
            "click #video-support": "videoSupport",
            "click #btn-insert": "saveClicked"
        },

        enableHide: function(hide) {
           if($('.mui-dialog').data('modal')) {
               $('.mui-dialog').data('modal').isShown = hide;
           }
        },

        acceptCancel: function(ev) {
            if($(ev.target).hasClass("modal-backdrop")) {
                return !(this.uploadinit); //If upload has started , cancel needs to be clicked
            }
            else if($(ev.target).hasClass("dialog_reject") || $(ev.target).hasClass("close")) {
                return true;
            }
            return false;
        },

        initialize: function()
        {
            var thisThis = this;
            $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_user_vstore.php", context: this, dataType: "json",
               success: function (data)
               {
                   thisThis.currentUsage = data.fsize;
               }, error: function ()
               {
               }});
        },

        renderDialog: function ()
        {
            var rusage = parseInt(_User.get("videousage"));
            var rlimit = parseInt(_User.get("videolimit"));
            if(this.currentUsage > rusage) {
                rusage = this.currentUsage;
            }
            this.availablespace = rlimit - rusage;
            var resData = {vusage: Math.round(rusage/rlimit  * 100) + "%", videousage: (rusage/1024).toFixed(2) + ' GB / ' + (rlimit/1024).toFixed(2) + ' GB' };
            this.$body.html(this.template({ str: str, data: resData}));
            $('#btn-cancel').hide();
        },

        
        videoSupport: function (ev) {
          var thisThis = this;
          if(parseInt(_User.get("videousage")) <= 500) { 
             (new MessageBox({ message: str.label_minvideo_usage, backdrop: false })).render(); 
          } else { 
            SC.call("local_monorailservices_send_support_mail", {
              mailinfos : [{subject: "Support: Request for additional space for videos", type: 'video'}]}, 
                  function (data) {
                    $('#video-support-feedback').show();
                    if ('warnings' in data && data.warnings.length > 0) {
                      //some error
                    }
                  }, this, { errorHandler: function (data) {
                      thisThis.upload_error();
                  }});
          }
        },

        cancelClicked: function (ev) {
          this.enableHide(true);
          this.cancelRequest = true;
          this.uploadinit = false;
          _Uploader.cancelUpload();
          if(this.vjqXHR && this.vjqXHR.readystate != 4){
            this.vjqXHR.abort();
          }
        },
       
        upload_process: function (e, data)
        {
           $('#upload-progress-now').css('width', Math.round(data.loaded / data.total * 100) + "%");
           $('#upload-progress-bar span').text(Math.round(data.loaded / (1024 * 1024)) + ' MB / ' + Math.round(data.total / (1024 * 1024)) + ' MB');
           if(data.loaded == data.total) {
               $('#upload-progress-bar span').text(str.checking_file);
               $('#upload-progress-bar').addClass('progress-striped');
               $('#upload-progress-bar').addClass('active');
           }
        },  

        final_upload_complete: function()
        {
           $('#upload-progress-bar span').text(this.filename);
           $('#upload-progress-bar').removeClass('progress-striped');
           $('#upload-progress-container').removeClass('span9');
           $('#upload-progress-container').addClass('span12');
           $('#upload-buttons').hide();
           $('#btn-cancel').hide();
           $('#btn-upload').hide();
           $('#btn-insert').removeClass('disabled');
        },

        upload_complete: function() 
        {
           $('#upload-progress-bar').addClass('progress-striped');
           $('#upload-progress-bar').addClass('active');
           $('#upload-progress-bar span').text(str.checking_file);
           $('#upload-progress-now').show();
        },

        upload_error: function(error) 
        {
           this.enableHide(true);
           $('#upload-progress-bar').removeClass('progress-striped');
           $('#upload-progress-bar span').text(str.upload_video_text1);
           $('#upload-progress-now').attr('width', "0%");
           $('#btn-cancel').hide();
           $('#btn-upload').show();
           var msg = str.error_unknown;
           if(error instanceof Object) {
             msg = error.error;
           } 
           if(this.cancelRequest) {
             msg = str.upload_cancelled;
           }
           $('#upload-error').text(msg);
           $('#upload-error').show();
           this.uploadinit = false;
        },

        upload_started: function() 
        {
           this.enableHide(false);
           this.uploadinit = true;
           $('#btn-cancel').show();
           $('#btn-upload').hide();
           $('#upload-progress-now').css('width', "1%");
        },

        saveClicked: function (ev) {
            this.enableHide(true);
            if (this.options.success instanceof Function)
            {
                this.options.success(vdata);
            }
        },

        uploadVideo: function (ev) {
            $('#upload-error').hide();
            $('#video-support-feedback').hide();
            var thisThis = this,
            filelimit = 0;
            this.cancelRequest = false;
            if(this.availablespace <= 0) {
               filelimit = 0;                
               return;
            } else if(this.availablespace >= 500){
               filelimit = 500 * 1024 * 1024;                
            } else {
               filelimit = Math.round(this.availablespace * 1020 * 1024)
            }
            _Uploader.owner(this).showWarning(false).setFileUploadLimit(filelimit).restrictFileType("video/mp4,video/x-m4v,video/*").restrictFileExts(['flv', 'asf', 'qt', 'mov', 'mpg', 'mpeg', 'avi', 'wmv', 'm4v', '3gp', 'mp4', 'webm','ogg']).exec(
                function (data) { thisThis.upload_started() },
                function (data) { thisThis.upload_error(data) },
                function (data)
                {
                    if(thisThis.cancelRequest) {
                         console.log('Request cancelled');
                         $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_remove_tempfile.php?file="+data.filename, context: this, dataType: "json",
                           success: function ()
                           {
                           }, error: function ()
                           {
                           }});
                         return;
                    }
                    thisThis.origname = data.origname;
                    thisThis.filename = data.filename;
                    thisThis.filesize = data.filesize; 

                    thisThis.vjqXHR = $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_video_upload.php?file="+data.filename+"&filename="+data.origname, context: this, dataType: "json",
                     success: function (vdata)
                     {
                       if(thisThis.cancelRequest) {
                         return;
                        }
                        thisThis.vresid = vdata.id; 
                        thisThis.thumbnail = "/480x360/sky/text:"+thisThis.filename; 
                        thisThis.vdata = vdata;
                        thisThis.final_upload_complete();

                     }, error: function ()
                     {
                        thisThis.upload_error();
                     }, 
                        timeout: 600000 
                     });
                });  
        }
    });
});
