/**
 * Eliademy.com
 * 
 * @copyright CBTec Oy
 * @license All rights reserved
 */

define([ "i18n!nls/strings", "app/views/dialogview"],
  function(str, DialogView) {
    return DialogView
    .extend({
      data : null,
      name : '',
      url : '',
      pickerLoaded : false,
      session: false,
      picker: null, 
      dialogEvents : {},

      initialize : function() {
        this.data = this.options.data;
        gapi.client.setApiKey(GApiKey);
      },

      renderDialog : function() {
        var config = {
            'client_id' : GApiKey,
            'scope' : 'https://www.googleapis.com/auth/drive.readonly',
            'immediate' : this.session 
        };
        var thisThis = this;
        gapi.auth.authorize(config, function() {
          thisThis.loadPicker();
        });
      },
  
      // Use the API Loader script to load google.picker.
      loadPicker : function() {
        if (this.pickerLoaded) {
          this.createPicker();
        } else {
          gapi.load('picker', {
            'callback' : _
            .bind(this.createPicker, this)
          });
        }
      },
  
      // Create and render a Picker object for searching images.
      createPicker : function() {
        this.pickerLoaded = true;
        var pickerview = new google.picker.DocsView()
        .setMode(google.picker.DocsViewMode.LIST)
        .setIncludeFolders(true);
        this.picker = new google.picker.PickerBuilder()
        .enableFeature(google.picker.Feature.NAV_HIDDEN).setAppId(GApiKey).setOAuthToken(
            gapi.auth.getToken().access_token)
            .addView(pickerview).setCallback(
                _.bind(this.pickerCallback, this))
                .build();
        this.picker.setVisible(true);
      },
  
      pickerCallback : function(data) {
        var url = '';
        if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
          var doc = data[google.picker.Response.DOCUMENTS][0];
          this.url = doc[google.picker.Document.URL];
          this.name = doc[google.picker.Document.NAME];
          this.$el.empty();
          this.undelegateEvents();
          $("#dialog-overlay").hide().unbind("click");
          $(".modal-backdrop").unbind("click").remove();
          if (this.options.accepted instanceof Function) {
            this.options.accepted
            .apply(
                this.options.context ? this.options.context
                    : this, []);
          }
        } else if(data[google.picker.Response.ACTION] == google.picker.Action.CANCEL){
            this.cancelClicked();
        }
      },
  
      cancelClicked : function() {
        this.picker.setVisible(false);
        this.$el.empty();
        this.undelegateEvents();
        $("#dialog-overlay").hide().unbind("click");
        $(".modal-backdrop").unbind("click").remove();
      },
    });
});
