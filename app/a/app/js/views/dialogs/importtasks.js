/**
 * Eliademy.com
 * 
 * @package   magic_ui
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


define(["text!templates/dialogs/importtasks.html", "i18n!nls/strings", "app/views/dialogview",
   "app/collections/course/courses"],

    function (tpl, str, DialogView, CoursesCollection)
{
    
    return DialogView.extend(
    {
        
        template: _.template(tpl),
        
        renderDialog: function () {
            var model = CoursesCollection.where({ code: this.options.mCourseCode })[0];
            var tasksCount = model.get("assignments").length;
            this.$el.find(".ptr").hide();
            this.$body.html(this.template({ str: str, name: model.get("fullname"), taskcount: tasksCount, tasks: model.get("assignments") }));
            
            $('#import-course-tasks-next').click( function () {
                // save checkboxes to dom before dialog is eaten
                window.tempTaskCheckboxes = $('input[type="checkbox"]');
            });
            
        }
    });
});
