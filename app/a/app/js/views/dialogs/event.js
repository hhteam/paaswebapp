/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/event.html", "i18n!nls/strings", "app/views/dialogview",
        "app/tools/dataloader", "app/tools/inlineedit", "app/views/dialogs/messagebox", "app/tools/utils",
        "datepicker", "timepicker", "datepair"],
    function(tpl, str, DialogView, DataLoader, InlineEdit, MessageBox, Utils)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        dialogEvents: {
            "click [data-call]": Utils.callMapper
        },

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();
        },

        renderDialog: function()
        {
            var courseList = [ ],
                data = this.model.toJSON();

            this.options.courses.each(function (c)
            {
                courseList.push(c.toJSON());
            });

            this.$body.html(this.template(
            {
                str: str,
                courses: courseList,
                header: this.model.isNew() ? str.event_new2 : str.edit_event,
                currentType: this.model.get("courseid") || 0,
                is_new: this.model.isNew(),
                data: data,
                from_date: moment(parseInt(data.timestart || 0), "X").format("DD.MM.YYYY"),
                from_time: moment(parseInt(data.timestart || 0), "X").format("HH:mm"),
                to_date: moment(parseInt(data.timestart || 0) + parseInt(data.timeduration || 0), "X").format("DD.MM.YYYY"),
                to_time: moment(parseInt(data.timestart || 0) + parseInt(data.timeduration || 0), "X").format("HH:mm")
            }));

            this.inlineEdit.init(this);
            this.inlineEdit.start();

            $("#event-dialog-from-date-field").datepicker({ orientation: "auto top", autoclose: true, format: "dd.mm.yyyy" });
            $("#event-dialog-from-time-field").timepicker({ timeFormat: "H:i", step: 15 })
            $("#event-dialog-to-date-field").datepicker({ orientation: "auto top", autoclose: true, format: "dd.mm.yyyy" });
            $("#event-dialog-to-time-field").timepicker({ timeFormat: "H:i", step: 15 });

            $(this.$body).datepair();
        },

        validate: function ()
        {
            if (this.inlineEdit.commit())
            {
                this.commitTimes();

                return true;
            }
            else
            {
                if (!this.inlineEdit.triggerValidation())
                {
                    (new MessageBox({ message: this.inlineEdit.errorString, backdrop: false })).render();
                }

                return false;
            }
        },

        saveClicked: function ()
        {
            var type = parseInt($("#event-dialog-type-field").val());

            if (type)
            {
                this.model.set({ courseid: type, eventtype: "course" });
            }
            else
            {
                this.model.set({ courseid: 0, eventtype: "user" });
            }

            this.model.save();
        },

        commitTimes: function ()
        {
            var start = moment(($("#event-dialog-from-date-field").val() || moment().format("DD.MM.YYYY")) + " " +
                    ($("#event-dialog-from-time-field").val() || moment().format("HH:mm")), "DD.MM.YYYY HH:mm").format("X"),
                end = moment(($("#event-dialog-to-date-field").val() || moment().format("DD.MM.YYYY")) + " " +
                    ($("#event-dialog-to-time-field").val() || moment().format("HH:mm")), "DD.MM.YYYY HH:mm").format("X");

            this.model.set({ timestart: start, timeduration: end - start });
        },

        deleteEvent: function ()
        {
            this.closeDialog();
            this.model.destroy();
        }
    });
});
