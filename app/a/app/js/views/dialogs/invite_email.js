/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/dialogs/invite_email.html", "i18n!nls/strings", "app/models/course/invited_user", "app/views/dialogview",
        "app/models/gapp", "app/tools/utils","app/models/captcha"],
    function (tpl, str, InvitedUserModel, DialogView, GoogleAppModel, Utils, CaptchaModel)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        mEmails: [],
        mSummary: '',
        mShowSummary : false,

        renderDialog: function()
        {
            var thisThis = this;
            var userTags = [];

            if (this.options.userTags)
            {
                userTags = this.options.userTags.toJSON();
            }

            var dialogTitle = str.button_send_email_invitation;
            var summaryPlaceHolder = str.email_custom_text;
            var showtabs = true;
            if(this.options.code == 'eliademy') {
              summaryPlaceHolder = str.email_eliademy_invite;
              dialogTitle = str.invite_friends;
              showtabs = false;
            }
            this.$body.html(this.template({ str: str, dlgTitle: dialogTitle, gapp: this.options.gapp,
                    cohort: this.options.cohort, cohortSelfRegister: this.options.cohortselfregister,
                    cohortinvurl: this.options.cohortinvurl, userTags: userTags, userRoles: this.options.userRoles, summaryPlaceHolder: summaryPlaceHolder,
                    showtabs: showtabs}));

            $('#cohort-footer').hide();
            // Backbone isn't handling "change" event. Shame on Backbone.
            this.$("#invite-upload-btn").change(function ()
            {
                if (this.files.length > 0)
                {
                    thisThis.loadFile(this.files[0]);
                }
            });

            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                if('#share-cohort-link' == $(e.target).attr('href')) {
                   $('#invite-summary').hide();
                   $('#cohort-footer').show();
                   $('#send-invite').hide();
                   $('#inv-msg-label').hide();
                   $("#invite-dlg-tag-selector-block").hide();
                } else {
                   $('#invite-summary').show();
                   $('#inv-msg-label').show();
                   $('#cohort-footer').hide();
                   $('#send-invite').show();
                   $("#invite-dlg-tag-selector-block").show();
                }
            })

            //Do not even getting click event for dialog button!            
            this.$("#admin-add-gusers-btn").get(0).addEventListener("click", function()
            {
                GoogleAppModel.set({ id: thisThis.options.cid },{ silent: true }); 
                GoogleAppModel.fetch({
                   success:function(method,model,option)
                   {
                        thisThis.$("#invite-gemails-input").val(GoogleAppModel.get('emailids'));
                   },
               });
            });
        },
        
        diplayValidateCheckError : function(msg)
        {
            if (this.$("#invite-type-emails").hasClass("active"))
             {
                 this.$("#invite-error-type-box").text(msg);
                 this.$("#invite-error-type-box").show();
             }
             else if (this.$("#invite-import-csv").hasClass("active"))
             {
                 this.$("#invite-error-upload-box").text(msg);
                 this.$("#invite-error-upload-box").show()
             }
             else
             {
                 this.$("#invite-google-error-type-box").text(msg);
                 this.$("#invite-google-error-type-box").show();
             }
        },
        
        verifyCaptcha: function ()
        {
            return true;
            /* disabling captcha verification */
            /* var response = $("#captchaframe").contents().find("#adcopy_response").val();
            if(!response)
            {
                this.diplayValidateCheckError("captcha field can't be empty");
                return false;
            }
            var challenge = $("#captchaframe").contents().find("#adcopy_challenge").val();
            var captchaModel = new CaptchaModel({id:this.options.cid,response: response, challenge:challenge });
            var ret = captchaModel.validate();
            if(!ret)
            {
                var iframe = document.getElementById("captchaframe");
                //refresh captcha
                iframe.src = iframe.src;
                this.diplayValidateCheckError("captcha error, retry");
            }
            return ret; */
        },

        validate: function ()
        {
            var emails = "", i;
            this.mSummary = this.$("#invite-summary").val();
            if(this.options.code == 'eliademy' && !this.mSummary) {
              this.mSummary = str.email_eliademy_invite;
            }
 
            if (this.$("#invite-type-emails").hasClass("active"))
            {
                // Type emails.
                emails = this.$("#invite-emails-input").val();
            }
            else if(this.$("#invite-import-csv").hasClass("active"))
            {
                // Import CSV.
                emails = this.$("#invite-emails-read").val();
            }
            else
            {
                emails = this.$("#invite-gemails-input").val();
            }

            var words = [], validEmails = [], invalidEmails = [];
            if(emails)
            {
               words = emails.split(/(?:\s+|,|;)/).filter(function(el) {return el.length != 0});
            }

            for (i=words.length-1; i>=0; i--)
            {
                 var w = words[i];

                  if (w.substring(0, 1) == "<")
                  {
                      w = w.substring(1);
                  }

                  if (w.substring(w.length - 1) == ">")
                  {
                      w = w.substring(0, w.length - 1);
                  }

                  if (Utils.validateEmail(w))
                  {
                      validEmails.push(w);
                  }
                  else
                  {
                      invalidEmails.push(w);
                  }
            }

            if (validEmails.length > 0 && !invalidEmails.length)
            {
                this.mEmails = validEmails;

                return true;
            }
            
            this.diplayValidateCheckError(str.invitemailErr);

            return false;
        },

        saveClicked: function ()
        {
             // If response is expected e.g user_directory
            var rexp = (typeof this.options.parent !== 'undefined') ? true : false;
            var count = 0;
            var thisThis = this;
            var userTag = this.$("#invite-dlg-tag-selector").val();

            for (var i=this.mEmails.length-1; i>=0; i--)
            {
                var invitedUser = new InvitedUserModel({email: this.mEmails[i], invitedby: 0, invitedwhen: 0, userTag: userTag,
                        userRole: this.$("#invite-dlg-role-selector").val() });

                if(rexp) {
                    invitedUser.setId(this.options.cid).setCode(this.options.code).setSummary(this.mSummary).save( undefined, { success: function()
                    {
                        count = count + 1;
                        if(count == thisThis.mEmails.length) {
                            thisThis.options.parent.pageReset();
                        }
                        //Emails sent successfully
                    },
                    error: function(){
                        count = count + 1;
                        if(count == thisThis.mEmails.length) {
                            thisThis.options.parent.pageReset();
                        }
                    }});
                } else {
                    invitedUser.setId(this.options.cid).setCode(this.options.code).setSummary(this.mSummary).save();
                }
            }
        },

        loadFile: function (file)
        {
            var reader = new FileReader(),
                thisThis = this;

            reader.onload = function (ev)
            {
                var words = ev.target.result.split(/(\s+|,|;)/), emails = [];

                for (var i=words.length-1; i>=0; i--)
                {
                    // Picking emaily-looking words.
                    if (words[i].indexOf("@") != -1)
                    {
                        var w = words[i];

                        if (w.substring(0, 1) == "<")
                        {
                            w = w.substring(1);
                        }

                        if (w.substring(w.length - 1) == ">")
                        {
                            w = w.substring(0, w.length - 1);
                        }

                        //TODO: we should validate these too?
                        emails.push(w);
                    }
                }

                thisThis.$("#invite-emails-read").val(emails.join("; "));
            };

            reader.readAsText(file);
        }
    });
});
