/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/dialogs/send_message.html", "i18n!nls/strings",
        "app/models/course/send_message","app/tools/warnings", "app/views/dialogview",
        "app/tools/utils", "app/models/captcha"],
    function (tpl, str, SendMessageModel, WarningTool, DialogView, Utils, CaptchaModel)
{
    return DialogView.extend(
    {
        template: _.template(tpl),
        mSummary: '',

        renderDialog: function()
        {
            var thisThis = this;
            this.$body.html(this.template({ str: str }));
        },
        
        verifyCaptcha: function ()
        {
            return true;
            /* disabling captcha verification
            var response = $("#captchaframe").contents().find("#adcopy_response").val();
            if(!response)
            {
                //TODO: Show an alert that captcha field can not be empty
                return false;
            }
            var challenge = $("#captchaframe").contents().find("#adcopy_challenge").val();
            var captchaModel = new CaptchaModel({id:this.options.cid,response: response, challenge:challenge });
            var ret = captchaModel.validate();
            if(!ret)
            {
                var iframe = document.getElementById("captchaframe");
                //refresh captcha
                iframe.src = iframe.src;
                //TODO: Show an alert for captcha failure
            }
            return ret; */
        },

        validate: function ()
        {
            this.mSummary = this.$("#message-summary").val();
            if($.trim(this.mSummary).length == 0)
                return false;
            return true;
        },

        saveClicked: function ()
        {
            var thisThis = this;
            var sendMessage = new SendMessageModel({});
            sendMessage.setCourseId(this.options.courseid).setUserIds(this.options.userids).setSummary(this.mSummary).save();
            WarningTool.processWarnings({msgtype:'success', message:str.message_sent, timeout:10000});
        },

    });
});
