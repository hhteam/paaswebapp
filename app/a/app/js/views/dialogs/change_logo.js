/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/dialogs/change_logo.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/warnings",
        "app/collections/admin/cohorts", "app/tools/dataloader", "jqueryui"],
    function (tpl, str, DialogView, WarningTool, CohortCollection, DataLoader)
{
    return DialogView.extend(
    {
        template: _.template(tpl),
        
        dialogEvents:
        {
            "click #btn-default": "useDefault",
            "click #btn-upload": "uploadLogo"
        },

        renderDialog: function ()
        {
            var instruction = '',
                filelimit = '',
                hideDefault = false,
                clearLabel = str.logo_default;

            switch (this.options.context)
            {
                case "orglogo":
                    hideDefault = true;

                case "courselogo":
                    instruction = str.logo_info;
                    break;

                case "coursebackground":
                    instruction = str.picture_info;
                    filelimit = "2";
                    break;

                case "certlogo":
                    instruction = str.cert_logo_msg;
                    clearLabel = str.button_delete;
                    break;
            }
            this.$body.html(this.template({ str: str, instruction: instruction, filelimit: filelimit, hideDefault: hideDefault, clearLabel: clearLabel }));
        },

        useDefault: function (ev) {
            if (this.options.context == 'courselogo') {
                if (this.options.model && this.options.model.get("cohortid"))
                {
                    DataLoader.exec({ context: this, collection: CohortCollection, id: parseInt(this.options.model.get("cohortid")) }, function (cohort)
                    {
                        $('#course-logo-img').attr('src', cohort.get("cohorticonurl"));
                    });
                }
                else
                {
                    $('#course-logo-img').attr('src', RootDir + "/app/img/logo-course-title.png");
                }
            } else if (this.options.context == 'coursebackground') {
                $('#uploaded-background').attr('src', '');
                $('#uploaded-background-container').hide();
            }

            if (this.options.success instanceof Function)
            {
                if (this.options.context == "orglogo")
                {
                    this.options.success(RootDir + "app/img/logo-course-title.png");
                } else
                {
                    this.options.success('/');
                }
            }

            this.closeDialog();
        },
        
        uploadLogo: function (ev) {
            var thisThis = this,
                filelimit = undefined;
            if (thisThis.options.context == 'courselogo' || thisThis.options.context == "orglogo") {
                filelimit = 0.5 * 1024 * 1024;
            } else if (thisThis.options.context == 'coursebackground') {
                filelimit = 2 * 1024 * 1024
            }
            
            _Uploader.owner(this).restrictFileType('image/*').setFileUploadLimit(filelimit).exec(
                //add 
                function (filedata)
                {
                    if (thisThis.options.context == 'courselogo') {
                        $(".course-logo").prepend('<div class="progress active" id="progress-bar" style="margin-top:20px;margin-bottom:-40px"> <div class="bar" id="progress-now" style="width: 0%;"></div> </div>');
                        $('#course-logo-img').css('opacity', '.25');
                    } else if (thisThis.options.context == 'coursebackground') {
                        $("#course-background").prepend('<div class="progress active" id="progress-bar" style="margin-top:20px;margin-bottom:-40px"> <div class="bar" id="progress-now" style="width: 0%;"></div> </div>');
                        $("#course-background").css('background', 'none');
                    }
                },
                //error 
                function (data)
                {
                    console.warn("Unable to upload file: " + data.error);
                    $('#progress-bar').remove();
                    if (thisThis.options.context == 'courselogo') {
                        $('#course-logo-img').css('opacity', '');
                    }
                    $('#btn_edit_save').attr('disabled' , false);
                },
                //done
                function (filedata)
                {
                    var extension = filedata.filename.split('.').pop().toLowerCase();
                    if ( ['jpeg', 'jpg', 'png', 'gif'].indexOf(extension) == -1 ) {
                        WarningTool.processWarnings({msgtype:'error', message:str.picture_error});
                        $('#progress-bar').remove();                    
                        $('#btn_edit_save').attr('disabled' , false);
                        return;
                    }
                    
                    if (thisThis.options.context == 'courselogo') {
                        $('#course-logo-img').attr("src", MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + filedata.filename + '&update=' + new Date().getTime());
                        $('#course-logo-img').css('opacity', '');
                    } else if (thisThis.options.context == 'coursebackground') {
                        $('#uploaded-background').attr("src", MoodleDir + "theme/monorail/ext/ajax_get_file.php?filename=" + filedata.filename + '&update=' + new Date().getTime()).load(function() {
                            var newheight = Math.round(850 / this.width * this.height);
                            $('#uploaded-background-container').css('top', newheight <= 170 ? 0 : -(newheight - 170));
                            $('#uploaded-background-container').css('height', newheight <= 170 ? newheight : 2*newheight - 170);
                            $('#uploaded-background-container').show();
                            $("#uploaded-background").css("top", newheight > 170 ? Math.ceil((newheight - 170) / 2) : 0);
                        });
                        
                        $('#uploaded-background').draggable({
                            axis: "y",
                            cursor: "move",
                            containment: "parent"
                        });
                    }

                    $('#progress-bar').remove();                    
                    $('#btn_edit_save').attr('disabled' , false);
                    
                    if (thisThis.options.success instanceof Function)
                    {
                        thisThis.options.success(filedata.filename);
                    }
                });

            this.closeDialog();
        }
    });
});
