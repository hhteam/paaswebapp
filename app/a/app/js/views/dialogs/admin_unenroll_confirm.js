/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/admin_unenroll_confirm.html", "i18n!nls/strings", "app/views/dialogview"], function (tpl, str, DialogView)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        dialogEvents: {
            "keyup input.confirmation_input": "validateInput"
        },

        renderDialog: function ()
        {
            this.$body.html(this.template({ str: str, coursename: this.options.coursename }));
        },

        validateInput: function ()
        {
            if ($(".confirmation_input").val() === str.user_unenroll_confirmation) {
                $('#confirm-delete-button').removeClass('disabled');
                $('#confirm-delete-button').attr('data-dismiss', 'modal');
            } else {
                if (! $('#confirm-delete-button.disabled').length) {
                    $('#confirm-delete-button').addClass('disabled');
                    $('#confirm-delete-button').attr('data-dismiss', null);
                }
            }
        }
    });
});
