/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/textinput.html", "i18n!nls/strings", "app/views/dialogview"], function (tpl, str, DialogView)
{
    return DialogView.extend(
    {
        template: _.template(tpl),
        value: "",

        renderDialog: function ()
        {
            this.$body.html(this.template({
                str: str,
                label: this.options.label,
                header: this.options.header,
                value: this.options.value,
                button: this.options.button,
                placeholder: this.options.placeholder }));
        },

        validate: function ()
        {
            if (this.options.noempty && !this.$("textarea[name='textinput']").val())
            {
                this.$("textarea[name='textinput']").focus();
                this.$(".control-group").addClass("error");

                return false;
            }

            return true;
        },

        saveClicked: function ()
        {
            this.value = this.$("textarea[name='textinput']").val();
        }
    });
});
