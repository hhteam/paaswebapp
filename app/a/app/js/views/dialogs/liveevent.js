/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/liveevent.html", "i18n!nls/strings", "app/views/dialogview",
        "app/tools/dataloader", "app/tools/inlineedit", "app/views/dialogs/messagebox", "app/tools/utils",
        "datepicker", "timepicker", "datepair"],
    function(tpl, str, DialogView, DataLoader, InlineEdit, MessageBox, Utils)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        dialogEvents: {
            "click [data-call]": Utils.callMapper
        },

        initialize: function ()
        {
            this.inlineEdit = new InlineEdit();
        },

        renderDialog: function()
        {
            var data = this.model.toJSON();

            this.$body.html(this.template(
            {
                str: str,
                header: this.model.isNew() ? str.event_new2 : str.edit_event,
                is_new: this.model.isNew(),
                data: data,
                live_id: data.id,
                from_date: moment(parseInt(data.timeavailable || 0), "X").format("YYYY-MM-DD"),
                from_time: moment(parseInt(data.timeavailable || 0), "X").format("HH:mm"),
                to_date: moment(parseInt(data.timedue || 0), "X").format("YYYY-MM-DD"),     
                to_time: moment(parseInt(data.timedue || 0), "X").format("HH:mm") 
            }));

            this.inlineEdit.init(this);
            this.inlineEdit.start();

            $("#event-dialog-from-date-field").datepicker({ orientation: "auto top", autoclose: true, format: "yyyy-mm-dd" });
            $("#event-dialog-to-date-field").datepicker({ orientation: "auto top", autoclose: true, format: "yyyy-mm-dd" });
            $("#event-dialog-from-time-field").timepicker({ timeFormat: "H:i", step: 15 })
            $("#event-dialog-to-time-field").timepicker({ timeFormat: "H:i", step: 15 });
            $(".mui-dialog").css({ width: "340px", "margin-left": "-180px" });
        },

        validate: function ()
        {
            if (this.inlineEdit.commit())
            {
                return this.commitTimes();
            }
            else
            {
                if (!this.inlineEdit.triggerValidation())
                {
                    (new MessageBox({ message: this.inlineEdit.errorString, backdrop: false })).render();
                }

                return false;
            }
        },

        deleteSession: function(ev, target)
        {
            this.options.parent.deleteSession(ev, target);
            this.closeDialog();
        },

        commitTimes: function ()
        {
            this.$("#errorstr").hide();
            var start = moment(($("#event-dialog-from-date-field").val() || moment().format("YYYY-MM-DD")) + " " +
                    ($("#event-dialog-from-time-field").val() || moment().format("HH:mm")), "YYYY-MM-DD HH:mm").format("X"),
                end = moment(($("#event-dialog-to-date-field").val() || moment().format("YYYY-MM-DD")) + " " +
                    ($("#event-dialog-to-time-field").val() || moment().format("HH:mm")), "YYYY-MM-DD HH:mm").format("X");

            var diff = end - start;
            if((diff <= 0) || (diff > 10800)) {
               this.$("#errorstr").text(str.live_timeerror);
               this.$("#errorstr").show();
               return false;
            }
            this.model.set({ timeavailable: start, timedue: end });
            return true;
        },
    });
});
