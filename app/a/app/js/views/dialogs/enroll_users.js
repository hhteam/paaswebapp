/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/enroll_users.html", "i18n!nls/strings", "app/views/dialogview",
        "app/views/pages/admin/course_manager"],
function (tpl, str, DialogView, CourseManagerView)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        initialize: function () {
            this.adminPanel = CourseManagerView;
        },

        renderDialog: function ()
        {
            CourseManagerView.mCohortId = this.options.cohortid;
            CourseManagerView.$body = this.$body;
            CourseManagerView.baseTemplate = this.template;
            CourseManagerView.mDialog = true;
            CourseManagerView.mCourseId = this.options.courseid;
            CourseManagerView.setElement($('.mui-dialog-body'));
            CourseManagerView.pageRender();
            $('.mui-dialog').css('width', '570px');
        }
    });
});
