/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/dialogs/admin_new_ad.html", "i18n!nls/strings", "app/views/dialogview", "app/tools/dataloader",
        "app/collections/countries", "app/tools/utils", "app/tools/warnings"],
    function (tpl, str, DialogView, DataLoader, CountryCollection, Utils, WarningTool)
{
    return DialogView.extend(
    {
        template: _.template(tpl),

        dialogEvents: {
            "click [data-call]": Utils.callMapper
        },

        renderDialog: function () {
            DataLoader.exec({ context: this, collection: CountryCollection }, function (countries) {
                this.$body.html(this.template({ str: str, countries: countries.toJSON() }));
            });
        },

        saveClicked: function () {
            this.data = {
                url: this.$("[name=\"ad_url\"]").val(),
                group: this.$("[name=\"ad_group\"]").val(),
                country: this.$("[name=\"ad_country\"]").val(),
                cover: this.$("#public-page-ad-image").attr("src")
            }
        },

        setAdImage: function () {
            var thisThis = this;

            _Uploader.owner(this).restrictFileType('image/*').setFileUploadLimit(0.5 * 1024 * 1024).exec(
                function (filedata)     //add
                { },
                function (data)         //error
                { },
                function (filedata)     //done
                {
                    var extension = filedata.filename.split('.').pop().toLowerCase();

                    if (['jpeg', 'jpg', 'png', 'gif'].indexOf(extension) == -1)
                    {
                        WarningTool.processWarnings({msgtype:'error', message:str.picture_error});
                        return;
                    }

                    $.ajax({ url: MoodleDir + "theme/monorail/ext/ajax_set_public_file.php",
                            data: { filename: filedata.filename, prefix: "pic" + thisThis.options.adnum + "_", cohortid: thisThis.options.cohortid },
                            success: function (data) {
                                console.log(data);
                                thisThis.$("#public-page-ad-image").show().attr("src", data);
                            },
                            error: function (data) {
                                console.warn("Network error");
                            }
                    });
                });
        }
    });
});
