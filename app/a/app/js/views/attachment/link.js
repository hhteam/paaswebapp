/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/attachment/link.html", "app/tools/utils", "backbone"], function (tpl, Utils)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),

        events: {
            "click [data-call]": Utils.callMapper
        },

        render: function ()
        {
            var data = this.model.toJSON();
            data.mimetype = '';

            if (data.contents instanceof Array && data.contents.length > 0)
            {
                data.mimetype = data.contents[0].mimetype;

                switch (data.contents[0].type)
                {
                    case "file":
                        data.url = data.contents[0].fileurl.replace("?forcedownload=1", "").replace("webservice/", "");
                        break;

                    case "url":
                        data.url = data.contents[0].fileurl;
                        break;
                }
            }

            // Reassign iconmod (TODO: use Utils version)
            switch(data.modname)
            {
                case "resource":
                case "task":
                    switch(data.mimetype) {
                        case "application/pdf":
                            data.modicon = RootDir + '/app/img/icon-pdf.png';
                        break;

                        case "application/vnd.ms-excel":
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            data.modicon = RootDir + '/app/img/icon-excel.png';
                        break;

                        case "application/vnd.oasis.opendocument.spreadsheet":
                            data.modicon = RootDir + '/app/img/icon-spreadsheet.png';
                            break;

                        case "application/vnd.ms-powerpoint":
                        case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                            data.modicon = RootDir + '/app/img/icon-powerpoint.png';
                        break;

                        case "application/vnd.oasis.opendocument.presentation":
                            data.modicon = RootDir + '/app/img/icon-presentation.png';
                            break;

                        case "application/msword":
                        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                            data.modicon = RootDir + '/app/img/icon-word.png';
                        break;

                        case "text/plain":
                        case "text/rtf":
                        case "application/vnd.oasis.opendocument.text":
                            data.modicon = RootDir + '/app/img/icon-text.png';
                            break;

                        default:
                            data.modicon = RootDir + '/app/img/icon-download.png';
                        break;
                    }
                    break;
                case "lti" :
                    if(!data.modicon) {
                        data.modicon = RootDir + '/app/img/icon-link.png';
                    }                
                    break;
                case "url":
                    data.modicon = RootDir + '/app/img/icon-link.png';
                    if(data.url.indexOf('https://docs.google.com') != -1) {
                        data.modicon = RootDir + '/app/img/icon-googledrive.png';
                    } 
                    break;
                default:
                    data.modicon = RootDir + '/app/img/icon-link.png';
                    break;
            }

            this.$el.html(this.template({ data: data, attachmentPath: this.options.attachmentPath }));
            this.$el.parent().addClass('block');
        }
    });
});
