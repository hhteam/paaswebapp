/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/attachment/oembed.html", "backbone", "oembed"], function (tpl)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),

        render: function ()
        {
            var data = this.model.toJSON();

            this.$el.html(this.template({ data: data, cid: this.model.cid }));
            this.$el.parent().css({ "display": "inline-block" });

            $("a#oembed-"+this.model.cid).oembed();
        }
    });
});
