/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/attachment/image.html", "backbone"], function (tpl)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),

        render: function ()
        {
            var data = this.model.toJSON();

            if (this.model.isNew())
            {
                // No thumbnails for new items and pngs...
                data.previewUrl = data.url;
            }
            else
            {
                data.previewUrl = data.url +"?preview=small";
            }

            this.$el.html(this.template({ data: data, cid: this.model.cid }));
        }
    });
});
