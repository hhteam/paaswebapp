/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["text!templates/attachment/kalturavideo.html", "text!templates/attachment/kalturavideopreview.html", "backbone"], function (tpl, tpl2)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),
        template2: _.template(tpl2),

        render: function ()
        {
            if (this.model.isNew())
            {
                // Preview
                this.$el.html(this.template2({ name: this.model.get("filename") }));
            }
            else
            {
                // Real
                var entry_id = this.model.get("entry_id");

                if (!entry_id) {
                    entry_id = this.model.get("vresid");
                }

                if (!entry_id) {
                    entry_id = this.model.get("id");
                }

                var eurl = vidUrl + '/p/' + vidPartnerId +'/sp/'+vidPartnerId+'00/embedIframeJs/uiconf_id/'+vidPlayerId+'/partner_id/'+vidPartnerId+'?iframeembed=true&playerId=kaltura_player_'+new Date().getTime()+'&entry_id='+entry_id;

                this.$el.html(this.template({ url: eurl }));
            }
        }
    });
});
