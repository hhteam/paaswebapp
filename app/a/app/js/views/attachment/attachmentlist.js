/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["i18n!nls/strings", "text!templates/attachment/placeholder.html", "app/views/attachment/link", "app/views/attachment/image",
        "app/views/attachment/media", "app/views/attachment/oembed", "app/views/attachment/kalturavideo", "app/tools/linkhandler",
        "app/views/dialogs/new_link","app/views/dialogs/google_drive", "app/views/addbutton", "app/views/dialogs/ltitool",
        "app/views/dialogs/videoupload", "app/tools/utils", "app/tools/ajaxprogress"],

    function (str, tpl, LinkAttachment, ImageAttachment, MediaAttachment, OEmbedAttachment, KalturaVideoAttachment, LinkHandler,
        NewLinkDialog, GoogleDriveDialog, AddButton, LtiToolDialog, VideoUploadDialog, Utils, AjaxProgress)
{
    return Backbone.View.extend(
    {
        mEditMode: false,
        mRemovedAttachments: [ ],
        mAttachments: [ ],
        mAddButton: null,
        template: _.template(tpl),
        hasChanged: false,

        events: {
            "click .attachment-remove-button": "removeItem"
        },

        initialize: function ()
        {
            try
            {
                this.options.collection.bind("reset", this.render, this);
                this.options.collection.bind("add", this.render, this);
                this.options.collection.bind("remove", this.render, this);
            }
            catch (err)
            {
                console.warn("Attachment collection not set!");
            }

            if (this.options.source == 'task') {
                this.mAddButton = new AddButton({ notasks: 1 });
            } else if (this.options.sectionIndex) {
                this.mAddButton = new AddButton();
            }
        },

        markAsChanged: function()
        {
            this.hasChanged = true;
        },

        startEdit: function ()
        {
            this.hasChanged = false;
            this.options.collection.on('add remove', this.markAsChanged, this);
            this.mEditMode = true;
            this.updateControls();
            this.mRemovedAttachments = [ ];
            if (this.options.source == 'task' || this.options.sectionIndex) {
                this.mAddButton.render();
            }

            _.each(this.mAttachments, function (item)
            {
                if ("startEdit" in item)
                {
                    item.startEdit();
                }
            }, this);
        },

        saveEdit: function (opts)
        {
            var leaveEditMode = function ()
            {
                this.options.collection.off('add remove', this.markAsChanged, this);

                this.mEditMode = false;
                this.updateControls();

                if (this.options.source == 'task' || this.options.sectionIndex) {
                    this.mAddButton.remove();
                }
            };

            if (opts instanceof Object && opts.deferred instanceof Object && opts.deferred.then instanceof Function)
            {
                opts.deferred.done(_.bind(leaveEditMode, this));
            }
            else
            {
                console.warn("Attachment list leaving edit mode without deferred...");
                leaveEditMode.apply(this);
            }

            var numToSave = this.options.collection.length,
                numToDelete = this.mRemovedAttachments.length;

            var completeSave = function ()
            {
                if (opts instanceof Object && opts.success instanceof Function && !numToSave && !numToDelete)
                {
                    opts.success.apply(null);
                }
            };

            _.each(this.mAttachments, function (item)
            {
                if ("saveEdit" in item)
                {
                    item.saveEdit();
                }
            }, this);

            completeSave();

            this.options.collection.each(function (item)
            {
                item.save(undefined, { success: function()
                {
                    numToSave--;
                    completeSave();
                }});
            });

            _.each(this.mRemovedAttachments, function (m)
            {
                if (!m.isNew())
                {
                    m.destroy({ success: function ()
                    {
                        numToDelete--;
                        completeSave();
                    }});
                }
            });

            this.mRemovedAttachments = [ ];
        },

        cancelEdit: function ()
        {
            this.options.collection.off('add remove', this.markAsChanged, this);
            this.mEditMode = false;
            this.updateControls();
            if (this.options.source == 'task' || this.options.sectionIndex) {
                this.mAddButton.remove();
            }

            _.each(this.mAttachments, function (item)
            {
                if ("cancelEdit" in item)
                {
                    item.cancelEdit();
                }
            }, this);

            var newItems = [ ], oldItems = [ ];

            this.options.collection.each(function (item)
            {
                if (item.isNew())
                {
                    newItems.push(item);
                }
            });

            _.each(this.mRemovedAttachments, function (item)
            {
                if (!item.isNew())
                {
                    oldItems.push(item);
                }
            }, this);

            this.options.collection.remove(newItems);
            this.options.collection.add(oldItems);
        },

        upload_process: function (e, data) {
            // Disable Save button until the file is uploaded
            $('#btn_edit_save').attr('disabled' , true);

            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-bar .bar').css(
                'width',
                progress + '%'
            );
        },

        newFile: function (data)
        {
            var that = this;
            _Uploader.owner(this).restrictFileType(null).exec(
                //add
                function (filedata)
                {
                    var tar = that.$el.find("#delimiter-file");
                    //let's create a temp frame for showing progress indicator
                    $('<div class="temp-attachment-block"><span class="attachment-remove-button small-remove-button" style="display: block;"></span></div>')
                    .insertBefore(tar).append('<div class="progress" id="progress-bar"> <div class="bar" id="progress-now" style="width: 0%;"></div> </div>');

                },
                //error
                function (data)
                {
                    console.warn("Unable to upload file: " + data.error);
                    $('#btn_edit_save').attr('disabled' , false);
                },
                //done
                function (filedata)
                {
                    var d = { type: "file", filename: filedata.origname, name: filedata.filename };
                    _.extend(d, _.pick(data, "sectionid", "courseid", "taskid", "source"));
                    this.options.collection.add(d);
                    $('#btn_edit_save').attr('disabled' , false);
                });
        },

        ltiTool: function (data)
        {
            var dlg = new LtiToolDialog({ context: this, data: data, accepted: function ()
            {
                var d = { type: "lti", typeid: dlg.typeid, name: dlg.name, modicon: dlg.iconurl, modname: "lti",
                           externalurl: dlg.launchurl, id:dlg.moduleid  };

                _.extend(d, _.pick(data, "sectionid", "courseid", "cmid", "source"));
                this.options.collection.add(d);
            }});

            dlg.render();
        },

        videoUpload: function (data)
        {
            AjaxProgress.disableAjax();
            var dlg = new VideoUploadDialog({ context: this, data: data, accepted: function ()
            {
                AjaxProgress.enableAjax();
                var d = { type: "vfile", filename: dlg.origname, name: dlg.filename, filesize: dlg.filesize , vresid: dlg.vresid, vthumbnail: dlg.thumbnail };
                _.extend(d, _.pick(data, "sectionid", "courseid", "taskid", "source"));

                this.options.collection.add(d);
            }, rejected: function() {
               AjaxProgress.enableAjax();
            }});

            dlg.render();
        },

        newForum: function (data)
        {
            var d = { type: "forum", name: str.forum_new, modicon: RootDir + "app/img/forum-icon.gif" };

            _.extend(d, _.pick(data, "sectionid", "courseid", "taskid", "source"));

            this.options.collection.add(d);
        },

        newPicture: function (data)
        {
            var that = this;
            _Uploader.owner(this).restrictFileType('image/*').exec(
                //add
                function (filedata)
                {

                    var tar = that.$(".delimiter-media");
                    //let's create a temp frame for showing progress indicator
                    $('<div class="attachment-place temp-attachment-media" style="display: block; float: left;"><span class="attachment-remove-button" style="display: block;"></span><div class="img-polaroid" style="clear: both"><a class="colorbox-image"></a></div></div>')
                    .insertBefore(tar).append('<div class="progress" id="progress-bar"> <div class="bar" id="progress-now" style="width: 0%;"></div> </div>');

                },
                //error
                function (data)
                {
                    console.warn("Unable to upload file: " + data.error);
                    $('#btn_edit_save').attr('disabled' , false);
                },
                //done
                function (filedata)
                {
                    var d = { type: "file", filename: filedata.origname, name: filedata.filename };
                    _.extend(d, _.pick(data, "sectionid", "courseid", "taskid", "source"));
                    this.options.collection.add(d);
                    $('#btn_edit_save').attr('disabled' , false);
                }
            );
        },

        newLink: function (data)
        {
            var dlg = new NewLinkDialog({ context: this, data: data, accepted: function ()
            {
                var d = { type: "link", externalurl: dlg.url, name: dlg.url };

                _.extend(d, _.pick(data, "sectionid", "courseid", "taskid", "source"));

                this.options.collection.add(d);
            }});

            dlg.render();
        },

        googleDrive: function (data)
        {
            var dlg = new GoogleDriveDialog({ context: this, data: data, accepted: function ()
            {
                var d = { type: "link", externalurl: dlg.url, url: dlg.url, name: dlg.name, modname: 'url' };

                _.extend(d, _.pick(data, "sectionid", "courseid", "taskid", "source"));

                this.options.collection.add(d);
            }});

            dlg.render();
        },

        removeItem: function (ev)
        {
            var m = this.options.collection.get($(ev.target).attr("data-cid"));

            //If something added and removed without saving
            if (m instanceof Object && m.isNew instanceof Function && !m.isNew()) {
              this.mRemovedAttachments.push(m);
            }

            this.options.collection.remove(m);
        },

        removeAll: function () {
            this.options.collection.each(function (att) {
                //If something added and removed without saving
                if (att.isNew instanceof Function && !att.isNew()) {
                  this.mRemovedAttachments.push(att);
                }
            }, this);

            this.options.collection.reset();
        },

        updateControls: function ()
        {
            if (this.mEditMode)
            {
                $(".attachment-remove-button").show();
            }
            else
            {
                $(".attachment-remove-button").hide();
            }
        },

        setAddButton: function (addBtnItems) {
            this.mAddButton.setItems(addBtnItems);

            return this;
        },

        render: function ()
        {
            var html1 = "", html2 = "";

            this.options.collection.each(function (mod)
            {
                //Do not show inline attachments - contextualdata visible:false
                var attvisible = true;
                if (mod.get('contextualdata') && mod.get("type") != "kalvidres")
                {
                   var cdata = JSON.parse(mod.get('contextualdata'));
                   if (cdata.visible !== undefined)
                   {
                      attvisible = cdata.visible;
                   }
                }
                if (((!mod.has("visible")) || mod.get("visible")) && attvisible)
                {
                    switch (mod.get("type"))
                    {
                        case "image": case "youtube": case "contextualImage": case "slideshare": case "vimeo": case "kalvidres": case "vfile":
                            html1 += this.template({ cid: mod.cid, media:true });
                            break;

                        default:
                            html2 += this.template({ cid: mod.cid, media: false });
                    }
                }
            }, this);

            // will not load htmlAddButton if it exists
            var htmlAddButton = "", idAddButton;
            if (this.options.source == 'task' || this.options.sectionIndex) {
                // course overview
                if (!this.options.readOnly)
                {
                    if (this.options.section) {
                        htmlAddButton = '<div id="add-button-place-' + this.options.section.id + '" class="add-button-place"></div>';
                        idAddButton = "#add-button-place-" + this.options.section.id;
                    } else {
                        htmlAddButton = '<div id="add-button-place" class="add-button-place"></div>';
                        idAddButton = "#add-button-place";
                    }
                }
            }

            if (htmlAddButton || html2)
            {
                html2 = '<div class="attachmentlist">' + htmlAddButton + html2 + '<div style="clear: both;" id="delimiter-file"></div><input id="testdata-filename" style="display: none;" type="text" value=""></div>';
            }

            if (html1)
            {
                html1 = "<div data-utils=\"gallery\">" + html1 + "</div>";
            }

            this.$el.html(html1 + '<div style="clear: both;" class="delimiter-media"></div>' + html2);

            if (this.options.source == 'task' || this.options.sectionIndex) {
                this.mAddButton.setElement(idAddButton);

                if (this.mEditMode) {
                    this.mAddButton.render();
                }
            }

            Utils.setupPopups(this.$el);

            this.mAttachments = [ ];

            this.options.collection.each(function (mod)
            {
                var v = null, attPath = undefined;
                if (this.options.attachmentPath)
                {
                    attPath = this.options.attachmentPath + "|" + mod.cid;
                }
                //Do not show inline attachments - contextualdata visible:false
                var attvisible = true;
                if (mod.get('contextualdata'))
                {
                   var cdata = JSON.parse(mod.get('contextualdata'));
                   if (cdata.visible !== undefined)
                   {
                      attvisible = cdata.visible;
                   }
                }

                if ((!mod.has("visible")) || mod.get("visible") && (mod.get("modname") != "assign") && attvisible)
                {
                    switch (mod.get("type"))
                    {
                        case "link": case "forum": case "file": case "lti":
                            v = new LinkAttachment({ model: mod, el: "#attachment-" + mod.cid + "-place", attachmentPath: attPath });
                            break;
                        case "contextualImage":
                            v = new ImageAttachment({ model: mod, el: "#attachment-" + mod.cid + "-place", attachmentPath: attPath });
                            break;
                        case "image":
                            v = new ImageAttachment({ model: mod, el: "#attachment-" + mod.cid + "-place", attachmentPath: attPath });
                            break;

                        case "youtube":
                            v = new MediaAttachment({ model: mod, el: "#attachment-" + mod.cid + "-place", attachmentPath: attPath });
                            break;

                        case "vimeo":
                        	v = new MediaAttachment({ model: mod, el: "#attachment-" + mod.cid + "-place", attachmentPath: attPath });
                            break;

                        case "slideshare":
                            v = new OEmbedAttachment({ model: mod, el: "#attachment-" + mod.cid + "-place", attachmentPath: attPath });
                            break;

                        case "vfile":
                        case "kalvidres":
                            v = new KalturaVideoAttachment({ model: mod, el: "#attachment-" + mod.cid + "-place", attachmentPath: attPath });
                            break;

                        default:
                            console.warn("Unknown attachment type in view: " + mod.get("type"));
                    }

                    if (v) {
                        v.mEditMode = this.mEditMode;
                        v.render();
                        this.mAttachments.push(v);
                    }
                }
            }, this);

            if (this.options.inlineEdit)
            {
                this.options.inlineEdit.reInit(this.$el);
            }

            this.updateControls();

            LinkHandler.setupView(this);
        }
    });
});
