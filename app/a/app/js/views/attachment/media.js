/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["text!templates/attachment/media.html", "backbone", "colorbox"], function (tpl)
{
    return Backbone.View.extend(
    {
        template: _.template(tpl),

        render: function ()
        {
            var data = this.model.toJSON(),
                cid = this.model.cid;

            this.$el.html(this.template({ data: data, cid: cid}));
            this.$el.parent().css({ "display": "block", "float": "left" });

			switch (data.type)
			{
				case "youtube":
					// Add the thumbnail
					$("#colorbox-video-" + cid + " .media-thumbnail").attr('src', "http://img.youtube.com/vi/" + data.videoid + "/hqdefault.jpg");

                    $.ajax({ url: "https://gdata.youtube.com/feeds/api/videos/" + data.videoid + "?v=2", dataType: "text", success: function (gdata)
                    {
                        // XXX: some magic to determine if video can play
                        // on not-tube-you site.
                        if ($(gdata).find("yt\\:accessControl[action='autoPlay']").attr("permission") == "allowed")
                        {
                            $("#colorbox-video-" + cid).colorbox(
                                { html: "<iframe type=\"text/html\" width=\"640\" height=\"390\""
                                    + "src=\"http://www.youtube.com/embed/" + data.videoid + "?autoplay=1\" frameborder=\"0\"/>" });
                        }
                        else
                        {
					        $("#colorbox-video-" + cid).click(function (ev)
                            {
                                ev.preventDefault();

                                window.open("https://www.youtube.com/watch?v=" + data.videoid, "_blank");
                            });
                        }
                    }});

		            break;

				case "vimeo":
					// Add the thumbnail
					$.ajax({
	    				type: 'GET',
	    				url: 'https://vimeo.com/api/v2/video/' + data.videoid + '.json',
	    				dataType: 'jsonp',
	    				success: function(vdata) {
	    					$("#colorbox-video-" + cid + " .media-thumbnail").attr('src', vdata[0].thumbnail_large);
	                    }
	    			});

		            $("#colorbox-video-" + cid).colorbox(
		                { html: "<iframe type=\"text/html\" width=\"640\" height=\"360\""
		                    + "src=\"https://player.vimeo.com/video/" + data.videoid + "?autoplay=1\" frameborder=\"0\"/>" });
		            break;
			}
            $("#play-icon-"+cid).hover(
                function() {
                    $(this).attr('src', '../app/img/icon-play-hover.png');
                },
                function() {
                    $(this).attr('src', '../app/img/icon-play.png');
                }
            );
        }
    });
});
