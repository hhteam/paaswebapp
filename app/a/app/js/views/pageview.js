/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "backbone"], function (str)
{
    return Backbone.View.extend(
    {
        mActive: false,

		el: "#page-content",

        show: function ()
        {
            this.mActive = true;

            return this.render();
        },

        hide: function ()
        {
            this.mActive = false;

            if (this.hidePage instanceof Function)
            {
                this.hidePage();
            }

            return this;
        },

        render: function ()
        {
            $("#page-warnings-container").empty();

            if (!this.mActive)
            {
                return this;
            }

            if (this.pageTitle)
            {
                document.title = _.unescape(this.pageTitle instanceof Function ? this.pageTitle() : this.pageTitle).replace("&nbsp;", " ");
            }
            else
            {
                document.title = _.unescape(str.site_name).replace("&nbsp;", " ");
            }

            this.pageRender();

            return this;
        },
        
        showOverlay: function()
        {
        	$('#tutorial-overlay').show();
			$('#tutorial-overlay').on('click', function() {
				$('#tutorial-overlay').hide();
			});
        },
        
        clearOverlayContent: function()
        {
        	var content = '<div id="overlay-layer"></div>';
        	content += '<div id="closebutton">X</div>';
        	$('#tutorial-overlay').html(content);
        }
    });
});
