/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/collections/feed", "app/tools/utils"], function (FeedCollection, Utils)
{
    var NotificationView = Backbone.View.extend(
    {
        render: function ()
        {
            var count = FeedCollection.filter(function(item) {
                return item.get("read") === 0;
            }).length;

            $('#courses_ncount').text(count);

            if (!count)
            {
                Utils.hide($('#courses_ncount').get(0));
            }
            else
            {
                Utils.unhide($('#courses_ncount').get(0));
            }

            return this;
        },

        initialize: function ()
        {
            FeedCollection.bind("reset", this.render, this);

            return this;
        }
    });

    return NotificationView;
});
