/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/feed", "app/tools/servicecalls"], function (FeedModel, SC)
{
	return new (Backbone.Collection.extend(
	{
		model: FeedModel,

        count: 0,
        moreAvailable: true,

        sync: function (method, collection, options) {
            if (method == "read")
            {
                if (! this.moreAvailable && ! options.always) {
                    return;
                }
                var params = {
                    count: (typeof options.increment === "undefined" || options.increment) ? this.count + 5 : this.count,
                };
                var that = this;

                if (SC.getToken())
                {
                    return SC.call("local_monorailservices_get_feed", params, function (data)
                    {
                        var models = [];

                        if (data instanceof Object)
                        {
                            if (data.moreavailable) {
                                this.moreAvailable = true;
                            } else {
                                this.moreAvailable = false;
                            }
                            if (data.notifications instanceof Array) {
                                _.each(data.notifications, function (item)
                                {
                                    models.push(new FeedModel(item));
                                });
                            }
                        }
                        this.count = models.length;
                        this.reset(models);
                    }, that);
                }
            }
        },

        markAllRead: function ()
        {
            var that = this;
            $.ajax({ url: MoodleDir + "local/monorailfeed/ext/ajax_mark_all_read.php", success: function ()
            {
                that.fetch({ always: true, increment: false });
            }});
        },

	}));
});
