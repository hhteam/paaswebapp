/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/admin/usertagrelation"], function (UserTagRelationModel)
{
    return Backbone.Collection.extend(
    {
        model: UserTagRelationModel,

        sync: function (method, collection, options)
        {
            console.log("usertag relation model: " + method);
        }
    });
});
