/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/tools/servicecalls", "app/models/admin/usertag"], function (SC, UserTagModel)
{
    return new (Backbone.Collection.extend(
    {
        model: UserTagModel,

        sync: function (method, collection, options)
        {
            switch (method)
            {
                case "read":
                    return SC.call("local_monorailservices_get_usertags", { }, function (data)
                    {
                        var models = [];

                        if (data instanceof Array)
                        {
                            _.each(data, function (tag)
                            {
                                models.push(new UserTagModel(tag));
                            });
                        }

                        this.reset(models);
                    }, this);
            }
        },

        comparator: function (item)
        {
            return item.get("name").toLowerCase();
        }
    }));
});
