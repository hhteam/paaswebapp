/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/admin/enrollment_analytics"], function (EnrollmentAnalyticsModel)
{
    return new (Backbone.Collection.extend(
    {
        model: EnrollmentAnalyticsModel
    }));
});
