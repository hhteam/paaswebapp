/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/admin/user_analytics"], function (UserAnalyticsModel)
{
    return new (Backbone.Collection.extend(
    {
        model: UserAnalyticsModel
    }));
});
