/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/admin/cohort"], function (CohortModel)
{
    return new (Backbone.Collection.extend(
    {
        model: CohortModel
    }));
});
