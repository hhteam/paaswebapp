/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/admin/invoice", "app/tools/servicecalls"], function (InvoiceModel, SC)
{
    return Backbone.Collection.extend(
    {
        sync: function (method, collection, options)
        {
            switch (method)
            {
                case "read":
                    return SC.call("local_monorailservices_get_invoices", { cohortid: this.mCohortId }, function (data)
                    {
                        var models = [];

                        if (data instanceof Object && data.invoices instanceof Array)
                        {
                            for (var i=0; i<data.invoices.length; i++)
                            {
                                models.push(new InvoiceModel(data.invoices[i]));
                            }
                        }

                        this.reset(models);
                    }, this);
                    break;

                default:
                    console.warn("TODO: " + method + " in invoice collection.");
            }
        },

        model: InvoiceModel,

        mCohortId: 0,

        setCohort: function (id) {
            this.mCohortId = id;
        }
    });
});
