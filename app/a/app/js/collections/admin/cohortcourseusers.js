/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/admin/cohortcourseuser", "app/tools/servicecalls"], function (CohortCourseUserModel, SC)
{
    return Backbone.Collection.extend(
    {
        sync: function (method, collection, options)
        {
            switch (method)
            {
                case "read":
                    return SC.call("local_monorailservices_cohort_course_users", { cohortid: collection.cohortid, courseid: collection.courseid }, function (data)
                    {
                        var models = [];

                        // TODO: neusers not used. remove from api.

                        if (data instanceof Object && data.eusers instanceof Array)
                        {
                            _.each(data.eusers, function (user)
                            {
                                models.push(new CohortCourseUserModel(_.extend({ cohortid: collection.cohortid, courseid: collection.courseid }, user)));
                            });
                        }

                        this.reset(models);
                    }, this);
                    break;

                default:
                    console.warn("TODO: " + method + " in cohort course user collection.");
            }
        },

        // Backbone doesn't know how to save collections, making custom method.
        save: function(options)
        {
            var newModels = [];

            _.each(this.models, function (model)
            {
                if (!model.get("cohortid"))
                {
                    newModels.push(
                    {
                        rolename: model.get("rolename"),
                        inviteid: model.get("inviteid"),
                        courseid: model.get("courseid"),
                        userid: model.get("id"),
                        cohortid: this.cohort.get("id")
                    });

                    model.set({ cohortid: this.cohort.get("id") }, { silent: true });
                }
            }, this);

            if (newModels.length > 0)
            {
                SC.call("local_monorailservices_cohort_course_enrol", { enrolments: newModels }, function (ret)
                {
                    if (options && options.success instanceof Function)
                    {
                        options.success();
                    }
                });
            }
            else
            {
                if (options && options.success instanceof Function)
                {
                    options.success();
                }
            }
        },

        removeAndSave: function (list, options)
        {
            if (list.length > 0)
            {
                var items = [ ];

                _.each(list, function (user)
                {
                    items.push({ courseid: user.get("courseid"), userid: user.get("id") });
                }, this);


                SC.call("local_monorailservices_unenrol_users", { enrolments: items }, function (ret)
                {
                    if (options && options.success instanceof Function)
                    {
                        options.success();
                    }
                });

                this.remove(list);
            }
            else
            {
                if (options && options.success instanceof Function)
                {
                    options.success();
                }
            }
        },

        model: CohortCourseUserModel
    });
});
