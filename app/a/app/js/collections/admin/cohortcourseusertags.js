/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/admin/cohortcourseusertag", "app/tools/servicecalls"], function (CohortCourseUserTagModel, SC)
{
    return Backbone.Collection.extend(
    {
        sync: function (method, collection, options)
        {
            switch (method)
            {
                case "read":
                    return SC.call("local_monorailservices_cohort_crs_usrts", { courseid: collection.courseid }, function (data)
                    {
                        var models = [];

                        if (data instanceof Array)
                        {
                            for (var i=0; i<data.length; i++)
                            {
                                models.push(new CohortCourseUserTagModel(data[i]));
                            }
                        }

                        this.reset(models);
                    }, this);
                    break;

                default:
                    console.warn("TODO: " + method + " in cohort course usertag collection.");
            }
        },

        model: CohortCourseUserTagModel
    });
});
