/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/admin/cohortuser", "app/tools/servicecalls"], function (CohortUserModel, SC)
{
    return Backbone.Collection.extend(
    {
        sync: function (method, collection, options)
        {
            switch (method)
            {
                case "read":
                    return SC.call("local_monorailservices_get_cohort_users", { cohortid: collection.cohortid }, function (data)
                    {
                        var models = [];

                        if (data instanceof Object)
                        {
                            // Normal users
                            // XXX: is it used anywhere?
                            if (data.users instanceof Array)
                            {
                                _.each(data.users, function (usr)
                                {
                                    var ustate = usr.admin ? 4 : 1;
                                    if(usr.suspended) {
                                       ustate = 2; 
                                    }
                                    models.push(new CohortUserModel(_.extend({ cohortid: collection.cohortid, state: ustate }, usr), { cohort: this.cohort }));
                                }, this);

                            }

                            // Invited users
                            if (data.invitedusers instanceof Array)
                            {
                                for (var i=0; i<data.invitedusers.length; i++) {
                                    var email = data.invitedusers[i].email.trim();

                                    //Use email address as id , as id shld be unique only them model is added to collection 
                                    models.push(new CohortUserModel({
                                        id: email,
                                        cohortid: collection.cohortid,
                                        state: 3,
                                        email: email,
                                        "status": data.invitedusers[i]["status"],
                                        dateinvited: data.invitedusers[i].date
                                    }, { cohort: this.cohort }));
                                }
                            }
                        }

                        this.reset(models);
                    }, this);
                    break;
            }
        },

        comparator: function (item)
        {
            return item.get("email").toLowerCase();
        },

        model: CohortUserModel
    });
});
