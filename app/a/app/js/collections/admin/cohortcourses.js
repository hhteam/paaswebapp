/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/admin/cohortcourse", "app/tools/servicecalls"], function (CohortCourseModel, SC)
{
    return Backbone.Collection.extend(
    {
        sync: function (method, collection, options)
        {
            switch (method)
            {
                case "read":
                    return SC.call("local_monorailservices_cohort_courses", { cohortid: collection.cohortid }, function (data)
                    {
                        var models = [];

                        if (data instanceof Object && data.courses instanceof Array)
                        {
                            _.each(data.courses, function (course)
                            {
                                models.push(new CohortCourseModel(_.extend({ cohortid: collection.cohortid }, course), { cohort: this.cohort }));
                            }, this);
                        }

                        this.reset(models);
                    }, this);
                    break;

                default:
                    console.warn("TODO: " + method + " in cohort course model.");
            }
        },

        comparator: function (item)
        {
            return item.get("fullname").toLowerCase();
        },

        model: CohortCourseModel
    });
});
