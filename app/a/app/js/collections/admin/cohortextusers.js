/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/admin/cohortuser", "app/tools/servicecalls"], function (CohortUserModel, SC)
{
    return Backbone.Collection.extend(
    {
        sync: function (method, collection, options)
        {
            switch (method)
            {
                case "read":
                    return SC.call("local_monorailservices_get_cohort_extusers", { cohortid: collection.cohortid }, function (data)
                    {
                        var models = [];

                        if (data instanceof Object)
                        {
                            // Normal users
                            if (data.users instanceof Array)
                            {
                                _.each(data.users, function (usr)
                                {
                                    ustate = 6; 
                                    models.push(new CohortUserModel(_.extend({ cohortid: collection.cohortid, state: ustate }, usr), { cohort: this.cohort }));
                                }, this);
                            }

                        }
                        this.reset(models);
                    }, this);
                    break;
            }
        },

        comparator: function (item)
        {
            return item.get("firstname").toLowerCase();
        },

        model: CohortUserModel
    });
});
