/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/landingstrings"], function (LandingStringsModel)
{
    return new (Backbone.Collection.extend(
    {
        model: LandingStringsModel
    }));
});
