/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/sharereminder"], function (ShareReminderModel)
{
    return new (Backbone.Collection.extend(
    {
        model: ShareReminderModel
    }));
});
