/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/forum/forum", "app/tools/servicecalls", "app/collections/forum/discussions"],
    function (ForumModel, SC, DiscussionCollection)
{
    /* Forums from a given course.
     * */
    return Backbone.Collection.extend(
    {
        mCourseId: null,

        maxAge: 600,

        setCourseId: function (id)
        {
            this.mCourseId = id;

            return this;
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    if (this.mCourseId)
                    {
                        return SC.call("local_monorailservices_forums", { courseid: this.mCourseId }, function (data)
                        {
                            if (data instanceof Object)
                            {
                                var that = this;

                                require(["app/collections/course/courses"], function (CourseCollection)
                                {
                                    var models = [];

                                    if (data.forums instanceof Array)
                                    {
                                        for (var i=0; i<data.forums.length; i++)
                                        {
                                            models.push(new ForumModel(data.forums[i]));

                                            for (var j=0; j<data.forums[i].discussions.length; j++)
                                            {
                                                DiscussionCollection.add(data.forums[i].discussions);
                                            }
                                        }
                                    }

                                    if (data.recent_posts instanceof Array)
                                    {
                                        CourseCollection.get(that.mCourseId).get("recent_posts").reset(data.recent_posts);
                                    }
                                    
                                    if (data.recent_discussions instanceof Array)
                                    {
                                        CourseCollection.get(that.mCourseId).get("recent_discussions").reset(data.recent_discussions);
                                    }

                                    that.reset(models);
                                });
                            }
                        }, this);
                    }
                    break;
            }
        },

        model: ForumModel
    });
});
