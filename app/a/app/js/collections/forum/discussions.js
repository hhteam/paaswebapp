/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/forum/discussion"], function (DiscussionModel)
{
    /* All discussions from all forums
     * */
    return new (Backbone.Collection.extend(
    {
        model: DiscussionModel
    }));
});
