/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/forum/discussion"], function (DiscussionModel)
{
    /* Recent discussions from all forums
     * */
    return Backbone.Collection.extend(
    {
        model: DiscussionModel
    });
});
