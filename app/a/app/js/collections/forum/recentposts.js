/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/forum/post"], function (PostModel)
{
    /* Recent posts from all forums
     * */
    return Backbone.Collection.extend(
    {
        model: PostModel
    });
});
