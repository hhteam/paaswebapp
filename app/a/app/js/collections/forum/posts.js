/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/forum/post", "app/tools/servicecalls"], function (PostModel, SC)
{
    /* Posts from a given discussion.
     * */
    return Backbone.Collection.extend(
    {
        mDiscussionId: null,

        maxAge: 600,    // Posts must be re-fetched every 10 min.

        setDiscussionId: function (id)
        {
            this.mDiscussionId = id;

            return this;
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    if (this.mDiscussionId)
                    {
                        return SC.call("local_monorailservices_forum_posts", { discussionid: this.mDiscussionId }, function (data)
                        {
                            var models = [];

                            if (data instanceof Array)
                            {
                                for (var i=0; i<data.length; i++)
                                {
                                    var mod = new PostModel(data[i]), att = [];

                                    if (data[i].attachments instanceof Array)
                                    {
                                        for (var j=0; j<data[i].attachments.length; j++) {
                                            att.push({ name: data[i].attachments[j].filename, id: data[i].attachments[j].fileid });
                                        }
                                    }

                                    mod.set("new_attachments", att);
                                    models.push(mod);
                                }
                            }

                            this.reset(models);
                        }, this);
                    }
                    break;
            }
        },

        model: PostModel
    });
});
