/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define([ "app/models/ltitool", "app/tools/servicecalls" ], function(
    LtiToolModel, SC) {
  return new (Backbone.Collection.extend({
    sync : function(method, collection, options) {
      if (method == "read") {
        return SC.call("local_monorailservices_get_ltitools", {},
            function(data) {
          var models = [];

          if (data instanceof Object
              && data.ltitools instanceof Array) {
            _.each(data.ltitools, function(ltitool) {
              models.push(new LtiToolModel(ltitool));
            });
          }

          this.reset(models);
        }, this);
      }
    },

    model : LtiToolModel
  }));
});

