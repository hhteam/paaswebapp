/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["backbone"], function ()
{
    return new (Backbone.Collection.extend(
    {
        sync: function (method, model, options)
        {
            switch (method) {
                case "read":
                    model.reset();
                    break;
            }
        }
    }));
});
