/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/task/task", "app/tools/servicecalls"], function (TaskModel, SC)
{
    // All tasks from a single course
	return Backbone.Collection.extend(
	{
        mCourseId: null,

        setCourseId: function (id)
        {
            this.mCourseId = id;
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    if (this.mCourseId)
                    {
                        return SC.call("local_monorailservices_get_user_tasks", { courseids: [ this.mCourseId ] }, function (data)
                        {
                            if (data.tasks instanceof Object && data.tasks instanceof Array && data.tasks.length > 0 &&
                                    data.tasks[0] instanceof Object && data.tasks[0].quizes instanceof Array && data.tasks[0].assignments instanceof Array) {

                                var items = data.tasks[0].assignments.concat(data.tasks[0].quizes);

                                items.sort(function (a, b) {
                                    if (a.duedate) {
                                        if (b.duedate) {
                                            return a.duedate - b.duedate;
                                        } else {
                                            return -1;
                                        }
                                    } else {
                                        if (b.duedate) {
                                            return 1;
                                        } else {
                                            return a.name == b.name ? 0 :
                                                (a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1);
                                        }
                                    }
                                });

                                this.reset(items);
                            } else {
                                // No tasks?
                                this.reset();
                            }
                        }, this);
                    }
                    break;

                default:
                    console.warn("Unhandled method " + method + " in course task collection.");
            }
        },

		model: TaskModel
	});
});
