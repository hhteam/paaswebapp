/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/task/question"], function (QuestionModel)
{
	return Backbone.Collection.extend(
	{
		model: QuestionModel,

        validate: function ()
        {
            var err, i = 0, c = this.models.length, question;

            for (; i<c; i++)
            {
                question = this.models[i];

                err = question.validate(question.attributes);

                if (err)
                {
                    return err;
                }
            }
        }
	});
});
