/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["i18n!nls/strings", "app/models/task/answer"], function (str, AnswerModel)
{
	return Backbone.Collection.extend(
	{
		model: AnswerModel,

        validate: function ()
        {
            var err, i = 0, c = this.models.length, a_correct = 0, answer;

            for (; i<c; i++)
            {
                answer = this.models[i];

                err = answer.validate(answer.attributes);

                if (err)
                {
                    return err;
                }

                if (answer.attributes.correct)
                {
                    a_correct++;
                }
            }

            if (a_correct == 0)
            {
                return str.quiz_error_select;
            }
        }
	});
});
