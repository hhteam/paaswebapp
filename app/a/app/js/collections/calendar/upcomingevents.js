/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/calendar/event", "app/tools/servicecalls", "moment"], function (EventModel, SC, moment)
{
    return new (Backbone.Collection.extend(
    {
        initialize: function ()
        {
            this.mCurrentTime = parseInt(moment().format("X"));
            this.on("reset", function () { this._isDirty = true; }, this);
        },

        maxAge: 600,

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_user_events", { timestart: this.mCurrentTime, untiltime: this.mCurrentTime + 3628800}, function (res)
                    {
                        var models = [], i;

                        if (res instanceof Object && res.events instanceof Array)
                        {
                            for (i=0; i<res.events.length; i++)
                            {
                                models.push(new EventModel(res.events[i]));
                            }
                        }

                        this.reset(models);
                        this._isDirty = false;
                    }, this);
                    break;

                default:
                    console.warn("Unimplemented upcoming event collection method: " + method);
            }
        },

        model: EventModel
    }));
});
