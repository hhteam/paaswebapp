/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/calendar/month"], function (MonthModel)
{
    /* Event data for all months. */
    return new (Backbone.Collection.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                default:
                    console.warn("Unimplemented calendar month collection method: " + method);
            }
        },

        model: MonthModel
    }));
});
