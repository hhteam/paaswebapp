/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/calendar/event"], function (EventModel)
{
    return Backbone.Collection.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                default:
                    console.warn("Unimplemented calendar event collection method: " + method);
            }
        },

        model: EventModel
    });
});
