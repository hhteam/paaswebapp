/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define( ["app/models/country", "app/tools/servicecalls"],
function( CountryModel,         SC )
{
    return new (Backbone.Collection.extend(
    {
        comparator: function (m1, m2) {
            return m1.attributes.value.localeCompare( m2.attributes.value );
        },

        sync: function (method, collection, options) {
            if( method != "read" )
                return;

            return SC.call( "local_monorailservices_get_countries", { },
                                                            function( data ) {
                if( data instanceof Object && data.countries instanceof Array )
                    this.reset( data.countries );
            }, this );
        },

        model: CountryModel
    }));
});
