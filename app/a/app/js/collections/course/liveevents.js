/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/course/liveevent", "app/tools/servicecalls", "app/tools/utils"], 
function (LiveEventModel, SC, Utils)
{
    return Backbone.Collection.extend(
    {
        mCourseId: null,

        setCourseId: function (id)
        {
            this.mCourseId = id;

            return this;
        },
        
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    if (!this.mCourseId)
                    {
                        console.warn("Course id not set for collection.");
                        return;
                    }

                    return SC.call('local_monorailservices_get_bbb_events', { courseid: this.mCourseId }, function (resp)
                    {
                        var data = resp.events;
                        if (data.length > 0)
                        {
                            var bbbsessions = [ ];
                            
                            _.each(data, function (bbb)
                            {
                                var iModel = new LiveEventModel(
                                {
                                  id: bbb.id,
                                  name: bbb.name,
                                  description: bbb.description,
                                  welcome: bbb.welcome,
                                  timedue: Math.floor(Utils.stringToDate(bbb.timedue).getTime() / 1000),
                                  timeavailable: Math.floor(Utils.stringToDate(bbb.timeavailable).getTime() / 1000),
                                });
                                bbbsessions.push(iModel);
                            }, this);
                            model.reset(bbbsessions);
                        }
                        else
                        {
                            model.reset();
                        }
                    }, this);
                    break;

                default:
                    console.warn("TODO: " + method + " for live sessions.");
            }
        },

        model: LiveEventModel
    });
});
