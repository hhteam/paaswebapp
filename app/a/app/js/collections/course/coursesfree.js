/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/courseshort", "app/tools/servicecalls"],
    function (CourseShortModel, SC)
{
    return new (Backbone.Collection.extend(
    {
        comparator: function (item)
        {
            return item.get("fullname").toLowerCase();
        },

        initialize: function ()
        {
            this.on("reset", function () { this._isDirty = true; }, this);
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_cc_courses", { }, function (data)
                    {
                        if (data instanceof Object && data.courses instanceof Array) {
                            this.reset(data.courses);
                            this._isDirty = false;
                        }
                    }, this);
                    break;

                default:
                    console.warn("Unhandled method " + method + " in short course collection");
            }
        },

        model: CourseShortModel
    }));
});
