/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/course/grades"], function (GradesModel)
{
    return new (Backbone.Collection.extend(
    {
        model: GradesModel
    }));
});
