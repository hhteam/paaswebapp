/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "app/models/course/cert_order"], function (SC, CertOrderModel)
{
    return new (Backbone.Collection.extend(
    {
        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_cert_orders", { }, function (ret)
                    {
                        model.reset(ret.orders);
                    }, this);
                    break;

                default:
                    console.warn("Unhandled method in cert order collection: " + method);
            }
        },

        model: CertOrderModel
    }));
});
