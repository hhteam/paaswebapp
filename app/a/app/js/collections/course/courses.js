/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/course"], function (CourseModel)
{
	return new (Backbone.Collection.extend(
	{
		model: CourseModel
	}));
});
