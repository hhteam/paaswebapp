/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/invited_user", "app/tools/servicecalls"],
function (InvitedUserModel, SC)
{
    return Backbone.Collection.extend(
    {
        mId: null,
        mType: 'course',

        setType: function (type)
        {
            this.mType = type;

            return this;
        },

        setId: function (id)
        {
            this.mId = id;

            return this;
        },

        mCode: null,

        setCode: function (code)
        {
            this.mCode = code;

            return this;
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    if (!this.mId)
                    {
                        console.warn("Target id not set for invited user collection.");
                        return;
                    }

                    return SC.call('local_monorailservices_get_invited', { id: this.mId, code: this.mCode, type: this.mType }, function (data)
                    {
                        if (data instanceof Array && data.length > 0)
                        {
                            model.reset(data);
                        }
                        else
                        {
                            model.reset();
                        }
                    }, this);
                    break;

                default:
                    console.warn("TODO: " + method + " for invited user collection.");
            }
        },

        model: InvitedUserModel
    });
});
