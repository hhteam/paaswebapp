/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/category", "app/tools/servicecalls"], function (CategoryModel, SC)
{
    return new (Backbone.Collection.extend(
    {
        sync: function (method, collection, options)
        {
            if (method == "read")
            {
                return SC.call("local_monorailservices_getcrs_categories", { }, function (data)
                {
                    var models = [];
                    if (data instanceof Object && data.categories instanceof Array)
                    {
                        _.each(data.categories, function (category)
                        {
                            models.push(new CategoryModel(category));
                        });
                    }
                    this.reset(models);
                }, this);
            }
        },

        model: CategoryModel
    }));
});
