/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/material"], function (MaterialModel)
{
    return new (Backbone.Collection.extend(
    {
        model: MaterialModel
    }));
});
