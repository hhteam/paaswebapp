/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/course/section", "app/tools/servicecalls"],
    function (SectionModel, SC)
{
    return Backbone.Collection.extend(
    {
        mCourseId: null,

        setCourseId: function (id)
        {
            this.mCourseId = id;

            return this;
        },

        sync: function (method, model, options)
        {
            if (this.mCourseId) {
                if (method == "read")
                {
                    SC.call("local_monorailservices_course_get_cont", { courseid: this.mCourseId }, function (data)
                    {
                        var items = [ ];

                        _.each(data, function (sec)
                        {
                            var m = new SectionModel({ id: sec.id, courseid: this.mCourseId, name: sec.name, summary: sec.summary, visible: sec.visible, section: sec.section }),
                                attach = m.get("attachments"),
                                tasks = [];

                            _.each(sec.modules, function (mod) {
                                // XXX: ignoring certain types of modules.
                                switch (mod.modname) {
                                    case "forum": break;

                                    case "assign": case "quiz":
                                        tasks.push(mod);
                                        break;

                                    default:
                                        mod.source = "course_modules";
                                        attach.add(mod);
                                }
                            });

                            m.set("tasks", tasks);
                            items.push(m);
                        }, this);

                        model.reset(items);
                    }, this);
                }

                return this;
            }
        },

        model: SectionModel
    });
});
