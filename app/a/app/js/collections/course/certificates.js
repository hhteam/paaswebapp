/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/certificate"], function (CertificateModel)
{
    return Backbone.Collection.extend(
    {
        model: CertificateModel
    });
});
