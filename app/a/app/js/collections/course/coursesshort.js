/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/courseshort", "app/tools/servicecalls"],
    function (CourseShortModel, SC)
{
    var Collection = Backbone.Collection.extend(
    {
        comparator: function (item)
        {
            return item.get("fullname").toLowerCase();
        },

        initialize: function ()
        {
            this.on("reset", function () { this._isDirty = true; }, this);
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    SC.call("local_monorailservices_get_user_courses", { isteacher: this.isTeacher, isactive: this.isActive }, function (data)
                    {
                        if (data instanceof Object && data.courses instanceof Array)
                        {
                            this.reset(data.courses);
                            this._isDirty = false;
                        }
                    }, this);
                    break;

                default:
                    console.warn("Unhandled method " + method + " in short course collection");
            }
        },

        model: CourseShortModel
    });

    var Instances = {
        LearningOngoing: new Collection(),
        LearningCompleted: new Collection(),
        TeachingOngoing: new Collection(),
        TeachingCompleted: new Collection()
    };

    Instances.LearningOngoing.isTeacher = 0;
    Instances.LearningOngoing.isActive = 1;

    Instances.LearningCompleted.isTeacher = 0;
    Instances.LearningCompleted.isActive = 0;

    Instances.TeachingOngoing.isTeacher = 1;
    Instances.TeachingOngoing.isActive = 1;

    Instances.TeachingCompleted.isTeacher = 1;
    Instances.TeachingCompleted.isActive = 0;

    return Instances;
});
