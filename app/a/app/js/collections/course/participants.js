/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/course/participant", "app/tools/servicecalls"],
function (ParticipantModel, SC)
{
    return Backbone.Collection.extend(
    {
        mCourseId: null,

        setCourseId: function (id)
        {
            this.mCourseId = id;

            return this;
        },

        sync: function (method, model, options)
        {
            switch (method)
            {
                case "read":
                    if (this.mCourseId) {
                        return SC.call("local_monorailservices_enrolled_users", { courseid: this.mCourseId }, function (data)
                        {
                            if (data.length > 0)
                            {
                                var users = [ ];

                                _.each(data, function (user)
                                {
                                    var uModel = new ParticipantModel(
                                    {
                                        id: user.id,
                                        courseid: this.mCourseId,
                                        fullname: user.fullname,
                                        profilepichash: user.profilepichash,
                                        title: user.title,
                                        url: '/profile/' + user.id,
                                        email: user.email,
                                        city: user.city,
                                        institution: user.institution,
                                        cohortadminrole: user.cohortadminrole
                                    });
                                    _.each(user.roles, function (role) {
                                        if (role.shortname == 'editingteacher' || role.shortname == "teacher") {
                                            uModel.set('teacher', true);
                                        }
                                        if (role.shortname == 'manager') {
                                            uModel.set('manager', true);
                                        }
                                    });
                                    users.push(uModel);
                                }, this);
                                model.reset(users);
                            }
                            else
                            {
                                model.reset();
                            }
                        }, this);
                    }
                    else
                    {
                        model.reset();
                    }
                break;
            }
        },

        comparator : function(model) {
            return [ model.get("fullname").toLowerCase() ]
        },

        model: ParticipantModel
    });
});
