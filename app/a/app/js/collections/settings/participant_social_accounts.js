/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/settings/participant_social_accounts"], function (SocialAccountsModel)
{
    return new (Backbone.Collection.extend(
    {
        model: SocialAccountsModel
    }));
});
