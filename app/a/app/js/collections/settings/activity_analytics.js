/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

define(["app/models/settings/activity_analytics"], function (ActivityAnalyticsModel)
{
    return new (Backbone.Collection.extend(
    {
        model: ActivityAnalyticsModel
    }));
});
