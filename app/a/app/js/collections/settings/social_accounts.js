/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/tools/servicecalls", "app/models/settings/social_account"], function (SC, SocialAccountModel)
{
    return new (Backbone.Collection.extend(
    {
        initialize: function ()
        {
            this.on("reset", function () { this._isDirty = true; }, this);
        },

        sync: function (method, model, options)
        {
            if (method == "read")
            {
                SC.call("local_monorailservices_get_social_settings", {'userid': _User.get("id") }, function (data)
                {
                    model.reset(data);
                    model._isDirty = false;
                }, this, { errorHandler: function ()
                {
                    // Web service failed. Assuming no social accounts.
                    model.reset();
                    model._isDirty = false;
                }});
            }
            return this;
        },

        model: SocialAccountModel
    }));
});
