/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define(["app/models/profile"], function (ProfileModel)
{
    // Cache of user profiles.
    return new (Backbone.Collection.extend(
    {
        model: ProfileModel
    }));
});
