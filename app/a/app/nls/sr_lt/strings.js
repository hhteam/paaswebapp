define(["app/tools/utils"], function (Utils) { 
 return { 
add_another_file : 'Dodaj još jednu datoteku',
add_file : 'Dodaj datoteku',
add_grade_for_other_task : 'Dodaj ocjenu za drugi zadatak',
add_instructions : 'Dodaj uputstva',
assignment_uploaded : function (name, link, time)
            {
                return "Zadatak <a href=\"" + link + "\">" + name + "</a>  je uspješno otpremljen sa "
                    + this.format_time(time);
            },
birthday : 'Rođendan',
blog : 'Blog',
button_back : 'Nazad',
button_cancel_editing : 'Odbaci promjene',
button_close : 'Zatvoriti',
button_delete : 'Izbrisati',
button_done : 'Urađeno',
button_next : 'Sljedeće',
button_no : 'Ne',
button_prev : 'Prethodno',
button_save : 'Sačuvati',
button_submit : 'Podnijeti',
button_upload_file : 'Otpremiti datoteku',
calendar : 'Kalendar',
category : { 
0 : 'Kategorija',
1 : 'Menadzment u ugostiteljstvu',
10 : 'Priroda i Nauka',
11 : 'Filozofija',
12 : 'Istorija, Kultura i Religija',
13 : 'Jezici',
15 : 'Ostalo',
2 : 'Umjetnost, dizajn i arhitektura',
3 : 'Biznis',
4 : 'Inžinjering i informacione tehnologije',
6 : 'Pravo',
7 : 'Obrazovanje',
8 : 'Socijalne nauke',
9 : 'Medicina i Farmacija'},
change_language : 'Promjeni jezik',
city : 'Grad',
completed_courses : 'Završeni kursevi',
completed_tasks : 'Završeni zadaci',
course_fname_limit : 'Naziv predmeta prevazilazi ograničenje od 120 znakova',
course_sname_limit : 'Skraćeni naziv kursa prevazilazi ograničenje od 40 znakova',
courses : 'Kursevi',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'Obriši zadatak',
edit : 'Izmjeniti ',
email : 'Elektronska pošta',
error_photo : 'Obrázok bude upravený na 180x70 pixelov.',
event_delete : 'Izbrisati događaj?',
export : 'Izvoz',
file_submissions : 'Podnošenje datoteke',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
forum : 'Diskusije',
forum_attachment : 'Dodaj datoteku',
grade_publish1 : 'Ocjene nisu još uvek objavljene',
grade_publish2 : 'Ocjene su objavljene',
graded_tasks : 'Ocijenjeni zadaci',
home : 'Kući',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? n + " dana" : n + " dana";
                }
                else if (n < 0)
                {
                    n = -n;
                    return n == 1 ?  " dan" : n + " dana";
                }
                else
                {
                    return "danas";
                }
            },
instructions : 'Instrukcije',
join_email : 'Pošaljite pozive na ove email adrese',
join_email_sent : 'Poziv je poslat. Učesnici će biti navedeni na ovoj stranici nakon što prihvate poziv',
join_participants : 'Učesnici',
label_attachment : 'Naslov priloga',
label_duedate : 'Datum dospjeća',
label_link : 'Link',
label_picture : 'Slika',
label_share : 'Podijeli',
label_start : 'Počnite',
label_status_failed : 'Neuspjelo',
label_teacher : 'Instructor',
logo_default : 'Koristi standardno',
logout : 'Odjava',
n_days_late : function (n)
            {
                return n == 1 ? n + " dan kasni" : n + " dana kasni";
            },
new_content : 'Dodaj sadržaj',
new_file : 'Datoteka/ Fajl',
new_task1 : 'Uputstva',
new_task2 : 'Promjene će biti pokazane ovdje',
new_write : 'Pišite ovdje',
notes : 'Bilješke',
ongoing_courses : 'Tekući kursevi',
overview : 'Sadržaj',
participants : 'Učesnici',
password : 'Lozinku',
privacy : 'Polisa privatnosti',
publish : 'Objaviti',
quiz_answer : 'Opcija za odgovaranje',
quiz_error_answer : 'Opcije za odgovore ne smiju biti prazna',
quiz_error_select : 'Morate odabrati makar jedan tačan odgovor',
quiz_finished : 'Završeni kvizovi',
quiz_results : 'Tačno',
quiz_select_tf : 'Tačan odgovor',
returned_tasks : 'Vraćeni zadaci',
role_student : 'Student',
settings : 'Podešavanja',
share_join_text : 'Pridružite se kursu u Eliademi',
share_twitter : 'Podijeliti na Twitter-u',
sidebar_edit_profile_button : 'Urediti profil',
submitted : 'Dostavljeno',
tab_forum : 'Diskusije',
task : 'Zadatak',
task_instructions : 'Piši ovdje ili otpremaj',
task_not_submitted : 'Ništa još nije podnijeto',
task_quiz : 'Kviz',
task_returned : function (n1, n2) {if (n1 == n2) {return "(sve vraćeno)";} else {return "(" + n1 + "of" + n2 + " vraćeno)";}},
task_save_draft : 'Spremi kao skicu',
task_status1 : 'Nije još ocijenjeno',
task_status2 : 'Nije vraćeno',
task_status3 : 'Neuspjelo',
tasks : 'Zadaci',
tasks_to_be_done : 'Zadaci koji treba da budu urađeni',
there_are_no_tasks : 'Nema zadataka',
update_file : 'Ažuriranje datoteke',
use_facebook_picture : 'Koristite Facebook profilnu sliku'};
});