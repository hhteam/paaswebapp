define(["app/tools/utils"], function (Utils) { 
 return { 
about : '關於我們',
add : '加入',
add_another_file : '加入另一個檔案',
add_file : '加入檔案',
add_grade_for_other_task : '給其他項目輸入成績',
add_instructions : '加入指示',
assignment_uploaded : function (name, link, time)
            {
                return "作業 <a href=\"" + link + "\">" + name + "</a> 在 上傳成功 "
                    + this.format_time(time);
            },
birthday : '生日',
blog : '部落格',
button_back : '上一頁',
button_cancel : '取消',
button_cancel_editing : '放棄修改',
button_close : '關閉',
button_delete : '刪除',
button_done : '完成',
button_next : '下一個',
button_no : '取消',
button_prev : '前一頁',
button_save : '儲存',
button_submit : '繳交',
button_yes : '沒問題',
calendar : '行程',
category : { 
0 : '- 選擇分類 -',
1 : '飯店管理',
10 : '自然科學',
11 : '哲學',
12 : '歷史與文化',
13 : '語言',
15 : '其他',
2 : '藝術',
3 : '商業',
4 : '工程',
6 : '法律',
7 : '教育',
8 : '社會科學',
9 : '醫學'},
change_language : '換個語言',
city : '城市',
completed_courses : '完成的課程',
completed_tasks : '完成的項目',
course : '課程',
course_fname_limit : '課程名稱超過120字的限制',
course_sname_limit : '課程縮寫超過40個字的限制',
courses : '課程',
delete_task : '刪除任務',
delete_task_message : '所有有關這個任務的資料將會被刪除, 你確定要刪掉這個任務嗎?',
edit : '修改繳交日期',
edit_cancel2 : '離開建立課程?已經建立的內容將會消失?',
email : '郵件',
event_delete : '刪除活動',
export : '輸出',
file_submissions : '檔案繳交',
fill_missing_fields : '請完成未完成的欄位',
follow_us : '跟隨我們',
format_time : function (time, notime)
            {
                var txt = Utils.padLeft(time.getDate(), "0", 2) + "." + Utils.padLeft(time.getMonth() + 1, "0", 2) +
                    "." + time.getFullYear();

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
forum : '討論區',
forum_attachment : '加入檔案',
grade_publish1 : '成績尚未出來',
grade_publish2 : '成績已發布',
graded_tasks : '給任務打個成績',
grades : '分數',
helpdesk : '支援',
home : '完成',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == n == 1 ? n + " 天前" : n + " 天前";
                }
                else if (n < 0)
                {
                    n = -n;

                    return n == n == 1 ? n + " 天" : n + " 天";
                }
                else
                {
                    return "今天";
                }
            },
instructions : '指示',
join_email : '發邀請信給這些郵件',
join_email_sent : '張邀請函已經送出, 只要他們接受, 加入的人將會列在下面.',
join_participants : '參加者',
label_attachment : '附件標題',
label_course_details : '課程內容',
label_description : '課程敘述',
label_duedate : '繳交日期',
label_link : '連結',
label_picture : '圖片',
label_share : '分享',
label_teacher : '老師',
logo_default : '使用預設',
logout : '登出',
n_days_late : function (n)
            {
                return 1 ? n + " 天遲交" : n + " 天遲交";
            },
new_content : '加入內容',
new_file : '繳交',
new_task1 : '指示',
new_task2 : '交回的檔案將會秀在這裡',
new_write : '寫在這裡',
no_due_date : '沒有結束的日期',
notes : '筆記',
ongoing_courses : '正在進行中的課程',
overview : '內容',
participants : '參加者',
password : '密碼',
privacy : '隱私宣告',
publish : '發佈',
returned_tasks : '繳回的任務',
role_student : '學生',
settings : ' 設定',
share_facebook : '在臉書分享',
share_join_text : '加入Eliademy的課程:',
share_twitter : '在推特分享',
sidebar_edit_profile_button : '修改個人檔案',
submitted : '已繳交',
tab_forum : '討論區',
task : '任務',
task_returned : function (n1, n2) {if(n1 == n2) {return"(全部繳交)";} else {return "(" + n1 + "of" + n2 + "繳交)";} },
task_status1 : '尚未公佈成績',
task_status2 : '尚未繳回',
tasks : '任務',
tasks_to_be_done : '即將要結束的任務',
there_are_no_completed_tasks : '還沒有完成的任務',
there_are_no_tasks : '還沒有任務',
tos : '服務條款',
update_file : '更新檔案',
use_facebook_picture : '使用臉書的大頭照',
wiz_course_details : '課程內容'};
});