define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'எங்களைப் பற்றி',
active_courses : 'செயலில் உள்ள கோர்ஸ்கள்',
add : 'சேர்க்கவும்',
add_another_file : 'மற்றொரு ஃபைல் சேர்க்க',
add_custom_lti : 'ஒரு Edu app சேர்',
add_file : 'ஃபைல் சேர்க்க',
add_grade_for_other_task : '% d நாட்கள் தாமதமாக திரும்பியளிக்கப்பட்டது',
add_gsers : 'கூகுள் யூஸர்ஸ்',
add_instructions : 'வழிமுறைகளை சேர்க்க',
add_linkedin_account : 'மற்றொரு விருப்பத்தை சேர்க்க',
add_social_button : 'சமூக வலைதள கணக்கு சேர்க்கவும்',
add_social_img_button : 'பேஸ்புக் ஃபுரபைல் படத்தை பயன்படுத்துக',
add_to_profile : 'சுயவிவரத்தில் சேர்',
addasadmin : 'நிர்வாகம் செய்ய',
admin : 'நிர்வாகி',
all : 'அனைத்து பாடப்பிரிவுகள்',
all_users : 'அனைத்து பயனர்கள்',
analytics : 'பகுப்பாய்வுகள்',
and : 'மற்றும்',
apply : 'பயன்படுத்து',
automatically_share_enrollments : 'தானாகவே என் பதிவை பற்றிய தகவல்களை பகிரவும்',
average_submissions : 'சராசரி சமர்ப்பிப்பு முன்னேற்றம்',
banner_update_timezone : 'உங்கள் நேர மண்டலத்தை புதுப்பிக்கவும்.',
bio : 'வாழ்க்கை வரலாறு',
bio_placeholder : 'உங்களை பற்றி உலகிற்கு சொல்லுங்கள்',
birthday : 'பிறந்தநாள்',
blog : 'வலைப்பூ',
browser_warning1 : 'நீங்கள் ஒரு ஆதரிக்கப்படாத பிரவுசரை பயன்படுத்துகிறீர்கள்.',
button_back : 'பின்னே',
button_cancel : 'ரத்து செய்',
button_cancel_editing : 'மாற்றத்தைக் கைவிடு',
button_close : 'மூடுக',
button_copy : 'நகல்',
button_delete : 'நீக்கு',
button_delete_course : 'கோர்ஸை நீக்கவும்',
button_discard1 : 'கோர்ஸை நிராகரிக்க',
button_done : 'முடிவுற்றது',
button_end_now : 'இப்போது நிறுத்து',
button_enroll : 'பதிவு',
button_enroll_to_course : '{% Coursename%} ல் பதிவுசெய்யவும்',
button_export_course : 'எக்ஸ்போர்ட் கோர்ஸ்',
button_import : 'இறக்குமதி ',
button_later : 'ஒருவேளை பின்னர்',
button_next : 'அடுத்து',
button_no : 'இல்லை',
button_open_nwin : 'புதிய சாளரத்தில் திறக்க',
button_post : 'போஸ்ட்',
button_prev : 'முந்தையது',
button_reuse : 'மறுபயன்பாடு',
button_save : 'சேமிக்கவும்',
button_save_changes : 'மாற்றங்களை சமர்ப்பிக்கவும்',
button_send : 'அனுப்புக',
button_start_new_course : 'புதிய கோர்ஸ் உருவாக்கு',
button_update : 'புதுப்பி',
button_upload_file : 'ஃபைல் பதிவேற்று',
button_yes : 'சரி',
calendar : 'நாள்காட்டி',
calexport : 'நாள்காட்டி உறுப்பினராகவும்',
calexport_text : 'இந்த யுஆர்எல்(url) உடன் ஐகால்( iCal)நெறிமுறை ஆதரிக்கும் எந்த ப்ரோகிராமிடமிருந்தும் எலியாடமி(Eliademy )காலண்டரில் பதிவு செய்யலாம்:',
calexport_text2 : 'உங்கள் காலண்டர் அபளிக்கேக்ஷனுக்கான படிநிலை வழிமுறைகள் தேவைப்பட்டால் {{FAQ}} பார்க்கவும்.',
catalog : 'பட்டியல்',
catalog_enrolled : 'சேர்க்கப்பட்டது',
catalog_remove : 'கேட்லாக்கிலிருந்து நீக்கவும்',
catalog_sort : 'வரிசைப்படுத்து',
catalog_sort1 : 'பெயர்',
catalog_sort2 : 'புத்தம்புதியவை',
cert_design_p1 : 'முக்கிய பயிற்றுவிப்பாளர் பெயர், தலைப்பு மற்றும் நிறுவனம் சான்றிதழில் காண்பிக்கப்படும். தனிப்பட்ட தகவல்கள் எப்போதும் <a href="/profile"> Profile </a> பக்கத்தில் புதுப்பிக்கப்படும்.',
cert_design_p2 : 'சான்றிதழில் நிறுவனத்தின் லோகோ மற்றும் பயிற்றுநர் கையொப்பம் அமைத்துக்கொள்ள முடியும். வெளிப்படையான பின்னணி PNG வடிவமைப்பில் இந்த கோப்புகளை பதிவேற்றி கொள்ளவும்.',
cert_dummy_stud : 'மாணவர் பெயர்',
cert_logo_msg : 'PNG படத்தை உபயோகிக்கவும் நேர்த்தியான பின்னணியில்',
certificate : 'சான்றிதழ்',
certificate_ready : 'சான்றிதழ் தயாராக உள்ளது',
change_language : 'மொழி',
change_password : 'கடவுச்சொல்லை மாற்றுக',
change_picture : 'படத்தை மாற்று',
change_timezone : 'நேரமண்டலம்',
city : 'நகரம்',
completed_courses : 'முடிக்கப்பட்ட கோர்ஸ்கள்',
completed_tasks : 'நிறைவேறிய பணிகள்',
congratulations : 'வாழ்த்துகள்!',
contact : 'தொடர்புகொள்',
course : 'பாடம்(கோர்ஸ்)',
course_delete_error : 'கோர்ஸை நீக்க முடியாது. தயவுசெய்து ஆதரவை தொடர்பு கொள்க.',
course_enable_confirmation : 'நீங்கள் கோர்ஸை செயல்படுத்த வேண்டுமா? கோர்ஸ் இறுதி தேதி நீக்கப்படும்.',
course_end_before_start : 'கோர்ஸ் இறுதி தேதி, தொடங்கும் தேதிக்கு முன் இருக்க கூடாது',
course_fname_limit : 'கோர்ஸ் பெயர் 120 எழுத்துக்கள் வரம்பை மீறுகிறது',
course_sname_limit : 'கோர்ஸ் குறுகிய பெயர் 40 எழுத்துக்கள் வரம்பை மீறுகிறது',
course_status_pending : 'கோர்ஸ் மறுஆய்வுக்கு சமர்ப்பிக்கப்படுகிறது',
courses : 'பாடங்கள்(கோர்ஸ்கள்)',
current : 'தற்போதைய',
current_users : 'பயனாளர்கள்',
currentpassword : 'தற்போதைய கடவுச்சொல்',
custom_add_lti_tool : 'edu appக்கு விவரங்களை குறிப்பிடவும்',
date : 'தேதி',
delete_task : 'பணியை நீக்கு',
delete_task_message : 'இந்த பணி தொடர்பான அனைத்து தகவலும் நீக்கப்படும். நீங்கள் இந்த பணியை நீக்க வேண்டும் என்று உறுதியாக இருக்கிறீர்களா?',
design : 'வடிவமைப்பு',
download : 'பதிவிறக்கம்',
edit : 'திருத்து',
edit_cancel2 : 'கோர்ஸ் உருவாக்காமல் வெளியேறுகிறீர்களா? ஏற்கனவே உருவாக்கிய உள்ளடக்கத்தை இழக்க நேரிடும்.',
edit_course_caption : 'கோர்ஸை திருத்து',
edit_group : ' குழு பெயரை திருத்தவும்',
email : 'மின்னஞ்சல்',
email_daily : 'தினசரி',
email_display : 'மின்னஞ்சல் காட்சி',
email_event : 'நாள்காட்டி நிகழ்வு மாற்றப்பட்டது',
email_forum : 'யாரோ ஒருவர் கலந்துரையாடலில் போஸ்ட் பண்ணியிருக்கிறார்',
email_grade : 'பணி தரப்படுத்தப்படுகிறது',
email_immediately : 'உடனடியாக',
email_invite_exceed_limit : 'உங்கள் தற்போதைய திட்டத்துடன் உங்களுக்கு % எல்லை அழைப்புகள் உள்ளது. சில மின்னஞ்சல்களை நீக்கி மீண்டும் முயற்சிக்கவும்',
email_material : 'புதிய கோர்ஸ் மெட்டீரியல் சேர்க்கப்பட்டுள்ளது',
email_never : 'ஒரு போதும் இல்லை',
email_platform_updates : 'Eliademy லிருந்து புதுப்பிப்புகளை பெற',
email_task : 'கோர்ஸின் உள்ளடக்கம் மாற்றப்பட்டது',
email_user_completed : 'மாணவர் கோ்ஸை முடித்துவிட்டார்',
email_user_enrolled : 'புதிய மாணவர் சேர்ந்துள்ளார்',
email_user_submitted : 'மாணவர் ஒரு பணியை சமர்ப்பித்தார்',
email_when : 'எனக்கு மின்னஞ்சல் அனுப்புக',
enrollment_history : 'பதிவு வரலாறு',
err_file_empty : 'ஃபைல் காலியாக உள்ளது',
error_notenrolled : 'நீங்கள் இந்த கோர்ஸில் சேரவில்லை.',
error_photo : 'படம் 180x70 பிக்சல்களில் பொருந்துமாறு மாற்றப்படும்.',
error_similar_skills : 'இதே போன்ற திறன்கள் கண்டறியப்பட்டது',
error_skills_failed_rems : 'பின்வரும் திறன் நீக்குதல் சேமிக்க தவறிவிட்டது',
error_skills_too_many : 'அதிகபட்ச மட்டும் 3 திறன்கள், சேர்க்க இயலவில்லை:{{skillname}}',
event_delete : 'நிகழ்வை நீக்கு',
event_new2 : 'புதிய நிகழ்வு',
export : 'ஏற்றுமதி செய்க',
export_inlcude_feedback : 'எக்ஸ்ப்போர்ட்டில் கருத்துக்களை சேர்க்கவும்',
feedback : 'கருத்து',
file_submissions : 'கோப்பு(ஃபைல்) சமர்ப்பித்தல்கள்',
fill_missing_fields : 'இந்த பீல்ட் காலியாக இருக்க கூடாது',
first_access : 'முதல் அணுகல்',
follow_us : 'எங்களை தொடர',
forum : 'விவாதம்',
forum_add_comment : 'கருத்துரை சேர்க்கவும்',
forum_all_forums_label : 'அனைத்து கருத்துக்களம்',
forum_attachment : 'ஃபைல் சேர்க்க',
forum_new : 'புதிய கருத்துக்களம் தொடங்கவும் ',
forum_new_discussion : 'புதிய விவாதத்தை தொடங்கவும்',
forum_no_recent_posts : 'இந்த கோர்ஸ் குறித்து பேச்சு இதுவரை எதும் இல்லை.',
forum_recent_posts : 'சமீபத்திய குறிப்புகள்',
get_gapp_users : 'எடு',
googledrive : 'கூகுள் ட்ரைவ்',
grade_distribution : 'கிரேடு விநியோகம்',
grade_publish1 : 'கிரேடட்ஸ் இன்னும் வெளியிடப்படவில்லை',
grade_publish2 : 'கிரேட்ஸ் வெளியிடப்பட்டது',
grade_range : 'கிரேடு வரம்பு(%)',
graded_tasks : 'கிரேடட் பணிகள்',
grades : 'தரங்கள்',
group : 'குழுமம்',
groups : 'குழுக்கள்',
helpdesk : 'ஆதரவு',
hide_email : 'அனைவரிடமிருந்து மறைக்கவும்',
home : 'துவக்கம்',
import_csv : 'ஃபைலில் இருந்து இறக்குமதி செய்க.',
import_error : 'கோர்ஸ் பேக்கப் இறக்குமதி செய்வதில் சிக்கல் ஏற்பட்டு உள்ளது. இது சரியான கோர்ஸ் மூடுல் தானா என்பதை சரிபார்க்கவும் மற்றும் தேவைப்பட்டால் எங்களை தொடர்பு கொள்ளவும்.',
import_error_tasks : 'பணிகளை இறக்குமதி செய்வதில் சிக்கல் ஏற்பட்டு உள்ளது.இது சரியான பணிகள் தானா என்பதை சரிபார்க்கவும் .பணிகளை டெலிட் செய்ய ,பணிகள் பக்கத்தை திறந்து ,எடிட் பட்டனக்கு அடுத்து உள்ள அம்புக்குறியை க்ளிக் பண்ணவும்',
import_gapp_contacts : 'மின்னஞ்சல் முகவரிகளை கூகுள் பயண்பாடுகள் (கூகுள் ஆப்ஸ்) களத்திலிருந்து பெறவும்',
import_moodle : 'மூடுல்(Moodle) வடிவத்தில் கோர்ஸ் இறக்குமதி செய்ய',
import_processing : 'இப்போது கோர்ஸ் டேட்டா செயலில் உள்ளது .ஒரு கணம் காத்திருக்கவும்.',
import_uploading : 'ஃபைலை பதிவேற்றுகிறது',
instructions : 'வழிமுறைகள்',
invitemailErr : 'முகவரிகள் சில தவறானவை, சரிபார்க்கவும்.',
issue : 'வழங்கல்',
join_catalog2 : 'பட்டியலில் வெளியிடப்பட்டது',
join_email : 'இந்த மின்னஞ்சல் முகவரிகளுக்கு அழைப்பிதழ்களை அனுப்பு:',
join_email_sent : 'அழைப்பு அனுப்பப்பட்டது. அவர்கள் அழைப்புகளை ஏற்றுக்கொண்ட பிறகு பங்கேற்பாளர்கள் பக்கத்தில் பட்டியலிடப்படுவார்கள்.',
join_home : 'முகப்புப் பக்கத்துக்குச் செல்க',
join_participants : 'பங்கேற்பாளர்கள்',
join_private : 'அழைப்பிதழ்கள் மட்டும்',
join_public : 'பொது(பப்ளிக்)அணுகல்',
label_admin : 'நிர்வாகி',
label_attachment : 'இணைப்பு தலைப்பு',
label_course_startdate : 'துவக்க தேதி',
label_description : 'விளக்கம்',
label_duedate : 'முடியும் தேதி',
label_finish : 'முடி',
label_link : 'இணைப்பு.',
label_picture : 'படம்',
label_request_more : 'மேலும் கோருவதற்கு',
label_see_results : 'முடிவுகளைக் காண்க',
label_share : 'பகிர்',
label_start : 'தொடங்கு',
label_teacher : 'கற்பிப்பவர்',
label_unlimited : 'வரையில்லாதது',
label_used : 'பயன்படுத்தப்பட்ட',
lable_error : 'பிழை',
last_access : 'கடைசி அணுகல்',
learn_more : 'மேலும் அறிய',
licences : 'உரிமம்',
links : 'இணைப்புகள்',
location : 'இருப்பிடம்',
logo_default : 'யூஸ் டீஃபால்ட்',
logo_info : 'லோகோ PNG, JPEG, அல்லது GIF ஃபைல் ஆக இருக்க வேண்டும். மேக்ஸிமம் அளவு 500 KB. படத்தை 192x76 பிக்சல்களில் பொருந்துமாறு மாற்றப்படும்.',
logout : 'வெளியேறு',
lti_customer_key : 'வாடிக்கையாளர் கீ',
lti_customer_secret : 'வாடிக்கையாளர் ரகசியம்',
lti_share_username : ' பயனாளர் பயனர் பெயரை பகிர்',
ltitool_icon_url : 'ஐகான் Url',
ltitool_name : 'பெயர்',
name : 'பெயர்',
new_content : 'உள்ளடக்கத்தை சேர்க்க',
new_file : 'ஃபைல்',
new_group : 'புதிய குழு',
new_task1 : 'வழிமுறைகள்',
new_task2 : 'திரும்பியளிக்கப்பட்ட ஃபைல்களை இங்கே காணலாம்',
new_topic : 'புதிய தலைப்பு',
new_write : 'இங்கே எழுதுக.',
newpassword : 'புதிய கடவுச்சொல்',
newpassword_again : 'புதிய கடவுச்சொல் மீண்டும்',
no_due_date : 'கெடு தேதி இல்லை',
notes : 'குறிப்புகள்',
number_of_enrollments : 'பதிவுகளின் எண்ணிக்கை',
number_of_submissions : 'சமர்ப்பிப்புகளின் எண்ணிக்கை',
number_of_views : 'பக்கப் பார்வையாளர்கள் எண்ணிக்கை',
ongoing_courses : 'நடைப்பெற்று கொண்டிருக்கும் கோர்ஸ்கள்',
opensource : 'ஓபன் சோர்ஸ்',
or : 'அல்லது',
org_analytics : 'உங்கள் நிறுவனத்தின் பகுப்பாய்வுகள்',
org_details : 'நிறுவனத்தின் விவரங்கள்',
org_logo : 'நிறுவனத்தின் லோகோ',
org_name : 'நிறுவனத்தின் பெயர்',
organization : 'நிறுவனம்',
orig_password_missing : 'நடப்பு கடவுச்சொல் வெறுமையாக இருக்கக்கூடாது.',
other_service : 'பிற சேவை',
overview : 'உள்ளடக்கம்',
overview_content : 'மேற்பார்வை',
participants : 'பங்கேற்பாளர்கள்',
password : 'கடவுச்சொல்',
password_change_success : 'உங்கள் கடவுச்சொல் வெற்றிகரமாகப் புதுப்பிக்கப்பட்டது.',
personal_event : 'தனிநபர் நிகழ்வு',
personal_skills : 'உங்கள் சாதிக்கப்பட திறன்கள்',
picture_error : 'படம் ஒரு PNG, JPEG, அல்லது GIF கோப்பாக இருக்க வேண்டும்',
plain_text : 'எளிய உரை',
position : 'நிலை',
privacy : 'தனியுரிமை கொள்கை',
profile_myprofile : 'எனது சுயவிபரம்',
promote_to_teacher : 'பயிற்றுவிப்பாளரை விளம்பரப்படுத்துக',
public_catalog : 'Eliademy  பட்டியல்',
publish : 'வெளியிடு',
quiz_answer : 'தேர்ந்தெடுத்து பதில் சொல்',
quiz_attempts_left : 'மீதமுள்ள முயற்சிகள் ',
quiz_error_answer : 'பதில் காலியாக இருக்கக்கூடாது.',
quiz_error_question : 'கேள்விகள் காலியாக இருக்க கூடாது',
quiz_error_select : 'தாங்கள் குறைந்தது ஒரு பதிலையாவது தேர்வு செய்ய வேண்டும்.',
quiz_finished : 'முடித்த வினாடி வினாக்கள்',
quiz_incorrect : 'தவறு',
quiz_instructions : 'இந்த வினாடி வினா முடிக்க ஒரே ஒரு முயற்சிதான் உள்ளது',
quiz_question : 'கேள்வி',
quiz_results : 'சரி',
quiz_select_tf : 'சரியான விடை',
removeasadmin : 'பயனாளர் உருவாக்க',
replytoaddress : 'support@eliademy.com',
replytofrom : 'noreply@eliademy.com',
resend_invite : 'மீண்டும் அழைப்பு அனுப்புக',
returned_tasks : 'சமர்ப்பிக்கப்பட்ட பணிகள்',
revert_to_student : 'மாணவர்க்கு மாற்று',
review : 'மறுஆய்வு',
revoke : 'இரத்து செய்க',
role_student : 'மாணவர்',
search_courses : 'தேடு...',
search_results : 'தேடல் முடிவுகள்',
select_course : 'கோர்ஸை தேர்ந்தெடுக்கவும்',
select_export_format : 'ஏற்றுமதி வடிவமைப்பை தேர்வு செய்க:',
service_facebook : 'Facebook',
settings : 'அமைப்புகள்',
settings_help_translate : 'எங்களுக்கு மொழிபெயர்க்க உதவுங்கள்',
share_course_title : 'இந்த கோர்ஸ் நன்றாக உள்ளதா?',
share_email : 'மின்னஞ்சல் மூலம் பகிர்',
share_enroll : 'நான் ஒரு பொது கோர்ஸில் பதிவு செய்யும் போது, எனது நண்பர்களுடன் பகிர‍வும்',
share_facebook : 'ஃபேஸ்புக்கில் பகிர்',
share_join_text : 'எலியாடமியில்(Eliademy)கோர்ஸ் சேர:',
share_linkedin : 'ட்விட்டரில் பகிர்',
share_live : 'Live ல் பகிர்',
share_twitter : 'ட்விட்டரில் பகிர்',
show_email : 'அனைவரும் பார்க்க அனுமதிக்கவும்',
signature : 'கையொப்பம்',
site_name : 'எலியாடமி(Eliademy)',
skype : 'ஸ்கைப்(Skype)',
social : 'சமூகம்',
social_placeholder : 'உங்கள் கணக்கு அதிக தொழில்முறையுடன் இருக்க  உங்கள் சமூக வலைதள கணக்குகளை சேர்க்கவும் ',
student_activity : 'மாணவர் செயல்பாடு',
submitted : 'சமர்பிக்கப்பட்டது',
support_response : 'உங்கள் கோரிக்கைக்கு நன்றி. எங்கள் உதவிக்குழு விரைவில் தொடர்புகொள்வார்கள்',
tab_forum : 'விவாதம்',
task : 'பணி',
task_draft : 'ட்ராப்ட் திருத்த',
task_instructions : 'இங்கே எழுதுக அல்லது கோப்புகளை பதிவேற்றுக',
task_late1 : '% d நாட்கள் தாமதமாக திரும்பியளிக்கப்பட்டது',
task_late2 : '% d மணி நேரம் தாமதமாக திரும்பியளிக்கப்பட்டது',
task_not_submitted : 'இன்னும் எதுவும் சமர்ப்பிக்கவில்லை',
task_quiz : 'வினாடி வினா',
task_save_draft : 'வரைவாக சேமி',
task_status1 : 'இன்னும் தரம்பிரிக்கப்படவில்லை',
task_status2 : 'சமர்ப்பிக்கவில்லை',
task_submission_progress : 'பணி சமர்ப்பிப்பு முன்னேற்றம் ',
task_submissions : 'சமர்ப்பித்தல்',
task_submit_changes : 'மாற்றங்களை சமர்ப்பிக்கவும்',
task_text : 'எழுதப்பட்ட உரை',
tasks : 'பணிகள்',
tasks_files : 'திரும்பியளிக்கப்பட்ட ஃபைல்கள்',
tasks_to_be_done : 'செய்யப்பட வேண்டிய பணிகள்',
there_are_no_completed_tasks : 'முடிந்த பணிகள் எதுவும் இல்லை',
there_are_no_tasks : 'எந்த பணிகளும் இல்லை',
topics : 'தலைப்புகள்',
tos : 'பயன்படுத்தும் வழிமுறை',
total : 'மொத்தம்',
total_interactions : 'மொத்த இடைவினைகள்',
type : 'வகை',
type_emails : 'ட்டைப் மின்னஞ்சல்கள்',
unlimited : 'வரையில்லாதது',
until : 'வரையிலும்',
update_file : 'கோப்பினை புதுப்பிக்கவும்',
update_password : 'கடவுச்சொல்லை புதுப்பி',
update_username : 'நீங்கள் உங்கள் பயனர் பெயர் மாற்ற விரும்பினால் எங்கள் ஆதரவு சேவையை தொடர்பு கொள்க.',
upload_cancelled : 'பதிவேற்றம் ரத்து செய்யப்பட்டது',
upload_csv_file : 'இங்கே CSV ஃபைல் பதிவேற்றுக',
upload_video : 'ஃபைல் பதிவேற்று',
use_facebook_picture : 'பேஸ்புக்  ஃபுரபைல் படத்தை பயன்படுத்துக',
user_directory : 'பயனர் டைரக்டரி',
user_firstname : 'முதல் பெயர்',
user_info_catalog_warning : 'சமர்ப்பிப்பு செய்யும் முன்  பயனர் படத்தை பதிவேற்றவும் மற்றும் பயோ ஃபீல்டை நிரப்பவும்.',
user_lastname : 'பெயர் பின்பகுதி',
users_many : 'பயனாளர்கள்',
valid_text_input : 'ஆல்பா நியுமரிக் எழுத்துக்கள் மட்டும் உள்ளிடவும்.',
view : 'காண்க',
view_certificate : 'சான்றிதழை பார்வையிடு',
wiz_description : 'கோர்ஸ் விளக்கம் <br> <br> கற்றல் நோக்கங்கள் மற்றும் வெளிப்பாடுகள் <br> <br> வளங்கள் <br> <br> மதிப்பீட்டு அளவுகோல்',
wiz_label_title : 'கோர்ஸ் பெயர் சேர்க்கவும்',
wiz_new_course : 'புது கோர்ஸ்',
wiz_step1 : 'புதிய கோர்ஸ் தொடங்க',
your_courses : 'உங்கள் கோர்ஸ்கள்'};
});