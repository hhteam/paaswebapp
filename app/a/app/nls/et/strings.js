define(["app/tools/utils"], function (Utils) { 
 return { 
access_ext_course_error : 'Puudub ligipääs kursusele. Palun võta ühendust meie kasutajatoega.',
analytics : 'Analüütika',
automatically_share_enrollments : 'Jaga automaatselt infot minu regisrteerumiste kohta',
button_grade_task : 'Hinne',
button_open_nwin : 'Ava uues aknas',
calexport : 'Telli sündmustevoog oma kalendrisse',
calexport_text : 'Sellel aadressil klõpsates võid tellida endale Eliademy kalendrisündmuste voo, mis automaatselt lisab sündmused sinu arvutis või telefonis olevasse kalendrisse iCal protokolli abil: ',
calexport_text2 : 'Kui vajad täpsemaid juhtnööre kalendriäpi kasutamiseks, vaata {{faq}} ',
cert_design_p1 : 'Vastutava õpetaja nimi, tiitel ja organisatsioon lisatakse tunnistusele. Isikuandmeid saab igal ajal muuta lehel <a href="/profile">Profiil</a>.',
cert_design_p2 : 'Tunnistusele saab lisada organisatsiooni logo ja õpetaja allkirja. Palun laadi need pildifailid üles PNG formaadis ja läbipaistva taustaga. ',
cert_logo_msg : 'Palun kasuta PNG failiformaati koos läbipaistva taustaga.',
cert_sig_picture_details : 'Toetatud formaadid: jpg, png<br>Maksimumsuurus: 500kb<br>Vihje: parimaks tulemuseks kasuta ruudukujulist pilti',
clone_error : 'Kursuse kloomisel tekkis tõrge. Palun katseta uuesti, vajadusel võta ühendust meie kasutajatoega.',
clone_error_modules : 'Kahjuks ei saa privaatseid videosid ja LTI äppe taaskasutada.',
completed_description_delete_nonempty : 'Avaliku kursuse kustutamiseks teavita kõiki õpilasi saates neile sõnum lehel "Osalejad". Seejärel vali kursuse tüübiks "Ainult kutsega" ja klõpsa uuesti "Kustuta" nupul.',
course_delete_body : 'Palun kirjuta sõna "{{confirmationstring}}" järgnevasse lünka kinnitamaks, et soovid kustutada kursust {{coursename}}:',
course_delete_confirmation : 'KUSTUTA',
course_delete_error : 'Kursust ei saanud kustutada. Palun võta ühendust meie kasutajatoega.',
course_end_confirmation_body : 'Kursust näidatakse kõigile õpilastele kui "Lõpetatud". Õpilased saavad ikka kursust vaadata ja õpetajad kursust muuta.',
course_keywords_description : 'Palun kasuta kursuse kirjeldamiseks komadega eraldatud üksikuid sõnasid. Märksõnad aitavad õppijatel su kursust kiiremini leida.',
course_not_completed_msg : 'Õppija pole seda kursust veel läbinud. Kas oled kindel, et soovid talle tunnistuse väljastada?',
course_reset_progress_msg : 'Oled kindel, et soovid õpilase edasiminekud tühistada? Kõik esitatud ülesanded ja testitulemused kustutatakse.',
csv_format : 'CSV fail (arvutitabel)',
early_live_session_emsg : 'Webinariga saab liituda 15 minutit enne selle toimumisaega.',
email_eliademy_invite : 'Ma kasutan Eliademy\'t põnevate kursuste avastamiseks, loomiseks ja jagamiseks. Liitu ka!',
enroll_groups_message : 'Grupi registreerimisel lisatakse kursusele kõik praegused ja tulevased grupi kasutajad.',
err_file_empty : 'Fail on tühi',
error_file_type : 'Lubatud on vaid %TYPES failitüüp',
error_payment : 'Makse töötlemine ebaõnnestus. Palun kontrolli makseandmeid või võta ühendust meie toetusmeeskonnaga.',
error_photo : 'Pildi suurus muudetakse 180x70 piksli mõõtudesse.',
error_service : 'Muutuste salvestamine ebaõnnestus. Palun vaata üle, kas su internetiühendus toimib ja proovi uuesti.',
error_unknown : 'Vabandust, midagi läks valesti',
export_inlcude_feedback : 'Kaasa eksportfaili ka tagasiside',
faq : 'KKK',
fill_billing_details_invoice_option : 'Kui soovid maksta arvega, palun kirjuta meile aadressil support@eliademy.com.',
forum_recent_posts_description : 'Kõik viimased postitused on siin kuvatud. Mine otse arutelu juurde või vali foorum külgribalt.',
hover_to_see_more : 'selgituste nägemiseks libista hiirega üle tulpade',
import_error : 'Kursuse importimisel tekkis tõrge. Palun kontrolli, kas tegemist on korrektse Moodle e-kursuse eksportfailiga ja vajadusel kontakteeru meie kasutajatoega. ',
import_error_tasks : 'Ülesannete importimisel tekkis tõrge. Kontrolli, kas imporditi õiged ülesanded. Võid ülesandeid kustutada lehel "Ülesanded".',
import_gapp_contacts : 'Impordi e-posti aadressid Google Apps domeenist',
import_instructions : 'Võimalik on importida järgnevat kursuse sisu: tekst, pildid (lisatud materjalidena), videod, manused, ülesanded ja testid (ainult valikvastustega). Järgi videojuhiseid ja laadi üles .ZIP või .MBZ fail.',
import_processing : 'Töötlen kursuse andmeid, oota pisut.',
is_awarded : 'väljastati õppijale ',
label_add_attachments : 'Failide lisamiseks {vali failid oma arvutist}| või {Google Docs alt}|. Sa võid ka {vistutada õppeäppe}| või {laadida üles videosid}|.',
label_cannot_unenroll_from_public_courses : 'Sa ei saa eemaldada oma tasulise kursuse osalejaid. Vajadusel võta palun ühendust Eliademy toega.',
label_cert_faq_a1 : 'Printimisel ja kohaletoimetamisel on erinevad vastutajad ja seega on võimatu tellimust tühistada. Aga kui sa pole kohaletoimetatud tootega rahul, anname endast parima, et probleemi lahendada.',
label_cert_faq_a2 : 'Jah, see on võimalik kuni tellimus on teele pandud. Teele panemine tomub tavaliselt 12-24 tundi peale tellimist. Palun kirjuta aadressil support@eliademy.com kohe, kui avastad, et aadress ei ole õige.',
label_cert_faq_a4 : 'Hinnale kohandub Euroopa käibemaks. Kui elad väljaspool Euroopa Liitu, ei pea sa käibemaksu maksma. Kui elad Euroopa Liidus, arvutame automaatselt õige käibemaksu summa sinu riigi põhjal.',
label_cert_online_msg : 'Peale kursuse lõpetamist saavad õpilased PDF faili veebiaadressi, mille nad saavad ise välja printida, lisada oma LinkedIn profiilile ja jagada sotsiaalvõrgustikes.',
label_cert_order_placed : 'Sa oled edukalt esitanud tellimuse %DATE_ORDER. Eeldatav kohalejõudmise kuupäev on %DATE_DELIVERY.',
label_cert_printed_msg : 'Lisaks tasuta veebipõhisele tunnistusele pakume võimalust osta tunnistuse paberversioon igale õpilasele hinnaga %PRICE EUR. Sa teenid 10 EUR iga ostetud tunnistuse eest, summa kantakse igakuiselt sinu PayPal kontole:',
label_cert_pro_feature2 : 'Vesimärgiga paber',
label_cert_pro_feature3 : 'Holograafilise pitsatiga',
label_cert_pro_feature4 : 'Tasuta kohaletoimetamine Soomest',
label_complete_purchase_legal : 'Ostu tegemisega nõustun Eliademy tingimustega: {Terms of Use} ja {Privacy Policy}. Arve esitatakse koheselt.',
label_licence_cc_info : 'Sisu võib taaskasutada muutustega või ilma kõigi Eliademy õpetajate poolt',
label_licence_nonfree : '© Copyright $INSTRUCTOR. All rights reserved.',
label_link_paypal : 'Seo PayPal teenusega',
label_minvideo_usage : 'Aitäh, et taotlesid videolimiidi suurendamist. Palun laadi kõigepealt üles vähemalt 500mb videomaterjali.',
label_per_ordered_copy : 'iga tellitud koopia kohta',
label_premium_delivery_p1 : 'Euroopa: 1-3 nädalat',
label_premium_delivery_p2 : 'USA & Kanada: 1-4 nädalat',
label_premium_delivery_p3 : 'Maailm: 2-6 nädalat',
label_pro_payment_failed_msg : 'Eliademy Premium makse ei ole meile kohale jõudnud. Kõik Premium funktsioonid on tühistatud, kuid sul säilib ligipääs kõikidele oma kursustele. Palun kontrolli, kas oled valinud toimiva makseviisi.',
label_quiz_no_results2 : 'Pea meeles, et sa ei saa küsimuste arvu muuta peale seda, kui vähemalt üks õpilane on testi ära teinud.',
label_subject_vat : 'Kohaldatakse EL käibemaksu',
label_title : 'Pamagat',
label_trial_expired_message : 'Su Eliademy Premium prooviperiood on lõppenud ja sa pole sisestanud oma makseandmeid. Kõik Premium funktsioonid on tühistatud, aga sul säilib ligipääs kõikidele oma kursustele. Kui sa oled valmis Premium programmiga jätkama, palun sisesta allpool oma makseandmed.',
label_unpublished : 'Peidetud',
label_vat : 'Käibemaks',
label_vat_id : 'VAT ID',
live_timeerror : 'Kõikide webinaride kestus on piiratud kolmeks tunniks.',
live_timezone_reminder : 'Pea meeles, et kõik sündmused on näidatud ajatsoonis %TIMEZONE. Ajatsooni on võimalik muuta seadete lehel.',
liveclientcompat_dialog_message : 'Enne sessiooni alustamist, palun tee kindlaks, et',
liveclientcompat_dialog_message_li1 : 'Sa kasutad kõige uuemat Chrome, Firefox, Safari või Internet Explorer versiooni;',
liveclientcompat_dialog_message_li2 : 'Adobe Flash Player on installitud ja uuendatud;',
liveclientcompat_dialog_message_li3 : 'Hüpikakende blokeerimine on su brauseris välja lülitatud;',
ltitool_configerror : 'Õppe-äpi seadistuse tõrge',
ngo_account_message : 'Sulle on antud tasuta Eliademy Premiumi kasutusõigus. Õigustatud kasutuse tingimused kehtivad.',
no_courses_student_more_info : 'Sa saad registreeruda kursusele, kui said kutse õpetajalt<br>või kui leidsid ise huvitava kursuse Eliademy kataloogist.',
no_live_sessions : 'Kui sa lõpetad webinari, ilmub see siin.',
password_condfail : 'Salasõna peab sisaldama vähemalt 8 tähemärki, neist vähemalt üks peaks olema väiketäht, üks suurtäht ja üks number. ',
picture_info : 'Kursuse pilt peab olema PNG, JPEG või GIF failitüüp. Maksimumsuurus on %s MB. Pildi suurust muudetakse 845x170 pikslile vastavaks. Pildi kohendamiseks tiri seda aknas üles- või allapoole.',
pro_course_pricacy_label_more : 'Kui otsid turvalist õpikeskkonda, alusta 30-päevast prooviperioodi juba täna.',
pro_webinar_label : 'Ekraani ja presentatsioonide jagamise võimalusega webinaride jaoks on vaja tellida Eliademy Premium.',
revoke_access_for : 'Eemalda ligipääs teenusele %SERVICE',
self_paced_description : 'Kursus on enda valitud õpitempoga. Õpilased saavad liituda ükskõik mis ajal ja õppida iseseisvalt.',
share_course_certificate_info : 'Sa saad seda jagada sotsiaavõrgustikes ja lisada oma LinkedIn profiilile.',
show_email : 'Luba kõigil vaadata',
sidebar_complete_profile_msg : 'Tee ennast äratuntavaks – lae üles profiilipilt ja lisa lühitutvustus.',
sidebar_localize_msg : 'Soovid, et Eliademy oleks saadaval ka kohalikus keeles? Liitu meie vabatahtlike tõlkijate meeskonnaga.',
sidebar_new_features_msg : 'Igal nädalal avaldatakse sinu tagasiside põhjal midagi uut. Uuri lähemalt meie blogist. ',
sidebar_own_your_lms_msg : 'Uuri lähemalt erinevate Eliademy võimaluste kohta ja jaga oma ideid uuenduste tegemiseks.',
sidebar_share_the_news_msg : 'Liitu meie hariduse demokratiseerimise missiooniga andes teistele õppijatele ja õpetajatele Eliademy kohta teada. Iga jagamine aitab kaasa parema teenuse loomisele.',
site_name : 'Eliademy',
social_placeholder : 'Lisa oma sotsiaalmeedia kontod, et muuta oma konto professionaalsemaks',
support_response : 'Täname pöördumise eest. Meie tugimeeskond võtab peatselt ühendust. ',
task_your_answer : 'Sinu vastus',
thumbnailpicture_details : 'Toetatud formaadid: jpg, png<br>Maksimumsuurus: 500kb<br>Vihje: parima tulemuse jaoks kasuta resolutsiooni 267x150',
title_grade : 'Hinne',
type_paste_emails : 'E-posti aadressid (komadega eraldatud)',
upload_video_text1 : 'Laadi üles fail formaadis .mp4, .mov või .ogg ',
user_unenroll_confirmation : 'EEMALDA',
zip : 'Postikood'};
});