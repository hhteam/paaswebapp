define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'Біз туралы',
add : 'Қосу',
add_another_file : 'Жаңа файл қосу',
add_grade_for_other_task : 'Тапсырманы бағалау',
add_instructions : 'Сипаттаманы қосу',
assignment_uploaded : function (name, link, time)
            {
                return "Тапсырма <a href=\"" + link + "\">" + name + "</a> сәтті жүктелді "
                    + this.format_time(time);
            },
birthday : 'Туған күн',
blog : 'Блог',
button_back : 'Артқа',
button_cancel : 'Болдырмау',
button_cancel_editing : 'Өңдеуді қабылдамау',
button_close : 'Жабу',
button_delete : 'Жою',
button_discard1 : 'Курстан бас тарту',
button_done : 'Дайын',
button_next : 'Ары қарай',
button_no : 'Жоқ',
button_prev : 'Алдыңғы',
button_save : 'Сақтау',
button_submit : 'Жіберу',
button_upload_file : 'Файлды жүктеңіз',
button_yes : 'Иә',
calendar : 'Күнтізбе',
category : { 
0 : 'Айдар',
1 : 'Қызмет көрсету',
10 : 'Ғылым',
11 : 'Философия',
12 : 'Тарих',
13 : 'Қазақ тілі/Шет тілдері',
15 : 'Тағы басқалары',
2 : 'Өнер, дизайн және архитектура',
3 : 'Бизнес',
4 : 'Ақпараттық технологиялар',
6 : 'Заң',
7 : 'Білім',
8 : 'Әлеуметтік ғылымдар',
9 : 'Медицина'},
change_language : 'Тілді өзгерту',
city : 'Қала',
completed_courses : 'Аяқталған курстар',
completed_tasks : 'Орындалған тапсырмалар',
course : 'Курс',
course_fname_limit : 'Курстың тақырыбы 120 таңбадан аспауы керек',
course_sname_limit : 'Курстың коды 40 таңбадан аспауы керек',
courses : 'Курстар',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'Тапсырманы өшіру',
delete_task_message : 'Барлық ақпарат өшіріледі. Сенімдісіз бе?',
edit : 'Өңдеу',
edit_cancel2 : 'Сіз шынымен курс жасамай шыққыңыз келе ме?Олай болса, жасалған өзгерістер сақталмайды.',
email : 'Электрондық пошта',
event_delete : 'Іс-шараны өшіресіз бе?',
export : 'Экспорт',
file_submissions : 'Жүктелген файлдар',
fill_missing_fields : 'Барлық бөліктерді толтырыңыз',
follow_us : 'Бізге еріңіз',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
grade_publish1 : 'Бағалар әлі жарияланбады',
grade_publish2 : 'Бағалар жарияланды',
graded_tasks : 'Бағаланған тапсрымалар',
grades : 'Бағалар',
helpdesk : 'Қолдау',
home : 'Басты бет',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? n + " күн бұрын" : n + " күн бұрын";
                }
                else if (n < 0)
                {
                    n = -n;
                    return n == 1 ? n + " күн" : n + " күн";
                }
                else
                {
                    return "бүгін";
                }
            },
instructions : 'Нұсқаулық',
join_email : 'Шақырулар мына адреске жіберіледі',
join_email_sent : 'Шақыру жіберілді. Қатысушылардің тізімі осы жерде, олар шақыруды қабылдағаннан кейін шығады.',
join_participants : 'Қатысушылар',
join_private : 'Тек қана шақыру бойынша',
join_public : 'Көпшілікке арналған рұқсат',
label_attachment : 'Тіркелген файлдың тақырыбы',
label_description : 'Сипаттамасы',
label_duedate : 'Тапсыру күні',
label_finish : 'Аяқтау',
label_link : 'Сілтеме:',
label_picture : 'Сурет/Кескін',
label_share : 'Бөлісу',
label_start : 'Бастау',
label_teacher : 'Мұғалім',
logo_default : 'Бастапқы логотипті қолдану',
logo_info : 'Логотипті PNG, JPEG немесе GIF форматында жүктеуге болады. Файлдың ең жоғарғы көлемі 500кб. Сурет 192x76 пикселге дейін кішірейтіледі.',
logout : 'Шығу',
n_days_late : function (n)
            {
                return 1 ? n + " күн мерзімі өткен" : n + " күн мерзімі өткен";
            },
new_content : 'Мазмұн қосу',
new_file : 'Файл',
new_task1 : 'Нұсқаулық',
new_task2 : 'Мына жерде көрсетіледі',
new_write : 'Мына жерге жазыңыз.',
no_due_date : 'күні жазылмаған',
notes : 'Ескертпелер',
ongoing_courses : 'Өтіп жатқан курстар',
overview : 'Мазмұн',
participants : 'Қатысушылар',
password : 'Кілтсөз',
privacy : 'Құпиялылық шарты',
publish : 'Жариялау',
quiz_answer : 'Жауап нұсқалары',
quiz_error_answer : 'Жауаптары болатын жер бос болмауы керек.',
quiz_error_question : 'Сұрақтар бос болмауы керек.',
quiz_error_select : 'Сіз ең болмағанда бір дұрыс жауапты таңдауыңыз керек.',
quiz_finished : 'Аяқталған бақылау жұмыстары',
quiz_incorrect : 'Дұрыс емес',
quiz_instructions : 'Бұл жұмысты аяқтау үшін тек қана бір мүмкіндігіңіз бар.',
quiz_question : 'сұрақ',
quiz_results : 'дұрыс',
quiz_select_tf : 'Дұрыс жауап',
returned_tasks : 'Қайтарылған тапсырмалар',
role_student : 'Студент',
settings : 'Баптаулар',
share_facebook : 'Facebook-те бөлісу',
share_join_text : 'Eliademy-дегі курсқа жазылыңыз',
share_twitter : ' twitter- бөлісу',
sidebar_edit_profile_button : 'Аккаунтты өзгерту',
site_name : 'Eliademy',
submitted : 'Ұсынылған',
task : 'Тапсырмалар',
task_draft : 'Нұсқаны өңдеу',
task_instructions : 'Мына жерге жазыңыз немесе файлды жүктеңіз',
task_late1 : '%d  күн кеш жіберілді',
task_late2 : '%d сағат кеш жіберілді',
task_not_submitted : 'Әлі ешнәрсе ұсынылмаған',
task_quiz : 'Бақылау жұмысы',
task_returned : function (n1, n2) {if(n1 == n2) {return "(Барлығы алынды)";} else {return "(" + n1 + " из " + n2 + "алынды)";}},
task_save_draft : 'Нұсқа ретінде сақтау',
task_status1 : 'Бағаланбады',
task_status2 : 'Тапсырмалар қайтарылмады',
task_submissions : 'Қабылдау',
task_submit_changes : 'Өзгерістерді қабылдау',
task_text : 'Мәтін',
tasks : 'Тапсырмалар',
tasks_files : 'Жүктелген файлдар',
tasks_to_be_done : 'Тапсырмалар',
there_are_no_completed_tasks : 'Тапсырмалар әлі орындалмаған',
there_are_no_tasks : 'Тапсырмалар жоқ',
tos : 'Қызмет көрсету шарттары',
update_file : 'Файлды жаңалау',
use_facebook_picture : 'Facebook-тағы суретті қолдану',
wiz_description : 'Курстың сипаттамасы<br><br>Мақсаты<br><br>Ресурстары<br><br>Бағалау критерийлері',
wiz_new_course : 'Жаңа курс',
wiz_step1 : '1. Жаңа курсты бастау'};
});