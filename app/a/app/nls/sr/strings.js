define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'О нама',
add : 'Додати',
add_another_file : 'Додај још један фајл',
add_file : 'Додај фајл',
add_grade_for_other_task : 'Додајте оцену за други задатак',
add_instructions : 'Додај инструкцију',
assignment_uploaded : function (name, link, time)
            {
                return "Уступање <a href=\"" + link + "\">" + name + "</a>  је успешно отпремљена на "
                    + this.format_time(time);
            },
birthday : 'Рођендан',
blog : 'Блог',
button_back : 'Назад',
button_cancel : 'Отказати',
button_cancel_editing : 'Одбаци промене',
button_close : 'Затвори',
button_delete : 'Избриши',
button_done : 'Готово',
button_grade_task : 'Оцена',
button_next : 'Следеће',
button_no : 'Не',
button_prev : 'Претходно',
button_save : 'Сачувај',
button_submit : 'Поднеси',
button_upload_file : 'Отпремити датотеку',
button_yes : 'Да',
calendar : 'Календар',
category : { 
0 : 'Категорија',
1 : 'Менаџмент у yгоститељствy',
10 : 'Природа и наука',
11 : 'Филозофија',
12 : 'Историја, култура и религиja',
13 : 'Језици',
15 : 'Остало ',
2 : 'Уметност, дизајн и архитектура',
3 : 'Бизнис',
4 : 'Инжињеринг и Информационe технологијe',
6 : 'Право',
7 : 'Образовање',
8 : 'Социјалне науке',
9 : 'Медицина и фармација'},
change_language : 'Промени језик',
city : 'Град',
completed_courses : 'Завршени курсеви',
completed_tasks : 'Завршени задаци',
course : 'Курс',
course_fname_limit : 'Име прeдмeтa превазилази 120 знакова',
course_public_fee : '€',
course_sname_limit : 'Кратко име курса превазилази 40 знакова',
courses : 'Курсеви',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'Избриши задатак',
delete_task_message : 'Све информације везано за овај задатак ће бити избрисане. Да ли сте сигурни да желите да избришете овај задатак?',
edit : 'Изменити',
edit_cancel2 : 'Отићи без прављења курса? Већ направљен садржај ће бити изгубљен',
email : 'Електронска пошта',
event_delete : 'Избрисати догађај?',
export : 'Извоз',
file_submissions : 'Подношење датотека',
fill_missing_fields : 'Молимо вас попуните празна поља',
follow_us : 'Пратите нас на',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
forum : 'Дискусијe',
forum_attachment : 'Додај фајл',
grade_publish1 : 'Оцене нису још објављене',
grade_publish2 : 'Оцене објављене',
graded_tasks : 'Оцењен задатак',
grades : 'Оцене',
helpdesk : 'Подршка',
home : 'Кући',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? n  + "данa" : n  + " данa";
                }
                else if (n < 0)
                {
                    return n == 1 ? n + " дан" : n + " данa"
                    return n == 1 ? n + " күн" : n + " күн";
                }
                else
                {
                    return "данас";
                }
            },
instructions : 'Инструкције',
join_email : 'Пошаљи позиве на ове е-маил адресе',
join_email_sent : 'Е-маил позиви послати.Учесници ће бити додати на ову листу, када прихвате позив',
join_participants : 'Учесници',
label_attachment : 'Наслов прилога',
label_course_details : 'Детаљи о курсу',
label_description : 'Опис или Дескрипција',
label_duedate : 'Датум доспећа',
label_finish : 'Завршите',
label_link : 'Веза:',
label_org_user : '-',
label_picture : 'Слика',
label_share : 'Подели',
label_start : 'Почните',
label_status_failed : 'Неуспело',
label_teacher : 'Модератор',
label_unpublished : 'Сакривено',
logo_default : 'Користи стандардно',
logout : 'Одјавa',
n_days_late : function (n)
            {
                return n == 1 ? n + " дан касни" : + n + " данa касни";
            },
new_content : 'Додај садржaj',
new_file : 'Фајл',
new_task1 : 'Инструкције',
new_task2 : 'Промене ће бити показане овде',
new_write : 'Пиши овде',
no_due_date : 'Нема датума доспећа',
notes : 'Белешке',
ongoing_courses : 'Курсеви у току',
overview : 'Садржај',
participants : 'Учесници',
password : 'шифра',
privacy : 'Полиса приватности',
publish : 'Објавити',
quiz_answer : 'Опција за одговарање',
quiz_error_answer : 'Опције за одговоре не смеју бити празна',
quiz_error_question : 'Питања не смеју бити празна',
quiz_error_select : 'Морате изабрати макар један тачан одговор',
quiz_finished : 'Завршена квизови',
quiz_incorrect : 'Нетачно',
quiz_instructions : 'Имаћете само један покушај да завршите овај квиз',
quiz_question : 'Питањe',
quiz_results : 'Тачно',
quiz_select_tf : 'Тачан одговор',
returned_tasks : 'Враћени задаци',
role_student : 'Студент',
settings : 'Подешавања',
share_facebook : 'Подели на Фејсбуку',
share_join_text : 'Придружи се на курс у Елиадемији',
share_twitter : 'Подели на Твитеру',
sidebar_edit_profile_button : 'Изменити профил',
site_name : 'Eliademy',
submitted : 'Достављено',
tab_forum : 'Дискусијe',
task : 'Задатак',
task_instructions : 'Пиши овде или отпремај',
task_not_submitted : 'Ништа још није поднето',
task_quiz : 'Квиз',
task_returned : function (n1, n2) {if(n1 == n2) {return "(Све враћено)";} else { return "(" + n1 + " of " + n2 + "враћено)";}},
task_save_draft : 'Сачувај као нацрт',
task_status1 : 'Није још оцењен',
task_status2 : 'Није враћено',
task_status3 : 'Неуспело',
task_submissions : 'Поднети',
tasks : 'Задаци',
tasks_to_be_done : 'Задаци који  треба да буду урађени',
there_are_no_completed_tasks : 'Немате завршених задатака',
there_are_no_tasks : 'Немате задатке',
title_grade : 'Оцена',
tos : 'Услови коришћења',
update_file : 'Ажурирајте фајл',
use_facebook_picture : 'Искористи профилну слику са Фејсбука',
wiz_course_details : 'Детаљи о курсу'};
});