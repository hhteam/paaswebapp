define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'O nama',
add : 'Dodati',
add_another_file : 'Dodaj još jedan fajl',
add_file : 'Dodaj fajl',
add_grade_for_other_task : 'Dodaj ocijenu za drugi zadatak',
add_instructions : 'Dodaj naredbe',
assignment_uploaded : function (name, link, time)
            {
                return "Zadatak <a href=\"" + link + "\">" + name + "je uspješno otpremljen sa </a>  "
                    + this.format_time(time);
            },
birthday : 'Rođendan',
blog : 'Blog',
button_back : 'Natrag',
button_cancel : 'Otkazati',
button_cancel_editing : 'Odbaci promjene',
button_close : 'Zatvoriti',
button_delete : 'Izbrisati',
button_done : 'Obavljeno',
button_grade_task : 'Ocijena',
button_next : 'Sljedeće',
button_no : 'Ne',
button_prev : 'Prijašnje',
button_save : 'Sačuvati',
button_submit : 'Podnijeti',
button_upload_file : 'Dodaj datoteku',
button_yes : 'Da',
calendar : 'Kalendar',
category : { 
0 : 'Kategorija',
1 : 'Poslovanje u ugostiteljstvu',
10 : 'Priroda i znanost',
11 : 'Filozofija',
12 : 'Historija, Kultura i Vjera',
13 : 'Jezici',
15 : 'Drugo',
2 : 'Umjetnost, dizajn i arhitektura',
3 : 'Posao',
4 : 'Inžinjering i informacione tehnologije',
6 : 'Zakon',
7 : 'Prosvjeta',
8 : 'Socijalna znanost',
9 : 'Liječništvo  i Ljekarništvo'},
change_language : 'Promjeni jezik',
city : 'Grad',
completed_courses : 'Kompletirani tečajevi',
completed_tasks : 'Kompletirani zadaci',
course : 'Tečaj',
course_fname_limit : 'Naziv tečaja prevazilazi ograničenje od 120 karaktera',
course_sname_limit : 'Skraćeni naziv tečaja prevazilazi ograničenje od 40 karaktera',
courses : 'Tečajevi',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'Izbriši zadatak',
delete_task_message : 'Sva obavještenja koja se odnose na ovaj zadatak će biti izbrisana.  Da li ste sigurni da želite da izbrišete ovaj zadatak?',
edit : 'Izmjeniti',
edit_cancel2 : 'Odustati od stvaranja tečaja? Kreirani sadržaj će biti izgubljen',
email : 'Elektronska pošta',
email_daily : 'Dnevno',
email_immediately : 'Odmah',
email_never : 'Nikad',
email_when : 'Javi e-poštom kada',
event_delete : 'Izbrisati događaj?',
file_submissions : 'Podnošenje datoteke',
fill_missing_fields : 'Molim vas ispunite prazna polja',
follow_us : 'Slijedite nas na',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
forum : 'Diskusije',
forum_attachment : 'Dodaj fajl',
grade_publish1 : 'Ocijene nisu još objavljene',
grade_publish2 : 'Ocijene su objavljene',
graded_tasks : 'Ocijenjeni zadaci',
grades : 'Ocijene',
helpdesk : 'Potpora',
import_error : 'Postoji problem u uvozu backupa za tečaj . Molimo provjerite da li je ovo validan Moodle tečaj  i ako je potrebno obratite nam se za podršku.',
import_error_tasks : 'Problemi prilikom uvoza zadataka. Provjerite da li su točni zadaci uvezeni. Možete izbrisati zadatke otvaranjem stranice-zadaci i izabrati strelicu meni pored Edit-gumba.',
import_moodle : 'Uvezi tečaj iz Moodle formata',
import_processing : 'Obrada podataka tečaja. Pričekajte trenutak.',
import_uploading : 'Prijenos datoteke',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? n + " dana" : n + " dana";
                }
                else if (n < 0)
                {
                    n = -n;

                    return n == 1 ? n + " dan" : n + " dana";
                }
                else
                {
                    return "danas";
                }
            },
instructions : 'Naredbe',
join_email : 'Pošaljite pozive na ove email adrese',
join_email_sent : 'Poziv je poslat. Učesnici će biti navedeni na ovoj stranici nakon što prihvate poziv',
join_participants : 'Učesnici',
label_attachment : 'Naslov priloga',
label_course_details : 'Detalji tečaja',
label_description : 'Opis',
label_duedate : 'Datum dospijeća',
label_export : 'Izvesti',
label_finish : 'Završiti',
label_link : 'Link',
label_picture : 'Fotka',
label_share : 'Podijeli',
label_start : 'Početi',
label_status_failed : 'Neuspjelo',
label_teacher : 'Moderator',
label_title : 'Naslov',
logo_default : 'Koristi podrazumijevano',
logout : 'Odjava',
n_days_late : function (n)
            {
                return n == 1 ? n + " dan kasni" : n + " dana kasni";
            },
new_content : 'Dodaj sadržaj',
new_file : 'Datoteka',
new_task1 : 'Naredbe',
new_task2 : 'Podnesci će se prikazati ovdje',
no_due_date : 'Nema datuma dospijeća',
ongoing_courses : 'Tekući tečajevi',
overview : 'Sadržina',
participants : 'Učesnici',
password : 'Lozinku',
privacy : 'Polisa privatnosti',
profile_label_title : 'Naslov',
quiz_answer : 'Opcija za odgovore',
quiz_error_answer : 'Opcije za odgovore ne mogu biti prazna',
quiz_error_question : 'Pitanja ne mogu biti prazna',
quiz_error_select : 'Morate odabrati najmanje jedan točan odgovor',
quiz_finished : 'Završeni kvizovi',
quiz_incorrect : 'Netočan',
quiz_instructions : 'Imate samo jedan pokušaj da završite ovaj kviz.',
quiz_question : 'Upit',
quiz_results : 'Točno',
quiz_select_tf : 'Točan odgovor',
returned_tasks : 'Vraćeni zadaci',
role_student : 'Student',
settings : 'Postavke',
share_facebook : 'Podijeliti na Facebook-u',
share_join_text : 'Pridružite se tečaju u Eliademi',
share_twitter : 'Podijeliti na Twitter-u',
sidebar_edit_profile_button : 'Uredite profil',
submitted : 'Dostavljeno',
tab_forum : 'Diskusije',
task : 'Zadatak',
task_instructions : 'Napiši ovdje ili dodaj datoteka',
task_not_submitted : 'Ništa jošt nije podnijeto',
task_quiz : 'Kviz',
task_returned : function (n1, n2) { if (n1 == n2) {return "(sve vraćeno)";} else {return "(" + n1 + " of " + n2 + "vraćeno)";}},
task_save_draft : 'Spremiti kao skicu',
task_status1 : 'Nije još ocijenjeno',
task_status2 : 'Nije vraćeno',
task_status3 : 'Neuspjelo',
task_submissions : 'Podnesak',
tasks : 'Zadaci',
tasks_to_be_done : 'Zadaci koji treba da budu urađeni',
there_are_no_completed_tasks : 'Nema ispunjenih zadataka',
there_are_no_tasks : 'Nema zadataka',
title_grade : 'Ocijena',
tos : 'Uvjeti usluge',
update_file : 'Ažuriranje datoteke',
use_facebook_picture : 'Koristite Facebook profilnu fotku',
wiz_course_details : 'Detalji tečaja'};
});