define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'Protelum',
add : 'Adde',
add_another_file : 'Alia addere file',
add_grade_for_other_task : 'Adde gradum ad alterum munus',
add_instructions : 'Adice praeceptis',
assignment_uploaded : function (name, link, time)
            {
                return "Assignatione <a href=\"" + link + "\">" + name + "est uploaded feliciter </a>  "
                    + this.format_time(time);
            },
birthday : 'Natali',
blog : 'Blog',
button_back : 'Retro',
button_cancel : 'Cancello',
button_cancel_editing : 'Mutationes abiicias',
button_close : 'Finis',
button_delete : 'Erado',
button_discard1 : 'Errado curriculum',
button_done : 'Effectus',
button_next : 'Deinde',
button_no : 'Non',
button_prev : 'Prior',
button_save : 'Salvum',
button_submit : 'Submitto',
button_upload_file : 'Mitte objectum',
button_yes : 'Imo',
calendar : 'Kalendarium',
category : { 
0 : 'Categoria',
1 : 'Hospitium',
10 : 'Physicum',
11 : 'Philosophia',
12 : 'Historia',
13 : 'Linguis',
15 : 'Alter',
2 : 'Ars',
3 : 'Negotium',
4 : 'Cunicularius',
6 : 'Lex',
7 : 'Disciplina',
8 : 'Humanitas',
9 : 'Medicina'},
change_language : 'Linguam immutare',
city : 'Urbem',
completed_courses : 'Curriculum completus',
completed_tasks : 'Munia completus',
course : 'Curriculum',
course_fname_limit : 'Curriculum nomine magis quam CXX symbola',
course_sname_limit : 'Curriculum brevi nomen est plus quam XL symbola',
courses : 'Curriculum',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'Aufer pensum',
delete_task_message : 'Vos volo ut aufer hoc pensum?',
edit : 'Emendo',
edit_cancel2 : 'Abibit creandi curriculum?',
email : 'Email',
event_delete : 'Aufer eventum?',
export : 'Exporto',
file_submissions : 'Obiectum misit',
fill_missing_fields : 'Hoc campo vacua esse non potest,',
follow_us : 'Sequere nos in eam',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
grade_publish1 : 'Gradus sunt non publici',
grade_publish2 : 'Gradus sunt edita',
graded_tasks : 'Gradus munia',
grades : 'Gradibus',
helpdesk : 'Suppetiae',
home : 'Domus',
import_moodle : 'In Moodle formo importo cursus ',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? n + " diem ante" : n + " diebus ante";
                }
                else if (n < 0)
                {
                    n = -n;

                    return n == 1 ? n + " die" : n + " diebus";
                }
                else
                {
                    return "hodie";
                }
            },
instructions : 'Instructiones',
join_email : 'Mitte inuitamenta his affatibus demonstravit:',
join_email_sent : 'Invitare mittuntur',
join_participants : 'Particepes',
join_private : 'Invitationes tantum',
join_public : 'Publicam aditum',
label_attachment : 'Title attachiamenta',
label_description : 'Circa',
label_duedate : 'Date debitum',
label_finish : 'Termino',
label_link : 'URL:',
label_picture : 'Imago',
label_share : 'Impertio',
label_start : 'Incoho',
label_teacher : 'Instructor',
logo_default : 'Uti defaltam',
logo_info : 'Logo can be PNG, JPEG or GIF object. Maximum size is 500kb. Image will be changed to 192x76 dots.',
logout : 'Exitus',
n_days_late : function (n)
            {
                return n == 1 ? n + " sero die" : n + " diebus tarde";
            },
new_content : 'Adicere',
new_file : 'Obiectum',
new_task1 : 'Instructiones ',
new_task2 : 'Dicetur hic submissionibus',
new_write : 'Hic scribere.',
no_due_date : 'Date nihil deberi',
notes : 'Noto',
ongoing_courses : 'Curriculum permanentis',
overview : 'Curriculum',
participants : 'Particepes',
password : 'Signum ',
privacy : 'Privacy Policy',
publish : 'Invulgo',
quiz_answer : 'Respondeo optionem',
quiz_error_answer : 'Responsum addere praeferentiae',
quiz_error_question : 'Facere quaestionem',
quiz_error_select : 'Vos have lego unus saltem rectam responsum.',
quiz_finished : 'Complevit quaestionario',
quiz_incorrect : 'Falsus',
quiz_instructions : 'Habetis unum tantum conatum',
quiz_question : 'Interrogo',
quiz_results : 'Emendatus',
quiz_select_tf : 'Justificus responsum',
returned_tasks : 'Reversus munia',
role_student : 'Alumnus',
settings : 'Collocation',
share_facebook : 'Mitte via Facebook',
share_join_text : 'Adiungat curriculum in Eliademy:',
share_twitter : 'Mitte via Twitter',
sidebar_edit_profile_button : 'Emendo notitia',
submitted : 'Summitto',
task : 'Muneris',
task_draft : 'Emendo poculum',
task_instructions : 'Hic scribere lima vel upload',
task_late1 : '%d diebus nuper redierunt',
task_late2 : '%d horarum nuper redierunt',
task_not_submitted : 'Nihil tamen subjecti',
task_quiz : 'Quaestionario',
task_returned : function (n1, n2) {if (n1 == n2) {return "(Omnes conversi)"; } else { return "(" + n1 + " ex " + n2 + " reversus)";}},
task_save_draft : 'Salvum fac sicut potio',
task_status1 : 'Non tamen gressibilia',
task_status2 : 'Non est reversa',
task_submissions : 'Mittens',
task_submit_changes : 'Mutationes subire',
task_text : 'Litteralique',
tasks : 'Munia',
tasks_files : 'Reversi sunt objecta',
tasks_to_be_done : 'Muneris fieri',
there_are_no_completed_tasks : 'Sunt non completur munia',
there_are_no_tasks : 'Non sunt munia',
update_file : 'Modificant obiectum',
use_facebook_picture : 'Uti simulacrum de Facebook',
wiz_description : 'Circa hoc curriculum',
wiz_new_course : 'Novum curriculum',
wiz_step1 : '1. Committitur novum curriculum'};
});