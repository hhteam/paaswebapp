define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'Về chúng tôi',
add : 'Thêm ',
add_another_file : 'Thêm tập tin khác ',
add_file : 'Thêm tập tin',
add_grade_for_other_task : 'Thêm điểm cho bài tập khác ',
add_instructions : 'Thêm hướng dẫn ',
analytics : 'Môn phân tích',
assignment_uploaded : function (name, link, time)
            {
                return "Bài tập <a href=\"" + link + "\">" + name + "</a> ay matagumpay na na-upload noong "
                    + this.format_time(time);
            },
banner_productupdate : 'Eliademy đã được cập nhật.',
birthday : 'Sinh nhật ',
blog : 'Blog ',
button_back : 'Quay lại ',
button_cancel : 'Hủy bỏ ',
button_cancel_editing : 'Loại bỏ thay đổi ',
button_close : 'Đóng',
button_delete : 'Xoá',
button_done : 'Hoàn tất ',
button_grade_task : 'Điểm ',
button_next : 'Tiếp tục ',
button_no : 'Không ',
button_open_nwin : 'Mở trong cửa sổ mới',
button_prev : 'Về trước ',
button_save : 'Lưu ',
button_submit : 'Gửi',
button_upload_file : 'Tải tập tin lên ',
button_yes : 'Có ',
calendar : 'Lịch ',
calexport : 'Đăng ký lịch',
category : { 
0 : '- Phân loại -',
1 : 'Quản lý khách sạn',
10 : 'Thiên nhiên & khoa học',
11 : 'Triết học',
12 : 'Lịch sữ, văn hóa & tôn giáo',
13 : 'Ngôn ngữ',
15 : 'Khác',
2 : 'Kiến trúc, nghệ thuật & thiết kế',
3 : 'Kinh doanh',
4 : 'Kỹ thuật & IT',
6 : 'Luật',
7 : 'Giáo dục',
8 : 'Khoa học xã hội',
9 : 'Y dược'},
change_language : 'Ngôn ngữ',
city : 'Thành phố ',
completed_courses : 'Các khóa học đã hoàn tất ',
completed_tasks : 'Các bài tập đã hoàn tất ',
course : 'Khóa học ',
course_delete_confirmation : 'DELETE',
course_fname_limit : 'Tên khóa học không được vượt quá 120 ký tự ',
course_public_fee : '€',
course_sname_limit : 'Tết tắt khóa học không được vượt quá 40 ký tự ',
courses : 'Các khóa học ',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'Xoá tác vụ',
delete_task_message : 'Tất cả thông tin về bài tập này sẽ bị xóa đi. Bạn có chắc là muốn xóa bài tập này không? ',
edit : 'Hiệu chỉnh',
edit_cancel2 : 'Hủy khóa học vừa tạo? Nội dung vừa được tao sẽ không được lưu lại. ',
email : 'Email ',
email_section : 'Nội dung đã thay đổi',
err_file_empty : 'Tập tin rỗng',
error_unknown : 'Á! Có cái gì đó sai.',
event_delete : 'Xóa sự kiện? ',
export : 'Xuất ',
faq : 'FAQ',
file_submissions : 'Gởi tập tin ',
fill_missing_fields : 'Xin hãy điền vào các ô còn thiếu ',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
forum : 'Thảo luận ',
forum_attachment : 'Thêm tập tin khác ',
grade_publish1 : 'Điểm chưa được công bố ',
grade_publish2 : 'Điểm đã được công bố ',
graded_tasks : 'Bài tập đã chấm điểm ',
grades : 'Điểm ',
helpdesk : 'Hỗ trợ ',
home : 'Trang chủ',
import_error : 'Thêm nội dung',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? n + " ngày đã qua" : n + " ngày đã qua";
                }
                else if (n < 0)
                {
                    n = -n;

                    return n == 1 ? n + " ngày" : n + " ngày";
                }
                else
                {
                    return "hôm nay";
                }
            },
instructions : 'Hướng dẫn ',
is_awarded : 'ให้รางวัลกับ',
join_email : 'Gởi thư mời đến những địa chỉ email sau: ',
join_email_sent : 'Thư mời\' đã được gởi. Những người tham dự sẽ được liệt kê trong trang này sau khi họ chấp nhận lời mời. ',
join_participants : 'Người tham gia ',
label_attachment : 'Tiêu đề của tập tin kèm ',
label_course_details : 'Chi tiết khóa học ',
label_description : 'Mô tả ',
label_duedate : 'Thời hạn ',
label_licence_nonfree : '© Copyright $INSTRUCTOR. All rights reserved.',
label_link : 'Liên kết:',
label_org_user : '-',
label_picture : 'Hình ảnh ',
label_share : 'Chia sẻ',
label_start : 'Bắt đầu',
label_status_failed : 'Rớt ',
label_teacher : 'Giảng viên ',
label_title : 'Tựa đề',
label_unpublished : 'Ẩn',
liveclientcompat_dialog_message_li2 : 'Adobe Flash Player ติดตั้งและล่าสุดแล้ว',
load_more : 'Tải thêm',
logo_default : 'Dùng mặc định ',
logout : 'Thoát ',
n_days_late : function (n)
            {
                return n == 1 ? n + " ngày trễ" : n + " ngày trễ";
            },
new_content : 'Thêm nội dung ',
new_file : 'Tập tin ',
new_task1 : 'Hướng dẫn',
new_task2 : 'Bài đã gởi sẽ hiện ra ở đây ',
new_write : 'Nhập ở đây. ',
no_due_date : 'Không thời hạn ',
notes : 'Sổ ghi chép ',
ongoing_courses : 'Các khóa học đang diễn ra ',
overview : 'Nội dung ',
participants : 'Người tham gia ',
privacy : 'Chính sách bảo mật ',
profile_label_title : 'Tựa đề',
publish : 'Công bố ',
quiz_answer : 'Tùy chọn trả lời ',
quiz_error_answer : 'Không thể bỏ trống tùy chọn trả lời. ',
quiz_error_select : 'Bạn phải chọn ít nhật một câu trả lời đúng. ',
quiz_finished : 'Các bài kiểm tra đã kết thúc ',
quiz_instructions : 'Bạn chỉ có một lần thử để hoàn tất bài kiểm tra này. ',
quiz_question : 'Câu hỏi',
quiz_results : 'Đúng',
remove_user_msg : 'ผู้ใช้คนนี้จะถูกยกเลิกการลงทะเบียนในคอร์สขององค์กรทั้งหมด เจ้าของบัญชีจะกลายเป็นผู้สอนหลักในคอร์สที่ผู้ใช้คนนี้สร้าง การดำเนินการนี้ไม่สามารถยกเลิกได้ กรุณาพิมพ์ {{confirmationstring}} เพื่อยืนยัน',
returned_tasks : 'Các bài tập đã nộp ',
revoke : 'Hủy bỏ',
role_student : 'Học viên ',
settings : 'Tùy chỉnh ',
share_facebook : 'Chia sẻ trên Facebook ',
share_join_text : 'Tham gia một khóa học tại Eliademy: ',
share_twitter : 'Chia sẻ trên Twitter ',
sidebar_edit_profile_button : 'Sửa hồ sơ',
site_name : 'Eliademy',
submitted : 'Đã gửi',
tab_forum : 'Thảo luận ',
task : 'Bài tập ',
task_instructions : 'Viết vào đây hay tải tập tin lên ',
task_not_submitted : 'Chưa gởi bài ',
task_quiz : 'Bài kiểm tra ',
task_returned : function (n1, n2) {if(n1 == n2) {return "(Tất cả đã nộp bài)";} else {return "(" + n1 + " trên " + n2 + "người đã nộp bài)";}},
task_save_draft : 'Lưu dạng nháp',
task_status1 : 'Chưa chấm điểm',
task_status2 : 'Chưa nộp bài ',
task_status3 : 'Thất bại',
tasks : 'Bài tập ',
tasks_to_be_done : 'Các bài tập cần làm ',
there_are_no_tasks : 'Không có bài tập nào ',
title_grade : 'Điểm ',
update_file : 'Cập nhật tập tin ',
use_facebook_picture : 'Dùng hình đại diện trong Facebook ',
user_remove_confirmation : 'REMOVE',
user_unenroll_confirmation : 'UNENROLL',
wiz_course_details : 'Chi tiết khóa học '};
});