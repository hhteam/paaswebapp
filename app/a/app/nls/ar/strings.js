define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'من نحن',
add : 'إضافة',
add_another_file : 'إضافة ملف آخر',
add_file : 'إضافة ملف',
add_grade_for_other_task : 'إضافة درجة لمهمة أخرى',
add_instructions : 'إضافة تعليمات',
assignment_uploaded : function (name, link, time)
            {
                return "تم رفع المادة المخصصةتم <a href=\"" + link + "\">" + name + "</a> بنجاح في تمام"
                    + this.format_time(time);
            },
birthday : 'عيد ميلاد',
blog : 'مدونة',
button_back : 'رجوع',
button_cancel : 'إلغاء',
button_cancel_editing : 'إلغاء تغييرات',
button_close : 'غلق',
button_delete : 'حذف',
button_done : 'تم',
button_grade_task : 'الدرجة',
button_next : 'التالي',
button_no : 'لا',
button_prev : 'السابق',
button_save : 'حفظ',
button_submit : 'إرسال',
button_upload_file : 'رفع ملف',
button_yes : 'نعم',
calendar : 'التقويم',
category : { 
0 : '- الفئة -',
1 : 'الفن والتصميم وفن العمارة',
10 : 'الطبيعة والعلوم',
11 : 'الفلسفة',
12 : 'التاريخ والحضارات والأديان',
13 : 'اللغات',
14 : 'أخرى',
2 : 'الأعمال التجارية',
3 : 'الهندسة وتكنولوجيا المعلومات',
4 : 'السياحة',
5 : 'الإدارة الفندقية',
6 : 'القانون',
7 : 'التعليم',
8 : 'العلوم الاجتماعية',
9 : 'الطب والصيدلة'},
change_language : 'تغيير اللغة',
city : 'المدينة',
completed_courses : 'دورات تعليمية مكتملة',
completed_tasks : 'مهام مكتملة',
course : 'دورة تعليمية',
course_fname_limit : 'اسم الدورة التعليمية يتجاوز حد الـ 120 علامة',
course_sname_limit : 'الاسم المختصر للدورة التعليمية يتجاوز حد الـ 40 علامة',
courses : 'الدورات',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'حذف مهمة',
delete_task_message : 'سيتم حذف جميع المعلومات المتعلقة بهذه المهمة. هل أنت متأكد من أنك تريد حذف هذه المهمة؟',
edit : 'تحرير',
edit_cancel2 : 'المغادرة دون إنشاء دورة تعليمية؟ المحتوى المُنشأ بالفعل سيُفقَد.',
email : 'البريد الإلكتروني',
event_delete : 'حذف حدث؟',
file_submissions : 'إرسالات ملف',
fill_missing_fields : 'رجاءً املأ الخانات الناقصة.',
follow_us : 'تابعنا على',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
forum : 'مناقشات',
forum_attachment : 'إضافة ملف',
grade_publish1 : 'لم يتم بعد نشر الدرجات',
grade_publish2 : 'تم نشر الدرجات',
graded_tasks : 'مهام مصنفة بالدرجات',
grades : 'درجات',
helpdesk : 'دعم',
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? "منذ " + n + " يوم" : "منذ " + n + " أيام";
                }
                else if (n < 0)
                {
                    n = -n;
                    return n == 1 ? n + " يوم" : n + " أيام";
                }
                else
                {
                    return "اليوم";
                }
            },
instructions : 'تعليمات',
join_email : 'إرسال دعوات لعناوين البريد الإلكتروني هذه:',
join_email_sent : 'تم إرسال الدعوة. سيتم تسجيل لائحة بالمشاركين على هذه الصفحة بعد قبولهم للدعوات.',
join_participants : 'المشاركون',
label_attachment : 'عنوان المرفقة',
label_course_details : 'تفاصيل الدورة التعليمية',
label_description : 'وصف',
label_duedate : 'الموعد المقرر',
label_export : 'تصدير',
label_finish : 'إنهاء',
label_link : 'الرابط:',
label_picture : 'صورة',
label_share : 'مشاركة',
label_start : 'بدء',
label_status_failed : 'فشل',
label_teacher : 'المساعد',
label_title : 'العنوان',
logo_default : 'استخدام القياسي',
logo_info : 'الشعار يمكن أن يكون ملف PNG أو JPEG أو GIF. الحجم الأقصى 500 كيلوبايت.سيتم إعادة تحجيم الصورة بحيث تلائم مقاس 192‏x‏76 بيكسل.',
logout : 'تسجيل الخروج',
n_days_late : function (n)
            {
                return n == 1 ? n + " يوم تأخير" : n + " أيام تأخير";
            },
new_content : 'إضافة محتوى',
new_file : 'ملف',
new_task1 : 'تعليمات',
new_task2 : 'سيتم عرض الإرسالات هنا',
no_due_date : 'لا يوجد موعد مقرر',
ongoing_courses : 'الدورات التعليمية المستمرة',
overview : 'المحتوى',
overview_content : 'نظرة عامة',
participants : 'المشاركون',
password : 'كلمة المرور',
privacy : 'سياسة الخصوصية',
profile_label_title : 'العنوان',
quiz_answer : 'خيار الإجابة',
quiz_error_answer : 'لا يمكن ترك خيارات الإجابة خالية.',
quiz_error_question : 'لا يمكن ترك الأسئلة خالية.',
quiz_error_select : 'يجب أن تختار إجابة واحدة صحيحة على الأقل.',
quiz_finished : 'اختبارات تم إنهاؤها',
quiz_incorrect : 'غير صحيح',
quiz_instructions : 'لديك محاولة واحدة فقط لإنهاء هذا الاختبار الموجز.',
quiz_question : 'سؤال',
quiz_results : 'صحيح',
quiz_select_tf : 'إجابة صحيحة',
returned_tasks : 'مهام مُرجَعة',
role_student : 'طالب',
settings : 'إعدادات',
share_facebook : 'مشاركة على الفيسبوك',
share_join_text : 'الاشتراك في دورة تعليمية بموقع Eliademy:',
share_twitter : 'مشاركة على تويتر',
sidebar_edit_profile_button : 'تحرير ملف تعريف',
submitted : 'تم الإرسال',
tab_forum : 'مناقشات',
task : 'مهمة',
task_instructions : 'اكتب هنا أو ارفع ملفات',
task_not_submitted : 'لم يتم بعد إرسال شيء',
task_quiz : 'اختبار موجز',
task_returned : function (n1, n2) {if(n1 == n2) {return "(تم إرجاع الكل)";} else {return "(" + n1 + " من " + n2 + "تم إرجاع)";}},
task_save_draft : 'حفظ كمسودة',
task_status1 : 'لم يتم بعد إعطاء درجة',
task_status2 : 'لم يتم الإرجاع',
task_status3 : 'فشل',
task_submissions : 'إرسال',
tasks : 'مهام',
tasks_to_be_done : 'المهام المراد تنفيذها',
there_are_no_completed_tasks : 'لا توجد مهام مكتملة',
there_are_no_tasks : 'لا توجد مهام',
title_grade : 'الدرجة',
tos : 'اشتراطات الخدمة',
update_file : 'تحديث ملف',
use_facebook_picture : 'استخدام صورة ملف التعريف بالفيسبوك',
wiz_course_details : 'تفاصيل الدورة التعليمية'};
});