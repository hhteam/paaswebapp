define(["app/tools/utils"], function (Utils) { 
 return { 
add_live_session : 'Programmer un webinaire',
assignment_uploaded : function (name, link, time)
{
return "Devoir <a href=\"" + link + "\">" + name + "</a> a été transféré avec succès au "+ this.format_time(time);
},
btn_retake_quiz : 'Veuillez réessayer',
button_add_billing : 'Ajouter les détails de facturation',
button_apply_ngo : 'Envoyer demande',
button_enroll_users : 'Inscrire des utilisateurs de l\'organisation',
button_grade_task : 'Note',
button_never_remind : 'Ne pas rappeler',
button_pro_go : 'Devenez Premium',
button_submit : 'Envoyer',
certificate_share_text : 'J\'ai reçu un Certificat d\'Achèvement du cours %COURSENAMME @Eliademy',
clear_all_notifications : 'Tout marquer comme lu',
clone_error_modules : 'Malheureusement les vidéos privées et les applications LTI ne peuvent pas être réutilisées.',
completed_description_delete_nonempty : 'Afin de supprimer le cours public, veuillez notifier tous les étudiants en leur envoyant un message via l\'écran Participants. Ensuite changez le type du cours par Sur invitation uniquement et cliquez sur le bouton Supprimer.',
country : 'Pays',
course_category : 'Catégorie du cours',
course_delete_body : 'Veuillez taper le mot "{{confirmationstring}}" dans l\'espace texte qui suit afin de confirmer la suppression du cours {{coursename}} :',
course_delete_header : 'Supprimer ce cours ?',
course_end_confirmation_body : 'Le cours affichera "Terminé" pour tous les étudiants. Terminer un cours n\'empêche pas les étudiants de le voir et les enseignants de l\'éditer.',
course_end_confirmation_header : 'Quitter ce cours ?',
course_inviteonly : 'Sur invitation seulement',
course_paid : 'Payé',
early_live_session_emsg : 'Un webinaire peut être rejoint jusqu\'à 15 minutes avant son commencement.',
edit_cancel : 'Êtes-vous sûr de vouloir abandonner les changements non sauvegardés ?',
email_custom_text : 'Ecrivez ici les messages que vous souhaitez envoyer aux participants du cours',
enroll_groups_message : 'Inscrire un groupe à un cours mènera à l\'inscription automatique de tous les utilisateurs actuels et futurs assignés à ce groupe.',
err_uploaded_file_exceeds_limit : 'Impossible de télécharger le fichier car il excède la limite de fichier %LIMIT MB',
error_file_type : 'Seuls les fichiers de type %TYPES sont acceptés',
error_payment : 'Nous ne sommes pas en mesure de procéder au paiement. Veuillez vérifier vos coordonnées bancaires ou contacter notre équipe d\'assistance.',
error_service : 'Nous n\'avons pas été dans la mesure de sauvegarder les changements que tu as effectués. Vérifies ta connexion Internet et réessayes.',
error_unknown : 'Désolé, quelque chose s\'est mal passé',
fill_billing_details_invoice_option : 'Si vous souhaitez payer par facture commerciale, contactez nous à support@eliademy.com.',
gradebook : 'Cahier de notes',
import_tasks : function (name, taskcount) { 
return "Cours <b>%name</b> dispose de %tasks tâches que vous souhaitez importer:".replace('%name', name).replace('%tasks', taskcount);
},
in_days : function (n) {
if (n > 0) {
return n == 1 ? n + " il ya jour" : n + " il ya jours";
} else if (n < 0) {
n = -n;

return n == 1 ? n + " jour" : n + " jours";
} else {
return "aujourd'hui";
}
},
invited_on : 'Invité le',
label_action : 'Action',
label_add_attachments : 'Pour joindre des fichiers {sélectionner des fichiers de votre ordinateur}| ou {Google Docs}|. Vous pouvez également {intégrer Edu App}| ou {charger une vidéo}|.',
label_address : 'Adresse',
label_billing_country : 'Pays de facturation',
label_billing_details : 'Détails de facturation',
label_browse : 'Parcourir',
label_cc_details : 'Détails de la carte de crédit',
label_choose_public_course : 'Choisissez un cours public que vous pouvez réutiliser',
label_complete_purchase : 'Effectuer l\'achat',
label_complete_purchase_legal : 'En faisant cet achat, j\'accepte les {Conditions d\'Utilisation} et la {Politique de Confidentialité} d\'Eliademy. Vous serez facturé immédiatement.',
label_confirm_remove_user : 'Êtes-vous sûr de vouloir retirer cet utlisateur',
label_course_details : 'Détails du cours',
label_course_licence : 'Licence du contenu du cours',
label_course_type : 'Type de cours',
label_details : 'Détails',
label_ends_on : 'Finit le',
label_free_days_left : 'Jours gratuits restants',
label_if_applicable : 'si applicable',
label_invoice : 'Facture',
label_invoice_num : 'Facture #',
label_invoices : 'Factures',
label_last_invoice : 'Dernière facture',
label_licence_cc_info : 'Le contenu peut être réutilisé avec ou sans modification par n\'importe quel instructeur sur Eliademy',
label_licence_nonfree : '© Copyright $INSTRUCTOR. Tous droits réservés.',
label_next_billing : 'Prochaine facture le',
label_no_courses : 'Il n\'y a aucun cours à afficher',
label_no_invoices : 'Pas encore de factures',
label_not_entered : 'Pas encore entré',
label_offer_details : 'Détails de l\'offre',
label_order_total : 'Total de la commande',
label_participants_no_students : 'Ici vous pourrez voir tous les collègues et étudiants qui ont rejoint votre cours.',
label_participants_send_invitations : 'Vous pouvez inviter le premier participant dès maintenant.',
label_print : 'Imprimer',
label_pro_payment_failed : 'Le paiement a échoué',
label_pro_payment_failed_msg : 'Il nous est impossible de collecter le paiement pour Eliademy Premium. Toutes les fonctionnalités Premium ont donc été désactivées, mais vous avez toujours accès à tous vos cours. Veuillez vérifier que vous avez fourni une méthode de paiement valide.',
label_progress_send_invitations : 'Tu peux envoyer ta première invitation depuis l\'onglet Participants.',
label_quantity : 'Quantité',
label_quiz_end : 'ou soumettre cette tentative',
label_quiz_no_results1 : 'Vous pouvez voir les résultats des étudiants ici. Si vous souhaitez modifier le quiz, cliquez sur le bouton Modifier.',
label_quiz_no_results2 : 'Veuillez noter que vous ne pouvez pas modifier le nombre de questions des quiz lorsqu\'un étudiant ou plus les a terminés.',
label_see_all_invoices : 'Voir toutes les factures',
label_status : 'Statut',
label_status_delivered : 'Délivré',
label_status_failed : 'Erreur',
label_status_not_paid : 'Impayé',
label_status_paid : 'Payé',
label_status_sending : 'Envoi',
label_status_sent : 'Envoyé',
label_subject_vat : 'Sujet à la TVA de l\'UE',
label_subtotal : 'Sous-total',
label_title : 'Titre',
label_trial_expired : 'Votre essai Premium s\'est terminé',
label_trial_expired_message : 'Votre période d\'essai d\'Eliademy Premium est terminée et vous n\'avez pas renseigné vos informations bancaires. Toutes les fonctionalités Premium sont désactivées, mais vous avez toujours accès à tous vos cours. Si vous souhaitez continuer d\'utiliser la version Premium, veuillez entrer vos informations bancaires ci-dessous.',
label_unit_price : 'Prix à l\'unité',
label_unpublished : 'Нуугдсан',
label_vat : 'TVA',
label_vat_id : 'Numéro d\'identification TVA',
lapsed_live_session_emsg : 'Ce webinaire est déjà terminé.',
lead_instructor : 'Instructeur principal',
link_payment_method : 'Moyen de paiement',
link_plan_details : 'Détails du plan',
live_timeerror : 'La durée de tous les webinaires est limitée à 3 heures.',
live_timezone_reminder : 'Prenez note que tous les événements sont affichés dans le fuseau horaire %TIMEZONE. Vous pouvez modifier votre fuseau horaire dans Paramètres.',
liveevents : 'Webinaires',
manage_courses : 'Gérer les inscriptions',
manage_users : 'Gérer les utilisateurs',
monthly_payment : 'Paiement mensuel',
msg_select_course_category : 'Veuillez choisir une catégorie de cours.',
n_days_late : function (n) {
return n == 1 ? n + " jour en retard" : n + " jours en retard";
},
new_live_session : 'Nouveau webinaire',
ngo_account_message : 'Vous bénéficiez de l\'utilisation gratuite d\'Eliademy Premium en tant qu\'organisation à but non lucratif ou institution académique publique. La politique d\'utilisation équitable est applicable.',
no_courses_teacher_more_info : 'Apprenez comment créer votre premier cours en regardant cette vidéo:',
no_lapsed_live_sessions : 'Vous n\'avez aucun webinaire de programmé.',
no_live_sessions : 'Lorsque vous finirez un webinaire, il apparaîtra ici.',
no_public_no_share_msg : 'Vous pouvez seulement partager les cours publics gratuits ou payants sur les réseaux sociaux.',
not_graded_tasks : 'Tâches actuelles',
notifications_placeholder : 'Vous n\'avez aucune Notification',
past_live_sessions : 'Précédents webinaires',
picture_info : 'Les images des cours peuvent être en format PNG, JPEG ou GIF. Leur taille maximum est %s MB. L\'image sera redimensionnée pour correspondre à 845x170 pixels. Vous pourrez l\'ajuster en la déplaçant vers le haut ou vers le bas dans la fenêtre d\'aperçu.',
pro_course_pricacy_label_more : 'Si vous êtes à la recherche d\'un espace d\'apprentissage sécurisé, commencez une période d\'essai d\'Eliademy de 30 jours, dès maintenant.',
pro_webinar_label : 'Les webinaires avec partage d\'écran et de présentation requièrent une souscription à Eliademy Premium.',
profile_label_title : 'Titre',
quiz_correct : 'Votre bonne réponse',
quiz_correct_answer : 'Une des bonnes réponses',
quiz_retake : 'Vous pouvez passer ce test {%attemptsleft%} fois de plus:',
quiz_retake_last : 'Vous pouvez effectuer ce quiz une fois de plus:',
quiz_unlimited : 'Vous pouvez passer ce quiz autant de fois que vous le désirez:',
rate_course_msg : 'Si vous avez apprécié le cours %COURSENAME, prenez un moment pour l\'évaluer et le recommander à vos amis.',
remove : 'Supprimer',
reuse_instructions : 'Ou choisissez un cours que vous avez précédemment créé',
revoke_access_for : 'Retirer l\'accès pour %SERVICE',
self_paced_description : 'Le cours est adapté au rythme de chacun. Les étudiants peuvent commencer à le suivre à n\'importe quel moment et apprendre par eux-mêmes.',
share_completed_course_label : 'Vous avez terminé avec succès le cours %COURSENAME. Prenez un moment pour l\'évaluer et le partager avec vos amis',
share_completed_course_msg : 'J\'ai terminé le cours %COURSENAME @Eliademy',
share_course_certificate_msg : 'J\'ai reçu un certificat %CERTURL @Eliademy pour le cours %COURSENAME',
share_course_student_msg : 'J\'apprécie ce cours %COURSENAME @Eliademy. Vous pourriez aussi être intéressé.',
share_course_teacher_msg : 'J\'enseigne ce cours %COURSENAME sur @Eliademy. Vous devriez nous rejoindre.',
share_on : 'Partager sur :',
sidebar_new_features : 'Nouvelles fonctionnalités',
sidebar_new_features_msg : 'Un nouveauté est publiée chaque semaine, sur la base de vos commentaires ou de notre plan d\'action. Apprenez-en plus sur notre blog.',
sign_up_new_org : 'Commencez une période d\'essai de 30 jours',
task_returned : function (n1, n2) {
if( (n1 == n2) && n2 ) {
return "(Tous rendus)";} 
else {return "(" + n1 + " sur " + n2 + " rendus)";}
},
task_status3 : 'Erreur',
task_your_answer : 'Votre réponse',
task_your_answer_start : 'Commence à rédiger ta réponse maintenant.',
there_are_no_graded_tasks : 'Lorsque toutes les soumissions auront été évaluées, la tâche sera affichée ici.',
there_are_no_ungraded_tasks : 'Ici vous verrez toutes les tâches nécessitant d\'être notées.',
thumbnailpicture_details : 'Formats supportés: jpg, png<br>Taille maximum : 500kb<br> Conseil : pour de meilleurs résultats utilisez la résolution 267x150',
title_grade : 'Note',
tooltip_disable_page : 'Lorsque la page est masquée, les participants du cours ne peuvent pas la voir.',
trial : 'Essai',
tutorial_video_certificate : 'Regardez une courte vidéo sur nos Certificats',
tutorial_video_sellcourses : 'Regardez une courte vidéo sur le paiement des inscriptions',
upcoming_live_sessions : 'Webinaires à venir',
user_unenroll_confirm_header : 'Se désinscrire',
user_unenroll_confirmation : 'SE DESINSCRIRE',
wiz_course_details : 'Détails du cours',
zip : 'Code postal'};
});