define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'Über uns',
access_ext_course_error : 'Auf den externen Kurs konnte nicht zugriffen werden. Bitte kontaktieren Sie den Support.',
activated : 'Aktivieren',
active_courses : 'Laufende Kurse',
add_grade_for_other_task : 'Note für eine andere Aufgabe geben',
add_gsers : 'Google-Nutzer',
add_instructions : 'Anweisungen hinzufügen',
add_lti_tool : 'Eine Edu App auswählen',
add_notes : 'Notizen hinzufügen',
add_social_button : 'Social Media-Konto hinzufügen',
add_social_img_button : 'Foto der Sozialen Medien verwenden',
add_to_profile : 'Zum Profil hinzufügen',
apply : 'Anwenden',
apply_for_ngo_title : 'Ermäßigung für akademische Institutionen anwenden',
assignment_uploaded : function (name, link, time)
{
return "Aufgabe <a href=\"" + link + "\">" + name + "</a> wurde erfolgreich hochgeladen "+ this.format_time(time);
},
banner_donate_more : 'Unterstützen Sie Eliademy.',
bio : 'Bio',
birthday : 'Geburtstag',
blog : 'Blog',
btn_end_quiz : 'Dieses Ergebnis übermitteln',
button_back_to : 'Zurück',
button_cancel : 'Abbrechen',
button_close : 'Schließen',
button_delete : 'Löschen',
button_delete_course : 'Kurs löschen',
button_end_now : 'Jetzt beenden',
button_enroll : 'Anmelden',
button_grade_task : 'Bewertung',
button_hide : 'Verbergen',
button_issue_all : 'Alles veröffentlichen',
button_no : 'Nein',
button_open_course : 'Open Source',
button_prev : 'Zurück',
button_reuse_course : 'Kurs wiederverwenden',
button_share : 'In sozialen Medien teilen',
button_start_new_course : 'Neuen Kurs erstellen',
button_start_trial : 'Testversion beginnen',
button_submit : 'Absenden',
button_upload_file : 'Datei hochladen',
button_view_task : 'Ansehen und benoten',
calexport : 'Kalender abonnieren',
calexport_text2 : 'Für ausführliche Anleitung für die Kalenderanwendung siehe {{faq}}.',
catalog : 'Katalog',
cert_dummy_stud : 'Name des Teilnehmers',
cert_logo_msg : 'Verwenden Sie das PNG-Bildformat und transparenten Hintergrund.',
certificate_ready : 'Das Zertifikat ist fertig',
certificate_share_text : 'Ich habe ein Teilnahmezertifikat erhalten für %COURSENAME @Eliademy',
change_password : 'Passwort ändern',
change_role : 'Rolle ändern',
checking_file : 'Datei wird überprüft...',
class_average : 'Klassendurchschnitt (%)',
clear_all_notifications : 'Alle als gelesen markieren',
clear_search : 'Suchfeld leeren',
completed : 'Abgeschlossen',
completed_percent : 'abgeschlossen',
contact : 'Kontakt',
content_engagement : 'Beschäftigung mit Kursinhalt',
copy_invite_link : 'Link kopieren',
course : 'Kurs',
course_delete_confirmation : 'LÖSCHEN',
course_delete_header : 'Wollen Sie diesen Kurs löschen?',
course_draft : 'Entwurf',
course_enable_confirmation : 'Sind Sie sicher, dass Sie den Kurs aktivieren wollen? Das Enddatum des Kurses wird gelöscht.',
course_end_confirmation_header : 'Wollen Sie diesen Kurs wirklich beenden?',
course_inviteonly : 'Nur auf Einladung',
course_keywords : 'Stichwörter zum Kurs',
course_keywords_placeholder : 'Beschreiben Sie Ihren Kurs mit 3 Stichwörtern',
course_paid : 'Bezahlt',
course_private : 'Nicht-öffentlich',
course_public_paid_description : 'Sie können dieses Quiz noch 1 Mal machen',
course_reset_progress : 'Fortschritt des Kursteilnehmers zurücksetzen',
course_reset_progress_msg : 'Sind Sie sicher, dass Sie den Fortschritt des Kursteilnehmers zurücksetzen möchten? Alle abgegebenen Aufgaben und Quizergebnisse werden gelöscht, und der Kursfortschritt wird zurückgesetzt.',
create_quiz_caption : 'Neues Quiz bei $1',
create_task_caption : 'Neue Aufgabe auf $1',
csv_format : 'CSV-Dokument (Kalkulationstabelle)',
current : 'Aktuell',
current_users : 'Benutzer',
custom_edit_lti_tool : 'Edu App bearbeiten/löschen',
date : 'Datum',
delete_group_message : 'Sind Sie sicher, dass Sie diese Gruppe {%group%} löschen wollen? Wenn Sie diese Gruppe löschen, werden die Benutzer Ihrer Organisation nicht gelöscht.',
delete_task : 'Aufgabe löschen',
design : 'Entwurf',
download : 'Herunterladen',
early_live_session_emsg : 'Einem Webinar kann 15 Minuten vor geplantem Beginn beigetreten werden.',
edit_cancel : 'Sind Sie sicher, dass Sie Ihre Änderungen nicht speichern und verwerfen wollen?',
edit_group : 'Gruppennamen bearbeiten',
edit_quiz_caption : 'Quiz bei $1 bearbeiten',
edit_task_caption : 'Aufgabe $1 bearbeiten',
email_comment : 'Rückmeldung für Aufgabe erhalten',
email_custom_text : 'Diese Seite ist nicht sichtbar für KursteilnehmerInnen',
email_immediately : 'Sofort',
email_section : 'Inhalt wurde geändert',
email_task : 'Die Kursaufgabe wurde geändert',
email_user_submitted : 'Kursteilnehmer hat eine Aufgabe abgegeben',
error_file_type : 'Es werden nur die Dateitypen %s akzeptiert',
error_notenrolled : 'Sie sind für diesen Kurs nicht angemeldet.',
error_payment : 'Wir konnten die Zahlung nicht bearbeiten. Bitte überprüfen Sie die Zahlungsdetails oder kontaktieren Sie unser Support Team.',
faq : 'FAQ',
feedback : 'Rückmeldungen',
fill_billing_details_invoice_option : 'Wenn Sie auf Firmenrechnung bezahlen wollen, kontaktieren Sie uns auf support@eliademy.com.',
follow_us : 'Folgen Sie uns auf ',
forum : 'Diskussionen',
forum_add_comment : 'Kommentar hinzufügen',
forum_attachment : 'Datei hinzufügen',
forum_confirm_cancel_post_message : 'Sind Sie sicher, dass Sie diesen Beitrag löschen möchten?',
forum_delete_body : 'Geben Sie das Wort "{{confirmationstring}}" in die Textbox ein, um das Löschen zu bestätigen.',
forum_discussion_name : 'Geben Sie einen Namen für diese Diskussion',
forum_edit_discussion : 'Diskussionsnamen bearbeiten',
forum_new : 'Neues Forum beginnen',
forum_new_discussion : 'Neue Diskussion beginnen',
forum_number_replies : 'Gesamtanzahl der Antworten',
forum_remove_post : 'Diese Nachricht löschen',
forum_total_posts : 'Anzahl der Beiträge',
forum_unread_posts : 'Nicht geselene Nachrichten',
from : 'Von',
googledrive : 'Google Drive',
grade_publish1 : 'Noch nicht veröffentlichte Noten',
grade_publish2 : 'Veröffentlichte Noten',
grade_range : 'Notenbereich (%)',
graded_tasks : 'Benotete Aufgaben',
group : 'Gruppe',
groups : 'Gruppen',
hover_to_see_more : 'bewegen Sie die Maus über die Balken, um Details zu sehen',
import_moodle : 'Kurs im Moodle-Format importieren',
import_tasks : function (name,taskcount){
return "Kurs <b>%name</b> Kurs besteht aus %tasks Aufgaben. Wählen Sie Aufgaben, die Sie importieren möchten:".replace('%name',name).replace('%tasks',taskcount);
},
import_tasks_warning : 'Der hochgeladene Kurs hat mehrere Aufgaben. Wählen Sie bitte, welche Aufgaben Sie importieren möchten.',
in_days : function (n) {
        if (n > 0) {
            return n == 1 ? n + " tag vor" : n + " vor tagen";
        } else if (n < 0) {
            n = -n;

            return n == 1 ? n + " tag" : n + " vor tagen";
        } else {
            return "heute";
        }
    },
instructions : 'Anweisungen',
instructor_issued : 'Vom Lehrer herausgegeben',
invited_on : 'Eingeladen am',
invoice_prepaid_item_description : 'Jahreszahlung für Eliademy Premium für %PERIOD (bis %COUNT Teilnehmer in nicht-öffentlichen Kursen)',
issue : 'Ausgabe',
issue_all_certs : 'Alle Zertifikate erstellen',
join_home : 'Zur Startseite gehen.',
join_participants : 'Teilnehmer',
label_account_owner : 'Besitzer des Kontos',
label_admin : 'Administrator',
label_amount : 'Gesamt',
label_billing_country : 'Rechnungsadresse Land',
label_cert_faq_a2 : 'Ja, das kann bis zum Versand gemacht werden. Der Versand erfolgt normalerweise innerhalb von 12 bis 24 Stunden nach der Bestellung. Kontaktieren Sie uns gleich auf support@eliademy.com, wenn Ihre Adresse falsch ist.',
label_cert_faq_a3 : 'Die Anzahl der bestellbaren Zertifikaten ist nicht limitiert. Ihre Daten werden von uns auf unbestimmte Zeit gespeichert, und Sie können Ihr Zertifikat auch Jahre nach dem Abschluss des Kurses ausdrucken.',
label_cert_faq_q2 : 'Kann ich die Lieferadresse ändern?',
label_cert_waiting : 'Sie können dieses Zertifikat runterladen oder bestellen, wenn alle Kursaufgaben abgeschlossen sind und der Lehrer Ihnen es ausgestellt hat.',
label_certificate_type : 'Zertifikatstyp',
label_change_account : 'Benutzerkonto wechseln',
label_comment_and_draft : 'Um Korrekturen bitten',
label_course_certificate_type : 'Wählen Sie den Kurszertifikattyp:',
label_course_completiondate : 'Abschlussdatum',
label_course_licence : 'Lizenz für Kursinhalte',
label_course_name : 'Name des Kurses',
label_course_startdate : 'Startdatum',
label_course_type : 'Kurstyp',
label_cutomer_details : 'Details des Kunden',
label_default : 'Standard',
label_delivery_failed_info : 'Leider konnten wir an diese Adresse keine Einladung schicken, bitte überprüfen Sie, dass die Adresse korrekt ist.',
label_description : 'Beschreibung',
label_enabled : 'Aktiviert',
label_ends_on : 'Endet am',
label_export : 'Export',
label_false : 'Falsch',
label_finish : 'Abschließen',
label_free_days_left : 'Noch zur Verfügung stehende Tage',
label_if_applicable : 'wenn anwendbar',
label_invoice : 'Rechnung',
label_link_paypal : 'Link mit PayPal',
label_message : 'Nachricht',
label_not_entered : 'Noch nicht eingegeben',
label_offer_details : 'Details zum Angebot',
label_order_subtotal : 'Bestellungen Gesamt',
label_participants_send_invitations : 'Sie können jetzt den ersten Teilnehmer einladen.',
label_place_student_name : 'Namen und Ort des Teilnehmers bearbeiten',
label_private_users : 'Teilnehmer in nicht-öffentlichen Kursen',
label_pro_payment_failed : 'Zahlung fehlgeschlagen',
label_pro_payment_failed_msg : 'Wir konnten keine Zahlung für Eliademy Premium einziehen. Alle Premium-Eigenschaften sind deaktiviert, aber Sie haben Zugang zu allen Kursen. Überprüfen Sie bitte, dass Sie eine gültige Zahlungsweise angegeben haben.',
label_quantity : 'Anzahl',
label_quiz_close_msg : 'Nachricht wird geschlossen',
label_quiz_end : 'oder diesen Versuch senden',
label_quiz_no_results1 : 'Sie sehen die Ergebnisse des Stundenten hier. Wenn Sie das Quiz bearbeiten wollen, drücken Sie auf die Schaltfläche Bearbeiten.',
label_quiz_no_results2 : 'Beachten Sie, dass Sie die Anzahl der Fragen nicht ändern können, wenn ein Student sie alle beantwortet hat.',
label_quiz_open_msg : 'Begrüßungstext',
label_random : 'Zufällig',
label_request_more : 'mehr anfordern',
label_sequential : 'sequenziell',
label_share : 'Teilen',
label_status_delivered : 'Geliefert',
label_status_not_paid : 'Nicht bezahlt',
label_status_paid : 'Bezahlt',
label_status_sending : 'Wird gesendet',
label_status_sent : 'Versendet',
label_subtotal : 'Zwischensumme',
label_title : 'Anrede',
label_true : 'Richtig',
label_unlimited : '-Unbegrenzt-',
label_unpublished : 'Caché',
label_vat : 'TVA',
label_vat_id : 'UID',
label_view_public_page : 'öffentliche Seite ansehen',
lable_error : 'Fehler',
lapsed_live_session_emsg : 'Dieses Webinar ist bereits abgeschlossen.',
last_access : 'Letzter Zugriff',
less : 'weniger...',
links : 'Links',
liveclientcompat_dialog_message : 'Bevor Sie mit einer Sitzung beginnen, vergewissern Sie sich',
liveevents : 'Webinare',
load_more : 'Mehr laden',
logo_info : 'Das Logo kann das Format PNG, JPEG oder GIF haben. Größe max. 500 Ko. Das Bild wird auf 192x76 Pixels geändert.',
logout : 'Abmelden',
lti_customer_key : 'Kundenschlüssel',
lti_share_username : 'Benutzernamen des Benutzers teilen',
more : 'mehr...',
n_days_late : function (n) {
        return n == 1 ? n + " tag zu spät" : n + " tage zu spät";
    },
new_task1 : 'Anweisungen',
new_task2 : 'Abgegebene Dateien werden hier gezeigt.',
no_courses_private_info : 'Hier sehen Sie alle nicht-öffentlichen Kurse, die in Ihrer Organisation verfügbar sind.',
no_courses_private_info_more : 'Lernen Sie mit diesem Video, wie Sie Benutzer verwalten und zu nicht-öffentlichen Kursen anmelden können:',
no_courses_teacher_info : 'Hier sehen Sie alle Kurse, die Sie leiten.',
no_courses_teacher_more_info : 'Lernen Sie mit diesem Video, wie Sie Ihren ersten Online-Kurs erstellen:',
no_events_msg : 'Ihre Veranstaltungen werden hier aufgelistet',
no_live_sessions : 'Wenn Sie ein Webinar abgeschlossen haben, erscheint es hier.',
not_available : 'Nicht verfügbar',
not_graded_tasks : 'Aktuelle Aufgaben',
notifications : 'Nachrichten',
number_of_enrollments : 'Anzahl der Anmeldungen',
ongoing : 'Läuft',
opensource : 'Open Source',
org_ad_picture_details : 'Unterstützte Formate: jpg, png<br>Maximale Größe: 500kb<br>Tipp: für die besten Resultate verwenden Sie 250x400',
org_analytics : 'Analyse Ihrer Organisation',
org_details : 'Details der Organisation',
org_name : 'Name der Organisation',
org_public_no_courses : 'Sie haben noch keine öffentlichen Kurse.',
org_public_page_add_category : 'Neue Kategorie hinzufügen',
org_public_page_banner1 : 'Gruppe #1',
org_public_page_banner2 : 'Gruppe #2',
org_public_page_banner3 : 'Gruppe #3',
org_public_page_cat_placeholder : 'Geben Sie hier den Namen der Kategorie ein...',
org_public_page_url_info : 'Bitte kontaktieren Sie unseren Support, wenn Sie Ihren öffentlichen URL ändern möchten',
organization : 'Organisation',
orig_password_missing : 'Das aktuelle Passwort darf nicht leer sein.',
overview_content : 'Übersicht',
participants : 'Teilnehmer',
password_change_success : 'Das Passwort wurde erfolgreich geändert.',
password_condfail : 'Das Passwort muss mindestens 8 Zeichen haben und mindestens 1 Kleinbuchstaben, 1 Großbuchstaben und eine Ziffer enthalten.',
plain_text : 'Nur Text',
position : 'Position',
press_save_to_confirm_delete : 'Klicken Sie auf Speichern, um dieses Thema zu löschen',
privacy : 'Datenschutzrichtlinien',
pro_course_pricacy_label : 'Um diesen Kurs nicht-öffentlich zu machen, brauchen Sie ein {Premium subscription}',
pro_course_pricacy_label_more : 'Wenn Sie einen sicheren Raum für Lernen suchen, starten Sie heute Ihr 30-Tage-Probeabo.',
pro_upgrade_label : 'Wenn Sie Werbeeinschaltungen entfernen, Webinare einsetzen, nicht-öffentliche Kurse leiten und zur Verbesserung von Eliademy beitragen wollen, ist das Premium-Abonnement eine gute Wahl.',
profile_label_title : 'Anrede',
profile_myprofile : 'Mein Profil',
quiz_answer : 'Antwortmöglichkeiten',
quiz_attempts : 'Anzahl der Versuche',
quiz_correct : 'Ihre richtige Antwort',
quiz_error_question : 'Fragen dürfen nicht leer sein.',
quiz_incorrect : 'Falsch',
quiz_multi_instructions : 'Sie haben {NUMBER} Versuche, um dieses Quiz abzuschließen',
quiz_new_answer : 'Antwort hinzufügen',
quiz_progress : 'Quiz-Fortschritt: $1',
quiz_question : 'Frage',
quiz_question_mc : 'Mehrfachauswahl',
quiz_reset_progress_msg : 'Wollen Sie das Quizergebnis des Teilnehmers wirklich zurücksetzen?',
quiz_results : 'richtig',
quiz_retake : 'Sie können dieses Quiz noch {%attemptsleft%} Mal versuchen.',
quiz_select_sa : 'Richtige Antworten',
quiz_select_tf : 'Richtige Antwort',
rate_course_msg : 'Wenn Ihnen der Kurs %COURSENAME gefallen hat, nehmen Sie sich bitte etwas Zeit, um ihn zu benoten und Ihren Freunden zu empfehlen.',
remove_user : 'Benutzer entfernen',
reuse_nocourse : 'Es ist leer, weil Sie noch keine Kurse haben, wo Sie unterrichten.',
revoke : 'Widerrufen',
revoke_access_for : 'Zugang für %SERVICE widerrufen',
role_student : 'KursteilnehmerIn',
search_no_results : 'Die Suche hat keine Übereinstimmungen ergeben.',
select_ltitool : 'Edu App ausgewählt',
self_paced : 'selbstgesteuert',
service_facebook : 'Facebook',
service_google_plus : 'Google+',
service_instagram : 'Instagram',
service_linkedin : 'LinkedIn',
service_live : 'Live',
service_pinterest : 'Pinterest',
service_twitter : 'Twitter',
service_vk : 'VKontakte',
service_weibo : 'Weibo',
settings_help_translate : 'Unterstützen Sie uns bei der Übersetzung',
share_completed_course_label : 'Sie haben den Kurs %COURSENAME erfolgreich abgeschlossen. Nehmen Sie sich einen Moment Zeit, um ihn zu bewerten, und teilen Sie ihn mit Ihren Freunden.',
share_course_certificate_info : 'Sie können es in {social media} teilen und in Ihr LinkedIn-Profil hinzufügen.',
share_course_teacher_msg : 'Ich unterrichte diesen Kurs %s @Eliademy. Komm einfach mit!',
share_join_text : 'Melden Sie sich für einen Eliademy-Kurs an:',
share_linkedin : 'Auf LinkedIn teilen',
sidebar_complete_profile : 'Profil vervollständigen',
sidebar_localize : 'Lokalisieren',
sidebar_own_your_lms : 'Eliademy für Fortgeschrittene',
sign_up_new_org : '30-Tage-Demoversion starten',
signature : 'Unterschrift',
site_name : 'Eliademy',
skype : 'Skype',
social : 'Sozial',
support_response : 'Vielen Dank für Ihre Anfrage. Unser Support-Team wird Sie in Kürze kontaktieren',
tab_forum : 'Diskussionen',
task : 'Aufgabe',
task_cancel : 'Alle Änderungen werden verloren gehen. Beenden, ohne die Aufgabe zu senden?',
task_end_time : 'Fällig am',
task_no_submission : 'Keine Antwort',
task_no_submission_student_info : 'Für diese Aufgabe müssen Sie keine Antworten schicken',
task_not_submitted : 'Keine abgegebenen Aufgaben',
task_options : 'Aufgabentyp:',
task_quiz : 'Quiz',
task_returned : function (n1,n2){
if((n1==n2)&&n2){return "(Alle zurückgegeben)";} 
else{return "("+n1+ " von "+n2+ " zurückgegeben)";}
},
task_start_time : 'Öffnen auf',
task_status1 : 'Noch nicht benotet',
task_status2 : 'Nicht abgegeben',
task_status3 : 'Fehlgeschlagen',
task_student_submissions : 'Ihre Antwort',
task_submission_progress : 'Fortschritt bei Aufgabenabgabe',
task_submissions : 'Übermittlung',
task_your_answer : 'Ihre Antwort',
tasks : 'Aufgaben',
teaching : 'Lehren',
text_madeinfinland : 'Made in Finland',
there_are_no_completed_tasks : 'Es gibt keine unerledigten Aufgaben',
thumbnailpicture_info : 'Die Kursvorschau wird automatisch vom Titelbild generiert. Sie können auch ein anderes Vorschaubild für Ihren Kurs hochladen. Es wird auf den Seiten Dashboard und Katalog sichtbar sein.',
time_driven : 'zeitgesteuert',
timed_content_engagement : 'Beschäftigung mit Kursinhalt im Zeitraum',
title_grade : 'Note',
to : 'An',
topic : 'Thema',
tos : 'Nutzungsbedingungen',
trial : 'Testversion',
tutorial_instructor_click_here : 'Klicken Sie hier, wenn Sie Lehrer sind',
tutorial_video_certificate : 'Ein kurzes Video über Zertifikate ansehen',
type : 'Typ',
type_paste_emails : 'E-Mailadresse (mit Kommas trennen)',
until : 'bis',
update_file : 'Datei aktualisieren',
upload_cancelled : 'Hochladen abgebrochen',
upload_video : 'Video hochladen',
use_facebook_picture : 'Facebook-Foto verwenden',
user_directory : 'Benutzer-Verzeichnis',
user_directory_ext : 'Alle Menschen',
user_directory_int : 'Organisationsteam',
user_directory_orgteam_label : 'Von einem Teammitglied erstellte Kurse werden mit dieser Organisation verlinkt, und sie können nicht-öffentlich sein. Die Organisationsteamrollen sind:',
users_with_licence : 'Angemeldet für einen nicht-öffentlichen Kurs',
welcome_msg : 'Willkommensnachricht für diese Sitzung',
wiz_course_details : 'Kursdetails',
zip : 'Postleitzahl'};
});