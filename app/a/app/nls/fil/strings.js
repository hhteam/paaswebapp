define(["app/tools/utils"], function (Utils) { 
 return { 
assignment_uploaded : function (name, link, time)
            {
                return "Asignatura <a href=\"" + link + "\">" + name + "</a> ay matagumpay na na-upload sa "
                    + this.format_time(time);
            },
button_grade_task : 'Grado',
category : { 
0 : '- Kategorya -',
1 : 'Hospitality Management',
10 : 'Kalikasan at Siyensiya',
11 : 'Pilosopya',
12 : 'Kasaysayan, Kultura at Relihiyon',
13 : 'Wika',
15 : 'Iba',
2 : 'Sining, Disenyo at Arkitektura',
3 : 'Negosyo',
4 : 'Inhenyerya at IT',
6 : 'Law',
7 : 'Edukasyon ',
8 : 'Araling Panlipunan',
9 : 'Medisina at Parmasya'},
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
in_days : function (n)
            {
                if (n > 0)
                {
                    return n == 1 ? n + " araw na nakalipas" : n + " mga araw na nakalipas";
                }
                else if (n < 0)
                {
                    n = -n;

                    return n == 1 ? n + " maghapon" : n + " araw";
                }
                else
                {
                    return "ngayon";
                }
            },
label_finish : 'Wakas',
label_vat_id : 'VAT ID',
n_days_late : function (n)
            {
                return n == 1 ? n + " araw huli" : n + " mga araw huli";
            },
site_name : 'Eliademy',
task_returned : function (n1, n2) {if(n1 == n2) {return "(Ibalik lahat)";} else {return "(" + n1 + "of" + n2 + "ibalik)";}},
title_grade : 'Grado'};
});