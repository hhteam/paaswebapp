define(["app/tools/utils"], function (Utils) { 
 return { 
about : 'हमारे बारे में',
add : 'अन्य',
add_another_file : 'अन्य फ़ाइल जोड़ें',
add_file : 'फ़ाइल जोड़ें',
add_grade_for_other_task : 'अन्य कार्य के लिए ग्रेड जोड़ें',
add_instructions : 'निर्देश जोड़ें',
assignment_uploaded : function (name, link, time)
            {
                return "असाइनमेंट <a href=\"" + link + "\">" + name + "</a> सफलतापूर्वक अपलोड किया गया "
                    + this.format_time(time);
            },
birthday : 'जन्मदिन',
blog : 'ब्लॉग',
button_back : 'वापस',
button_cancel : 'रद्द करें',
button_cancel_editing : 'परिवर्तन त्यागें',
button_close : 'बंद करें',
button_delete : 'हटाएँ',
button_done : 'समापन करें',
button_next : 'अगला',
button_no : 'नहीं',
button_prev : 'पिछला',
button_save : 'बचाऐं ',
button_submit : 'प्रस्तुत करना',
button_upload_file : 'फ़ाइल अपलोड करें ',
button_yes : 'हां',
calendar : 'कैलेंडर',
category : { 
0 : '- श्रेणी -',
1 : 'आतिथ्य प्रबंधन',
10 : 'प्रकृति एवं विज्ञान',
11 : 'दर्शन',
12 : 'इतिहास, संस्कृति और धर्म',
13 : 'भाषाएँ',
15 : 'अन्य',
2 : 'कला, डिजाइन और वास्तुकला',
3 : 'व्यापार',
4 : 'इंजीनियरिंग और आईटी',
6 : 'कानून',
7 : 'शिक्षा',
8 : 'सामाजिक विज्ञान',
9 : 'चिकित्सा और फार्मेसी'},
change_language : 'भाषा बदलें',
city : 'शहर',
completed_courses : 'पूर्ण पाठ्यक्रम',
completed_tasks : 'पूर्ण कार्य',
course : 'कोर्स',
course_fname_limit : 'कोर्स का नाम 120 वर्ण सीमा से अधिक',
course_sname_limit : 'छोटा कोर्स का नाम 40 वर्ण सीमा से अधिक',
courses : 'पाठ्यक्रम',
date_format : function ()
            {
               // Function so this doesn't end up in pootle, tweak carefully
               // datepicker needs to be tested at least and conversion from string -> timestamp
               return "dd.mm.yyyy";
            },
delete_task : 'कार्य हटाएं',
delete_task_message : 'इस कार्य से संबंधित सभी जानकारी हटा दिया जाएगा. कृपया यह सुनिश्चित करें कि आप इस कार्य को हटाना चाहते हैं?',
edit : 'संपादित करें',
edit_cancel2 : 'पाठ्यक्रम बनाने के बिना छोड़ दें? पहले से ही बनाई गई सामग्री खो जाएगी',
email : 'ईमेल',
event_delete : 'वृत्तांत को हटाना चाहते हैं?',
file_submissions : 'फ़ाइल प्रस्तुतियाँ',
fill_missing_fields : 'अनुपस्थित फ़ील्ड भरें.',
follow_us : 'हमारा अनुगमन करें',
format_time : function (time, notime)
            {
                var txt = this.date_format()
                    .replace('dd', Utils.padLeft(time.getDate(), "0", 2))
                    .replace('mm', Utils.padLeft(time.getMonth() + 1, "0", 2))
                    .replace('yyyy', time.getFullYear());

                if (!notime)
                {
                    txt += ", " + Utils.padLeft(time.getHours(), "0", 2) + ":" + Utils.padLeft(time.getMinutes(), "0", 2);
                }

                return txt;
            },
forum : 'चर्चाएँ',
forum_attachment : 'फ़ाइल जोड़ें',
grade_publish1 : 'ग्रेड अभी तक प्रकाशित नहीं',
grade_publish2 : 'ग्रेड प्रकाशित',
graded_tasks : 'श्रेणीबद्ध कार्यों',
grades : 'ग्रेड',
helpdesk : 'समर्थन',
in_days : function (n) {
                        if (n > 0) {
                           return n == n == 1 ? n + " दिन पहले" : n + " दिन पहले"; 
                        } else if (n < 0) {
                            n = -n;
                            return n == 1 ? n + " दिन" : n + " दिन"; 
                        } else {
                            return "आज";
                        }
                    },
instructions : 'निर्देश',
join_email : 'इन ईमेल पतों को निमंत्रण भेजें:',
join_email_sent : 'आमंत्रण भेजा गया. प्रतिभागियों के निमंत्रण स्वीकार करने पर इस पृष्ठ पर सूचीबद्ध किया जाएगा ',
join_participants : 'प्रतिभागियों',
label_attachment : 'अनुलग्नक शीर्षक',
label_description : 'विवरण',
label_duedate : 'नियत तारीख',
label_export : 'निर्यात',
label_finish : 'समाप्त करना ',
label_link : 'लिंक:',
label_picture : 'तस्वीर',
label_share : 'शेयर',
label_start : 'प्रारंभ ',
label_teacher : 'शिक्षक',
logo_default : 'डिफ़ॉल्ट का उपयोग करें',
logo_info : 'लोगो PNG, JPEG या GIF फ़ाइल हो सकता है. अधिकतम आकार 500KB <br> छवि 192x76 पिक्सल फिट करने के लिए बदल दिया जाएगा.',
logout : 'लॉगआउट',
n_days_late : function (n) {
                        return n = 1 ? n + " दिन देर से" : n + " दिनों कि देरी से";                 
                    },
new_content : 'सामग्री जोड़ें',
new_file : 'फ़ाइल',
new_task1 : 'निर्देश',
new_task2 : 'प्रस्तुतियाँ यहाँ दिखाई  जाएगी ',
no_due_date : 'कोई नियत दिनांक',
ongoing_courses : 'चल रहे पाठ्यक्रमों',
overview : 'सामग्री',
participants : 'प्रतिभागियों',
password : 'पासवर्ड',
privacy : 'गोपनीयता नीति',
quiz_answer : 'विकल्प के उत्तर ',
quiz_error_answer : 'उत्तर विकल्प खाली नहीं हो सकता है. ',
quiz_error_question : 'प्रश्न खाली नहीं हो सकता. ',
quiz_error_select : 'आप कम से कम एक सही उत्तर का चयन किया है. ',
quiz_finished : 'प्रश्नोत्तरी समाप्त ',
quiz_incorrect : 'गलत ',
quiz_instructions : 'आप  पास केवल एक प्रयास  है इस क्विज खत्म करने हेतु. ',
quiz_question : 'प्रश्न ',
quiz_results : 'सही है ',
quiz_select_tf : 'जवाब सही ',
returned_tasks : 'वापस किए गए कार्यों',
role_student : 'छात्र',
settings : 'सेटिंग्स',
share_facebook : 'Facebook पर शेयर',
share_join_text : 'Eliademy के कोर्स में शामिल हों:',
share_twitter : 'Twitter पर साझा करें',
sidebar_edit_profile_button : 'प्रोफ़ाइल संपादित करें',
site_name : 'Eliademy',
submitted : 'पेश',
tab_forum : 'चर्चाएँ',
task : 'कार्य',
task_instructions : 'यहाँ लिखें या फ़ाइलों को अपलोड करें ',
task_not_submitted : 'अभी तक कुछ भी नहीं पेश ',
task_quiz : 'प्रश्नोत्तरी ',
task_returned : function (n1, n2) {if(n1 == n2) {return "(सभी वापसी)";} else {return "(" + n2 + " में " + n1 +" की  वापसी)";} },
task_save_draft : 'ड्राफ्ट के रूप में सहेजें ',
task_status1 : 'वर्गीकृत अभी तक नहीं',
task_status2 : 'नहीं लौटा',
task_submissions : 'प्रस्तुतीकरण ',
tasks : 'कार्य',
tasks_to_be_done : 'करने वाले कार्य',
there_are_no_completed_tasks : 'यहॉ कोई पूण्य  कार्य नहि  हैं',
there_are_no_tasks : 'कोई  कार्य नहि  हैं',
tos : 'सेवा की शर्तें',
update_file : 'फ़ाइल अद्यतन',
use_facebook_picture : 'Facebook प्रोफ़ाइल तस्वीर का उपयोग करें'};
});