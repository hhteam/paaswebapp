<?php
/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

$config = new stdClass();

$config->RootDir            = "/app/a/";
$config->MoodleDir          = "/app/";

$config->EliademyUrl        = "https://mrdi.eliademy.com";
$config->LoginUrl           = "{$config->EliademyUrl}/login";
$config->SignupUrl          = "{$config->EliademyUrl}/signup";
$config->ExitUrl            = "{$config->EliademyUrl}/support";
$config->CatalogUrl         = "{$config->EliademyUrl}/catalog";

$config->FileUploadLimit    =   10 * 1024 * 1024;

// List of ids of users with "special" capabilities (temporary setting).
$config->SpecialUsers       = array();
//$config->SpecialUsers     = array(12, 16, 3469, 15943, 7631, 14836, 1020, 23534);
//$config->SpecialUsers     = array(12, 16, 3469, 3993, 5843, 7269, 14281, 15943, 7631, 14836, 21253, 21229, 21220);

// In production mode, we load "compiled" scripts/styles.
$config->Production         = true;

// Keys for Slideshare API.
$config->SlideshareKey      = "IgEi7xai";
$config->SlideshareSecret   = "xcNbQHFs";

$config->GApiKey            = "807835848233.apps.googleusercontent.com";
$config->FBAppID            = "109786629688133";
$config->LINAppID           = "81hb7gexofufnq";
$config->PayPalID           = "AR4oNhCCKZNnNQ3yHlVJ6BsPfwKBQ1I4hFR0oYTRgHxf5IL_ptvxVlDrQL3v";

// Relay server
$config->RelayHost          = "https://relay.cbtec.fi:38583";

$config->vidUrl             = "https://paasvideo.eliademy.com";
$config->vidPartnerId       = "107";
$config->vidPlayerId        = "23448171";

//If video upload needs to be enabled or disabled
$config->videoUploadEnabled = true;

// Moodle database
$config->db_host            = "localhost";
$config->db_name            = "moodle";
$config->db_user            = "moodle";
$config->db_password        = "S1B2@moodle";
$config->db_prefix          = "mdl_";

$config->cert_price         = 30;
$config->cert_teacher_cut   = 10;

/*---- plato currently also configures the following (seemingly unused) values:

$config->LibrejsFriendlyRoot    = "https://plato.cloudberrytec.com/app/a/";
$config->VideoFileUploadLimit   = 100 * 1024 * 1024;
$config->VKAppID                = "4428787";

------------------------------------------------------------------------------*/

?>
