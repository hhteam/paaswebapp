<?php
/**
 * Eliademy.com
 *
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

/* Fetch thumbnail image of a slideshow. */

require_once "config.php";

$ts = time();
$hash = sha1($config->SlideshareSecret . $ts);

if (isset($_GET["thumbnail"]))
{
    $curl = curl_init("https://www.slideshare.net/api/2/get_slideshow?api_key={$config->SlideshareKey}&ts=$ts&hash=$hash&slideshow_url=" . $_GET["thumbnail"]); 

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $data = curl_exec($curl);

    $xml = simplexml_load_string($data); 

    if ($xml->ThumbnailURL)
    {
        echo $xml->ThumbnailURL;
    }
    else
    {
        echo $data;
    }
}
