<?php
/**
 * Monorail Admin Tool settings
 *
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  Copyright 2012, CBTec Oy. All rights reserved.
 */


defined('MOODLE_INTERNAL') || die;

$ADMIN->add('root', new admin_category('monorail', 'Monorail Admin Tools'));
$ADMIN->add('monorail', new admin_externalpage('admin_e4b', 'Manage E4B', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_e4b.php"));
$ADMIN->add('monorail', new admin_externalpage('monorail_admin_email', 'Send Messages', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_email.php"));
$ADMIN->add('monorail', new admin_externalpage('monorail_admin_course_background', 'Course Background', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_course_background.php"));
$ADMIN->add('monorail', new admin_externalpage('magento_catalog', 'Magento Catalog', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/magento.php"));
$ADMIN->add('monorail', new admin_externalpage('monorail_admin_public_courses', 'Public Courses', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_public_courses.php"));
$ADMIN->add('monorail', new admin_externalpage('monorail_admin_analytics', 'Analytics', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_analytics.php"));
$ADMIN->add('monorail', new admin_externalpage('admin_extcourse', 'Ext Courses', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_extcourse.php"));
$ADMIN->add('monorail', new admin_externalpage('admin_delist', 'Delist public courses', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_delist.php"));
$ADMIN->add('monorail', new admin_externalpage('admin_cache', 'Cache control', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_cache.php"));
$ADMIN->add('monorail', new admin_externalpage('admin_coursepurchases', 'Course purchases', "$CFG->wwwroot/$CFG->admin/tool/monorail_admin_tool/admin_coursepurchases.php"));
