<?php
/**
 * @package    theme
* @subpackage monorail
* @copyright  2012 Cloudberry Tec
*/

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG, $PAGE;

$id = required_param('id', PARAM_INT);

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/filestorage/file_storage.php');

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$context = context_course::instance(SITEID);

$fs = get_file_storage();
$file = $fs->get_file($context->id, 'course', 'summary', 0, '/', $id);

$result = $file->delete();

$result = $result && $DB->delete_records('monorail_course_background', array('id' => $id));

echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);