<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2013 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

require_login();
admin_externalpage_setup('magento_catalog');


echo $OUTPUT->header();

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once $CFG->dirroot . "/local/monorailservices/mageapi.php";
    require_once $CFG->dirroot . '/theme/monorail/lib.php';

    $api = new MageApi($CFG->mage_api_url);

    echo "<pre>";

    if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
    {
        try
        {
            $api->preloadCatalogData();
        }
        catch (Exception $e)
        {
            echo "Error: " . $e->getMessage();
            goto import_end;
        }

        // Course products already registered in Magento.
        $oldCourses = array();

        foreach ($api->getProductList() as $prod)
        {
            if ($prod["set"] == $api->getAttributeSetId())
            {
                $oldCourses[$prod["sku"]] = $prod;
            }
        }

        $api->begin();

        foreach ($DB->get_recordset_sql("SELECT c.id AS id, c.category AS category, c.fullname AS fullname, c.shortname AS shortname," .
                    " d.code AS code, d.mainteacher AS mainteacher" .
                    " FROM {course} AS c INNER JOIN {monorail_course_data} AS d ON c.id=d.courseid") as $course)
        {
            $data = array();

            foreach ($DB->get_recordset_sql("SELECT datakey, value FROM {monorail_data} WHERE itemid=?" .
                " AND datakey IN ('public', 'coursebackground', 'course_logo')", array($course->id)) as $item)
            {
                $data[$item->datakey] = $item->value;
            }

            if (@$data["public"])
            {
                $prod = $api->prepareCourseData($data, $course, $course);

                if (array_key_exists($course->code, $oldCourses))
                {
                    echo "Updating public course: " . $course->fullname . "<br/>";

                    unset($prod["product"]["categories"]);
                    unset($prod["product"]["visibility"]);
                    unset($prod["product"]["websites"]);

                    // Update existing course product.
                    $api->updateProduct($course->code, $prod["product"]);

                    $oldMedia = $api->getProductMediaList($oldCourses[$course->code]["product_id"]);

                    if (!empty($oldMedia))
                    {
                        foreach ($oldMedia as $media)
                        {
                            $api->deleteProductMedia($oldCourses[$course->code]["product_id"], $media["file"]);
                        }
                    }

                    try
                    {
                        $api->createProductMedia($course->code, $prod["course_logo"], "logo", "course_logo", 100);
                        $api->createProductMedia($course->code, $prod["course_background"], "background", "course_background", 100);

                        if (@$prod["teacher_avatar"])
                        {
                            $api->createProductMedia($course->code, $prod["teacher_avatar"], "teacher", "teacher_avatar", 100);
                        }
                    }
                    catch (Exception $e)
                    {
                        echo "Unable to set image";
                    }

                    unset($oldCourses[$course->code]);
                }
                else
                {
                    echo "Adding public course: " . $course->fullname . "<br/>";

                    // Create new course product.
                    $api->createProduct("virtual", $api->getAttributeSetId(), $course->code, $prod["product"]);

                    // TODO: move outside of transaction - this call
                    // consumes a lot of memory (not good when committing
                    // many courses...)
                    try
                    {
                        $api->createProductMedia($course->code, $prod["course_logo"], "logo", "course_logo", 100);
                        $api->createProductMedia($course->code, $prod["course_background"], "background", "course_background", 100);

                        if (@$prod["teacher_avatar"])
                        {
                            $api->createProductMedia($course->code, $prod["teacher_avatar"], "teacher", "teacher_avatar", 100);
                        }
                    }
                    catch (Exception $e)
                    {
                        echo "Unable to set image";
                    }
                }
            }
        }

        // Delete course products that don't exist in Moodle
        foreach ($oldCourses as $course)
        {
            echo "Deleting course: " . $course["name"] . "<br/>";

            $api->deleteProduct($course["product_id"]);
        }

        $api->commit();

      import_end:
        $api->disconnect();
    }
    else
    {
        echo "Connection to Magento FAILED. Check configuration.";
    }

    echo "</pre>";

    echo "<input type=\"button\" value=\"OK\" onclick=\"window.history.back();\" />";
}
else
{
?>
<form method="post">
 <input type="submit" value="Sync courses to catalog." />
</form>
<?php
}

echo $OUTPUT->footer();
