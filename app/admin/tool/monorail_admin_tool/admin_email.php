<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2012 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->dirroot . '/theme/monorail/lib.php');
require_once($CFG->dirroot . '/local/monorailfeed/lib.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/message/lib.php');
require_once('lib.php');

require_login();
admin_externalpage_setup('monorail_admin_email');

ini_set('max_execution_time', 3000);

$admins = get_admins();
$isadmin = false;
foreach ($admins as $admin) {
 if ($USER->id == $admin->id) {
  $isadmin = true;
  break;
 }
}
if (!$isadmin) {
 print_error('invaliduserid');
}

$messagehtml   	= optional_param('messagehtml','',PARAM_CLEANHTML);
$subject	= optional_param('subject', '', PARAM_TEXT);
$tousers	= optional_param('tousers', '', PARAM_RAW);
$summary	= optional_param('summary', '', PARAM_TEXT);
$send 		= optional_param('send',false,PARAM_BOOL);
$preview 	= optional_param('preview',false,PARAM_BOOL);
$edit 		= optional_param('edit','',PARAM_BOOL);
$remail	        = optional_param('remail', '', PARAM_TEXT);
$ruser	        = optional_param('ruser', '', PARAM_TEXT);

echo $OUTPUT->header();

if (!empty($SESSION->adminemail) && empty($messagehtml)) {
 $messagehtml = $SESSION->adminemail['messagehtml'];
 $subject	 = $SESSION->adminemail['subject'];
 $summary	 = $SESSION->adminemail['summary'];
 $tousers	 = $SESSION->adminemail['tousers'];
 $remail	 = $SESSION->adminemail['remail'];
 $ruser	         = $SESSION->adminemail['ruser'];
}

if ($preview) {
 if ($subject == '') {
  print_error('Subject cannot be empty', null, $CFG->wwwroot.'/admin/tool/monorail_admin_tool/admin_email.php');
 }
 if ($summary == '') {
  print_error('Summary cannot be empty', null, $CFG->wwwroot.'/admin/tool/monorail_admin_tool/admin_email.php');
 }
 if ($messagehtml == '') {
  print_error('HTML Message cannot be empty', null, $CFG->wwwroot.'/admin/tool/monorail_admin_tool/admin_email.php');
 }
 if ($tousers == '') {
  print_error('Receivers list cannot be empty', null, $CFG->wwwroot.'/admin/tool/monorail_admin_tool/admin_email.php');
 }

 if (empty($SESSION->adminemail)) {
  $SESSION->adminemail = array();
 }
 $SESSION->adminemail['subject'] 	= $subject;
 $SESSION->adminemail['summary'] 	= $summary;
 $SESSION->adminemail['tousers'] 	= $tousers;
 $SESSION->adminemail['messagehtml'] = $messagehtml;
 $SESSION->adminemail['remail'] = $remail;
 $SESSION->adminemail['ruser'] = $ruser;

 $btConfirm = new single_button(new moodle_url($CFG->wwwroot.'/admin/tool/monorail_admin_tool/admin_email.php?send=true'), 'Confirm', 'POST');
 $btCancel  = new single_button(new moodle_url($CFG->wwwroot.'/admin/tool/monorail_admin_tool/admin_email.php'), 'Cancel', 'POST');

 echo $OUTPUT->confirm('Are you sure that you want to send this email to all users?', $btConfirm, $btCancel);


  $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
        'body' => $messagehtml,
        'title' => $subject,
        'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
        ), $lang),
        'block_header' => $summary 
    ), $lang);

 echo <<<HTML
<table border="0" cellpadding="5">
<tr valign="top">
	<td align="right"><b>Subject</b></td>
	<td align="left">$subject</td>
</tr>
<tr valign="top">
	<td align="right"><b>To</b></td>
	<td align="left">$tousers</td>
</tr>
<tr valign="top">
    <td align="right"><b>Message</b></td>
    <td align="left" rowspan="2">$template_content
    </td>
</tr>
</table>
HTML;
}
else if ($send) {


 $fail  = array();
 $syscontext = context_system::instance();

if(strcmp ($tousers , "allserviceusers") == 0) {
     $max_id = $DB->get_field_sql('SELECT MAX(id) FROM mdl_user');
     echo 'Getting all service users from DB '.$max_id.'</br>';
     $start_id = 3;
     $end_id = 300;  
     //process users in batch
     while($end_id <= $max_id) {
     echo 'Fetching users from start id: '.$start_id.' End id: '.$end_id.'</br>';
     $users = $DB->get_records_sql('SELECT * FROM mdl_user WHERE id BETWEEN '.$start_id.' AND '.$end_id.' AND suspended=0');
     foreach($users as $user) {
      $usersettings = monorailfeed_user_email_settings($user->id, null, true);
      if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
        $fail[] = $user->email;
        continue;
      }
      try {
       $result = false;
       if(filter_var($user->email, FILTER_VALIDATE_EMAIL)) { 
           $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
               'body' => monorail_template_compile($messagehtml,array('username' => $user->firstname), $lang),
               'title' => $subject,
               'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
                ), $lang),
               'block_header' => $summary
           ), $lang);
           $result = monorail_send_email('platform_updates', $subject, $template_content, null, null, $user->email);
       }
       if (!$result) {
        $fail[] = $user->email;
       }
      } catch (Exception $e) {
       $fail[] = $user->email;
      }
     }
     if($end_id >= $max_id) {
       //exit this loop
       break;
     }
     $start_id = $end_id + 1; 
     $end_id = $end_id + 300;
     if($end_id > $max_id) {
        $end_id = $max_id; //Max id
     } 
  }
 } else if(strcmp ($tousers , "allnoncohortteachers") == 0) {
     $max_id = $DB->get_field_sql('SELECT MAX(id) FROM mdl_user');
     echo 'Getting all service users from DB '.$max_id.'</br>';
     $start_id = 3;
     $end_id = 300;  
     //process users in batch
     $roleid = $DB->get_field('role', 'id', array('shortname'=>'teacher'), MUST_EXIST);
     $troleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
     while($end_id <= $max_id) {
     echo 'Fetching users from start id: '.$start_id.' End id: '.$end_id.'</br>';
     $users = $DB->get_records_sql('SELECT * FROM mdl_user WHERE id BETWEEN '.$start_id.' AND '.$end_id.' AND suspended=0');
     foreach($users as $user) {
      if(user_has_role_assignment($user->id,$roleid) || user_has_role_assignment($user->id,$troleid)) {
        //Check for non cohort;
        $cohortuser = $DB->count_records("cohort_members", array('userid'=>$user->id ));
        if($cohortuser > 0) {
          $fail[] = 'Teacher in cohort '.$user->email;
          continue;
        }
      } else {
        $fail[] = 'Not Teacher '.$user->email;
        continue;
       }

      $usersettings = monorailfeed_user_email_settings($user->id, null, true);

      if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
        $fail[] = $user->email;
        continue;
      }
      try {
       $result = false;
       if(filter_var($user->email, FILTER_VALIDATE_EMAIL)) { 
           $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
               'body' => monorail_template_compile($messagehtml,array('username' => $user->firstname), $lang),
               'title' => $subject,
               'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
                ), $lang),
               'block_header' => $summary
           ), $lang);
           $result = monorail_send_email('platform_updates', $subject, $template_content, null, null, $user->email);
       }
       if (!$result) {
        $fail[] = $user->email;
       }
      } catch (Exception $e) {
       $fail[] = $user->email;
      }
     }
     if($end_id >= $max_id) {
       //exit this loop
       break;
     }
     $start_id = $end_id + 1; 
     $end_id = $end_id + 300;
     if($end_id > $max_id) {
        $end_id = $max_id; //Max id
     } 
  }
 } else if(strpos($tousers, 'mailinglist') !== FALSE){
     $listname = explode("-", $tousers);
     //Check if such file exists
     if((sizeof($listname) == 2) && file_exists('mailinglist/'.$listname[1].'.txt')) {
     
     $defaulttemplate_content = monorail_template_compile(monorail_get_template('mail/base'), array(
       'body' => $messagehtml,
       'title' => $subject,
       'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
       'change_settings_url' => $CFG->magic_ui_root.'/settings'
       ), $lang),
       'block_header' => $summary
       ), $lang);

     //Read emails from file
     $filename = 'mailinglist/'.$listname[1].'.txt';
     $file = fopen($filename,"r");
     while(!feof($file)) {
      try {
       $result = false;
       $email = trim(fgets($file));
       if(filter_var($email, FILTER_VALIDATE_EMAIL)) { 
           if(!empty($ruser) && !empty($remail) && filter_var($remail, FILTER_VALIDATE_EMAIL)) {
             $result = monorail_send_system_email_adv($email, $subject, $defaulttemplate_content, null, 'platform_updates',
                                             true, null, $remail, $ruser);
           } else {
             $result = monorail_send_email('platform_updates', $subject, $defaulttemplate_content, null, null, $email);
           }
       }
       if (!$result) {
        $fail[] = $email;
       }
      } catch (Exception $e) {
       $fail[] = $email;
      }
     }
     fclose($file);
    } else { 
      echo 'Message failed to sent to the mailing list :'.$tousers;
    } 
 } else if(strpos($tousers, 'catalogteachers') !== FALSE){
    // FIXME: outdated public/private check!
    $publiccourses = $DB->get_records_sql("SELECT itemid FROM {monorail_data} WHERE datakey='public' AND value=1");
    $roleid = $DB->get_field('role', 'id', array('shortname'=>'teacher'), MUST_EXIST);
    $troleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
    $sentusers = array();
    foreach($publiccourses as $publiccourse) {
      if(!$DB->record_exists('course', array('id'=> $publiccourse->itemid))) {
        continue;
      }
      try {
        $coursecontext = context_course::instance($publiccourse->itemid);
      } catch (Exception $ex) {
        echo "No course context :".$ex->getMessage().'</br>';
        continue;
      }
      if($coursecontext) {
          $teachers = get_role_users(array($roleid,$troleid), $coursecontext);
          foreach($teachers as $teacher) {
            $result = false;
            $usersettings = monorailfeed_user_email_settings($teacher->id, null, true);
            if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
              $fail[] = $user->email;
              continue;
            }
            if(!in_array($teacher->id, $sentusers) && filter_var($teacher->email, FILTER_VALIDATE_EMAIL)) { 
              $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
               'body' => monorail_template_compile($messagehtml,array('username' => $teacher->firstname), $lang),
               'title' => $subject,
               'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
                ), $lang),
               'block_header' => $summary
             ), $lang);
             array_push($sentusers, $teacher->id);
             $result = monorail_send_email('platform_updates', $subject, $template_content, null, null, $teacher->email);
             echo "Mail Sent to :".$teacher->email.'</br>';
           }
           if (!$result) {
             $fail[] = $teacher->email;
           }
         }
       }}
 }
 else if ($tousers == "userswithdraftcourses")
 {
    // Users with draft courses only and no students:
    $users = $DB->get_records_sql("SELECT u.id AS id, u.email AS email, u.firstname AS firstname," .
        " COUNT(*) AS total, COUNT(DISTINCT cd.courseid) AS courses," .
        " COUNT(DISTINCT CASE WHEN cp.caccess=1 THEN cp.course END) AS draft" .
            " FROM {user} AS u" .
                " INNER JOIN {monorail_course_data} AS cd ON cd.mainteacher=u.id" .
                " INNER JOIN {monorail_course_perms} AS cp ON cp.course=cd.courseid" .
                " INNER JOIN {context} AS ctx ON ctx.instanceid=cd.courseid" .
                " INNER JOIN {role_assignments} AS ra ON ra.contextid=ctx.id" .
            " WHERE ctx.contextlevel=50" .
                " GROUP BY u.id HAVING total=courses AND total=draft");

    foreach ($users as $user)
    {
        $result = false;
        $usersettings = monorailfeed_user_email_settings($user->id, null, true);
        if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
          $fail[] = $user->email;
          continue;
        }

        if (filter_var($user->email, FILTER_VALIDATE_EMAIL))
        {
            $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
                'body' => monorail_template_compile($messagehtml, array('username' => $user->firstname), $lang),
                'title' => $subject,
                'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
                ), $lang),
                'block_header' => $summary
                ), $lang);

             $result = monorail_send_email('platform_updates', $subject, $template_content, null, null, $user->email);

             echo "Mail Sent to :" . $user->email.'</br>';
        }

        if (!$result)
        {
            $fail[] = $user->email;
        }
    }
 }
 else if ($tousers == "democohortadmins")
 {
    // Admins of demo cohorts.
    $users = $DB->get_records_sql("SELECT DISTINCT u.id AS id, u.email AS email, u.firstname AS firstname " .
            "FROM {user} AS u " .
                "INNER JOIN {monorail_cohort_admin_users} AS mca ON mca.userid=u.id " .
                "INNER JOIN {monorail_cohort_info} AS mci ON mci.cohortid=mca.cohortid " .
                    "WHERE mci.company_status=1");

    foreach ($users as $user)
    {
        $result = false;
        $usersettings = monorailfeed_user_email_settings($user->id, null, true);
        if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
          $fail[] = $user->email;
          continue;
        }

        if (filter_var($user->email, FILTER_VALIDATE_EMAIL))
        {
            $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
                'body' => monorail_template_compile($messagehtml, array('username' => $user->firstname), $lang),
                'title' => $subject,
                'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
                ), $lang),
                'block_header' => $summary
                ), $lang);

             $result = monorail_send_email('platform_updates', $subject, $template_content, null, null, $user->email);

             echo "Mail Sent to :" . $user->email.'</br>';
        }

        if (!$result)
        {
            $fail[] = $user->email;
        }
    }
 }
 else if ($tousers == "allteachers")
 {
    $teacher_roles = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN ('coursecreator', 'editingteacher', 'teacher')");

    if (empty($teacher_roles)) {
        // Disaster!
        $teacher_roles = array(0);
        error_log("No teacher roles found!");
    }

    // Users with teacher roles.
    $users = $DB->get_records_sql("SELECT DISTINCT u.id AS id, u.email AS email, u.firstname AS firstname " .
            "FROM {user} AS u " .
                "INNER JOIN {role_assignments} AS ra ON ra.userid=u.id " .
                    "WHERE ra.roleid IN (" . implode(", ", $teacher_roles) . ")");

    foreach ($users as $user)
    {
        $result = false;
        $usersettings = monorailfeed_user_email_settings($user->id, null, true);
        if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
          $fail[] = $user->email;
          continue;
        }

        if (filter_var($user->email, FILTER_VALIDATE_EMAIL))
        {
            $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
                'body' => monorail_template_compile($messagehtml, array('username' => $user->firstname), $lang),
                'title' => $subject,
                'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
                ), $lang),
                'block_header' => $summary
                ), $lang);

             $result = monorail_send_email('platform_updates', $subject, $template_content, null, null, $user->email);

             echo "Mail Sent to :" . $user->email.'</br>';
        }

        if (!$result)
        {
            $fail[] = $user->email;
        }
    }
 }
 else
 {
     $emails = explode(";", $tousers);
     $defaulttemplate_content = monorail_template_compile(monorail_get_template('mail/base'), array(
       'body' => $messagehtml,
       'title' => $subject,
       'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
       'change_settings_url' => $CFG->magic_ui_root.'/settings'
       ), $lang),
       'block_header' => $summary
       ), $lang);
     foreach ($emails as $email) {
     $user = $DB->get_record('user', array('email'=>strtolower($email)));
     if($user) {
         $usersettings = monorailfeed_user_email_settings($user->id, null, true);
          if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
          $fail[] = $user->email;
          continue;
         }
      }
      try {
       $result = false;
       if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
           if($user) {
             $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
               'body' => monorail_template_compile($messagehtml,array('username' => $user->firstname), $lang),
               'title' => $subject,
               'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
                ), $lang),
               'block_header' => $summary
             ), $lang);
           } else {
             $template_content = $defaulttemplate_content; 
           }
           $result = monorail_send_email('platform_updates', $subject, $template_content, null, null, $email);
       }
       if (!$result) {
        $fail[] = $email;
       }
      } catch (Exception $e) {
       $fail[] = $email;
      }
     }
 }

 if (empty($fail)) {
  unset($SESSION->adminemail);
  echo 'Message sent successfully.';
 }
 else {
  echo 'Message failed to sent to the following users:</br> '.count($fail);
  foreach($fail as $mail) {
   echo $mail.'</br>';
  }
 }

 echo $OUTPUT->continue_button(new moodle_url($CFG->wwwroot.'/admin/tool/monorail_admin_tool/admin_email.php'));
}
else {
 $templates = $DB->get_records('monorail_email_template');
 require('message_form.php');
 $PAGE->requires->js('/admin/tool/monorail_admin_tool/js/jquery.min.js');
 $PAGE->requires->js_init_call('M.tool_monorail_admin_tool.initiateAdminEmail', array(), true);
}
echo $OUTPUT->footer();
