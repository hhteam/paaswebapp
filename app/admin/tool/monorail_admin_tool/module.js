/**
 * @namespace
 */
M.tool_monorail_admin_tool = M.tool_monorail_admin_tool || {};

/**
 * YUI instance holder
 */
M.tool_monorail_admin_tool.Y = {};

M.tool_monorail_admin_tool.initiateAdminEmail = function(Y) {
	var popup = null;
	
	var uploader = new qq.FileUploader({
        element: $('div#templateUploader')[0],
        action: M.cfg.wwwroot+'/admin/tool/monorail_admin_tool/admin_email.php',
        multiple: false,
        allowedExtensions: ['html', 'htm'],
        debug: true,
        params: {},
        uploadButtonText: 'Upload Template',
        onComplete: function(id, fileName, responseJSON) {
        	if (responseJSON != false) {
        		window.location.reload();
        	}
        }
	});
	
	$('#inputFileUpload').on('change', function() {
		$('#theform').submit();
	})
	
	$('#btPreview').on('click', function () {
		var content = data.template;
		
		var editor = tinyMCE.get('idMessage').getContent();
		
		console.log(editor);
		content = content.replace('{{content}}', editor);
		
		popup = window.open('Preview', '_blank');
		
		var d = popup.document.open(); 
		d.write(content);
		d.close();
	});
}

M.tool_monorail_admin_tool.initiateCourseBackground = function (Y) {
	$('.carousel').carousel('pause');
	$.ajax({
		type: 'POST',
		url: M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_course_background_by_category.php',
		data: {categoryid: $('#selCategories').val()},
		dataType: 'json',
		success: function(data) {
			for (var i = 0; i < data.length; i++) {
				$('.carousel-inner > div.item.active').removeClass('active');
        		
        		$('.carousel-inner').append('<div class="item active" id="'+ data[i]['id'] +'"></div>');
        		
        		$('div.item.active').append('<img src="'+ data[i]['url'] +'"/>')
			}
        }
	});
	
	$('#btDelete').on('click', function () {
		$.ajax({
			type: 'POST',
			url: M.cfg.wwwroot+'/admin/tool/monorail_admin_tool/ext_delete_course_background.php',
			data: {id: $('.carousel-inner > div.item.active').attr('id')},
			dataType: 'json',
			success: function(data) {
				if (data === true) {
					$('.carousel-inner > div.item.active').remove();
					
					$('.carousel').carousel('prev');
				}
	        }
		});
	});
	
	var uploader = new qq.FileUploader({
        element: $('div#pictureuploadcontainer')[0],
        action: M.cfg.wwwroot+'/theme/monorail/ext/ajax_upload_course_background.php',
        multiple: false,
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        debug: true,
        params: {'attachfilehash': $('input#attachfilehash').val(), 'categoryid':$('#selCategories').val()},
        uploadButtonText: 'Upload',
        onComplete: function(id, fileName, responseJSON) {
        	console.log(responseJSON);
        	if (responseJSON != false) {
        		$('.carousel-inner > div.item.active').removeClass('active');
        		
        		$('.carousel-inner').append('<div class="item active"></div>');
        		
        		$('div.item.active').append('<img src="'+ responseJSON['url'] +'"/>')
        	}
        }
	});
	
	$('#selCategories').on('change', function() {
		uploader.setParams({'attachfilehash': $('input#attachfilehash').val(), 'categoryid':$('#selCategories').val()});
		
		$('.carousel-inner').html('');		
		$.ajax({
				type: 'POST',
				url: M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_course_background_by_category.php',
				data: {categoryid: $('#selCategories').val()},
				dataType: 'json',
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('.carousel-inner > div.item.active').removeClass('active');
		        		
		        		$('.carousel-inner').append('<div class="item active" id="'+ data[i]['id'] +'"></div>');
		        		
		        		$('div.item.active').append('<img src="'+ data[i]['url'] +'"/>')
					}
                }
			});
	});
};