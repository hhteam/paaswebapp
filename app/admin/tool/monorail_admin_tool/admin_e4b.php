<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2012 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

require_login();
admin_externalpage_setup('admin_e4b');

$admins = get_admins();
$isadmin = false;
foreach ($admins as $admin) {
 if ($USER->id == $admin->id) {
  $isadmin = true;
  break;
 }
}

if (!$isadmin) {
 print_error('invaliduserid');
}


if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if ($_POST["submit"] == "Save")
    {
        if (@$_POST["cid"])
        {
            $crec = $DB->get_record('cohort', array('id'=>$_POST["cid"]));

            if ($crec) {
                $cset = $DB->get_record('monorail_cohort_info', array('cohortid'=>$crec->id));

                if ($cset) {
                    $upMsg = "Cohort data updated.";

                    $cset->userscount = $_POST["users"];

                    if (@$_POST["valid_until"]) {
                        $cset->valid_until = strtotime($_POST["valid_until"]);
                    }

                    if (@$_POST["demo_period_end"]) {
                        $cset->demo_period_end = strtotime($_POST["demo_period_end"]);
                    }

                    if (@$_POST["next_billing"]) {
                        $cset->next_billing = strtotime($_POST["next_billing"]);
                    }

                    if (@$_POST["status"]) {
                        $cset->company_status = $_POST["status"];
                    }

                    if (@$_POST["notes"]) {
                        $cset->notes = $_POST["notes"];
                    }

                    if (@$_POST["public_page_path"] && $_POST["public_page_path"] != $cset->public_page_path) {
                        require_once __DIR__ . "/../../../theme/monorail/lib.php";

                        switch (monorail_check_org_public_page_path($_POST["public_page_path"])) {
                            case 0:
                                $cset->public_page_path = $_POST["public_page_path"];
                                break;

                            case 1: $upMsg .= " But public page path was too short (minimum 3 characters)."; break;
                            case 2: $upMsg .= " But public page path conained invalid characters (lowercase letters and numbers only)."; break;
                            case 3: $upMsg .= " But public page path was conflicting with other links."; break;
                            default: $upMsg .= " But public page path was already taken."; break;
                        }
                    }

                    if (@$_POST["toggle_public_page"] == "OK") {
                        $cset->public_page = ($cset->public_page ? 0 : 1);

                        if ($cset->public_page && !$cset->public_page_path) {
                            $path = strtolower(preg_replace('/[^a-zA-Z0-9]+/', "", $crec->name));
                            $orPath = $path;
                            $num = 0;
                            $ok = false;

                            require_once __DIR__ . "/../../../theme/monorail/lib.php";

                            while ($num < 2000 && !$ok) {
                                switch (monorail_check_org_public_page_path($path)) {
                                    case 0:
                                        $ok = true;
                                        break;

                                    case 1:
                                        $path .= "0";
                                        $orPath = $path;
                                        break;

                                    case 2:
                                        // No invalid charaters should be here...
                                        $num = 10000;
                                        break;

                                    default:
                                        $num++;
                                        $path = $orPath . $num;
                                }
                            }

                            if ($ok) {
                                $cset->public_page_path = $path;
                            } else {
                                $upMsg .= " But failed to generate a new path for public page (enter it manually).";
                            }
                        }
                    }

                    $DB->update_record('monorail_cohort_info', $cset);

                    setcookie("current_e4b_message", $upMsg);
                }
            }
        }

        header("Location: " . $_SERVER["REQUEST_URI"], 303);
    }
    else if ($_POST["submit"] == "Delete")
    {
        if ($_POST["confirm_delete"] == "yes" && @$_POST["cid"]) {
            $members = $DB->get_fieldset_sql("SELECT userid FROM {cohort_members} WHERE cohortid=?", array($_POST["cid"]));

            $DB->execute("DELETE FROM {cohort} WHERE id=?", array($_POST["cid"]));
            $DB->execute("DELETE FROM {cohort_members} WHERE cohortid=?", array($_POST["cid"]));
            $DB->execute("DELETE FROM {monorail_cohort_courseadmins} WHERE cohortid=?", array($_POST["cid"]));
            $DB->execute("DELETE FROM {monorail_cohort_info} WHERE cohortid=?", array($_POST["cid"]));
            $DB->execute("DELETE FROM {monorail_cohort_admin_users} WHERE cohortid=?", array($_POST["cid"]));

            foreach ($members as $mid) {
                $cids = $DB->get_fieldset_sql("SELECT id FROM {monorail_webservice_cache} WHERE userid=?", array($mid));

                if (!empty($cids)) {
                    $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $cids) . ")");
                    $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $cids) . ")");
                }
            }

            setcookie("current_e4b_message", "Cohort deleted.");
            header("Location: /app/admin/tool/monorail_admin_tool/admin_e4b.php", 303);
        } else {
            setcookie("current_e4b_message", "You don't seem to be sure about this :)");
            header("Location: " . $_SERVER["REQUEST_URI"], 303);
        }
    }

    exit(0);
}
else
{
    if (@$_GET["action"]) {
        switch ($_GET["action"]) {
            case "owner":
                $DB->execute("UPDATE {monorail_cohort_admin_users} SET usertype=2 WHERE cohortid=?", array($_GET["cid"]));
                $DB->execute("UPDATE {monorail_cohort_admin_users} SET usertype=1 WHERE cohortid=? AND userid=?", array($_GET["cid"], $_GET["uid"]));
                setcookie("current_e4b_message", "Account owner changed.");
                break;
        }

        header("Location: /app/admin/tool/monorail_admin_tool/admin_e4b.php?cid=" . $_GET["cid"], 303);
        exit(0);
    }

    if (isset($_COOKIE["current_e4b_message"]))
    {
        setcookie("current_e4b_message", "", time() - 1000);
    }

    echo $OUTPUT->header();

    if (isset($_COOKIE["current_e4b_message"]))
    {
        ?><div style="margin: 10px 0px; padding: 10px; border: 1px solid #000;"><?php echo $_COOKIE["current_e4b_message"] ?></div><?php
    }

    if (@$_GET["cid"])
    {
        $cohortInfo = $DB->get_record_sql("SELECT c.id AS id, c.name AS name, ci.userscount AS users," .
                    " ci.valid_until AS valid_until, ci.company_status AS status, ci.billing_details AS billing_details, ci.notes AS notes, ci.next_billing AS next_billing," .
                    " ci.demo_period_end AS demo_period_end, ci.public_page AS public_page, ci.public_page_path AS public_page_path " .
            "FROM {cohort} AS c " .
                "INNER JOIN {monorail_cohort_info} AS ci ON c.id=ci.cohortid " .
                "WHERE c.id=?", array($_GET["cid"]));
?>

<div>
    <a href="admin_e4b.php">&laquo; Back</a>
</div>

<form method="post">
<input type="hidden" name="cid" value="<?php echo $_GET["cid"]; ?>">
<table>
 <tr>
  <td>Cohort:</td>
  <td><strong><?php echo $cohortInfo->name; ?> (id: <?php echo $cohortInfo->id; ?>)</strong></td>
 </tr>
 <tr>
  <td>Admins:</td>
  <td><table><?php

    $cohortAdmins = $DB->get_recordset_sql("SELECT u.id AS id, u.email AS email, mca.usertype AS usertype " .
        "FROM {monorail_cohort_admin_users} AS mca INNER JOIN {user} AS u ON mca.userid=u.id WHERE cohortid=?",
            array($cohortInfo->id));

    foreach ($cohortAdmins as $adm) {
        if ($adm->usertype == 1) {
            echo "<tr><td><strong><a href=\"/app/user/profile.php?id=" . $adm->id . "\">" . $adm->email . "</a></strong></td><td>(owner)</td></tr>";
        } else {
            echo "<tr><td><a href=\"/app/user/profile.php?id=" . $adm->id . "\">" . $adm->email . "</a></td><td>[<a href=\"?action=owner&cid=" . $cohortInfo->id . "&uid=" . $adm->id . "\">make owner</a>]</td></tr>";
        }
    }

  ?></table></td>
 </tr>
 <tr>
  <td>Number of prepaid licences:</td>
  <td><input type="text" name="users" value="<?php echo $cohortInfo->users ?>"></td>
 </tr>
 <tr>
  <td>Prepaid licences valid until:</td>
  <td><input type="text" name="valid_until" value="<?php echo $cohortInfo->valid_until ? substr(date("c", $cohortInfo->valid_until), 0, 10) : ""; ?>"></td>
 </tr>
 <tr>
  <td>Demo period ends:</td>
  <td><input type="text" name="demo_period_end" value="<?php echo $cohortInfo->demo_period_end ? substr(date("c", $cohortInfo->demo_period_end), 0, 10) : ""; ?>"></td>
 </tr>
 <tr>
  <td>Next billing date (monthly billing):</td>
  <td><input type="text" name="next_billing" value="<?php echo $cohortInfo->next_billing ? substr(date("c", $cohortInfo->next_billing), 0, 10) : ""; ?>"></td>
 </tr>
 <tr>
  <td>Status:</td>
  <td><select name="status">
   <option value="">-</option>
   <option value="1"<?php if ($cohortInfo->status == 1) echo " selected=\"1\"" ?>>Demo</option>
   <option value="2"<?php if ($cohortInfo->status == 2) echo " selected=\"1\"" ?>>Regular paying user</option>
   <option value="3"<?php if ($cohortInfo->status == 3) echo " selected=\"1\"" ?>>NGO</option>
   <option value="4"<?php if ($cohortInfo->status == 4) echo " selected=\"1\"" ?>>Suspended (manually)</option>
   <option value="5"<?php if ($cohortInfo->status == 5) echo " selected=\"1\"" ?>>NGO review pending</option>
  </select>
<?php

    if ($cohortInfo->status == 1 && $cohortInfo->demo_period_end < time()) {
        echo "<strong style=\"color: olive;\">(Demo expired)</strong>";
    } else if ($cohortInfo->status == 2 && $cohortInfo->next_billing < time() &&
               $DB->record_exists("monorail_data", array("type" => "PAYNOTIF", "itemid" => $cohortInfo->id))) {
        echo "<strong style=\"color: red;\">(Payment failed)</strong>";
    }

?>
  </td>
 </tr>
 <tr>
  <td>Admin notes:</td>
  <td><textarea name="notes"><?php echo $cohortInfo->notes; ?></textarea></td>
 </tr>
 <tr>
  <td>Billing details:</td>
  <td><code><?php echo $cohortInfo->billing_details ?></code></td>
 </tr>
 <tr>
  <td>Has public page:</td>
  <td><?php echo $cohortInfo->public_page ? "YES" : "NO" ?>&nbsp;-&nbsp;<label><input type="checkbox" name="toggle_public_page" value="OK"><?php echo $cohortInfo->public_page ? "Disable" : "Enable" ?></label></td>
 </tr>
 <tr>
  <td>Public page link part:</td>
  <td><span>https://eliademy.com/</span><input type="text" name="public_page_path" value="<?php echo $cohortInfo->public_page_path ?>"></td>
 </tr>
 <tr>
  <td colspan="2"><input type="submit" name="submit" value="Save"></td>
 </tr>
 <tr>
  <td colspan="2"><input type="submit" name="submit" value="Delete">&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="checkbox" name="confirm_delete" value="yes" style="margin: 10px 0px;"> Yes, really.</label></td>
 </tr>
</table>
</form>

  <?php } else { ?>

    <form method="get" action="">
     <input type="text" name="search" value="<?php echo @$_GET["search"] ?>">
     <input type="submit" value="Search">
    </form>

    <?php

        $strdelete = get_string('delete');
        $returnurl = new moodle_url('/admin/tool/monorail_admin_tool/admin_e4b.php', array());
        $table = new html_table();
        $table->head = array ();
        $table->align = array();

        $table->head[] = 'Name';
        $table->align[] = 'left';
        $table->size[] = '350px';

        $table->head[] = 'Admin Id';
        $table->align[] = 'left';
        $table->size[] = '200px';

        $table->head[] = 'Licences';
        $table->align[] = 'left';
        $table->size[] = '70px';

        $table->head[] = 'Valid until';
        $table->align[] = 'left';
        $table->size[] = '250px';

        $table->head[] = 'Status';
        $table->align[] = 'left';
        $table->size[] = '70px';

        $sql = "SELECT c.id AS id, c.name AS name, GROUP_CONCAT(u.email SEPARATOR ', ') AS admins, ci.userscount AS users, ci.valid_until AS valid_until, " .
                "ci.company_status AS status, ci.demo_period_end AS demo_period_end, ci.next_billing AS next_billing, COUNT( IF( ca.usertype = 1, 1, NULL ) ) AS owners " .
            "FROM {cohort} AS c " .
                "INNER JOIN {monorail_cohort_info} AS ci ON c.id=ci.cohortid " .
                "LEFT JOIN {monorail_cohort_admin_users} AS ca ON c.id=ca.cohortid " .
                "LEFT JOIN {user} AS u ON u.id=ca.userid";

        if (@$_GET["search"])
        {
            $sql .= " WHERE name LIKE '%" . $_GET["search"] . "%' OR u.email LIKE '%" . $_GET["search"] . "%'";
        }

        $sql .= " GROUP BY c.id";

        $cohorts = $DB->get_records_sql($sql);

        foreach ($cohorts as $cinfo) {
         $row = array ();
         $row[] = "<a href=\"admin_e4b.php?cid=" . $cinfo->id . "\">" . $cinfo->name . " (id: " . $cinfo->id . ")</a>";

         if ($cinfo->owners == 1) {
            $row[] = $cinfo->admins;
         } else {
            $row[] = "<strong style=\"color: red;\">" . $cinfo->admins . " (problems with owners)</strong>";
         }

         $row[] = $cinfo->users;
         if ($cinfo->valid_until) {
            $row[] = date("r", $cinfo->valid_until);
         } else {
            $row[] = "-";
         }

         switch ($cinfo->status) {
            case 1:
                if ($cinfo->demo_period_end < time()) {
                    $row[] = "<strong style=\"color: olive;\">Demo expired</strong>";
                } else {
                    $row[] = "Demo";
                }
                break;

            case 2:
                if ($cinfo->next_billing < time() && $DB->record_exists("monorail_data", array("type" => "PAYNOTIF", "itemid" => $cinfo->id))) {
                    $row[] = "<strong style=\"color: red;\">Payment failed</strong>";
                } else {
                    $row[] = "Regular";
                }
                break;

            case 3: $row[] = "NGO"; break;
            case 4: $row[] = "Suspended"; break;
            case 5: $row[] = "Pending review"; break;
            default: $row[] = "Not set";
         }

         $table->data[] = $row;
        }

        if (!empty($table)) {
         echo html_writer::table($table);
        }
    }

    echo $OUTPUT->footer();
}
