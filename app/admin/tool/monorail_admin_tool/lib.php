<?php
function monorail_message_post_message($userfrom, $userto, $messagehtml, $messagetxt, $subject, $format) {
    global $SITE, $CFG, $USER;

    $eventdata = new stdClass();
    $eventdata->component        = 'moodle';
    $eventdata->name             = 'instantmessage';
    $eventdata->userfrom         = $userfrom;
    $eventdata->userto           = $userto;
    $eventdata->subject          = $subject;
    $eventdata->fullmessagehtml  = $messagehtml;
    $eventdata->fullmessage 	 = $messagetxt;

    $eventdata->fullmessageformat 	= $format;
    $eventdata->smallmessage     	= $messagetxt;//store the message unfiltered. Clean up on output.
    $eventdata->timecreated     	= time();

    return message_send($eventdata);
}