<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2012 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

require_login();
admin_externalpage_setup('monorail_admin_public_courses');

$admins = get_admins();
$isadmin = false;
foreach ($admins as $admin) {
 if ($USER->id == $admin->id) {
  $isadmin = true;
  break;
 }
}
if (!$isadmin) {
 print_error('invaliduserid');
}


$from = "FROM {course} AS c, {monorail_data} AS md "
                . "WHERE c.visible AND md.itemid=c.id AND md.datakey='public' AND md.value=1 ORDER BY c.fullname";
$courses = $DB->get_records_sql("SELECT c.id, c.fullname, c.shortname " . $from);


echo $OUTPUT->header();

$table = new html_table();
$table->head = array ();
$table->align = array();
$table->head[] = 'Course';
$table->align[] = 'left';
$table->size[] = '100px';
$table->head[] = 'Teacher Name';
$table->align[] = 'left';
$table->size[] = '100px';
$table->head[] = 'Email Id';
$table->align[] = 'left';
$table->size[] = '70px';
$table->width = "70%";

foreach ($courses as $course) {
 $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$course->id));
 $tname = ''; 
 $tmail = '';
 if($coursedata && $coursedata->mainteacher != 0) {
     $teacher = $DB->get_record('user', array('id'=>$coursedata->mainteacher));
     if($teacher) {
         $tname = fullname($teacher); 
         $tmail = $teacher->email;
     }
 }else {
     //Get teachers for this course if no main teacher
 }
 $row = array ();
 $row[] = $course->fullname;
 $row[] = $tname;
 $row[] = $tmail;
 $table->data[] = $row;
}


if (!empty($table)) {
 echo html_writer::table($table);
}

echo $OUTPUT->footer();
