<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once $CFG->libdir.'/formslib.php';


class admin_analytics_form extends moodleform {
    function definition () {
        $mform = $this->_form;

        $mform->addElement('header', 'analyticsheader', 'Analytics');

        $mform->addElement('date_selector', 'assesstimestart', 'Start Date : ', array('startyear' => 1970,'stopyear'  => 2020,'timezone'  => 99,'optional'  => false));
        $mform->addElement('date_selector', 'assesstimefinish', 'End Date : ', array('startyear' => 1970,'stopyear'  => 2020,'timezone'  => 99,'optional'  => false));

        $mform->addElement('html', '<label><br></label>');

        $mform->addElement('html', '<label><b>Analytics data - Cumulative data till given End Date</b></label>');
        $mform->addElement('html', '<label><br></label>');

        $mform->addElement('static', 'courses', "Courses: Total", '');
        $mform->addElement('static', 'participants', "Users: Total", '');
        $mform->addElement('static', 'signupse4b', "E4B: cohorts Total", '');
        $mform->addElement('static', 'totale4blicences', "E4B: Cohorts Licences Total", '');


        $mform->addElement('html', '<label><br></label>');
        $mform->addElement('html', "<label><b>Analytics data - New users/courses created in between (Start Date - End Date)</b></label>");
        $mform->addElement('html', '<label><br></label>');

        $mform->addElement('static', 'signups', "Signups: Free", '');
        $mform->addElement('static', 'signupse4busers', "Signups: E4B users", '');
        $mform->addElement('static', 'signupse4b', "Signups: E4B cohorts", '');

        $mform->addElement('html', '<label><br></label>');
        $mform->addElement('static', 'activeusers', "Users: Active", '');
        $mform->addElement('static', 'activeuserse4b', "Users: Active E4B", '');


        $mform->addElement('html', '<label><br></label>');
        $mform->addElement('static', 'tcourses', "Courses: Total", '');
        $mform->addElement('static', 'tparticipants', "Users: Total", '');
        $mform->addElement('static', 'tsignupse4b', "E4B: cohorts Total", '');


        $mform->addElement('html', '<label><br></label>');
        $mform->addElement('html', "<label><b>Non E4B Courses</b></label>");
        $mform->addElement('static', 'xcourses', "Courses: Total", '');
        $mform->addElement('static', 'ccourse', "Courses: Closed", '');
        $mform->addElement('static', 'pccourse', "Courses: Public", '');
        $mform->addElement('static', 'pccoursec', "Courses: Public in Catalog", '');
        $mform->addElement('static', 'pccoursecp', "Courses: Public in Catalog(Paid)", '');
        $mform->addElement('html', '<label><br></label>');

        $mform->addElement('html', "<label><b>Participants</b></label>");
        $mform->addElement('static', 'cstudents', "Enrollments: Total", '');
        $mform->addElement('static', 'ccstudents', "Enrollments: Closed courses", '');
        $mform->addElement('static', 'pcstudents', "Enrollments: Public courses", '');
        $mform->addElement('static', 'pcstudentsc', "Enrollments: Public in Catalog", '');
        $mform->addElement('static', 'pcstudentscp', "Enrollments: Public in Catalog(Paid)", '');
        $mform->addElement('html', '<label><br></label>');


        $mform->addElement('html', "<label><b>E4B Courses</b></label>");
        $mform->addElement('static', 'bcourses', "E4B Courses: Total", '');
        $mform->addElement('static', 'bccourse', "E4B Courses: Closed", '');
        $mform->addElement('static', 'bpccourse', "E4B Courses: Public", '');
        $mform->addElement('static', 'bpccoursec', "E4B Courses: Public in Catalog", '');
        $mform->addElement('static', 'bpccoursecp', "E4B Courses: Public in Catalog(Paid)", '');
        $mform->addElement('html', '<label><br></label>');


        $mform->addElement('html', "<label><b>E4B Participants</b></label>");
        $mform->addElement('static', 'bcstudents', "E4B Enrollments: Total", '');
        $mform->addElement('static', 'bccstudents', "E4B Enrollments: Closed courses", '');
        $mform->addElement('static', 'bpcstudents', "E4B Enrollments: Public courses", '');
        $mform->addElement('static', 'bpcstudentsc', "E4B Enrollments: Public in Catalog", '');
        $mform->addElement('static', 'bpcstudentscp', "E4B Enrollments: Public in Catalog(Paid)", '');
        $mform->addElement('html', '<label><br></label>');

        $mform->addElement('html', "<label><b>Social Analytics</b></label>");
        $mform->addElement('static', 'fbshares', "Facebook shares Total", '');
        $mform->addElement('static', 'fbcshares', "Course Total", '');
        $mform->addElement('static', 'fbccshares', "Certificate Total", '');
        $mform->addElement('static', 'fbcsshares', "Skills Total", '');
        $mform->addElement('static', 'fbcrshares', "Review Total", '');
        $mform->addElement('static', 'fbcishares', "Invites Total", '');
        $mform->addElement('static', 'fbcdshares', "Completed Total", '');
        $mform->addElement('static', 'fbceshares', "Enroll Total", '');
        $mform->addElement('html', '<label><br></label>');

        $mform->addElement('static', 'lnshares', "Linkedin shares Total", '');
        $mform->addElement('static', 'lncshares', "Course Total", '');
        $mform->addElement('static', 'lnccshares', "Certificate Total", '');
        $mform->addElement('static', 'lncsshares', "Skills Total", '');
        $mform->addElement('static', 'lncrshares', "Review Total", '');
        $mform->addElement('static', 'lncdshares', "Completed Total", '');
        $mform->addElement('static', 'lnceshares', "Enroll Total", '');
        $mform->addElement('html', '<label><br></label>');

        $mform->addElement('static', 'twshares', "Twitter shares Total", '');
        $mform->addElement('static', 'twcshares', "Course Total", '');
        $mform->addElement('static', 'twccshares', "Certificate Total", '');
        $mform->addElement('static', 'twcsshares', "Skills Total", '');
        $mform->addElement('static', 'twcrshares', "Review Total", '');
        $mform->addElement('static', 'twcishares', "Invites Total", '');
        $mform->addElement('static', 'twcdshares', "Completed Total", '');
        $mform->addElement('static', 'twceshares', "Enroll Total", '');
        $mform->addElement('html', '<label><br></label>');

        $this->add_action_buttons(false, 'Fetch Data!');
    }

    function get_data() {
        $data = parent::get_data();
        return $data;
    }
}
