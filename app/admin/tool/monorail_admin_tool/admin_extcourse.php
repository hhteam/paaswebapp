<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2012 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

require_login();
admin_externalpage_setup('admin_extcourse');

$admins = get_admins();
$isadmin = false;
foreach ($admins as $admin) {
 if ($USER->id == $admin->id) {
  $isadmin = true;
  break;
 }
}
if (!$isadmin) {
 print_error('invaliduserid');
}


$coursesinfo = array();
$extcourses = $DB->get_records_sql('SELECT * FROM {monorail_external_courses}');
foreach ($extcourses as $ecourse) {
  $cinfo = array();
try {
  $courseid = $DB->get_field('monorail_course_data', 'courseid', array('code'=>$ecourse->intcode),MUST_EXIST);
  $coursename = $DB->get_field('course', 'fullname', array('id'=>$courseid),MUST_EXIST);
  $cinfo['name'] = $coursename;
  $cinfo['extcode'] = $ecourse->extcode;
  $cinfo['productid'] = $ecourse->productid;
  $cinfo['code'] = $ecourse->intcode;
  $cinfo['provider'] = $ecourse->provider;
  $coursesinfo[] = $cinfo;
}catch (Exception $ex) {
 //Continue;
}
}
echo $OUTPUT->header();
$returnurl = new moodle_url('/admin/tool/monorail_admin_tool/admin_extcourse.php', array());
$table = new html_table();
$table->head = array ();
$table->align = array();
$table->head[] = 'Course Name';
$table->align[] = 'left';
$table->size[] = '100px';
$table->head[] = 'External Code';
$table->align[] = 'left';
$table->size[] = '100px';
$table->head[] = 'Product Id';
$table->align[] = 'left';
$table->size[] = '100px';
$table->head[] = 'Course Code';
$table->align[] = 'left';
$table->size[] = '100px';
$table->head[] = 'Provider';
$table->align[] = 'left';
$table->size[] = '100px';
foreach ($coursesinfo as $cinfo) {
 $row = array ();
 $row[] = $cinfo['name'];
 $row[] = $cinfo['extcode'];
 $row[] = $cinfo['productid'];
 $row[] = $cinfo['code'];
 $row[] = $cinfo['provider'];
 $table->data[] = $row;
}
if (!empty($table)) {
 echo html_writer::table($table);
}

echo $OUTPUT->footer();
