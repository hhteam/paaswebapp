<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2012 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

require_login();
admin_externalpage_setup('admin_cache');

$admins = get_admins();
$isadmin = false;
foreach ($admins as $admin) {
 if ($USER->id == $admin->id) {
  $isadmin = true;
  break;
 }
}

if (!$isadmin) {
 print_error('invaliduserid');
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    switch ($_POST["cmd"]) {
        case "clear_user":
            $uid = $DB->get_field_sql("SELECT id FROM {user} WHERE email=?", array($_POST["email"]));

            if ($uid) {
                $cids = $DB->get_fieldset_sql("SELECT id FROM {monorail_webservice_cache} WHERE userid=?", array($uid));

                if (!empty($cids)) {
                    $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $cids) . ")");
                    $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $cids) . ")");

                    setcookie("current_cache_message", "Cache cleared for user " . $_POST["email"]);
                } else {
                    setcookie("current_cache_message", "User " . $_POST["email"] . " did not have any cache.");
                }
            } else {
                setcookie("current_cache_message", "User " . $_POST["email"] . " was not found.");
            }
            break;

        case "clear_service":
            $cids = $DB->get_fieldset_sql("SELECT id FROM {monorail_webservice_cache} WHERE servicename=?", array($_POST["service"]));

            if (!empty($cids)) {
                $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $cids) . ")");
                $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $cids) . ")");

                setcookie("current_cache_message", "Cache cleared for service " . $_POST["service"]);
            } else {
                setcookie("current_cache_message", "Nothing found for service " . $_POST["service"]);
            }
            break;
    }

    header("Location: " . $_SERVER["REQUEST_URI"], 303);
    exit(0);
}
else
{
    if (isset($_COOKIE["current_cache_message"]))
    {
        setcookie("current_cache_message", "", time() - 1000);
    }

    echo $OUTPUT->header();

    if (isset($_COOKIE["current_cache_message"]))
    {
        ?><div style="margin: 10px 0px; padding: 10px; border: 1px solid #000;"><?php echo $_COOKIE["current_cache_message"] ?></div><?php
    }
?>

<form method="post">
 <p>Clear cache for user (enter email): <input type="email" name="email" required></p>
 <input type="submit" value="Clear">
 <input type="hidden" name="cmd" value="clear_user">
</form>

<br>
<br>
<br>

<form method="post">
 <p>Clear cache by service name: <input type="text" name="service" required></p>
 <input type="submit" value="Clear">
 <input type="hidden" name="cmd" value="clear_service">
</form>

<?php
    echo $OUTPUT->footer();
}
