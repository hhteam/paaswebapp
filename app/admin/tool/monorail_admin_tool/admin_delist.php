<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2014 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

require_once $CFG->libdir . "/../theme/monorail/lib.php";
require_once $CFG->libdir . "/../local/monorailservices/mageapi.php";

require_login();
admin_externalpage_setup('admin_delist');

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    switch (@$_POST["action"])
    {
        case "select_course":
            $cid = $DB->get_field_sql("SELECT courseid FROM {monorail_course_data} WHERE code=?", array(@$_POST["course_code"]));

            if ($cid)
            {
                setcookie("current_delist_course", $cid);
            }
            else
            {
                setcookie("current_delist_course", "", time() - 1000);
                setcookie("current_delist_message", "Course not found.");
            }
            break;

        case "delist_course":
            setcookie("current_delist_course", "", time() - 1000);
            $cid = $_COOKIE["current_delist_course"];

            $courseData = $DB->get_record('monorail_course_data', array('courseid' => $cid), '*', MUST_EXIST);

            $email_message = "";
            $email_subject = "";

            if ($_POST["submit"] == "DELIST THIS COURSE")
            {
                $perms = monorail_get_course_perms($cid);
                monorail_update_course_perms($cid, $perms["course_access"], 5);

                $api = new MageApi($CFG->mage_api_url);

                if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
                {
                    $catalogItem = $api->getProductBySku($courseData->code);

                    if (!empty($catalogItem))
                    {
                        $api->deleteProduct($catalogItem["product_id"]);
                    }

                    $api->disconnect();

                    setcookie("current_delist_message", "Course delisted!");

                    if ($_POST["notify_user"] == "yes")
                    {
                        $email_message = str_replace("{%coursename%}",
                            "<a href=\"https://eliademy.com/app/a/courses/" . $courseData->code . "\">" .
                                    $DB->get_field_sql("SELECT fullname FROM {course} WHERE id=?", array($cid)) . "</a>",
                                get_string("email_course_delisted_msg", "theme_monorail")) .
                                    "<br><br><center><a href=\"https://eliademy.com/app/a/courses/" . $courseData->code . "\">" .
                                    get_string("email_course_delisted_go_to_course", "theme_monorail") . "</a></center>";

                        $email_subject = get_string("email_course_delisted_subject", "theme_monorail");
                    }
                    else if ($_POST["notify_user"] == "custom")
                    {
                        $email_message = $_POST["notify_user_message"] .
                            "<br><br><center><a href=\"https://eliademy.com/app/a/courses/" . $courseData->code . "\">" .
                            get_string("email_course_delisted_go_to_course", "theme_monorail") . "</a></center>";
                        
                        $email_subject = get_string("email_course_delisted_subject", "theme_monorail");
                    }
                }
                else
                {
                    setcookie("current_delist_message", "Failed to connect to magento! Course not delisted! Panic!");
                }
            }
            else if ($_POST["submit"] == "UNDELIST THIS COURSE")
            {
                $perms = monorail_get_course_perms($cid);
                monorail_update_course_perms($cid, $perms["course_access"], 1);
                $perms = monorail_get_course_perms($cid);

                $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($cid));
                $monorailData = array();

                foreach ($moreData as $md)
                {
                    $monorailData[$md->datakey] = $md->value;
                }

                $monorailData['course_access'] = $perms['course_access'];
                $monorailData['course_privacy'] = $perms['course_privacy'];

                $courseinfo = $DB->get_record('course', array('id' => $cid), "*", MUST_EXIST);

                magento_update_course($courseData, $monorailData, $courseinfo);

                setcookie("current_delist_message", "Course undelisted!");

                if ($_POST["notify_user"] == "yes")
                {
                    $email_message = str_replace("{%coursename%}", "<a href=\"https://eliademy.com/app/a/courses/" . $courseData->code . "\">" . $courseinfo->fullname . "</a>",
                        get_string("email_course_listed_msg", "theme_monorail"));

                    $email_subject = get_string("email_course_listed_subject", "theme_monorail");
                }
                else if ($_POST["notify_user"] == "custom")
                {
                    $email_message = $_POST["notify_user_message"];
                    $email_subject = get_string("email_course_listed_subject", "theme_monorail");
                }
            }
            else if ($_POST["submit"] == "MOVE COURSE TO DRAFT")
            {
                $perms = monorail_get_course_perms($cid);
                monorail_update_course_perms($cid, 1, $perms["course_privacy"]);

                $api = new MageApi($CFG->mage_api_url);

                if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
                {
                    $catalogItem = $api->getProductBySku($courseData->code);

                    if (!empty($catalogItem))
                    {
                        $api->deleteProduct($catalogItem["product_id"]);
                    }

                    $api->disconnect();

                    setcookie("current_delist_message", "Course moved to draft mode!");

                    if ($_POST["notify_user"] == "yes")
                    {
                        $email_message = str_replace("{%coursename%}", "<a href=\"https://eliademy.com/app/a/courses/" . $courseData->code . "\">"
                                    . $DB->get_field_sql("SELECT fullname FROM {course} WHERE id=?", array($cid)) . "</a>",
                                get_string("email_course_delisted_msg", "theme_monorail")) .

                            "<br><br><center><a href=\"https://eliademy.com/app/a/courses/" . $courseData->code . "\">" .
                            get_string("email_course_delisted_go_to_course", "theme_monorail") . "</a></center>";

                        $email_subject = get_string("email_course_delisted_subject", "theme_monorail");
                    }
                    else if ($_POST["notify_user"] == "custom")
                    {
                        $email_message = $_POST["notify_user_message"].
                            "<br><br><center><a href=\"https://eliademy.com/app/a/courses/" . $courseData->code . "\">" .
                            get_string("email_course_delisted_go_to_course", "theme_monorail") . "</a></center>";

                        $email_subject = get_string("email_course_delisted_subject", "theme_monorail");
                    }
                }
                else
                {
                    setcookie("current_delist_message", "Failed to connect to magento! Course not removed! Panic!");
                }
            }

            if (!empty($email_subject) && !empty($email_message))
            {
                $body = monorail_template_compile(monorail_get_template('mail/base'), array(
                   'title' => $email_subject,
                   'body' => $email_message,
                   'footer_additional_link' => '',
                   'block_header' => ''));

                monorail_send_email('', $email_subject, $body, $courseData->mainteacher);
            }
            break;

        case "search_courses":
            setcookie("course_search_name", $_POST["course_search_name"]);
            break;
    }

    header("Location: " . $_SERVER["REQUEST_URI"], 303);
    exit(0);
}

if (isset($_COOKIE["current_delist_message"]))
{
    setcookie("current_delist_message", "", time() - 1000);
}

echo $OUTPUT->header();
?>

<table width="100%">
<tr>
<td style="vertical-align: top;">
<h1>Currently delisted courses</h1>
<?php
    $courses = $DB->get_records_sql("SELECT c.id AS id, c.fullname AS fullname FROM {course} AS c INNER JOIN {monorail_course_perms} AS cp ON c.id=cp.course WHERE cp.privacy=5");
?>
<script>
function selectCourse(id)
{
    document.cookie="current_delist_course=" + id;
    location.reload();
    return false;
}
</script>
<table>
<?php foreach ($courses as $course) { ?>
 <tr>
  <td><a href="/app/course/view.php?id=<?php echo $course->id; ?>"><?php echo $course->fullname ?></a></td>
  <td>[<a href="#" onclick="selectCourse(<?php echo $course->id ?>);">Select</a>]</td>
 </tr>
<?php } ?>
</table>

</td>

<td style="vertical-align: top;">
<h1>Select course</h1>

<?php if (isset($_COOKIE["current_delist_message"])) { ?>
<div style="margin: 10px 0px; padding: 10px; border: 1px solid #000;"><?php echo $_COOKIE["current_delist_message"] ?></div>
<?php } ?>

<?php if (isset($_COOKIE["current_delist_course"])) {

    $currentCourse = $DB->get_record_sql("SELECT fullname FROM {course} WHERE id=?", array($_COOKIE["current_delist_course"]));

?>

<form method="post" style="padding: 10px; border: 1px solid #000;">
 <div style="margin: 10px 0px;">Course seleced: <strong><?php echo $currentCourse->fullname ?></strong></div>

<?php
    $perms = monorail_get_course_perms($_COOKIE["current_delist_course"]);

    if ($perms["course_privacy"] == 5) {
?>
<div style="margin-bottom: 10px;">Course is currently delisted. Undelist?</div>

 - <input type="submit" name="submit" value="UNDELIST THIS COURSE"> -
 <input type="submit" name="submit" value="Cancel"> -
 <input type="hidden" name="action" value="delist_course">

<?php } else { ?>

 - <input type="submit" name="submit" value="DELIST THIS COURSE"> -
 <input type="submit" name="submit" value="MOVE COURSE TO DRAFT"> -
 <input type="submit" name="submit" value="Cancel"> -
 <input type="hidden" name="action" value="delist_course">

<?php } ?>

<div style="margin-top: 10px;">
 <div>
  <label><input type="radio" name="notify_user" value="yes" checked> <span>Send user the default notification email.</span>
  <em style="padding: 10px; display: block;">
  <?php echo str_replace("{%coursename%}", $currentCourse->fullname, get_string(($perms["course_privacy"] == 5) ? "email_course_listed_msg" : "email_course_delisted_msg", "theme_monorail")); ?>
  </em></label>
 </div>

 <div>
  <label><input type="radio" name="notify_user" value="no"> <span>Don't send any message.</span></label>
 </div>

 <div style="margin: 10px 0px;">
  <label><input type="radio" name="notify_user" value="custom"> <span>Send user a custom message:</span><br>
  <textarea name="notify_user_message" cols="60" rows="8"></textarea></label>
 </div>
</div>
</form>

<?php } else { ?>

<form method="post">
 <label>Enter course code/sku: <input type="text" name="course_code"></label>
 <input type="submit" value="Select">
 <input type="hidden" name="action" value="select_course">
</form>

<?php } ?>

<h1>Search courses</h1>

<form method="post">
 <label>Enter course name: <input type="text" name="course_search_name" value="<?php echo $_COOKIE["course_search_name"] ?>"></label>
 <input type="submit" value="Search">
 <input type="hidden" name="action" value="search_courses">
</form>

<?php
if (isset($_COOKIE["course_search_name"])) {

$matches = $DB->get_records_sql("SELECT c.id AS id, c.fullname AS fullname, cd.code AS code, u.username AS username, u.id AS uid FROM {course} AS c " .
    "INNER JOIN {monorail_course_data} AS cd ON c.id=cd.courseid " .
    "LEFT JOIN {user} AS u ON u.id=cd.mainteacher " .
        "WHERE c.fullname LIKE '%" . $_COOKIE["course_search_name"] . "%' LIMIT 10");

if (empty($matches)) { ?>
<div>No courses found.</div>
<?php } else { ?>
<table>
<?php foreach ($matches as $m) { ?>
<tr>
 <td><a href="/app/course/view.php?id=<?php echo $m->id ?>"><?php echo $m->fullname ?></a></td>
 <td><a href="/app/user/profile.php?id=<?php echo $m->uid ?>"><?php echo $m->username ?></a></td>
 <td>[<a href="#" onclick="selectCourse(<?php echo $m->id ?>);">Select</a>]</td>
</tr>
<?php } ?>
</table>
<?php } } ?>
</td>

</tr>
</table>
<?php
echo $OUTPUT->footer();
