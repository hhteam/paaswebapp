<?php

/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2012 Cloudberry Tec
 */

function xmldb_tool_monorail_admin_tool_upgrade($oldversion = 0) {

    global $DB;
    $dbman = $DB->get_manager();

    $result = true;

    if ($oldversion < 2012112204) {

    	// Define table monorail_email_template to be created
    	$table = new xmldb_table('monorail_email_template');

    	// Adding fields to table monorail_email_template
    	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    	$table->add_field('name', XMLDB_TYPE_TEXT, null, null, null, null, null);
    	$table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
    	$table->add_field('content', XMLDB_TYPE_TEXT, null, null, null, null, null);

    	// Adding keys to table monorail_email_template
    	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

    	// Conditionally launch create table for monorail_email_template
    	if (!$dbman->table_exists($table)) {
    		$dbman->create_table($table);
    	}

    	// monorail_admin_tool savepoint reached
    	upgrade_plugin_savepoint(true, 2012112204, 'tool', 'monorail_admin_tool');
    }

    if ($oldversion < 2013020703) {

        //************************************ monorail_course_background ************************************//
        // Define table monorail_data to be created
        $table = new xmldb_table('monorail_course_background');

        // Adding fields to table monorail_data
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('filename', XMLDB_TYPE_CHAR, '80', null, null, null, null);
        $table->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_data
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_data
        $table->add_index('categoryid', XMLDB_INDEX_NOTUNIQUE, array('categoryid'));

        // Conditionally launch create table for monorail_data
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    // monorail_admin_tool savepoint reached
    upgrade_plugin_savepoint(true, 2013020703, 'tool', 'monorail_admin_tool');
    }

    return true;
}