<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2012 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');
require_once('admin_analytics_form.php');

require_login();
admin_externalpage_setup('monorail_admin_analytics');

$admins = get_admins();
$isadmin = false;
foreach ($admins as $admin) {
 if ($USER->id == $admin->id) {
  $isadmin = true;
  break;
 }
}
if (!$isadmin) {
 print_error('invaliduserid');
 die();
}

global $DB;

$assesstimestart = optional_param('assesstimestart', 0, PARAM_INT);
$assesstimefinish = optional_param('assesstimefinish', 0, PARAM_INT);
$courses = optional_param('courses', 0, PARAM_INT);
$participants = optional_param('participants', 0, PARAM_INT);
$signupse4b = optional_param('signupse4b', 0, PARAM_INT);
$tcourses = optional_param('tcourses', 0, PARAM_INT);
$tparticipants = optional_param('tparticipants', 0, PARAM_INT);
$tsignupse4b = optional_param('tsignupse4b', 0, PARAM_INT);
$activeusers = optional_param('activeusers', 0, PARAM_INT);
$activeuserse4b = optional_param('activeuserse4b', 0, PARAM_INT);
$signups = optional_param('signups', 0, PARAM_INT);
$signupse4busers = optional_param('signupse4busers', 0, PARAM_INT);
$bcstudents = optional_param('bcstudents', 0, PARAM_INT);
$bccstudents = optional_param('bccstudents', 0, PARAM_INT);
$bpcstudents = optional_param('bpcstudents', 0, PARAM_INT);
$bpcstudentsc = optional_param('bpcstudentsc', 0, PARAM_INT);
$bpcstudentscp = optional_param('bpcstudentscp', 0, PARAM_INT);
$cstudents = optional_param('cstudents', 0, PARAM_INT);
$ccstudents = optional_param('ccstudents', 0, PARAM_INT);
$pcstudents = optional_param('pcstudents', 0, PARAM_INT);
$pcstudentsc = optional_param('pcstudentsc', 0, PARAM_INT);
$pcstudentscp = optional_param('pcstudentscp', 0, PARAM_INT);
$bcourses = optional_param('bcourses', 0, PARAM_INT);
$bccourse = optional_param('bccourse', 0, PARAM_INT);
$bpccourse = optional_param('bpccourse', 0, PARAM_INT);
$bpccoursec = optional_param('bpccoursec', 0, PARAM_INT);
$bpccoursecp = optional_param('bpccoursecp', 0, PARAM_INT);
$xcourses = optional_param('xcourses', 0, PARAM_INT);
$ccourse = optional_param('ccourse', 0, PARAM_INT);
$pccourse = optional_param('pccourse', 0, PARAM_INT);
$pccoursec = optional_param('pccoursec', 0, PARAM_INT);
$pccoursecp = optional_param('pccoursecp', 0, PARAM_INT);
$totale4blicences = optional_param('totale4blicences', 0, PARAM_INT);

$fbshares = optional_param('fbshares', 0, PARAM_INT);
$lnshares = optional_param('lnshares', 0, PARAM_INT);
$twshares = optional_param('twshares', 0, PARAM_INT);
//Share course
$fbcshares = optional_param('fbcshares', 0, PARAM_INT);
$lncshares = optional_param('lncshares', 0, PARAM_INT);
$twcshares = optional_param('twcshares', 0, PARAM_INT);
//Share course certificate
$fbccshares = optional_param('fbccshares', 0, PARAM_INT);
$lnccshares = optional_param('lnccshares', 0, PARAM_INT);
$twccshares = optional_param('twccshares', 0, PARAM_INT);
//Share course skills 
$fbcsshares = optional_param('fbcsshares', 0, PARAM_INT);
$lncsshares = optional_param('lncsshares', 0, PARAM_INT);
$twcsshares = optional_param('twcsshares', 0, PARAM_INT);
//Share course review
$fbcrshares = optional_param('fbcrshares', 0, PARAM_INT);
$lncrshares = optional_param('lncrshares', 0, PARAM_INT);
$twcrshares = optional_param('twcrshares', 0, PARAM_INT);
//Share course invite
$fbcishares = optional_param('fbcishares', 0, PARAM_INT);
$twcishares = optional_param('twcishares', 0, PARAM_INT);
//Share course complete(done)
$fbcdshares = optional_param('fbcdshares', 0, PARAM_INT);
$lncdshares = optional_param('lncdshares', 0, PARAM_INT);
$twcdshares = optional_param('twcdshares', 0, PARAM_INT);
//Share course enroll(done)
$fbceshares = optional_param('fbceshares', 0, PARAM_INT);
$lnceshares = optional_param('lnceshares', 0, PARAM_INT);
$twceshares = optional_param('twceshares', 0, PARAM_INT);


$form = new admin_analytics_form();

$form->set_data(
                 array('assesstimestart' => $assesstimestart,'assesstimefinish' => $assesstimefinish,'courses' => $courses,
                'participants' => $participants, 'signupse4b' => $signupse4b,
                'tcourses' => $tcourses, 'tparticipants' => $tparticipants, 'tsignupse4b' => $tsignupse4b,
                'activeusers' => $activeusers,'activeuserse4b' => $activeuserse4b,'signups' => $signups,
                'signupse4busers' => $signupse4busers,  
                'bcstudents' => $bcstudents, 'bccstudents' => $bccstudents, 'bpcstudents' => $bpcstudents, 
                'bpcstudentsc' => $bpcstudentsc, 'bpcstudentscp' => $bpcstudentscp, 
                'cstudents' => $cstudents, 'ccstudents' => $ccstudents, 'pcstudents' => $pcstudents, 
                'pcstudentsc' => $pcstudentsc, 'pcstudentscp' => $pcstudentscp, 
                'bcourses' => $bcourses, 'bccourse' => $bccourse, 'bpccourse' => $bpccourse, 
                'bpccoursec' => $bpccoursec, 'bpccoursecp' => $bpccoursecp,
                'xcourses' => $xcourses, 'ccourse' => $ccourse, 'pccourse' => $pccourse, 
                'fbshares' => $fbshares , 'fbcshares' => $fbcshares , 'fbccshares' => $fbccshares , 'fbcsshares' => $fbcsshares , 
                'fbcrshares' => $fbcrshares , 'fbcishares' => $fbcishares , 'fbcdshares' => $fbcdshares , 'fbceshares' => $fbceshares,
                'twshares' => $twshares , 'twcshares' => $twcshares , 'twccshares' => $twccshares , 'twcsshares' => $twcsshares , 
                'twcrshares' => $twcrshares , 'twcishares' => $twcishares , 'twcdshares' => $twcdshares , 'twceshares' => $twceshares,
                'lnshares' => $lnshares , 'lncshares' => $lncshares , 'lnccshares' => $lnccshares , 'lncsshares' => $lncsshares , 
                'lncrshares' => $lncrshares , 'lncdshares' => $lncdshares , 'lnceshares' => $lnceshares,
                'pccoursec' => $pccoursec, 'pccoursecp' => $pccoursecp, 'totale4blicences' => $totale4blicences));

ini_set('max_execution_time', 3000);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/admin/tool/monorail_admin_tool/admin_analytics.php'));
} else if ($fromform = $form->get_data()) {
    $starttime = $fromform->assesstimestart; 
    $endtime = $fromform->assesstimefinish; 
    if(($starttime > $endtime) || ($starttime == $endtime)) {
       redirect(new moodle_url('/admin/tool/monorail_admin_tool/admin_analytics.php'));
    }

    $activeuserse4b = 0;
    $signupse4busers = 0;
    $susers = $DB->get_records_sql("SELECT u.id, u.timecreated, u.deleted, u.suspended FROM {user} AS u WHERE u.deleted=? AND u.suspended=? AND u.timecreated>=? AND u.timecreated<=?", array(0,0,$starttime,$endtime));
    foreach($susers as $suser) {
        if($DB->record_exists('cohort_members', array('userid'=>$suser->id))) {
            $signupse4busers = $signupse4busers + 1;
        }
    }

    $ausers = $DB->get_records_sql("SELECT u.id, u.timecreated, u.deleted, u.suspended FROM {user} AS u WHERE u.deleted=? AND u.suspended=? AND u.lastaccess>=? AND u.lastaccess<=?", array(0,0,$starttime,$endtime));
    foreach($ausers as $auser) {
        if($DB->record_exists('cohort_members', array('userid'=>$auser->id))) {
            $activeuserse4b = $activeuserse4b + 1;
        }
    }
    $signups = count($susers) - $signupse4busers; 
    $activeusers = count($ausers) - $activeuserse4b;

    $totalparticipants = $DB->count_records_sql("SELECT COUNT(*) FROM {user} WHERE deleted=? AND suspended=? AND timecreated<=?",array(0,0,$endtime));
    $totalcourses = $DB->count_records_sql("SELECT COUNT(*) FROM {course} WHERE timecreated<=?", array($endtime));
    $totale4b = $DB->count_records_sql("SELECT COUNT(*) FROM {cohort} WHERE timecreated<=?", array($endtime)); 

    //Cohort licences
    $crecord = $DB->get_record_sql("SELECT SUM(c.userscount) AS licences FROM {monorail_cohort_info} c JOIN (SELECT id FROM {cohort}) cht ON cht.id=c.id");
    $totale4blicences = 0; 
    if($crecord) {  
      $totale4blicences = $crecord->licences; 
    }
    $t_totale4b = $DB->count_records_sql("SELECT COUNT(*) FROM {cohort} WHERE timecreated>=? AND timecreated<=?", array($starttime, $endtime)); 
    $t_totalparticipants = $DB->count_records_sql("SELECT COUNT(*) FROM {user} AS u WHERE deleted=? AND suspended=? AND timecreated>=? AND timecreated<=?", 
                                              array(0,0,$starttime,$endtime)); 
    $t_totalcourses = $DB->get_records_sql("SELECT u.id, u.timecreated FROM {course} AS u WHERE u.timecreated>=? AND u.timecreated<=?", 
                                         array($starttime, $endtime));

    $st_bcstudents = 0; // E4B Total students
    $st_bccstudents = 0; // E4B Closed course students;
    $st_bpcstudents = 0; // E4B Public Course students(Non published);
    $st_bpcstudentsc = 0; // E4B Public catalog published course(free) students;
    $st_bpcstudentscp = 0; // E4B Public catalog published paid course students;

    $st_cstudents = 0; // Total students (Non E4B courses)
    $st_ccstudents = 0; // Closed course students;
    $st_pcstudents = 0; // Public course Students(Non published);
    $st_pcstudentsc = 0; // Public catalog published course(free) students;
    $st_pcstudentscp = 0; //Public catalog published paid course students;

    $st_bcourses = 0; // Total E4B courses
    $st_bccourse = 0; // E4B Closed Courses;
    $st_bpccourse = 0; // E4B Public courses(not published in catalog);
    $st_bpccoursec = 0; // E4b Public courses - Published in catalog, free;
    $st_bpccoursecp = 0; // E4B Public courses - Published in catalog , non free;

    $st_courses = 0; // Total Non E4B courses
    $st_ccourse = 0; // Non E4B Closed Courses;
    $st_pccourse = 0; // Non E4B Public courses(not published in catalog);
    $st_pccoursec = 0; // Non E4b Public courses - Published in catalog, free;
    $st_pccoursecp = 0; // Non E4B Public courses - Published in catalog , non free;

    foreach($t_totalcourses as $etcourse) {
       $pst = $DB->count_records_sql("SELECT COUNT(id) FROM {enrol} WHERE courseid=?", array($etcourse->id));
       $public = $DB->count_records("monorail_data", array("datakey" => 'public', "itemid" => $etcourse->id));
       $public_catalog = $DB->count_records("monorail_data", array("datakey" => 'public_catalog', "itemid" => $etcourse->id));
       $cohort = false;
       if($DB->record_exists('monorail_cohort_courseadmins', array('courseid'=> $etcourse->id))) {
           $cohort = true;
       } 

       if(($public > 0) || ($public_catalog > 0)) {
         if(!($public_catalog > 0)) {
           if(!$cohort) {
             $st_pcstudents = $st_pcstudents + $pst;
             $st_pccourse = $st_pccourse + 1;
           } else {
             $st_bpcstudents = $st_bpcstudents + $pst;
             $st_bpccourse = $st_bpccourse + 1;
           }
         } else {
           //Course published in catalog 
           $course_price = $DB->get_record("monorail_data", array("datakey" => 'course_price', "itemid" => $etcourse->id));
           if($course_price && ($course_price->value > 0)) {
             if(!$cohort) {
               $st_pccoursecp = $st_pccoursecp + 1;
               $st_pcstudentscp = $st_pcstudentscp + $pst;
             } else {
               $st_bpccoursecp = $st_bpccoursecp + 1;
               $st_bpcstudentscp = $st_bpcstudentscp + $pst;
             }
           } else {
             if(!$cohort) {
               $st_pccoursec = $st_pccoursec + 1;
               $st_pcstudentsc = $st_pcstudentsc + $pst;
             } else {
               $st_bpccoursec = $st_bpccoursec + 1;
               $st_bpcstudentsc = $st_bpcstudentsc + $pst;
             }
           }
         }
       } else {
         if(!$cohort) {
           $st_ccstudents = $st_ccstudents + $pst;
           $st_cccourse = $st_cccourse + 1;
         } else {
           $st_bccstudents = $st_bccstudents + $pst;
           $st_bccourse = $st_bccourse + 1;
         }
       }
    }



    $st_bcstudents = $st_bccstudents + $st_bpcstudents + $st_bpcstudentsc + $st_bpcstudentscp; // E4B Total students
    $st_cstudents = $st_ccstudents + $st_pcstudents + $st_pcstudentsc + $st_pcstudentscp; // Total students (Non E4B courses) 
    $st_bcourses = $st_bccourse + $st_bpccourse + $st_bpccoursec + $st_bpccoursecp; // Total E4B courses
    $st_courses = $st_cccourse + $st_pccourse + $st_pccoursec + $st_pccoursecp; // Total Non E4B courses


    $fbshares =  0;
    $lnshares =  0;
    $twshares =  0;

    //Share course
    $fbcshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-facebook-course',$starttime,$endtime));
    $lncshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-linkedin-course',$starttime,$endtime));
    $twcshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-twitter-course',$starttime,$endtime));
    //Share course certificate
    $fbccshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-facebook-certificate',$starttime,$endtime));
    $lnccshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-linkedin-certificate',$starttime,$endtime));
    $twccshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-twitter-certificate',$starttime,$endtime));
    //Share course skills
    $fbcsshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-facebook-skills',$starttime,$endtime));
    $lncsshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-linkedin-skills',$starttime,$endtime));
    $twcsshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-twitter-skills',$starttime,$endtime));
    //Share course review
    $fbcrshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-facebook-review',$starttime,$endtime));
    $lncrshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-linkedin-review',$starttime,$endtime));
    $twcrshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-twitter-review',$starttime,$endtime));
    //Share course invite
    $fbcishares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-facebook-course-invite',$starttime,$endtime));
    $twcishares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-twitter-course-invite',$starttime,$endtime));
    //Share course complete(done)
    $fbcdshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-facebook-course-done',$starttime,$endtime));
    $lncdshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-linkedin-course-done',$starttime,$endtime));
    $twcdshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-twitter-course-done',$starttime,$endtime));
    //Share course enroll(done)
    $fbceshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-facebook-course-enroll',$starttime,$endtime));
    $lnceshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-linkedin-course-enroll',$starttime,$endtime));
    $twceshares = $DB->count_records_sql("SELECT COUNT(id) FROM {monorail_user_activity_log} WHERE action=? AND timestamp>=? AND timestamp<=?", array('s-twitter-course-enroll',$starttime,$endtime));


    $fbshares = $fbcshares + $fbccshares + $fbcsshares + $fbcrshares + $fbcishares + $fbcdshares + $fbceshares;
    $twshares = $twcshares + $twccshares + $twcsshares + $twcrshares + $twcishares + $twcdshares + $twceshares;
    $lnshares = $lncshares + $lnccshares + $lncsshares + $lncrshares + $lncdshares + $lnceshares;

    
    redirect(new moodle_url('/admin/tool/monorail_admin_tool/admin_analytics.php', 
                   array('assesstimestart' => $fromform->assesstimestart,'assesstimefinish' => $fromform->assesstimefinish,
                   'courses' => $totalcourses, 'participants' => $totalparticipants, 'signupse4b' => $totale4b,
                   'tcourses' => count($t_totalcourses), 'tparticipants' => $t_totalparticipants, 'tsignupse4b' => $t_totale4b,
                   'activeusers' => $activeusers,'activeuserse4b' => $activeuserse4b,'signups' => $signups,
                   'signupse4busers' => $signupse4busers,  
                   'bcstudents' => $st_bcstudents, 'bccstudents' => $st_bccstudents, 'bpcstudents' => $st_bpcstudents, 
                   'bpcstudentsc' => $st_bpcstudentsc, 'bpcstudentscp' => $st_bpcstudentscp, 
                   'cstudents' => $st_cstudents, 'ccstudents' => $st_ccstudents, 'pcstudents' => $st_pcstudents, 
                   'pcstudentsc' => $st_pcstudentsc, 'pcstudentscp' => $st_pcstudentscp, 
                   'bcourses' => $st_bcourses, 'bccourse' => $st_bccourse, 'bpccourse' => $st_bpccourse, 
                   'bpccoursec' => $st_bpccoursec, 'bpccoursecp' => $st_bpccoursecp,
                   'xcourses' => $st_courses, 'ccourse' => $st_cccourse, 'pccourse' => $st_pccourse, 
                   'fbshares' => $fbshares , 'fbcshares' => $fbcshares , 'fbccshares' => $fbccshares , 'fbcsshares' => $fbcsshares , 
                   'fbcrshares' => $fbcrshares , 'fbcishares' => $fbcishares , 'fbcdshares' => $fbcdshares , 'fbceshares' => $fbceshares,
                   'twshares' => $twshares , 'twcshares' => $twcshares , 'twccshares' => $twccshares , 'twcsshares' => $twcsshares , 
                   'twcrshares' => $twcrshares , 'twcishares' => $twcishares , 'twcdshares' => $twcdshares , 'twceshares' => $twceshares,
                   'lnshares' => $lnshares , 'lncshares' => $lncshares , 'lnccshares' => $lnccshares , 'lncsshares' => $lncsshares , 
                   'lncrshares' => $lncrshares , 'lncdshares' => $lncdshares , 'lnceshares' => $lnceshares,
                   'pccoursec' => $st_pccoursec, 'pccoursecp' => $st_pccoursecp, 'totale4blicences' => $totale4blicences)));
}

echo $OUTPUT->header();
$form->display();
echo $OUTPUT->footer();
