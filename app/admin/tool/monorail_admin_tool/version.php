<?php
/**
 * Monorail Admin Tool info
 *
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  Copyright 2012, CBTec Oy. All rights reserved.
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2013102000; // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012061700; // Requires this Moodle version
$plugin->component = 'tool_monorail_admin_tool'; // Full name of the plugin (used for diagnostics)
$plugin->dependencies 	= array(
		'theme_monorail' => ANY_VERSION
);
