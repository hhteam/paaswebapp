<?php
/**
 * @package    tool
 * @subpackage monorail_admin_tool
 * @copyright  2015 Cloudberry Tec
 */

require_once('../../../config.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

require_login();
admin_externalpage_setup('admin_coursepurchases');

$admins = get_admins();
$isadmin = false;
foreach ($admins as $admin) {
 if ($USER->id == $admin->id) {
  $isadmin = true;
  break;
 }
}

if (!$isadmin) {
 print_error('invaliduserid');
}

    if (@$_POST["action"] == "save")
    {
        switch ($_POST["type"]) {
            case 1: $table = "monorail_course_payments"; break;
            case 2: $table = "monorail_cert_order"; break;
            case 3: $table = "monorail_cohort_invoice"; break;
        }

        $DB->execute("UPDATE {" . $table . "} SET quantity=?, unitprice=?, totalprice=?, totaltax=?, notes=? WHERE id=?",
            array((int)$_POST["quantity"], (float)$_POST["unitprice"], (float)$_POST["totalprice"], (float)$_POST["totaltax"], $_POST["notes"], $_POST["id"]));

        header("Location: /app/admin/tool/monorail_admin_tool/admin_coursepurchases.php?type=" . $_POST["type"] . "&start_date=" . $_POST["start_date"] . "&end_date=" . $_POST["end_date"]);
        exit(0);
    }

    if (@$_GET["action"] == "teacher_paid") {
        switch ($_GET["type"]) {
            case 1:
                $DB->execute("UPDATE {monorail_course_payments} SET teacher_paid=? WHERE id=?", array(time(), $_GET["id"]));
                break;

            case 2:
                $DB->execute("UPDATE {monorail_cert_order} SET teacher_paid=? WHERE id=?", array(time(), $_GET["id"]));
                break;
        }

        header("Location: /app/admin/tool/monorail_admin_tool/admin_coursepurchases.php?type=" . $_GET["type"] . "&start_date=" . $_GET["start_date"] . "&end_date=" . $_GET["end_date"]);
        exit(0);
    }

    echo $OUTPUT->header();

    $start_date = strtotime(@$_GET["start_date"]);
    $end_date = strtotime(@$_GET["end_date"]);
    $type = $_GET["type"];

    if (!$start_date) {
        $start_date = strtotime("first day of this month midnight");
    } else {
        $start_date = strtotime("midnight", $start_date);
    }

    if (!$end_date) {
        $end_date = strtotime("last day of this month midnight");
    } else {
        $end_date = strtotime("midnight", $end_date);
    }

    if (!$type) {
        $type = 1;
    }

    if (@$_GET["action"] == "edit") {

        switch ($_GET["type"]) {
            case 1: $table = "monorail_course_payments"; break;
            case 2: $table = "monorail_cert_order"; break;
            case 3: $table = "monorail_cohort_invoice"; break;
        }

        $info = $DB->get_record_sql("SELECT id, quantity, unitprice, totalprice, totaltax, notes FROM {" . $table . "} WHERE id=?", array($_GET["id"]));

?>

<form method="post" action="?" style="border: 1px solid #000; padding: 1em;">
<div style="margin-bottom: 6px;"><label><span style="min-width: 80px; display: inline-block;">Quantity:</span><input type="text" value="<?php echo $info->quantity ?>" name="quantity"></label></div>
<div style="margin-bottom: 6px;"><label><span style="min-width: 80px; display: inline-block;">Unit price:</span><input type="text" value="<?php echo $info->unitprice ?>" name="unitprice"></label></div>
<div style="margin-bottom: 6px;"><label><span style="min-width: 80px; display: inline-block;">Total price:</span><input type="text" value="<?php echo $info->totalprice ?>" name="totalprice"></label></div>
<div style="margin-bottom: 6px;"><label><span style="min-width: 80px; display: inline-block;">Total tax:</span><input type="text" value="<?php echo $info->totaltax ?>" name="totaltax"></label></div>
<div style="margin-bottom: 6px;"><label><span style="min-width: 80px; display: inline-block; vertical-align: top;">Notes:</span><textarea name="notes" rows="5" cols="40"><?php echo $info->notes ?></textarea></div>
<div><input type="submit" value="Save"></div>
<input type="hidden" name="start_date" value="<?php echo $_GET["start_date"] ?>">
<input type="hidden" name="end_date" value="<?php echo $_GET["end_date"] ?>">
<input type="hidden" name="type" value="<?php echo $_GET["type"] ?>">
<input type="hidden" name="id" value="<?php echo $_GET["id"] ?>">
<input type="hidden" name="action" value="save">
</form>

<?php } else { ?>

<form style="border: 1px solid #000; padding: 1em; margin: 1em 0px;" method="get" action="?">
 <label style="margin-right: 1em;">Start date: <input type="date" name="start_date" value="<?php echo date("Y-m-d", $start_date) ?>"></label>
 <label style="margin-right: 1em;">End date: <input type="date" name="end_date" value="<?php echo date("Y-m-d", $end_date); ?>"></label>
 <label style="margin-right: 1em;">Type:
  <select name="type">
   <option value="1"<?php if ($type == 1) echo " selected=\"selected\"" ?>>Enrollments</option>
   <option value="2"<?php if ($type == 2) echo " selected=\"selected\"" ?>>Certificates</option>
   <option value="3"<?php if ($type == 3) echo " selected=\"selected\"" ?>>Premiums</option>
  </select>
 </label>
 <input type="submit" value="Apply">
</form>

<?php

    if ($type == 1)
    {
        $items = $DB->get_records_sql("SELECT mcp.id AS id, c.id AS cid, u.id AS userid, c.fullname AS fullname, u.firstname AS firstname, u.lastname AS lastname, " .
                "mcp.timestamp AS timestamp, mcp.totalprice AS totalprice, mcp.totaltax AS totaltax, mcd.mainteacher AS mainteacher, mcp.notes AS notes, mcp.teacher_paid AS teacher_paid " .
            "FROM {monorail_course_payments} AS mcp " .
                "INNER JOIN {course} AS c ON c.id=mcp.courseid " .
                "INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=mcp.courseid " .
                "INNER JOIN {user} AS u ON u.id=mcp.userid " .
                "WHERE mcp.timestamp>=? AND mcp.timestamp<=? AND mcp.active=0 ORDER BY c.id, mcp.timestamp",
                    array($start_date, strtotime("next day midnight", $end_date)));
    }
    else if ($type == 2)
    {
        $items = $DB->get_records_sql("SELECT mco.id AS id, c.id AS cid, u.id AS userid, c.fullname AS fullname, u.firstname AS firstname, u.lastname AS lastname, " .
                "mco.created AS timestamp, mco.totalprice AS totalprice, mco.totaltax AS totaltax, mcd.mainteacher AS mainteacher, mco.notes AS notes, mco.teacher_paid AS teacher_paid " .
            "FROM {monorail_cert_order} AS mco " .
                "INNER JOIN {course} AS c ON c.id=mco.courseid " .
                "INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=mco.courseid " .
                "INNER JOIN {user} AS u ON u.id=mco.userid " .
                "WHERE mco.created>=? AND mco.created<=? AND mco.status>=2 ORDER BY c.id, mco.created",
                    array($start_date, strtotime("next day midnight", $end_date)));
    }
    else if ($type == 3)
    {
        $items = $DB->get_records_sql("SELECT mci.id AS id, c.id AS cid, u.id AS userid, c.name AS fullname, u.firstname AS firstname, u.lastname AS lastname, " .
                "mci.timepaid AS timestamp, mci.totalprice AS totalprice, mci.totaltax AS totaltax, mci.invoicetype AS invoicetype, " .
                "mcinfo.billing_details AS billing_details, mci.notes AS notes, NULL AS teacher_paid " .
            "FROM {monorail_cohort_invoice} AS mci " .
                "INNER JOIN {cohort} AS c ON c.id=mci.cohortid " .
                "INNER JOIN {user} AS u ON u.id=mci.paidby " .
                "INNER JOIN {monorail_cohort_info} AS mcinfo ON mcinfo.cohortid=mci.cohortid " .
                "WHERE mci.timepaid>=? AND mci.timepaid<=? AND mci.paymentstatus=2 ORDER BY c.id, mci.timepaid",
                    array($start_date, strtotime("next day midnight", $end_date)));
    }
    else
    {
        $items = array();
    }

    echo "<table style=\"width: 100%; border: 1px solid #000;\">";
    echo "<tr><td style=\"padding-left: 4em;\">Date</td><td>User</td><td>Total price</td><td>Total tax</td><td>Notes</td><td>Teacher paid</td><td>&nbsp;</td></tr>";

    $oldCid = 0;

    foreach ($items as $item)
    {
        if ($oldCid != $item->cid)
        {
            echo "<tr style=\"font-weight: bold;\">";

            if ($type == 1 || $type == 2)
            {
                $tinfo = $DB->get_record_sql("SELECT id, firstname, lastname FROM {user} WHERE id=?", array($item->mainteacher));
                $pp = $DB->get_field_sql("SELECT value FROM {monorail_data} WHERE itemid=? AND datakey='course_paypal_account'", array($item->cid));

                echo "<td colspan=\"7\"><a href=\"/app/course/view.php?id=" . $item->cid . "\">" . $item->fullname . "</a> (Main teacher: <a href=\"/app/user/profile.php?id=" .
                        $item->mainteacher . "\">" . $tinfo->firstname . " " . $tinfo->lastname . "</a>, PayPal account: " . $pp . ")</td>";
            }
            else if ($type == 3)
            {
                $cinfo = array();

                try {
                    $cinfo = json_decode($item->billing_details);
                } catch (Exception $err) { }

                echo "<td colspan=\"7\"><a href=\"/app/admin/tool/monorail_admin_tool/admin_e4b.php?cid=" . $item->cid . "\">" . $item->fullname .
                        "</a> (Company: " . @$cinfo->company_name . ", VAT ID: " . @$cinfo->vat_id . ", Country: " . @$cinfo->country . ")" . "</td>";
            }
            else
            {
                echo "<td colspan=\"7\">&nbsp;</td>";
            }

            echo "</tr>";
            $oldCid = $item->cid;
        }

        echo "<tr style=\"border-bottom: 1px solid #EEE;\">";
        echo "<td style=\"padding-left: 4em;\">" . date("Y-m-d H:i", $item->timestamp) . "</td>";
        echo "<td><a href=\"/app/user/profile.php?id=" . $item->userid . "\">" . $item->firstname . " " . $item->lastname . "</a></td>";

        if ($item->totalprice) {
            echo "<td>" . $item->totalprice . "</td>";
            echo "<td>" . $item->totaltax . "</td>";
        } else {
            echo "<td>-</td>";
            echo "<td>-</td>";
        }

        echo "<td style=\"width: 30%;\">" . $item->notes;

        if ($type == 3) {
            switch ($item->invoicetype) {
                case 1: echo "&nbsp;&nbsp;<em>(Per user)</em>"; break;
                case 2: echo "&nbsp;&nbsp;<em>(Prepaiment)</em>"; break;
            }
        }

        echo "</td>";

        if ($type == 1 || $type == 2) {
            echo "<td>" . ($item->teacher_paid ? ("Teacher paid " . date("Y-m-d H:i", $item->teacher_paid)) : "Teacher not paid [<a href=\"?type=" . $type . "&action=teacher_paid&id=" . $item->id . "&start_date=" . date("Y-m-d", $start_date) . "&end_date=" . date("Y-m-d", $end_date) . "\">Mark as paid</a>]") . "</td>";
        } else {
            echo "<td>-</td>";
        }

        echo "<td>[<a href=\"?type=" . $type . "&id=" . $item->id . "&action=edit&start_date=" . date("Y-m-d", $start_date) . "&end_date=" . date("Y-m-d", $end_date) . "\">Edit</a>]</td>";
        echo "</tr>";
    }

    echo "</table>";
}

    echo $OUTPUT->footer();
