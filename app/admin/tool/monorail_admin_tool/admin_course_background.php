<?php
/**
 * @package    theme
 * @subpackage monorail
 * @copyright  2012 Cloudberry Tec
 */

	require_once('../../../config.php');
	require_once($CFG->libdir.'/adminlib.php');
	require_once($CFG->libdir.'/filelib.php');

	require_login();
	admin_externalpage_setup('monorail_admin_course_background');

	$admins = get_admins();
    $isadmin = false;
    foreach ($admins as $admin) {
        if ($USER->id == $admin->id) {
            $isadmin = true;
            break;
        }
    }
    if (!$isadmin) {
    	print_error('invaliduserid');
    }

	echo $OUTPUT->header();

	// Get all available course background
	$categories = get_categories('none', 'path ASC');

	echo '<select id="selCategories">';

	foreach ($categories as $category) {
		$level = str_repeat('-', $category->depth - 1);
		echo "<option value='$category->id'>$level$category->name</option>";
	}

	echo '</select>';

	echo '<div id="pictureuploadcontainer"></div>';

	echo <<<HTML
	<input type="button" value="Delete" id="btDelete">
    <div id="myCarousel" class="carousel slide">
    <!-- Carousel items -->
    <div class="carousel-inner">
    </div>
    <!-- Carousel nav -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
HTML;

	$PAGE->requires->js('/admin/tool/monorail_admin_tool/js/jquery.min.js');
	$PAGE->requires->js('/theme/monorail/javascript/bootstrap.min.js');
	$PAGE->requires->js_init_call('M.tool_monorail_admin_tool.initiateCourseBackground');

	echo $OUTPUT->footer();
