<?php
/**
 * Monorail course overview
 * 
 * @package   monorail_course_overview
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once($CFG->dirroot.'/lib/weblib.php');
require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->dirroot.'/theme/monorail/lib.php');

class block_monorail_course_overview extends block_base {
    /**
     * block initializations
     */
    public function init() {
        $this->title   = get_string('pluginname', 'block_monorail_course_overview');
    }

    public function instance_allow_multiple() {
    	return true;
    }

    public function specialization() {
    	if (empty($this->config)) {
    		$this->config = new stdClass();
    		$this->config->type = 'ongoingcourses';
    	}
    	$this->title = get_string($this->config->type, 'block_monorail_course_overview');
    }

    function html_attributes() {
    	$attributes = array(
    			'id' => 'inst' . $this->instance->id,
    			'class' => 'block_' . $this->name(). '  block ' .$this->config->type
    	);
    	if ($this->instance_can_be_docked() && get_user_preferences('docked_block_instance_'.$this->instance->id, 0)) {
    		$attributes['class'] .= ' dock_on_load';
    	}
    	return $attributes;
    }

    public function hide_header() {
    	return true;
    }

    /**
     * block contents
     *
     * @return object
     */
    public function get_content() {
        global $USER, $CFG;

        if($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->text = '';
        $this->content->footer = '';

        $content = array();

        $courses = enrol_get_my_courses('id, shortname, modinfo', 'fullname ASC');
        $site = get_site();
        $course = $site; //just in case we need the old global $course hack

        if (is_enabled_auth('mnet')) {
            $remote_courses = get_my_remotecourses();
        }
        if (empty($remote_courses)) {
            $remote_courses = array();
        }

        if (array_key_exists($site->id,$courses)) {
            unset($courses[$site->id]);
        }

	    if ($this->config->type == 'ongoingcourses') {
	        foreach ($courses as $key => $c) {
	        	$info = new completion_info($c);
	        	if ($info->is_course_complete($USER->id)) {
	        		unset($courses[$key]);
	        		continue;
	        	}

	            if (isset($USER->lastcourseaccess[$c->id])) {
	                $courses[$c->id]->lastaccess = $USER->lastcourseaccess[$c->id];
	            } else {
	                $courses[$c->id]->lastaccess = 0;
	            }
	        }

	        if (empty($courses) && empty($remote_courses)) {
	            $content[] = '<div class="empty">'.get_string('nocourses','my').'</div>';
	        } else {
	            ob_start();

	            require_once $CFG->dirroot."/course/lib.php";
	            $this->print_overview($courses, $remote_courses);

	            $content[] = ob_get_contents();
	            ob_end_clean();
	        }

	        $this->content->text = implode($content);
        }
        else {
        	$completedcourses = array();

        	foreach ($courses as $c) {
        		$info = new completion_info($c);
        		if ($info->is_course_complete($USER->id)) {
        			$completedcourses[] = $c;
        		}
        	}

        	if (!empty($completedcourses)) {
        		require_once($CFG->libdir.'/gradelib.php');
        		require_once($CFG->dirroot.'/grade/querylib.php');

        		ob_start();

        		$this->print_completed_courses($completedcourses);
        		$content[] = ob_get_contents();
        		ob_end_clean();

        		$this->content->text = implode($content);
        	}
        }

        if ($this->content->text) {
			$header  = "<div class='title'>";
			$header .= "<div class='box-icon'><span class='icon'></span></div>";
			$header .= "<h2 class='title-block'>$this->title</h2>";
			$header .= "</div>";

			$this->content->text = $header.$this->content->text;
        }
        return $this->content;
    }

    /**
     * allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats() {
        return array('my-index'=>true);
    }

    function print_completed_courses($courses) {
    	global $CFG, $USER, $DB, $OUTPUT;

    	foreach ($courses as $course) {
    		$grade 	  = grade_get_course_grade($USER->id, $course->id);
    		$fullname = format_string($course->fullname, true, array('context' => get_context_instance(CONTEXT_COURSE, $course->id)));
    		if ($grade->grade) {
    			$fullname.= " <span class='course-grade'>$grade->str_grade</span>";
    		}
    		$fullname.= " <span class='course-code'>$course->idnumber</span>";
    		$attributes = array('title' => s($fullname));
    		if (empty($course->visible)) {
    			//$attributes['class'] = 'dimmed';
    			continue;
    		}

    		//$completedbox = "<span class='completed-icon'></span>";

    		$notificationtext = '<li class="coursesubmenucont" id="coursesubmenucont_'.$course->id.'"><span id="course_ncount_'.$course->id.'" class="completed-icon notification floatright"></span>';
    		$notificationtext .= '<div class="coursesubmenu hide" id="coursesubmenu_'.$course->id.'"><img class="coursesubmenuarrow" src="'.$OUTPUT->pix_url('arrow-dialog-box-left', 'theme').'"><table class="coursesubmenutable">';
    		$notificationtext .= '<tr id="noverview_subcount_'.$course->id.'" class="hide"><th></th><td><a href="'.
    				$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.
    				get_string('overview','block_monorail_course_overview').'</a></td></tr>';
    		$notificationtext .= '<tr id="ntasks_subcount_'.$course->id.'" class="hide"><th></th><td><a href="'.
    				$CFG->wwwroot.'/theme/monorail/index.php?id='.$course->id.'&mod=tasks">'.
    				get_string('tasks','block_monorail_course_overview').'</a></td></tr>';
    		$notificationtext .= '<tr id="nforum_subcount_'.$course->id.'" class="hide"><th></th><td><a href="'.
    				$CFG->wwwroot.'/theme/monorail/forum.php?c='.$course->id.'">'.
    				get_string('forum','block_monorail_course_overview').'</a></td></tr>';
    		$notificationtext .= '</table></div></li>';
    		$notificationtext .= '<div class="hide" id="coursesubmenulineimg">'.$OUTPUT->pix_url('line', 'theme').'</div>';

    		echo $OUTPUT->box_start('coursebox');
    		echo $OUTPUT->heading($notificationtext . html_writer::link(
    				new moodle_url('/course/view.php', array('id' => $course->id)), $fullname, $attributes), 3);
    		echo $OUTPUT->box_end();
    	}
    }

    function print_overview($courses, $remote_courses) {
    	global $CFG, $USER, $DB, $OUTPUT;

    	$htmlarray = array();
    	$this->print_all_tasks($courses, $htmlarray);

    	foreach ($courses as $course) {
    		$fullname 	= format_string($course->fullname, true, array('context' => get_context_instance(CONTEXT_COURSE, $course->id)));
    		$attributes = array('title' => s($fullname));
    		$fullname  .= " <span class='course-code'>$course->idnumber</span>";
    		if (empty($course->visible)) {
    			//$attributes['class'] = 'dimmed';
    			continue;
    		}
    		echo $OUTPUT->box_start('coursebox course-'.$course->id);

            $notificationtext = '<li class="coursesubmenucont" id="coursesubmenucont_'.$course->id.'"><span id="course_ncount_'.$course->id.'" class="notification floatright"></span>';
            $notificationtext .= '<div class="coursesubmenu hide" id="coursesubmenu_'.$course->id.'"><img class="coursesubmenuarrow" src="'.$OUTPUT->pix_url('arrow-dialog-box-left', 'theme').'"><table class="coursesubmenutable">';
            $notificationtext .= '<tr id="noverview_subcount_'.$course->id.'" class="hide"><th></th><td><a href="'.
                $CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.
                get_string('overview','block_monorail_course_overview').'</a></td></tr>';
            $notificationtext .= '<tr id="ntasks_subcount_'.$course->id.'" class="hide"><th></th><td><a href="'.
                $CFG->wwwroot.'/theme/monorail/index.php?id='.$course->id.'&mod=tasks">'.
                get_string('tasks','block_monorail_course_overview').'</a></td></tr>';
            $notificationtext .= '<tr id="nforum_subcount_'.$course->id.'" class="hide"><th></th><td><a href="'.
                $CFG->wwwroot.'/theme/monorail/forum.php?c='.$course->id.'">'.
                get_string('forum','block_monorail_course_overview').'</a></td></tr>';
            $notificationtext .= '</table></div></li>';
            $notificationtext .= '<div class="hide" id="coursesubmenulineimg">'.$OUTPUT->pix_url('line', 'theme').'</div>';
        	echo $OUTPUT->heading($notificationtext . html_writer::link(
    				new moodle_url('/course/view.php', array('id' => $course->id)), $fullname, $attributes), 3);
    		if (array_key_exists($course->id,$htmlarray)) {
    			foreach ($htmlarray[$course->id] as $html) {
    				echo $html;
    			}
    		}
            echo '<div class="nmarker ncourse_'.$course->id.'"></div>';
    		echo $OUTPUT->box_end();
    	}
	}

    function get_all_open_task_instances($courses) {
        global $USER, $DB, $CFG;

        require_once($CFG->libdir.'/gradelib.php');

        $outputarray = array();

        if (empty($courses) || !is_array($courses) || count($courses) == 0) {
            return $outputarray;
        }

        list($coursessql, $params) = $DB->get_in_or_equal(array_keys($courses));

        $query = <<<EOD
            select cm.id as cmid, m.name as modulename, cm.instance, cs.section, cm.course from {course_modules} cm, {course_sections} cs, {modules} m
                where
                    m.name in ('assign', 'quiz') AND
                    m.id = cm.module AND
                    cm.course $coursessql AND
                    cm.visible = 1 AND
                    cm.section = cs.id
EOD;
        if (!$rawtasks = $DB->get_records_sql($query, $params)) {
            return $outputarray;
        }

        $assignids = array();
        $quizids = array();
        foreach ($rawtasks as $task) {
            if ($task->modulename == 'assign') {
                $assignids[] = $task->instance;
            } else if ($task->modulename == 'quiz') {
                $quizids[] = $task->instance;
            }
        }

        if (count($assignids) > 0) {
            list($assignidssql, $params) = $DB->get_in_or_equal($assignids, SQL_PARAMS_NAMED, 'c0');
            $query = <<<EOD
                select {assign}.id, name, allowsubmissionsfromdate, preventlatesubmissions, duedate, {assign_submission}.id as submissionid
                    from {assign} left outer join {assign_submission} on
                        {assign}.id = {assign_submission}.assignment AND
                        {assign_submission}.userid = :userid
                    where
                        {assign}.id $assignidssql
EOD;
            $params['userid'] = $USER->id;
            $assigns = $DB->get_records_sql($query, $params);
        }
        if (count($quizids) > 0) {
            list($quizidssql, $params) = $DB->get_in_or_equal($quizids, SQL_PARAMS_NAMED, 'c0');
            $query = <<<EOD
                select {quiz}.id, name, timeopen, timeclose, {quiz_attempts}.id as attemptid
                    from {quiz} left outer join {quiz_attempts} on
                        {quiz}.id = {quiz_attempts}.quiz AND
                        {quiz_attempts}.userid = :userid
                    where
                        {quiz}.id $quizidssql
                    group by
            			{quiz}.id
EOD;
            $params['userid'] = $USER->id;
            $quizzes = $DB->get_records_sql($query, $params);
        }

        $time = time();
        $tasks = array();
        foreach ($rawtasks as $task) {
            if ($task->modulename == 'assign') {
                $module = $assigns[$task->instance];
                if (!isset($module->submissionid)) {
                    if ($module->duedate) {
                        if ($module->preventlatesubmissions) {
                            $isopen = ($module->allowsubmissionsfromdate <= $time && $time <= $module->duedate);
                        } else {
                            $isopen = ($module->allowsubmissionsfromdate <= $time);
                        }
                    }
                    if (empty($isopen) || empty($module->duedate) || $isopen) {
                        // Check whether the teacher has graded this assignment or not
                    	$grading_info = grade_get_grades($task->course, 'mod', 'assign', $task->instance, $USER->id);

                    	if (isset($grading_info->items[0]) && !$grading_info->items[0]->grades[$USER->id]->hidden && $grading_info->items[0]->grades[$USER->id]->str_grade != '-') {
                    		continue;
                    	}
                    } else {
                        continue;
                    }

                } else {
                	// In case there is submission, must check whether it is draft or final submission
                	$submission	= $DB->get_record('assign_submission', array('assignment'=>$module->id, 'userid'=>$USER->id));

                	// Already submitted
                	if ($submission->status == 'submitted') {
                    	continue;
                	}
                }

                $task->name = $module->name;
                $task->duedate = $module->duedate;
            } else if ($task->modulename == 'quiz') {
                $module = $quizzes[$task->instance];
                if (!isset($module->attemptid)) {
                    if ($module->timeopen && $module->timeclose) {
                        $isopen = ($module->timeopen <= $time && $time <= $module->timeclose);
                    } else if ($module->timeclose) {
                        $isopen = ($module->timeclose >= $time);
                    } else if ($module->timeopen) {
                        $isopen = ($module->timeopen <= $time);
                    }
                    if (empty($isopen) || $isopen) {
                        //ok
                    } else {
                        continue;
                    }
                    $task->name = $module->name;
                    $task->duedate = $module->timeclose;
                } else {
                    continue;
                }
            }
            $tasks[$task->cmid] = $task;
        }

        // order by duedate
        uasort($tasks, "sort_using_due_date");

        return $tasks;

    }

    function print_all_tasks($courses, &$htmlarray) {
        global $USER, $CFG, $DB;

        if (!$assignments = $this->get_all_open_task_instances($courses)) {
            return;
        }

        $strduedate 		= get_string('duedate', 'assign');
		$strduedateno 		= get_string('duedateno', 'assign');
		$strgraded 			= get_string('graded', 'assign');
		$strnotgradedyet 	= get_string('notgradedyet', 'assign');
		$strnotsubmittedyet = get_string('notsubmittedyet', 'assign');
		$strsubmitted 		= get_string('submitted', 'assign');
		$strreviewed 		= get_string('reviewed','assign');

		$showduedate		= $this->page->theme->name == 'monorail';

		foreach ($assignments as $assignment) {

            $strmodulename 		= get_string('modulename', $assignment->modulename);

            if ($assignment->modulename == 'assign') {
            	$link = $CFG->wwwroot.'/theme/monorail/assign.php?id='.$assignment->cmid;
            }
            else {
            	$link = $CFG->wwwroot.'/mod/'.$assignment->modulename.'/view.php?id='.$assignment->cmid;
            }
			$str = '<div class="'.$assignment->modulename.' normal overview clickable ntodo ntask_'.$assignment->cmid.'">';
			$str.= '<div class="name">';
			$str.= '<a class="assignment-name" '.'title="'.$strmodulename.'" href="'.$link.'">'.format_string($assignment->name).'</a>';
			$str.= '</div>';

			if ($showduedate) {
				if ($assignment->duedate) {
					$date = userdate($assignment->duedate);
					$str .= '<div class="task-due">'.$strduedate.': '.userdate($assignment->duedate, '%d.%m.%Y, %H:%M');

					// Calculate and show how much time is left for the assignment
					$timeleft = date_diff(new DateTime($date), new DateTime(date("m/d/Y h:i:s a", time())));

					// Check if overdue or timeleft < 2 days
					$urgent = $timeleft->invert == 0 || $timeleft->days <= 2;
					if ($urgent) {
						$str .= " <span class='timespan urgent'>";
					}
					else {
						$str .= " <span class='timespan'>(";
					}

					// Fill in with the time
					if ($timeleft->days > 0) {
						$str .= $timeleft->days . ' ' . get_string('days');
					}
					else {
						$str .= $timeleft->h . ' ' . get_string('hours') . ' ' . $timeleft->i . ' ' . get_string('minutes');
					}

					if ($timeleft->invert == 1) {
						$str .= " ".get_string('left', 'theme_monorail');
					}
					else {
						$str .= " ".get_string('late', 'theme_monorail');
					}

					if ($urgent) {
						$str .= "</span>";
					}
					else {
						$str .= ")</span>";
					}

					$str .= '</div>';
				} else {
					$str .= '<div class="task-due">'.$strduedateno.'</div>';
				}
			}
			$str .= '</div>';

            $htmlarray[$assignment->course][] = $str;
		}
    }
}
?>
