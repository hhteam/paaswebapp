<?php
/**
 * Monorail course overview
 * 
 * @package   monorail_course_overview
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2013011700;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012061700;        // Requires this Moodle version
$plugin->component = 'block_monorail_course_overview'; // Full name of the plugin (used for diagnostics)
