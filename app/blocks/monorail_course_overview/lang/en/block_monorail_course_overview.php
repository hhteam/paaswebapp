<?php
/**
 * Monorail course overview
 * 
 * @package   monorail_course_overview
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

$string['pluginname'] 			= 'Monorail course overview';

$string['completedcourses'] 	= 'Completed courses';
$string['ongoingcourses']		= 'Ongoing courses';

$string['settings_blocktype']	= 'Select type of Course Overview';

$string['overview']             = 'Overview';    
$string['tasks']                = 'Tasks';
$string['forum']                = 'Forum';
