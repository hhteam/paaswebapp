<?php
/**
 * Monorail course overview
 * 
 * @package   monorail_course_overview
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

class block_monorail_course_overview_edit_form extends block_edit_form {
	protected function specific_definition($mform) {

		// Section header title according to language file.
		$mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

		$options = array('ongoingcourses' => get_string('ongoingcourses', 'block_monorail_course_overview'),
						 'completedcourses' => get_string('completedcourses', 'block_monorail_course_overview'));

		// A sample string variable with a default value.
		$mform->addElement('select', 'config_type', get_string('settings_blocktype', 'block_monorail_course_overview'), $options);
		$mform->setDefault('config_type', 'ongoingcourses');
		$mform->setType('config_type', PARAM_ALPHA);

	}
}