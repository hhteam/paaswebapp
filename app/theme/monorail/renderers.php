<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once('define.php');

class theme_monorail_core_renderer extends core_renderer {
	
	private $_titleblock = '';			// title for the title block
	private $_tabbuttons = array();		// tab buttons collection
	private $_headertab	 = array();		// current header tab selection
	private $_returnlink = NULL;		// link of the back button

    static public $_editableCourses = array();

	public function __construct(moodle_page $page, $target) {
		global $CFG;

		if (strstr($_SERVER['PHP_SELF'], 'user/profile.php')) {
			$link = new moodle_url($CFG->wwwroot);
			$this->redirect($link);
		}

		if (strstr($_SERVER['PHP_SELF'], 'my/index.php')) {
			$link = new moodle_url($CFG->magic_ui_root);
			$this->redirect($link);
		}

		$this->set_header_tab();

		parent::__construct($page, $target);
	}

	function redirect($url, $message='', $delay=-1) {
		global $OUTPUT, $PAGE, $SESSION, $CFG;

		if (CLI_SCRIPT or AJAX_SCRIPT) {
			// this is wrong - developers should not use redirect in these scripts,
			// but it should not be very likely
			throw new moodle_exception('redirecterrordetected', 'error');
		}

		// prevent debug errors - make sure context is properly initialised
		if ($PAGE) {
			$PAGE->set_context(null);
			$PAGE->set_pagelayout('redirect');  // No header and footer needed
		}

		if ($url instanceof moodle_url) {
			$url = $url->out(false);
		}

		$debugdisableredirect = false;
		do {
			if (defined('DEBUGGING_PRINTED')) {
				// some debugging already printed, no need to look more
				$debugdisableredirect = true;
				break;
			}

			if (empty($CFG->debugdisplay) or empty($CFG->debug)) {
				// no errors should be displayed
				break;
			}

			if (!function_exists('error_get_last') or !$lasterror = error_get_last()) {
				break;
			}

			if (!($lasterror['type'] & $CFG->debug)) {
				//last error not interesting
				break;
			}

			// watch out here, @hidden() errors are returned from error_get_last() too
			if (headers_sent()) {
				//we already started printing something - that means errors likely printed
				$debugdisableredirect = true;
				break;
			}

			if (ob_get_level() and ob_get_contents()) {
				// there is something waiting to be printed, hopefully it is the errors,
				// but it might be some error hidden by @ too - such as the timezone mess from setup.php
				$debugdisableredirect = true;
				break;
			}
		} while (false);

		// Technically, HTTP/1.1 requires Location: header to contain the absolute path.
		// (In practice browsers accept relative paths - but still, might as well do it properly.)
		// This code turns relative into absolute.
		if (!preg_match('|^[a-z]+:|', $url)) {
			// Get host name http://www.wherever.com
			$hostpart = preg_replace('|^(.*?[^:/])/.*$|', '$1', $CFG->wwwroot);
			if (preg_match('|^/|', $url)) {
				// URLs beginning with / are relative to web server root so we just add them in
				$url = $hostpart.$url;
			} else {
				// URLs not beginning with / are relative to path of current script, so add that on.
				$url = $hostpart.preg_replace('|\?.*$|','',me()).'/../'.$url;
			}
			// Replace all ..s
			while (true) {
				$newurl = preg_replace('|/(?!\.\.)[^/]*/\.\./|', '/', $url);
				if ($newurl == $url) {
					break;
				}
				$url = $newurl;
			}
		}

		// Sanitise url - we can not rely on moodle_url or our URL cleaning
		// because they do not support all valid external URLs
		$url = preg_replace('/[\x00-\x1F\x7F]/', '', $url);
		$url = str_replace('"', '%22', $url);
		$encodedurl = preg_replace("/\&(?![a-zA-Z0-9#]{1,8};)/", "&amp;", $url);
		$encodedurl = preg_replace('/^.*href="([^"]*)".*$/', "\\1", clean_text('<a href="'.$encodedurl.'" />', FORMAT_HTML));
		$url = str_replace('&amp;', '&', $encodedurl);

		if (!empty($message)) {
			if ($delay === -1 || !is_numeric($delay)) {
				$delay = 3;
			}
			$message = clean_text($message);
		} else {
			$message = get_string('pageshouldredirect');
			$delay = 0;
		}

		if (defined('MDL_PERF') || (!empty($CFG->perfdebug) and $CFG->perfdebug > 7)) {
			if (defined('MDL_PERFTOLOG') && !function_exists('register_shutdown_function')) {
				$perf = get_performance_info();
				error_log("PERF: " . $perf['txt']);
			}
		}

		if ($delay == 0 && !$debugdisableredirect) {
			// workaround for IIS bug http://support.microsoft.com/kb/q176113/
			if (session_id()) {
				session_get_instance()->write_close();
			}

			//302 might not work for POST requests, 303 is ignored by obsolete clients.
			@header($_SERVER['SERVER_PROTOCOL'] . ' 303 See Other');
			@header('Location: '.$url);
			echo bootstrap_renderer::plain_redirect_message($encodedurl);
			exit;
		}

		// Include a redirect message, even with a HTTP redirect, because that is recommended practice.
		if ($PAGE) {
			$CFG->docroot = false; // to prevent the link to moodle docs from being displayed on redirect page.
			echo $OUTPUT->redirect_message($encodedurl, $message, $delay, $debugdisableredirect);
			exit;
		} else {
			echo bootstrap_renderer::early_redirect_message($encodedurl, $message, $delay);
			exit;
		}
	}

	public function header() {
		global $PAGE, $CFG;

		if ($PAGE->pagetype == 'login-forgot_password' || $PAGE->pagetype == 'login-change_password') {
			$PAGE->set_pagelayout('login');
		}
		if ($PAGE->pagetype == 'calendar-view') {
		    $PAGE->set_title('Eliademy: ' . get_string('calendar', 'theme_monorail'));
		}

		require_once ('lessc.inc.php');

		$less = new lessc;
		$less->addImportDir("$CFG->dirroot/theme/monorail/less/");

		if (!empty($PAGE->theme->sheets)) {
			foreach ($PAGE->theme->sheets as $sheet) {
				if (is_file("$CFG->dirroot/theme/monorail/less/$sheet.less")) {
					$less->checkedCompile("$CFG->dirroot/theme/monorail/less/$sheet.less", "$CFG->dirroot/theme/monorail/style/$sheet.css");
				}
			}
		}

		ob_start();

		return parent::header();
	}

	public function footer() {

		$str = ob_get_contents();
		ob_end_clean();

		echo $this->replace_url($str);

		return parent::footer();
	}

	private function replace_url($str) {
		// Replace Assignment URL
		$str = str_replace('/mod/assign/view.php?id=', '/theme/monorail/assign.php?id=', $str);
		$str = str_replace('/mod/book/view.php', '/theme/monorail/book.php', $str);
		return $str;
	}
	
	
	public function button_dropdown($name, $items, $attributes = NULL) {
		if (!isset($attributes)) {
			$attributes = array();
		}
		$attributes['class'] = 'btn-group';
		
		$output  = html_writer::start_tag('div', $attributes);
		
		$itemlist = '';
		foreach ($items as $item) {
			$itemlist .= html_writer::tag('li', $item);
		}
		
		$output .= <<<TXT
<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
	$name
	<span class="caret"></span>
</a>
<ul class="dropdown-menu">
$itemlist
</ul>
TXT;
		
		$output .= html_writer::end_tag('div');
		
		return $output;
	}

	protected function render_user_picture(user_picture $userpicture) {
		global $CFG, $DB;

        $user = $userpicture->user;

        if ($userpicture->alttext) {
            if (!empty($user->imagealt)) {
                $alt = $user->imagealt;
            } else {
                $alt = get_string('pictureof', '', fullname($user));
            }
        } else {
            $alt = '';
        }

        if (empty($userpicture->size)) {
            $size = 35;
        } else if ($userpicture->size === true or $userpicture->size == 1) {
            $size = 100;
        } else {
            $size = $userpicture->size;
        }

        $class = $userpicture->class;

        if ($user->picture == 0) {
            $class .= ' defaultuserpic';
        }

        require_once($CFG->dirroot . "/user/lib.php");
        $userarray  = user_get_user_details($user, null, array('profileimageurlsmall'));
        $src = $userarray["profileimageurlsmall"];

        $attributes = array('src'=>$src, 'alt'=>$alt, 'title'=>$alt, 'class'=>$class, 'width'=>$size, 'height'=>$size);

        // Teacher tag
        $strTeacher = get_string('defaultcourseteacher');
        $height		= intval($size/6);
        $pos		= $height + 2;
        $fontsize	= $height - 1;

        // Get userrole
        $context = get_context_instance(CONTEXT_COURSE, SITEID);
        $roles 	 = get_user_roles($context , $user->id);
		if (!empty($roles) && reset($roles)->name == $strTeacher) {
			$label = <<< EOD
	        <div style='display:block;width:{$size}px;height:{$height}px;
	        position:relative;top:-{$pos}px;z-index:100;background-color:#2676CB;
	        line-height:{$height}px;font-size:{$fontsize}px;color:white;text-align:center;margin-bottom:-{$height}px'>
	        	$strTeacher
	        </div>
EOD;
		}
		else {
			$label = '';
		}

        // get the image html output fisrt
        $output = html_writer::empty_tag('img', $attributes) . $label;

        $output = "<div class='userpic-container' style='width:{$size}px;height:{$size}px'>$output</div>";

        // then wrap it in link if needed
        if (!$userpicture->link) {
            return $output;
        }

        if (empty($userpicture->courseid)) {
            $courseid = $this->page->course->id;
        } else {
            $courseid = $userpicture->courseid;
        }

        /*
        if ($courseid == SITEID) {
            $url = new moodle_url('/theme/monorail/user.php', array('id' => $user->id));
        } else {
            $url = new moodle_url('/theme/monorail/user.php', array('id' => $user->id, 'course' => $courseid));
        }
        */

        $url = $CFG->magic_ui_root.'/profile/'.$user->id;

        $attributes = array('href'=>$url, 'class'=>'ignored_link', 'data-userid'=>$user->id);

        if ($userpicture->popup) {
            $id = html_writer::random_id('userpicture');
            $attributes['id'] = $id;
            $this->add_action_handler(new popup_action('click', $url), $id);
        }

        return html_writer::tag('a', $output, $attributes);
	}
    
    public function set_title_block($titleblock) {
    	$this->_titleblock = $titleblock;
    }
    
    public function get_title_block() {
    	return $this->_titleblock;
    }
    
    public function set_header_tab($header_selected = NULL) {
    	for ($i = 0; $i < NUMBER_OF_HEADER_TABS; $i++) {
    		if ($i === $header_selected)
    			$this->_headertab[$i] = 'select';
    		else 
    			$this->_headertab[$i] = '';
    	}
    }
    
    public function get_header_tab() {
    	return $this->_headertab;
    }
    
    public function set_tab_buttons($tabbuttons) {
    	$this->_tabbuttons = $tabbuttons;
    }
    
    public function get_tab_buttons() {
    	return $this->_tabbuttons;
    }
    
    public function set_return_link($returnlink) {
    	$this->_returnlink = $returnlink;    	
    }
    
    public function get_return_link() {
    	return $this->_returnlink;
    }
}

include_once($CFG->dirroot . "/calendar/renderer.php");

/* Calendar renderer.
 */
class theme_monorail_core_calendar_renderer extends core_calendar_renderer
{
    public function show_day(calendar_information $calendar, moodle_url $returnurl = null)
	{
		global $USER;
		global $CFG;
        global $DB;

        $all_courses = $DB->get_records('course');
        theme_monorail_core_renderer::$_editableCourses = array(array("id" => null, "fullname" => get_string('personal_event', 'theme_monorail')));

        foreach ($all_courses as $course)
        {
            $context = context_course::instance($course->id);
        
            try
            {
                require_capability('moodle/course:manageactivities', $context);
                theme_monorail_core_renderer::$_editableCourses[] = array("id" => $course->id, "fullname" => $course->fullname);
            }
            catch (Exception $e) { }
        }

        if ($returnurl === null) {
            $returnurl = $this->page->url;
        }

        // make sure timezone is correctly set
        $user = $DB->get_record('user', array('id'=>$USER->id));
        if (isset($user->timezone)) {
            $USER->timezone = $user->timezone;
        }

        $calendar->checkdate();
		$output = "";

		$display = $this->make_display($calendar);
		$showstamp = mktime(0, 0, 0, $calendar->month, $calendar->day, $calendar->year);
		$todaystamp = (int) (time() / 86400) * 86400;

        // Making header
        // -------------

		$nextWeek = getdate($showstamp + 604800);
		$prevWeek = getdate($showstamp - 604800);
		$prevLink = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', "subview" => isset($_GET["subview"]) ? $_GET["subview"] : "", 'course'=>$calendar->courseid)), $prevWeek["mday"], $prevWeek["mon"], $prevWeek["year"]);
		$nextLink = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', "subview" => isset($_GET["subview"]) ? $_GET["subview"] : "", 'course'=>$calendar->courseid)), $nextWeek["mday"], $nextWeek["mon"], $nextWeek["year"]);
                $exportCalLink = $CFG->magic_ui_root.'/calexport';
        $output .= "<div class=\"title-block\"><div>" .
                   "<a href=\"$prevLink\" class=\"cal_prev_icon icon\" id=\"calendar_prev_week\"></a>" .
                   "<a href=\"javascript:void(0)\" id=\"cal_week_header\" data-weeknum=\"" . (int) date("W", $showstamp) . "\">" . get_string("week") . " " . (int) date("W", $showstamp) . " " .get_string(date("F", $showstamp),"theme_monorail") ." ".date("Y", $showstamp) . "</a>" .
                   "<a href=\"$nextLink\" class=\"cal_next_icon icon\" id=\"calendar_next_week\"></a>" .
                   "<a href=\"javascript:void(0)\" class=\"cal_new_icon icon pull-right\" id=\"calendar_event_add_btn\"></a>" .
                   "</div>".
                   "<a href=\"$exportCalLink\" class=\"pull-right\" style=\"margin-top: 10px;\">".get_string("calexport",'theme_monorail')."</a>".
                   "</div>";

		// Starting table...
        $table = new html_table();
        $table->attributes = array('class'=>'calendarmonth calendartable cal_day_view', "style" => "width: 100%;");

		// Making weekday row
        $wdays = array(0 => get_string('sun', 'calendar'),
                       1 => get_string('mon', 'calendar'),
                       2 => get_string('tue', 'calendar'),
                       3 => get_string('wed', 'calendar'),
                       4 => get_string('thu', 'calendar'),
                       5 => get_string('fri', 'calendar'),
                       6 => get_string('sat', 'calendar'));

        $wfdays = array(0 => get_string('sunday', 'calendar'),
                       1 => get_string('monday', 'calendar'),
                       2 => get_string('tuesday', 'calendar'),
                       3 => get_string('wednesday', 'calendar'),
                       4 => get_string('thursday', 'calendar'),
                       5 => get_string('friday', 'calendar'),
                       6 => get_string('saturday', 'calendar'));

        // Loop only if the day offset is greater than 0.
        // This loop involves shifting the days around until the desired start day
        // is at the start of the array.
        $daycount = 0;

        while ($display->minwday > $daycount++)
		{
            $wdays_end = array_shift($wdays);
            array_push($wdays, $wdays_end);

            $wdays_end = array_shift($wfdays);
            array_push($wfdays, $wdays_end);
        }

		$wdRow = new html_table_row();
		$wdRow->style = "height: auto; background-color: #F0EFEF;";

		$firststamp = $showstamp;

		while (date("N", $firststamp) != ($daycount == 1 ? 7 : $daycount - 1))
		{
			$firststamp -= 86400;
		}

		$emptyCell = new html_table_cell();
		$emptyCell->style = "width: auto; height: auto;";
        $emptyCell->attributes = array("class" => "bord");
		$wdRow->cells[] = $emptyCell;

		$currentstamp = $firststamp;

		for ($i=0; $i<7; $i++)
		{
			$wdCell = new html_table_cell();

            $dayhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', 'course'=>$calendar->courseid)),
				date("j", $currentstamp), date("n", $currentstamp), date("Y", $currentstamp));

			$wdCell->style = "text-align: center; vertical-align: middle; height: 43px;";
			$wdCell->attributes = array("class" => "bord");

			if (!isset($_GET["subview"]) || $_GET["subview"] != "week")
			{
				if ($showstamp == $currentstamp)
				{
					$wdCell->style .= "width: 50%;";
					$wdayname = $wfdays[$i];

					$dayhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', "subview"=>"week", 'course'=>$calendar->courseid)),
						date("j", $currentstamp), date("n", $currentstamp), date("Y", $currentstamp));
				}
				else
				{
					$wdCell->style .= "width: 8%;";
					$wdayname = $wdays[$i];
				}
			}
			else
			{
				$wdCell->style .= "width: 14%;";
				$wdayname = $wdays[$i];
			}

			$wdCell->text = "<a href=\"" . $dayhref . "\" id=\"".$currentstamp."\" class=\"fill_cell\"><span class=\"cl6 fn2\">" . date("d", $currentstamp) . " " . $wdayname . "</span></a>";

			$wdRow->cells[] = $wdCell;

			$currentstamp += 86400;
		}

		$table->data[] = $wdRow;

		// Making event rows...
		// a) - pulling data
		$eventData = array();

		for ($i=0; $i<7; $i++)
		{
			$col = array();

			for ($j=0; $j<146; $j++)
			{
				$col[] = array();
			}

			$eventData[] = $col;
		}

		$currentstamp = $firststamp;

		for ($i=0; $i<7; $i++)
		{
        	$events = calendar_get_upcoming($calendar->courses, $calendar->groups, $calendar->users, 1, 100, usertime($currentstamp));

			foreach ($events as $ev)
			{
                $event = new calendar_event($ev);
                $event->calendarcourseid = $calendar->courseid;

                $evDate = usergetdate($event->timestart);
                $colDate = getdate($currentstamp);

				if ($evDate["yday"] != $colDate["yday"] || $evDate["hours"] < 7)
				{
					// Event starts not today, or before 07:00, or after 18:00
					// - we show it on the first line.

					$eventData[$i][0][] = array("e" => $event, "t" => 1);
				}
                else if ($evDate["hours"] > 18)
                {
                    // Event starts after 19:00

					$eventData[$i][145][] = array("e" => $event, "t" => 1);
                }
				else
				{
					// Event starts today, after 07:00, and before 18:00

					$j = $k = ((int) (($evDate["hours"] - 7) * 12)) + ((int) ($evDate["minutes"] / 5)) + 1;
					$d = $k + ((int) ($event->timeduration - 1) / 300);

					for ($j++; $j < 145 && $j < $d; $j++)
					{
						$eventData[$i][$j][] = array("e" => $event, "t" => 3);
					}

					$eventData[$i][$k][] = array("e" => $event, "t" => 2, "s" => $j - $k);
				}
			}

			$currentstamp += 86400;
		}

		// b) - making rows
		$evRows = array();

		for ($i=0; $i<146; $i++)
		{
			$row = new html_table_row();

            $row->style = "height: 0.5em;";

			$emptyCell = new html_table_cell();
			$emptyCell->attributes = array("class" => "hours");

			if (!$i)
			{
				$emptyCell->text = "00h<br>I<br>06h";
				$emptyCell->style = "height: 6em;";

			    $row->cells[] = $emptyCell;
			}
			else if ($i == 145)
			{
				$emptyCell->text = "19h<br>I<br>24h";
				$emptyCell->style = "height: 6em;";

			    $row->cells[] = $emptyCell;
			}
			else if ($i % 12 == 1)
			{
				$emptyCell->text = sprintf("%02dh", ($i / 12) + 7);
				$emptyCell->style = "height: auto; vertical-align: top;";
                $emptyCell->rowspan = 12;

			    $row->cells[] = $emptyCell;
			}

			$currentstamp = $firststamp;

			for ($j=0; $j<7; $j++)
			{
				$cell = new html_table_cell();
				$cell->style = "width: auto; height: auto;";
				$classes = "fn4 pad2 cal_day_cell";

                $showFullDay = (!isset($_GET["subview"]) || $_GET["subview"] != "week") && $showstamp == $currentstamp;

                $idstring = date("Y", $currentstamp).date("n", $currentstamp).date("j", $currentstamp).$i;

                $classes .= " bord";

				if (empty($eventData[$j][$i]))
				{
                    if ($this->is_today($currentstamp))
                    {
                        $classes .= " empty_line_today";
                    }
                    else
                    {
                        $classes .= " empty_line";
                    }

					$cell->attributes = array
					(
						"class" => $classes,
						"data-timestamp" => $currentstamp + ($i ? ($i * 300 + 21600) : 0),
						"data-laststamp" => $currentstamp + ($i ? ($i * 300 + 21900) : 21600),
						"data-daynum" => $i
					);

                    $cell->id = $idstring;

                    if (!$i || $i == 145)
                    {
    			        $row->cells[] = $cell;
                    }
                    else if ($i % 12 == 1)
                    {
                        $emCnt = 1;

                        while ($emCnt < 12 && empty($eventData[$j][$i + $emCnt]))
                        {
                            $emCnt++;
                        }

                        $cell->rowspan = $emCnt;
    			        $row->cells[] = $cell;
                    }
                    else if ($i % 12 != 0 && !empty($eventData[$j][$i - 1]))
                    {
                        $emCnt = 0;

                        while ((($i + $emCnt) % 12 != 0) && empty($eventData[$j][$i + $emCnt]))
                        {
                            $emCnt++;
                        }

                        if (($i + $emCnt) % 12 == 0)
                        {
                            $emCnt++;
                        }

                        $cell->rowspan = $emCnt;

                        $row->cells[] = $cell;
                    }
				}
				else
				{
					if (!$i || $i == 145)
					{
						$classes .= " morning_evening";

						foreach ($eventData[$j][$i] as $event)
						{
							$cell->text .= "<a name=\"event_" . $event["e"]->id . "\">";

                            $cell->text .= $this->make_event_link($event["e"],
                                userdate($event["e"]->timestart, "%H:%M") . " " . $this->trim_line($event["e"]->name, 10),
                                "cl" . ($event["e"]->eventtype == "user" ? "3" : "1") . " fn6 d_block", "e" . $event["e"]->id);

							$cell->text .= "</a>";
						}

						$cell->attributes = array
						(
							"class" => $classes,
							"data-timestamp" => $currentstamp,
							"data-laststamp" => $currentstamp + 21600,
							"data-daynum" => "0"
						);

                        $cell->id = $idstring;
						$row->cells[] = $cell;
					}
					else
					{
                        $si = 0;
                        $rs = 1;
						$text = "";
                        $evClass = "";

                        $intEv = array();

                        while ($si < $rs)
                        {
                            foreach ($eventData[$j][$i + $si] as $key => $event)
                            {
                                if ($event["t"] == 2)
                                {
                                    $intEv[] = $event;
                                    $eventData[$j][$i + $si][$key]["t"] = 5;

                                    if ($rs < $event["s"] + $si)
                                    {
                                        $rs = $event["s"] + $si;
                                    }
                                }
                            }

                            $si++;
                        }

						foreach ($intEv as $event)
						{
                            if (!empty($text))
                            {
                                $text .= "<br />";
                            }

                            $text .= "<a name=\"event_" . $event["e"]->id . "\">";

                            $linkText = "<span class=\"fn6\">" . userdate($event["e"]->timestart, "%H:%M") .
                                " - " . userdate($event["e"]->timestart + $event["e"]->timeduration, "%H:%M") . "</span><br />";

                            if ($showFullDay)
                            {
                                $linkText .= "<span class=\"fn6\">" . strip_tags($event["e"]->name) . "</span>";
                            }
                            else
                            {
                                $linkText .= "<span class=\"fn6\">" . $this->trim_line(strip_tags($event["e"]->name), 30) . "</span>";
                            }

                            if ($event["e"]->eventtype == "course")
                            {
                                $location = $event["e"]->description;

                                if (!$showFullDay)
                                {
                                    $brPos = strpos($location, "<br");

                                    if ($brPos !== FALSE)
                                    {
                                        $location = substr($location, 0, $brPos);
                                    }
                                }

                                $linkText .= "<br/><span class=\"fn3\">" . strip_tags($location, "<br>") . "</span>";
                            }

                            if ($event["e"]->eventtype == "user")
                            {
                                if (empty($evClass))
                                {
                                    $evClass = " event_green";
                                }
                            }
                            else
                            {
                                $evClass = " event_blue";
                            }

                            $text .= $this->make_event_link($event["e"], $linkText, "", "e" . $event["e"]->id);

                            $text .= "</a>";
						}

						if (!empty($text))
						{
							$classes .= $evClass;

							$cell->attributes = array
							(
								"class" => $classes,
								"data-timestamp" => $currentstamp + ($i * 300 + 21600),
								"data-laststamp" => $currentstamp + ($i * 300 + 21900),
								"data-daynum" => $i
							);

                            $cell->id = $idstring;
							$cell->text = $text;
                            $cell->rowspan = $rs;

							$row->cells[] = $cell;
						}
					}
				}

				$currentstamp += 86400;
			}

			$evRows[] = $row;
		}

		foreach ($evRows as $row)
		{
			$table->data[] = $row;
		}

		// Flushing table
        $output .= html_writer::table($table);

		$output .= "<script type=\"text/javascript\">M.cfg.userid=" . $USER->id . "</script>";
		$output .= "<script type=\"text/javascript\">M.cfg.timezoneOffset=" . (get_user_timezone_offset() * 3600) . "</script>";
		$output .= "<script type=\"text/javascript\">M.cfg.writable_courses=" . json_encode(theme_monorail_core_renderer::$_editableCourses) . ";</script>";
		$output .= "<script type=\"text/javascript\">M.cfg.strings=" . json_encode(array(
            'button_save' => get_string('button_save', "theme_monorail"),
            'button_cancel' => get_string('button_cancel', "theme_monorail"),
            'button_delete' => get_string('button_delete', "theme_monorail"),
            'click_cancel' =>  get_string('click_cancel', "theme_monorail"),
            'select_time' =>  get_string('select_time', "theme_monorail"),
            'end_time' =>  get_string('end_time', "theme_monorail"),
            'start_time' =>  get_string('start_time', "theme_monorail"),
            'title' =>  get_string('title', "theme_monorail"),
            'location' =>  get_string('location', "theme_monorail"),
            'showmonth' =>  get_string('showmonth', "theme_monorail"),
            'showtoday' =>  get_string('showtoday', "theme_monorail"),
            'cal_go' =>  get_string('cal_go', "theme_monorail"),
            'event_delete' => get_string('event_delete', "theme_monorail")
        )) . ";</script>";

        return $output;
    }

    public function show_month_detailed(calendar_information $calendar, moodle_url $returnurl  = null)
    {
		global $USER;
        global $DB;

        $all_courses = $DB->get_records('course');
        theme_monorail_core_renderer::$_editableCourses = array(array("id" => null, "fullname" => get_string('personal_event', 'theme_monorail')));

        foreach ($all_courses as $course)
        {
            $context = context_course::instance($course->id);
        
            try
            {
                require_capability('moodle/course:manageactivities', $context);
                theme_monorail_core_renderer::$_editableCourses[] = array("id" => $course->id, "fullname" => $course->fullname);
            }
            catch (Exception $e) { }
        }
    	global $CFG;

    	if (empty($returnurl)) {
    		$returnurl = $this->page->url;
    	}

    	$calendar->checkdate();
    	$display = $this->make_display($calendar);

    	// Get events from database
    	$events = calendar_get_events(usertime($display->tstart), usertime($display->tend), $calendar->users, $calendar->groups, $calendar->courses);
    	if (!empty($events)) {
    		foreach($events as $eventid => $event) {
    			$event = new calendar_event($event);
    			if (!empty($event->modulename)) {
    				$cm = get_coursemodule_from_instance($event->modulename, $event->instance);
    				if (!groups_course_module_visible($cm)) {
    					unset($events[$eventid]);
    				}
    			}
    		}
    	}

    	// Extract information: events vs. time
    	calendar_events_by_day($events, $calendar->month, $calendar->year, $eventsbyday, $durationbyday, $typesbyday, $calendar->courses);
    	$output='';

    	$days = calendar_get_days();

    	$showstamp = mktime(0, 0, 0, $calendar->month, $calendar->day, $calendar->year);

        // Making header
        // -------------

    	$prevMonth = getdate($showstamp);
        $prevMonth["mday"] = 1;

        if (!--$prevMonth["mon"])
        {
            $prevMonth["mon"] = 12;
            $prevMonth["year"]--;
        }

    	$nextMonth = getdate($showstamp);
        $nextMonth["mday"] = 1;

        if (++$nextMonth["mon"] > 12)
        {
            $nextMonth["mon"] = 1;
            $nextMonth["year"]++;
        }

    	$prevLink = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'month', "subview" => isset($_GET["subview"]) ? $_GET["subview"] : "", 'course'=>$calendar->courseid)), $prevMonth["mday"], $prevMonth["mon"], $prevMonth["year"]);
    	$nextLink = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'month', "subview" => isset($_GET["subview"]) ? $_GET["subview"] : "", 'course'=>$calendar->courseid)), $nextMonth["mday"], $nextMonth["mon"], $nextMonth["year"]);
    	$monhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'month', 'course'=>$calendar->courseid)), $calendar->day, $calendar->month, $calendar->year);

        $output .= "<div class=\"title-block\"><div>" .
                   "<a href=\"$prevLink\" class=\"cal_prev_icon icon\" id=\"calendar_prev_week\"></a>" .
                   "<a href=\"javascript:void(0)\" id=\"cal_date_header\" data-href=\"" . $monhref . "\" >" .get_string(date("F", $showstamp),"theme_monorail") ." ".date("Y", $showstamp) . "</a>" .
                   "<a href=\"$nextLink\" class=\"cal_next_icon icon\" id=\"calendar_next_week\"></a>" .
                   "<a href=\"javascript:void(0)\" class=\"cal_new_icon icon pull-right\" id=\"calendar_event_add_btn\"></a>" .
                   "</div></div>";

    	$table = new html_table();
    	$table->attributes = array('class'=>'calendartable cal_mon_view');

    	$firststamp = mktime(0, 0, 0, $calendar->month, 1, $calendar->year);

        while (date("w", $firststamp) != $display->minwday)
        {
            $firststamp -= 86400;
        }

    	$curstamp = $firststamp;

    	// Now display all the calendar
    	$weekend = CALENDAR_DEFAULT_WEEKEND;
    	if (isset($CFG->calendar_weekend)) {
    		$weekend = intval($CFG->calendar_weekend);
    	}

    	// For the table display. $week is the row; $dayweek is the column.
    	$week = 1;
    	$dayweek = 0;

    	$row = new html_table_row(array());
    	$row->attributes=array("class"=>"bord2");

    	// Paddding (the first week may have blank days in the beginning)
    	while (!$this->same_month($curstamp, $showstamp) && $dayweek < 7)
        {
    		$dayhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', 'course'=>$calendar->courseid)),
    				date("j", $curstamp), date("n", $curstamp), date("Y", $curstamp));

    		$cell = new html_table_cell();

    		$cellclasses=array("bord2");

            if ($this->is_today($curstamp))
            {
                // It's really, really today :-)
    			$cellclasses[] = 'today_today';
            }
    		else
            {
                // It's just a today...
    			$cellclasses[] = 'today';
    		}

    		$cell->style="height:92px";

            $ldaymon = date("j",$curstamp)." ".get_string(strtolower(date("D",$curstamp)), 'calendar'); 

    		$cell->text = html_writer::tag('div',html_writer::link($dayhref,$ldaymon),array('class'=>'dayprevmon'));

    		$cell->attributes = array('class'=>join(' ',$cellclasses));
    		$row->cells[] = $cell;

            $curstamp += 86400;
            $dayweek++;
    	}

    	//$dayhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day','subview'=>'week',  'course'=>$calendar->courseid)), $calendar->day, $calendar->month, $calendar->year);
    	for ($calendar->day = 1; $calendar->day <= $display->maxdays; ++$calendar->day)
    	{
    		if ($dayweek >= 7)
    		{
    			$table->data[] = $this->make_week_number_row($calendar);

    			$table->data[] = $row;
    			$row = new html_table_row(array());
    			$dayweek = $display->minwday;
    			++$week;
                $dayweek = 0;
    		}

    		// Reset vars
    		$cell=new html_table_cell();

    		$dayhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', 'course'=>$calendar->courseid)),
    				date("j", $curstamp), date("n", $curstamp), date("Y", $curstamp));

    		$cellclasses = array("cal_day_cell", "bord2");

            if ($this->is_today($curstamp))
            {
    			$cellclasses[] = 'today_today';
            }
    		else if ($weekend & (1 << ($dayweek % 7)))
            {
    			// Weekend. This is true no matter what the exact range is.
    			$cellclasses[] = 'weekend';
    		}
            else
            {
    			$cellclasses[] = 'today';
            }

    		// Special visual fx if an event is defined
                $ldaymon = date("j",$curstamp)." ".get_string(strtolower(date("D",$curstamp)), 'calendar'); 
    		if (isset($eventsbyday[$calendar->day])) {
    			if(count($eventsbyday[$calendar->day]) == 1) {
    				$title = get_string('oneevent', 'calendar');
    			} else {
    				$title = get_string('manyevents', 'calendar', count($eventsbyday[$calendar->day]));
    			}

    			$cell->text = html_writer::tag('div', html_writer::link($dayhref,$ldaymon,array('title'=>$title)), array('class'=>'daymon'));

    		} else {

    			$cell->text = html_writer::tag('div',html_writer::link($dayhref,$ldaymon),array('class'=>'daymon'));
    		}

    		// Special visual fx if an event spans many days
    		$durationclass = false;
    		if (isset($typesbyday[$calendar->day]['durationglobal'])) {
    			$durationclass = 'duration_global';
    		} else if (isset($typesbyday[$calendar->day]['durationcourse'])) {
    			$durationclass = 'duration_course';
    		} else if (isset($typesbyday[$calendar->day]['durationgroup'])) {
    			$durationclass = 'duration_group';
    		} else if (isset($typesbyday[$calendar->day]['durationuser'])) {
    			$durationclass = 'duration_user';
    		}
    		if ($durationclass) {
    			$cellclasses[] = $durationclass;
    		}

    		$cell->attributes = array
			(
				'class'=>join(' ',$cellclasses),
				'data-timestamp' => $curstamp,
				'data-laststamp' => $curstamp + 86400
			);

    		$cell->style="height:92px";

    		if (isset($eventsbyday[$calendar->day])) {

    			$cell->text .= html_writer::start_tag('ul', array('class'=>'events-new'));
    			foreach($eventsbyday[$calendar->day] as $eventindex) {
    				// If event has a class set then add it to the event <li> tag
    				$attributes = array();
    				if (!empty($events[$eventindex]->class)) {
    					$attributes['class'] = $events[$eventindex]->class;
    				}
                    $attributes["id"] = "e" . $events[$eventindex]->id;
    				$cell->text .= html_writer::tag('li', $this->make_event_link($events[$eventindex],
                        userdate($events[$eventindex]->timestart, "%H:%M") . " " . $this->trim_line(strip_tags($events[$eventindex]->name), 10),
                        "fn6 cl" . ($events[$eventindex]->eventtype == "user" ? "3" : "1"), ""), $attributes);
    			}
    			$cell->text .= html_writer::end_tag('ul');
    		}
    		if (isset($durationbyday[$calendar->day])) {
    			$cell->text .= html_writer::start_tag('ul', array('class'=>'events-underway'));
    			foreach($durationbyday[$calendar->day] as $eventindex) {
    				$cell->text .= html_writer::tag('li', format_string($events[$eventindex]->name,true), array('class'=>'events-underway'));
    			}
    			$cell->text .= html_writer::end_tag('ul');
    		}

    		$row->cells[] = $cell;

    		$curstamp += 86400;
            $dayweek++;
    	}

    	$blankcurstamp=$curstamp;

    	// Paddding (the last week may have blank days at the end)
    	while ($dayweek < 7)
        {
    		$dayhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', 'course'=>$calendar->courseid)),
    				date("j", $blankcurstamp), date("n", $blankcurstamp), date("Y", $blankcurstamp));

    		$cell = new html_table_cell('&nbsp;');

    		$cellclasses = array("bord2");

            if ($this->is_today($blankcurstamp))
            {
    			$cellclasses[]= 'today_today';
            }
            else
            {
    			$cellclasses[]= 'today';
            }

    		$cell->attributes = array('class'=>join(' ',$cellclasses));
    		$cell->style="height:92px";
                $ldaymon = date("j",$blankcurstamp)." ".get_string(strtolower(date("D",$blankcurstamp)), 'calendar');
    		$cell->text = html_writer::tag('div',html_writer::link($dayhref,$ldaymon),array('class'=>'dayprevmon'));

    		$row->cells[] = $cell;

    		$blankcurstamp += 86400;
            $dayweek++;
    	}

    	$table->data[] = $this->make_week_number_row($calendar);
    	$table->data[] = $row;

    	$output .= html_writer::table($table);

		$output .= "<script type=\"text/javascript\">M.cfg.userid=" . $USER->id . "</script>";
		$output .= "<script type=\"text/javascript\">M.cfg.timezoneOffset=" . (get_user_timezone_offset() * 3600) . "</script>";
		$output .= "<script type=\"text/javascript\">M.cfg.writable_courses=" . json_encode(theme_monorail_core_renderer::$_editableCourses) . ";</script>";
		$output .= "<script type=\"text/javascript\">M.cfg.strings=" . json_encode(array(
            'button_save2' => get_string('button_save2', "theme_monorail"),
            'button_save' => get_string('button_save', "theme_monorail"),
            'button_cancel' => get_string('button_cancel', "theme_monorail"),
            'button_delete' => get_string('button_delete', "theme_monorail"),
            'click_cancel' =>  get_string('click_cancel', "theme_monorail"),
            'select_time' =>  get_string('select_time', "theme_monorail"),
            'end_time' =>  get_string('end_time', "theme_monorail"),
            'start_time' =>  get_string('start_time', "theme_monorail"),
            'title' =>  get_string('title', "theme_monorail"),
            'location' =>  get_string('location', "theme_monorail"),
            'event_delete' => get_string('event_delete', "theme_monorail")
        )) . ";</script>";

    	return $output;
    }

    //Displaying event detail will not work now untill dayview has a link to event detail

    	public function event(calendar_event $event, $showactions=true) {
    		global $DB,$CFG;

    		$output='';
    		$event = calendar_add_event_metadata($event);

    		$anchor  = html_writer::tag('a', '', array('name'=>'event_'.$event->id));

    		$table = new html_table();
    		$table->attributes = array('class'=>'calendarmonth calendartable');

    		//making header for monthview
    		$headerRow = new html_table_row();
    		$headerRow->style = "height: auto";

    		$headerCell = new html_table_cell();
    		$headerCell->colspan = 7;
    		$headerCell->attributes=array("class"=>"empty_line");



    		$headerCell->style = "text-align: left; height: auto;";


    		$headerCell->text="<div class=\"cal_button\" style=\"margin-right: 20px;\"><span class=\"icon_back\"id=\"back_btn\">&nbsp;</span></div>";





    		if (!empty($event->referer)) {
    			$headerCell->text .= "<a href=\"" ."\" class=\"fn5\" style=\"line-height: 56px;font-size:22pt;color:#2676CB;\"><span class=\"cl1\">" .$event->referer."</span></a>";

    		} else if (!empty($event->courselink)) {
    			$headerCell->text .=html_writer::tag('span', $event->courselink, array('class'=>'CourseName'));
    		}
    		/*if (!empty($event->courselink)) {
    			$headerCell->text .= "<a href=\"" ."\" class=\"fn5\" style=\"line-height: 56px;font-size:22pt;color:#2676CB;\"><span class=\"cl1\">" .$event->courselink."</span></a>";

    		}*/


    		$headerRow->cells[] = $headerCell;
    		$table->data[] = $headerRow;
    		$output .=html_writer::table($table);



    		$newTable=new html_table();
    		$newTable->style="height:100%";
    		$newTable->attributes = array('class'=>'calendarmonth calendartable');
    		$blankrow=new html_table_row();
    		$blankrow->style="height:20px";
    		$blankrow->style="color:#D2DBE1";

    		$newTable->data[]=$blankrow;


    		$row=new html_table_row();
    		$row->style="height:580px;";

    		$rowCell= new html_table_cell();
    		$rowCell->style="padding-left:2em";



    		//adding event timing
    		if (!empty($event->time)) {
    			$rowCell->text = html_writer::start_tag('div');

    			$rowCell->text .= html_writer::tag('span',userdate($event->timestart, '%A  %d.%m.%Y|  '),array('class'=>'date'),calendar_time_representation($event->timestart/* + get_user_timezone_offset()*3600*/),array('class'=>'date'));
    			$rowCell->text .= html_writer::tag('span',calendar_time_representation($event->timestart + get_user_timezone_offset()*3600),array('class'=>'date'));
    			$rowCell->text .= html_writer::tag('span',' - ');

    			$rowCell->text .= html_writer::tag('span', calendar_time_representation($event->timestart+$event->timeduration + get_user_timezone_offset()*3600),array('class'=>'date'));
    			$rowCell->text .= html_writer::end_tag('div');


    		} else {
    			$rowCell->text .= html_writer::tag('div', calendar_time_representation($event->timestart + get_user_timezone_offset()*3600),array('class'=>'date'));

    		}
    		$rowCell->attributes=array("class"=>"fabric");


    		// $course = get_string('course',$calender->courseid);
    		//add event description
    		//$rowCell->text .= html_writer::tag('div', $event->description, array('class'=>'description'));

    		//making link to course box
    		if (!empty($event->courselink)) {
    			//$courselink="<a href="".$CFG->wwwroot.'/course/view.php?id='.$event->courseid."">'."</a>;
    			//$rowCell->text. ='<a href="'.$CFG->wwwroot.'/course/view.php?id='.$event->courseid.'">'.Course Overview.'</a>';

    			$rowCell->text .= html_writer::tag('div',$event->courselink, array('class'=>'course'),'Course Overview',array('class'=>'course'));


    		}
    		else{
    			$rowCell->text .= html_writer::tag('div','Course Overview', array('class'=>'course'));

    		}


    		if (isset($event->cssclass)) {
    			$rowCell->attributes['class'] .= ' '.$event->cssclass;
    		}
    		$row->cells[]=$rowCell;
    		$newTable->data[] = $row;


    		$output .=html_writer::table($newTable);



    		return $output;

    	}

	/* ----------------------------------------------------------------- */

	private function make_week_number_row(calendar_information $calendar)
	{
		$title = get_string("week") . " " . (int) date("W", mktime(0, 0, 0, $calendar->month, $calendar->day - 1, $calendar->year));
		$weekhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view'=>'day', 'subview'=>'week', 'course'=>$calendar->courseid)), $calendar->day - 1, $calendar->month, $calendar->year);

		$weekRow = new html_table_row(array());
		$weekCell = new html_table_cell("<a href=\"" . $weekhref . "\" class=\"fill_cell\">" . $title . "</a>");

		$weekCell->colspan = 7;
		$weekCell->attributes["class"] = "week_info_mon";
		$weekCell->style = "height: 42px;margin-left:5px;";


		$weekRow->cells[] = $weekCell;

		return $weekRow;
	}

	private function make_display(calendar_information $calendar)
	{
        $date = usergetdate(time());

        $display = new stdClass;
        $display->minwday = get_user_preferences('calendar_startwday', calendar_get_starting_weekday());
        $display->maxwday = $display->minwday + 6;
        $display->thismonth = ($date['mon'] == $calendar->month);
        $display->maxdays = calendar_days_in_month($calendar->month, $calendar->year);

        $useroffset = get_user_timezone_offset();
        if ($useroffset < 99) {
            // We 'll keep these values as GMT here, and offset them when the time comes to query the db
            // $display->tstart = gmmktime(0, 0, 0, $calendar->month, 1, $calendar->year); // This is GMT
            // $display->tend = gmmktime(23, 59, 59, $calendar->month, $display->maxdays, $calendar->year); // GMT
            $display->tstart = mktime(0, 0, 0, $calendar->month, 1, $calendar->year) + $useroffset*3600;
            $display->tend = mktime(23, 59, 59, $calendar->month, $display->maxdays, $calendar->year) + $useroffset*3600;
        } else {
            // no timezone info specified
            $display->tstart = mktime(0, 0, 0, $calendar->month, 1, $calendar->year);
            $display->tend = mktime(23, 59, 59, $calendar->month, $display->maxdays, $calendar->year);
        }

		return $display;
	}

	private function make_event_link($event, $name, $class = "", $id = "")
	{
        global $CFG, $DB;

		$text = "";

		if ($event->eventtype == "user")
		{
			// Popup

			$text .= "<a href=\"#\" class=\"" . $class . " cal_event_item\"";

            if (!empty($id))
            {
                $text .= " id=\"" . $id . "\"";
            }

            $text .= " data-name=\""
                . $event->name . "\" data-start=\"" . userdate($event->timestart, "%FT%T") . "\" data-duration=\""
                . $event->timeduration . "\" data-id=\"" . $event->id . "\" data-location=\""
                . $event->description . "\" data-type=\"user\">" . $name . "</a>";
		}
		else
		{
			if ($event->eventtype == "course")
			{
				// It's a course

                $can_edit = false;

                foreach (theme_monorail_core_renderer::$_editableCourses as $edcourse)
                {
                    if ($edcourse["id"] == $event->courseid)
                    {
                        $can_edit = true;
                        break;
                    }
                }

                if ($can_edit)
                {
                    $text .= "<a href=\"#\" class=\"" . $class . " cal_event_item\"";

                    if (!empty($id))
                    {
                        $text .= " id=\"" . $id . "\"";
                    }

                    $text .= " data-name=\""
                        . $event->name . "\" data-start=\"" . userdate($event->timestart, "%FT%T") . "\" data-duration=\""
                        . $event->timeduration . "\" data-id=\"" . $event->id . "\" data-location=\""
                        . $event->description . "\" data-type=\"course\">" . $name . "</a>";

                    return $text;
                }
                else
                {
                    $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$event->courseid));
				    $eventLink = $CFG->magic_ui_root . "/courses/" . $courseData->code;
                }
			}
			else
			{
				// Building link from module name...

                global $DB;

                // XXX: this method of deduction of event id must be tested carefully!
                $cms = $DB->get_records_sql("select cm.id as cmid from {course_modules} as cm, {modules} as mods"
                    . " where cm.course=:course and cm.instance=:instance and cm.module=mods.id and mods.name=:module",
                    array("course" => $event->courseid, "instance" => $event->instance, "module" => $event->modulename));

                if ($event->modulename == "assign" || $event->modulename == "quiz")
                {
                    $eventLink = $CFG->magic_ui_root . "/tasks/" . reset($cms)->cmid;
                }
                else
                {
				    $eventLink = "../mod/" . $event->modulename . "/view.php?id=" . reset($cms)->cmid;
                }
			}

			$text .= "<a href=\"" . $eventLink . "\"";

            if (!empty($class))
            {
                $text .= " class=\"" . $class . "\"";
            }

            if (!empty($id))
            {
                $text .= " id=\"" . $id . "\"";
            }

            $text .= ">" . $name . "</a>";
		}

		return $text;
	}

    private function is_today($stamp)
    {
        return date("Y-m-d", $stamp) == date("Y-m-d");
    }

    private function same_month($stamp1, $stamp2)
    {
        $d1 = getdate($stamp1);
        $d2 = getdate($stamp2);

        return $d1["year"] == $d2["year"] && $d1["mon"] == $d2["mon"];
    }

    private function trim_line($txt, $maxLen)
    {
        if (strlen($txt) <= $maxLen + 2)
        {
            return $txt;
        }

        $t = substr($txt, 0, $maxLen + 2);
        $s = strrpos($t, " ");

        if ($s !== FALSE)
        {
            $t = substr($t, 0, $s);
        }

        $t .= "...";

        return $t;
    }
}

require_once($CFG->dirroot.'/course/format/weeks/renderer.php');

class theme_monorail_format_weeks_renderer extends format_weeks_renderer {

    protected function format_summary_text($section) {
        $context = context_course::instance($section->course);
        $summarytext = file_rewrite_pluginfile_urls($section->summary, 'pluginfile.php',
            $context->id, 'course', 'section', $section->id);

        $options = new stdClass();
        //~ $options->noclean = true;
        $options->overflowdiv = true;
        $text = format_text($summarytext, $section->summaryformat, $options);
        $text = str_replace('  ', '&nbsp; ', $text);
        // this causes problems when there is JS in summary, disabling
        //~ $text = nl2br($text);
        return $text;
    }
}

require_once($CFG->dirroot.'/course/format/topics/renderer.php');

class theme_monorail_format_topics_renderer extends format_topics_renderer {

    protected function format_summary_text($section) {
        $context = context_course::instance($section->course);
        $summarytext = file_rewrite_pluginfile_urls($section->summary, 'pluginfile.php',
            $context->id, 'course', 'section', $section->id);

        $options = new stdClass();
        //~ $options->noclean = true;
        $options->overflowdiv = true;
        $text = format_text($summarytext, $section->summaryformat, $options);
        $text = str_replace('  ', '&nbsp; ', $text);
        // this causes problems when there is JS in summary, disabling
        //~ $text = nl2br($text);
        return $text;
    }
}
