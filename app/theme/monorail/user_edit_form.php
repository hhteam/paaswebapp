<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot.'/lib/formslib.php');

class theme_monorail_user_edit_form extends moodleform {

    // Define the form
    function definition () {
        global $CFG, $COURSE, $USER;

        $mform =& $this->_form;
        $editoroptions = null;
        $filemanageroptions = null;
        $userid = $USER->id;

        if (is_array($this->_customdata)) {
            if (array_key_exists('editoroptions', $this->_customdata)) {
                $editoroptions = $this->_customdata['editoroptions'];
            }
            if (array_key_exists('filemanageroptions', $this->_customdata)) {
                $filemanageroptions = $this->_customdata['filemanageroptions'];
            }
            if (array_key_exists('userid', $this->_customdata)) {
                $userid = $this->_customdata['userid'];
            }
        }
        //Accessibility: "Required" is bad legend text.
        $strgeneral  = get_string('general');
        $strrequired = get_string('required');

        /// Add some extra hidden fields
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'course', $COURSE->id);
        $mform->setType('course', PARAM_INT);

        /// shared fields
        monorail_useredit_shared_definition($mform, $editoroptions, $filemanageroptions);

        /// extra settigs
        if (!empty($CFG->gdversion) and !empty($CFG->disableuserimages)) {
            $mform->removeElement('deletepicture');
            $mform->removeElement('imagefile');
        }

        /// Next the customisable profile fields
        profile_definition($mform, $userid);
        $this->add_action_buttons(false, get_string('updatemyprofile'));
    }

    function definition_after_data() {
        global $CFG, $DB, $OUTPUT;

        $mform =& $this->_form;
        $userid = $mform->getElementValue('id');

        if ($user = $DB->get_record('user', array('id'=>$userid))) {

            // print picture
            if (!empty($CFG->gdversion)) {
                $context = get_context_instance(CONTEXT_USER, $user->id, MUST_EXIST);
                $fs = get_file_storage();
                $hasuploadedpicture = ($fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.png') || $fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.jpg'));

                $imagevalue = $OUTPUT->user_picture($user, array('courseid' => SITEID, 'size'=>200, 'link'=>false));
                $imagevalue = $OUTPUT->box($imagevalue, null, 'PictureBox');
                
                $dropdownmenu = array(
                		0 => '<a url="#" id="mUploadFile">'.get_string('upload_file', 'theme_monorail').'</a>',
                		1 => '<a url="#" id="mUseFacebookPicture">'.get_string('use_facebook_picture', 'theme_monorail').'</a>');
                $imagevalue .= $OUTPUT->button_dropdown(get_string('change_picture', 'theme_monorail'), $dropdownmenu, array('id' => 'btChangePicture'));
                
                $imageelement = $mform->getElement('currentpicture');
                $imageelement->setValue($imagevalue);
            }

            /// disable fields that are locked by auth plugins
            $fields = get_user_fieldnames();
            $authplugin = get_auth_plugin($user->auth);
            foreach ($fields as $field) {
                if (!$mform->elementExists($field)) {
                    continue;
                }
                $configvariable = 'field_lock_' . $field;
                if (isset($authplugin->config->{$configvariable})) {
                    if ($authplugin->config->{$configvariable} === 'locked') {
                        $mform->hardFreeze($field);
                        $mform->setConstant($field, $user->$field);
                    } else if ($authplugin->config->{$configvariable} === 'unlockedifempty' and $user->$field != '') {
                        $mform->hardFreeze($field);
                        $mform->setConstant($field, $user->$field);
                    }
                }
            }

            /// Next the customisable profile fields
            profile_definition_after_data($mform, $user->id);

        } else {
            profile_definition_after_data($mform, 0);
        }
    }

    function validation($usernew, $files) {
        global $CFG, $DB;

        $errors = parent::validation($usernew, $files);

        $usernew = (object)$usernew;
        $user    = $DB->get_record('user', array('id'=>$usernew->id));

        // validate email
        if (!isset($usernew->email)) {
            // mail not confirmed yet
        } else if (!validate_email($usernew->email)) {
            $errors['email'] = get_string('invalidemail');
        } else if (($usernew->email !== $user->email) and $DB->record_exists('user', array('email'=>$usernew->email, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['email'] = get_string('emailexists');
        }

        if (isset($usernew->email) and $usernew->email === $user->email and over_bounce_threshold($user)) {
            $errors['email'] = get_string('toomanybounces');
        }

        if (isset($usernew->email) and !empty($CFG->verifychangedemail) and !isset($errors['email']) and !has_capability('moodle/user:update', get_context_instance(CONTEXT_SYSTEM))) {
            $errorstr = email_is_not_allowed($usernew->email);
            if ($errorstr !== false) {
                $errors['email'] = $errorstr;
            }
        }

        /// Next the customisable profile fields
        $errors += profile_validation($usernew, $files);

        return $errors;
    }
}

function monorail_useredit_shared_definition(&$mform, $editoroptions = null, $filemanageroptions = null) {
	global $CFG, $USER, $DB, $OUTPUT;

	$user = $DB->get_record('user', array('id' => $USER->id));
	useredit_load_preferences($user, false);

	$strrequired = get_string('required');

	$nameordercheck = new stdClass();
	$nameordercheck->firstname = 'a';
	$nameordercheck->lastname  = 'b';

	$mform->addElement('header', 'userpicture', null);

	$mform->addElement('static', 'currentpicture');

	$mform->addElement('header', 'generalinfo', null);

	if (fullname($nameordercheck) == 'b a' ) {  // See MDL-4325
		$mform->addElement('text', 'lastname',  get_string('lastname')." <span class='required'>($strrequired)</span>",  'maxlength="100" size="30"');
		$mform->addElement('text', 'firstname', get_string('firstname')." <span class='required'>($strrequired)</span>", 'maxlength="100" size="30"');
	} else {
		$mform->addElement('text', 'firstname', get_string('firstname')." <span class='required'>($strrequired)</span>", 'maxlength="100" size="30"');
		$mform->addElement('text', 'lastname',  get_string('lastname')." <span class='required'>($strrequired)</span>",  'maxlength="100" size="30"');
	}

	$mform->addRule('firstname', $strrequired, 'required', null, 'user');
	$mform->setType('firstname', PARAM_NOTAGS);

	$mform->addRule('lastname', $strrequired, 'required', null, 'client');
	$mform->setType('lastname', PARAM_NOTAGS);



	// Do not show email field if change confirmation is pending
	if (!empty($CFG->emailchangeconfirmation) and !empty($user->preference_newemail)) {
		$notice = get_string('emailchangepending', 'auth', $user);
		$notice .= '<br /><a href="user_edit.php?cancelemailchange=1&amp;id='.$user->id.'">'
				. get_string('emailchangecancel', 'auth') . '</a>';
		$mform->addElement('static', 'emailpending', get_string('email'), $notice);
	} else {
		$mform->addElement('text', 'email', get_string('email')." <span class='required'>($strrequired)</span>", 'maxlength="100" size="30"');
		$mform->addRule('email', $strrequired, 'required', null, 'client');
	}

	$mform->addElement('text', 'city', get_string('city')." <span class='required'>($strrequired)</span>", 'maxlength="120" size="30"');
	$mform->setType('city', PARAM_MULTILANG);
	$mform->addRule('city', $strrequired, 'required', null, 'client');
	if (!empty($CFG->defaultcity)) {
		$mform->setDefault('city', $CFG->defaultcity);
	}

	$choices = get_string_manager()->get_list_of_countries();
	$choices= array(''=>get_string('selectacountry').'...') + $choices;
	$mform->addElement('select', 'country', get_string('selectacountry')." <span class='required'>($strrequired)</span>", $choices);
	$mform->addRule('country', $strrequired, 'required', null, 'client');
	if (!empty($CFG->country)) {
		$mform->setDefault('country', $CFG->country);
	}

	if (!empty($CFG->usetags) and empty($USER->newadminuser)) {
		$mform->addElement('tags', 'interests', get_string('interestslist'), array('display' => 'noofficial'));
	}

	if (!empty($CFG->gdversion) and empty($USER->newadminuser)) {

		if (!empty($CFG->enablegravatar)) {
			$mform->addElement('html', html_writer::tag('p', get_string('gravatarenabled')));
		}
	}


}
