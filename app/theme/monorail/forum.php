<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(dirname(dirname(__FILE__))).'/config.php');

require_once(dirname(__FILE__).'/lib.php');

require_once($CFG->dirroot . '/mod/forum/lib.php');

define('MOD_FORUM', 	'forum');
define('ACTION_POST',	'post');

// Get Param from request
$course_id 	= optional_param('c', null, PARAM_INT);   		// course
$discuss_id = optional_param('d', null, PARAM_INT);   		// discussion
$action		= optional_param('action', null, PARAM_ALPHA);	// action

// Get Param for post
$reply   = optional_param('reply', 0, PARAM_INT);
$forum_id= optional_param('forum', 0, PARAM_INT);
$edit    = optional_param('edit', 0, PARAM_INT);
$delete  = optional_param('delete', 0, PARAM_INT);
$prune   = optional_param('prune', 0, PARAM_INT);
$name    = optional_param('name', '', PARAM_CLEAN);
$confirm = optional_param('confirm', 0, PARAM_INT);
$groupid = optional_param('groupid', null, PARAM_INT);
$cancel  = optional_param('cancel', null, PARAM_TEXT);

if ($course_id && !$course = $DB->get_record('course', array('id'=>$course_id))) {
	print_error('invalidcourseid');
}
if ($discuss_id && !isset($course_id)) {
	$discussion = $DB->get_record('forum_discussions', array('id' => $discuss_id), '*', MUST_EXIST);
	$course 	= $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
	$course_id	= $course->id;
}
if (empty($course)) {
	print_error('invalidcourseid');
}

require_course_login($course);

add_to_log($course->id, 'theme_monorail', 'view '.MOD_FORUM, 'index.php?id='.$course->id.'&mod='.MOD_FORUM, '');

$context 	= get_context_instance(CONTEXT_COURSE, $course->id, MUST_EXIST);

$PAGE->set_url('/theme/monorail/forum.php', array(
		'c' 		=> $course_id,
		'd' 		=> $discuss_id,
		'action' 	=> $action,
		'reply' 	=> $reply,
        'forum' 	=> $forum_id,
        'edit'  	=> $edit,
        'delete'	=> $delete,
        'prune' 	=> $prune,
        'name'  	=> $name,
        'confirm'	=>$confirm,
        'groupid'	=>$groupid,));
$PAGE->set_title('Eliademy: ' . $course->fullname);
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
$PAGE->set_course($course);
$PAGE->set_pagetype('theme-monorail-' . MOD_FORUM);
$PAGE->set_pagelayout('incourse');

echo $OUTPUT->header();

unset($SESSION->fromdiscussion);

$imgplus	  = '<img src="'.$OUTPUT->pix_url('discussion-new', 'theme_monorail').'"/>';

$forums = array();

// Fetch all the course forums
$rawforums = $DB->get_records('forum', array('course' => $course->id));

$modinfo 		= get_fast_modinfo($course);

if (!isset($modinfo->instances['forum'])) {
	$modinfo->instances['forum'] = array();
}

// Put forum into array
foreach ($modinfo->instances['forum'] as $forumid=>$cm) {
	if (!$cm->uservisible or !isset($rawforums[$forumid])) {
		continue;
	}

	$forum = $rawforums[$forumid];

	if (!has_capability('mod/forum:viewdiscussion', $context)) {
		continue;
	}

	$forums[$forum->id] = $forum;
}
echo "<table id='forum-content'><tr>";
if ($forums) {
	echo '<td id="forum-box">';
	foreach ($forums as $forum) {
		$cm    	= $modinfo->instances['forum'][$forum->id];
		$count 	= forum_count_discussions($forum, $cm, $course);

		$groupmode    = groups_get_activity_groupmode($cm, $course);
		$currentgroup = groups_get_activity_group($cm);

		echo "<div class=\"forum-heading\" id=\"forum-" . $forum->id . "\">" . format_string($forum->name, true) . "</div>";

        /*
		$table   = new html_table();
		$table->attributes['class'] = 'forum-header';

		$row	 = new html_table_row();
		$row->id = 'forum-'.$forum->id;

        $cell1 = new html_table_cell($forumname);

        $row->cells = array($cell1);

		$table->data[] = $row;
        
		echo html_writer::table($table);
        */
		
		// Get all discussion
		if ($discussions = forum_get_discussions($cm, '', false, null, 0, true, -1, 0))
        {
			$replies = forum_count_discussion_replies($forum->id);
			$unreads = forum_get_discussions_unread($cm);

			$canviewparticipants = has_capability('moodle/course:viewparticipants',$context);

			foreach ($discussions as $discussion) {
				if (!empty($replies[$discussion->discussion])) {
					$discussion->replies 	= $replies[$discussion->discussion]->replies;
					$discussion->lastpostid = $replies[$discussion->discussion]->lastpostid;
				} else {
					$discussion->replies = 0;
				}

				if (empty($unreads[$discussion->discussion])) {
					$discussion->unread = 0;
				} else {
					$discussion->unread = $unreads[$discussion->discussion];
				}

				$ownpost = ($discussion->userid == $USER->id);

				// Use discussion name instead of subject of first post
				$discussion->subject = $discussion->name;

                $forumlink	 = '<a class="discussion-name" id="'.$discussion->discussion.'" href="javascript:void(0)" onclick="return false;">'.$discussion->subject.'</a>';
				$fullname 	 = fullname($discussion, has_capability('moodle/site:viewfullnames', $context));

				$select = '';
				if ($discuss_id && $discuss_id == $discussion->discussion) {
					$select = 'selected';
				}

				echo "<div class='discussion-header $select clickable ndiscuss_$discussion->discussion' onclick=\"removeURLHash(); monorail_forum_show_discussion(".$discussion->discussion.",".$course->id."); return false;\">";
				echo $forumlink;
				echo "<div><div id=\"rcount\" class=\"inline\">$discussion->replies</div> ".get_string('replies', 'forum')."<div class=\"forum_notification hide\">&nbsp;<div id=\"ncount\" class=\"inline\">0</div> ".get_string('newreply', 'theme_monorail')."</div>";
				echo " | ".get_string('startedby', 'forum')." $fullname</div>";
				echo "<div class=\"notifybarleft hide\"> </div>";
				echo '</div>'; // Close discussion-box
			}
		}

		// Check whether user can create new discussion
		if (forum_user_can_post_discussion($forum, $currentgroup, $groupmode, $cm, $context))
        {
            echo("<p style=\"text-align: center; padding: 15px 0px;\"><button class=\"btn\" onclick=\"monorail_forum_new_discussion($forum->id, $course->id); return false;\">" . get_string('new_discussion', 'theme_monorail') . "</button></p>");
        }

        /*
		if ($canstart && !$newdiscuss) {
			$clicksummary = "<a onclick=\"monorail_forum_new_discussion($forum->id, $course->id); return false;\">$imgplus</a>";
            $cell2 = new html_table_cell($clicksummary);
            //show its cursor style
            $cell2->style = 'cursor:pointer';

		}
        if($canstart && !$newdiscuss)
            $row->cells = array($cell1, $cell2);
        else

        $newdiscuss = $forum_id == $forum->id && $action == ACTION_POST;

		if ($newdiscuss) {
			echo "<div class='discussion-header selected'>";
			echo "<span class='discussion-name'>".get_string('new_discussion', 'theme_monorail')."</span>";
			echo '<div>&nbsp;</div>';
			echo '</div>'; // Close discussion-box
		}
        */

	}
	echo '</td>';
}

echo '<td id="discussion-box">';
if ($action == ACTION_POST) {

	if ($edit) {
		require_once 'discuss.php';
	}
	else {
		require_once 'post.php';
	}
}
else if ($discuss_id) {
	require_once 'discuss.php';
}
else {
	echo '<div class="forumnodiscuss">'.get_string('no_selected_discussion', 'theme_monorail').'</div>';
}
echo '</td>';

echo '</tr></table>';

echo("<div class=\"block-shadow\"></div>");

$PAGE->requires->data_for_js('data', array('txtmessage' => get_string("message", "forum"), 'txtsubject' => get_string("subject", "forum"), 'userid' => $USER->id, 'courseid' => $course->id, 'discussionid' => ($discuss_id) ? $discuss_id : 0));
if ($discuss_id)
    $PAGE->requires->js_init_call('M.theme_monorail.show_discussion');
// JS lang strings
$PAGE->requires->string_for_js('erroremptymessage', 'forum');
$PAGE->requires->string_for_js('erroremptysubject', 'forum');
$PAGE->requires->string_for_js('attachment', 'forum');
$PAGE->requires->string_for_js('edit', 'forum');
$PAGE->requires->string_for_js('delete', 'forum');
$PAGE->requires->string_for_js('replies', 'forum');
$PAGE->requires->string_for_js('reply', 'forum');
$PAGE->requires->string_for_js('startedby', 'forum');
$PAGE->requires->string_for_js('posttoforum', 'forum');
$PAGE->requires->string_for_js('erroruploadfailed', 'theme_monorail');
$PAGE->requires->string_for_js('forum_attachment', 'theme_monorail');
$PAGE->requires->string_for_js('newreply', 'theme_monorail');
$PAGE->requires->string_for_js('cancel', 'moodle');

echo $OUTPUT->footer();
