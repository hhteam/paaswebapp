/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

M.theme_monorail = {};

M.theme_monorail.initpostdefaultvalue = function () {
	var defaultSubject = data.txtsubject;
    var defaultMessage = data.txtmessage;
    if ($('input#id_subject').length) {
    	if ($('input#id_subject').val() == '') {
    		$('input#id_subject').val(defaultSubject);
		}
    	$('input#id_subject').focus(function ClearDefaulValue() {
    		if ($(this).val() == defaultSubject) {
    			$(this).val('');
    		}
    	});
    	
    	$('input#id_subject').blur(function SetDefaulValue() {
    		if ($(this).val() == '') {
    			$(this).val(defaultSubject);
    		}
    	});
    }
    
    if ($('#id_message').length) {
    	
    	if ($('#id_message').val() == '') {
    		$('#id_message').val(defaultMessage);
		}
    	
    	$('#id_message').focus(function ClearDefaulValue() {
    		if ($(this).val() == defaultMessage) {
    			$(this).val('');
    		}
    	});
    	
    	$('#id_message').blur(function SetDefaulValue() {
    		if ($(this).val() == '') {
    			$(this).val(defaultMessage);
    		}
    	});
    }
};

M.theme_monorail.changecancelaction = function () {
	if ($("div#post-form.indiscuss-reply").length) {
		var text = $("#id_cancel").val();
		var newcontent = '<input type="button" value="' + text + '" onclick="monorail_forum_hidereply()" id="id_cancel">';
		$("#id_cancel").replaceWith(newcontent);
	}
};

M.theme_monorail.show_discussion = function (Y) {
    monorail_forum_show_discussion(data.discussionid, data.courseid);
}

M.theme_monorail.show_note = function(Y) {
	monorail_notes_show_note(data.noteid);
}

M.theme_monorail.initpicturebox = function (Y, clientid) {
	var boxFileUpload = $("<div>").attr('id', 'pictureuploadcontainer').css({ width:0, height:0, 'overflow':'hidden'}).appendTo($(document.body))
	var uploader = new qq.FileUploader({
        element: $('div#pictureuploadcontainer')[0],
        action: M.cfg.wwwroot+'/theme/monorail/ext/ajax_save_user_avatar.php',
        multiple: false,
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        debug: true,
        params: {'attachfilehash': $('input#attachfilehash').val()},
        uploadButtonText: '',
        onComplete: function(id, fileName, responseJSON) {
        	if (responseJSON != false) {
        		var userpicture = $('#PictureBox img.userpicture');
        		var src = userpicture.attr('src');
        		src = src.substring(0, src.indexOf('?rev=') + 5);
        		src = src + responseJSON;
        		
        		userpicture.attr('src', src + new Date().getTime());
        	}
        	$('#PictureBox img.userpicture').show();
        	$('#PictureBox').css('background-position', '-9999px -9999px');
        },
        onProgress: function(id, fileName, loaded, total){
        	$('#PictureBox img.userpicture').hide();
        	$('#PictureBox').css('background-position', 'center');
        },
        onCancel: function(id, fileName){
        	$('#PictureBox img.userpicture').show();
        	$('#PictureBox').css('background-position', '-9999px -9999px');
        },
        onError: function(id, fileName){
        	$('#PictureBox img.userpicture').show();
        	$('#PictureBox').css('background-position', '-9999px -9999px');
        }
    });
	
	$('#mUploadFile').click(function () {
		$('input[name="file"]').click();
	});
	
	$('#mUseFacebookPicture').click(function () {
		$url = "https://www.facebook.com/dialog/oauth/?client_id="+clientid+"&redirect_uri="+M.cfg.wwwroot+"/theme/monorail/ext/ajax_get_facebook_avatar.php&scope=email&response_type=code";       
		
		$win = window.open($url, null, "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");
		$win.focus();
	});
};

window.addEventListener('load', function() {
    FastClick.attach(document.body);
}, false);
