/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

var notifications = null;

$(document).ready(function() {
	// ALERT: Dirty hack to convert all button into bootstrap button
	$('input[type=button]:not(.btn)').addClass('btn btn-primary');
	$('input[type=submit]:not(.btn)').addClass('btn btn-primary');
	
	// Convert all the select
	//$('select').selectbox();
	

	// Make the whole element having .clickable to be clickable
	if ($(".clickable").data("events") == null) {
		$(".clickable").click(function clickarea(){
		     window.location=$(this).find("a").attr("href"); 
		     return false;
		});
	}

	// Initiate the notification
    if ($('body').attr('id') != 'page-login-index' &&
        $('body').attr('id') != 'page-login-forgot_password' && $('body').attr('id').indexOf('page-login') == -1) {
        notifications = new Notifications();
        var intervalId;

        notifications.init();
        //TODO make refresh number configurable
        intervalId = setInterval(function() { notifications.getNotifications(); }, 20000);
    }
    
    // Show the notification menu
    $('.coursesubmenucont').hover(
        function () {
            //show menu
            if ($(this).find('.notify-unread').length)
                notifications.toggleMenu('show', this);
        },
        function () {
            //hide menu
            if ($(this).find('.notify-unread').length)
                notifications.toggleMenu('hide', this);     
        }
    );
	
    // Show default value in login page
    var defaultUsername = 'Username';
    var defaultEmail	= 'Email address'
    if ($('.loginform #username').length) {    	
    	if ($('#username').val() == '') {
    		$('#username').val(defaultUsername);
		}
    	$('#username').focus(function ClearDefaulValue() {
    		if ($(this).val() == defaultUsername) {
    			$(this).val('');
    		}
    	});
    	
    	$('#username').blur(function SetDefaulValue() {
    		if ($(this).val() == '') {
    			$(this).val(defaultUsername);
    		}
    	});
    }
    
    // Show list of course in Course page
    $( "#popup" ).click(function() {
		$("#course-selection" ).toggle( "blind", {} , 400 );
		$('#popup').toggleClass('open');
		return false;
	});
    
    // global var so that we don't end up in an ajax loop
    var monorailReloadNotifications = true;
    // Create previews for any resource links
    if ($('body').attr('id').indexOf('page-course-view') > -1) {
        $('li.modtype_resource').each(function() {
            // add container for images, if needed
            if (! $(this).parent().siblings('div.embedded-image-container').length) {
                $(this).parent().siblings('div.summary').after(function() {
                    return '<div class="embedded-image-container"></div>';
                });
            }
            // disable existing link
            $(this).find('a').attr('href', "javascript:void(0)");
            // get new content
            var id = $(this).attr('id').replace('module-', '');
            var courseId = $.urlParam('id');
            jQuery.getJSON(
                M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_resource.php', 
                {i:id, c:courseId},
                function(data) {
                    if (data[0]['type'] == 'image') {
                        // add to page
                        $('li.modtype_resource#module-'+data[0]['itemid']).parent().siblings('div.embedded-image-container').append(data[0]['html']);
                        // remove original
                        $('li.modtype_resource#module-'+data[0]['itemid']).remove();
                        // reapply colorbox
                        jQuery('a.embedded-image').colorbox({rel:"resource-images", maxWidth: "98%"});
                    } else {
                        $('li.modtype_resource#module-'+data[0]['itemid']+' div.mod-indent').html(data[0]['html']);
                        $('li.modtype_resource#module-'+data[0]['itemid']).show();
                    }
                }
            );
        });
        $("body").ajaxStop(function(){
            if (monorailReloadNotifications) {
                // Reload notifications as it is assumed at this point we have all the Ajax loaded images on the page (if any)
                monorailReloadNotifications = false;    // only once
                notifications.getNotifications();
            }
        });
    }
    
    // Forgot password page
    if ($('body#page-login-forgot_password').length) {
    	
    	if ($('#id_username').val() == '') {
    		$('#id_username').val(defaultUsername);
		}
    	$('#id_username').focus(function () {
    		if ($(this).val() == defaultUsername) {
    			$(this).val('');
    		}
    	});
    	$('#id_username').blur(function () {
    		if ($(this).val() == '') {
    			$(this).val(defaultUsername);
    		}
    	});
    	$('#id_username').change(function () {
    		if ($(this).val() == '' || $(this).val() == defaultUsername) {
    			$('#id_email').prop('disabled', false);
    			$('#id_email').removeClass('dimmed');
    		}
    		else {
    			$('#id_email').prop('disabled', true);
    			$('#id_email').addClass('dimmed');
    		}
    	});
    	
    	if ($('#id_email').val() == '') {
    		$('#id_email').val(defaultEmail);
		}
    	$('#id_email').focus(function () {
    		if ($(this).val() == defaultEmail) {
    			$(this).val('');
    		}
    	});
    	$('#id_email').blur(function () {
    		if ($(this).val() == '') {
    			$(this).val(defaultEmail);
    		}
    	});
    	$('#id_email').change(function () {
    		if ($(this).val() == '' || $(this).val() == defaultEmail) {
    			$('#id_username').prop('disabled', false);
    			$('#id_username').removeClass('dimmed');
    		}
    		else {
    			$('#id_username').prop('disabled', true);
    			$('#id_username').addClass('dimmed');
    		}
    	});
    	
    	$('#bt_retrievepassword').click(function () {
    		if ($('#id_username').val() != '' && $('#id_username').val() != defaultUsername) {
    			$('#id_submitbuttonusername').trigger('click');
    		}
    		else if ($('#id_email').val() != '' && $('#id_email').val() != defaultEmail) {
    			$('#id_submitbuttonemail').trigger('click');
    		}
    	});

    	if (!$('.continuebutton').length) {
    		$('#bt_retrievepassword').show();
    	}
    }

     // Sign up page
    if ($('body#page-theme-monorail-user_settings').length) {
        $('select#languageselect').change(function () {
                var ix=document.getElementById("languageselect").selectedIndex;
                var iy=document.getElementById("languageselect").options;
                $data='language='+iy[ix].value;
                $.ajax({
                    type: "POST",
                    url: "../monorail/ext/ajax_save_user_lang.php",
                    data: $data,
                    success: function (msg) {
                       window.location.reload(); 
                    },
                    error: function (x, e) {
                    }
                }); 
        });
    }
   
    // Sign up page
    if ($('body#page-login-signup').length) {
        $('#bt_getdatafacebook').click(function () {
                window.location = "https://www.facebook.com/dialog/oauth/?client_id="+M.cfg.fbclientid+"&redirect_uri="+M.cfg.wwwroot+"/auth/googleoauth2/facebook_redirect.php&scope=email,user_birthday,user_location&response_type=code"; 
        });

        $("#id_submitbutton").css({"float": "right"});
        $("#id_cancel").css({"float": "none"});
    }

    // Sign up page
    if ($('body#page-login-index').length) {
        $('#bt_signup').click(function () {
            //window.location = M.cfg.wwwroot+"/login/signup.php";
    	    window.location = M.cfg.wwwroot+"/theme/monorail/signup.php";
        });
    }
    
    // Forgot password page
    if ($('body#page-login-change_password').length) {
    	if (!$('.continuebutton').length) {
    		$('#passworldpolicy').show();
    	}
    }
});

// Function that hide/show news item
function toggleNewsItem() {
	var temp = $(this).html();
	if ($(this).val() && $(this).css('overflow') == 'hidden') {
		$(this).html($(this).val());
		$(this).val(temp);
		$(this).css('overflow', 'visible');
		$(this).height('auto');
	}
	else {
		$(this).css('overflow', 'hidden');
		$(this).height('46px');
		$(this).html($(this).val());
		$(this).val(temp);
	}
}

// Function by user geekyjohn @ http://snipplr.com/view/26662/get-url-parameters-with-jquery--improved/
$.urlParam = function(name){
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (!results) { 
        return 0; 
    }
    return results[1] || 0;
};

// Small customization to function by user Scott Dowding @ http://stackoverflow.com/a/488073/1489738
function isScrolledIntoView(elem) {
	if ($(elem).length) {
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();

		var elemTop = $(elem).offset().top;
		//var elemBottom = elemTop + $(elem).height();
        var elemBottom = elemTop + 20;

		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	} else
		return false;
}

// Override setTimeout to accept and remember additional variables
//TODO: test IE!
// Unknown author, found from: http://www.richardcastera.com/blog/javascript-passing-multiple-parameters-through-settimeout
var _st = window.setTimeout;
delayExec = function(fRef, mDelay) {
    if(typeof fRef == "function") { 
        var argu = Array.prototype.slice.call(arguments,2);
        var f = (function(){ fRef.apply(null, argu); });
        return _st(f, mDelay);
    }
    return _st(fRef,mDelay);
};

// Small hack to trigger the Add button in Filemanager
function triggeradd() {
	$('.filemanager .fp-btn-add').trigger('click');
}

// Trigger the Select File input, then monitor the uploading process and submit the form
function uploadpic() {
	$('input[name="repo_upload_file"]').trigger('click');
	$('input[name="repo_upload_file"]').change(function (){		
		$('.fp-upload-btn').trigger('click');
	});
	
	$('.fp-content').contentChange( function() {
		if ($('img.realpreview').attr('src')) {
			$('#id_submitbutton').trigger('click');
		}
	});
}

// Check if this element has overflow content
$.fn.hasOverflow = function() {
    var $this = $(this);
    var $children = $this.find('*');
    var len = $children.length;

    if (len) {
        var maxWidth = 0;
        var maxHeight = 0
        $children.map(function(){
            maxWidth = Math.max(maxWidth, $(this).outerWidth(true));
            maxHeight = Math.max(maxHeight, $(this).outerHeight(true));
        });

        return maxWidth > $this.width() || maxHeight > $this.height();
    }

    return false;
};

// An event occur when the content of the element change
jQuery.fn.contentChange = function(callback){
    var elms = jQuery(this);
    elms.each(
      function(i){
        var elm = jQuery(this);
        elm.data("lastContents", elm.html());
        window.watchContentChange = window.watchContentChange ? window.watchContentChange : [];
        window.watchContentChange.push({"element": elm, "callback": callback});
      }
    )
    return elms;
  }
  setInterval(function(){
    if(window.watchContentChange){
      for( i in window.watchContentChange){
        if(window.watchContentChange[i].element.data("lastContents") != window.watchContentChange[i].element.html()){
          window.watchContentChange[i].callback.apply(window.watchContentChange[i].element);
          window.watchContentChange[i].element.data("lastContents", window.watchContentChange[i].element.html())
        };
      }
    }
  },500);

// http://note19.com/2007/05/27/javascript-guid-generator/
function S4() {
   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}
// http://note19.com/2007/05/27/javascript-guid-generator/
function guid() {
   return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

// Removes the #foo in the url - this is needed when clicking on the discussion panel if the current
// url contains a referral to a single post - otherwise that anchor will transfer
function removeURLHash() {
    var stateObj = { foo: "bar" };
    var url = window.location.toString().replace(window.location.hash, '');
    history.pushState(stateObj, 'Forum post', url);
}
