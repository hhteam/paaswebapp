/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

function Notifications() {
	
    var courseId = "0";
    var pageName = '';
    var compId = "0";
    var compName = '';
    var markRead = Array();
    var notif = Array();
    var originalWindowTitle;
    var forumNotifs = Array();  
    var discussNotifs = Array();
    
    var lineSrc = $('#coursesubmenulineimg').html();
	
	this.init = function() {
        // identify where we are by looking at body classes and body id
		if ($('body').attr('id') == 'page-my-index') {
			pageName = 'courses_home';
		} else if ($('body').attr('id').indexOf('page-calendar-') > -1) {
			pageName = 'calendar';
		} else if ($('body').attr('id') == 'page-theme-monorail-forum') {
			compName = 'forum';
            compId = $.urlParam('d');
		}
		if ($('body').is('.pagelayout-course')) {
			pageName = 'course';
			var classes = $('body').attr('class').split(" ");
			for (var i in classes) {
				//TODO: consider adding these to monorail templates as hidden divs
				if (classes[i].substring(0, 7) == 'course-') {
					courseId = classes[i].substring(7);
				}
				if (compId == '0' && classes[i].substring(0, 5) == 'cmid-') {
					compId = classes[i].substr(5);
				}
			}
		}
        // save original window title
        originalWindowTitle = document.title;
        
        if (typeof M.cfg.monorailfeed !== 'undefined') {
            notif = M.cfg.monorailfeed.notifJson;
            processNotifications();
        }
	};
	
    this.setNofications = function(data) {
        notif = data;
    };
	
	processNotifications = function() {
        
        setCounts();
        
        if (typeof notif["c"] !== 'undefined') {
            
            if (compName == 'forum' && typeof notif["c"]["o"][courseId] !== 'undefined' && notif["c"]["o"][courseId]["f"] > 0) {
                for (var i in notif) {
                    if (i != "c" && notif[i]["t"] == 'forum') {
                        if ($('div.ndiscuss_'+notif[i]["di"]).length) {
                            // discussion is on page
							if ($.inArray(i, forumNotifs) == -1) {
								var curCount = parseInt($('div.ndiscuss_'+notif[i]["di"]+' div#ncount').html());
								if (curCount == NaN)
									curCount = 0;
								else
									curCount ++;
								$('div.ndiscuss_'+notif[i]["di"]+' div#ncount').html(curCount.toString());
								if ($('div.ndiscuss_'+notif[i]["di"]+' div.forum_notification').is('.hide')) {
									$('div.ndiscuss_'+notif[i]["di"]+' div.forum_notification').removeClass('hide');
									$('div.ndiscuss_'+notif[i]["di"]+' div.forum_notification').addClass('inline');
                                    $('div.ndiscuss_'+notif[i]["di"]+' div.notifybarleft').removeClass('hide');
								}
								forumNotifs.push(i);
							}
                        }
                        if ($('td.forum-post#p'+notif[i]["d"]).length) {
                            // post is on page, highlight
                            if ($('td.forum-post#p'+notif[i]["d"]+'.nunread').length == 0) {
                                $('td#p'+notif[i]["d"]+' div.post-header-text').after(function() {
                                    return '<div class="forum_notification inline">(new)</div>';
                                });
                                $('td.forum-post#p'+notif[i]["d"]).addClass('nunread');
                            }
                        } else {
                            if (notif[i]["di"] == compId) {
                                // post is not on page and this is the right discussion - append to page
                                monorail_forum_insert_post(notif[i]["d"], notif[i]["ui"], courseId, true, null, false, null);
                            }
                        }
                        // mark read or create watcher
                        if (isScrolledIntoView('td.forum-post#p'+notif[i]["d"]+'.nunread')) {
							$('td.forum-post#p'+notif[i]["d"]).removeClass("nunread");
							markAsRead(i, notif[i]["c"], "f");
						} else {
                            // only create a watcher for unread notifs if we are on the correct discussion page
							if (notif[i]["di"] == compId) {
                                $(window).scroll({'id':notif[i]["d"], 'notifid':i, 'course':courseId}, function(event) {
                                    if (isScrolledIntoView('td.forum-post#p'+event.data.id+'.nunread')) {
                                        $('td.forum-post#p'+event.data.id).removeClass("nunread");
                                        markAsRead(event.data.notifid, event.data.course, "f");
                                    }
                                });
                            }
						}
                    }
                }
            }
            if (pageName == 'calendar' && notif["c"]["e"] > 0) {
                // check if we are on week or month view
                var calType = $.urlParam('view');
                var elem = false;
                if (calType == 'month') {
                    elem = 'li';
                } else if (calType == 'day') {
                    elem = 'a';
                } else {
                    // where are we??
                    elem = false;
                }
                if (elem !== false) {
                    for (var i in notif) {
                        if (i != "c" && (notif[i]["t"] == 'calendar' || notif[i]["o"] == 'calendar')) {
                            var highlighted = false;
                            if ($(elem+'#e'+notif[i]["d"]).length && notif[i]["t"] == 'calendar') {
                                if ($(elem+'#e'+notif[i]["d"]+'.nunread').length == 0) {
                                    $(elem+'#e'+notif[i]["d"]).addClass('event_red nunread');
                                    if ($(elem+'#e'+notif[i]["d"]+' > a.cl1').length) {
                                        $(elem+'#e'+notif[i]["d"]+' > a').removeClass('cl1');
                                    }
                                    highlighted = true;
                                } else {
                                    highlighted = false;
                                }
                            } else {
                                // not on page
                                // check if this calendar page has an url to this day and add there if exists
                                //TODO fix calUrl when it is fixed in calendar
                                var weekParam = '';
                                if (notif[i]["ed"] == $.urlParam('cal_d')) {
                                    weekParam = '&subview=week';
                                }
                                var calUrl = M.cfg.wwwroot+'/calendar/view.php?view=day'+weekParam+'&course=1&cal_d='+notif[i]["ed"]+'&cal_m='+notif[i]["em"]+'&cal_y='+notif[i]["ey"];
                                var queryStr = 'a[href="'+calUrl+'"]';
                                if ($(queryStr).length) {
                                    var eventClass = 'event_red nunread';
                                    // found
                                    if (calType == 'month') {
                                        var content = '<li id="e'+notif[i]["d"]+'" class="calendar_event_course '+eventClass+'">';
                                        if (notif[i]["o"] == 'event_upd' || notif[i]["o"] == 'event_new') 
                                            content += '<a href="'+calUrl+'#event_'+notif[i]["d"]+'">';
                                        else
                                            content += '<a href="#">';
                                        content += notif[i]["n"]+'</a>';
                                        content += '</li>';
                                        if ($(queryStr).parent().parent().find('ul.events-new').length) {
                                            $(queryStr).parent().parent().find('ul.events-new > li:last').after(function() {
                                                return content;
                                            });
                                        } else {
                                            $(queryStr).parent().parent().find('div.daymon').after(function() {
                                                content = '<ul class="events-new">'+ content +'</ul>';
                                                return content;
                                            });
                                        }
                                    } else {
                                        // check where to place event
                                        // get todays timestamp
                                        var dayStamp = $(queryStr).attr("id");
                                        var stamp = notif[i]["s"] - dayStamp;
                                        // using diff, get the right location
                                        var loc = 0;
                                        if (stamp >= 64800)
                                            loc = 13;
                                        else if (stamp < 25200)
                                            loc = 0;
                                        else {
                                            loc = parseInt((stamp - 25200) / 3600) + 1;
                                        }
                                        //TODO are cl and fn numbered classes important? not setting them. see other rows for example
                                        var content = '<a id="e'+notif[i]["d"]+'" class="block '+eventClass+'" href="">'+notif[i]["n"]+'</a>';
                                        // add
                                        var html = $('td#'+notif[i]["ey"]+notif[i]["em"]+notif[i]["ed"]+loc).html();
                                        if (html && html.length > 0) {
                                            $('td#'+notif[i]["ey"]+notif[i]["em"]+notif[i]["ed"]+loc).html(html + content);
                                        } else {
                                            $('td#'+notif[i]["ey"]+notif[i]["em"]+notif[i]["ed"]+loc).html(content);
                                        }
                                        if (loc == 0 || loc == 13) {
                                            $('td#'+notif[i]["ey"]+notif[i]["em"]+notif[i]["ed"]+loc).addClass('morning_evening').removeClass('empty_line');
                                        } else {
                                            $('td#'+notif[i]["ey"]+notif[i]["em"]+notif[i]["ed"]+loc).addClass('event_blue').removeClass('empty_line');
                                        }
                                    }
                                    highlighted = true;
                                    // refresh counts
                                    setCounts();
                                }
                            }
                            if (highlighted) {
                                // mark read or create watcher
                                if (isScrolledIntoView(elem+'#e'+notif[i]["d"]+'.nunread')) {
                                    $(elem+'#e'+notif[i]["d"]).removeClass("nunread");
                                    markAsRead(i, notif[i]["c"], "e");
                                } else {
                                    $(window).scroll({'id':notif[i]["d"], 'notifid':i, 'course':courseId, 'comp':notif[i]["o"]}, function(event) {
                                        if (isScrolledIntoView(elem+'#e'+event.data.id+'.nunread')) {
                                            $(elem+'#e'+event.data.id).removeClass("nunread");
                                            markAsRead(event.data.notifid, event.data.course, "e");
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
			}
        }
	};
	
    setCounts = function() {
        if (typeof notif["c"] === 'undefined' || notif["c"]["a"] == 0) {
            $('#courses_ncount').html('');
            $('#courses_ncount').removeClass('notify-unread');
            document.title = originalWindowTitle;
            if ($('body').attr('id') == 'page-my-index')
                $('button#feed-mark-all-read-button').hide();
        } else {
            $('#courses_ncount').html(notif["c"]["a"]);
            $('#courses_ncount').addClass('notify-unread');
            document.title = originalWindowTitle; //+ ' (' + notif["c"]["a"] + ')';
            if ($('body').attr('id') == 'page-my-index')
                $('button#feed-mark-all-read-button').show();
        }
    };
    
	this.getNotifications = function() {
		getNotifications();
	};
	
	getNotifications = function() {
		jQuery.getJSON(M.cfg.wwwroot+'/local/monorailfeed/ext/ajax_notifications.php', function(data) {
            notif = data;
			processNotifications();
		});
	};
	
	markAsRead = function(id, course, type) {
        if (notif["c"]["e"] > 0 && notif["c"]["c"] && $.inArray(type, ["t", "e"]) > -1) {
            var updateNotifs = true;
        } else {
            var updateNotifs = false;
        }
		if (type != "none") {
			notif["c"]["a"]--;
			if (type == 'e') {
				notif["c"]["e"]--;
			} else {
				notif["c"]["c"]--;
				notif["c"]["o"][course][type]--;
				notif["c"]["o"][course]["a"]--;
			}
			// refresh counts
			setCounts();
		}
        jQuery.getJSON(M.cfg.wwwroot+'/local/monorailfeed/ext/ajax_mark_read.php', {'i':id});
        if (updateNotifs) {
            getNotifications();
        }
	};
	
}
