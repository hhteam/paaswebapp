/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 



function monorail_notes_show_note(noteid) {
	var stateObj = { foo: "bar" };
    history.pushState(stateObj, 'Notes', M.cfg.wwwroot+"/theme/monorail/notes.php?noteid="+noteid);
	
	// Check if any note is selected
	if ($('#noteview-box .forumnodiscuss').length) {
		// Empty noteview
		$('#noteview-box').html('');
		
		$("<div>").attr('id', 'noteview-name').appendTo($('#noteview-box'));
		$("<div>").attr('id', 'noteview-content').appendTo($('#noteview-box'));
	}
	
	// Add loading image
	var loading = '<div class="post-loading-image">';
	loading += '<img src="'+M.cfg.wwwroot+'/theme/monorail/pix/loading.gif">';
	loading += '</div>';
	
	// Remove old content
	$('#noteview-name').html(loading);
	$('#noteview-content').html('');
	
	jQuery.get(
	        M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_note.php', 
	        { i:noteid }, 
	        function(data) {
	            $('#noteview-name').html(data['name']);
	            $('#noteview-content').html(data['content']);
	            
	            $('.note-header').removeClass('selected');
	            $('#noteid_'+noteid).addClass('selected');
	            
	            $.scrollTo('div#noteview-box', 800);
	        }, 
	        'json'
	);
}
