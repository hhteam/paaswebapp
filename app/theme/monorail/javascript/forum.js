/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

// MonoRail Forum JavaScript
// (c) CBTec Oy, 2012

// Used to store user data between page loads to decrease amount of ajax calls
var userCache = {};

function monorail_forum_insert_post_content(data) {
    var message = data['m'].replace(/\r\n/g, '<br>').replace(/  /g, '&nbsp;&nbsp;');
    $('td#p'+data['i']+' div.posting').html(message);
    $('td#p'+data['i']+' div.post-header-text br').after(function() {
        return data['t'];
    });
    // hide loading gif and show post
    $('td#p'+data['i']+' div.post-loading-image').addClass('hide');
    $('td#p'+data['i']+' div.post-container').removeClass('hide');
    
    if (data["e"] == 1)
        // user can edit
        $('td#p'+data['i']+' div.post-header div.commands').html(mh_forum_commands(data['i'], data['c']));
    if (data['a'] != '' && data['a'] > 0) {
        // add forum attachments
        jQuery.getJSON(M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_forum_attachments.php', 
            {i: data['i']},
            function(data) {
                var images = '';
                var documents = '';
                var postid = 0;
                $.each(data, function(index) {
                    if (data[index]['type'] == 'image')
                        images += '<br>'+data[index]['html'];
                    else if (data[index]['type'] == 'other')
                        documents += data[index]['html'];
                    if (postid == 0)
                        postid = data[index]['itemid'];
                });
                if (images.length > 0) 
                    $('div.p'+postid+'attachedimages').html(images);
                if (documents.length > 0) {
                    $('div#p'+postid+'attachments.attachment-list').html(documents);
                }
                
                // reapply colorbox
                jQuery('a.embedded-image').colorbox({rel:"attachments", maxWidth: "98%"});
            }
        );
    }
}

function monorail_forum_insert_post(id, userid, courseid, isnew, customplacement, celldataonly, postdata) {
    if (customplacement == null) {
        $('td#discussion-box table.table-forum tr:last').after(function() {
            return mh_forum_post(id, userid, courseid, isnew, celldataonly);
        }).fadeIn('slow');
    } else {
        $(customplacement).html(function() {
            return mh_forum_post(id, userid, courseid, isnew, celldataonly);
        });
    }
    // get post and insert
    if (! postdata) {
        jQuery.getJSON(M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_forum_post.php', {
                i:id
            }, function(data) {
                monorail_forum_insert_post_content(data);
        });
    } else {
        monorail_forum_insert_post_content(postdata);
    }
    // get user data and insert
    if (typeof userCache[userid] === 'undefined') {
        jQuery.getJSON(M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_user_data.php', {
                i:userid
            }, function(data) {
                $('div#puser_'+data['i']).replaceWith(data['p']);
                $('span#puser_'+data['i']).html(data['n']+' ');
                // store user in cache
                userCache[data["i"]] = {'pic':data['p'], 'name':data['n']};
        });
    } else {
        $('div#puser_'+userid).replaceWith(userCache[userid]['pic']);
        $('span#puser_'+userid).html(userCache[userid]['name']+' ');
    }
} 

function monorail_forum_convert_message_for_saving() {
    var message = $("div#post-form form#mform1 textarea#id_message").val().replace(/'<br>'/g, '\n').replace(/'&nbsp;&nbsp;'/g, '  ');
    $("div#post-form form#mform1 textarea#id_message").val(message);
}

function monorail_forum_validate_post(message, subject, action) {
    var errors = '';
    var defaultSubject = data.txtsubject;
    var defaultMessage = data.txtmessage;
    
    //message
    if (message.length == 0 || message == data.txtmessage) {
        errors += M.str.forum.erroremptymessage+"<br>";
    }
    //subject
    if (action == 'new' && (subject.length == 0 || subject == data.txtsubject)) {
        errors += M.str.forum.erroremptysubject+"<br>";
    }
    
    return errors;
}

function monorail_forum_attachment_uploader() {
    var uploader = new qq.FineUploader({
        element: $('div#fileuploadcontainer')[0],
        request: {
            endpoint: M.cfg.wwwroot+'/theme/monorail/ext/ajax_upload_file_chunk.php'
        },
        multiple: false,
        validation: {
            allowedExtensions: ['jpg', 'jpeg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx',
                            'ppt', 'pptx', 'rtf', 'odt', 'odp', 'mp3', 'ogg', 'odf', 'ods',
                            'mp4', 'avi', 'txt', '3gpp', '3gp'],
            sizeLimit: 10485760,
        },
        debug: false,
        text: {
            failUpload: M.str.theme_monorail.erroruploadfailed,
            uploadButton: M.str.theme_monorail.forum_attachment
        },
        callbacks: {
            onComplete: function(id, fileName, responseJSON) {
                if (responseJSON['success'] == true && responseJSON['uploadName']) {
                    $('input#attachfilename').val(responseJSON['uploadName']);
                } else {
                    $('input#attachfilename').val('');
                }
            }
        },
        chunking: {
            enabled: true,
            partSize: 300000
        }
    });
    $('span.qq-drop-processing').css('display', 'none');
    $('.qq-upload-button').addClass('btn');
    $('.qq-upload-button').css('padding', '4px 14px 4px 14px');
    return uploader;
}

function monorail_forum_showreply(userId, groupId, courseId, discussionId) {
    
    var postId = $('td#discussion-box table.table-forum tr:last > td.forum-post').attr('id').replace('p','');
    
    $('div#post-form').html(function() {
        return mh_forum_post_form('reply', userId, groupId, 0, courseId, discussionId, postId, '');
    });
    
    M.theme_monorail.initpostdefaultvalue();
    
    $('div#post-form input#id_submitbutton').click(function () {
        $('div#post-form input#id_submitbutton').prop('disabled', true);
        monorail_forum_convert_message_for_saving();
        
        // validate
        var errors = monorail_forum_validate_post(
            $("div#post-form form#mform1 textarea#id_message").val(), 
            $("div#post-form form#mform1 input#id_subject").val(),
            'reply');
        if (errors.length > 0) {
            $("div#post-form #errordialog").html(errors);
            $("div#post-form #errordialog").removeClass('hide');
            $('div#post-form input#id_submitbutton').prop('disabled', false);
            return false;
        }
        var params = $("div#post-form form#mform1").serialize();
        jQuery.post(
            M.cfg.wwwroot+'/theme/monorail/ext/ajax_save_post.php', 
            params, 
            function(data) {
                monorail_forum_insert_post(data["i"], data["u"], data["c"], false, null, false, null);
                // set reply count in left discussion list
                $('div.ndiscuss_'+data["d"]+' div#rcount').html(parseInt($('div.ndiscuss_'+data["d"]+' div#rcount').html())+1);
            }, 
            'json'
        );
        
        monorail_forum_hidereply();
        $('div#post-form input#id_submitbutton').prop('disabled', false);
        
        return false;
    });
    
    var uploader = monorail_forum_attachment_uploader();
        
    $("div#replybox" ).hide();
    $("#post-form" ).show( "blind", {} , 500 );
	$('#id_message').focus();
};

function monorail_forum_hidereply() {
	if ($("#post-form").length)
        $("#post-form").hide();
    else
        $("#mform1").remove();
	$("div#replybox" ).show();
	$('#id_message').val('');
};

function monorail_forum_editpost(postId) {
    $('td#p'+postId+' div.post-content').html(function() {
        return mh_forum_post_form('edit', 0, 0, 0, 0, 0, postId, '');
    });
    
    M.theme_monorail.initpostdefaultvalue();
    
    jQuery.getJSON(M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_forum_post.php', {
            i:postId
        }, function(data) {
            $('textarea#id_message').val(data['m']);
            
            // add existing attachments
            if (data['a'] != '' && data['a'] > 0) {
                // add forum attachments
                jQuery.getJSON(M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_forum_attachments.php', 
                    {i: data['i']},
                    function(data) {
                        var content = '';
                        var postid = 0;
                        $.each(data, function(index) {
                            content += '<li class=" qq-upload-success">';
                            content += '<span class="qq-progress-bar" style="width: 100%;"></span>';
                            content += '<span class="qq-upload-finished"></span>';
                            content += '<span class="qq-upload-file">'+data[index]['name']+'</span>';
                            content += '<span class="qq-upload-size" style="display: inline;">'+data[index]['sizestr']+'</span>';
                            content += '<span class="qq-upload-failed-text">'+M.str.theme_monorail.erroruploadfailed+'</span>';
                            content += '</li>';
                            if (postid == 0)
                                postid = data[index]['itemid'];
                        });
                        if (postid != 0) {
                            $('td#p'+postid+' ul.qq-upload-list').html(content);
                        }
                    }
                );
            }
            
            
    });
    
    $('td#p'+postId+' div.post-content input#id_submitbutton').click(function () {
        $('td#p'+postId+' div.post-content input#id_submitbutton').prop('disabled', true);
        monorail_forum_convert_message_for_saving();
        // validate
        var errors = monorail_forum_validate_post(
            $("div.post-content form#mform1 textarea#id_message").val(), 
            $("div.post-content form#mform1 input#id_subject").val(),
            'edit');
        if (errors.length > 0) {
            $("div.post-content #errordialog").html(errors);
            $("div.post-content #errordialog").removeClass('hide');
            $('div.post-content input#id_submitbutton').prop('disabled', false);
            return false;
        }
        var params = $("td#p"+postId+" div.post-content form#mform1").serialize();
        jQuery.post(
            M.cfg.wwwroot+'/theme/monorail/ext/ajax_save_post.php', 
            params, 
            function(data) {
                monorail_forum_insert_post(data["i"], data["u"], data["c"], false, 'td#p'+postId, true, null);
                // TODO discussion name fix on edit? currently discussion cannot be renamed after creation..
            }, 
            'json'
        );
        
        monorail_forum_hidereply();
        $('td#p'+postId+' div.post-content input#id_submitbutton').prop('disabled', false);
        
        return false;
    });
    
    var uploader = monorail_forum_attachment_uploader();
    
    $("div#replybox" ).hide();
	$('#id_message').focus();
    
    return false;
}

function monorail_forum_new_discussion(forumId, courseId) {
    if (! $('td#discussion-box table.table-forum').length)
        $('td#discussion-box').html(mh_forum_discussion_core(0));
    $('td#discussion-box table.table-forum').hide();
    $('td#discussion-box table.table-forum').html(function() {
        return mh_forum_empty_discussion();
    });
    $('td#discussion-box div#reply-container div#post-form').html(function() {
        return mh_forum_post_form('new', 0, 0, forumId, courseId, 0, 0, '');
    });
    
    $.scrollTo('div#course-menu', 800);
    
    M.theme_monorail.initpostdefaultvalue();
    
    $('div#post-form form#mform1 input#id_submitbutton').click(function () {
        $('div#post-form form#mform1 input#id_submitbutton').prop('disabled', true);
        monorail_forum_convert_message_for_saving();
        // validate
        var errors = monorail_forum_validate_post(
            $("div#post-form form#mform1 textarea#id_message").val(), 
            $("div#post-form form#mform1 input#id_subject").val(),
            'new');
        if (errors.length > 0) {
            $("div#post-form #errordialog").html(errors);
            $("div#post-form #errordialog").removeClass('hide');
            $('div#post-form input#id_submitbutton').prop('disabled', false);
            return false;
        }
        var params = $("div#post-form form#mform1").serialize();
        jQuery.post(
            M.cfg.wwwroot+'/theme/monorail/ext/ajax_save_post.php', 
            params, 
            function(data) {
                monorail_forum_insert_post(data["i"], data["u"], data["c"], false, null, false, null);
                $('td#discussion-box table.table-forum tr#forumtable-header td div.forum-current').html(data["n"]);
                $('td#discussion-box table.table-forum').show();
                var stateObj = { foo: "bar" };  // should this be something different? :)
                history.pushState(stateObj, "Forum new discussion", M.cfg.wwwroot+"/theme/monorail/forum.php?d="+data["d"]);
                if (! $("div#replybox").length) {
                    // TODO fix groupid with groups support later
                    $("span#replyboxcontainer").html(mh_forum_reply_box(0, data["c"], data["d"]));
                }
                // set discussion id for page, needed by notifications
                $('div#discuss_id').html(data["d"]);
                
                // insert new discussion to discussion list panel
                $('#forum-'+data['f']).after(function () {
                    return mh_forum_discussion_list_item(data["d"], data["n"], data["f"], data["a"], true);
                });
            }, 
            'json'
        );
        
        monorail_forum_hidereply();
        $('div#post-form form#mform1 input#id_submitbutton').prop('disabled', false);
        
        return false;
    });
    
    var uploader = monorail_forum_attachment_uploader();
    
    $("div#replybox" ).hide();
    $("#post-form" ).show( "blind", {} , 500 );
}

function monorail_forum_show_discussion(discussionId, courseId) {
    
    // check for possible post id in url params
    var postAnchor = window.location.hash.replace('#post','');
    if (postAnchor.length > 0)
        $('body').data('postAnchor', postAnchor);
    else
        $('body').data('postAnchor', '');
    
    var stateObj = { foo: "bar" };
    history.pushState(stateObj, 'Forum show discussion', M.cfg.wwwroot+"/theme/monorail/forum.php?d="+discussionId+window.location.hash);
    
    if (! $('td#discussion-box table.table-forum').length)
        $('td#discussion-box').html(mh_forum_discussion_core(0));
    
    $('td#discussion-box table.table-forum').html(function() {
        return mh_forum_empty_discussion();
    });
    
    monorail_forum_hidereply();
    $('td#discussion-box table.table-forum').show();
    
    jQuery.getJSON(M.cfg.wwwroot+'/local/monorailfeed/ext/ajax_get_discussion.php', {i:discussionId}, function(data) {
        $('td#discussion-box table.table-forum tr#forumtable-header td div.forum-current').html(data["d"]);
    });
    
    jQuery.get(
        M.cfg.wwwroot+'/theme/monorail/ext/ajax_get_posts_discussion.php', 
        { i:discussionId }, 
        function(data) {
            var discussId = 0;
            $.each(data, function(index) {
                monorail_forum_insert_post(data[index]["i"], data[index]["u"], courseId, false, null, false, data[index]["p"]);
                if (index == 0)
                    discussId = data[index]["d"];
            });
            if ($('body').data('postAnchor').length > 0) {
                // scroll to requested post
                $.scrollTo('td#p'+$('body').data('postAnchor'), 800, {offset:-50});
            }
            // set reply count in left discussion list
            $('div.ndiscuss_'+discussId+' div#rcount').html(data.length-1);
            // mark this discussion as selected in left discussion list
            $('div.discussion-header.selected').removeClass('selected');
            $('div.ndiscuss_'+discussId).addClass('selected');
            
            // get and show notifications
            notifications.getNotifications();
        }, 
        'json'
    );
    
    if (postAnchor.length == 0) {
        $.scrollTo('div#course-menu', 800);
    }
    
    // add replybox if it is not in page yet
    if (! $("div#replybox").length) {
        if (! $("span#replyboxcontainer").length)
            $('td#discussion-box table.table-forum').after(mh_forum_reply_container());
        $("span#replyboxcontainer").html(mh_forum_reply_box(0, courseId, discussionId));
        $('#replybox').css('text-align', 'right');
    }
    
    // set discussion id for page, needed by notifications
    $('div#discuss_id').html(discussionId);
 
    return false;
}
