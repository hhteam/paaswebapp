/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

// Helpers for inserting html to MonoRail theme
// Copyright CbTec 2012

function mh_overview_task(instance, component, name, classes, url, first, removable) {
    var content = '';
    if (first)
        content += '<ul class="section img-text">';
    content += '<li id="module-'+instance+'" class="activity '+component+' modtype_'+component+' '+classes+'">';
    content += '<div class="mod-indent">';
    if (url)
        content += '<a href="'+M.cfg.wwwroot+url+instance+'">';
    if (component != 'resource' && component != 'url')
        content += '<img class="activityicon" src="'+M.cfg.wwwroot+'/theme/image.php/monorail/'+component+'/'+M.cfg.jsrev+'/icon">';
    else
        content += '<img class="activityicon" src="'+M.cfg.wwwroot+'/theme/monorail/pix_core/f/unknown.png">';
    content += '<span class="instancename">'+name+'</span>';
    if (url)
        content += '</a>';
    if (removable)
        content += '<div class="remove-text clickable">Remove</div>';
    content += '</div></li>';
    if (first)
        content += '</ul>';
    return content;
}

function mh_forum_post(postid, authorid, courseid, isnew, celldataonly) {
    var content = '';
    if (!celldataonly)
        content += '<tr><td class="forum-post '+((isnew)?'nunread':'')+'" id="p'+postid+'">';
    content += '<span class="anchor" id="post'+postid+'"></span>';
    content += '<div class="post-container hide">';     // hide initially
    content += '<div class="post-header">';
    content += '<div id="puser_'+authorid+'">';
    content += '<a href="'+M.cfg.wwwroot+'/a/profile/'+authorid+'">';
    content += '<div class="userpic-container" style="width:37px;height=37px;display:inline-block;margin-bottom:-6px">';
    content += '<img class="userpicture defaultuserpic" id="pic_'+authorid+'" width="37" height="37" src="'+M.cfg.wwwroot+'/theme/image.php/monorail/core/'+M.cfg.jsrev+'/u/f2"></div>';
    content += '</a></div><div class="post-header-text"><b>';
    content += '<a href="'+M.cfg.wwwroot+'/a/profile/'+authorid+'"><span id="puser_'+authorid+'"></span></a></b><br></div>';
    if (isnew)
        content += '<div class="forum_notification inline">('+M.str.theme_monorail.newreply+')</div>';
    content += '<div class="commands">';
    content += '</div>';
    content += '</div>';
    content += '<div class="post-content"><div class="posting"><p></p></div>';
    content += '<div class="p'+postid+'attachedimages"></div>';
    content += '<div id="p'+postid+'attachments" class="attachment-list"></div>';
    content += '</div>';
    content += '</div>';
    content += '<div class="post-loading-image">';
    content += '<center><img src="'+M.cfg.wwwroot+'/theme/monorail/pix/loading.gif"></center>';
    content += '</div>';
    if (!celldataonly)
        content += '</td></tr>';
    return content;
}

function mh_forum_commands(postid, courseid) {
    content = '';
    content += '<a href="javascript:void(0)" onclick="monorail_forum_editpost('+postid+');">'+M.str.forum.edit+'</a>';
    content += ' | ';
    content += '<a href="'+M.cfg.wwwroot+'/theme/monorail/forum.php?delete='+postid+'&action=post&c='+courseid+'">'+M.str.forum.delete+'</a>';
    return content;
}

function mh_forum_post_form(action, userId, groupId, forumId, courseId, discussionId, postId, subject) {
    var content = '';
    content += '<form id="mform1" class="mform" accept-charset="utf-8" method="post" action="#" autocomplete="off">';
        content += '<div style="display: none;">';
            content += '<input type="hidden" value="'+subject+'" name="subject">';
            content += '<input type="hidden" value="'+courseId+'" name="course">';
            content += '<input type="hidden" value="'+forumId+'" name="forum">';
            content += '<input type="hidden" value="'+discussionId+'" name="discussion">';
            if (action == 'reply')
                var parent = postId;
            else
                var parent = 0;
            content += '<input type="hidden" value="'+parent+'" name="parent">';
            content += '<input type="hidden" value="'+userId+'" name="userid">';
            content += '<input type="hidden" value="'+groupId+'" name="groupid">';
            if (action == 'edit')
                var edit = postId;
            else
                var edit = 0;
            content += '<input type="hidden" value="'+edit+'" name="edit">';
            if (action == 'reply')
                var reply = postId;
            else
                var reply = 0;
            content += '<input type="hidden" value="'+reply+'" name="reply">';
            content += '<input type="hidden" value="" id="attachfilename" name="attachfilename">';
        content += '</div>';
    if (action == 'new')
        content += '<input id="id_subject" type="text" value="" name="subject" size="48" maxlength="250">';
    content += '<textarea id="id_message" cols="80" rows="15" name="message[text]">';
    content += '</textarea>';
    content += '<div id="fileuploadcontainer" style="display:inline-block;"></div>';
    content += '<input id="id_submitbutton" class="btn btn-primary" style="padding: 4px 14px 4px 14px; margin-right: -13px;" type="submit" value="'+M.str.forum.posttoforum+'" onclick="return false;" name="submitbutton"><input id="id_cancel" style="padding: 4px 14px 4px 14px;" class="btn btn-danger" type="button" onclick="monorail_forum_hidereply()" value="'+M.str.moodle.cancel+'">';
    content += '</form>';
    content += '<div id="errordialog" class="hide"></div>';
    return content;
}

function mh_forum_empty_discussion() {
    var content = '';
    content += '<tr id="forumtable-header"><td><div class="forum-current"><img src="'+M.cfg.wwwroot+'/theme/monorail/pix/loading.gif"></div></td></tr>';
    return content;
}

function mh_forum_discussion_core(discussId) {
    var content = '';
    content += '<div id="discuss_id" class="box hide">'+discussId+'</div>';
    content += '<table class="table-forum"></table>';
    content += '<div id="reply-container">';
        content += '<div id="post-form" class="indiscuss-reply"></div>';
        content += '<span id="replyboxcontainer">';
        content += '</span>';
    content += '</div>';
    return content;
}

function mh_forum_reply_container() {
    var content = '';
    content += '<div id="reply-container">';
        content += '<div id="post-form" class="indiscuss-reply"></div>';
        content += '<span id="replyboxcontainer">';
        content += '</span>';
    content += '</div>';
    return content;
}

function mh_forum_discussion_list_item(discussionId, discussionName, forumId, authorName, selected) {
    var content = '';
    var onclick = 'removeURLHash(); monorail_forum_show_discussion('+discussionId+','+forumId+'); return false;';
    content += '<div class="discussion-header clickable ndiscuss_'+discussionId+' '+((selected)?'selected':'')+'"';
    content += ' onclick="'+onclick+'">';
    content += '<a id="'+discussionId+'" class="discussion-name" onclick="'+onclick+'" href="javascript:void(0)">'+discussionName+'</a>';
    content += '<div>';
    content += '<div id="rcount" class="inline">0</div>';
    content += ' '+M.str.forum.replies;
    content += '<div class="forum_notification hide">';
    content += '<div id="ncount" class="inline">0</div>';
    content += ' '+M.str.theme_monorail.newreply;
    content += '</div>';
    content += ' | '+M.str.forum.startedby+' '+authorName;
    content += '</div>';
    content += '<div class="notifybarleft hide"> </div>';
    content += '</div>';
    if (selected)   // remove old selection
        $('div.discussion-header.selected').removeClass('selected');
    return content;
}

function mh_forum_reply_box(groupId, courseId, discussionId) {
    var content = '';
    content += '<div id="replybox" style="text-align: right;">';
    content += '<input type="button" class="btn pull-right" onclick="monorail_forum_showreply(data.userid, '+groupId+', '+courseId+', '+discussionId+')" value="'+M.str.forum.reply+'">';
    content += '<div class="clear"></div>';
    content += '</div>';
    return content;
}
