/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

/* from http://stackoverflow.com/a/11888430/1489738 */
Date.prototype.stdTimezoneOffset = function() {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}
Date.prototype.dst = function() {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}
/* end from */
Date.prototype.calOffset = function() {
    if (this.dst()) {
        return (this.getTimezoneOffset() + 60) * 60;
    } else {
        return this.getTimezoneOffset() * 60;
    }
}

$(document).ready(function ()
{
    if ($('body').attr('id') == 'page-calendar-view') {
        var EventDialog = function ()
        {
            this.open = function (x, y, data)
            {
                if (!mVisible)
                {
                    mRootNode.appendTo(document.body)
                    mVisible = true
                }

                mRootNode.css({ top: y + "px", left: (x - 138) + "px" })
                mNameGroup.removeClass("error");

                var time = data.time ? data.time : new Date();

                mStart.timepicker("setTime", time);
                mDate = data.date ? data.date : new Date();
                mName.val("name" in data ? data.name : "");
                mLocation.val("description" in data ? data.description : "");
                mDuration.timepicker({ minTime: time, showDuration: true, timeFormat: "H:i", step: 15 });
                mDuration.timepicker("setTime", new Date("duration" in data ? data.duration + time.getTime() : time.getTime() + 3600000));

                mEventType = data.type;

                mLastStartTime = mStart.timepicker("getTime").getTime()

                if ("action" in data && data.action == "new")
                {
                    mDelBtnGroupBtn.css({ display: "none" });
                    mOkBtn.removeClass("mb")
                    mAction = "new"

                    if (mType)
                    {
                        mType.css({ display: "block" });
                        mType.val(0);
                    }

                    mNameGroup.css({ display: "block" });
                }
                else
                {
                    mDelBtnGroupBtn.css({ display: "inline" });
                    mOkBtn.addClass("mb")
                    mAction = "edit"
                    mEventId = data.id

                    if (mType)
                    {
                        mType.css({ display: "none" });
                    }

                    mNameGroup.css({ display: (data.type == "course" ? "none" : "block") });
                }

                mActiveDialog = this
            }

            this.close = function ()
            {
                if (mVisible)
                {
                    mRootNode.detach()
                    mVisible = false
                }

                mActiveDialog = null
            }

            this.isVisible = function ()
            {
                return mVisible
            }

            this.rootNode = function ()
            {
                return mRootNode.get(0)
            }

            var mRootNode = $("<div>").addClass("new_event_dialog").append($("<div>").addClass("ptr")),
                mVisible = false,
                mDate,
                mEventId,
                mAction,
                mLastStartTime,
                mEventType,
                mFormNode = $("<fieldset>").appendTo(mRootNode);

            var mType = null,
                mNameGroup = $("<div>").addClass("control-group").appendTo(mFormNode),
                mName = $("<input>").prop({ type: "text", placeholder: M.cfg.strings["title"], maxlength: 50 }).appendTo(mNameGroup),
                mLocation = $("<input>").prop({ type: "text", placeholder: M.cfg.strings["location"], maxlength: 50 }).appendTo($("<div>").addClass("control-group").appendTo(mFormNode)),
                mStart = $("<input>").css({ width: "180px", "float": "right" }).prop({ type: "text", maxlength: 50 }).appendTo($("<div>").addClass("control-group").appendTo(mFormNode).append($("<label>").addClass("control-label").text(M.cfg.strings["start_time"]))).timepicker({ timeFormat: "H:i", step: 15 }),
                mDuration = $("<input>").css({ width: "180px", "float": "right" }).prop({ type: "text", maxlength: 50 }).appendTo($("<div>").addClass("control-group").appendTo(mFormNode).append($("<label>").addClass("control-label").text(M.cfg.strings["end_time"]))).timepicker({ showDuration: true, timeFormat: "H:i", minTime: new Date(), setp: 15 });

            if (M.cfg.writable_courses.length > 1)
            {
                mType = $("<select>").css({ width: "270px" });

                for (var i in M.cfg.writable_courses)
                {
                    $("<option>").attr("value", M.cfg.writable_courses[i].id).html(M.cfg.writable_courses[i].fullname).appendTo(mType);
                }

                mNameGroup.before($("<div>").addClass("control-group").append(mType));

                mType.change(function ()
                {
                    var course = parseInt(mType.val());

                    mNameGroup.css({ display: (course ? "none" : "block") });
                    mOkBtn.text(course ? M.cfg.strings["button_save2"] : M.cfg.strings["button_save"]);

                    if (course)
                    {
                        for (var i in M.cfg.writable_courses)
                        {
                            if (M.cfg.writable_courses[i].id == course)
                            {
                                mName.val(M.cfg.writable_courses[i].fullname);
                                break;
                            }
                        }
                    }
                });
            }

            mStart.on("changeTime", function ()
            {
                var newStart = mStart.timepicker("getTime"),
                    dur = mDuration.timepicker("getTime").getTime() - mLastStartTime

                mDuration.timepicker({ minTime: newStart, showDuration: true, timeFormat: "H:i", step: 15 })
                mDuration.timepicker("setTime", new Date(dur + newStart.getTime()))

                mLastStartTime = newStart.getTime()
            })

            var mOkBtn = $("<button>").addClass("btn btn-primary floatright").css({ "margin-left": "1em" }).text(M.cfg.strings["button_save"]).appendTo(mRootNode).click(function ()
            {
                if (mName.val() == "")
                {
                    mNameGroup.addClass("error")
                    mName.focus()
                    return
                }

                var frame = $("<iframe>").prop({ name: "fakepost" }).css({ display: "none" }).appendTo(document.body),
                    form = $("<form>").prop({ method: "post", action: "event.php", target: "fakepost" }).css({ display: "none" }).appendTo(document.body),
                    stime = mStart.timepicker("getTime"),
                    endtime = mDuration.timepicker("getTime")

                if (mAction == "edit")
                {
                    $("<input>").prop({ type: "hidden", name: "id" }).val(mEventId).appendTo(form)
                }

                $("<input>").prop({ type: "hidden", name: "userid" }).val(M.cfg.userid).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "action" }).val(mAction).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "sesskey" }).val(M.cfg.sesskey).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "_qf__event_form" }).val(1).appendTo(form)

                if (mAction != "edit")
                {
                    if (mType && mType.val())
                    {
                        $("<input>").prop({ type: "hidden", name: "eventtype" }).val("course").appendTo(form)
                        $("<input>").prop({ type: "hidden", name: "courseid" }).val(mType.val()).appendTo(form)
                    }
                    else
                    {
                        $("<input>").prop({ type: "hidden", name: "eventtype" }).val("user").appendTo(form)
                    }
                }

                $("<input>").prop({ type: "hidden", name: "name" }).val(mName.val()).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "description[text]" }).val(mLocation.val()).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "description[format]" }).val(1).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "description[itemid]" }).val(1).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "timestart[day]" }).val(mDate.getDate()).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "timestart[month]" }).val(mDate.getMonth() + 1).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "timestart[year]" }).val(mDate.getFullYear()).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "timestart[hour]" }).val(stime.getHours()).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "timestart[minute]" }).val(stime.getMinutes()).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "duration" }).val(2).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "timedurationminutes" }).val((endtime.getTime() - stime.getTime()) / 60000).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "submitbutton" }).val("Save changes").appendTo(form)

                form.submit()

                mOkBtn.attr("disabled", "true");

                setTimeout(function () { location.reload() }, 2000 )
            })

            var mDelBtnGroup = $("<div>").addClass("btn-group floatright").appendTo(mRootNode);

            var mCloseBtn = $("<button>").addClass("btn btn-danger").text(M.cfg.strings["button_cancel"]).appendTo(mDelBtnGroup).click(this.close)

            var mDelBtnGroupBtn = $("<button class=\"btn btn-danger dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"caret\"></span></button>").appendTo(mDelBtnGroup);

            var mDeleteBtn = $("<a>").attr({ href: "#" }).text(M.cfg.strings["button_delete"]).appendTo($("<li>").appendTo($("<ul>").addClass("dropdown-menu").appendTo(mDelBtnGroup))).click(function (ev)
            {
                ev.preventDefault();

                if (mEventType == "course")
                {
                    if (!confirm(M.cfg.strings["event_delete"]))
                    {
                        return;
                    }
                }

                var frame = $("<iframe>").prop({ name: "fakepost" }).css({ display: "none" }).appendTo(document.body),
                    form = $("<form>").prop({ method: "post", action: "delete.php", target: "fakepost" }).css({ display: "none" }).appendTo(document.body)

                $("<input>").prop({ type: "hidden", name: "id" }).val(mEventId).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "sesskey" }).val(M.cfg.sesskey).appendTo(form)
                $("<input>").prop({ type: "hidden", name: "confirm" }).val(1).appendTo(form)

                form.submit()

                setTimeout(function () { location.reload() }, 2000)
            })
        }

        var MonthPickerDialog = function ()
        {
            this.open = function (x, y, options)
            {
                if (!mVisible)
                {
                    mVisible = true
                    $(document.body).append(mRootNode)
                }

                mRootNode.css({ left: (x - 138) + "px", top: (y + 10) + "px" })

                if ("href" in options)
                {
                    mCurrentHref = options.href
                }
                else
                {
                    mCurrentHref = location.href
                }

                mActiveDialog = this

                mDatePick.datepicker("setValue", options.date);

                if (options.mode == "month")
                {
                    mShowMonthBtn.hide();
                }
                else
                {
                    mShowMonthBtn.show();
                }
            }

            this.close = function ()
            {
                if (mVisible)
                {
                    mRootNode.detach()
                    mVisible = false
                }

                mActiveDialog = null
            }

            this.isVisible = function ()
            {
                return mVisible
            }

            this.rootNode = function ()
            {
                return mRootNode.get(0)
            }
        
            var mRootNode = $("<div>").addClass("new_event_dialog"),
                mVisible = false,
                mCurrentHref

            var makeLink = function (date)
            {
                return mCurrentHref.replace(
                    /cal_d=(\d+)/, "cal_d=" + date.getDate()).replace(
                    /cal_m=(\d+)/, "cal_m=" + (date.getMonth() + 1)).replace(
                    /cal_y=(\d+)/, "cal_y=" + date.getFullYear())
            };

            $("<div>").addClass("ptr").appendTo(mRootNode);

            var mShowMonthBtn = $("<button>").text(M.cfg.strings["showmonth"]).css({ width: "180px", display: "block", "margin-bottom": "10px" }).addClass("btn").appendTo(mRootNode);

            mShowMonthBtn.click(function ()
            {
                location.href = mCurrentHref.replace(/view=[a-zA-Z]+/, "view=month");
            });

            $("<button>").text(M.cfg.strings["showtoday"]).css({ width: "180px", display: "block", "margin-bottom": "10px" }).addClass("btn").appendTo(mRootNode).click(function ()
            {
                location.href = makeLink(new Date())
            });

            var mDatePick = $("<input>").prop({ type: "text", maxlength: 50 }).css({ width: "100px" }).appendTo(mRootNode);

            mDatePick.datepicker({ format: "dd.mm.yyyy", weekStart: 1 })
                .on("changeDate", function ()
                {
                    mDatePick.datepicker("hide");
                });

            $("<button>").text(M.cfg.strings["cal_go"]).addClass("btn").css({ "float": "right" }).appendTo(mRootNode).click(function ()
            {
                var d = new Date();

                d.setFullYear(mDatePick.val().substr(6));
                d.setMonth(mDatePick.val().substr(3, 2) - 1);
                d.setDate(mDatePick.val().substr(0, 2));

                location.href = makeLink(d);
            });
        }

        var toggleAddMode = function (mode)
        {
            mAddMode = mode instanceof Boolean ? mode : !mAddMode;

            if (mAddMode)
            {
                mEventDialog.close();

                var htmtext = "<span style=\"font-size: 9pt;\">" + M.cfg.strings["select_time"] + "</span>&nbsp;<button class=\"btn btn-danger\">" + M.cfg.strings["button_cancel"] + "</button>&nbsp;&nbsp;";

                $("#calendar_event_add_btn").removeClass("cal_new_icon").html(htmtext).css({ width: "auto" });

                $(".cal_day_cell").bind("click", dayClickHandler).addClass("clickable");
            }
            else
            {
                $("#calendar_event_add_btn").addClass("cal_new_icon").text("").css({ width: "57px" });

                $(".cal_day_cell").unbind("click", dayClickHandler).removeClass("clickable");
            }
        }

        var dayClickHandler = function (e)
        {
            var box = e.target.getBoundingClientRect(),
                date = parseInt($(e.target).attr("data-timestamp")) * 1000,
                lastdate = $(e.target).attr("data-laststamp") * 1000,
                evTime = new Date();

            date -= ((new Date()).getTimezoneOffset() * 60000);

            toggleAddMode(false);

            if (lastdate - date > 43200000)
            {
                evTime.setMinutes(0)
                evTime.setHours(evTime.getHours() + 1)
            }
            else
            {
                var dnum = parseInt($(e.target).attr("data-daynum")) / 12

                evTime.setMinutes(0)
                evTime.setHours(dnum == 0 ? 0 : dnum + 7)
            }

            mEventDialog.open((box.left + window.scrollX) + (box.width / 2), e.pageY,
                { time: evTime,
                  date: new Date(date),
                  name: "",
                  action: "new" })

            e.stopPropagation()
        }

        var eventClickHandler = function (e)
        {
            var stamp = new Date($(this).attr("data-start") + "Z");

            stamp.setTime(stamp.getTime() + stamp.getTimezoneOffset() * 60000);

            mEventDialog.open(e.pageX, e.pageY,
                { name: $(this).attr("data-name"),
                  description: $(this).attr("data-location"),
                  type: $(this).attr("data-type"),
                  time: stamp, date: stamp,
                  duration: parseInt($(this).attr("data-duration")) * 1000,
                  action: "edit",
                  id: $(this).attr("data-id") })

            e.stopPropagation()

            return false
        }

        var calDateClickHandler = function (e)
        {
            var d = new Date(),
                href = $(e.target).attr("data-href")

            d.setFullYear(parseInt(href.match(/cal_y=(\d+)/)[1]))
            d.setMonth(parseInt(href.match(/cal_m=(\d+)/)[1]) - 1)

            mMonthPickDialog.open(e.pageX, e.pageY, { date: d, mode: "month", href: href })

            e.stopPropagation()

            return false
        }

        var calWeekClickHandler = function (e)
        {
            var d = new Date()

            d.setFullYear(parseInt(location.href.match(/cal_y=(\d+)/)[1]))
            d.setMonth(parseInt(location.href.match(/cal_m=(\d+)/)[1]) - 1)
            d.setDate(parseInt(location.href.match(/cal_d=(\d+)/)[1]) - 1)

            mMonthPickDialog.open(e.pageX, e.pageY, { date: d })

            e.stopPropagation()

            return false
        }

        var mAddMode = false,
            mEventDialog = new EventDialog(),
            mMonthPickDialog = new MonthPickerDialog(),
            mActiveDialog = null

        $("#calendar_event_add_btn").click(toggleAddMode)
        $(".cal_event_item").click(eventClickHandler)
        $("#cal_date_header").click(calDateClickHandler)
        $("#cal_week_header").click(calWeekClickHandler)
        $("#back_btn").click(function() {history.back();return false;});

        $(document.body).click(function (e)
        {
            if (mActiveDialog && mActiveDialog.isVisible())
            {
                if (!$(mActiveDialog.rootNode()).has($(e.target)).length &&
                    e.target != mActiveDialog.rootNode())
                {
                    mActiveDialog.close()

                    e.stopPropagation()
                }
            }
        })

        // TODO: fill this with localised names.
        var mMonthNames = ["January", "February", "March", "April", "May",
            "June", "July", "August", "September", "October", "November", "December"]
    }
});
