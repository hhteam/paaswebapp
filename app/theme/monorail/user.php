<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


	require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
	require_once($CFG->dirroot . '/my/lib.php');
	require_once($CFG->dirroot . '/tag/lib.php');
	require_once($CFG->dirroot . '/user/profile/lib.php');
	require_once($CFG->libdir.'/filelib.php');

	$userid 	= optional_param('id', null, PARAM_INT);   	// userid
	$courseid 	= optional_param('course', null, PARAM_INT);

	$param = array('id' => $userid);
	if ($courseid) {
		$param['course'] = $courseid;
	}
	$PAGE->set_url('/theme/monorail/user.php', $param);
	$PAGE->set_pagelayout('profile');

	if (!empty($CFG->forceloginforprofiles)) {
		require_login();
		if (isguestuser()) {
			$SESSION->wantsurl = $PAGE->url->out(false);
			redirect(get_login_url());
		}
	} else if (!empty($CFG->forcelogin)) {
		require_login();
	}

	$userid = $userid ? $userid : $USER->id;
	$user 	= $DB->get_record('user', array('id' => $userid));

	if ($user->deleted) {
		$PAGE->set_context(get_context_instance(CONTEXT_SYSTEM));
		$PAGE->set_heading(get_string('userdeleted'));
		echo $OUTPUT->header();
		notice(get_string('userdeleted'), new moodle_url('/my/'));
		echo $OUTPUT->footer();
		die;
	}

	$currentuser 	= ($user->id == $USER->id);
	$context 		= $usercontext = get_context_instance(CONTEXT_USER, $userid, MUST_EXIST);

	if ($courseid && !$currentuser){
		$course 		= $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
		$coursecontext 	= get_context_instance(CONTEXT_COURSE, $course->id);

		$PAGE->set_course($course);

		$fullname 		= fullname($user, has_capability('moodle/site:viewfullnames', $coursecontext));
		if (empty($_SERVER['HTTP_REFERER'])) {
			$linkback = new moodle_url('/course/view.php?id='.$courseid);
		}
		else {
			$linkback = $_SERVER['HTTP_REFERER'];
		}

		// check course level capabilities
		if (!has_capability('moodle/user:viewdetails', $coursecontext) && // normal enrolled user or mnager
				($user->deleted or !has_capability('moodle/user:viewdetails', $usercontext))) {   // usually parent
			print_error('cannotviewprofile');
		}

		if (!is_enrolled($coursecontext, $user->id)) {
			echo $OUTPUT->header();
			if (has_capability('moodle/role:assign', $coursecontext)) {
				notice(get_string('notenrolled', '', $fullname), $linkback);
			} else {
				notice(get_string('notenrolledprofile'), $linkback);
			}
			echo $OUTPUT->footer();
			die;
		}

		// If groups are in use and enforced throughout the course, then make sure we can meet in at least one course level group
		if (groups_get_course_groupmode($course) == SEPARATEGROUPS and $course->groupmodeforce
				and !has_capability('moodle/site:accessallgroups', $coursecontext) and !has_capability('moodle/site:accessallgroups', $coursecontext, $user->id)) {
			if (!isloggedin() or isguestuser()) {
				// do not use require_login() here because we might have already used require_login($course)
				redirect(get_login_url());
			}
			$mygroups = array_keys(groups_get_all_groups($course->id, $USER->id, $course->defaultgroupingid, 'g.id, g.name'));
			$usergroups = array_keys(groups_get_all_groups($course->id, $user->id, $course->defaultgroupingid, 'g.id, g.name'));
			if (!array_intersect($mygroups, $usergroups)) {
				print_error("groupnotamember", '', "../course/view.php?id=$course->id");
			}
		}
	}
	else {
		if (!$currentuser &&
				!empty($CFG->forceloginforprofiles) &&
				!has_capability('moodle/user:viewdetails', $context) &&
				!has_coursecontact_role($userid)) {

			// Course managers can be browsed at site level. If not forceloginforprofiles, allow access (bug #4366)
			$struser = get_string('user');
			$PAGE->set_context(get_context_instance(CONTEXT_SYSTEM));
			$PAGE->set_title("$SITE->shortname: $struser");  // Do not leak the name
			$PAGE->set_heading($struser);
			$PAGE->set_url('/theme/monorail/user.php', array('id'=>$userid));
			echo $OUTPUT->header();
			notice(get_string('usernotavailable', 'error'), new moodle_url('/my/'));
			echo $OUTPUT->footer();
			exit;
		}
	}

	// Get the profile page.  Should always return something unless the database is broken.
	if (!$currentpage = my_get_page($userid, MY_PAGE_PUBLIC)) {
		print_error('mymoodlesetup');
	}

	if (!$currentpage->userid) {
		$context = get_context_instance(CONTEXT_SYSTEM);  // A trick so that we even see non-sticky blocks
	}

	$PAGE->set_context($context);
	$PAGE->set_pagetype('user-profile');

	// Set up block editing capabilities
	if (isguestuser()) {     // Guests can never edit their profile
		$USER->editing = $edit = 0;  // Just in case
		$PAGE->set_blocks_editing_capability('moodle/my:configsyspages');  // unlikely :)
	} else {
		if ($currentuser) {
			$PAGE->set_blocks_editing_capability('moodle/user:manageownblocks');
		} else {
			$PAGE->set_blocks_editing_capability('moodle/user:manageblocks');
		}
	}

	if (has_capability('moodle/user:viewhiddendetails', $context)) {
		$hiddenfields = array();
	} else {
		$hiddenfields = array_flip(explode(',', $CFG->hiddenuserfields));
	}

	// Start setting up the page
	$strpublicprofile = get_string('publicprofile');

	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title(fullname($user).": $strpublicprofile");
	$PAGE->set_heading(fullname($user));

	if ($currentuser) {
		$PAGE->add_body_class('currentuser');
	}

	echo $OUTPUT->header();
	echo $OUTPUT->box_start('userprofile');


	// Print the standard content of this page, the basic profile info

	if (is_mnet_remote_user($user)) {
		$sql = "SELECT h.id, h.name, h.wwwroot,
                   a.name as application, a.display_name
              FROM {mnet_host} h, {mnet_application} a
             WHERE h.id = ? AND h.applicationid = a.id";

		$remotehost = $DB->get_record_sql($sql, array($user->mnethostid));
		$a = new stdclass();
		$a->remotetype = $remotehost->display_name;
		$a->remotename = $remotehost->name;
		$a->remoteurl  = $remotehost->wwwroot;

		echo $OUTPUT->box(get_string('remoteuserinfo', 'mnet', $a), 'remoteuserinfo');
	}

	// Output user's profile picture
	echo '<div class="userprofilebox clearfix"><div class="profilepicture">';
	echo $OUTPUT->user_picture($user, array('size'=>200, 'link'=>false));
	echo '</div>';

	// Starting output user information
	echo $OUTPUT->box_start('descriptionbox');

	$divstart = "<div class='clickable clickbox'>";
	$divend   = "</div>";
	if ($currentuser) {
		$divstart = '';
		$divend	  = '';
	}

	// Email
	if ($currentuser
			or $user->maildisplay == 1
			or has_capability('moodle/course:useremail', $context)
			or ($user->maildisplay == 2 and enrol_sharing_course($user, $USER))) {
		echo $divstart;
		print_row(get_string("email"), obfuscate_mailto($user->email, ''));
		echo $divend;
	}

	// Instant Messager
	if ($user->icq && !isset($hiddenfields['icqnumber'])) {
		echo $divstart;
		print_row(get_string('icqnumber'),"<a href=\"http://web.icq.com/wwp?uin=".urlencode($user->icq)."\">".s($user->icq)." <img src=\"http://web.icq.com/whitepages/online?icq=".urlencode($user->icq)."&amp;img=5\" alt=\"\" /></a>");
		echo $divend;
	}
	if ($user->skype && !isset($hiddenfields['skypeid'])) {
		echo $divstart;
		print_row(get_string('skypeid'),'<a href="skype:'.urlencode($user->skype).'?call">'.s($user->skype).
				' <img src="http://mystatus.skype.com/smallicon/'.urlencode($user->skype).'" alt="'.get_string('status').'" '.
				' /></a>');
		echo $divend;
	}
	if ($user->yahoo && !isset($hiddenfields['yahooid'])) {
		echo $divstart;
		print_row(get_string('yahooid'), '<a href="http://edit.yahoo.com/config/send_webmesg?.target='.urlencode($user->yahoo).'&amp;.src=pg">'.s($user->yahoo)." <img src=\"http://opi.yahoo.com/online?u=".urlencode($user->yahoo)."&m=g&t=0\" alt=\"\"></a>");
		echo $divend;
	}
	if ($user->aim && !isset($hiddenfields['aimid'])) {
		echo $divstart;
		print_row(get_string('aimid'), '<a href="aim:goim?screenname='.urlencode($user->aim).'">'.s($user->aim).'</a>');
		echo $divend;
	}
	if ($user->msn && !isset($hiddenfields['msnid'])) {
		echo $divstart;
		print_row(get_string('msnid'), s($user->msn));
		echo $divend;
	}

	// City
	if (! isset($hiddenfields['city']) && $user->city) {
		print_row(get_string('city'), $user->city);
	}

	// Country
	if (! isset($hiddenfields['country']) && $user->country) {
		print_row(get_string('country'), get_string($user->country, 'countries'));
	}

	// Hidden details
	if (has_capability('moodle/user:viewhiddendetails', $context)) {
		if ($user->address) {
	        print_row(get_string("address"), "$user->address");
	    }
	    if ($user->phone1) {
	        print_row(get_string("phone"), "$user->phone1");
	    }
	    if ($user->phone2) {
	        print_row(get_string("phone2"), "$user->phone2");
	    }
	}

	// Webpage
	if ($user->url && !isset($hiddenfields['webpage'])) {
		$url = $user->url;
		if (strpos($user->url, '://') === false) {
			$url = 'http://'. $url;
		}
		print_row(get_string("webpage") , '<a href="'.s($url).'">'.s($user->url).'</a>');
	}

	// Printing tagged interests
	if (!empty($CFG->usetags)) {
		if ($interests = tag_get_tags_csv('user', $user->id) ) {
			print_row(get_string('interests'), $interests);
		}
	}

	echo $OUTPUT->box_end(); // description
	echo $OUTPUT->box_end(); // userprofilebox


	echo $OUTPUT->blocks_for_region('content');

	if ($CFG->debugdisplay && debugging('', DEBUG_DEVELOPER) && $currentuser) {  // Show user object
		echo '<br /><br /><hr />';
		echo $OUTPUT->heading('DEBUG MODE:  User session variables');
		print_object($USER);
	}

	echo '</div>';  // userprofile class
	echo $OUTPUT->footer();

	function print_row($name, $value) {
		global $OUTPUT;
		echo $OUTPUT->box($name, 'desc-name');
		echo $OUTPUT->box($value, 'desc-value');
	}
