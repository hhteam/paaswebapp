<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require(dirname(__FILE__).'/../../config.php');
require_once($CFG->dirroot.'/mod/book/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id        = optional_param('id', 0, PARAM_INT);        // Course Module ID
$bid       = optional_param('b', 0, PARAM_INT);         // Book id
$chapterid = optional_param('chapterid', 0, PARAM_INT); // Chapter ID

// =========================================================================
// security checks START - teachers edit; students view
// =========================================================================
if ($id) {
	$cm = get_coursemodule_from_id('book', $id, 0, false, MUST_EXIST);
	$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);
	$book = $DB->get_record('book', array('id'=>$cm->instance), '*', MUST_EXIST);
} else {
	$book = $DB->get_record('book', array('id'=>$bid), '*', MUST_EXIST);
	$cm = get_coursemodule_from_instance('book', $book->id, 0, false, MUST_EXIST);
	$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);
	$id = $cm->id;
}

require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
require_capability('mod/book:read', $context);

$viewhidden = has_capability('mod/book:viewhiddenchapters', $context);

// read chapters
$chapters = book_preload_chapters($book);

// Check chapterid and read chapter data
if ($chapterid == '0') { // Go to first chapter if no given.
	foreach ($chapters as $ch) {
		if (!$ch->hidden) {
			$chapterid = $ch->id;
			break;
		}
	}
}

if (!$chapterid or !$chapter = $DB->get_record('book_chapters', array('id'=>$chapterid, 'bookid'=>$book->id))) {
	print_error('errorchapter', 'mod_book', new moodle_url('/course/view.php', array('id'=>$course->id)));
}

// chapter is hidden for students
if ($chapter->hidden and !$viewhidden) {
	print_error('errorchapter', 'mod_book', new moodle_url('/course/view.php', array('id'=>$course->id)));
}

$PAGE->set_url('/theme/monorail/book.php', array('id'=>$id, 'chapterid'=>$chapterid));


// Unset all page parameters.
unset($id);
unset($bid);
unset($chapterid);

// Security checks END.

add_to_log($course->id, 'book', 'view', 'view.php?id='.$cm->id.'&amp;chapterid='.$chapter->id, $book->id, $cm->id);

// Read standard strings.
$strbooks = get_string('modulenameplural', 'mod_book');
$strbook  = get_string('modulename', 'mod_book');
$strtoc   = get_string('toc', 'mod_book');

// prepare header
$PAGE->set_title($book->name);
$PAGE->set_heading($course->fullname);
$PAGE->set_pagetype('theme-monorail-book');

$chapter->id = 0;
//$toc 		 = book_get_toc($chapters, $chapter, $book, $cm, 0);
$toc		 = '<p style="font-weight:bold;margin-bottom:30px">'.get_string('toc', 'mod_book').'</p>'.generate_toc($chapters, $book, $cm);
$toc		 = "<div class='inner-box'>$toc</div>";

// we are cheating a bit here, viewing the book means user has viewed the whole book
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

// =====================================================
// Book display HTML code
// =====================================================

echo $OUTPUT->header();

$book_content = '';
foreach ($chapters as $ch) {
	$chapter = $DB->get_record('book_chapters', array('id'=>$ch->id, 'bookid'=>$book->id));

	// Skip chapter if hidden
	if ($chapter->hidden) {
		continue;
	}

	if (!$book->customtitles) {
		if (!$chapter->subchapter) {
			$currtitle = book_get_chapter_title($chapter->id, $chapters, $book, $context);
			$book_content.= '<p class="book_chapter_title">'.$currtitle.'</p><a class="anchor" id="chapter'.$chapter->id.'"></a>';
		} else {
			$currsubtitle = book_get_chapter_title($chapter->id, $chapters, $book, $context);
			$book_content.= '<p class="book_chapter_title sub_chapter">'.$currsubtitle.'</p><a class="anchor" id="chapter'.$chapter->id.'"></a>';
		}
	}

	$chaptertext 	 = file_rewrite_pluginfile_urls($chapter->content, 'pluginfile.php', $context->id, 'mod_book', 'chapter', $chapter->id);
	$book_content 	.= format_text($chaptertext, $chapter->contentformat, array('noclean'=>true, 'context'=>$context));
}
$book_content = "<div class='inner-box'>$book_content</div>";

// Book container
echo $OUTPUT->box_start('', 'book-container');
// TOC
echo $OUTPUT->box($toc, '', 'book-toc');
//Content
echo $OUTPUT->box($book_content, '', 'book-content');
echo $OUTPUT->box_end(); // close tag book-container

echo $OUTPUT->footer();

function generate_toc($chapters, $book, $cm) {
    global $USER, $OUTPUT;

    $toc = '';
    $nch = 0;   // Chapter number
    $ns = 0;    // Subchapter number
    $first = 1;

    $context = context_module::instance($cm->id);

    switch ($book->numbering) {
    	case BOOK_NUM_NONE:
    		$toc .= html_writer::start_tag('div', array('class' => 'book_toc_none'));
    		break;
    	case BOOK_NUM_NUMBERS:
    		$toc .= html_writer::start_tag('div', array('class' => 'book_toc_numbered'));
    		break;
    	case BOOK_NUM_BULLETS:
    		$toc .= html_writer::start_tag('div', array('class' => 'book_toc_bullets'));
    		break;
    	case BOOK_NUM_INDENTED:
    		$toc .= html_writer::start_tag('div', array('class' => 'book_toc_indented'));
    		break;
    }

    $toc .= html_writer::start_tag('ul');
    foreach ($chapters as $ch) {
    	$title = trim(format_string($ch->title, true, array('context'=>$context)));
    	if (!$ch->hidden) {
    		if (!$ch->subchapter) {
    			$nch++;
    			$ns = 0;

    			if ($first) {
    				$toc .= html_writer::start_tag('li');
    			} else {
    				$toc .= html_writer::end_tag('ul');
    				$toc .= html_writer::end_tag('li');
    				$toc .= html_writer::start_tag('li');
    			}

    			if ($book->numbering == BOOK_NUM_NUMBERS) {
    				$title = "$nch $title";
    			}
    		} else {
    			$ns++;

    			if ($first) {
    				$toc .= html_writer::start_tag('li');
    				$toc .= html_writer::start_tag('ul');
    				$toc .= html_writer::start_tag('li');
    			} else {
    				$toc .= html_writer::start_tag('li', array('class' => 'sub_chapter'));
    			}

    			if ($book->numbering == BOOK_NUM_NUMBERS) {
    				$title = "$nch.$ns $title";
    			}
    		}
    		$toc .= html_writer::link("#chapter$ch->id", $title, array('title' => s($title)));

    		if (!$ch->subchapter) {
    			$toc .= html_writer::start_tag('ul');
    		} else {
    			$toc .= html_writer::end_tag('li');
    		}

    		$first = 0;
    	}
    }

    $toc .= html_writer::end_tag('ul');
    $toc .= html_writer::end_tag('li');
    $toc .= html_writer::end_tag('ul');
    $toc .= html_writer::end_tag('div');

    $toc = str_replace('<ul></ul>', '', $toc); // Cleanup of invalid structures.

    return $toc;
}
