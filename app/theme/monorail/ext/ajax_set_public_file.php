<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

/* Used to store uploaded files under publicly accessible path. Can be
 * associated with cohort (and course is TODO).
 * */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

$filename = required_param('filename', PARAM_TEXT);                     // File uploaded
$prefix = required_param("prefix", PARAM_TEXT);                         // Prefix to add to file name
$cohortid = optional_param("cohortid", 0, PARAM_INT);                   // Cohort id to associate with

$allGood = true;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
    $allGood = false;
}

$file = $CFG->dataroot.'/temp/files/'.$USER->id.'/'.$filename;

if (!file_exists($file)) {
    $allGood = false;
}

if ($allGood) {
    if ($cohortid) {
        // A cohort file.
        $isAdmin = $DB->get_recordset_sql("SELECT id FROM {monorail_cohort_admin_users} WHERE cohortid=? AND userid=? LIMIT 1",
            array($cohortid, $USER->id));

        if (!empty($isAdmin)) {
            $newName = $CFG->dirroot . '/public_images/org/' . $prefix . $cohortid . '.png';
            $newUrl = $CFG->wwwroot . '/public_images/org/' . $prefix . $cohortid . '.png';

            rename($file, $newName);

            echo $newUrl;
        } else {
            // Must be an admin in cohort to change public files...
        }
    } else {
        // Or else... TODO?
    }
}

if (file_exists($file)) {
    unlink($file);
}
