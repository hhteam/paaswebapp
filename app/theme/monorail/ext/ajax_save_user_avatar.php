<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

$uploadedfile = required_param('uploaded_file', PARAM_FILE);

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$filename = $CFG->dataroot.'/temp/files/' .$USER->id. '/'. $uploadedfile;

require_once($CFG->libdir.'/gdlib.php');
require_once(dirname(__FILE__).'/../lib.php');

$pichash = monorail_process_avatar($USER->id, $filename);

// save in moodle too
// TODO: remove saving to moodle once huxley is patched
$context = get_context_instance(CONTEXT_USER, $USER->id, MUST_EXIST);
$newpicture = (int)process_new_icon($context, 'user', 'icon', 0, $filename);
$result = $DB->set_field('user', 'picture', $newpicture, array('id' => $USER->id));

@unlink($filename);
@unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$filename);

if ($pichash) {
    echo json_encode($pichash);
} else {
    echo "";
}
