<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$file = required_param('file', PARAM_FILE);
$courseid = required_param('course', PARAM_INT);
$filepath = $CFG->dataroot.'/temp/files/' .$USER->id. '/'. $file;
$field1 = "custom_cert_orig";
$field2 = "custom_cert_preview";

if (!is_file($filepath))
{
    $filepath = null;
}

$coursecontext = context_course::instance($courseid);

if (!has_capability('moodle/course:manageactivities', $coursecontext))
{
    // Don't have access to course.
    if ($filepath)
    {
        unlink($filepath);
    }

    exit(0);
}

require_once __DIR__ . "/../../../local/monorailservices/cachelib.php";
wscache_reset_by_dependency("course_info_" . $courseid);

require_once($CFG->libdir.'/filestorage/file_storage.php');
$fs = get_file_storage();

foreach (array($field1, $field2) as $field) {
    $monorailData = $DB->get_records_sql("SELECT value FROM {monorail_data} WHERE itemid=? AND datakey=?", array($courseid, $field . "_fileid"));

    if (!empty($monorailData))
    {
        $monorailData = reset($monorailData);
        $oldPicId = $monorailData->value;

        if ($oldPicId)
        {
            $oldFile = $fs->get_file_by_id($oldPicId);
            $oldFile->delete();

            $DB->delete_records("monorail_data", array("itemid" => $courseid, "datakey" => $field));
            $DB->delete_records("monorail_data", array("itemid" => $courseid, "datakey" => $field . "_fileid"));
        }
    }
}

if ($filepath) {
    // Prepare the file
    list($width,$height) = getimagesize($filepath);

    $newheight = $height;
    $newwidth = $width;

    if ($width != 540) {
        $dw = 540.0 / $width;

        $newwidth = round($width * $dw);
        $newheight = round($height * $dw);
    }

    if ($newwidth != $width || $newheight != $height) {
        $extension = getExtension($filepath);
        if ($extension == "jpg" || $extension == "jpeg" ) {
            $src = imagecreatefromjpeg($filepath);
        } else if($extension == "png") {
            $src = imagecreatefrompng($filepath);
        } else if($extension == "gif") {
            $src = imagecreatefromgif($filepath);
        } else {
            // TODO: return error message
            echo htmlspecialchars(false, ENT_NOQUOTES);
        }

        $tmp = imagecreatetruecolor($newwidth, $newheight);

        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagejpeg($tmp, $filepath . ".jpg", 80);
        imagedestroy($src);
        imagedestroy($tmp);
    } else {
        copy($filepath, $filepath . ".jpg");
    }

    $fileinfo1 = array(
        'contextid' => $coursecontext->id,
        'component' => "course",
        'filearea' => "summary",
        'itemid' => 0,
        'filepath' => '/',
        'filename' => md5(time() . $field1) . "_" . $file
    );

    $fileinfo2 = array(
        'contextid' => $coursecontext->id,
        'component' => "course",
        'filearea' => "summary",
        'itemid' => 0,
        'filepath' => '/',
        'filename' => md5(time() . $field2) . "_" . $file . ".jpg"
    );

    $file1 = $fs->create_file_from_pathname($fileinfo1, $filepath);
    $file2 = $fs->create_file_from_pathname($fileinfo2, $filepath . ".jpg");

    // Insert new data in monorail data.
    $rec = new stdClass();
    $rec->itemid = $courseid;
    $rec->type = "INTEGER";
    $rec->datakey = $field1 . "_fileid";
    $rec->value = $file1->get_id();
    $DB->insert_record("monorail_data", $rec);

    $rec = new stdClass();
    $rec->itemid = $courseid;
    $rec->type = "INTEGER";
    $rec->datakey = $field2 . "_fileid";
    $rec->value = $file2->get_id();
    $DB->insert_record("monorail_data", $rec);

    @unlink($filepath);
    @unlink($filepath . ".jpg");

    echo json_encode(array(
        "id" => $file1->get_id(),
        "original" => "{$CFG->wwwroot}/pluginfile.php/{$file1->get_contextid()}/{$file1->get_component()}/{$file1->get_filearea()}/{$file1->get_filename()}",
        "thumbnail" => "{$CFG->wwwroot}/pluginfile.php/{$file2->get_contextid()}/{$file2->get_component()}/{$file2->get_filearea()}/{$file2->get_filename()}"
    ));
}

function getExtension($str) {
    $i = strrpos($str,".");
    if (!$i) { return ""; }

    $l = strlen($str) - $i;
    $ext = strtolower(substr($str,$i+1,$l));
    return $ext;
}
