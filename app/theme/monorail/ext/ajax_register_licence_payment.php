<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

// WARNING: this is used by Magento to register payments for licences and shouldn't
// be used (or be accessible) by anything else.
// NOTE: this is obsolete (new spec)


require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $DB, $CFG;

if (!@in_array($_SERVER["REMOTE_ADDR"], $CFG->mage_ips))
{
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 Not Found</h1>";
    exit(0);
}

try
{
    $userId = required_param('user', PARAM_INT);
    $licenceCount = required_param('count', PARAM_INT);

    // TODO: save payment details somewhere?
    $details = required_param("details", PARAM_TEXT);

    $transaction = $DB->start_delegated_transaction();

    $cohortid = $DB->get_field("monorail_cohort_admin_users", "cohortid", array("userid" =>$userId));
    $info = $DB->get_record("monorail_cohort_info", array("cohortid" => $cohortid), "*", MUST_EXIST);

    $info->userscount = $licenceCount;

    // now + 365 days
    $info->valid_until = time() + 31536000;
    $info->is_paid = 1;

    $DB->update_record("monorail_cohort_info", $info);

    $transaction->allow_commit();

    echo "OK";
}
catch (Exception $err)
{
    echo "FAIL";
}
