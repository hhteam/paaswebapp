<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


define('BG_WIDTH', 845);
define('BG_HEIGHT', 170);

define('THUMBNAIL_WIDTH', 267);
define('THUMBNAIL_HEIGHT', 150);

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

$uploadedfile = required_param('filename', PARAM_FILE);
$course_id = required_param('course', PARAM_INT);
$top = required_param('top', PARAM_INT);

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/filestorage/file_storage.php');

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$filename = $CFG->dataroot.'/temp/files/' .$USER->id. '/'. $uploadedfile;
$newFilename = $CFG->dirroot . '/public_images/course_logo/bg' . $course_id . '.jpg';
$newTnFilename = $CFG->dirroot . '/public_images/course_logo/bg' . $course_id . '-tn.jpg';

$image = new Imagick($filename);

$image->scaleImage(BG_WIDTH, 0);
$image->cropImage(BG_WIDTH, BG_HEIGHT, 0, $top);
$image->setCompression(Imagick::COMPRESSION_JPEG);
$image->setCompressionQuality(95);
$image->writeImage($newFilename);

$tnimage = new Imagick($filename);
$tnimage->cropThumbnailImage( THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT );
$tnimage->setCompression(Imagick::COMPRESSION_JPEG);
$tnimage->setCompressionQuality(95);
$tnimage->writeImage($newTnFilename);

@unlink($filename);
@unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$uploadedfile);

$url = $CFG->wwwroot . '/public_images/course_logo/bg' . $course_id . '.jpg';

echo htmlspecialchars(json_encode($url), ENT_NOQUOTES);
