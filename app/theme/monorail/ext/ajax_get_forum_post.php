
<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

try {
    $i = required_param('i', PARAM_INT);
} catch (Exception $ex) {
    die();
}

// get data
try {
    $post = $DB->get_record('forum_posts', array('id' => $i), '*', MUST_EXIST);
    $discussion = $DB->get_record('forum_discussions', array('id' => $post->discussion), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
    $forum = $DB->get_record('forum', array('id' => $discussion->forum), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('forum', $forum->id, $course->id, false, MUST_EXIST);
} catch (Exception $ex) {
    die();
}

// check access
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}
$modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
try {
    require_capability('mod/forum:viewdiscussion', $modcontext, NULL, true, 'noviewdiscussionspermission', 'forum');
} catch (Exception $ex) {
    die();
}

// get post and return
try {
    $post = $DB->get_record('forum_posts', array('id' => $i), '*', MUST_EXIST);
    $date = userdate($post->modified);

    if ($USER->id == $post->userid || has_capability('mod/forum:editanypost', $modcontext))
        $canedit = true;
    else
        $canedit = false;
    if ($canedit && !($forum->type == 'news' && !$post->parent && $discussion->timestart > time())) {
        if (((time() - $post->created) > $CFG->maxeditingtime) and
                    !has_capability('mod/forum:editanypost', $modcontext)) {
            $canedit = false;
        }
    }

    $return = array('i'=>$i, 'm'=>$post->message, 't'=>$date, 'a'=>$post->attachment, 'e'=>(($canedit)?1:0), 'c'=>$course->id);

    echo json_encode($return);
} catch (Exception $ex) {
    die();
}


?>
