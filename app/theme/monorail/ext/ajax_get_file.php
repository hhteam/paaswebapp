<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

$filename = required_param('filename', PARAM_TEXT);

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$file = $CFG->dataroot.'/temp/files/'.$USER->id.'/'.$filename;

if (file_exists($file))
{
    header('Content-Type: ' . mime_content_type($file));
    header('Content-Disposition: inline; filename='.basename($file));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
}
else
{
    header("HTTP/1.0 404 Not Found");
}
