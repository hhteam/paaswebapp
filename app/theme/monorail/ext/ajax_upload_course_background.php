<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG, $PAGE;

$filename = required_param('qqfile', PARAM_FILE);
$categoryid = required_param('categoryid', PARAM_INT);

$allowedExtensions = array();
$sizeLimit = 2 * 1024 * 1024;
//$sizeLimit = (int)(ini_get('upload_max_filesize'));

require(dirname(__FILE__).'/../file-uploader.php');

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/filestorage/file_storage.php');

$attachfilehash = 'coursebackground';

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
$result = $uploader->handleUpload($attachfilehash, $CFG->dataroot.'/temp/files/');

add_to_log(1, 'theme_monorail', 'ext/ajax_upload_course_background', '', 'User '.$USER->id.' uploaded file to temp folder, '.$filename.' hash '.$attachfilehash);

if ($result['success']) {

	// Insert into database to get itemid
	$record = new stdClass();
	$record->categoryid = $categoryid;
	$record->timemodified = time();
	$id = $DB->insert_record('monorail_course_background', $record, true);

	$context = context_course::instance(SITEID);

	$fs = get_file_storage();

	// Prepare file record object
	$fileinfo = array(
			'contextid' => $context->id, // ID of context
			'component' => 'course',     // usually = table name
			'filearea' => 'summary',     // usually = table name
			'itemid' => 0,
			'filepath' => '/',           // any path beginning and ending in /
			'filename' => $id); // any filename

	$path = $CFG->dataroot.'/temp/files/'.$attachfilehash.'-'.$filename;
	$file = $fs->create_file_from_pathname($fileinfo, $path);

	//$temp = $fs->get_file($context->id, 'course', 'summary', 0, '/0/', $filename);

	// Update databse
	$record->id = $id;
	$record->filename = $file->get_id();
	$DB->update_record('monorail_course_background', $record);

	$url = moodle_url::make_pluginfile_url($context->id, 'course', 'summary', null, '/', $id);

	unlink($path);

	$result['url'] = $url->out(false);

	echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
}
else {
	echo htmlspecialchars(json_encode(false), ENT_NOQUOTES);
}
