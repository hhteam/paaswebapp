<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


define('THUMBNAIL_WIDTH', 267);
define('THUMBNAIL_HEIGHT', 150);

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

$uploadedfile = required_param('filename', PARAM_FILE);
$course_id = required_param('course', PARAM_INT);

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/filestorage/file_storage.php');

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

if(!$course_id) {
  die();
}

if(strcmp($uploadedfile,'default') == 0) {
  try {
    $DB->delete_records('monorail_data', array('type'=>'TEXT', 'itemid'=>$course_id, 'datakey'=>'coursebackgroundtncustom'));
  } catch (Exception $e) {
    // Do nothing
  }
  @unlink($CFG->dataroot. '/public_images/course_logo/bg' . $course_id . '-tncustom.jpg');
  $url = $CFG->wwwroot . '/public_images/course_logo/bg' . $course_id . '-tn.jpg';
  echo htmlspecialchars(json_encode($url), ENT_NOQUOTES);
  die();
}
$filename = $CFG->dataroot.'/temp/files/' .$USER->id. '/'. $uploadedfile;
$newTnFilename = $CFG->dirroot . '/public_images/course_logo/bg' . $course_id . '-tncustom.jpg';

$tnimage = new Imagick($filename);
$tnimage->cropThumbnailImage( THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT );
$tnimage->setCompression(Imagick::COMPRESSION_JPEG);
$tnimage->setCompressionQuality(95);
$tnimage->writeImage($newTnFilename);

try {
    $DB->delete_records('monorail_data', array('type'=>'TEXT', 'itemid'=>$course_id, 'datakey'=>'coursebackgroundtncustom'));
    $data = new stdClass();
    $data->type             = 'TEXT';
    $data->itemid           = $course_id;
    $data->datakey          = 'coursebackgroundtncustom';
    $data->value            = '/public_images/course_logo/bg' . $course_id . '-tncustom.jpg';
    $data->timemodified = time();
    $result = $DB->insert_record('monorail_data', $data);
} catch (Exception $e) {
    // Do nothing
}


@unlink($filename);
@unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$uploadedfile);

$url = $CFG->wwwroot . '/public_images/course_logo/bg' . $course_id . '-tncustom.jpg';

echo htmlspecialchars(json_encode($url), ENT_NOQUOTES);
