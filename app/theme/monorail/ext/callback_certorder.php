<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/* Handle call-backs from metida (update certificate order status)
 * */

define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');
define('THRIFT_PATH', __DIR__ . "/../");

global $DB, $CFG;

$orders = array();

if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST["id"])) {
    $data = json_decode(file_get_contents("php://input"));

    foreach ($data["ORDER"] as $ord) {
        $order = $DB->get_record_sql("SELECT id, status, orderid FROM {monorail_cert_order} WHERE orderid=?",
            array($ord));

        if ($order) {
            $orders[] = $order;
        }
    }
} else {
    $order = $DB->get_record_sql("SELECT id, status, orderid FROM {monorail_cert_order} WHERE orderid=?", array($_REQUEST["id"]));

    if ($order) {
        $orders[] = $order;
    }
}

if (empty($orders)) {
    echo "OK";
    exit(0);
}

require_once THRIFT_PATH . '/Thrift/ClassLoader/ThriftClassLoader.php';

$classLoader = new Thrift\ClassLoader\ThriftClassLoader();
$classLoader->registerNamespace('Thrift', THRIFT_PATH);
$classLoader->register();

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TSocket;
use Thrift\Transport\TSSLSocket;
use Thrift\Transport\THttpClient;
use Thrift\Transport\TBufferedTransport;
use Thrift\Exception\TException;

// (!) include after classLoader
require_once __DIR__ . '/../OrderManager/Metida/OrderManager.php';
require_once __DIR__ . '/../OrderManager/Metida/Types.php';

use Metida\OrderManagerClient;
use Metida\OrderStatus;

try
{
    $socket = new TSSLSocket($CFG->mprint_host, 443, FALSE, null, array('certfile' => realpath(__DIR__ . '/../OrderManager/cacert.pem')));

    $timeout = 10; // in seconds.
    $socket->setRecvTimeout($timeout*1000);
    $socket->setSendTimeout($timeout*1000);

    // Buffering is critical. Raw sockets are very slow
    $transport = new TBufferedTransport($socket, 1024, 1024);

    // Wrap in a protocol
    $protocol = new TBinaryProtocol($transport);

    //Create a client to use the protocol encoder
    $client = new OrderManagerClient($protocol);

    // Connect!
    $transport->open();

    // Get auth token
    $authToken = $client->getAuthToken($CFG->mprint_user, $CFG->mprint_password);

    foreach ($orders as $order)
    {
        $orderStatus = $client->getOrderDetails($authToken, $order->orderid);

        if (empty($orderStatus))
        {
            echo "NOK";
            exit(0);
        }

        /* 0 - created, 1 - paid, 2 - placed, 3 - processing, 4 - shipped, 5 - completed */
        $newStatus = $order->status;

        switch ($orderStatus[0]->status)
        {
            case OrderStatus::RECEIVED:
            case OrderStatus::PAUSED:
                $newStatus = 2;
                break;

            case OrderStatus::PROCESSING:
            case OrderStatus::MANUFACTURED:
                $newStatus = 3;
                break;

            case OrderStatus::SHIPPED:
                $newStatus = 4;
                break;

            case OrderStatus::COMPLETED:
                $newStatus = 5;
                break;
        }

        if ($order->status != $newStatus)
        {
            $DB->execute("UPDATE {monorail_cert_order} SET status=? WHERE id=?",
                array($newStatus, $order->id));
        }
    }

    echo "OK";
}
catch (Exception $err)
{
    error_log(var_export($err, true));
    echo "NOK";
}
