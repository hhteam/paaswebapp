<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $USER, $DB, $CFG;

// check login
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

try
{
    $courses = enrol_get_users_courses($USER->id, false, "id");
    $data = array();

    foreach ($courses as $course)
    {
        $code = $DB->get_field('monorail_course_data', 'code', array('courseid' => $course->id));

        if (!$code)
        {
            continue;
        }

        $context = context_course::instance($course->id);
        $roles = get_user_roles($context, $USER->id);
        $canedit = false;

        foreach ($roles as $role)
        {
            if ($role->shortname === 'teacher' || $role->shortname === 'manager' || $role->shortname == "editingteacher")
            {
                $canedit = true;
                break;
            }
        }

        $data[$code] = array("canedit" => $canedit);
    }

    $cohortMember = 1;
    $cohortName = "";

    $auser = $DB->get_records_sql("SELECT id, userid, cohortid FROM {monorail_cohort_admin_users} WHERE userid=? LIMIT 1", array($USER->id));

    if (empty($auser))
    {
        $cuser = $DB->get_records_sql("SELECT userid FROM {cohort_members} WHERE userid=? LIMIT 1", array($USER->id));

        if (empty($cuser))
        {
            $cohortMember = 0;
        }
    }
    else
    {
        $auser = reset($auser);

        $cohortName = $DB->get_field_sql("SELECT name FROM {cohort} WHERE id=?", array($auser->cohortid));
    }

    $pic = $CFG->wwwroot . '/public_images/user_picture/' . monorail_get_userpic_hash($USER->id) . '_32.jpg';
    $name = fullname($USER);

    $return = array('i'=>$USER->id, 'p'=>$pic, 'n'=>$name, 'c'=>$cohortMember,
        'country' => $DB->get_field('user', 'country', array('id' => $USER->id)),
        "org" => $cohortName);

    $paidCourses = array();

    $paid = $DB->get_records_sql("SELECT d.code AS code FROM {monorail_course_data} AS d " .
        "INNER JOIN {monorail_course_payments} AS p ON p.courseid=d.courseid " .
            "WHERE p.userid=? AND p.active", array($USER->id));

    foreach ($paid as $pc)
    {
        $paidCourses[$pc->code] = 1;
    }

    $address = monorail_data("ADDRESS", $USER->id, "user_address");

    if (!empty($address))
    {
        $address = reset($address);
        $address = json_decode($address->value);
    }
    else
    {
        $address = null;
    }

    echo json_encode(array(
        "courses" => $data,
        "user" => $return,
        "paidCourses" => $paidCourses,
        "address" => $address));
}
catch (Exception $ex)
{
    die();
}
