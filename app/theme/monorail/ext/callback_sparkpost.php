<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/* Handle call-backs from sparkpost (update email status)
 * */

define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $DB, $CFG;

$data = json_decode(file_get_contents("php://input"));

foreach ($data as $item) {
    if (!$item->msys->message_event->rcpt_meta || !$item->msys->message_event->rcpt_meta->invite_id) {
        // No invite id.
        continue;
    }

    $invite = $DB->get_record_sql("SELECT id, email FROM {monorail_invite_users} WHERE id=?",
        array($item->msys->message_event->rcpt_meta->invite_id));

    if (!$invite || strtolower($invite->email) != strtolower($item->msys->message_event->rcpt_to)) {
        // No invite record/wrong address
        continue;
    }

    $DB->execute("UPDATE {monorail_invite_users} SET status=? WHERE id=?",
        array($item->msys->message_event->type == "delivery" ? 3 : 4, $invite->id));
}
