<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');
require_once('../evernote/functions.php');

global $USER, $DB, $SESSION;

try {
    $i = required_param('i', PARAM_TEXT);
} catch (Exception $ex) {
    die();
}

// check access
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	die();
}

// get data
try {
	$note = notes_fetch_note_by_id($i);
} catch (Exception $ex) {
	die();
}
if ($note) {
	$return = array(
			'id' 		=> $note->id,
			'name' 		=> $note->name,
			'content' 	=> $note->content
			);
	echo json_encode($return);
}
?>