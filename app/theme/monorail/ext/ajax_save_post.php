<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

try {
    $discussionid = optional_param('discussion', 0, PARAM_INT);
    $message = required_param_array('message', PARAM_RAW);
    $subject = optional_param('subject', '', PARAM_RAW);
    $forumid = optional_param('forum', 0, PARAM_INT);
    $groupid = optional_param('groupid', null, PARAM_INT);
    $reply = optional_param('reply', null, PARAM_INT);
    $edit = optional_param('edit', null, PARAM_INT);
    $attachfilename = optional_param('attachfilename', null, PARAM_FILE);
} catch (Exception $ex) {
    die();
}

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

require_once('../lib.php');

// try to post as logged in user
// no audit from here, it happens in the next method call
try {
    $postid = monorail_forum_save_post($discussionid, $subject, $message, $reply, $forumid, $groupid, $edit);
} catch (Exception $ex) {
    //~ echo $ex->getMessage();
    //~ echo $ex->getTraceAsString();
    die();
}

$post = $DB->get_record('forum_posts', array('id'=>$postid));
$discussion = $DB->get_record('forum_discussions', array('id'=>$post->discussion));
$user = $DB->get_record('user', array('id'=>$post->userid));

if (isset($attachfilename) && strlen($attachfilename) > 0) {
    // context
    $cm = get_coursemodule_from_instance('forum', $discussion->forum, $discussion->course, false, MUST_EXIST);
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
    // save attachment
    $fs = get_file_storage();
    
    $user = $DB->get_record('user', array('id' => $post->userid));
    
    // Check if this post already has an attachment, if yes, delete it
    $files = $fs->get_area_files($context->id, 'mod_forum', 'attachment', $post->id);
    foreach ($files as $file) {
        $file->delete();
    }
    
    // Prepare file record object
    $fileinfo = array(
        'contextid' => $context->id, // ID of context
        'component' => 'mod_forum',     // usually = table name
        'filearea' => 'attachment',     // usually = table name
        'itemid' => $post->id,               // usually = ID of row in table
        'userid' => $post->userid,
        'source' => $attachfilename,
        'author' => fullname($user),
        'license' => 'allrightsreserved',
        'filepath' => '/',           // any path beginning and ending in /
        'filename' => $attachfilename); // any filename
        
    $path = $CFG->dataroot.'/temp/files/'.$USER->id.'/'.$attachfilename;
    $file = $fs->create_file_from_pathname($fileinfo, $path);
    
    monorail_trigger_document_conversion($file, $path);
    
    // update post
    $post->attachment = 1;
    $DB->update_record('forum_posts', $post);
    
    // remove temp files
    unlink($path);
    unlink($CFG->dataroot.'/temp/files/'.$USER->id.'/thumbnail/'.$attachfilename);
}

$postdata = array('i'=>$post->id, 'u'=>$post->userid, 'c'=>$discussion->course, 'n'=>$discussion->name, 'd'=>$discussion->id, 'f'=>$discussion->forum, 'a'=>fullname($user));

echo json_encode($postdata);
