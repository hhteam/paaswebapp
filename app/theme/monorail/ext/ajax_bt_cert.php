<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');
define('THRIFT_PATH', __DIR__ . "/../");

global $USER, $SESSION, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$nonce = required_param('nonce', PARAM_TEXT);
$amount = required_param("amount", PARAM_FLOAT);
$tax = required_param("tax", PARAM_FLOAT);
$quantity = required_param("quantity", PARAM_INT);
$courseid = required_param("courseid", PARAM_INT);

header('Content-Type: application/json');

$order = $DB->get_record_sql("SELECT id, userid, courseid, status, address, created FROM {monorail_cert_order} " .
    "WHERE userid=? AND courseid=? AND status=0 ORDER BY created DESC LIMIT 1", array($USER->id, $courseid));

if (!$order) {
    error_log("CERTIFICATE ORDER ERROR: No order placed for user " . $USER->id . ", course " . $courseid);

    echo json_encode(array("state" => "error"));
    exit(0);
}

$cert_hashurl = $DB->get_field_sql("SELECT hashurl FROM {monorail_certificate} " .
    "WHERE userid=? AND courseid=?", array($USER->id, $courseid));

if (!$cert_hashurl) {
    // Certificate doesn't exist?
    error_log("CERTIFICATE ORDER ERROR: Certificate not issued! " . $USER->id . ", course " . $courseid);

    echo json_encode(array("state" => "error"));
    exit(0);
}

if ($quantity < 1 || $quantity > 100 || (($CFG->cert_price - 1) * $quantity) > $amount) {
    // Never trust a client.
    error_log("CERTIFICATE ORDER ERROR: client-server price mismatch! " . $USER->id . ", course " . $courseid);

    echo json_encode(array("state" => "error"));
    exit(0);
}

require_once __DIR__ . "/../braintree/lib/Braintree.php";

Braintree_Configuration::environment($CFG->braintree_environment);
Braintree_Configuration::merchantId($CFG->braintree_merchantId);
Braintree_Configuration::publicKey($CFG->braintree_publicKey);
Braintree_Configuration::privateKey($CFG->braintree_privateKey);

$bt_userid = str_replace(".", "_", $CFG->my_host) . "-" . $USER->id;
$pm_token = null;

require_once THRIFT_PATH . '/Thrift/ClassLoader/ThriftClassLoader.php';

$classLoader = new Thrift\ClassLoader\ThriftClassLoader();
$classLoader->registerNamespace('Thrift', THRIFT_PATH);
$classLoader->register();

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TSocket;
use Thrift\Transport\TSSLSocket;
use Thrift\Transport\THttpClient;
use Thrift\Transport\TBufferedTransport;
use Thrift\Exception\TException;

// (!) include after classLoader
require_once __DIR__ . '/../OrderManager/Metida/OrderManager.php';
require_once __DIR__ . '/../OrderManager/Metida/Types.php';

use Metida\AccessDenied;
use Metida\GeneralError;
use Metida\OrderError;
use Metida\AuthTokenExpired;
use Metida\OrderManagerClient;
use Metida\Person;
use Metida\Address;
use Metida\ProductData;
use Metida\ShipmentData;
use Metida\OrderMiscDetails;
use Metida\DeliveryMode;
use Metida\PackagingMode;

try {
    $customer = Braintree_Customer::find($bt_userid);
} catch (Braintree_Exception_NotFound $no) {
    $result = Braintree_Customer::create(array(
        'id' => $bt_userid,
        'firstName' => $USER->firstname,
        'lastName' => $USER->lastname,
        'email' => $USER->email
    ));
}

$result = Braintree_PaymentMethod::create(array(
    'customerId' => $bt_userid,
    'paymentMethodNonce' => $nonce,
    'options' => array (
        'makeDefault' => true,
        'failOnDuplicatePaymentMethod' => false
    )
));

$paymentMethod = $result->paymentMethod;

try {
    $details = json_decode($order->address, true);

    $result = Braintree_Transaction::sale(array(
      'amount' => $amount,
      'taxAmount' => $tax,
      'customerId' => $bt_userid,
      'paymentMethodToken' => $paymentMethod->token,
      'options' => array(
        'submitForSettlement' => true
      ),
      'deviceData' => $_POST["device_data"],
      'customFields' => array(
            'itemcode' => 'certificate',
            'country' => $details["country"],
            'course' => $courseid,
            'user' => $USER->id
      )
    ));

    if ($result->success) {
        // Payment successful, mark order as paid.
        $DB->execute("UPDATE {monorail_cert_order} SET status=1 WHERE id=?", array($order->id));

        $DB->execute("UPDATE {monorail_cert_order} SET quantity=?, unitprice=?, totaltax=?, totalprice=? WHERE id=?",
            array($quantity, ($amount - $tax) / $quantity, $tax, $amount, $order->id));

        $orderId = place_cert_order($cert_hashurl, $details, $quantity);

        if ($orderId) {
            // Order successful, save id, mark as ordered.
            $DB->execute("UPDATE {monorail_cert_order} SET status=2, orderid=? WHERE id=?", array($orderId, $order->id));
        }

        // Send receipt.
        require_once __DIR__ . "/../lib.php";
        $subject = "Thank you for your purchase";

        $body = monorail_template_compile(monorail_get_template('mail/base'), array(
           'title' => $subject,

           'body' => monorail_template_compile(monorail_get_template("mail/certinvoice"),
                array(
                    "COURSENAME" => $DB->get_field_sql("SELECT fullname FROM {course} WHERE id=?", array($courseid)),
                    "QTY" => $quantity,
                    "SUBTOTAL" => number_format($amount - $tax, 2),
                    "VAT" => number_format($tax, 2),
                    "TOTAL" => number_format($amount, 2),
                    "ADDRESS" => $details["address"] . ", " . $details["city"] . ", " . $details["zip"] . " " . $details["state"] . " " . $details["country"]
                )),

           'footer_additional_link' => '',
           'block_header' => '',
           "username" => $DB->get_field_sql("SELECT firstname FROM {user} WHERE id=?", array($USER->id))
        ), "EN");

        monorail_send_email('receipt', $subject, $body, $USER->id, "EN");

        mail("certificates@eliademy.com", "New certificate order",
            "Certificate order has been created for user " . $USER->id . ".\nOrder id: " . $orderId .
            "\nCertificate url: https://eliademy.com/cert/" . $cert_hashurl . ".pdf" .
            "\n\nAddress: " . $details["address"] . ", " . $details["city"] . ", " . $details["zip"] . " " . $details["state"] . " " . $details["country"] .
            "\nQuantity: " . $quantity . ". Total paid: " . number_format($amount, 2));

        echo json_encode(array("state" => "success"));
    } else {
        mail("aurelijus@cloudberrytec.com", "Certificate payment error!", var_export($result, true));
        error_log(var_export($result, true));
        echo json_encode(array("status" => "error"));
    }
} catch (Exception $err) {
    mail("aurelijus@cloudberrytec.com", "Error while processing certificate payment!", var_export($err, true));
    error_log(var_export($err, true));
    echo json_encode(array("state" => "error"));
}

function place_cert_order($cert_hashurl, $details, $qty)
{
    global $CFG, $USER, $courseid;

    try
    {
        $socket = new TSSLSocket($CFG->mprint_host, 443, FALSE, null, array('certfile' => realpath(__DIR__ . '/../OrderManager/cacert.pem')));

        $timeout = 10; // in seconds.
        $socket->setRecvTimeout($timeout*1000);
        $socket->setSendTimeout($timeout*1000);

        // Buffering is critical. Raw sockets are very slow
        $transport = new TBufferedTransport($socket, 1024, 1024);

        // Wrap in a protocol
        $protocol = new TBinaryProtocol($transport);

        //Create a client to use the protocol encoder
        $client = new OrderManagerClient($protocol);

        // Connect!
        $transport->open();

        // Get auth token
        $authToken = $client->getAuthToken($CFG->mprint_user, $CFG->mprint_password);

        $person = new Person(array('name' => $details["name"]));

        $addr = new Address(array('to' => $person,
                    'city' => $details["city"],
                    'state' => $details["state"],
                    'addressLine1' => $details["address"],
                    'zip' => $details["zip"],
                    'cc' => $details["country"]));

        $shipment = new ShipmentData(array('address' => $addr,
                         'deliveryMode' => DeliveryMode::ECONOMY,
                         'packagingMode' => PackagingMode::ENVELOPE));

        $product = new ProductData(array('productCode' => 'LILJA-VITT-120',
                       'qty' => $qty,
                       'url' => 'https://eliademy.com/cert/' . $cert_hashurl . '.pdf',
                       'md5' => md5_file($CFG->external_data_root . "/../../cert/" . $cert_hashurl . ".pdf")));
                       // TESTING...
                       //'url' => "https://eliademy.com/cert/4d5a83700f65b9c159c9b08ca148e1b5.pdf",
                       //'md5' => "3b8ad83540e8ab1fe06daca748ec6b62"));

        $misc = new OrderMiscDetails(array('comment' => "User: " . $USER->id . ", Course: " . $courseid));

        return $client->newOrder($authToken, $shipment, array($product), $misc);
    }
    catch (Exception $ex)
    {
        error_log("FAILED TO PLACE CERTIFICATE ORDER: " . var_export($ex, true));

        mail("aurelijus@cloudberrytec.com,support@cloudberrytec.com",
             "FAILED TO PLACE CERTIFICATE ORDER! ERROR!",
             "Certificate: https://eliademy.com/cert/" . $cert_hashurl . ".html\n\nError:\n" . var_export($ex, true));

        return null;
    }
}
