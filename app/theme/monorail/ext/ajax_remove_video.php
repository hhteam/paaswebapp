<?php

// Upload a file to the KMC
require_once(dirname(__FILE__) . '/../../../config.php');


global $CFG, $USER, $PAGE, $DB;

// verify that user is logged in
try {
  require_login(null, false, null, false, true);
} catch (Exception $ex) {
  // not logged in, just die
  die();
}


$entryid = required_param('entryid', PARAM_TEXT);

require_once($CFG->dirroot . '/local/kaltura/locallib.php');
error_reporting(0);
$kclient = local_kaltura_login(true);

if (!$kclient) {
  $vfdata = new stdClass();
  $vfdata->entryid  = $entryid;
  $DB->insert_record('monorail_course_delvideorsc', $vfdata);
  die("Could not establish K session.");
}


try {
  add_to_log(1, 'monorail', 'ext.ajax_delete_video', '', 'Deleting task video rsc :'.$entryid);
  $resp = $kclient->media->delete($entryid);
} catch (Exception $ex) {
  //Maybe failed to delete
  $vfdata = new stdClass();
  $vfdata->entryid  = $entryid;
  $DB->insert_record('monorail_course_delvideorsc', $vfdata);
  add_to_log(1, 'monorail', 'externallib.del_course', '', 'ERROR: '.$ex->getMessage());
}

echo '[]';
?>
