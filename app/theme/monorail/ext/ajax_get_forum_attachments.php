
<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG, $OUTPUT;

try {
    $i = required_param('i', PARAM_INT);
} catch (Exception $ex) {
    die();
}

// get data
try {
    $post = $DB->get_record('forum_posts', array('id' => $i), '*', MUST_EXIST);
    $discussion = $DB->get_record('forum_discussions', array('id' => $post->discussion), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
    $forum = $DB->get_record('forum', array('id' => $discussion->forum), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('forum', $forum->id, $course->id, false, MUST_EXIST);
} catch (Exception $ex) {
    die();
}

// check access
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}
$modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
try {
    require_capability('mod/forum:viewdiscussion', $modcontext, NULL, true, 'noviewdiscussionspermission', 'forum');
} catch (Exception $ex) {
    die();
}

require_once(dirname(__FILE__) . '/../lib.php');

// get post attachments and return
try {
    if (isset($post->id)) {
    
        $out = array();
    
        $fs = get_file_storage();
        $files = $fs->get_area_files($modcontext->id, 'mod_forum', 'attachment', $post->id);
         
        foreach ($files as $file) {
            $record = monorail_file_embed_record($file);
            if ($record)
                $out[] = $record;
        }

        if (count($out) > 0)
            echo json_encode($out);
        else
            echo '[]';
    }
} catch (Exception $ex) {
    die();
}


?>
