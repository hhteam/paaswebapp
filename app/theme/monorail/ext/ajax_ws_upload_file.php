<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(dirname(__FILE__) . '/../../../config.php');
global $CFG;
require_once($CFG->dirroot . '/webservice/lib.php');
//authenticate the user
$token = $_REQUEST["token"];
$webservicelib = new webservice();
$authenticationinfo = $webservicelib->authenticate_user($token);
// FineUploader from monorail theme
require(dirname(__FILE__) . '/../qqFileUploader.php');
$uploader = new qqFileUploader();
$uploader->inputName = 'qqfile';
$uploader->sizeLimit = $CFG->monorail_upload_limit;
if (!file_exists($CFG->dataroot.'/temp/files/chunks/')) {
 mkdir($CFG->dataroot.'/temp/files/chunks/');
}
$uploader->chunksFolder = $CFG->dataroot.'/temp/files/chunks/';
if (!file_exists($CFG->dataroot.'/temp/files/'.$authenticationinfo['user']->id.'/')) {
 mkdir($CFG->dataroot.'/temp/files/'.$authenticationinfo['user']->id.'/');
}
$result = $uploader->handleUpload($CFG->dataroot.'/temp/files/'.$authenticationinfo['user']->id.'/');
$result['uploadName'] = $uploader->getUploadName();
if ($result['uploadName']) {
 add_to_log(1, 'theme_monorail', 'ext/ajax_upload_file', '', 'User '.$authenticationinfo['user']->id.' uploaded file "'.$result['uploadName'].'" to temp folder');
}
header("Content-Type: text/plain");
echo json_encode($result);
