<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 */

define('AJAX_SCRIPT', true);
define('REQUIRE_CORRECT_ACCESS', true);
require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');
require_once($CFG->dirroot.'/mod/bigbluebuttonbn/lib.php');
require_once($CFG->dirroot.'/mod/bigbluebuttonbn/locallib.php');
require_once($CFG->dirroot.'/mod/bigbluebuttonbn/viewer.php');
global $DB, $CFG;

$bbbid = required_param('id', PARAM_INT);

// verify that user is logged in
try {
   require_login(null, false, null, false, true);
} catch (Exception $ex) {
   die();
}

try {
  if(!$DB->record_exists('bigbluebuttonbn', array('id'=>$bbbid))) {
    die();
  }
  $bbrec = $DB->get_record('bigbluebuttonbn', array('id'=>$bbbid));
  $cm = $DB->get_record('course_modules', array('instance' => $bbbid, 'course' => $bbrec->course));
  $response = get_bigblue_url($cm->id); 
  $error = ''; 
  if(array_key_exists('error',$response)) {
     $error = $response["error"];
  }
  if(array_key_exists('url',$response)) {
    $exturl = $response['url'];
  } else {
    $exturl = $CFG->wwwroot.'/mod/bigbluebuttonbn/view.php?id='.$cm->id;
  }
  echo json_encode(array('url' => $exturl, 'error' => $error));
} catch (Exception $ex) {
   die();
}

