
<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

try {
    $i = required_param('i', PARAM_INT);
} catch (Exception $ex) {
    //~ echo $ex->getTraceAsString();
    die();
}

// get data
try {
    $discussion = $DB->get_record('forum_discussions', array('id' => $i), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
    $forum = $DB->get_record('forum', array('id' => $discussion->forum), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('forum', $forum->id, $course->id, false, MUST_EXIST);
} catch (Exception $ex) {
    //~ echo $ex->getTraceAsString();
    die();
}

// check access
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
    //~ echo $ex->getTraceAsString();
	die();
}
$modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
$coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
try {
    require_capability('mod/forum:viewdiscussion', $modcontext, NULL, true, 'noviewdiscussionspermission', 'forum');
    if (!$cm->visible and !has_capability('moodle/course:viewhiddenactivities', $coursecontext)) {
        // hidden
        echo '[]';
        die();
    }
} catch (Exception $ex) {
    //echo $ex->getTraceAsString();
    die();
}

// collect some data
try {
    $posts = $DB->get_records('forum_posts', array('discussion'=>$i), MUST_EXIST);

    $return = array();
    foreach ($posts as $post) {        
        $date = userdate($post->modified);

        if ($USER->id == $post->userid || has_capability('mod/forum:editanypost', $modcontext))
            $canedit = true;
        else
            $canedit = false;
        if ($canedit && !($forum->type == 'news' && !$post->parent && $discussion->timestart > time())) {
            if (((time() - $post->created) > $CFG->maxeditingtime) and
                        !has_capability('mod/forum:editanypost', $modcontext)) {
                $canedit = false;
            }
        }

        $postdata = array('i'=>$post->id, 'm'=>$post->message, 't'=>$date, 'a'=>$post->attachment, 'e'=>(($canedit)?1:0), 'c'=>$course->id);
        $return[] = array('i'=>$post->id, 'u'=>$post->userid, 'd'=>$discussion->id, 'p'=>$postdata);
    }
    
    // add audit log entry that discussion posts have been retrieved
    add_to_log($course->id, 'theme_monorail', 'ext/ajax_get_posts_discussion', 'User '.$USER->id.' viewed discussion '.$discussion->id);

    echo json_encode($return);
} catch (Exception $ex) {
    //~ echo $ex->getTraceAsString();
    die();
}

?>
