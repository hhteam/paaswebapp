<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

// WARNING: this is used by Magento to register payments for courses and shouldn't
// be used (or be accessible) by anything else.


require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $DB, $CFG;

if (!@in_array($_SERVER["REMOTE_ADDR"], $CFG->mage_ips))
{
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 Not Found</h1>";
    exit(0);
}

try
{
    $courseCode = required_param('course', PARAM_TEXT);
    $userId = required_param('user', PARAM_INT);
    $totaltax = (float) required_param("totaltax", PARAM_TEXT);
    $totalprice = (float) required_param("totalprice", PARAM_TEXT);

    $courseId = $DB->get_field('monorail_course_data', 'courseid', array('code' => $courseCode), MUST_EXIST);

    $record = new stdClass();

    $record->userid = $userId;
    $record->courseid = $courseId;
    $record->active = 1;
    $record->timestamp = time();
    $record->quantity = 1;
    $record->unitprice = $totalprice - $totaltax;
    $record->totaltax = $totaltax;
    $record->totalprice = $totalprice;
    $record->paymentdetails = "ALL OK";

    $DB->insert_record("monorail_course_payments", $record);

    echo "OK";
}
catch (Exception $err)
{
    echo "FAIL";
}
