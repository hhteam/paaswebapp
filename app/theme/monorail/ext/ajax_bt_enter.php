<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $SESSION, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$nonce = required_param('nonce', PARAM_TEXT);
$cohortid = required_param("cohort", PARAM_INT);

require_once __DIR__ . "/../braintree/lib/Braintree.php";

Braintree_Configuration::environment($CFG->braintree_environment);
Braintree_Configuration::merchantId($CFG->braintree_merchantId);
Braintree_Configuration::publicKey($CFG->braintree_publicKey);
Braintree_Configuration::privateKey($CFG->braintree_privateKey);

$bt_userid = str_replace(".", "_", $CFG->my_host) . "-" . $USER->id;

try {
    $customer = Braintree_Customer::find($bt_userid);
} catch (Braintree_Exception_NotFound $no) {
    $result = Braintree_Customer::create(array(
        'id' => $bt_userid,
        'firstName' => $USER->firstname,
        'lastName' => $USER->lastname,
        'email' => $USER->email
    ));
}

$result = Braintree_PaymentMethod::create(array(
    'customerId' => $bt_userid,
    'paymentMethodNonce' => $nonce,
    'options' => array (
        'makeDefault' => true,
        'failOnDuplicatePaymentMethod' => false
    )
));

// TODO: device_data? but there's not sale here

header('Content-Type: application/json');

if ($result->success) {
    $cinfo = $DB->get_record_sql("SELECT id, company_status, next_billing FROM {monorail_cohort_info} WHERE cohortid=?", array($cohortid));

    if ($cinfo->company_status == 2 && $cinfo->next_billing < time() &&
        $DB->record_exists("monorail_data", array("type" => "PAYNOTIF", "itemid" => $cohortid)))
    {
        // Already paid customer, with failed payment - try to collect now.
        require_once __DIR__ . "/../premium_payments.php";

        $status = premium_payments_process_payment($cohortid);
        $result = "Cohort has entered new payment method: http://eliademy.com/app/admin/tool/monorail_admin_tool/admin_e4b.php?cid=" . $cohortid . "\n";
        $hasErrors = false;

        if ($status["ok"]) {
            $result .= " SUCCESS: ";
        } else {
            $result .= " FAILURE: ";
            $hasErrors = true;
        }

        $result .= $status["msg"] . "\n\n";

        mail("aurelijus@cloudberrytec.com,info@cloudberrytec.com",
            "Premium payment collection report" . ($hasErrors ? " (CONTAINS ERRORS!)" : ""),
            $result);
    }
    else
    {
        if ($cinfo->company_status == 1) {
            // A demo user - convert to paid user.
            $cinfo->next_billing = strtotime("+1 month midnight");
            $cinfo->company_status = 2;

            $DB->update_record("monorail_cohort_info", $cinfo);
        }
    }

    echo json_encode(array("state" => "success"));
} else {
    echo json_encode(array("state" => "failure"));
}
