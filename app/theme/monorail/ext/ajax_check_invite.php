
<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $USER, $DB, $SESSION, $CFG;

header('Content-Type: application/json');

try {
    $code = required_param('code', PARAM_TEXT);
} catch (Exception $ex) {
    die(json_encode(array('message' => 'INVITE_CODE_MISSING')));
}

// get data
$invite = monorail_get_invitation($code);

if ($invite && $invite->type == 'course') {
    $courseData = $DB->get_record("monorail_course_data", array("courseid"=>$invite->targetid));
    $result = array('courseid'=>$invite->targetid, 'coursecode'=>$courseData->code, 'active'=>$invite->active, 'type' =>$invite->type);
    if ($USER->id) {
        $result['userid'] = $USER->id;
    } else {
        $result['userid'] = 0;
    }
    echo json_encode($result);
} else if($invite && $invite->type == 'cohort'){
    $cohortData = $DB->get_record("cohort", array("id"=>$invite->targetid));
    $result = array('cohortid'=>$invite->targetid, 'active'=>$invite->active, 'type' =>$invite->type, "cohortName" => $cohortData->name);
    if ($USER->id) {
        $result['userid'] = $USER->id;
    } else {
        $result['userid'] = 0;
    }
    echo json_encode($result);
} else {
    // not valid, return error response
    header('HTTP/1.1 500 Internal Server Error');
    die(json_encode(array('message' => 'CANNOT_FIND_INVITATION')));
}

?>
