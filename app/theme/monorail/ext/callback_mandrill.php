<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/* Handle call-backs from mandrill (update email status)
 * */

define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $DB, $CFG;

$data = json_decode($_POST["mandrill_events"]);

foreach ($data as $item)
{
    if (!$item->msg->metadata || !$item->msg->metadata->invite_id) {
        // No invite id.
        continue;
    }

    $invite = $DB->get_record_sql("SELECT id, email FROM {monorail_invite_users} WHERE id=?",
        array($item->msg->metadata->invite_id));

    if (!$invite || strtolower($invite->email) != strtolower($item->msg->email)) {
        // No invite record/wrong address
        continue;
    }

    $DB->execute("UPDATE {monorail_invite_users} SET status=? WHERE id=?",
        array($item->event == "send" ? 3 : 4, $invite->id));
}
