<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}


$data = json_decode($_POST["stats"]);

$transaction = $DB->start_delegated_transaction();

foreach ($data as $item)
{
    $rec = new stdClass();

    $rec->userid = $USER->id;
    $rec->action = $item->action;
    $rec->timestamp = $item->time;

    $rec->courseid = @$item->details->courseid;
    $rec->section = @$item->details->section;
    $rec->taskid = @$item->details->taskid;
    $rec->quizid = @$item->details->quizid;

    $DB->insert_record("monorail_user_activity_log", $rec);
}

$transaction->allow_commit();
