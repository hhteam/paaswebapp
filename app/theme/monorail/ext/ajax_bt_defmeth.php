<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $SESSION, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

require_once __DIR__ . "/../braintree/lib/Braintree.php";

Braintree_Configuration::environment($CFG->braintree_environment);
Braintree_Configuration::merchantId($CFG->braintree_merchantId);
Braintree_Configuration::publicKey($CFG->braintree_publicKey);
Braintree_Configuration::privateKey($CFG->braintree_privateKey);

$bt_userid = str_replace(".", "_", $CFG->my_host) . "-" . $USER->id;

header('Content-Type: application/json');

try {
    $customer = Braintree_Customer::find($bt_userid);

    // Check paypal accounts
    foreach ($customer->paypalAccounts as $i) {
        if ($i->default) {
            echo json_encode(array("name" => $i->email, "image" => $i->imageUrl));
            exit(0);
        }
    }

    // Credit cards.
    foreach ($customer->creditCards as $i) {
        if ($i->default) {
            echo json_encode(array("name" => $i->maskedNumber, "image" => $i->imageUrl));
            exit(0);
        }
    }

    // And so on?

} catch (Braintree_Exception_NotFound $no) {
    // No user no method
}

echo json_encode(array("state" => "error"));
