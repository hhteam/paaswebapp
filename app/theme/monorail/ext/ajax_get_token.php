<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

define('AJAX_SCRIPT', true);
define('REQUIRE_CORRECT_ACCESS', true);

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $USER, $DB;
$username = optional_param('username', null, PARAM_EMAIL);
// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
       // not logged in, just die
       //Exit with better error codes
       $error = 1; 
       if($username) {
           $auth = $DB->get_field('user', 'auth', array('username'=>str_replace(' ', '', $username)));
           if(!$auth) {
               $error = 1001;
           } else if($auth && (strcmp($auth,'googleoauth2') == 0)) {
               $error = 1002;
           } 
       }
       $resp = array("error" => $error);
       echo json_encode($resp);
       exit; 
}

$token = monorail_handle_ws_token($USER);

try {
  monorail_create_relay_channel();
} catch (Exception $ex) {
  // not fatal, but log
  add_to_log(1, 'monorail', 'ajax_get_token', '', 'ERROR: Failed to create relay channel : '.$ex->getMessage()); 
}

$acid='';
$ucids='';
$auser = NULL;
$expire_date = false;
try {
  $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id));
  $lang = $DB->get_field('user', 'lang', array('id'=>$USER->id));
  if(!empty($lang)) {
      $USER->lang = $lang;
  }
} catch(Exception $ex) {
  add_to_log(1, 'ajax_get_token', 'monorail_cohort_admin_users', '', 'ERROR: Failed to get : '.$ex->getMessage()); 
}

if($auser) {
    $acid = $auser->cohortid;
    $expire_date = $DB->get_field('monorail_cohort_info', 'demo_period_end',
                            array('cohortid'=>$acid, 'company_status'=>1));
} else {
    //Maybe not admin but cohort member, get first match;
    $ucids = "";
    $cusers = $DB->get_records_sql("SELECT cohortid FROM {cohort_members} WHERE userid=?", array($USER->id));

    foreach ($cusers as $cuser)
    {
          $ucids .= $cuser->cohortid . ",";
    }
}
$adminMsg = null;
$is_paas = false;

if(property_exists($CFG, 'adminMessage')) {
    $adminMsg = $CFG->adminMessage;
}

if(property_exists($CFG, 'isPaas')) {
    $isPaas = $CFG->isPaas;
}

$data = array("token"   => $token,
              "userid"  => $USER->id,
              "sesskey" => sesskey(),
              "lang"    => $USER->lang,
              "auth"    => $USER->auth,
              "acid"    => $acid,
              "ucids"   => $ucids,
              'expired' => $expire_date && ($expire_date < time()),
              "adminMsg"=> $adminMsg,
              "isPaas"  => $isPaas
    );

echo json_encode($data);
