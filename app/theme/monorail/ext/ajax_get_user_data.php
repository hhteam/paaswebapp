
<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $OUTPUT;

// check login
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}
// we don't need any other access checks since we only want name and pic, which
// is ok since we require login to get the data

try {
    $i = required_param('i', PARAM_INT);
} catch (Exception $ex) {
    $i = $USER->id;
}

// get data
try {
    $user = $DB->get_record('user', array('id' => $i));
    if (!isset($user)) {
        die();
    }
} catch (Exception $ex) {
    die();
}

$cohortMember = 1;

$auser = $DB->get_records_sql("SELECT userid FROM {monorail_cohort_admin_users} WHERE userid=? LIMIT 1", array($USER->id));

if (empty($auser))
{
    $cuser = $DB->get_records_sql("SELECT userid FROM {cohort_members} WHERE userid=? LIMIT 1", array($USER->id));

    if (empty($auser))
    {
        $cohortMember = 0;
    }
}

// define data
try {
    $avatar = new user_picture($user);
    $avatar->size = 37;
    $pic = $OUTPUT->render($avatar);
    $name = fullname($user);

    $return = array('i'=>$i, 'p'=>$pic, 'n'=>$name, 'c'=>$cohortMember);

    echo json_encode($return);
} catch (Exception $ex) {
    die();
}

?>
