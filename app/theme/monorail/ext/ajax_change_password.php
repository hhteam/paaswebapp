<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

define('AJAX_SCRIPT', true);
define('REQUIRE_CORRECT_ACCESS', true);

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $USER, $DB;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$password = optional_param('p', NULL,PARAM_TEXT);
$newpassword = required_param('np', PARAM_TEXT);
$changepassword = false;
$success = false;
try {
  if((strcmp($USER->auth,"googleoauth2") == 0) && empty($password)) {
    //oauth - init password missing maybe
    $changepassword = true; 
  } else {
    //check if current password is right!
    if(validate_internal_user_password($USER, $password)) {
       $changepassword = true; 
    }
  }
  if($changepassword && !empty($newpassword)) { 
    $success = update_internal_user_password($USER, $newpassword);
  }
} catch(Exception $ex) {
  add_to_log(1, 'ajax_change_pwd', 'monorail', '', 'ERROR: Failed : '.$ex->getMessage()); 
}

$data = array("success" => $success);
echo json_encode($data);
