
<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG, $OUTPUT;

try {
    $i = required_param('i', PARAM_INT);
    $c = required_param('c', PARAM_INT);
} catch (Exception $ex) {
    die();
}

// get data
try {
    $cm = get_coursemodule_from_id('resource', $i, $c, false, MUST_EXIST);
    $resource = $DB->get_record('resource', array('id'=>$cm->instance), '*', MUST_EXIST);
} catch (Exception $ex) {
    die();
}

// check access
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}
$modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
try {
    require_capability('mod/resource:view', $modcontext);
} catch (Exception $ex) {
    die();
}

require_once(dirname(__FILE__) . '/../lib.php');

// audit
add_to_log($c, 'monorail', 'ext/ajax_get_resource', '', 'View resource '.$cm->id.', instance '.$resource->id);

// get resource file and return
try {
    $out = array();

    $fs = get_file_storage();
    $files = $fs->get_area_files($modcontext->id, 'mod_resource', 'content');
     
    foreach ($files as $file) {
        $record = monorail_file_embed_record($file, $resource->name, $i);
        if ($record) {
            $out[] = $record;
            break;
        }
    }

    if (count($out) > 0)
        echo json_encode($out);
    else
        echo '[]';
} catch (Exception $ex) {
    die();
}


?>
