<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

$uploadedfile = required_param('filename', PARAM_FILE);
$cohort_id = required_param('cohort', PARAM_INT);

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/filestorage/file_storage.php');

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

// TODO: check if user has admin access to cohort...

$filename = $CFG->dataroot.'/temp/files/' .$USER->id. '/'. $uploadedfile;

resizeImage($filename, $CFG->dirroot . '/public_images/course_logo/coh' . $cohort_id . '.png', 192, 76);
resizeImage($filename, $CFG->dirroot . '/public_images/org/logo' . $cohort_id . '.png', 192, 192);

@unlink($filename);
@unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$uploadedfile);

$url = $CFG->wwwroot . '/public_images/course_logo/coh' . $cohort_id . '.png';

echo htmlspecialchars(json_encode($url), ENT_NOQUOTES);

function resizeImage($filename, $saveAs, $maxWidth, $maxHeight)
{
    list($width,$height) = getimagesize($filename);

    $newheight = $height;
    $newwidth = $width;

    if ($width > $maxWidth || $height > $maxHeight) {
        if ( ($width / $maxWidth) > ($height / $maxHeight)) {
            $newheight = round($height * $maxWidth / $width);
            $newwidth = $maxWidth;
        } else {
            $newwidth = round($width * $maxHeight / $height);
            $newheight = $maxHeight;
        }
    }

    if ($newwidth != $width || $newheight != $height)
    {
        $extension = getExtension($filename);
        if($extension == "jpg" || $extension == "jpeg" ) {
            $src = imagecreatefromjpeg($filename);
        } else if($extension == "png") {
            $src = imagecreatefrompng($filename);
        } else if($extension == "gif") {
            $src = imagecreatefromgif($filename);
        } else {
            // TODO: return error message
            echo htmlspecialchars(false, ENT_NOQUOTES);
        }

        $tmp = imagecreatetruecolor($newwidth, $newheight);
        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagepng($tmp, $saveAs, 0);
        imagedestroy($src);
        imagedestroy($tmp);
    }
    else
    {
        copy($filename, $saveAs);
    }
}

function getExtension($str) {
    $i = strrpos($str,".");
    if (!$i) { return ""; }

    $l = strlen($str) - $i;
    $ext = strtolower(substr($str,$i+1,$l));
    return $ext;
}
