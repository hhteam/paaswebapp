<?php

// Upload a file to the KMC
require_once(dirname(__FILE__) . '/../../../config.php');


global $CFG, $USER, $PAGE;

// verify that user is logged in
try {
        require_login(null, false, null, false, true);
} catch (Exception $ex) {
        // not logged in, just die
        die();
}


$file = required_param('file', PARAM_FILE);

$vfile = $CFG->dataroot."/temp/files/".$USER->id."/".$file;

//Remove uploaded file
unlink($vfile);
echo '[]';
?>
