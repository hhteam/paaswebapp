<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


define('LOGO_WIDTH', 192);
define('LOGO_HEIGHT', 76);

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

$uploadedfile = required_param('filename', PARAM_FILE);
$course_id = required_param('course', PARAM_INT);

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/filestorage/file_storage.php');

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$filename = $CFG->dataroot.'/temp/files/' .$USER->id. '/'. $uploadedfile;

// Prepare the file
list($width,$height) = getimagesize($filename);

$newheight = $height;
$newwidth = $width;

if ($width > LOGO_WIDTH || $height > LOGO_HEIGHT) {
    if ( ($width / LOGO_WIDTH) > ($height / LOGO_HEIGHT)) {
        $newheight = round($height * LOGO_WIDTH / $width);
        $newwidth = LOGO_WIDTH;
    } else {
        $newwidth = round($width * LOGO_HEIGHT / $height);
        $newheight = LOGO_HEIGHT;        
    }
}

if ($width != $newwidth || $height != $newheight)
{
    $extension = getExtension($uploadedfile);
    if($extension == "jpg" || $extension == "jpeg" ) {
        $src = imagecreatefromjpeg($filename);
    } else if($extension == "png") {
        $src = imagecreatefrompng($filename);
    } else if($extension == "gif") {
        $src = imagecreatefromgif($filename);
    } else {
        // TODO: return error message
        echo htmlspecialchars(false, ENT_NOQUOTES);
    }

    $tmp = imagecreatetruecolor($newwidth, $newheight);
    imagealphablending($tmp, false);
    imagesavealpha($tmp, true);
    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    @unlink($filename);
    @unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$uploadedfile);

    $filename = $CFG->dirroot . '/public_images/course_logo/' . $course_id . '.png';
    imagepng($tmp, $filename, 0);

    imagedestroy($src);
    imagedestroy($tmp);

    $url = $CFG->wwwroot . '/public_images/course_logo/' . $course_id . '.png';
}
else
{
    rename($filename, $CFG->dirroot . '/public_images/course_logo/' . $course_id . '.' . getExtension($uploadedfile));
    @unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$uploadedfile);

    $url = $CFG->wwwroot . '/public_images/course_logo/' . $course_id . '.' . getExtension($uploadedfile);
}

echo htmlspecialchars(json_encode($url), ENT_NOQUOTES);

function getExtension($str) {
    $i = strrpos($str,".");
    if (!$i) { return ""; }

    $l = strlen($str) - $i;
    $ext = strtolower(substr($str,$i+1,$l));
    return $ext;
}
