<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

// WARNING: this is used by Magento to enrol users to courses and shouldn't
// be used (or be accessible) by anything else.


require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');
require_once(dirname(__FILE__) . '/../sociallib.php');

global $DB, $CFG;

if (!@in_array($_SERVER["REMOTE_ADDR"], $CFG->mage_ips))
{
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 Not Found</h1>";
    exit(0);
}

$courseCode = required_param('course', PARAM_TEXT);
$userId = required_param('user', PARAM_INT);

$courseId = $DB->get_field('monorail_course_data', 'courseid', array('code' => $courseCode), MUST_EXIST);

require_once($CFG->libdir . '/enrollib.php');

$enrol = enrol_get_plugin('manual');
$enrolinstances = enrol_get_instances($courseId, true);

foreach ($enrolinstances as $courseenrolinstance)
{
    if ($courseenrolinstance->enrol == "manual")
    {
        $instance = $courseenrolinstance;
        break;
    }
}

if ($instance)
{
    $roleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST);

    $monorailData = $DB->get_records_sql("SELECT value FROM {monorail_data} WHERE itemid=? AND datakey='course_price'", array($courseId));
    $courseInfo = $DB->get_record_sql("SELECT c.fullname AS fullname, mcd.mainteacher AS mainteacher " .
        "FROM {course} AS c INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=c.id WHERE c.id=?", array($courseId));

    if ($courseInfo) {
        foreach ($monorailData as $md) {
            if ((float) $md->value > 0) {
                monorail_send_info_mail('mail/coursepurchasenotification',
                    str_replace("{%name%}", $courseInfo->fullname, get_string('email_order_notification_subject', 'theme_monorail')),
                    array(
                        "COURSE_NAME" => $courseInfo->fullname,
                        "TOTALPRICE" => number_format(0, 2),
                        "VAT" => number_format(0, 2),
                        "STUDENT_NAME" => $DB->get_field_sql("SELECT CONCAT(firstname, ' ', lastname) FROM {user} WHERE id=?", array($userId)),
                        "STUDENT_EMAIL" => $DB->get_field_sql("SELECT email FROM {user} WHERE id=?", array($userId))
                    ),
                    $courseInfo->mainteacher);

                break;
            }
        }
    }

    $enrol->enrol_user($instance, $userId, $roleid, time(), 0, ENROL_USER_ACTIVE);

    social_share_course_enroll($courseId, $userId);

    require_once __DIR__ . "/../../../local/monorailservices/cachelib.php";

    wscache_reset_by_dependency("course_info_" . $courseId);
    wscache_reset_by_dependency("user_courses_" . $userId);

    echo "OK";

    fastcgi_finish_request();
    $DB->dispose();

    if (pcntl_fork())
    {
        exit(0);
    }

    unset($DB);
    setup_DB();

    if (isset($CFG->mage_api_url))
    {
        require_once __DIR__ . "/../../../local/monorailservices/mageapi.php";
        $api = new MageApi($CFG->mage_api_url);

        if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
        {
            $api->updateParticipants($courseId);
            $api->disconnect();
        }
    }
}
