<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $CFG, $OUTPUT, $PAGE;

$categoryid = optional_param('categoryid', null, PARAM_INT);
$courseid = optional_param('courseid', null, PARAM_INT);

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/filestorage/file_storage.php');

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$result = array();

$categoryNum = $DB->get_record('course_categories', array('id'=>$categoryid));

// '0' means unspecific 
if( 0 == $categoryNum->idnumber )
	$files = $DB->get_records('monorail_course_background');
else
	$files = $DB->get_records('monorail_course_background', array('categoryid'=>$categoryid));

if (!empty($files)) {

	$context = get_system_context();
	foreach($files as $file) {
		$image = new stdClass();
		$image->id = $file->id;

		$context = context_course::instance(SITEID);

		$url = moodle_url::make_pluginfile_url($context->id, 'course', 'summary', null, '/', $file->id);
		$image->url = $url->out(false);

		$result[] = $image;
	}
}

echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
