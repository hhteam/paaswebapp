<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

// WARNING: this is used by Magento to update user data and shouldn't
// be used (or be accessible) by anything else.

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $DB, $CFG;

if (!@in_array($_SERVER["REMOTE_ADDR"], $CFG->mage_ips))
{
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 Not Found</h1>";
    exit(0);
}

$country = required_param('country', PARAM_TEXT);
$user = required_param("user", PARAM_INT);

$DB->set_field('user', 'country', $country, array('id' => $user));

require_once __DIR__ . "/../../../local/monorailservices/cachelib.php";

wscache_reset_by_dependency("user_info_" . $user);

echo "OK";
