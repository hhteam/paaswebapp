<?php

// Upload a file to the KMC
require_once(dirname(__FILE__) . '/../../../config.php');


global $CFG, $USER, $PAGE;
ini_set('max_execution_time', 3000);
// verify that user is logged in
try {
        require_login(null, false, null, false, true);
} catch (Exception $ex) {
        // not logged in, just die
        die();
}


$file = required_param('file', PARAM_FILE);
$filename = required_param('filename', PARAM_FILE);
$vfile = $CFG->dataroot."/temp/files/".$USER->id."/".$file;

try
{
    require_once($CFG->dirroot . '/local/kaltura/locallib.php');
    error_reporting(0);
    $kclient = local_kaltura_login(true);

    if (!$kclient) {
        die("Could not establish Kaltura session. Please verify that you are using valid Kaltura partner credentials.");
    }

    $token = $kclient->media->upload($vfile);
}
catch (Exception $err)
{
    @unlink($vfile);

    throw $err;
}

unlink($vfile);
$data = array("id" => $token);
echo json_encode($data);
?>
