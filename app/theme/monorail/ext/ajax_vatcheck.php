<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $SESSION, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$vat = required_param('vat', PARAM_TEXT);
$country = required_param('country', PARAM_TEXT);

$clen = strlen($country);

if (substr($vat, 0, $clen) == $country) {
    $vat = substr($vat, $clen);
}

$text = trim(file_get_contents("http://isvat.appspot.com/" . $country . "/" . $vat . "/"));

header('Content-Type: application/json');

if ($text == "false") {
    echo json_encode(array("valid" => false));
} else {
    echo json_encode(array("valid" => true));

    if ($text != "true") {
        error_log("Failed to validate VAT code: " . $text);
    }
}
