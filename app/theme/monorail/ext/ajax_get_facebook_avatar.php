<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG, $PAGE;

try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$code = optional_param('code', '', PARAM_TEXT); //Google can return an error

if (empty($code)) {
	echo htmlspecialchars(json_encode(false), ENT_NOQUOTES);
	die;
}

//set the params specific to the authentication provider
$params = array();
$params['client_id'] 		= get_config('auth/googleoauth2', 'facebookclientid');
$params['client_secret'] 	= get_config('auth/googleoauth2', 'facebookclientsecret');
$requestaccesstokenurl 		= 'https://graph.facebook.com/oauth/access_token';
$params['redirect_uri'] 	= $CFG->wwwroot . '/theme/monorail/ext/ajax_get_facebook_avatar.php';
$params['code'] 			= $code;

//request by curl an access token and refresh token
require_once($CFG->libdir . '/filelib.php');
$curl 				= new curl();
$postreturnvalues 	= $curl->post($requestaccesstokenurl, $params);

parse_str($postreturnvalues, $returnvalues);
$accesstoken = $returnvalues['access_token'];

//with access token request by curl the email address
if (empty($accesstoken)) {
	//echo htmlspecialchars(json_encode(false), ENT_NOQUOTES);
	returnValue(false);
	die;
}
$params 				= array();
$params['access_token'] = $accesstoken;
$postreturnvalues 		= $curl->get('https://graph.facebook.com/me', $params);
$facebookuser 			= json_decode($postreturnvalues);

$picurl 	= "https://graph.facebook.com/". $facebookuser->id . "/picture?type=large";

if (!file_exists($CFG->dataroot.'/temp/files/'.$USER->id.'/')) {
    mkdir($CFG->dataroot.'/temp/files/'.$USER->id.'/');
}

// save picture to temp files
$result = copy($picurl, $CFG->dataroot.'/temp/files/'.$USER->id. '/' . $USER->id.'facebookprofilepicture');

returnValue($picurl);

die;

function returnValue($picurl) {
    if ($picurl) {
        $picurl = "'".$picurl."'";
        $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
var userpicture = window.opener.jQuery("img#user-profile-image");
var src = userpicture.attr('src');
src = src.substring(0, src.indexOf('?v=') + 3);
    		
userpicture.attr('src', {$picurl});
self.close();
</script>
<body>
</body>
</html>
HTML;
    } else {
        $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script>
self.close();
</script>
<body>
</body>
</html>
HTML;
    }
	echo $text;
}
