<?php

require_once(__DIR__ . '/../../../config.php');
require_once(__DIR__ . '/../lib.php');

global $USER, $DB, $SESSION;

// verify that user is logged in
try {
        require_login(null, false, null, false, true);
} catch (Exception $ex) {
        // not logged in, just die
        die();
}

$parts = explode(":", $_GET["state"]);

if (count($parts) != 2 || $parts[0] != "course")
{
    // Invalid course identification.
    die();
}

$ok = false;
$roles = $DB->get_recordset_sql("SELECT ra.id AS id, r.shortname AS shortname " .
        "FROM {role_assignments} AS ra " .
            "INNER JOIN {context} AS ctx ON ra.contextid=ctx.id AND ctx.contextlevel=50 " .
            "INNER JOIN {role} AS r ON r.id=ra.roleid " .
                "WHERE ra.userid=? AND ctx.instanceid=?", array($USER->id, $parts[1]));

foreach ($roles as $role)
{
    if ($role->shortname == "manager" || $role->shortname == "coursecreator" || $role->shortname == "editingteacher" || $role->shortname == "teacher")
    {
        $ok = true;
        break;
    }
}

if (!$ok)
{
    // User doesn't own this course.
    die();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>Please wait...</title>
</head>
<body>
<?php

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.paypal.com/v1/identity/openidconnect/tokenservice");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_USERPWD, $CFG->paypal_userid . ":" . $CFG->paypal_secret);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=authorization_code&code=" . $_GET["code"] .
        "&redirect_uri=https://eliademy.com/app/theme/monorail/ext/callback_paypal.php");
    $data = json_decode(curl_exec($ch));
    curl_close($ch);

    $ch1 = curl_init();

    curl_setopt($ch1, CURLOPT_URL, "https://api.paypal.com/v1/identity/openidconnect/userinfo/?schema=openid");
    curl_setopt($ch1, CURLOPT_POST, 0);
    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch1, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $data->access_token));

    $data = json_decode(curl_exec($ch1));
    curl_close($ch1);

    monorail_data('TEXT', $parts[1], 'course_paypal_account', $data->email, true);

    require_once __DIR__ . "/../../../local/monorailservices/cachelib.php";
    wscache_reset_by_dependency("course_info_" . $parts[1]);

?>

<script>
 window.opener.paypalAccountAdded();
 self.close();
</script>

</body>
</html>
