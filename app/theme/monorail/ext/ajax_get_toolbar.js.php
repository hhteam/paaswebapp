<?php /* vim: set ft=javascript: */
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');
header('Content-Type: text/javascript; charset=utf-8');

global $USER, $DB, $CFG, $SESSION;

$langs = get_string_manager()->get_list_of_translations();
$currentLang = $SESSION->lang;
$haveUser = false;

if (!$currentLang || !array_key_exists($currentLang, $langs))
{
    $currentLang = "en";
}

try
{
	require_login(null, false, null, false, true);

    $haveUser = true;

    $courses = enrol_get_users_courses($USER->id, false, "id");
    $data = array();

    foreach ($courses as $course)
    {
        $code = $DB->get_field('monorail_course_data', 'code', array('courseid' => $course->id));

        if (!$code)
        {
            continue;
        }

        $context = context_course::instance($course->id);
        $roles = get_user_roles($context, $USER->id);
        $canedit = false;

        foreach ($roles as $role)
        {
            if ($role->shortname === 'teacher' || $role->shortname === 'manager' || $role->shortname == "editingteacher")
            {
                $canedit = true;
                break;
            }
        }

        $data[$code] = array("canedit" => $canedit);
    }

    $cohortMember = 1;
    $cohortAdmin = 0;
    $cohortName = "";

    $auser = $DB->get_records_sql("SELECT id, userid, cohortid FROM {monorail_cohort_admin_users} WHERE userid=? LIMIT 1", array($USER->id));

    if (empty($auser))
    {
        $cuser = $DB->get_records_sql("SELECT userid, cohortid FROM {cohort_members} WHERE userid=? LIMIT 1", array($USER->id));

        if (empty($cuser))
        {
            $cohortMember = 0;
        }
        else
        {
            $cuser = reset($cuser);
            $cohortName = $DB->get_field_sql("SELECT name FROM {cohort} WHERE id=?", array($cuser->cohortid));
        }
    }
    else
    {
        $auser = reset($auser);

        $cohortName = $DB->get_field_sql("SELECT name FROM {cohort} WHERE id=?", array($auser->cohortid));
        $cohortAdmin = 1;
    }

    $paidCourses = array();

    $paid = $DB->get_records_sql("SELECT d.code AS code FROM {monorail_course_data} AS d " .
        "INNER JOIN {monorail_course_payments} AS p ON p.courseid=d.courseid " .
            "WHERE p.userid=? AND p.active", array($USER->id));

    foreach ($paid as $pc)
    {
        $paidCourses[$pc->code] = 1;
    }

    $address = monorail_data("ADDRESS", $USER->id, "user_address");

    if (!empty($address))
    {
        $address = reset($address);
        $address = json_decode($address->value);
    }
    else
    {
        $address = null;
    }
}
catch (Exception $ex)
{ }

function remove_code($txt)
{
    return html_entity_decode(preg_replace("/\s*\(.*\)$/", "", $txt), ENT_HTML5, "UTF-8");
}

?>
var _Header = { };

(function (H, $)
{
    /* Make DOM element tree out of JSON data */
    var makeDom = function (data)
    {
        var el;

        if (data instanceof Array)
        {
            el = document.createDocumentFragment();

            for (var i=0; i<data.length; i++)
            {
                el.appendChild(makeDom(data[i]));
            }
        }
        else if (data instanceof Object)
        {
            el = document.createElement(data.node);

            if (data.attr instanceof Object)
            {
                for (var i in data.attr)
                {
                    el.setAttribute(i, data.attr[i]);
                }
            }

            if (data.child)
            {
                el.appendChild(makeDom(data.child));
            }
        }
        else
        {
            el = document.createTextNode(data || "");
        }

        return el;
    };

    /* DOM tree data for language menu. */
    var languageMenu = { node: "div", attr: { "class": "dropdown", "style": "display: inline-block; vertical-align: middle;" }, child: [
            { node: "a", attr: { "class": "dropdown-toggle header-menu-item", "data-toggle": "dropdown" }, child: [
                "<?php echo(remove_code($langs[$currentLang])); ?> ",
                { node: "i", attr: { "class": "fa fa-chevron-down", "style": "font-size: 14px; margin-top: -3px;" } } ] },
            { node: "ul", attr: { "class": "dropdown-menu mobile-reset", "style": "margin-left: -200px;" }, child: [
                { node: "li", child: { node: "a", attr: { href: "http://translate.eliademy.com/", target: "_blank" }, child: "<?php echo get_string("button_help_translate", "theme_monorail"); ?>" } },
                { node: "li", attr: { "class": "divider" } },
                { node: "li", child: { node: "div", attr: { style: "width: 400px;", "class": "mobile-reset" }, child: [<?php

    $keys = array_keys($langs);
    $cnt = count($keys);
    $half = ceil($cnt / 2);
    $i = 0;

    echo "{ node: \"ul\", attr: { \"class\": \"dropdown-menu\", style: \"width: 200px; display: inline-block; position: static; margin: 0px; border: none; box-shadow: none; float: none; min-width: 0; vertical-align: top;\" }, child: [\n";

    for (; $i<$half; $i++)
    {
        echo " { node: \"li\", attr: { role: \"menuitem\" }, child: { node: \"a\", attr: { href: \"" . $CFG->landing_page . "/" . $keys[$i] . "/" . @$_GET["page"] . "\" }, child: \"" . remove_code($langs[$keys[$i]]) . "\" } },\n";
    }

    echo " ] }, { node: \"ul\", attr: { \"class\": \"dropdown-menu\", style: \"width: 200px; display: inline-block; position: static; margin: 0px; border: none; box-shadow: none; float: none; min-width: 0; vertical-align: top;\" }, child: [\n";

    for (; $i<$cnt; $i++)
    {
        echo " { node: \"li\", attr: { role: \"menuitem\" }, child: { node: \"a\", attr: { href: \"" . $CFG->landing_page . "/" . $keys[$i] . "/" . @$_GET["page"] . "\" }, child: \"" . remove_code($langs[$keys[$i]]) . "\" } },\n";
    }

    echo " ] }";
?>]
                } }
            ] }
        ] };

    H.init = function (el, activeLink)
    {
        el.innerHTML = "";

<?php if ($haveUser): ?>

        el.appendChild(makeDom([
            { node: "a", attr: { href: "<?php echo $CFG->landing_page . "/" . $currentLang; ?>", "class": "header-logo" }, child:
                { node: "img", attr: { src: "<?php echo $CFG->landing_page; ?>/img/header_logo.png", style: "height: 41px;" } } },

            { node: "span", attr: { "class": "btn header-mobile-btn", id: "header-toggle-mobile-menu" }, child:
                { node: "i", attr: { "class": "fa fa-align-justify" } } },

            { node: "div", attr: { "class": "header-mobile-bar", "id": "header-mobile-menu" }, child: [
                { node: "a", attr: { href: "<?php echo($CFG->magic_ui_root); ?>/", "class": "header-menu-item" + (activeLink == "courses" ? " active" : "") }, child: [
                    { node: "i", attr: { "class": "fa fa-bars" } },
                    { node: "span", attr: { "class": "header-button-label" }, child: "<?php echo get_string("header_my_courses", "theme_monorail"); ?>" } ] },

                { node: "a", attr: { href: "<?php echo($CFG->magic_ui_root); ?>/calendar", "class": "header-menu-item" + (activeLink == "calendar" ? " active" : "") }, child: [
                    { node: "i", attr: { "class": "fa fa-calendar" } },
                    { node: "span", attr: { "class": "header-button-label" }, child: "<?php echo get_string("header_calendar", "theme_monorail"); ?>" } ] },

                

<?php if ($cohortMember): ?>
                { node: "a", attr: { href: "<?php echo($CFG->magic_ui_root); ?>/catalog", "class": "header-menu-item" }, child: [
                    { node: "i", attr: { "class": "fa fa-users" } },
                    { node: "span", attr: { "class": "header-button-label" }, child: "<?php echo $cohortName; ?> <?php echo get_string("header_courses", "theme_monorail"); ?>" } ] },
<?php endif; ?>

<?php if ($cohortAdmin): ?>
                { node: "a", attr: { href: "<?php echo($CFG->magic_ui_root); ?>/organization-details", "class": "header-menu-item" }, child: [
                    { node: "i", attr: { "class": "fa fa-cogs" } },
                    { node: "span", attr: { "class": "header-button-label" }, child: "<?php echo get_string("header_admin", "theme_monorail"); ?>" } ] },
<?php endif; ?>

                { node: "div", attr: { "class": "header-right-box" }, child:
                    { node: "div", attr: { "class": "dropdown header-menu-item" }, child: [
                        { node: "a", attr: { "class": "dropdown-toggle", "data-toggle": "dropdown" }, child: [
                            { node: "img", attr: { "class": "header-user-picture", src: "<?php echo $CFG->wwwroot . '/public_images/user_picture/' . monorail_get_userpic_hash($USER->id) . '_32.jpg'; ?>" } },
                            //{ node: "span", child: "<?php echo fullname($USER); ?>" },
                            { node: "i", attr: { "class": "fa fa-chevron-down", style: "font-size: 14px; margin: -4px 0px 0px 5px;" } }
                        ] },
                        { node: "ul", attr: { "class": "dropdown-menu pull-right" }, child: [
                            { node: "li", child: { node: "a", attr: { href: "<?php echo $CFG->magic_ui_root ?>/profile" }, child: [ { node: "i", attr: { "class": "fa fa-user" } }, " <?php echo get_string("header_myprofile", "theme_monorail"); ?>" ] } },
                            { node: "li", child: { node: "a", attr: { href: "<?php echo $CFG->magic_ui_root ?>/settings" }, child: [ { node: "i", attr: { "class": "fa fa-cog" } }, " <?php echo get_string("header_settings", "theme_monorail"); ?>" ] } },
                            { node: "li", child: { node: "a", attr: { href: "<?php echo $CFG->magic_ui_root ?>/analytics" }, child: [ { node: "i", attr: { "class": "fa fa-bar-chart-o" } }, " <?php echo get_string("header_analytics", "theme_monorail"); ?>" ] } },
                            { node: "li", child: { node: "a", attr: { href: "http://helpdesk.eliademy.com/knowledgebase", target: "_blank" }, child: [ { node: "i", attr: { "class": "fa fa-question-circle" } }, " <?php echo get_string("header_helpdesk", "theme_monorail"); ?>" ] } },
                            { node: "li", attr: { "class": "divider" } },
                            { node: "li", child: { node: "a", attr: { id: "header-logout-button" }, child: [ { node: "i", attr: { "class": "fa fa-power-off" } }, " <?php echo get_string("header_logout", "theme_monorail"); ?>" ] } }
                        ] }
                    ] } }
            ]}
        ]));

        $("#header-logout-button").click(function ()
        {
            var doLogout = function ()
            {
                localStorage.removeItem('login.wants-url');
                localStorage.removeItem('user_login');

                window.location.href = "<?php echo $CFG->landing_page . "/" . $currentLang; ?>";
            };

            var postCleanup = function ()
            {
                $.ajax({ url: "<?php echo $CFG->wwwroot; ?>/login/logout.php?sesskey=<?php echo sesskey(); ?>",
                     type: "POST", success: doLogout, error: doLogout });
            };

            $.ajax({ url: "<?php echo $CFG->wwwroot; ?>/theme/monorail/ext/ajax_logout_cleanup.php",
                 type: "POST", success: postCleanup, error: postCleanup });
        });

<?php else: ?>

        var loginEl;

        if (activeLink == "login")
        {
            loginEl = { node: "a", attr: { "class": "btn btn-success", href: "<?php echo $CFG->landing_page . "/" . $currentLang ?>/signup" }, child: "<?php echo get_string("header_signup", "theme_monorail"); ?>" };
        }
        else
        {
            loginEl = { node: "a", attr: { "class": "btn btn-primary", href: "<?php echo $CFG->landing_page . "/" . $currentLang ?>/login" }, child: "<?php echo get_string("header_login", "theme_monorail"); ?>" };
        }

        el.appendChild(makeDom([
            { node: "a", attr: { href: "<?php echo $CFG->landing_page . "/" . $currentLang; ?>", "class": "header-logo" }, child:
                { node: "img", attr: { src: "<?php echo $CFG->landing_page; ?>/img/header_logo.png", style: "height: 41px;" } } },

            { node: "span", attr: { "class": "btn header-mobile-btn", id: "header-toggle-mobile-menu" }, child:
                { node: "i", attr: { "class": "fa fa-align-justify" } } },

            { node: "div", attr: { "class": "header-mobile-bar", "id": "header-mobile-menu" }, child: [
                { node: "a", attr: { href: "<?php echo($CFG->landing_page . "/" . $currentLang); ?>/premium", "class": "header-menu-item" + (activeLink == "business" ? " active" : "") }, child: "<?php echo get_string("header_business", "theme_monorail"); ?>" },
                { node: "a", attr: { href: "<?php echo($CFG->landing_page . "/" . $currentLang); ?>/features", "class": "header-menu-item" + (activeLink == "features" ? " active" : "") }, child: "<?php echo get_string("header_features", "theme_monorail"); ?>" },
                <?php /* { node: "a", attr: { href: "<?php echo($CFG->catalog_url); ?>/", "class": "header-menu-item" + (activeLink == "catalog" ? " active" : "") }, child: "<?php echo get_string("header_catalog", "theme_monorail"); ?>" },*/?>
                { node: "a", attr: { href: "http://helpdesk.eliademy.com/knowledgebase", "class": "header-menu-item" + (activeLink == "support" ? " active" : "") }, child: "<?php echo get_string("header_helpdesk", "theme_monorail"); ?>" },
                { node: "a", attr: { href: "<?php echo($CFG->landing_page); ?>/blog/", "class": "header-menu-item" + (activeLink == "blog" ? " active" : "") }, child: "<?php echo get_string("header_blog", "theme_monorail"); ?>" },
                { node: "div", attr: { "class": "header-right-box" }, child: [ languageMenu, loginEl ] }
            ] }
        ]));

<?php endif; ?>

        $("#header-toggle-mobile-menu").click(function ()
        {
            $("#header-mobile-menu").fadeToggle();
        });
    };

    H.initFooter = function (el)
    {
        el.innerHTML = "";

        el.appendChild(makeDom([
            { node: "h3", attr: { style: "text-align: center; color: #4189DD; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-weight: 300; font-size: 22pt; padding: 35px 0; line-height: normal;" }, child: "<?php echo get_string("footer_slogan", "theme_monorail"); ?>" },
            { node: "table", attr: { style: "width: 100%; margin: 20px 0px;" }, child:
                { node: "tr", child: [
                    { node: "td", attr: { style: "width: 25%;" }, child:
                        { node: "a", attr: { href: "http://cbtec.fi", target: "_blank", style: "color: #777;" }, child: "<?php echo get_string("text_madeinfinland", "theme_monorail"); ?>" } },

                    { node: "td", attr: { style: "text-align: center;" }, child: [
                        { node: "a", attr: { href: "/terms", style: "color: #777; margin-right: 1em;" }, child: "<?php echo get_string("login_terms2", "theme_monorail"); ?>" },
                        { node: "a", attr: { href: "/privacy", style: "color: #777; margin-right: 1em;" }, child: "<?php echo get_string("privacy", "theme_monorail"); ?>" },
                        { node: "a", attr: { href: "/opensource", style: "color: #777;" }, child: "<?php echo get_string("opensource", "theme_monorail"); ?>" }
                    ] },

                    { node: "td", attr: { style: "width: 25%; text-align: right;" }, child: [
                        { node: "span", attr: { style: "margin-right: 5px;" }, child: "<?php echo get_string("follow_us", "theme_monorail"); ?>" },
                        { node: "a", attr: { href: "http://www.facebook.com/eliademy", target: "_blank", style: "margin-right: 5px;" }, child: { node: "img", attr: { src: "/img/logo-facebook.png", alt: "Facebook", style: "border-radius: 10px; width: 20px; height: 20px;" } } },
                        { node: "a", attr: { href: "https://twitter.com/eliademy", target: "_blank", style: "margin-right: 5px;" }, child: { node: "img", attr: { src: "/img/logo-twitter.png", alt: "Twitter", style: "border-radius: 10px; width: 20px; height: 20px;" } } },
                        { node: "a", attr: { href: "https://plus.google.com/u/0/105453567066036560669/posts", target: "_blank", style: "margin-right: 5px;" }, child: { node: "img", attr: { src: "/img/logo-gplus.png", alt: "Google+", style: "border-radius: 10px; width: 20px; height: 20px;" } } },
                        { node: "a", attr: { href: "http://pinterest.com/eliademy/", target: "_blank", style: "margin-right: 5px;" }, child: { node: "img", attr: { src: "/img/logo-pinterest.png", alt: "Pinterest", style: "border-radius: 10px; width: 20px; height: 20px;" } } },
                        { node: "a", attr: { href: "http://www.linkedin.com/company/eliademy", target: "_blank", style: "margin-right: 5px;" }, child: { node: "img", attr: { src: "/img/logo-linkedin.png", alt: "LinkedIn", style: "border-radius: 10px; width: 20px; height: 20px;" } } },
                        { node: "a", attr: { href: "https://www.youtube.com/c/Eliademy", target: "_blank" }, child: { node: "img", attr: { src: "/img/logo-youtube.png", alt: "YouTube", style: "border-radius: 10px; width: 20px; height: 20px;" } } }
                    ] }
                ] }
            }
        ]));
    };

<?php if ($haveUser): ?>

    H.info = {
        userid: <?php echo $USER->id ?>,
        username: "<?php echo fullname($USER); ?>",
        userpic: "<?php echo $CFG->wwwroot . '/public_images/user_picture/' . monorail_get_userpic_hash($USER->id) . '_32.jpg'; ?>",
        sesskey: "<?php echo sesskey(); ?>",
        courses: <?php echo json_encode($data); ?>,
        paidCourses: <?php echo json_encode($paidCourses); ?>,
        cohort: <?php echo $cohortMember ?>,
        org: "<?php echo ($cohortAdmin ? $cohortName : ""); ?>",
        address: <?php echo json_encode($address); ?>,
        lang: "<?php echo $currentLang; ?>"
    };

<?php endif; ?>

}) (_Header, window.jQuery);
