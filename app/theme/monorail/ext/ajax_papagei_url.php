<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 */ 

define('AJAX_SCRIPT', true);
define('REQUIRE_CORRECT_ACCESS', true);
require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $USER, $DB, $CFG;

$coursecode = required_param('code', PARAM_TEXT);

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
   die();
}

try {
  $provider = 'papagei';
  if(!$DB->record_exists('monorail_external_courses', array('provider'=>$provider, 'intcode'=>$coursecode)) || 
   !$DB->record_exists('monorail_course_data', array('code'=> $coursecode))) {
    die();
  } 

  $courseData = $DB->get_record_sql("SELECT courseid FROM {monorail_course_data} WHERE code=?", array($coursecode));
  $courseid = $courseData->courseid;

  $course_context = context_course::instance($courseid);
  if (!is_enrolled($course_context, $USER->id)) {
    die();
  }


  $productId = $DB->get_field('monorail_external_courses', 'productid', array('provider'=> $provider, 'intcode'=> $coursecode), MUST_EXIST);
  $trainingId = $DB->get_field('monorail_external_courses', 'extcode', array('provider'=> $provider, 'intcode'=> $coursecode), MUST_EXIST);
  $language = $DB->get_field('monorail_external_courses', 'lang', array('provider'=> $provider, 'intcode'=> $coursecode), MUST_EXIST);
  if(!$language) {
    $language = 'en';
  }
  require_once(dirname(__FILE__) . '/../tpp/'.$provider.'/provider.php');
  $email = $USER->email;
  if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $password = '';
    if($DB->record_exists('monorail_user_ext_provider', array('userid'=>$USER->id, 'provider'=> $provider))) {
        $password = $DB->get_field('monorail_user_ext_provider', 'password', array('userid'=>$USER->id, 'provider'=> $provider), MUST_EXIST);
    } else {
        //First time access 
        $udata = new stdClass();
        $udata->userid  = $USER->id;
        $udata->provider  = 'papagei';
        $password = $udata->provider.hash_hmac("md5", substr(sha1(rand()), 0, 7), 'Taeje');
        $udata->password  = $password;
        $DB->insert_record('monorail_user_ext_provider', $udata);
    }
    $phash = new \Papagei\Api\Hash();
    $phash->setSecret($CFG->tpp_papagei_secret);
    $phash->setAlgorithm('sha256');
    $params = array(
        'username' => $email,
        'password' => $password,
        'email' => $email,
        'products' => array($productId)
        );
    $hashstr = $phash->createHash('/rest/login', 'post', $params);
    //Use this hash
    require_once($CFG->libdir . '/filelib.php');
    $curl = new curl();
    $curl->setHeader('X-Api-Version:'.'v1');
    $curl->setHeader('X-Api-Language:'. $language);
    $curl->setHeader('X-Api-Key:'.$CFG->tpp_papagei_key);
    $curl->setHeader('X-Api-Hash:'.$hashstr);

    $postreturnvalues = $curl->post('http://eliademy.papagei.com/rest/login', http_build_query($params, '', '&'));
    $postreturnvalues = json_decode($postreturnvalues);
    if(!$postreturnvalues->success) {
      die();
    }
    $data =  $postreturnvalues->data;
    $exturl = $data->domain . $data->path . '?' . 'trainingId=' . $trainingId . '&token=' . $data->token; 
    echo htmlspecialchars(json_encode($exturl), ENT_NOQUOTES);
  } else {
    die();
  }
} catch (Exception $ex) {
   die();
}
