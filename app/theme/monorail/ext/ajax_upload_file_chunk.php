<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

// which uploader?
if (strpos($_SERVER['HTTP_REFERER'], 'monorail/forum.php')) {
    // TODO: delete this part - no more non-magic ui!
    // FineUploader from monorail theme
    require(dirname(__FILE__) . '/../qqFileUploader.php');
    $uploader = new qqFileUploader();
    $uploader->inputName = 'qqfile';
    $uploader->sizeLimit = $CFG->monorail_upload_limit;
    if (!file_exists($CFG->dataroot.'/temp/files/chunks/')) {
        mkdir($CFG->dataroot.'/temp/files/chunks/');
    }
    $uploader->chunksFolder = $CFG->dataroot.'/temp/files/chunks/';
    if (!file_exists($CFG->dataroot.'/temp/files/'.$USER->id.'/')) {
        mkdir($CFG->dataroot.'/temp/files/'.$USER->id.'/');
    }
	$result = $uploader->handleUpload($CFG->dataroot.'/temp/files/'.$USER->id.'/');
    $result['uploadName'] = $uploader->getUploadName();
    if ($result['uploadName']) {
        add_to_log(1, 'theme_monorail', 'ext/ajax_upload_file', '', 'User '.$USER->id.' uploaded file "'.$result['uploadName'].'" to temp folder');
    }
    header("Content-Type: text/plain");
    echo json_encode($result);
} else {
    // jQuery Fileupload from Magic UI
    require(dirname(__FILE__) . '/../UploadHandler.php');
    $regexp = '/.*filename=([^ ]+)/';
    preg_match($regexp, $_SERVER['HTTP_CONTENT_DISPOSITION'], $file_name);
    $file_name = rawurldecode(trim($file_name[1], '";'));
    // from http://stackoverflow.com/a/2727693/1489738
    $file_name = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $file_name);
    if (strpos($_SERVER['HTTP_REFERER'], '/app/a/courses/import')) {
        $maxsize = return_bytes(ini_get('upload_max_filesize'));    // use PHP size
    } else {
        $maxsize = $CFG->monorail_video_upload_limit;     // use Moodle config size
    }
    $content_range = isset($_SERVER['HTTP_CONTENT_RANGE']) ? preg_split('/[^0-9]+/', $_SERVER['HTTP_CONTENT_RANGE']) : null;
    $result = new UploadHandler( array('upload_dir'=>$CFG->dataroot.'/temp/files/'.$USER->id.'/', 'max_file_size'=>$maxsize) );
    if ($content_range == null || intval($content_range[2])+1 == intval($content_range[3])) {
        // hackish way to know if whole file is uploaded for one log message
        add_to_log(1, 'theme_monorail', 'ext/ajax_upload_file', '', 'User '.$USER->id.' uploaded file "'.$file_name.'" to temp folder');
    }
}

