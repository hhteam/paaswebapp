<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $SESSION, $CFG, $DB;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$nonce = required_param('nonce', PARAM_TEXT);
$amount = required_param("amount", PARAM_FLOAT);
$numlicences = required_param("numlicences", PARAM_FLOAT);
$tax = required_param("tax", PARAM_FLOAT);
$cohortid = required_param("cohort", PARAM_INT);

try {
    $billing_details = json_decode($DB->get_field_sql("SELECT billing_details FROM {monorail_cohort_info} WHERE cohortid=?",
        array($cohortid)));
} catch (Exception $err) { }

require_once __DIR__ . "/../braintree/lib/Braintree.php";

Braintree_Configuration::environment($CFG->braintree_environment);
Braintree_Configuration::merchantId($CFG->braintree_merchantId);
Braintree_Configuration::publicKey($CFG->braintree_publicKey);
Braintree_Configuration::privateKey($CFG->braintree_privateKey);

$bt_userid = str_replace(".", "_", $CFG->my_host) . "-" . $USER->id;
$pm_token = null;

try {
    $customer = Braintree_Customer::find($bt_userid);
} catch (Braintree_Exception_NotFound $no) {
    $result = Braintree_Customer::create(array(
        'id' => $bt_userid,
        'firstName' => $USER->firstname,
        'lastName' => $USER->lastname,
        'email' => $USER->email
    ));
}

$result = Braintree_PaymentMethod::create(array(
    'customerId' => $bt_userid,
    'paymentMethodNonce' => $nonce,
    'options' => array (
        'makeDefault' => true,
        'failOnDuplicatePaymentMethod' => false
    )
));

$paymentMethod = $result->paymentMethod;

$result = Braintree_Transaction::sale(array(
  'amount' => $amount,
  'taxAmount' => $tax,
  'customerId' => $bt_userid,
  'paymentMethodToken' => $paymentMethod->token,
  'options' => array(
    'submitForSettlement' => true
  ),
  'deviceData' => $_POST["device_data"],
  'customFields' => array(
        // XXX: scanlang hardcoded. other types?
        'itemcode' => 'annualoffer-' . $numlicences,
        'country' => $billing_details->country,
        'user' => $USER->id
  )
));

header('Content-Type: application/json');

$invoice = new stdClass();

$invoice->cohortid = $cohortid;
$invoice->timeissued = time();
$invoice->timepaid = time();
$invoice->paidby = $USER->id;
$invoice->invoicetype = 2;
$invoice->quantity = 1;
$invoice->paymentstatus = 1;
$invoice->unitprice = $amount - $tax;
$invoice->totaltax = $tax;
$invoice->totalprice = $amount;

try {
    if ($result->success) {
        $invoice->paymentstatus = 2;
        $invoice->invoicenum = ((int) $DB->get_field_sql("SELECT MAX(invoicenum) FROM {monorail_cohort_invoice}")) + 1;

        // XXX: validity is hardcoded here.
        $DB->execute("UPDATE {monorail_cohort_info} SET userscount=?, valid_until=?, is_paid=1, company_status=2, next_billing=? WHERE cohortid=?",
            array($numlicences, strtotime("+1 year midnight"), strtotime("+1 month midnight"), $cohortid));

        echo json_encode(array("state" => "success"));
    } else {
        $invoice->paymentstatus = 3;
        mail("aurelijus@cloudberrytec.com", "Annual licence payment error!", var_export($result, true));
        echo json_encode(array("status" => "error"));
    }

    $invoice->paymentmessage = var_export($result, true);
} catch (Exception $err) {
    mail("aurelijus@cloudberrytec.com", "Error while processing payment!", var_export($err, true));

    echo json_encode(array("state" => "error"));
}

$invid = $DB->insert_record("monorail_cohort_invoice", $invoice);

if ($invoice->paymentstatus == 2) {
    require_once __DIR__ . "/../lib.php";

    $subject = get_string_manager()->get_string('email_subject_invoice', 'theme_monorail', null, "EN");

    $body = monorail_template_compile(monorail_get_template('mail/base'), array(
       'title' => $subject,
       'body' => monorail_template_compile(monorail_get_template("mail/cohortinvoice"),
            array("INVOICENUM" => $invoice->invoicenum, "DATE" => date("F j, Y"), "AMOUNT" => $invoice->totalprice, "INVOICEID" => $invid)),
       'footer_additional_link' => '',
       'block_header' => '',
       "username" => $DB->get_field_sql("SELECT firstname FROM {user} WHERE id=?", array($USER->id))
    ), "EN");

    monorail_send_email('receipt', $subject, $body, $USER->id, "EN");
}
