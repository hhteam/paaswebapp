<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

// verify that user is logged in
try {
  require_login(null, false, null, false, true);
} catch (Exception $ex) {
  die();
}

echo  "<script type='text/javascript'>";
echo "window.close();";
echo "</script>";
