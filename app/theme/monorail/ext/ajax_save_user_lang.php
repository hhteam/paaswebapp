<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

$language = required_param('language', PARAM_TEXT);
// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

add_to_log(1, 'theme_monorail', 'ext/ajax_save_user_language', '', 'User '.$USER->id.' language '.$language);

$result = $DB->set_field('user', 'lang', $language, array('id' => $USER->id));
$USER->lang=$language;

require_once __DIR__ . "/../../../local/monorailservices/cachelib.php";

wscache_reset_by_dependency("user_language_" . $USER->id);
