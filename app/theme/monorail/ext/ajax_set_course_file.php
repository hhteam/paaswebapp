<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

/* Used to set/unset files associated with course. File name is stored in
 * monorail data under the given field name. File id is stored under
 * <field name>_fileid in monorail data.
 * */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$file = required_param('file', PARAM_FILE);
$field = required_param('field', PARAM_TEXT);
$courseid = required_param('course', PARAM_INT);
$oldPicId = null;
$filepath = $CFG->dataroot.'/temp/files/' .$USER->id. '/'. $file;

if (!is_file($filepath)) {
    $filepath = null;
}

$coursecontext = context_course::instance($courseid);

if (!has_capability('moodle/course:manageactivities', $coursecontext)) {
    // Don't have access to course.
    if ($filepath) {
        unlink($filepath);
    }

    exit(0);
}

require_once __DIR__ . "/../../../local/monorailservices/cachelib.php";
wscache_reset_by_dependency("course_info_" . $courseid);

$monorailData = $DB->get_records_sql("SELECT value FROM {monorail_data} WHERE itemid=? AND datakey=?", array($courseid, $field . "_fileid"));

if (!empty($monorailData))
{
    $monorailData = reset($monorailData);
    $oldPicId = $monorailData->value;
}

require_once($CFG->libdir.'/filestorage/file_storage.php');

$fs = get_file_storage();

if ($oldPicId)
{
    $oldFile = $fs->get_file_by_id($oldPicId);
    $oldFile->delete();

    $DB->delete_records("monorail_data", array("itemid" => $courseid, "datakey" => $field));
    $DB->delete_records("monorail_data", array("itemid" => $courseid, "datakey" => $field . "_fileid"));
}

if ($filepath)
{
    // Must create new file

    $fileinfo = array(
        'contextid' => $coursecontext->id,
        'component' => "course",
        'filearea' => "summary",
        'itemid' => 0,
        'filepath' => '/',
        'filename' => md5(time() . $field) . "_" . $file
    );

    $file = $fs->create_file_from_pathname($fileinfo, $filepath);

    unlink($filepath);

    $url = "{$CFG->wwwroot}/pluginfile.php/{$file->get_contextid()}/{$file->get_component()}/{$file->get_filearea()}/{$file->get_filename()}";

    // Insert new data in monorail data.
    $rec = new stdClass();

    $rec->itemid = $courseid;
    $rec->type = "URL";
    $rec->datakey = $field;
    $rec->value = $url;

    $DB->insert_record("monorail_data", $rec);

    $rec->type = "INTEGER";
    $rec->datakey = $field . "_fileid";
    $rec->value = $file->get_id();

    $DB->insert_record("monorail_data", $rec);

    echo($url);
}
