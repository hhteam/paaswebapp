<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

// WARNING: this is used by Magento to sync course ratings and shouldn't
// be used (or be accessible) by anything else.


require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');

global $DB, $CFG;

if (!@in_array($_SERVER["REMOTE_ADDR"], $CFG->mage_ips))
{
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 Not Found</h1>";
    exit(0);
}

$courseCode = required_param('course', PARAM_TEXT);
$rating = required_param('rating', PARAM_INT);

$courseId = $DB->get_field('monorail_course_data', 'courseid', array('code' => $courseCode), MUST_EXIST);

monorail_data('INTEGER', $courseId, 'course_rating', $rating, true);

echo "OK";
