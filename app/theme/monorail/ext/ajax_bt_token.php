<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $SESSION, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

require_once __DIR__ . "/../braintree/lib/Braintree.php";

Braintree_Configuration::environment($CFG->braintree_environment);
Braintree_Configuration::merchantId($CFG->braintree_merchantId);
Braintree_Configuration::publicKey($CFG->braintree_publicKey);
Braintree_Configuration::privateKey($CFG->braintree_privateKey);

$bt_userid = str_replace(".", "_", $CFG->my_host) . "-" . $USER->id;

try {
    $customer = Braintree_Customer::find($bt_userid);
} catch (Braintree_Exception_NotFound $no) {
    $result = Braintree_Customer::create(array(
        'id' => $bt_userid,
        'firstName' => $USER->firstname,
        'lastName' => $USER->lastname,
        'email' => $USER->email
    ));
}

$clientToken = Braintree_ClientToken::generate(array("customerId" => $bt_userid));

header('Content-Type: application/json');

echo json_encode(array("token" => $clientToken, "merchant" => $CFG->braintree_merchantId));
