<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

$cohortid = @$_POST["cohortid"];
$message = @$_POST["message"];

if (!$DB->record_exists("monorail_cohort_admin_users", array("userid" => $USER->id, "cohortid" => $cohortid)))
{
    die();
}

$DB->execute("UPDATE {monorail_cohort_info} SET company_status=5 WHERE cohortid=?", array($cohortid));

$cname = $DB->get_field_sql("SELECT name FROM {cohort} WHERE id=?", array($cohortid));
$username = $DB->get_field_sql("SELECT CONCAT(firstname, ' ', lastname, ' <', email, '>') FROM {user} WHERE id=?", array($USER->id));

$message = "Request for NGO account for cohort " . $cname . " (id " . $cohortid . ") by user " . $username .
    "\n\nhttps://eliademy.com/app/admin/tool/monorail_admin_tool/admin_e4b.php?cid=" . $cohortid . "\n\nMessage:\n" . $message . "\n";

mail("support@cloudberrytec.com,aurelijus@cloudberrytec.com",
     "NGO request",
     $message,
     "Reply-To: " . $USER->firstname . " " . $USER->lastname . " <" . $USER->email . ">");
