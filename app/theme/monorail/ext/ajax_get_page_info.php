<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

/* Returns meta properties of the specified page.
 * */

define('AJAX_SCRIPT', true);
define('REQUIRE_CORRECT_ACCESS', true);
require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

// check access
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	die();
}

try {
    $url = required_param('url', PARAM_TEXT);
} catch (Exception $ex) {
    die();
}

$dom = new DOMDocument();

$dom->loadHTMLFile($url);

$metas = $dom->getElementsByTagName("meta");
$data = array();

foreach ($metas as $meta) {
    if ($meta->hasAttribute("property")) {
        $data[$meta->getAttribute("property")] = $meta->getAttribute("content");
    }
}

echo json_encode($data);
