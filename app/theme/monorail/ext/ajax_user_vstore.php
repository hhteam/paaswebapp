<?php

// Upload a file to the KMC
require_once(dirname(__FILE__) . '/../../../config.php');


global $CFG, $USER;

// verify that user is logged in
try {
        require_login(null, false, null, false, true);
} catch (Exception $ex) {
        // not logged in, just die
        die();
}

$vusage = 0;
$vrecords = $DB->get_records("monorail_course_videorsc", array("userid"=>$USER->id));
foreach($vrecords as $vrecord) {
  $vusage = $vusage + $vrecord->filesize; //MB
}
$vusage = round($vusage/(1024 * 1024)); //MB

$data = array("fsize" => $vusage);
echo json_encode($data);
?>
