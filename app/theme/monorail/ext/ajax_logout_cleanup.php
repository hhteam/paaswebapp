<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $CFG;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

require_once('../lib.php');
//Delete any third party user data before logout 
try {
    $context = get_context_instance(CONTEXT_USER, $USER->id);
    $fs = get_file_storage();
    $fs->delete_area_files($context->id, 'evernote_userfile', 'evernote_files', $USER->id);
} catch (Exception $ex) {
    add_to_log(1, 'monorail', 'ext.cleanup.resource', '', 'Error! Failed to cleanup evernote rsc: '.get_class($ex).': '.$ex->getMessage());
}

// Close session and relay connection
try {
    $DB->delete_records('monorail_sessions', array('userid'=>$USER->id, 'sesskey'=>sesskey()));
    require_once __DIR__ . '/../vendor/autoload.php';
    $redis = new Predis\Client(array(
        'scheme' => 'tcp',
        'host'   => $CFG->relay_redis_host,
        'port'   => $CFG->relay_redis_port,
    ));
    $redis->del(sesskey());
} catch (Exception $ex) {
    add_to_log(1, 'monorail', 'ext.cleanup.resource', '', 'Error! Failed to close relay connection: '.get_class($ex).': '.$ex->getMessage());
}
echo '[]';
