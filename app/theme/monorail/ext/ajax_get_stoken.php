<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);
define('NO_MOODLE_COOKIES', true);

global $DB,$CFG, $SESSION;
require_once(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/authlib.php');
require_once(dirname(__FILE__) . '/../lib.php');

$accesstoken = required_param('token', PARAM_RAW);
$provider  = required_param('provider',  PARAM_ALPHANUMEXT);
$email = required_param('email',  PARAM_RAW);
$serviceshortname  = required_param('service',  PARAM_ALPHANUMEXT);

//get the user record corrsponding to the username
$user = $DB->get_record('user', array('username'=>$email, 'deleted'=> 0, 'suspended' => 0, 'auth' => 'googleoauth2', 'email' => $email));

if(!empty($user)) {
 //Taken from googleoauth2 module for fetching info about user once we have the access token

 //with access token request by curl the email address
 if (!empty($accesstoken)) {
  require_once($CFG->libdir . '/filelib.php');
  $curl = new curl();
  //get the username matching the email
  switch ($provider) {
   case 'google':
    $params = array();
    $params['access_token'] = $accesstoken;
    $params['alt'] = 'json';
    $postreturnvalues = $curl->get('https://www.googleapis.com/userinfo/email', $params);
    $postreturnvalues = json_decode($postreturnvalues);
    $useremail = $postreturnvalues->data->email;
    $verified = $postreturnvalues->data->isVerified;
    break;

   case 'facebook':
    $params = array();
    $params['access_token'] = $accesstoken;
    $postreturnvalues = $curl->get('https://graph.facebook.com/me', $params);
    $facebookuser = json_decode($postreturnvalues);
    $useremail = $facebookuser->email;
    //~ $verified = $facebookuser->verified;
    $verified = 1; //see bug https://bugs.cloudberrytec.com/show_bug.cgi?id=195
    break;

   case 'linkedin':
    $params = array();
    $params['oauth2_access_token'] = $accesstoken;
    $params['format'] = 'json';
    $postreturnvalues = $curl->get('https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,site-standard-profile-request,main-address,email-address)', $params);
    $linkedinuser = json_decode($postreturnvalues);
    $useremail = $linkedinuser->emailAddress;
    //~ $verified = $linkedinuser->verified;
    $verified = 1; //see bug https://bugs.cloudberrytec.com/show_bug.cgi?id=195
    break;

   case 'messenger':
    $params = array();
    $params['access_token'] = $accesstoken;
    $postreturnvalues = $curl->get('https://apis.live.net/v5.0/me', $params);
    $messengeruser = json_decode($postreturnvalues);
    $useremail = $messengeruser->emails->preferred;
    $verified = 1; //not super good but there are no way to check it yet:
    //http://social.msdn.microsoft.com/Forums/en-US/messengerconnect/thread/515d546d-1155-4775-95d8-89dadc5ee929
    break;

   default:
    break;
  }

  //throw an error if the email address is not verified
  if (!$verified) {
   throw new moodle_exception('emailaddressmustbeverified', 'auth_googleoauth2');
  }

  // Prohibit login if email belongs to the prohibited domain
  if ($err = email_is_not_allowed($useremail)) {
   throw new moodle_exception($err, 'auth_googleoauth2');
  }

  //if email not existing in user database then create a new username (userX).
  if (empty($useremail) or $useremail != clean_param($useremail, PARAM_EMAIL)) {
   throw new moodle_exception('couldnotgetuseremail');
   //TODO: display a link for people to retry
  }

  //Ok we got the token from mobile client , verify this user is the same
  if((strcmp($useremail,$email) == 0) && (strcmp($useremail,$user->username) == 0)) {
   //get the user - don't bother with auth = googleoauth2 because
   //authenticate_user_login() will fail it if it's not 'googleoauth2'
   $user = $DB->get_record('user', array('email' => $useremail, 'deleted' => 0, 'mnethostid' => $CFG->mnet_localhost_id));
   if(!empty($user)) {
    //required to set the session up
    $SESSION->goauthv = true;//Request from current oauth verified session;
    $user = monorail_authenticate_user_login($user->username, $user->password);
    unset($SESSION->goauthv);
    if($user) {
     complete_user_login($user);
     $token = monorail_handle_ws_token($USER);
     $rdata = array("token" => $token, "userid" => $USER->id, "sesskey" => sesskey(), "lang" => $user->lang);
     echo json_encode($rdata);
     die;
    }
   }
  }

 }
}
echo json_encode(array());
