<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '/../lib.php');
global $DB, $CFG;

$logged = true;
// verify that user is logged in
try {
  require_login(null, false, null, false, true);
} catch (Exception $ex) {
  $logged = false;
}

$data = array("logged" => $logged, "public" => false);
try {
    $code = required_param('code', PARAM_TEXT);
} catch (Exception $ex) {
    echo json_encode($data);
    die();
}

if($code) {
  $result = 0;
  $courseData = $DB->get_record('monorail_course_data', array('code'=>$code));
  if($courseData) {
    $cperms = monorail_get_course_perms($courseData->courseid);
    $result = (($cperms["course_privacy"] == 1) && ($cperms["course_access"] != 1)) ? 1 : 0;
  }
  $public = false;
  if($result == 1) {
   $public = true;
  }

  $data['public'] = $public;
}
echo json_encode($data);
?>

