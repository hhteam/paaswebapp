<?php

class theme_monorail_testcase extends advanced_testcase {

    private function init_settings() {
        global $CFG;
        // add some configs that are missing on test side
        $CFG->landing_page = "https://127.0.0.1";
        
        require_once("$CFG->dirroot/local/monorailservices/externallib.php");
        $this->services = new local_monorailservices_external();
    }

    private function init_cohort_user() {
        global $DB;
        // data
        $user = $this->getDataGenerator()->create_user(array('email'=>'user@test.127.0.0.1', 'username'=>'user@test.127.0.0.1'));

        $cohorts = array(
            array(
                'name' => 'Personal Support Cohort',
                'domain' => 'test.127.0.0.1',
                'restrictdomain' => 0,
                'movecourses' => 0,
            )
        );
        $result = $this->services->add_cohorts($cohorts);
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $this->assertObjectHasAttribute('id', $user, $message = 'User should have an id');
        $this->assertObjectHasAttribute('id', $cohort, $message = 'Cohort should have an id');
        $result = $this->services->update_cohort_admins(array($user->id), $cohort->id, 1);
        $this->assertTrue($DB->record_exists('monorail_cohort_admin_users', array('cohortid'=>$cohort->id, 'userid'=>$user->id), "User should be admin of cohort"));
    }
    
    public function test_delay() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'delay-test';
        $message->identifier = 'return-true';
        $message->template = 'welcome';
        $message->userfilter = 'cohort-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 5;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewing user creation -4 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = time() - 345600;
        $DB->update_record('user', $user);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was not sent an email
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-5)), "Email was sent");
        
        // rewing user creation -6 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = time() - 518400;
        $DB->update_record('user', $user);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was not sent an email
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-5)), "Email was sent");
        
        // rewing user creation -5 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = time() - 432000;
        $DB->update_record('user', $user);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was sent an email
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-5)), "Email was not sent");
    }
    
    public function test_only_one_email() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'only-one-email-test';
        $message->identifier = 'return-true';
        $message->template = 'welcome';
        $message->userfilter = 'cohort-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 5;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewing user creation -5 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = $user->timecreated - 432000;
        $DB->update_record('user', $user);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        // trigger again
        engagements_process();
        
        // check that user was sent only one email
        $this->assertEquals(1, $DB->count_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-10)), "Only one email should have been sent");
    }

    public function test_personal_support_cohort() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'personal-support-cohort';
        $message->identifier = 'return-true';
        $message->template = 'personal-support-cohort';
        $message->userfilter = 'cohort-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 2;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewing user creation -2 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = $user->timecreated - 172800;
        $DB->update_record('user', $user);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was sent an email
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was not sent");   
    }
    
    public function test_customize_cohort_reminder() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'customize-cohort-reminder';
        $message->identifier = 'cohort-not-customized';
        $message->template = 'customize-cohort-reminder';
        $message->userfilter = 'cohort-and-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 3;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewind user creation -3 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = $user->timecreated - 259200;
        $DB->update_record('user', $user);
        // rewind cohort creation -3 days
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $cohort->timecreated = $cohort->timecreated - 259200;
        $cohort->timemodified = $cohort->timemodified - 259200;
        $DB->update_record('cohort', $cohort);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was sent an email
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was not sent");
    }
    
    public function test_encourage_learning_together() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'encourage-learning-together';
        $message->identifier = 'return-true';
        $message->template = 'encourage-learning-together';
        $message->userfilter = 'cohort-and-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 7;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewind user creation -7 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = $user->timecreated - 604800;
        $DB->update_record('user', $user);
        // rewind cohort creation -7 days
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $cohort->timecreated = $cohort->timecreated - 604800;
        $cohort->timemodified = $cohort->timemodified - 604800;
        $DB->update_record('cohort', $cohort);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was sent an email
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was not sent");
    }
    
    public function test_encourage_learning_together_2() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'encourage-learning-together-2';
        $message->identifier = 'cohort-no-participants-added';
        $message->template = 'encourage-learning-together';
        $message->userfilter = 'cohort-and-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 14;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewind user creation -14 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = $user->timecreated - 1209600;
        $DB->update_record('user', $user);
        // rewind cohort creation -14 days
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $cohort->timecreated = $cohort->timecreated - 1209600;
        $cohort->timemodified = $cohort->timemodified - 1209600;
        $DB->update_record('cohort', $cohort);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was sent an email
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was not sent");
    }
    
    public function test_encourage_learning_together_2_negative() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'encourage-learning-together-2';
        $message->identifier = 'cohort-no-participants-added';
        $message->template = 'encourage-learning-together';
        $message->userfilter = 'cohort-and-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 14;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewind user creation -14 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = $user->timecreated - 1209600;
        $DB->update_record('user', $user);
        // rewind cohort creation -14 days
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $cohort->timecreated = $cohort->timecreated - 1209600;
        $cohort->timemodified = $cohort->timemodified - 1209600;
        $DB->update_record('cohort', $cohort);
        // add user to cohort
        $user2 = $this->getDataGenerator()->create_user(array('email'=>'user2@test.127.0.0.1', 'username'=>'user2@test.127.0.0.1'));
        $record = new stdClasS();
        $record->cohortid = $cohort->id;
        $record->userid = $user2->id;
        $record->timeadded = time();
        $DB->insert_record('cohort_members', $record);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that neither user was sent an email
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email to user@test.127.0.0.1 was sent");
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user2@test.127.0.0.1', time()-2)), "Email to user2@test.127.0.0.1 was sent");
    }
    
    public function test_manage_learning_space() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'manage-learning-space';
        $message->identifier = 'return-true';
        $message->template = 'manage-learning-space';
        $message->userfilter = 'cohort-and-user-created-days-ago';
        $message->type = 'time';
        $message->delay = 8;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewind user creation -8 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->timecreated = $user->timecreated - 691200;
        $DB->update_record('user', $user);
        // rewind cohort creation -8 days
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $cohort->timecreated = $cohort->timecreated - 691200;
        $cohort->timemodified = $cohort->timemodified - 691200;
        $DB->update_record('cohort', $cohort);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was sent an email
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was not sent");
        
        // print_r($DB->get_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)));
    }
    
    public function test_cohort_creating_a_course_reminder() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'cohort-creating-a-course-2';
        $message->identifier = 'cohort-no-course-created';
        $message->template = 'cohort-creating-a-course';
        $message->userfilter = 'cohort-user-inactive-for-days';
        $message->type = 'time';
        $message->delay = 28;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewind user lastaccess -29 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->lastaccess = time() - 2505600;
        $DB->update_record('user', $user);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was sent an email
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was not sent");
        
        // print_r($DB->get_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)));
    }
    
    public function test_cohort_creating_a_course_reminder_n() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'cohort-creating-a-course-2';
        $message->identifier = 'cohort-no-course-created';
        $message->template = 'cohort-creating-a-course';
        $message->userfilter = 'cohort-user-inactive-for-days';
        $message->type = 'time';
        $message->delay = 28;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // test
        // rewind user lastaccess -27 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->lastaccess = time() - 2332800;
        $DB->update_record('user', $user);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was not sent an email
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was sent");
        
        // rewind user lastaccess -29 days
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $user->lastaccess = time() - 2505600;
        $DB->update_record('user', $user);
        // make sure cohort has a course - or a record indicating so anyway
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $record = new stdClass();
        $record->cohortid = $cohort->id;
        $record->adminid = $user->id;
        $record->courseid = 123456;
        $DB->insert_record('monorail_cohort_courseadmins', $record);
        
        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that user was not sent an email
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)), "Email was sent");
        
        // print_r($DB->get_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-2)));
    }
    
    public function test_cohort_enroll_more_people_course() {
        global $CFG, $DB;

        $this->resetAfterTest(true);
        
        // add engagement message
        $message = new stdClass();
        $message->name = 'cohort-enroll-more-people-course';
        $message->identifier = 'cohort-course-no-participants';
        $message->template = 'cohort-enroll-more-people-course';
        $message->userfilter = 'cohort-course-created-days-ago';
        $message->type = 'time';
        $message->delay = 1;
        $message->percourse = 1;
        $message->status = 1;
        $message->timemodified = time();
        $DB->insert_record('monorail_engagement_messages', $message);

        // data and config
        $this->init_settings();
        $this->init_cohort_user();

        // trigger engagement messages
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
        
        // check that nothing was sent
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-5)), "Email was sent");

        // add course to cohort and add a member also
        $course1 = $this->getDataGenerator()->create_course();
        $cohort = $DB->get_record('cohort', array('name'=>'Personal Support Cohort'));
        $user = $DB->get_record('user', array('username'=>'user@test.127.0.0.1'));
        $record = new stdClasS();
        $record->cohortid = $cohort->id;
        $record->userid = $user->id;
        $record->timeadded = time();
        $DB->insert_record('cohort_members', $record);
        // add to admins, because otherwise course will not be linked to cohort...
        $record = new stdClass();
        $record->cohortid = $cohort->id;
        $record->adminid = $user->id;
        $record->courseid = $course1->id;
        $DB->insert_record('monorail_cohort_courseadmins', $record);
        // enrol teacher
        require_once($CFG->dirroot . '/enrol/externallib.php');  
        $enrol = enrol_get_plugin('manual'); 
        $enrolinstances = enrol_get_instances($course1->id, true);
        foreach ($enrolinstances as $courseenrolinstance) {
            if ($courseenrolinstance->enrol == "manual") {
                $instance = $courseenrolinstance;
                break;
            }
        }
        $role = $DB->get_record('role', array('shortname' => 'editingteacher'));
        $enrol->enrol_user($instance, $user->id, $role->id, time(), 0, ENROL_USER_ACTIVE);
        
        // trigger engagement messages
        engagements_process();
        // check that nothing was sent
        $this->assertFalse($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-5)), "Email was sent");
        
        // rewind course creation
        $course = $DB->get_record('course', array('id'=>$course1->id));
        $course->timecreated = time() - 86400;
        $DB->update_record('course', $course);
        
        // trigger engagement messages
        engagements_process();
        // check that email was sent
        $this->assertTrue($DB->record_exists_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-5)), "Email was not sent");
        // trigger engagement messages
        engagements_process();
        // check that user was sent only one email
        $this->assertEquals(1, $DB->count_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-10)), "Only one email should have been sent");
        
        // add new course to cohort and add a member also
        $course2 = $this->getDataGenerator()->create_course();
        $record = new stdClass();
        $record->cohortid = $cohort->id;
        $record->adminid = $user->id;
        $record->courseid = $course2->id;
        $DB->insert_record('monorail_cohort_courseadmins', $record);
        // add user to cohort
        $user2 = $this->getDataGenerator()->create_user(array('email'=>'user2@test.127.0.0.1', 'username'=>'user2@test.127.0.0.1'));
        $record = new stdClasS();
        $record->cohortid = $cohort->id;
        $record->userid = $user2->id;
        $record->timeadded = time();
        $DB->insert_record('cohort_members', $record);
        // enrol student
        require_once($CFG->dirroot . '/enrol/externallib.php');  
        $enrol = enrol_get_plugin('manual'); 
        $enrolinstances = enrol_get_instances($course2->id, true);
        foreach ($enrolinstances as $courseenrolinstance) {
            if ($courseenrolinstance->enrol == "manual") {
                $instance = $courseenrolinstance;
                break;
            }
        }
        $role = $DB->get_record('role', array('shortname' => 'student'));
        $enrol->enrol_user($instance, $user2->id, $role->id, time(), 0, ENROL_USER_ACTIVE);
        // enrol teacher
        require_once($CFG->dirroot . '/enrol/externallib.php');  
        $enrol = enrol_get_plugin('manual'); 
        $enrolinstances = enrol_get_instances($course2->id, true);
        foreach ($enrolinstances as $courseenrolinstance) {
            if ($courseenrolinstance->enrol == "manual") {
                $instance = $courseenrolinstance;
                break;
            }
        }
        $role = $DB->get_record('role', array('shortname' => 'editingteacher'));
        $enrol->enrol_user($instance, $user->id, $role->id, time(), 0, ENROL_USER_ACTIVE);
        
        // rewind course creation
        $course = $DB->get_record('course', array('id'=>$course2->id));
        $course->timecreated = time() - 86400;
        $DB->update_record('course', $course);
        // trigger engagement messages
        engagements_process();
        // check that user was sent only one email
        $this->assertEquals(1, $DB->count_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user@test.127.0.0.1', time()-60)), "Only one email should have been sent");
        
        // add user to cohort
        $user3 = $this->getDataGenerator()->create_user(array('email'=>'user3@test.127.0.0.1', 'username'=>'user3@test.127.0.0.1'));
        $record = new stdClasS();
        $record->cohortid = $cohort->id;
        $record->userid = $user3->id;
        $record->timeadded = time();
        $DB->insert_record('cohort_members', $record);
        // enrol teacher
        require_once($CFG->dirroot . '/enrol/externallib.php');  
        $enrol = enrol_get_plugin('manual'); 
        $enrolinstances = enrol_get_instances($course2->id, true);
        foreach ($enrolinstances as $courseenrolinstance) {
            if ($courseenrolinstance->enrol == "manual") {
                $instance = $courseenrolinstance;
                break;
            }
        }
        $role = $DB->get_record('role', array('shortname' => 'editingteacher'));
        $enrol->enrol_user($instance, $user3->id, $role->id, time(), 0, ENROL_USER_ACTIVE);
        
        // trigger engagement messages
        engagements_process();
        // check that user3 was sent email
        $this->assertEquals(1, $DB->count_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user3@test.127.0.0.1', time()-60)), "Email was not sent");
        
        // print_r($DB->get_records_select('monorail_sent_emails', "type = ? and toemail = ? and timecreated > ?", array('engagement', 'user3@test.127.0.0.1', time()-60)));
    }
 }
 
 
 ?>