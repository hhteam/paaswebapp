<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


	require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
	require_once($CFG->dirroot . '/my/lib.php');
	require_once($CFG->dirroot . '/tag/lib.php');
	require_once($CFG->dirroot . '/user/profile/lib.php');
	require_once($CFG->libdir.'/filelib.php');

	$userid   = optional_param('id', $USER->id, PARAM_INT);    // user id

	require_login();

	$user = $DB->get_record('user', array('id' => $userid));

	$PAGE->set_url('/theme/monorail/user_settings.php', array('id' => $userid));
	$PAGE->set_pagelayout('standard');
	$PAGE->set_context(get_system_context());
        $OUTPUT->set_title_block(get_string('settings', 'theme_monorail'));

	// Guest can not edit
	if (isguestuser()) {
		print_error('guestnoeditprofile');
	}

	// The user profile we are editing
	if (!$user = $DB->get_record('user', array('id'=>$userid))) {
		print_error('invaliduserid');
	}

	// Guest can not be edited
	if (isguestuser($user)) {
		print_error('guestnoeditprofile');
	}

	// remote users cannot be edited
	if (is_mnet_remote_user($user)) {
		if (user_not_fully_set_up($user)) {
			$hostwwwroot = $DB->get_field('mnet_host', 'wwwroot', array('id'=>$user->mnethostid));
			print_error('usernotfullysetup', 'mnet', '', $hostwwwroot);
		}
		redirect($CFG->magic_ui_root . "/courses/{$course->id}/participants");
	}

	// load the appropriate auth plugin
	$userauth = get_auth_plugin($user->auth);

	if (!$userauth->can_edit_profile()) {
		print_error('noprofileedit', 'auth');
	}

	if ($editurl = $userauth->edit_profile_url()) {
		// this internal script not used
		redirect($editurl);
	}

	/// Display page header
	$strsettings 		= get_string('settings', 'theme_monorail');

	$PAGE->set_title($strsettings);
	$PAGE->set_heading($strsettings);
	$PAGE->add_body_class('user-settings currentuser');
        $PAGE->requires->data_for_js('data', array('userid' => $user->id));
    
        $SESSION->wantsurl = $CFG->magic_ui_root . "/settings";
    
	echo $OUTPUT->header();

	echo '<div class="desc-title">' . get_string('password') . '</div>';

	echo '<div class="desc-title clickable clickbox btn btn-primary" id="btChangePassword">';
	echo '<a href="'.$CFG->magic_ui_root.'/settings">';
	echo get_string('change_password', 'theme_monorail') . '</a></div>';

        $languages = get_string_manager()->get_list_of_translations();
	echo '<div class="desc-title">' . get_string('change_language', 'theme_monorail') . '</div>';
        echo '<div><select id="languageselect">';
        foreach ($languages as $key => $value) {
            $select = ''; 
            if($user->lang == $key){
               $select = 'selected';
            } 
            echo '<option '.$select.' value='.$key.'>'.$value.'</option>';
        }       
        echo '</select></div>';
  
	echo $OUTPUT->footer();
