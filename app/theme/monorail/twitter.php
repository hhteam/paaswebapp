<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(dirname(dirname(__FILE__))).'/config.php');

require_once(dirname(__FILE__).'/twitter/twitteroauth.php');


// verify that user is logged in
try {
        require_login(null, false, null, false, true);
} catch (Exception $ex) {
        // not logged in, just die
        die();
}

//Check if credentials exist and are valid !!

$pic = optional_param('p', false, PARAM_BOOL);

try {
    $connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET);

    $temporary_credentials = $connection->getRequestToken(TWITTER_OAUTH_CALLBACK);
    /* Save temporary credentials to session. */
    $_SESSION['oauth_token'] = $token = $temporary_credentials['oauth_token'];
    $_SESSION['oauth_token_secret'] = $temporary_credentials['oauth_token_secret'];
    if($pic) {
        $_SESSION['sgetpic'] = $pic;
    }
    header('Location: ' . $connection->getAuthorizeURL($temporary_credentials, FALSE));
} catch (Exception $ex) {
    die();
}
