<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


function monorail_get_user_fullname() {
       global $USER, $DB;

       $user = $DB->get_record('user', array('id'=>$USER->id));
       return fullname($user, true);
}

function monorail_get_logout_url() { 
	global $CFG, $OUTPUT, $SESSION;

        $url = "<a href=\"$CFG->wwwroot/theme/monorail/cleanup.php\">".get_string('logout')."</a>";

	return $url;
}

function monorail_get_course_background_tn($coursebackground,$course_id, $local = false) {
	global $CFG, $DB;

    $default = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
    if($local) {
      $default = $CFG->magic_ui_dir . "/app/img/stock/other-tn.jpg";
    } 
    try {
        $bcktn  = monorail_data('TEXT', $course_id, 'coursebackgroundtncustom');
        $bcktn = reset($bcktn);

        if($bcktn && $bcktn->value)  {
           if($local) {
             return $CFG->dirroot . $bcktn->value;
           } else {
             return $CFG->wwwroot . $bcktn->value;
           }
        }

	if (!$coursebackground) {
            if($local) {
              return $CFG->magic_ui_dir . "/app/img/stock/other-tn.jpg";
            }
	    return $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
	}

        $spos = strrpos($coursebackground, "/app/img/stock/");

        if ($spos !== FALSE)
        {
            if($local) {
              $filename = basename($coursebackground,'.jpg');
              return $CFG->magic_ui_dir . "/app/img/stock/".$filename."-tn.jpg";
            }
            return str_replace('.jpg','-tn.jpg', $coursebackground);
        } else if(strrpos($coursebackground, "public_images/course_logo") !== FALSE){
           // User custom image , need to check if thumbnail exists
           $info = pathinfo($coursebackground);
           $filename = basename($coursebackground,'.'.$info['extension']);
           $tnfileurl = $CFG->dirroot . '/public_images/course_logo/'.$filename.'-tn.jpg';
           if(file_exists($tnfileurl)) {
               if($local) {
                 return $CFG->dirroot . '/public_images/course_logo/' . $filename.'-tn.jpg';
               }
               return $CFG->wwwroot . '/public_images/course_logo/' . $filename.'-tn.jpg';
           } else {
               $tnimage = new Imagick($coursebackground);
               $tnimage->cropThumbnailImage(267 , 150 );
               $tnimage->writeImage($tnfileurl);
               if($local) {
                 return $CFG->dirroot . '/public_images/course_logo/' . $filename.'-tn.jpg';
               }
               return $CFG->wwwroot . '/public_images/course_logo/' . $filename.'-tn.jpg';
           }
        }
    } catch (Exception $ex) {
      return $default;
    }
    return $default;
}

function monorail_get_course_background($courseid = NULL) {
	global $CFG, $PAGE, $OUTPUT, $DB;

	if (!$courseid) {
		$courseid = $PAGE->course->id;
	}

	$data = monorail_data('TEXT', $courseid, 'coursebackground');

	if (!empty($data)) {
		$data = reset($data);
		$id = $data->value;
        $spos = strrpos($id, "/");

        if ($spos !== FALSE)
        {
            $filename = substr($id, $spos + 1);

            if (empty($filename))
            {
		        return $CFG->magic_ui_root . "/app/img/stock/other.jpg";
            }
            else if (preg_match("/^\\d+$/", $filename))
            {
                $id = $filename;
            }
            else
            {
                return $id;
            }
        }

		$file = $DB->get_record('monorail_course_background', array('id'=>$id));

		$context = context_course::instance(SITEID);
		$url = moodle_url::make_pluginfile_url($context->id, 'course', 'summary', null, '/', $file->id);
		return $url->out(false);
	}
	else {
		return $CFG->magic_ui_root . "/app/img/stock/other.jpg";
	}
}

function monorail_get_user_picture() {
	global $USER, $OUTPUT;

	$output  = $OUTPUT->user_picture($USER, array('size' => 31, 'link'=>false));
        $result = preg_match('/<img src="([^"]+)"/i', $output, $matches);
        if(($result == 1) && $matches[1]) {
            $output = str_replace($matches[1], $matches[1].'?ver='.time(), $output);
        }
	return $output;
}

/* get hash that maps to user picture on drive
   does no control checks here */
function monorail_get_userpic_hash($userid) {
    $hash = monorail_data('user', $userid, 'pichash');
    if (! empty($hash)) {
        return reset($hash)->value;
    } else {
        return 'default';
    }
}

function monorail_get_current_page() {
	global $PAGE;
	$selected = array('tasks' => '', 'forum' => '', 'overview' => '', 'participants' => '');

	if (stripos($PAGE->pagetype, 'theme-monorail-tasks', 0) === 0) {
		$selected['tasks'] = 'selected';
	}
	else if (stripos($PAGE->pagetype, 'theme-monorail-forum', 0) === 0) {
		$selected['forum'] = 'selected';
	}
	else if (stripos($PAGE->pagetype, 'course-view', 0) === 0) {
		$selected['overview'] = 'selected';
	}
	else if (stripos($PAGE->pagetype, 'theme-monorail-participants', 0) === 0) {
		$selected['participants'] = 'selected';
	}
	return $selected;
}

function monorail_get_assignment_status() {
	global $PAGE, $CFG, $DB, $USER;

	// Only show duedate in assign page
	if ($PAGE->pagetype != 'mod-assign-view') {
		return;
	}

	require_once($CFG->dirroot . '/mod/assign/locallib.php');
	$id = optional_param('id', null, PARAM_INT);

	if ($id) {
		$cm 	= get_coursemodule_from_id('assign', $id, 0, false, MUST_EXIST);
		$assign	= new assign($PAGE->context,$cm,$PAGE->course);

		$due		= $assign->get_instance()->duedate;
		$submitted 	= $DB->get_record('assign_submission', array('assignment'=>$id, 'userid'=>$USER->id));

		$grading_info = grade_get_grades($PAGE->course->id, 'mod', 'assign', $cm->instance, $USER->id);
		if (isset($grading_info->items[0]) && !$grading_info->items[0]->grades[$USER->id]->hidden ) {
			$grade = $grading_info->items[0]->grades[$USER->id]->str_grade;

			if ($grade == '-') {
				$grade = NULL;
			}
		}
		else {
			$grade =  NULL;
		}

		$str	= '<div class="course-info"><span class="course-title">'.$PAGE->course->fullname.'</span> <span class="course-code">('.$PAGE->course->idnumber.')</span>';

		// Check if submitted or graded
		if ($submitted == NULL && $grade == NULL) {
			if ($due) {
				$date = userdate($due);
				$str .= '<span class="task-due floatright">'.get_string('duedate', 'assign').': '.userdate($due, '%d.%m.%Y, %H:%M');

				// Calculate and show how much time is left for the assignment
				$timeleft = date_diff(new DateTime($date), new DateTime(date("m/d/Y h:i:s a", time())));
				if ($timeleft->invert == 1) {
					$str .= " <span class='bold'>(";
					if ($timeleft->days > 0) {
						$str .= $timeleft->days . ' ' . get_string('days');
					}
					else {
						$str .= $timeleft->h . ' ' . get_string('hours') . ' ' . $timeleft->i . ' ' . get_string('minutes');
					}
					$str .= ")&nbsp;</span>";
				}
				// In case of overdue assignment
				else {
					$str .= " <span class='bold urgent'>(";
					if ($timeleft->days > 0) {
						$str .= $timeleft->days . ' ' . get_string('days');
					}
					else {
						$str .= $timeleft->h . ' ' . get_string('hours') . ' ' . $timeleft->i . ' ' . get_string('minutes');
					}
					$str .= " ".get_string('late', 'theme_monorail').")</span>";
				}

				$str .= '</span>';
			} else {
				$str .= '<span class="task-due floatright">'.$strduedateno.'</span>';
			}
		}
		else {
			$str .= '<span class="task-grade floatright">';
			if ($grade) {
				$str .= get_string('graded', 'theme_monorail') . $grade;
			}
			else {
				$str .= get_string('ungraded', 'theme_monorail');
			}
			$str .= '</span>';
		}
		$str .= '</div>';

		return $str;
	}
}

function monorail_get_assignment_feedback() {
	global $PAGE, $CFG, $DB, $USER;

	// Only show feedback in assign page
	if ($PAGE->pagetype != 'mod-assign-view') {
		return;
	}

	require_once($CFG->dirroot . '/mod/assign/locallib.php');
	$id = optional_param('id', null, PARAM_INT);

	if ($id) {
		$cm 		= get_coursemodule_from_id('assign', $id, 0, false, MUST_EXIST);
		$assign		= new assign($PAGE->context,$cm,$PAGE->course);
		$comments 	= $assign->get_feedback_plugin_by_type('comments');
		$file		= $assign->get_feedback_plugin_by_type('file');

		$grading_info = grade_get_grades($PAGE->course->id, 'mod', 'assign', $cm->instance, $USER->id);
		if (isset($grading_info->items[0]) && !$grading_info->items[0]->grades[$USER->id]->hidden ) {
			$grade = $grading_info->items[0]->grades[$USER->id];
		}
		else {
			$grade =  NULL;
		}


		if ($comments) {
			if ($comments->is_enabled() && $comments->is_visible() && $grade) {
				$str = '<div id="assign-feedback"><h3 class="main">'.get_string('feedback', 'assign').'</h3>';
				$str .= "<div class='feedback-content'>$grade->str_feedback</div>";
				$str .= '</div>';

				return $str;
			}
		}
	}
}

function monorail_goback_link() {
	global $PAGE, $CFG;

	if ($PAGE->pagetype == 'theme_monorail_assign_details') {
		return $CFG->magic_ui_root.'/courses/'.$PAGE->course->id.'/tasks';
    } else if ($PAGE->pagetype == 'theme-monorail-book') {
        return $CFG->magic_ui_root.'/courses/'.$PAGE->course->id;
    } else if (strstr($PAGE->pagetype, 'mod-quiz')) {
    	return $CFG->magic_ui_root.'/courses/'.$PAGE->course->id.'/tasks';
	} else if ($PAGE->pagetype == 'user-profile' && $PAGE->course->id != SITEID) {
		if (strstr($_SERVER["HTTP_REFERER"], $CFG->wwwroot.'/theme/monorail/index.php')) { // hack alert
			// User come from Participants page
			return $_SERVER["HTTP_REFERER"];
		} else {
			return $CFG->magic_ui_root.'/courses/'.$PAGE->course->id;
		}
	} else {
		return $CFG->magic_ui_root;
	}
}

function monorail_get_password_policy() {
	global $CFG, $USER;

	require_once($CFG->dirroot . '/lib/weblib.php');

	$text  = '<b>' . get_string('changepassworduser', 'theme_monorail') . ' ' . $USER->username.'</b><br/>';
	$text .= print_password_policy();
	return $text;
}

/**
 * Print a forum post
 *
 * @global object
 * @global object
 * @uses FORUM_MODE_THREADED
 * @uses PORTFOLIO_FORMAT_PLAINHTML
 * @uses PORTFOLIO_FORMAT_FILE
 * @uses PORTFOLIO_FORMAT_RICHHTML
 * @uses PORTFOLIO_ADD_TEXT_LINK
 * @uses CONTEXT_MODULE
 * @param object $post The post to print.
 * @param object $discussion
 * @param object $forum
 * @param object $cm
 * @param object $course
 * @param boolean $ownpost Whether this post belongs to the current user.
 * @param boolean $reply Whether to print a 'reply' link at the bottom of the message.
 * @param boolean $link Just print a shortened version of the post as a link to the full post.
 * @param int $post_read true, false or -99. If we already know whether this user
 *          has read this post, pass that in, otherwise, pass in -99, and this
 *          function will work it out.
 * @param bool|null $hidecommands
 * @param bool|null $istracked
 * @param bool|null $isvoid
 */
function monorail_print_post($post, $discussion, $forum, &$cm, $course, $ownpost=false, $reply=false, $link=false,
$postisread=null, $hidecommands=false, $istracked=null, $isvoid=false) {
	global $USER, $CFG, $OUTPUT;

	require_once($CFG->libdir . '/filelib.php');

	// String cache
	static $str;

	$modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);

	$post->course = $course->id;
	$post->forum  = $forum->id;
	$post->message = file_rewrite_pluginfile_urls($post->message, 'pluginfile.php', $modcontext->id, 'mod_forum', 'post', $post->id);

	// caching
	if (!isset($cm->cache)) {
		$cm->cache = new stdClass;
	}

	if (!isset($cm->cache->caps)) {
		$cm->cache->caps = array();
		$cm->cache->caps['mod/forum:viewdiscussion']   = has_capability('mod/forum:viewdiscussion', $modcontext);
		$cm->cache->caps['moodle/site:viewfullnames']  = has_capability('moodle/site:viewfullnames', $modcontext);
		$cm->cache->caps['mod/forum:editanypost']      = has_capability('mod/forum:editanypost', $modcontext);
		$cm->cache->caps['mod/forum:deleteownpost']    = has_capability('mod/forum:deleteownpost', $modcontext);
		$cm->cache->caps['mod/forum:deleteanypost']    = has_capability('mod/forum:deleteanypost', $modcontext);
	}

	if (!isset($cm->uservisible)) {
		$cm->uservisible = coursemodule_visible_for_user($cm);
	}

	if ($istracked && !$postisread) {
		$postisread = forum_tp_is_post_read($USER->id, $post);
	}

	if (empty($str)) {
		$str = new stdClass;
		$str->edit         = get_string('edit', 'forum');
		$str->delete       = get_string('delete', 'forum');
		$str->reply        = get_string('reply', 'forum');
	}

	// Build an object that represents the posting user
	$postuser = new stdClass;
	$postuser->id        = $post->userid;
	$postuser->firstname = $post->firstname;
	$postuser->lastname  = $post->lastname;
	$postuser->imagealt  = $post->imagealt;
	$postuser->picture   = $post->picture;
	$postuser->email     = $post->email;
	// Some handy things for later on
	$postuser->fullname    = fullname($postuser, $cm->cache->caps['moodle/site:viewfullnames']);
	$postuser->profilelink = new moodle_url('/theme/monorail/user.php', array('id'=>$post->userid, 'course'=>$course->id));

	// Prepare the attachements for the post, files then images
	list($attachments, $attachedimages) = forum_print_attachments($post, $cm, 'separateimages');

	// Hack for allow to edit news posts those are not displayed yet until they are displayed
	$commands = array();
	$age = time() - $post->created;
	if (!$post->parent && $forum->type == 'news' && $discussion->timestart > time()) {
		$age = 0;
	}
	if (($ownpost && $age < $CFG->maxeditingtime) || $cm->cache->caps['mod/forum:editanypost']) {
		$commands[] = array('url'=>'javascript:void(0)',
				array(), 'text'=>$str->edit, 'attrs'=> array('onclick'=>'monorail_forum_editpost('.$post->id.');'));
	}

	if (($ownpost && $age < $CFG->maxeditingtime && $cm->cache->caps['mod/forum:deleteownpost']) || $cm->cache->caps['mod/forum:deleteanypost']) {
		$commands[] = array('url'=>new moodle_url('/theme/monorail/forum.php',
				array('delete'=>$post->id, 'action'=>'post','c'=>$forum->course)), 'text'=>$str->delete);
	}

// 	if ($reply) {
// 		$commands[] = array('url'=>new moodle_url('/mod/forum/post.php', array('reply'=>$post->id)), 'text'=>$str->reply);
// 	}
	// Finished building commands


	// Begin output

	$output  = '';

	$topicclass = '';
	if (empty($post->parent)) {
		$topicclass = ' firstpost starter';
	}

	$row 	= new html_table_row();
	$cell 	= new html_table_cell();

	$cell->attributes['class'] = 'forum-post';
	$cell->id				   = 'p' . $post->id;

	$cell->text .= "<span class='anchor' id='post$post->id'></span>";
	$cell->text .= "<div class='post-header'>";
	$cell->text .= html_writer::link($postuser->profilelink, $OUTPUT->user_picture($postuser, array('courseid'=>$course->id, 'size'=>37, 'link'=>false)));
	$cell->text .= '<div class="post-header-text"><b>' . html_writer::link($postuser->profilelink, $postuser->fullname).'</b>';
	$cell->text .= "<br/>".userdate($post->modified, '%d.%m.%Y, %H:%M')."</div>";

	if (count($commands) && $hidecommands == false) {
		$commandhtml = array();
		foreach ($commands as $command) {
			if (is_array($command)) {
				$commandhtml[] = html_writer::link($command['url'], $command['text'], $command['attrs']);
			} else {
				$commandhtml[] = $command;
			}
		}
		$cell->text .= html_writer::tag('div', implode(' | ', $commandhtml), array('class'=>'commands'));
	}

	$cell->text .= "</div>";


	$cell->text .= "<div class='post-content'>";

	$options = new stdClass;
	$options->para    = false;
	$options->trusted = $post->messagetrust;
	$options->context = $modcontext;

	// Prepare whole post
	$postcontent  = format_text($post->message, $post->messageformat, $options, $course->id);
    if ($post->messageformat == FORMAT_HTML) {
        $postcontent = nl2br($postcontent);
    }
    $postcontent = str_replace('  ', '&nbsp; ', $postcontent);
	$postcontent .= html_writer::tag('div', $attachedimages, array('class'=>'attachedimages'));

	$cell->text .= html_writer::tag('div', $postcontent, array('class'=>'posting'));

	if (!empty($attachments)) {
		$attachmentslist = explode('<br />', $attachments);
		array_pop($attachmentslist);
		$postcontent = '';
		foreach ($attachmentslist as $attachment) {
			$postcontent .= html_writer::tag('div', $attachment, array('class'=>'attachment clickable'));
		}
		$cell->text .= html_writer::tag('div', $postcontent, array('class'=>'attachment-list'));
	}

	$cell->text .= "</div>";

	forum_tp_mark_post_read($USER->id, $post, $forum->id);

	$row->cells[] = $cell;

	if ($isvoid) {
		print $cell->text;
	}
	return $row;
}

function sort_using_due_date($a, $b) {
    if (!$a->duedate) return 1;
    if (!$b->duedate) return -1;
    if ( $a->duedate < $b->duedate ) return -1;
    if ( $a->duedate > $b->duedate ) return 1;
    return 0; // equality
}

/**
 * Save a forum post
 * 
 * Note! Calling component MUST handle logged in checks
 * Here we will check for actual posting access
 * 
 * @param int $discussionid
 * @param str $subject  (can be empty unless we are saving a new discussion)
 * @param text $message
 * @param int $reply    (id of post we are replying to, if any)
 * @param int $forumid  (id of forum IF this is a new discussion, otherwise should be 0)
 * @param int $groupid  (NOT SUPPORTED YET, leave null)
 * @param int $edit     (id of post that is being edited, if editing, otherwise null)
 * @return int post ID that was created
 * @throws exception if post cannot be created
 */
function monorail_forum_save_post($discussionid, $subject, $message, $reply, $forumid = 0, $groupid = null, $edit=null) {
    require_once(dirname(__FILE__) . '/../../config.php');
    
    global $DB, $CFG, $USER;
    require_once($CFG->dirroot . '/mod/forum/lib.php');
    require_once($CFG->libdir.'/completionlib.php');
    require_once($CFG->dirroot . '/local/monorailfeed/lib.php');
    
    $action = null;
    
    // checks depending on action
    if (!empty($edit)) {
        if (! $post = forum_get_post_full($edit)) {
            print_error('invalidpostid', 'forum');
        }
        if ($post->parent) {
            if (! $parent = forum_get_post_full($post->parent)) {
                print_error('invalidparentpostid', 'forum');
            }
        }
        $action = 'edit';
        $discussionid = $post->discussion;
    }
    if ($action == null && $discussionid == 0 && (!isset($forumid) || $forumid == 0)) {
        // not possible to open new discussion without forum id!
        print_error('invalidforumid', 'forum');
    }
    if ($discussionid != 0) {
        if (! $discussion = $DB->get_record("forum_discussions", array("id" => $discussionid))) {
            print_error('notpartofdiscussion', 'forum');
        } else {
            $forumid = $discussion->forum;
        }
    }
    if (!empty($reply)) {
        if (! $parent = forum_get_post_full($reply)) {
            print_error('invalidparentpostid', 'forum');
        }
        if (! $discussion = $DB->get_record("forum_discussions", array("id" => $parent->discussion))) {
            print_error('notpartofdiscussion', 'forum');
        }
        $action = 'reply';
    }
    
    // general checks
    if (! $forum = $DB->get_record("forum", array("id" => $forumid))) {
        print_error('invalidforumid', 'forum');
    }
    if (! $course = $DB->get_record("course", array("id" => $forum->course))) {
        print_error('invalidcourseid');
    }
    if (! $cm = get_coursemodule_from_instance("forum", $forum->id, $course->id)) {
        print_error("invalidcoursemodule");
    }
    
    // context
    $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
    $modcontext    = get_context_instance(CONTEXT_MODULE, $cm->id);

    // access
    if (! forum_user_can_post_discussion($forum, $groupid, -1, $cm)) {
        print_error('nopostforum', 'forum');
    }
    if (!$cm->visible and !has_capability('moodle/course:viewhiddenactivities', $coursecontext)) {
        print_error("activityiscurrentlyhidden");
    }
    if ($action == 'edit') {
        if (!($forum->type == 'news' && !$post->parent && $discussion->timestart > time())) {
            if (((time() - $post->created) > $CFG->maxeditingtime) and
                        !has_capability('mod/forum:editanypost', $modcontext)) {
                print_error('maxtimehaspassed', 'forum', '', format_time($CFG->maxeditingtime));
            }
        }
        if (($post->userid <> $USER->id) and
                    !has_capability('mod/forum:editanypost', $modcontext)) {
            print_error('cannoteditposts', 'forum');
        }
    }
    
    // Make sure user can post here
    if (isset($cm->groupmode) && empty($course->groupmodeforce)) {
        $groupmode =  $cm->groupmode;
    } else {
        $groupmode = $course->groupmode;
    }
    if ($groupmode == SEPARATEGROUPS and !has_capability('moodle/site:accessallgroups', $modcontext)) {
        if (isset($discussion)) {
            if ($discussion->groupid == -1) {
                print_error('nopostforum', 'forum');
            } else {
                if (!groups_is_member($discussion->groupid)) {
                    print_error('nopostforum', 'forum');
                }
            }
        } else {
            print_error('nopostforum', 'forum');
        }
    }
    
    if (!$action) {
        $action = 'new';
    }
    
    // Post variable
    // Load up the $post variable.
    if ($action != 'edit') {
        $post = new stdClass();
        $post->course        = $course->id;
        $post->forum         = $forum->id;
        $post->userid        = $USER->id;
        $post->messageformat = FORMAT_HTML;
        $post->messagetrust  = 0;
        $post->created    = time();
        $post->mailed     = "0";
    } else {
        $post->edit   = $edit;
        $post->course = $course->id;
        $post->forum  = $forum->id;
        $post->groupid = ($discussion->groupid == -1) ? 0 : $discussion->groupid;
        $post = trusttext_pre_edit($post, 'message', $modcontext);
        //~ $post->subject = $subject;  // editing subject not supported yet
    }
    if ($action == 'new') {
        $post->discussion    = 0;           // ie discussion # not defined yet
        $post->parent        = 0;
        if (isset($groupid)) {
            $post->groupid = $groupid;
        } else {
            $post->groupid = groups_get_activity_group($cm);
        }
        $post->subject       = $subject;
    }
    if ($action == 'reply') {
        $post->discussion = $parent->discussion;
        $post->parent = $parent->id;
        $post->subject     = $parent->subject;
        $post->groupid = ($discussion->groupid == -1) ? 0 : $discussion->groupid;
        $strre = get_string('re', 'forum');
        if (!(substr($post->subject, 0, strlen($strre)) == $strre)) {
            $post->subject = $strre.' '.$post->subject;
        }
    }
    
    // actual message
    $post->message       = $message['text'];   
    if ($action == 'edit') {
        if ($USER->id != $post->userid) {   // Not the original author, so add a message to the end
            $data->date = userdate($post->modified);
            if ($post->messageformat == FORMAT_HTML) {
                $data->name = '<a href="'.$CFG->wwwroot.'/user/index.php?id='.$USER->id.'&course='.$post->course.'">'.
                               fullname($USER).'</a>';
                $post->message .= '<p>(<span class="edited">'.get_string('editedby', 'forum', $data).'</span>)</p>';
            } else {
                $data->name = fullname($USER);
                $post->message .= "\n\n(".get_string('editedby', 'forum', $data).')';
            }
        }        
    }
    
    // other fields
    $post->modified = time();
    
    // save
    if ($action == 'edit') {
        $DB->update_record('forum_posts', $post);
        if (!$post->parent) {   // Post is a discussion starter - update discussion title and times too
            $discussion->name      = $post->subject;
            if (isset($post->timestart)) {
                $discussion->timestart = $post->timestart;
                $discussion->timeend   = $post->timeend;
            }
        }
        $discussion->timemodified = $post->modified; // last modified tracking
        $discussion->usermodified = $post->userid;   // last modified tracking
        $DB->update_record('forum_discussions', $discussion);
    } else {
        $post->id = $DB->insert_record("forum_posts", $post);
        if ($action == 'new') {
            $discussion = new stdClass();
            $discussion->course         = $post->course;
            $discussion->forum          = $post->forum;
            $discussion->name           = $post->subject;
            $discussion->firstpost    = $post->id;
            $discussion->timemodified = time();
            $discussion->usermodified = $post->userid;
            $discussion->userid       = $USER->id;
            $discussion->groupid        = $post->groupid;
            $post->discussion = $DB->insert_record("forum_discussions", $discussion);
            
            $DB->set_field("forum_posts", "discussion", $post->discussion, array("id"=>$post->id));
        } else {
            $DB->set_field("forum_discussions", "timemodified", $post->modified, array("id" => $post->discussion));
            $DB->set_field("forum_discussions", "usermodified", $post->userid, array("id" => $post->discussion));
        }
    }
    
    add_to_log($course->id, "monorail", "monorail_forum_save_post", "", "$action post '$subject' to forum $forum->id");

    // create notifications
    if ($action != "edit") {
        try {
            monorailfeed_create_notification($course->id, 'post', 'forum', $post->id, $USER->id);
        } catch (Exception $ex) {
            add_to_log($course->id, 'monorail', 'lib.monorail_forum_save_post', '', 'Error! Failed to create a notification: '.get_class($ex).': '.$ex->getMessage());
        }
    }

    // tracking
    if (forum_tp_can_track_forums($forum) && forum_tp_is_tracked($forum)) {
        forum_tp_mark_post_read($post->userid, $post, $post->forum);
    }
    
    // Update completion state
    if ($action = 'new') {
        $completion=new completion_info($course);
        if($completion->is_enabled($cm) &&
            ($forum->completionreplies || $forum->completionposts)) {
            $completion->update_state($cm,COMPLETION_COMPLETE);
        }
    }
    
    return $post->id;
}

/**
 * Format bytes to human readable
 * From here: http://www.php.net/manual/en/function.filesize.php#106935
 * Author: yatsynych at gmail dot com
 * 
 * @param int $a_bytes
 * @return string
 */
function monorail_format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' kB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GB';
    }
}

/**
 * Get file url, converted if available
 * 
 * @param object $file
 * @return string
 */
function monorail_get_file_url($file) {
    global $CFG;
    $url = "{$CFG->wwwroot}/pluginfile.php/{$file->get_contextid()}/{$file->get_component()}/{$file->get_filearea()}";
    $filename = $file->get_filename();
    if ($filename != '.') {
        $fileurl = $url.$file->get_filepath().$file->get_itemid().'/'.$filename;
        if ($file->get_mimetype() == 'application/pdf') {
            $urlparts = parse_url($fileurl);
            $pdfurl = str_replace($urlparts['scheme'].'://'.$urlparts['host'], '', $fileurl);
            return '/pdfjs/web/viewer.html?file='.$pdfurl;
        } else {
            // if a pdf with the right contenthash exists in converted documents folder, use that
            $fileparts = pathinfo($filename);
            $pdfurl = monorail_converted_fileurl($file, true);
            if ($pdfurl) {
                // PDF found
                $urlparts = parse_url($fileurl);
                $fileurl = str_replace($urlparts['scheme'].'://'.$urlparts['host'], '', $fileurl);
                return '/pdfjs/web/viewer.html?original='.$fileurl.'&file='.$pdfurl;
            } else {
                // something else
                return $fileurl;
            }
        }
    }
    return false;
}

/**
 * Get record for embedding files to forum and course pages
 * 
 * @param object $file
 * @return array
 */
function monorail_file_embed_record($file, $customname = null, $customid = null) {
    global $CFG, $OUTPUT;
    $return = array();
    
    $url = "{$CFG->wwwroot}/pluginfile.php/{$file->get_contextid()}/{$file->get_component()}/{$file->get_filearea()}";
    $filename = $file->get_filename();
    if ($filename != '.') {
        if ($customname) {
            $name = $customname;
        } else {
            $name = $filename;
        }
        if ($customid) {    // note $file->get_itemid() should still always be used for pluginfile.php url generation!
            $itemid = $customid;
        } else {
            $itemid = $file->get_itemid();
        }
        $fileurl = $url.$file->get_filepath().$file->get_itemid().'/'.$filename;
        // depending on file type
        if (file_mimetype_in_typegroup($file->get_mimetype(), 'web_image')) {
            // image
            $return = array(
                'html' => "<a class='embedded-image' id='embedded-image-{$itemid}' href='{$fileurl}'><img src=\"{$fileurl}?preview=small\" alt=\"{$filename}\" title='".get_string('thumbnailclicktoenlarge', 'theme_monorail')."' /></a>",
                'type' => 'image', 'pdf' => false, 'converted' => false, 'convertedpath' => '', 'mimetype' => $file->get_mimetype()
                );
        } else if ($file->get_mimetype() == 'application/pdf') {
            // pdf
            $iconimage = $OUTPUT->pix_icon(file_file_icon($file), get_mimetype_description($file), 'moodle', array('class' => 'icon'));
            $urlparts = parse_url($fileurl);
            $pdfurl = str_replace($urlparts['scheme'].'://'.$urlparts['host'], '', $fileurl);
            $return = array(
                'html' => '<div class="attachment clickable" onclick="window.open(\'/pdfjs/web/viewer.html?file='.$pdfurl.'\', \'pdfjs\')">'.$iconimage.'<a href="javascript:void(0)">'.$name.'</a>'.'</div>',
                'type' => 'other', 'pdf' => true, 'converted' => false, 'convertedpath' => '', 'mimetype' => $file->get_mimetype()
                );
        } else {
            // if a pdf with the right contenthash exists in converted documents folder, use that
            $fileparts = pathinfo($filename);
            $pdfurl = monorail_converted_fileurl($file, true);
            if ($pdfurl) {
                // PDF found
                $iconimage = $OUTPUT->pix_icon(file_file_icon($file), get_mimetype_description($file), 'moodle', array('class' => 'icon'));
                $urlparts = parse_url($fileurl);
                $fileurl = str_replace($urlparts['scheme'].'://'.$urlparts['host'], '', $fileurl);
                $return = array(
                    'html' => '<div class="attachment clickable" onclick="window.open(\'/pdfjs/web/viewer.html?original='.$fileurl.'&file='.$pdfurl.'\', \'pdfjs\')">'.$iconimage.'<a href="javascript:void(0)">'.$name.'</a>'.'</div>',
                    'type' => 'other', 'pdf' => true, 'converted' => true, 'convertedpath' => $pdfurl, 'mimetype' => $file->get_mimetype()
                    );
            } else {
                // something else
                $iconimage = $OUTPUT->pix_icon(file_file_icon($file), get_mimetype_description($file), 'moodle', array('class' => 'icon'));
                $return = array(
                    'html' => '<div class="attachment clickable" onclick="window.open(\''.$fileurl.'\')">'.$iconimage.'<a href="javascript:void(0)">'.$name.'</a></div>',
                    'type' => 'other', 'pdf' => false, 'converted' => false, 'convertedpath' => '', 'mimetype' => $file->get_mimetype()
                    );
            }
        }
        $return['mimetype'] = $file->get_mimetype();
        $return['sizestr'] = monorail_format_bytes($file->get_filesize());
        $return['name'] = $name;
        $return['itemid'] = $itemid;
        $return['id'] = $file->get_id();
        return $return;
    }
    return null;
}

/**
 * Check if file can be converted
 * @param object $file
 * @return boolean
 */
function monorail_is_convertable_file($file) {
    $typegroups = array('spreadsheet', 'document', 'presentation');
    if (file_mimetype_in_typegroup($file->get_mimetype(), $typegroups))
        return true;
    else
        return false;
}

/**
 * Get file type
 * @param object $file
 * @return string, one of => spreadsheet, document, presentation, pdf, other
 */
function monorail_get_file_type($file) {
    if (strtolower(pathinfo($file->get_filename(), PATHINFO_EXTENSION)) === 'pdf') {
        return 'pdf';
    } else {
        $typegroups = array('spreadsheet', 'document', 'presentation');
        foreach ($typegroups as $typegroup) {
            if (file_mimetype_in_typegroup($file->get_mimetype(), $typegroup)) {
                return $typegroup;
            }
        }
    }
    return 'other';
}

/**
 * Trigger a document for conversion
 * Can be safely called for non-documents or pdfs, will return
 * false if conversion not triggered
 * @param object $file
 * @param str $path (path to file to be triggered)
 * $return boolean
 */
function monorail_trigger_document_conversion($file, $path) {
    global $CFG;
    if (monorail_is_convertable_file($file)) {
        // queue the file for conversion
        $path_parts = pathinfo($path);
        $extn = $path_parts['extension'];
        if(empty($extn)) {
           $extn = pathinfo($file->get_filename(), PATHINFO_EXTENSION); 
           if(empty($extn)) {
               return false;
           }
        }
        copy($path, $CFG->dataroot.'/monorailtempdocs/'.$file->get_contenthash().'.'.$extn);
        return true;
    }
    return false;
}

/**
 * Get converted file url, if any
 * Can be called for any file object, returns false
 * if no converted file exists
 * @param object $file
 * @return str or boolean
 */
function monorail_converted_fileurl($file, $stripdomain = false) {
    global $CFG;

    if (file_exists($CFG->dataroot.'/monorailpdf/'.$file->get_contenthash().'.pdf')) {
        $fileurl = $CFG->wwwroot.'/monorailpdf/'.$file->get_contenthash().'.pdf';
        if ($stripdomain) {
            $urlparts = parse_url($fileurl);
            $fileurl = str_replace($urlparts['scheme'].'://'.$urlparts['host'], '', $fileurl);
        }
        return $fileurl;
    }

    //TODO legacy, refactor these away
    $filename = $file->get_filename();
    $fileparts = pathinfo($filename);
    if (file_exists($CFG->dataroot.'/monorailpdf/'.$file->get_contenthash().'-'.str_replace(' ', '', str_replace($fileparts['extension'], 'pdf',  $filename)))) {
        $fileurl = $CFG->wwwroot.'/monorailpdf/'.$file->get_contenthash().'-'.str_replace(' ', '', str_replace($fileparts['extension'], 'pdf',  $filename));
        if ($stripdomain) {
            $urlparts = parse_url($fileurl);
            $fileurl = str_replace($urlparts['scheme'].'://'.$urlparts['host'], '', $fileurl);
        }
        return $fileurl;
    }

    // not found
    return false;
}

/**
 * Get or save data in monorail_data key/value table
 * If no value given, will just return current. Always returns current even if added a value.
 * NOTE! $replace = true will delete all other values
 * 
 * @param str $objecttype
 * @param int $itemid
 * @param str $datakey
 * @param str $value (optional, if not given current values will be returned)
 * @param boolean $replace (optional, if false, will enable multiple values of same key)
 * @param boolean $delete (optional, overrides $replace, just delete all the values with this key)
 * @return array (values of data)
 */
function monorail_data($objecttype, $itemid, $datakey, $value = null, $replace = true, $delete = false) {
    global $DB;
    
    if ($delete) {
        // just delete the values
        $DB->delete_records('monorail_data', array('type'=>$objecttype, 'itemid'=>$itemid, 'datakey'=>$datakey));
        $return = null;
    } else {
        // if value supplied, save it
        if ($value !== null) {
            if ($replace) {
                // just do a delete, better than selecting and then having to remove - we don't know how many there are
                $DB->delete_records('monorail_data', array('type'=>$objecttype, 'itemid'=>$itemid, 'datakey'=>$datakey));
            }
            $data = new stdClass();
            $data->type = $objecttype;
            $data->itemid = $itemid;
            $data->datakey = $datakey;
            $data->value = $value;
            $data->timemodified = time();
            $DB->insert_record('monorail_data', $data);  // don't check for exception, fall to calling component instead
        }
        
        // get all and return
        try {
            $return = $DB->get_records('monorail_data', array('type'=>$objecttype, 'itemid'=>$itemid, 'datakey'=>$datakey), null, 'id, value');
        } catch (Exception $ex) {
            // no data found
            $return = null;
        }
    }
    return $return;
}

/**
 * Trigger call via Events API - helper function
 * @param str $eventname
 * @param object $cm
 * @param object $instance
 * @param int $userid
 * @return boolean
 */
function monorail_trigger_event($eventname, $modulename, $cmid, $courseid, $userid) {
    // Trigger mod_created/mod_updated event with information about this module.
    $eventdata = new stdClass();
    $eventdata->modulename = $modulename;
    $eventdata->name       = $eventname;
    $eventdata->cmid       = $cmid;
    $eventdata->courseid   = $courseid;
    $eventdata->userid     = $userid;
    events_trigger($eventname, $eventdata);
}

/**
 * Get invitation record using existing code (must exist)
 * @param str $code
 * @return object or null
 */
function monorail_get_invitation($code) {
    global $DB;

    try {
        $row = $DB->get_record('monorail_invites', array('code'=>$code), '*', MUST_EXIST);
        return $row;
    } catch (Exception $ex) {
        return null;
    }
}

/**
 * Get or create an invitation
 * @param str $type
 * @param int $targetid
 * @return object
 */
function monorail_get_or_create_invitation($type, $targetid) {
    global $DB;
    
    // check first we don't already have this created, if we do just return it
    try {
        $row = $DB->get_record('monorail_invites', array('type'=>$type, 'targetid'=>$targetid), '*', MUST_EXIST);
        return $row;
    } catch (Exception $ex) {
        // create
        $codeneeded = true;
        while ($codeneeded):
            $code = uniqid('', true);
            $codeneeded = $DB->record_exists('monorail_invites', array('code'=>$code)); 
        endwhile;
        $row = new stdClass();
        $row->type = $type;
        $row->targetid = $targetid;
        $row->code = $code;
        $row->active = 1;
        $row->timemodified = time();
        $rowid = $DB->insert_record('monorail_invites', $row);
        $row = $DB->get_record('monorail_invites', array('id'=>$rowid), '*', MUST_EXIST);
        return $row;
    }
}

/**
 * Change invite link status
 * @param str $code
 * @param int $status
 * @return boolean
 */
function monorail_set_invite_status($code, $status) {
    global $DB;
    
    try {
        $row = $DB->get_record('monorail_invites', array('code'=>$code), '*', MUST_EXIST);
        $row->active = $status;
        $row->timemodified = time();
        return $DB->update_record('monorail_invites', $row);
    } catch (Exception $ex) {
        return false;
    }
}

/**
 * Format invite link
 * @param str $code
 * @return str
 */
function monorail_format_invite_link($code) {
    global $CFG;
    
    return $CFG->magic_ui_root.'/invitation/'.$code;
}

/** 
 * Save invited user details
 * @param str $code
 * @param str $email
 * @param int $invitedby
 * @return boolean
 */
function monorail_save_invitation($code, $email, $invitedby, $userTag = 0, $userRole = 0) {
    global $DB;
    
    try {
    	$invite = monorail_get_invitation($code);
        $row = $DB->get_record('monorail_invite_users', array('email'=>$email, 'monorailinvitesid'=>$invite->id, 'invitedby'=>$invitedby), '*', MUST_EXIST);
        // already here, do nothing
        return $row->id;
    } catch (Exception $ex) {
        $row = new stdClass();
        $invite = monorail_get_invitation($code);
        if (! $invite)
            return false;
        $row->monorailinvitesid = $invite->id;
        $row->email = $email;
        $row->invitedby = $invitedby;
        $row->timemodified = time();
        $row->usertag = $userTag;
        $row->userrole = $userRole;
        $rowid = $DB->insert_record('monorail_invite_users', $row);
        if ($rowid) {
            return $rowid;
        } else {
            return false;
        }
    }
    return false;
}

/**
 * Get invited emails for a code
 * @param str code
 * @return array of objects
 */
function monorail_get_invited_emails($code) {
    global $DB;
    
    try {
        $invite = monorail_get_invitation($code);
        if (! $invite)
            return array();

        $values = $DB->get_records_sql("SELECT id, email, invitedby, timemodified, status FROM {monorail_invite_users}" .
            " WHERE monorailinvitesid=?", array($invite->id));

        return $values;

    } catch (Exception $ex) {
        return array();
    }
}

/**
 * Send an email advance
 * Deploys mail sending to remote emailer via a redis queue
 * //TODO queue multiple emails and send them all at once might be better
 * //TODO could combine this into monorail_send_email
 */
function monorail_send_system_email_adv($to, $subject, $body, $fromName, $mailtype, 
               $replyToUser = false, $fromEmail, $replyToUserEmail, $replyToUserName, $meta = null) {
    global $CFG, $USER, $DB;
   
    if(empty($mailtype)) {
        $mailtype = "general";
    }  
    if(empty($fromName)) {
        $fromName = 'PACE';
    }
    if(empty($fromEmail)) {
        $fromEmail = 'noreply@eliademy.com';
    }  
    if(empty($replyToUserEmail)) {
        $replyToUserEmail = $USER->email;
    }  
    if(empty($replyToUserName)) {
      $replyToUserName = fullname($USER); 
    }  

    // compose json object
    $supportuser = generate_email_supportuser();
    $mail = array(
        'type' => $mailtype,
        'environment' => str_replace('https://', '', $CFG->landing_page),
        'to' => $to,
        'from' => $fromEmail,
        'fromname' => $fromName,
        'replyto' => ($replyToUser) ? array('email'=>$replyToUserEmail, 'name'=>$replyToUserName) : array('email' => 'noreply@eliademy.com', 'name' => 'PACE'),
        'subject' => substr($subject, 0, 900),
        'body' => $body,
        'tags' => array($mailtype), 
        'timecreated' => time(),
        'html' => 1,
        'meta' => $meta
    );
    // save to db
    $mailsent = new stdClass();
    $mailsent->triggeredby = $USER->id;
    $mailsent->fromemail = (isset($mail['replyto']['email'])) ? $mail['replyto']['email'] : $mail['from'];
    $mailsent->toemail = $to;
    $mailsent->type = $mailtype;
    $mailsent->valuesjson = json_encode($mail);     // save a copy temporarely if sending fails
    $mailsent->status = 'draft';
    $mailsent->timecreated = $mail['timecreated'];
    $mailsent->timemodified = $mail['timecreated'];
    try {
        $mailsent->id = $DB->insert_record('monorail_sent_emails', $mailsent);
        $mail['id'] = $mailsent->id;
    } catch (Exception $ex) {
        // do not die, instead try to keep sending but log an error
        add_to_log(1, 'monorail', 'lib', 'monorail_send_system_email', 'Exception when saving email to sent_emails: '.$ex->getMessage());
        $mail['id'] = 0;
    }
    $mailjson = json_encode($mail);

    // deploy to redis
    try {
        require_once __DIR__ . '/vendor/autoload.php';
        require_once __DIR__ . '/vendor/autoload.php';
        $redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host'   => $CFG->emailer_redis_host,
            'port'   => $CFG->emailer_redis_port,
        ));
        $redis->rpush('emails', $mailjson);
        // NOTE: afaik no close is needed for the connection as it comes from php connection pools
        // but worth mentioning if some problems arise and this could be the fault
        $status = 'sent';
        $statusinfo = null;
    } catch (Exception $ex) {
        $status = 'error';
        $statusinfo = 'Failed to deploy to redis';
        add_to_log(1, 'monorail', 'lib', 'monorail_send_system_email', 'Exception when deploying email to redis: '.$ex->getMessage());
    }
    
    // update as sent
    if ($mail['id'] > 0) {
        if ($status == 'sent' && $mailtype != 'course_messages') {
            if (empty($CFG->debug) || $CFG->debug < DEBUG_DEVELOPER) {
                $mailsent->valuesjson = null;
            }
        }
        $mailsent->status = $status;
        $mailsent->statusinfo = $statusinfo;
        $DB->update_record('monorail_sent_emails', $mailsent);
        return ($status == 'sent') ? true : false;
    }
    return ($status == 'sent') ? true : false;
}

/**
 * Send an email
 * Deploys mail sending to remote emailer via a redis queue
 * //TODO queue multiple emails and send them all at once might be better
 * //TODO could combine this into monorail_send_email
 */
function monorail_send_system_email($to, $subject, $body, $fromName, $mailtype, $replyToUser = false, $meta = null) {
    global $CFG, $USER, $DB;

    if(empty($mailtype)) {
        $mailtype = "general";
    }  
    // compose json object
    $supportuser = generate_email_supportuser();
    $mail = array(
        'type' => $mailtype,
        'environment' => str_replace('https://', '', $CFG->landing_page),
        'to' => $to,
        'from' => 'noreply@eliademy.com',
        'fromname' => $fromName,
        'replyto' => ($replyToUser) ? array('email'=>$USER->email, 'name'=>fullname($USER)) : array('email' => 'noreply@eliademy.com', 'name' => 'Eliademy'),
        'subject' => substr($subject, 0, 900),
        'body' => $body,
        'tags' => array($mailtype), 
        'timecreated' => time(),
        'html' => 1,
        'meta' => $meta
    );

    // save to db
    $mailsent = new stdClass();
    $mailsent->triggeredby = $USER->id;
    $mailsent->fromemail = (isset($mail['replyto']['email'])) ? $mail['replyto']['email'] : $mail['from'];
    $mailsent->toemail = $to;
    $mailsent->type = $mailtype;
    $mailsent->valuesjson = json_encode($mail);     // save a copy temporarely if sending fails
    $mailsent->status = 'draft';
    $mailsent->timecreated = $mail['timecreated'];
    $mailsent->timemodified = $mail['timecreated'];
    try {
        $mailsent->id = $DB->insert_record('monorail_sent_emails', $mailsent);
        $mail['id'] = $mailsent->id;
    } catch (Exception $ex) {
        // do not die, instead try to keep sending but log an error
        add_to_log(1, 'monorail', 'lib', 'monorail_send_system_email', 'Exception when saving email to sent_emails: '.$ex->getMessage());
        $mail['id'] = 0;
    }
    $mailjson = json_encode($mail);

    // deploy to redis
    try {
        require_once __DIR__ . '/vendor/autoload.php';
        $redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host'   => $CFG->emailer_redis_host,
            'port'   => $CFG->emailer_redis_port,
        ));
        $redis->rpush('emails', $mailjson);
        // NOTE: afaik no close is needed for the connection as it comes from php connection pools
        // but worth mentioning if some problems arise and this could be the fault
        $status = 'sent';
        $statusinfo = null;
    } catch (Exception $ex) {
        $status = 'error';
        $statusinfo = 'Failed to deploy to redis';
        add_to_log(1, 'monorail', 'lib', 'monorail_send_system_email', 'Exception when deploying email to redis: '.$ex->getMessage());
    }
    
    // update as sent
    if ($mail['id'] > 0) {
        if ($status == 'sent' && $mailtype != 'course_messages') {
            if (empty($CFG->debug) || $CFG->debug < DEBUG_DEVELOPER) {
                $mailsent->valuesjson = null;
            }
        }
        $mailsent->status = $status;
        $mailsent->statusinfo = $statusinfo;
        $DB->update_record('monorail_sent_emails', $mailsent);
        return ($status == 'sent') ? true : false;
    }
    return ($status == 'sent') ? true : false;
}

/**
 * Get template from disk
 *
 * @param $templatepath Relative path from templates/, without .html
 * @return string Template
 */
function monorail_get_template($templatepath) {
	//read the base template
    $filename = dirname(__FILE__). '/templates/'.$templatepath.'.html';
    $fh = fopen($filename, 'r');     
	$content = fread($fh, filesize($filename));
	fclose($fh);
	return $content;
}

/**
 * Compile a template
 * Takes an associative array of variables. Also replaces any strings from localization.
 *
 * @param $base Template (string)
 * @param $vars Array of key->value pairs (array)
 * @param $lang Language to be used or null for current language
 * @return string Processed template
 */
function monorail_template_compile($base, array $vars, $lang=null) {
	preg_match_all('/\{\{([a-zA-Z0-9-_\.]+)\}\}/', $base, $strings);
	if (count($strings)) {
		if (gettype($strings[1]) == 'string') {
			$strings[1] = array($strings[1]);
		}
		foreach ($strings[1] as $string) {
			$base = str_replace(
				'{{'.$string.'}}', 
				get_string_manager()->get_string($string, 'theme_monorail', null, $lang),
				$base
			);
		}
	}
	if (count($vars)) {
		foreach ($vars as $key => $value) {
			$base = str_replace('{%'.$key.'%}', $value, $base);
		}
	}
	return $base;
}

/**
 * Send notification email
 * TODO: Move to background processing module once we have that :)
 * @param $subject Subject for email
 * @param $text Text for email
 * @param $userid User to send to
 * @param $lang Language to send in or null for current
 * @return boolean
 */
function monorail_send_notifications($subject, $text, $userid, $lang=null) {
    global $DB, $CFG;

    $body = monorail_template_compile(monorail_get_template('mail/base'), array(
    	'body' => $text,
    	'title' => $subject,
    	//'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
    	//	'change_settings_url' => $CFG->magic_ui_root.'/settings'
    	//), $lang),
        'block_header' => ''
    ), $lang);
    return monorail_send_email('notification', $subject, $body, $userid, $lang);      
}

/**
 * Send info email
 * @param $mailtemplate Mail template to use 
 * @param $subject Subject for email
 * @param $mailbody Body for email
 * @param $link Link to button 
 * @param $userid User to send to
 * @param $lang Language to send in or null for current
 * @return boolean
 */
function monorail_send_info_mail($mailtemplate,$subject, $params, $userid, $lang=null) {
    global $CFG;

    $body = monorail_template_compile(monorail_get_template('mail/base'), array(
       'title' => $subject,
       'body' => monorail_template_compile(monorail_get_template($mailtemplate), $params),
       'footer_additional_link' => '',
       'block_header' => ''
    ), $lang);

    return monorail_send_email('info', $subject, $body, $userid, $lang);
}

/*
 * @param $subject Subject for email
 * @param $body Body for email
 * @param $userid User to send to
 * @param $lang Language to send in or null for current
 * @return boolean
 */
function monorail_send_email($mailtype, $subject, $body, $userid=null, $lang=null, $email=null, $replyToUser=false, $fromName = null, $meta = null) {
    global $DB, $CFG;

    if (isset($userid)) {
        try {
            $user = $DB->get_record('user', array('id'=>$userid), 'email', MUST_EXIST);
            $email = $user->email;
        } catch (Exception $ex) {
            return false;
        }
    }
    if (!isset($email)) {
        return false;
    }

    // validate
    if (!validate_email($email)) {
        return false;
    }
    //Reject mails that user has not opted for platform_updates/engagements etc
    // XXX: some mails must go through in any case
    if (isset($userid) && $mailtype != "receipt") {
      require_once($CFG->dirroot . '/local/monorailfeed/lib.php');
      $usersettings = monorailfeed_user_email_settings($userid, null, true);
      if(array_key_exists('platform_updates', $usersettings) && $usersettings['platform_updates'] == 0) {
       return false;
      }	
    }

    // Don't send to fake addresses.
    if (substr($email, -13) == "@eliademy.com" && is_numeric(substr($email, -14, 1))) {
        return false;
    }

    if (!$fromName) {
        $fromName = get_string_manager()->get_string('service_name', 'theme_monorail', null, $lang);
    }

    $result = monorail_send_system_email($email, $subject, $body, $fromName, $mailtype, $replyToUser, $meta);

    if ($result) {
        if (!empty($CFG->debug) && $CFG->debug >= DEBUG_DEVELOPER) {
            add_to_log(1, 'monorail', 'lib', 'monorail_send_email', 'Sent email to '.$email);
        }
        return true;
    } else {
        add_to_log(1, 'monorail', 'lib', 'monorail_send_email', 'Error when sending email to '.$email);
        return false;
    }
}

/*
 * Send invitation to course
 * @param object $invitation
 * @param str $email
 * @param str $summary
 * @param int $userTag
 * @return boolean
 */
function monorail_send_invitation($invitation, $email, $summary = '', $userTag = 0, $userRole = 0) {
    global $USER, $CFG, $PAGE, $DB;
    
    // validate
    if (!validate_email($email)) {
        return false;
    }

    $subject='';
    $body='';
    if($invitation->type == 'course') {
        if(!$DB->record_exists('monorail_invite_uniquetokens', array('course'=>$invitation->targetid,
                            'email'=>$email,'type'=>$invitation->type, 'invitecode' => $invitation->code))) {
            $token = uniqid('', true);
            $invt = new stdClass();
            $invt->course   = $invitation->targetid;
            $invt->token   = $token;
            $invt->email   = $email;
            $invt->invitecode   = $invitation->code;
            $invt->type   = $invitation->type;
            $DB->insert_record('monorail_invite_uniquetokens',$invt);
        } else {
            $token = $DB->get_field('monorail_invite_uniquetokens', 'token' ,
                array('course'=>$invitation->targetid, 'email' => $email, 'type' => $invitation->type, 'invitecode' => $invitation->code));
        } 
        $link = $CFG->magic_ui_root.'/invitation/'.$invitation->code.'/'.$email.'/enroll/'.$token;
        $cperm = monorail_get_course_perms($invitation->targetid);
        if (($cperm['course_privacy'] == 1) && ($cperm['course_access'] == 3)) {
             $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$invitation->targetid));
             if(!empty($courseData)) {
                 $link = $CFG->landing_page.'/'.$courseData->code;
             }
        }

        $subject = monorail_template_compile(get_string('email_subject_invite','theme_monorail'), array(
    	    'courseName' => $PAGE->course->fullname,
    	    'teacherName' => fullname($USER)    	
        ));

        $body = monorail_template_compile(monorail_get_template('mail/base'), array(
    	    'title' => $subject,
    	    'body' => monorail_template_compile(monorail_get_template('mail/courseinvitation'), array(
	            'courseName' => $PAGE->course->fullname, 
	    	    'teacherName' => fullname($USER),
	    	    'link' => $link,
                    'emailSummary' => $summary
	        )),
    	    'footer_additional_link' => '',
            'block_header' => ''
        ));
    } else if ($invitation->type == 'cohort') {
        $link = $CFG->magic_ui_root.'/invitation/'.$invitation->code.'/'.$email.'/signup';
        $googleappuser = $DB->get_record('monorail_gapp_creds', array('userid' => $USER->id, 'domain' => $USER->department));
        if($googleappuser) {
            $redirect_uri = $CFG->wwwroot . '/auth/googleoauth2/google_redirect.php';
            $link = 'https://accounts.google.com/o/oauth2/auth?client_id='.get_config('auth/googleoauth2', 'googleclientid').'&redirect_uri='.$redirect_uri.'&scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&response_type=code&state=invcode='.$invitation->code;
        }
        $cohort = $DB->get_record('cohort', array('id' => $invitation->targetid));
        $subject = monorail_template_compile(get_string('email_subject_invite_cohort','theme_monorail'), array(
    	    'cohortadminName' => fullname($USER),
    	    'cohortName' => $cohort->name    	
        ));

        switch ($userRole) {
            case 2:
                $bodyTpl = 'mail/cohortinvite';
                break;

            case 5:
                $bodyTpl = 'mail/cohortinvite3';
                break;

            default:
                $bodyTpl = 'mail/cohortinvite2';
        }

        $body = monorail_template_compile(monorail_get_template('mail/base'), array(
    	    'title' => $subject,
    	    'body' => monorail_template_compile(monorail_get_template($bodyTpl), array(
	            'cohortName' => $cohort->name,
	    	    'cohortadminName' => fullname($USER),
                    'emailSummary' => $summary,
                    'invEmail' => $email,
	    	    'link' => $link
	        )),
    	    'footer_additional_link' => '',
            'block_header' => ''
        ));
    }

    $invId = monorail_save_invitation($invitation->code, $email, $USER->id, $userTag, $userRole);

    $result = monorail_send_email('invitation', $subject, $body, null, null, $email, true, fullname($USER) . " " . get_string("sent_via_service", "theme_monorail"), array("invite_id" => $invId));

    if ($result) {
        return true;
    } else {
        return false;
    }
}

/* moodle cron job */
function theme_monorail_cron() {
    global $CFG, $DB;
    /* send engagement emails */
    try {
        require_once("$CFG->dirroot/theme/monorail/engagements.php");
        engagements_process();
    } catch (Exception $ex) {
         add_to_log(1, 'monorail', 'lib', 'cron', 'Error processing engagements');
    }
    /* check for courses that have passed their end date */
    try {
        monorail_process_course_end();
    } catch (Exception $ex) {
         add_to_log(1, 'monorail', 'lib', 'cron', 'Error processing course ends');
    }

    // Count premium users
    try {
        require_once __DIR__ . "/premium_counter.php";
        premium_counter_process();
    } catch (Exception $e) {
        error_log("ERROR!!!" . var_export($e, true));
        mail("aurelijus@eliademy.com", "Failed to count premium users!", var_export($e, true));
    }

    /* Collecting payments from premium users! */
    try {
        require_once __DIR__ . "/premium_payments.php";
        premium_payments_process();
    } catch (Exception $e) {
        error_log("ERROR!!!" . var_export($e, true));
        mail("aurelijus@eliademy.com", "Payment collection error!", var_export($e, true));
    }

    // Cache cleanups
    try {
        require_once __DIR__ . "/../../local/monorailservices/cachelib.php";
        wscache_cleanup();
    } catch (Exception $e) {
        // ...
    }
}

/* course over end date processing */
function monorail_process_course_end($courseid = null) {
    global $DB, $CFG;

    require_once __DIR__ . "/../../local/monorailservices/cachelib.php";

    if (! $courseid) {
        // get all ended
        $courses = $DB->get_records_select('monorail_course_data', "status = 1 and enddate != 0 and enddate < ?", array(time()));
        foreach ($courses as $coursedata) {
            monorail_process_course_end($coursedata->courseid);
        }
    } else {
        // one
        $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$courseid));
        try {
            $coursedata->status = 0;
            $coursedata->enddate = time();
            $coursedata->timemodified = time();
            $DB->update_record('monorail_course_data', $coursedata);
            // trigger skill achievement updates
            try {
                require_once("$CFG->dirroot/theme/monorail/skills.php");
                monorail_grant_skills_from_course($courseid);
            } catch (Exception $ex) {
                add_to_log($courseid, 'monorail', 'lib', 'process_course_end', 'Exception when trying to grant skills: '.$ex->getMessage());
            }

            $courseUsers = $DB->get_fieldset_sql("SELECT DISTINCT ra.userid AS id " .
                "FROM {role_assignments} AS ra " .
                    "INNER JOIN {context} AS ctx ON ra.contextid=ctx.id AND ctx.contextlevel=50 " .
                        "WHERE ctx.instanceid=?", array($courseid));

            foreach ($courseUsers as $uid) {
                wscache_reset_by_user($uid);
            }

            wscache_reset_by_dependency("course_info_" . $courseid);
        } catch (Exception $ex) {
            add_to_log($coursedata->courseid, 'monorail', 'lib', 'process_course_end', 'Error processing course end: '.$ex->getMessage());
        }
    }
}

function monorail_csspostprocess($css, $theme)
{
    global $CFG;

    // Fonts.
    $css = preg_replace("/url\\('font\\//", "url('" . $CFG->wwwroot . "/theme/monorail/fonts/", $css);

    // Bootstrap icons.
    $css = preg_replace("/url\\(\"..\\/img\\//", "url(\"" . $CFG->wwwroot . "/theme/monorail/img/", $css);

    return $css;
}

//Copy of function from moodlelig authenticate_user_login only change being that it does not  
//update_internal_user_password . Reason is that to extend the plugin functionality to also
//allow user to login with username/password and also by social login and we do not want to reset
//user password if user is logging in via social auth. 
/**
 * Authenticates a user against the chosen authentication mechanism
 *
 * Given a username and password, this function looks them
 * up using the currently selected authentication mechanism,
 * and if the authentication is successful, it returns a
 * valid $user object from the 'user' table.
 *
 * Uses auth_ functions from the currently active auth module
 *
 * After authenticate_user_login() returns success, you will need to
 * log that the user has logged in, and call complete_user_login() to set
 * the session up.
 *
 * Note: this function works only with non-mnet accounts!
 *
 * @param string $username  User's username
 * @param string $password  User's password
 * @return user|flase A {@link $USER} object or false if error
 */
function monorail_authenticate_user_login($username, $password) {
    global $CFG, $DB;

    require_once($CFG->libdir.'/authlib.php');
    require_once($CFG->libdir.'/moodlelib.php');
    $authsenabled = get_enabled_auth_plugins();

    if ($user = get_complete_user_data('username', $username, $CFG->mnet_localhost_id)) {
        $auth = empty($user->auth) ? 'manual' : $user->auth;  // use manual if auth not set
        if (!empty($user->suspended)) {
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Suspended Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }
        if ($auth=='nologin' or !is_enabled_auth($auth)) {
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Disabled Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }
        $auths = array($auth);

    } else {
        // check if there's a deleted record (cheaply)
        if ($DB->get_field('user', 'id', array('username'=>$username, 'deleted'=>1))) {
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Deleted Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }

        // User does not exist
        $auths = $authsenabled;
        $user = new stdClass();
        $user->id = 0;
    }

    foreach ($auths as $auth) {
        $authplugin = get_auth_plugin($auth);

        // on auth fail fall through to the next plugin
        if (!$authplugin->user_login($username, $password)) {
            continue;
        }

        // successful authentication
        if ($user->id) {                          // User already exists in database
            if (empty($user->auth)) {             // For some reason auth isn't set yet
                $DB->set_field('user', 'auth', $auth, array('username'=>$username));
                $user->auth = $auth;
            }

            //update_internal_user_password($user, $password); // just in case salt or encoding were changed (magic quotes too one day)

            if ($authplugin->is_synchronised_with_external()) { // update user record from external DB
                $user = update_user_record($username);
            }
        } else {
                continue;
        }

        $authplugin->sync_roles($user);

        foreach ($authsenabled as $hau) {
            $hauth = get_auth_plugin($hau);
            $hauth->user_authenticated_hook($user, $username, $password);
        }

        if (empty($user->id)) {
            return false;
        }

        if (!empty($user->suspended)) {
            // just in case some auth plugin suspended account
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Suspended Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }

        return $user;
    }

    // failed if all the plugins have failed
    add_to_log(SITEID, 'login', 'error', 'index.php', $username);
    if (debugging('', DEBUG_ALL)) {
        error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Failed Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
    }
    return false;
}

/**
 * Handle WS token checking and generation
 */
function monorail_handle_ws_token($user) {
    global $DB, $CFG;
    
    require_once($CFG->libdir ."/externallib.php");
    
    $service_record = $DB->get_record('external_services', array('shortname'=>'services', 'enabled'=>1), '*', MUST_EXIST);
    if (empty($service_record)) {
        throw new moodle_exception('servicenotavailable', 'webservice');
    }
    //check if there is any required system capability
    if ($service_record->requiredcapability and !has_capability($service_record->requiredcapability, get_context_instance(CONTEXT_SYSTEM), $user)) {
        throw new moodle_exception('missingrequiredcapability', 'webservice', '', $service_record->requiredcapability);
    }

    $tokenssql = "SELECT t.id, t.sid, t.token, t.validuntil, t.iprestriction
                  FROM {external_tokens} t
                 WHERE t.userid = ? AND t.externalserviceid = ?
              ORDER BY t.timecreated ASC";
    $tokens = $DB->get_records_sql($tokenssql, array($user->id, $service_record->id));

    //A bit of sanity checks
    foreach ($tokens as $key=>$token) {

        /// Checks related to a specific token. (script execution continue)
        $unsettoken = false;
        //if sid is set then there must be a valid associated session no matter the token type
        if (!empty($token->sid)) {
            $session = session_get_instance();
            if (!$session->session_exists($token->sid)){
                //this token will never be valid anymore, delete it
                $DB->delete_records('external_tokens', array('sid'=>$token->sid));
                $unsettoken = true;
            }
        } else {
            // tokens should always be linked to sessions
            $DB->delete_records('external_tokens', array('token'=>$token->token));
            $unsettoken = true;
        }

        //remove token if no valid anymore
        //Also delete this wrong token (similar logic to the web service servers
        //    /webservice/lib.php/webservice_server::authenticate_by_token())
        if (!empty($token->validuntil) and $token->validuntil < time()) {
            $DB->delete_records('external_tokens', array('token'=>$token->token));
            $unsettoken = true;
        }

        // remove token if its ip not in whitelist
        if (isset($token->iprestriction) and !address_in_subnet(getremoteaddr(), $token->iprestriction)) {
            $unsettoken = true;
        }

        if ($unsettoken) {
            unset($tokens[$key]);
        }
    }

    // if some valid tokens exist then use the most recent
    if (count($tokens) > 0) {
        $token = array_pop($tokens);
        $token = $token->token;
    } else {
        if (has_capability('moodle/webservice:createtoken', get_system_context())) {
            // create a new token
            $token = external_generate_token(EXTERNAL_TOKEN_EMBEDDED, $service_record, $user->id, get_context_instance(CONTEXT_SYSTEM)->id);
            add_to_log(SITEID, 'webservice', 'automatically create user token', '' , 'User ID: ' . $user->id);
        } else {
            throw new moodle_exception('cannotcreatetoken', 'webservice', '', $serviceshortname);
        }
    }

    // log token access
    $DB->set_field('external_tokens', 'lastaccess', time(), array('token'=>$token));
    
    return $token;
}


function monorail_create_relay_channel() {
    global $DB, $CFG, $USER;
    
    try {
        require_once __DIR__ . '/vendor/autoload.php';
        $redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host'   => $CFG->relay_redis_host,
            'port'   => $CFG->relay_redis_port,
        ));
        if ($DB->record_exists('monorail_sessions', array('userid'=>$USER->id, 'sesskey'=>sesskey()))) {
            $record = $DB->get_record('monorail_sessions', array('userid'=>$USER->id, 'sesskey'=>sesskey()));
            $record->timestamp = time();
            $DB->update_record('monorail_sessions', $record);
        } else {
            $record = new stdClass();
            $record->userid = $USER->id;
            $record->sesskey = sesskey();
            $record->timestamp = time();
            $DB->insert_record('monorail_sessions', $record);
        }
        $redis->hset(sesskey(), 'userid', strval($USER->id));
        // NOTE: afaik no close is needed for the connection as it comes from php connection pools
        // but worth mentioning if some problems arise and this could be the fault
    } catch (Exception $ex) {
        add_to_log(1, 'monorail', 'lib', 'monorail_create_relay_channel', 'Exception when creating relay in redis: '.$ex->getMessage());
    }
}

/* tnx http://www.xeweb.net/2011/02/11/generate-a-random-string-a-z-0-9-in-php/ */
function monorail_random_string($length = 6) {
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}


/**
 * Stores optimised user pictures
 * Base copied from moodle.lib.gdlib
 *
 * @param int $userid
 * @param string $originalfile
 * @return pichash or false
 */
function monorail_process_avatar($userid, $originalfile) {
    global $CFG, $DB;

    require_once($CFG->libdir.'/filelib.php');
    require_once($CFG->libdir.'/gdlib.php');
    require_once($CFG->libdir.'/filestorage/file_storage.php');

    if (empty($CFG->gdversion)) {
        return false;
    }

    if (!is_file($originalfile)) {
        return false;
    }

    $imageinfo = getimagesize($originalfile);

    if (empty($imageinfo)) {
        return false;
    }

    $image = new stdClass();
    $image->width  = $imageinfo[0];
    $image->height = $imageinfo[1];
    $image->type   = $imageinfo[2];

    $t = null;
    switch ($image->type) {
        case IMAGETYPE_GIF:
            if (function_exists('imagecreatefromgif')) {
                $im = imagecreatefromgif($originalfile);
            } else {
                debugging('GIF not supported on this server');
                return false;
            }
            // Guess transparent colour from GIF.
            $transparent = imagecolortransparent($im);
            if ($transparent != -1) {
                $t = imagecolorsforindex($im, $transparent);
            }
            break;
        case IMAGETYPE_JPEG:
            if (function_exists('imagecreatefromjpeg')) {
                $im = imagecreatefromjpeg($originalfile);
            } else {
                debugging('JPEG not supported on this server');
                return false;
            }
            break;
        case IMAGETYPE_PNG:
            if (function_exists('imagecreatefrompng')) {
                $im = imagecreatefrompng($originalfile);
            } else {
                debugging('PNG not supported on this server');
                return false;
            }
            break;
        default:
            return false;
    }

    if (function_exists('imagejpeg')) {
        $imagefnc = 'imagejpeg';
        $imageext = '.jpg';
        $filters = null; // not used
        $quality = 90;
    } else {
        debugging('Jpeg not supported on this server, please fix server configuration');
        return false;
    }

    if (function_exists('imagecreatetruecolor') and $CFG->gdversion >= 2) {
        $im1 = imagecreatetruecolor(32, 32);
        $im2 = imagecreatetruecolor(90, 90);
        $im3 = imagecreatetruecolor(185, 185);
        $im4 = imagecreatetruecolor(512, 512);
        if ($image->type != IMAGETYPE_JPEG and $imagefnc === 'imagepng') {
            if ($t) {
                // Transparent GIF hacking...
                $transparentcolour = imagecolorallocate($im1 , $t['red'] , $t['green'] , $t['blue']);
                imagecolortransparent($im1 , $transparentcolour);
                $transparentcolour = imagecolorallocate($im2 , $t['red'] , $t['green'] , $t['blue']);
                imagecolortransparent($im2 , $transparentcolour);
                $transparentcolour = imagecolorallocate($im3 , $t['red'] , $t['green'] , $t['blue']);
                imagecolortransparent($im3 , $transparentcolour);
                $transparentcolour = imagecolorallocate($im4 , $t['red'] , $t['green'] , $t['blue']);
                imagecolortransparent($im4 , $transparentcolour);
            }

            imagealphablending($im1, false);
            $color = imagecolorallocatealpha($im1, 0, 0,  0, 127);
            imagefill($im1, 0, 0,  $color);
            imagesavealpha($im1, true);

            imagealphablending($im2, false);
            $color = imagecolorallocatealpha($im2, 0, 0,  0, 127);
            imagefill($im2, 0, 0,  $color);
            imagesavealpha($im2, true);

            imagealphablending($im3, false);
            $color = imagecolorallocatealpha($im3, 0, 0,  0, 127);
            imagefill($im3, 0, 0,  $color);
            imagesavealpha($im3, true);
            
            imagealphablending($im4, false);
            $color = imagecolorallocatealpha($im4, 0, 0,  0, 127);
            imagefill($im4, 0, 0,  $color);
            imagesavealpha($im4, true);
        }
    } else {
        $im1 = imagecreate(32, 32);
        $im2 = imagecreate(90, 90);
        $im3 = imagecreate(185, 185);
        $im4 = imagecreate(512, 512);
    }

    $cx = $image->width / 2;
    $cy = $image->height / 2;

    if ($image->width < $image->height) {
        $half = floor($image->width / 2.0);
    } else {
        $half = floor($image->height / 2.0);
    }

    imagecopybicubic($im1, $im, 0, 0, $cx - $half, $cy - $half, 32, 32, $half * 2, $half * 2);
    imagecopybicubic($im2, $im, 0, 0, $cx - $half, $cy - $half, 90, 90, $half * 2, $half * 2);
    imagecopybicubic($im3, $im, 0, 0, $cx - $half, $cy - $half, 185, 185, $half * 2, $half * 2);
    imagecopybicubic($im4, $im, 0, 0, $cx - $half, $cy - $half, 512, 512, $half * 2, $half * 2);

    $oldhash = monorail_data('user', $userid, 'pichash');
    if (! empty($oldhash)) {
        $oldhash = reset($oldhash)->value;
    } else {
        $oldhash = false;
    }
    $pichash = monorail_random_string(50);

    $fn1 = $CFG->dirroot . '/public_images/user_picture/' . $pichash . '_32.jpg';
    $fn2 = $CFG->dirroot . '/public_images/user_picture/' . $pichash . '_90.jpg';
    $fn3 = $CFG->dirroot . '/public_images/user_picture/' . $pichash . '_185.jpg';
    $fn4 = $CFG->dirroot . '/public_images/user_picture/' . $pichash . '_512.jpg';

    if (! $imagefnc($im1, $fn1, $quality, $filters) ||
        ! $imagefnc($im2, $fn2, $quality, $filters) ||
        ! $imagefnc($im3, $fn3, $quality, $filters) ||
        ! $imagefnc($im4, $fn4, $quality, $filters) ) {
        // error, keep old pics
        @unlink($fn1);
        @unlink($fn2);
        @unlink($fn3);
        @unlink($fn4);
        $return = false;
    } else {
        // success
        monorail_data('user', $userid, 'pichash', $pichash);
        
        if ($oldhash) {
            $ofn1 = $CFG->dirroot . '/public_images/user_picture/' . $oldhash . '_32.jpg';
            $ofn2 = $CFG->dirroot . '/public_images/user_picture/' . $oldhash . '_90.jpg';
            $ofn3 = $CFG->dirroot . '/public_images/user_picture/' . $oldhash . '_185.jpg';
            $ofn4 = $CFG->dirroot . '/public_images/user_picture/' . $oldhash . '_512.jpg';
            @unlink($ofn1);
            @unlink($ofn2);
            @unlink($ofn3);
            @unlink($ofn4);
        }
        $return = $pichash;
    }
    imagedestroy($im1);
    imagedestroy($im2);
    imagedestroy($im3);
    imagedestroy($im4);
    return $return;
}

function monorail_set_course_review($courseid, $status)
{
    global $DB;

    $perms = $DB->get_record("monorail_course_perms", array("course"=>$courseid));
    if($perms) {
      $perms->review = $status;
      $DB->update_record("monorail_course_perms", $perms);
    }
}

function monorail_update_course_perms($courseid, $course_access, $course_privacy) {
    global $DB;

    $perms = $DB->get_record("monorail_course_perms", array("course"=>$courseid));
    if($perms) {
      $perms->course = $courseid;
      $perms->caccess = $course_access;
      $perms->privacy = $course_privacy;
      $DB->update_record("monorail_course_perms", $perms);
    } else {
      $perms = new stdClass();
      $perms->course = $courseid;
      $perms->caccess = $course_access;
      $perms->privacy = $course_privacy;
      $perms->review = 0;
      $DB->insert_record("monorail_course_perms",$perms);
    } 
}

function monorail_get_course_perms($courseid) {
    global $DB, $CFG;

    $perms = $DB->get_record("monorail_course_perms", array("course"=>$courseid));
    if($perms) {
      if(($perms->caccess == 1)) {
        if($perms->privacy == 2) {
           monorail_update_course_perms($courseid, 2, $perms->privacy);
           return array("course_access"=> 2, "course_privacy"=>$perms->privacy, "review" => $perms->review);
        }
        $role = $DB->get_record('role', array('shortname' => 'student'));
        $usercnt = $DB->get_record_sql("SELECT COUNT(*) AS cnt FROM {role_assignments} r, {course} c, {context} t WHERE t.instanceid=c.id AND r.contextid=t.id AND r.roleid=5 AND c.id =?", array($courseid));
        $usercount = $usercnt->cnt;
        if($usercount > 0) {// self,and manual default user
          monorail_update_course_perms($courseid, 2, $perms->privacy);
          return array("course_access"=> 2, "course_privacy"=>$perms->privacy, "review" => $perms->review);
        }
      }

      return array("course_access"=>$perms->caccess, "course_privacy"=>$perms->privacy, "review" => $perms->review);
    } else {
      $course_access = 2; // Invite 
      $course_privacy = 2; // Private 
      $public = reset(monorail_data('BOOLEAN', $courseid, 'public'));
      $public = (! empty($public)) ? $public->value : false;
      $published_in_business_catalog = reset(monorail_data('BOOLEAN', $courseid, 'business'));
      $published_in_business_catalog = (! empty($published_in_business_catalog)) ? $published_in_business_catalog->value : false;
      $published_in_public_catalog = reset(monorail_data('BOOLEAN', $courseid, 'public_catalog'));
      $published_in_public_catalog = (! empty($published_in_public_catalog)) ? $published_in_public_catalog->value : false;
      $cohortcourse = false;//init
      $cohort = $DB->get_field('monorail_cohort_courseadmins', 'cohortid', array('courseid'=>$courseid), IGNORE_MULTIPLE);
      if($cohort) {
        $cohortcourse = true;
      }
      $role = $DB->get_record('role', array('shortname' => 'student'));
      $usercnt = $DB->get_record_sql("SELECT COUNT(*) AS cnt FROM {role_assignments} r, {course} c, {context} t WHERE t.instanceid=c.id AND r.contextid=t.id AND  r.roleid=5 AND c.id =?", array($courseid));
      $usercount = $usercnt->cnt;
      $review = 0;
      if($cohortcourse) {
        $course_privacy =  2; // Private              
        if($published_in_business_catalog || $published_in_public_catalog || $public) {
          $course_access =  3; // Open              
        } else {
          $course_access =  2; // Invite only              
        }
      } else {
        if($public || $published_in_public_catalog) {
          $course_access =  3; // Open              
        } else {
          $course_access =  2; // Invite              
        }
      }
      if($public || $published_in_public_catalog) {
        $course_privacy =  1; //  Public course              
        if($usercount == 0) {
          $course_access =  1;
        }
        if($published_in_public_catalog == 1) {
           $review = 1;
        } else if ($published_in_public_catalog == 2) {
           $review = 2;
        } 
       }
      }
      monorail_update_course_perms($courseid, $course_access, $course_privacy); 
      monorail_set_course_review($courseid, $review);
      return array("course_access"=>$course_access, "course_privacy"=>$course_privacy, "review" => $perms->review);
}




/**
 * Filter given HTML piece and replace inline attachments with public
 * files.
 */
function monorail_export_attachments($html)
{
    global $CFG;

    if (! $html || ! strlen($html)) {
        return "";
    }
    $dom = new DOMDocument();
    $fs = get_file_storage();

    $dom->loadHTML(mb_convert_encoding($html, 'html-entities', 'utf-8'));
    $imageElements = $dom->getElementsByTagName("img");
    $i = 0;

    while ($i < $imageElements->length)
    {
        $img = $imageElements->item($i);

        if ($img->hasAttribute("data-attachment"))
        {
            $modcontext = get_context_instance(CONTEXT_MODULE, $img->getAttribute("data-attachment"));

            $files = $fs->get_area_files($modcontext->id, 'mod_resource', 'content');

            foreach ($files as $file)
            {
                if ($file->get_filename() != ".")
                {
                    $img->setAttribute("src", $CFG->landing_page . "/extimg/" . $file->get_id() . "_" . $file->get_filename());

                    $file->copy_content_to($CFG->external_data_root . "/../../extimg/" . $file->get_id() . "_" . $file->get_filename());

                    break;
                }
            }

            $i++;
        }
        else if ($img->hasAttribute("data-attachment-youtube"))
        {
            $iframe = $dom->createElement("iframe");

            $iframe->setAttribute("src", "https://www.youtube.com/embed/" . $img->getAttribute("data-attachment-youtube") . "?autoplay=0");
            $iframe->setAttribute("frameborder", "0");
            $iframe->setAttribute("type", "text/html");
            $iframe->setAttribute("style", "vertical-align: middle;" . $img->getAttribute("style"));

            if ($img->hasAttribute("width"))
            {
                $iframe->setAttribute("width", $img->getAttribute("width"));
            }

            if ($img->hasAttribute("height"))
            {
                $iframe->setAttribute("height", $img->getAttribute("height"));
            }

            $img->parentNode->replaceChild($iframe, $img);
        }
        else if ($img->hasAttribute("data-attachment-vimeo"))
        {
            $iframe = $dom->createElement("iframe");

            $iframe->setAttribute("src", "https://player.vimeo.com/video/" . $img->getAttribute("data-attachment-vimeo") . "?autoplay=0");

            $iframe->setAttribute("frameborder", "0");
            $iframe->setAttribute("type", "text/html");
            $iframe->setAttribute("style", "vertical-align: middle;" . $img->getAttribute("style"));

            if ($img->hasAttribute("width"))
            {
                $iframe->setAttribute("width", $img->getAttribute("width"));
            }

            if ($img->hasAttribute("height"))
            {
                $iframe->setAttribute("height", $img->getAttribute("height"));
            }

            $img->parentNode->replaceChild($iframe, $img);
        }
        else
        {
            $i++;
        }

        // TODO: slide share, oembed...
    }

    $html2 = "";

    foreach ($dom->getElementsByTagName("body")->item(0)->childNodes as $subNode)
    {
        // XXX: DOMDocument turns <br /> into <br></br>, which browsers see
        // as <br /><br />...

        $html2 .= str_replace("</br>", "", $subNode->C14N());
    }

    return $html2;
}

    /**
     * Return the id's of all users who have any of the role in the particular cource
     */
    function get_cource_users_with_roles($courseid, $rolesid_array)
    {
        global $DB;
        $users  = $DB->get_fieldset_sql("SELECT ra.userid AS id " .
                "FROM {role_assignments} AS ra " .
                    "INNER JOIN {context} AS ctx ON ra.contextid=ctx.id AND ctx.contextlevel=50 " .
                        "WHERE ra.roleid IN (" . implode(", ", $rolesid_array) . ") AND ctx.instanceid=?",
                            array($courseid));
        $unique_users = array_unique($users);
        return $unique_users;
    }
    
    /**
     * Return the id's of all students in the course
     */
    function get_students_in_cource($courseid)
    {
        global $DB;
        $ret = array();
        static $rolesid_array = null;
        if(!$rolesid_array) {
            $rolesid_array = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN  ('student')");
        }
        if($rolesid_array && count($rolesid_array))
        {
            $ret = get_cource_users_with_roles($courseid, $rolesid_array);
        }
        return $ret;
    }

// Get course details
function monorail_get_course_details($id)
{
    require_once __DIR__ . "/../../local/monorailservices/cachelib.php";

    return wscache_get_data("course_info_" . $id, function () use ($id)
    {
        global $CFG, $DB;
        
        $uc = $DB->get_record_sql("SELECT c.id AS id, c.fullname AS fullname, c.shortname AS shortname, mcd.status AS coursestatus, " .
                "c.startdate AS startdate, mcd.enddate AS enddate, mcd.code AS code, mcd.mainteacher AS mainteacher " .
            "FROM {course} AS c INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=c.id " .
                    "WHERE c.id=?", array($id));

        if (!$uc) {
            return null;
        }

        // Monorail data
        $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($uc->id));
        $monorailData = array();

        foreach ($moreData as $md)
        {
            $monorailData[$md->datakey] = $md->value;
        }

        // Load background picture
        $backgroundimage = @$monorailData["coursebackground"];

        if (!$backgroundimage)
        {
            $backgroundimage = $CFG->magic_ui_root . "/app/img/stock/other.jpg";
            $backgroundimagetn = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
        }
        else
        {
            try {
                $backgroundimagetn = monorail_get_course_background_tn($backgroundimage, $uc->id);
            } catch (Exception $err) { }

            if(!$backgroundimagetn) {
                $backgroundimagetn = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
            }
        }

        $courseLogo = @$monorailData["course_logo"];

        if (!$courseLogo || $courseLogo == "/")
        {
            $courseLogo = $CFG->magic_ui_root . "/app/img/logo-course-title.png";
        }
        
        if ($uc->startdate)
        {
            $datestring = userdate($uc->startdate, "%d %b %Y")." - ";

            if($uc->enddate) {
              $datestring .= userdate($uc->enddate, "%d %b %Y");
            }
        }
        else
        {
            //$datestring = get_string("self_paced", "theme_monorail");
            $datestring = 0;
        }

        $cperm = monorail_get_course_perms($uc->id);

        $teacherName = "";
        $teacherTitle = "";
        $teacherLogo = "";

        // Main teacher
        if ($uc->mainteacher)
        {
            $teacher = $DB->get_record_sql("SELECT firstname, lastname FROM {user} WHERE id=?", array($uc->mainteacher));
            $teacherData = array();

            foreach ($DB->get_recordset_sql("SELECT datakey, value FROM {monorail_data} WHERE itemid=?" .
                " AND datakey IN ('pichash')", array($uc->mainteacher)) as $item)
            {
                $teacherData[$item->datakey] = $item->value;
            }

            $teacherName = $teacher->firstname . " " . $teacher->lastname;
            $teacherTitle = get_string("label_teacher", "theme_monorail");
            $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);
            if($fieldid) {
                $title = $DB->get_field('user_info_data', 'data', array('userid'=>$uc->mainteacher, 'fieldid' => $fieldid), IGNORE_MULTIPLE);
                if($title) {
                    $teacherTitle = $title;
                }
            }
            $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/default_32.jpg"; 
            // Teacher logo.
            if (array_key_exists("pichash", $teacherData))
            {
                $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/" . $teacherData["pichash"] . "_90.jpg";
            }
        }

        $uc->coursebackgroundtn = $backgroundimagetn;
        $uc->coursebackground = $backgroundimage;
        $uc->courselogo = $courseLogo;
        $uc->students = count(get_students_in_cource($id));
        $uc->stars =  (int) @$monorailData["course_rating"];
        $uc->price =  (double) @$monorailData["course_price"];
        $uc->durationstr = $datestring;
        $uc->cert_enabled = (int) (@$monorailData["cert_enabled"]);
        $uc->course_country = @$monorailData["course_country"];
        $uc->course_privacy = $cperm['course_privacy'];
        $uc->course_access = $cperm['course_access'];
        $uc->teachername = $teacherName;
        $uc->teachertitle = $teacherTitle;
        $uc->teacherlogo = $teacherLogo;

        return $uc;
    });
}

function monorail_check_org_public_page_path($path)
{
    global $DB;

    if (strlen($path) < 3) {
        return 1;
    }

    if (preg_replace('/[a-z0-9]+/', "", $path)) {
        return 2;
    }

    if (in_array($path, array("app", "about", "business", "premium", "login", "love", "lti", "mobile", "opensource", "privacy", "signup", "terms", "support", "cert", "blog", "4ee7f5533746524be7b97043c2152f94", "translate", "get", "helpdesk", "www", "www.translate"))) {
        return 3;
    }

    if ($DB->get_field_sql("SELECT id FROM {monorail_cohort_info} WHERE public_page_path=?", array($path)))
    {
        return 4;
    }

    return 0;
}

?>
