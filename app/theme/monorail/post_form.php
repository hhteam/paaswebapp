<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


// NOTE!! This whole file can be removed once forum is fully refactored
// to work via Ajax and JS. Currently only post deletion is done via
// here and also the forum needs to be loaded for some of the JS
// to work. Jason / 15th Oct 2012

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/formslib.php');

class theme_monorail_post_form extends moodleform {

    /**
     * Returns the options array to use in forum text editor
     *
     * @return array
     */
    public static function editor_options() {
        global $COURSE, $PAGE, $CFG;
        return array(
            'trusttext'=> true
        );
    }

    function definition() {

        global $CFG;
        $mform    =& $this->_form;

        $course        = $this->_customdata['course'];
        $cm            = $this->_customdata['cm'];
        $coursecontext = $this->_customdata['coursecontext'];
        $modcontext    = $this->_customdata['modcontext'];
        $forum         = $this->_customdata['forum'];
        $post          = $this->_customdata['post'];
        $reply         = $this->_customdata['reply'];
        $parent        = $this->_customdata['parent'];

        $mform->addElement('header', 'general', '');//fill in the data depending on page params later using set_data
        if (empty($reply) && empty($parent)) {
	        $mform->addElement('text', 'subject', get_string('subject', 'forum'), 'size="48"');
	        $mform->setType('subject', PARAM_TEXT);
	        $mform->addRule('subject', get_string('required'), 'required', null, 'client');
	        $mform->addRule('subject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        }
        else {
        	$mform->addElement('hidden', 'subject');
        }


        $mform->addElement('editor', 'message', get_string('message', 'forum'), null, self::editor_options());

        //$mform->addElement('textarea', 'message', get_string("message", "forum"), array('wrap'=>"virtual", 'rows'=>10, 'cols'=>50));
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', get_string('required'), 'required', null, 'client');

        $mform->addElement('hidden', 'timestart');
        $mform->setType('timestart', PARAM_INT);
        $mform->addElement('hidden', 'timeend');
        $mform->setType('timeend', PARAM_INT);
        $mform->setConstants(array('timestart'=> 0, 'timeend'=>0));

        if (groups_get_activity_groupmode($cm, $course)) { // hack alert
            $groupdata = groups_get_activity_allowed_groups($cm);
            $groupcount = count($groupdata);
            $modulecontext = get_context_instance(CONTEXT_MODULE, $cm->id);
            $contextcheck = has_capability('mod/forum:movediscussions', $modulecontext) && empty($post->parent) && $groupcount > 1;
            if ($contextcheck) {
                $groupinfo = array('0' => get_string('allparticipants'));
                foreach ($groupdata as $grouptemp) {
                    $groupinfo[$grouptemp->id] = $grouptemp->name;
                }
                $mform->addElement('select','groupinfo', get_string('group'), $groupinfo);
                $mform->setDefault('groupinfo', $post->groupid);
            } else {
                if (empty($post->groupid)) {
                    $groupname = get_string('allparticipants');
                } else {
                    $groupname = format_string($groupdata[$post->groupid]->name);
                }
                $mform->addElement('static', 'groupinfo', get_string('group'), $groupname);
            }
        }
        //-------------------------------------------------------------------------------
        // buttons

        if (isset($post->edit)) { // hack alert
            $submit_string = get_string('savechanges');
        } else {
            $submit_string = get_string('posttoforum', 'forum');
        }

        $this->add_action_buttons(true, $submit_string);

        $mform->addElement('hidden', 'course');
        $mform->setType('course', PARAM_INT);

        $mform->addElement('hidden', 'forum');
        $mform->setType('forum', PARAM_INT);

        $mform->addElement('hidden', 'discussion');
        $mform->setType('discussion', PARAM_INT);

        $mform->addElement('hidden', 'parent');
        $mform->setType('parent', PARAM_INT);

        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT);

        $mform->addElement('hidden', 'groupid');
        $mform->setType('groupid', PARAM_INT);

        $mform->addElement('hidden', 'edit');
        $mform->setType('edit', PARAM_INT);

        $mform->addElement('hidden', 'reply');
        $mform->setType('reply', PARAM_INT);
    }
}

