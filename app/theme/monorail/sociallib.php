<?php

define("FACEBOOK_LIBS", dirname(__FILE__) . DIRECTORY_SEPARATOR . "facebook". DIRECTORY_SEPARATOR . "src");
ini_set("include_path", ini_get("include_path") . PATH_SEPARATOR . FACEBOOK_LIBS);

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/twitter/twitteroauth.php');
require_once($CFG->libdir . '/filelib.php');
require_once("$CFG->dirroot/theme/monorail/lib.php");

require_once('Facebook/HttpClients/FacebookHttpable.php');
require_once('Facebook/HttpClients/FacebookCurl.php');
require_once('Facebook/HttpClients/FacebookCurlHttpClient.php');
require_once('Facebook/FacebookSession.php');
require_once('Facebook/FacebookResponse.php');
require_once('Facebook/FacebookRequest.php');
require_once('Facebook/GraphObject.php');
require_once('Facebook/FacebookSDKException.php');
require_once('Facebook/FacebookRequestException.php');
require_once('Facebook/FacebookAuthorizationException.php');

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;

FacebookSession::setDefaultApplication(get_config('auth/googleoauth2', 'facebookclientid'), get_config('auth/googleoauth2', 'facebookclientsecret'));

function get_tracker_type($sharetype)
{
  $rv = '';
  switch ($sharetype) {
    case "share_certificate":
        $rv = "certificate";
        break;
    case "share_review":
        $rv = "review";
        break;
    case "share_complete":
        $rv = "course-done";
        break;
    case "share_enroll":
        $rv = "course-enroll";
        break;
  }
  return $rv;
}

function  social_save_stats($service,$sharetype,$userid,$courseid) {
   global $DB;

    $rec = new stdClass();
    $rec->userid = $userid;
    $rec->action = 's-'.$service.'-'.get_tracker_type($sharetype);
    $rec->timestamp = time();
    $rec->courseid = $courseid;

    try {
      $DB->insert_record("monorail_user_activity_log", $rec);
    } catch(Exception $ex) {
        add_to_log(1, 'monorail', 'sociallib', 'social_save_stats', 'Exception when trying add stats: '.$ex->getMessage());
    }
}

function social_facebook_allowed_permission($accesstoken,$permission)
{
    $session = new FacebookSession($accesstoken);
    if($session) {
      try {
        $response = (new FacebookRequest(
          $session, 'GET',
         '/me/permissions'))->execute()->getGraphObject()->asArray();
         if($response) {
             foreach($response as $perm) {
               if((strcmp($perm->permission,$permission) == 0) && (strcmp($perm->status,'granted') == 0)) {
                   return true;
               }
             }
         }
      } catch(FacebookRequestException $ex) {
        add_to_log(1, 'monorail', 'sociallib', 'social_post_facebook_permissions', 'Exception when trying to perm FB: '.$ex->getMessage());
      }
    }
    return false;
}


function social_post_facebook($link,$description,$name,$picture,$sharetype,$userid,$courseid) {
   global $DB;


    if(empty($link) || empty($description) || empty($name) || empty($picture) || empty($sharetype)) {
        return;
    }
    $usetting = $DB->get_record('monorail_social_settings', array('userid' => $userid, 'social' => 'facebook', 'datakey' => $sharetype));

    if(!$usetting || (intval($usetting->value) == 0)) {
        return;
    }
    $accesstoken =  null;
    $userrec = $DB->get_record('monorail_social_keys', array('userid' => $userid, 'social' => 'facebook'));
    if($userrec) {
       $valid = $userrec->timemodified + $userrec->expires - time();
       if($valid) {
           $accesstoken = $userrec->accesstoken;
       }
    }
    if(!$accesstoken) {
        return;
    }
    $session = new FacebookSession($accesstoken);
    if($session) {
      try {
        $response = (new FacebookRequest(
          $session, 'POST', '/me/feed', array(
            'link' => $link,
            'name' => $name,
            'description' => $description,
            'picture' => $picture,
          )
        ))->execute()->getGraphObject();
      } catch(FacebookRequestException $ex) {
        add_to_log(1, 'monorail', 'sociallib', 'social_post_facebook', 'Exception when trying to post FB: '.$ex->getMessage());
        return;
      }   
    }
    social_save_stats('facebook',$sharetype,$userid,$courseid);
}

function social_post_linkedin($link,$description,$name,$picture,$sharetype,$userid,$courseid) {
   global $DB;


    if(empty($link) || empty($description) || empty($name) || empty($picture) || empty($sharetype)) {
        return;
    }
    $usetting = $DB->get_record('monorail_social_settings', array('userid' => $userid, 'social' => 'linkedin', 'datakey' => $sharetype));
    if(!$usetting || (intval($usetting->value) == 0)) {
        return;
    }
    $accesstoken =  null;
    $userrec = $DB->get_record('monorail_social_keys', array('userid' => $userid, 'social' => 'linkedin'));
    if($userrec) {
       $valid = $userrec->timemodified + $userrec->expires - time();
       if($valid) {
           $accesstoken = $userrec->accesstoken;
       }
    }
    if(!$accesstoken) {
        return;
    }

    $postData = array('content' => array(
                'title' =>  $name,
                'description' => $description,
                'submitted-url' => $link,
                'submitted-image-url' => $picture), 
                'visibility' => array('code' => 'connections-only'));
    $params = array( 'oauth2_access_token' => $accesstoken, 'format' => 'json' ); 
    $surl = 'https://api.linkedin.com/v1/people/~/shares'. '?' .http_build_query($params);
    $curl = curl_init($surl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json","x-li-format: json", 'Connection: close'));
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($postData));
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION  , true);
    curl_setopt($curl, CURLOPT_HEADER , false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    try {
        $response = curl_exec($curl); 
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if($http_status != 201) {
            add_to_log(1, 'monorail', 'sociallib', 'social_post_linkedin', 'Post to linkedin failed: '.$response);
        }
        curl_close($curl);
    } catch(Exception $ex) {
       add_to_log(1, 'monorail', 'sociallib', 'social_post_linkedin', 'Exception when trying to post: '.$ex->getMessage());
       return;
    }   
    social_save_stats('linkedin',$sharetype,$userid,$courseid);
}

function social_post_twitter($link,$description,$name,$picture,$sharetype,$userid,$courseid) {
   global $DB;


    if(empty($link) || empty($description) || empty($name) || empty($picture) || empty($sharetype)) {
        return;
    }
    $usetting = $DB->get_record('monorail_social_settings', array('userid' => $userid, 'social' => 'twitter', 'datakey' => $sharetype));
    if(!$usetting || (intval($usetting->value) == 0)) {
        return;
    }
    $accesstoken =  null;
    $userrec = $DB->get_record('monorail_social_keys', array('userid' => $userid, 'social' => 'twitter'));
    if($userrec) {
       $accesstoken = $userrec->accesstoken;
    }
    if(!$accesstoken) {
        return;
    }

    $atoken = explode(":TWITTER:", $accesstoken);
    if(!$atoken and (count($atoken) != 2)) {
       return;
    } 
    try {
      $connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $atoken[0], $atoken[1]);
      $response = $connection->post('statuses/update', array('status' => $description));
    } catch(Exception $ex) {
       add_to_log(1, 'monorail', 'sociallib', 'social_post_linkedin', 'Exception when trying to post: '.$ex->getMessage());
       return;
    }   
    social_save_stats('twitter',$sharetype,$userid,$courseid);
}

function social_share_course_enroll($courseid,$userid)
{
 global $DB, $CFG;
 try {
  $cperms = monorail_get_course_perms($courseid);
  if(!($cperms["course_privacy"] == 1)) {
     //Course not public , return
     return;
  }
  $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
  $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?",
                    array($courseid));
  $img = $CFG->landing_page . "/extimg/" . $courseData->code . "_course_background.jpg";
  $name = str_replace("%s", $course->fullname, get_string('share_course_msg', 'theme_monorail'));
  $courseurl = $CFG->catalog_url . "/catalog/product/view/sku/" . $courseData->code; 
  social_post_facebook($courseurl,$name,$course->fullname,$img,'share_enroll',$userid, $courseid);
  social_post_linkedin($courseurl,$name,$course->fullname,$img,'share_enroll',$userid, $courseid);
  social_post_twitter($courseurl,str_replace("%s", $courseurl, get_string('share_course_smsg', 'theme_monorail')),
                        $course->fullname,$img,'share_enroll',$userid, $courseid);
 } catch(Exception $ex) {
   add_to_log(1, 'monorail', 'sociallib', 'social_share_course_enroll', 'Exception when trying to post: '.$ex->getMessage());
 }
}

function social_share_certificate($courseid,$userid)
{
  global $DB, $CFG;
 try {

  $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
  $certificates =  $DB->get_records_sql("SELECT id, state, hashurl FROM {monorail_certificate} WHERE userid=? AND courseid=?",
                    array($userid, $courseid));
  $certUrl = '';
  $certImgUrl = '';
  $cert_hashurl = '';
  foreach ($certificates as $cert)
  {
     $cert_hashurl = $cert->hashurl;
  }
  if(!empty($cert_hashurl)) {
    $certUrl = $CFG->landing_page . "/cert/" . $cert_hashurl . ".html";
    $certImgUrl = $CFG->landing_page . "/cert/" . $cert_hashurl . ".jpg";
  }
  $name = str_replace("{%coursename%}", $course->fullname, get_string('email_notif_student_cert_share', 'theme_monorail'));
  social_post_facebook($certUrl,$name,$course->fullname,$certImgUrl,'share_certificate',$userid, $courseid);
  social_post_linkedin($certUrl,$name,$course->fullname,$certImgUrl,'share_certificate',$userid, $courseid);
  social_post_twitter($certUrl,str_replace("%s", $certImgUrl, get_string('share_course_cert_smsg', 'theme_monorail')),$course->fullname,$certImgUrl,'share_certificate',$userid, $courseid);
 } catch(Exception $ex) {
   add_to_log(1, 'monorail', 'sociallib', 'social_share_certificate', 'Exception when trying to post: '.$ex->getMessage());
 }
}

function social_share_course_complete($courseid,$userid)
{
 global $DB, $CFG;
 try {
  $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
  $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?",
                    array($courseid));
  $img = $CFG->landing_page . "/extimg/" . $courseData->code . "_course_background.jpg";
  $name = str_replace("%s", $course->fullname, get_string('share_course_complete_msg', 'theme_monorail'));
  $courseurl = $CFG->catalog_url . "/catalog/product/view/sku/" . $courseData->code; 
  social_post_facebook($courseurl,$name,$course->fullname,$img, 'share_complete',$userid, $courseid);
  social_post_linkedin($courseurl,$name,$course->fullname,$img, 'share_complete',$userid, $courseid);
  social_post_twitter($courseurl,str_replace("%s", $courseurl, get_string('share_course_complete_smsg', 'theme_monorail')),$course->fullname,$img, 'share_complete',$userid, $courseid);
 } catch(Exception $ex) {
   add_to_log(1, 'monorail', 'sociallib', 'social_share_course_complete', 'Exception when trying to post: '.$ex->getMessage());
 }
}

function social_share_course_review($courseid, $stars, $review, $shareon, $image)
{
 global $DB, $CFG, $USER;
 try {
  $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
  $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?",
                    array($courseid));
  if ($image)
  {
    $img = $image;
  }
  else
  {
    $img = $CFG->landing_page . "/extimg/" . $courseData->code . "_course_background.jpg";
  }
  if(empty($review)) {
    $review = str_replace("%1", $stars,
                str_replace("%2", $CFG->catalog_url . "/catalog/product/view/sku/" . $courseData->code,
                    str_replace("%3", $course->fullname,
                        get_string('share_course_review_msg', 'theme_monorail'))));
  }
  $courseurl = $CFG->catalog_url . "/catalog/product/view/sku/" . $courseData->code;

    // If $shareon is empty, post to all configured accounts. If not, post
    // only if $shareon contains the account name.
  if (!$shareon || strpos($shareon, "facebook") !== FALSE)
  {
    social_post_facebook($courseurl,$review,$course->fullname,$img,'share_review',$USER->id, $courseid);
  }
  if (!$shareon || strpos($shareon, "linkedin") !== FALSE)
  {
    social_post_linkedin($courseurl,$review,$course->fullname,$img,'share_review',$USER->id, $courseid);
  }
  if (!$shareon || strpos($shareon, "twitter") !== FALSE)
  {
    social_post_twitter($courseurl, $review, $course->fullname, $img, 'share_review', $USER->id, $courseid);
  }

 } catch(Exception $ex) {
   add_to_log(1, 'monorail', 'sociallib', 'social_share_course_review', 'Exception when trying to post: '.$ex->getMessage());
 }
}

?>
