<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


function xmldb_theme_monorail_upgrade($oldversion = 0) {

    global $DB;
    $dbman = $DB->get_manager();

    $result = true;

    if ($oldversion < 2012122100) {

        // Define table monorail_evernote_users to be created
        $etable = new xmldb_table('monorail_evernote_users');

        // Adding fields to table monorail_evernote_users
        $etable->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $etable->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $etable->add_field('oauthtoken', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, '0');
        $etable->add_field('expires', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_evernote_users
        $etable->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_evernote_users
        $etable->add_index('userid', XMLDB_INDEX_UNIQUE, array('userid'));

        // Conditionally launch create table for monorail_evernote_users
        if (!$dbman->table_exists($etable)) {
            $dbman->create_table($etable);
        }

        //************************************ monorail_data ************************************//
        // Define table monorail_data to be created
        $table = new xmldb_table('monorail_data');

        // Adding fields to table monorail_data
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '80', null, null, null, null);
        $table->add_field('itemid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('datakey', XMLDB_TYPE_CHAR, '80', null, null, null, null);
        $table->add_field('value', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_data
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_data
        $table->add_index('type_itemid_datakey', XMLDB_INDEX_NOTUNIQUE, array('type', 'itemid', 'datakey'));

        // Conditionally launch create table for monorail_data
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //************************************ monorail_file_context_data ************************************//
        // Define table monorail_file_context_data to be created
        $table = new xmldb_table('monorail_file_context_data');

        // Adding fields to table monorail_file_context_data
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('pathhash', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('contextualdata', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('fileid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);


        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_data
        $table->add_key('fileid', XMLDB_KEY_UNIQUE, array('fileid'));

        // Conditionally launch create table for monorail_data
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }


        //************************************ monorail_course_background ************************************//
    	// Define table monorail_data to be created
    	$table = new xmldb_table('monorail_course_background');

    	// Adding fields to table monorail_data
    	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    	$table->add_field('filename', XMLDB_TYPE_CHAR, '80', null, null, null, null);
    	$table->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
    	$table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

    	// Adding keys to table monorail_data
    	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

    	// Adding indexes to table monorail_data
    	$table->add_index('categoryid', XMLDB_INDEX_NOTUNIQUE, array('categoryid'));

    	// Conditionally launch create table for monorail_data
    	if (!$dbman->table_exists($table)) {
    		$dbman->create_table($table);
    	}


    	//************************************ monorail_invites ************************************//
        // Define table monorail_invites to be created
        $table = new xmldb_table('monorail_invites');

        // Adding fields to table monorail_invites
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '30', null, null, null, null);
        $table->add_field('targetid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('code', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('active', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_invites
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_invites
        $table->add_index('code', XMLDB_INDEX_UNIQUE, array('code'));
        $table->add_index('typetargetid', XMLDB_INDEX_UNIQUE, array('type', 'targetid'));

        // Conditionally launch create table for monorail_invites
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }


        //************************************ monorail_invite_users ************************************//
        // Define table monorail_invite_users to be created
        $table = new xmldb_table('monorail_invite_users');

        // Adding fields to table monorail_invite_users
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('monorailinvitesid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        $table->add_field('invitedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_invite_users
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('fk_monorailinvitesid', XMLDB_KEY_FOREIGN, array('monorailinvitesid'), 'monorail_invites', array('id'));

        // Conditionally launch create table for monorail_invite_users
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2012122100, 'theme', 'monorail');

    }
    
    if ($oldversion < 2013071700) {

        // Define table monorail_course_data to be created
        $table = new xmldb_table('monorail_course_data');

        // Adding fields to table monorail_course_data
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '19', null, null, null, null);
        $table->add_field('code', XMLDB_TYPE_CHAR, '10', null, null, null, null);

        // Adding keys to table monorail_course_data
        $table->add_key('courseid', XMLDB_KEY_UNIQUE, array('courseid'));
        $table->add_key('code', XMLDB_KEY_UNIQUE, array('code'));

        // Conditionally launch create table for monorail_course_data
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013071700, 'theme', 'monorail');
    }
    
    if ($oldversion < 2013072502) {

        //**********
        // OK fixing schema - PLEASE keep to these guidelines: http://docs.moodle.org/dev/XMLDB_defining_an_XML_structure#Conventions
        //**********
        $transaction = $DB->start_delegated_transaction();
        $coursedatas = $DB->get_records('monorail_course_data');
        // Define table monorail_course_data to be dropped
        $table = new xmldb_table('monorail_course_data');
        
        // Conditionally launch drop table for monorail_course_data
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Define table monorail_course_data to be created
        $table = new xmldb_table('monorail_course_data');

        // Adding fields to table monorail_course_data
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('code', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('enddate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_course_data
        $table->add_key('code', XMLDB_KEY_UNIQUE, array('code'));
        $table->add_key('courseid', XMLDB_KEY_UNIQUE, array('courseid'));
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_course_data
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        //********* monorail_course_data fix end
        // Return data and set some new
        try {
            foreach ($coursedatas as $coursedata) {
                add_to_log(1, 'monorail', 'db/upgrade', 'upgrade', $coursedata->courseid." is ".$coursedata->code);
                $record = new stdClass();
                $record->courseid = $coursedata->courseid;
                $record->code = $coursedata->code;
                $record->modifiedby = 2;
                $record->enddate = 0;
                try {
                    $course = $DB->get_record('course', array('id'=>$coursedata->courseid), 'timemodified', MUST_EXIST);
                    $record->timemodified = $course->timemodified;
                } catch (Exception $ex) {
                    $record->timemodified = time();
                }
                $DB->insert_record('monorail_course_data', $record);
            }
            $transaction->allow_commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            add_to_log(1, 'monorail', 'db/upgrade', 'upgrade', $ex->getMessage());
        }
        
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013072502, 'theme', 'monorail');
    }
    
    if ($oldversion < 2013080700) {

        // Define table monorail_firstuse_users to be dropped
        $table = new xmldb_table('monorail_firstuse_users');

        // Conditionally launch drop table for monorail_firstuse_users
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013080700, 'theme', 'monorail');
    }

    if ($oldversion < 2013081201) {

        // add field mainteacher to monorail_course_data
        $table = new xmldb_table('monorail_course_data');
        $field = new xmldb_field('mainteacher', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'enddate');

        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Loop courses and fill in mainteacher
        $courses = $DB->get_records('course', array());
        foreach($courses as $course) {
            $context = get_context_instance(CONTEXT_COURSE, $course->id);
            try {
                $teacher = $DB->get_records_sql("SELECT u.* FROM {user} AS u, {role} AS r, {role_assignments} AS a" .
                        " WHERE r.shortname='editingteacher' AND r.id=a.roleid AND a.userid=u.id AND a.contextid=? LIMIT 1",
                        array($context->id));
                $teacher = reset($teacher);
                $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$course->id));
                $coursedata->mainteacher = $teacher->id;
                $DB->update_record('monorail_course_data', $coursedata);
            } catch (Exception $ex) {
                add_to_log($course->id, 'monorail', 'upgrade', '2013081201', 'Error upgrading mainteacher: '.$ex->getMessage());
            }
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013081201, 'theme', 'monorail');
    }     

    if ($oldversion < 2013081400) {

        //************************************ monorail_cohort_info ************************************//
        // Define table monorail_cohort_info to be created
        $table = new xmldb_table('monorail_cohort_info');

        // Adding fields to table monorail_cohort_info
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('fileid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('userscount', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('domain', XMLDB_TYPE_CHAR, '200', null, XMLDB_NOTNULL, null, null);
        $table->add_field('restrictdomain', XMLDB_TYPE_INTEGER, '3', null, null, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_cohort_info
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_cohort_info
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //************************************ monorail_cohort_susers ************************************//
         // Define table monorail_cohort_susers to be created
        $table = new xmldb_table('monorail_cohort_susers');

        // Adding fields to table monorail_cohort_susers
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseids', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_cohort_susers
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_cohort_susers
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //************************************ monorail_cohort_admin_users ************************************//
        // Define table monorail_cohort_admin_users to be created
        $table = new xmldb_table('monorail_cohort_admin_users');

        // Adding fields to table monorail_cohort_admin_users
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_cohort_admin_users
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_cohort_admin_users
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_cohort_courseadmins to be created
        $table = new xmldb_table('monorail_cohort_courseadmins');

        // Adding fields to table monorail_cohort_courseadmins
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('adminid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_cohort_courseadmins
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_cohort_courseadmins
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_cohort_crsinvitee to be created
        $table = new xmldb_table('monorail_cohort_crsinvitee');

        // Adding fields to table monorail_cohort_crsinvitee
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('rolename', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '200', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timestart', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('timeend', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('suspend', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('inviteid', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_cohort_crsinvitee
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_cohort_crsinvitee
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_gapp_creds to be created
        $table = new xmldb_table('monorail_gapp_creds');

        // Adding fields to table monorail_gapp_creds
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('token', XMLDB_TYPE_CHAR, '250', null, XMLDB_NOTNULL, null, null);
        $table->add_field('secret', XMLDB_TYPE_CHAR, '250', null, XMLDB_NOTNULL, null, null);
        $table->add_field('domain', XMLDB_TYPE_CHAR, '250', null, XMLDB_NOTNULL, null, null);
        $table->add_field('expiry', XMLDB_TYPE_INTEGER, '20', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_gapp_creds
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_gapp_creds
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        //Insert data to existing tables - social links user profile fields!
        $recordid = '';
        $eliaextn = $DB->get_record('user_info_category', array('name'=>'Eliademy'));
        if (!$eliaextn) {
            $record = new stdClass();
            $record->name = 'Eliademy';
            $recordid = $DB->insert_record('user_info_category', $record);
        } else {
            $recordid = $eliaextn->id; 
        }
        //Insert data now
        if (!$DB->record_exists('user_info_field', array('shortname'=>'facebook', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'facebook', 'Facebook', 'text', '<p>Facebook URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }
        if (!$DB->record_exists('user_info_field', array('shortname'=>'linkedin', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'linkedin', 'LinkedIn', 'text', '<p>LinkedIn URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }
        
        if (!$DB->record_exists('user_info_field', array('shortname'=>'googleplus', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'googleplus', 'Googleplus', 'text', '<p>Google plus URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }

        if (!$DB->record_exists('user_info_field', array('shortname'=>'twitter', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'twitter', 'Twitter', 'text', '<p>Twitter URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }

        if (!$DB->record_exists('user_info_field', array('shortname'=>'pinterest', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'pinterest', 'Pinterest', 'text', '<p>Pinterest URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }

        if (!$DB->record_exists('user_info_field', array('shortname'=>'instagram', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('instagram', 'Instagram', 'text', '<p>Instagram URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013081400, 'theme', 'monorail');
    }

    if ($oldversion < 2013082100) {
        // Define table monorail_cohort_settings to be created
        $table = new xmldb_table('monorail_cohort_settings');

        // Adding fields to table monorail_cohort_settings
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('value', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_cohort_settings
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_cohort_settings
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013082100, 'theme', 'monorail');
    }

    if ($oldversion < 2013082101) {
        //Insert data to existing tables - generic field to store user options.
        $recordid = '';
        $eliaextn = $DB->get_record('user_info_category', array('name'=>'Eliademy'));
        if (!$eliaextn) {
            $record = new stdClass();
            $record->name = 'Eliademy';
            $recordid = $DB->insert_record('user_info_category', $record);
        } else {
            $recordid = $eliaextn->id; 
        }

        //Insert data now
        if (!$DB->record_exists('user_info_field', array('shortname'=>'options', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('options', 'Options', 'text', '<p>Custom options (JSON object)</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013082101, 'theme', 'monorail');
    }

    if ($oldversion < 2013082201) {
        //Insert data to existing tables - more social links...
        $recordid = '';
        $eliaextn = $DB->get_record('user_info_category', array('name'=>'Eliademy'));
        if (!$eliaextn) {
            $record = new stdClass();
            $record->name = 'Eliademy';
            $recordid = $DB->insert_record('user_info_category', $record);
        } else {
            $recordid = $eliaextn->id; 
        }

        //Insert data now
        if (!$DB->record_exists('user_info_field', array('shortname'=>'vk', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('vk', 'Options', 'text', '<p>VK link</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }

        if (!$DB->record_exists('user_info_field', array('shortname'=>'weibo', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('weibo', 'Options', 'text', '<p>Weibo link</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }

        if (!$DB->record_exists('user_info_field', array('shortname'=>'web', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('web', 'Options', 'text', '<p>User homepage link</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013082201, 'theme', 'monorail');
    }
    
    if ($oldversion < 2013083000) {

        // Define table monorail_sent_emails to be created
        $table = new xmldb_table('monorail_sent_emails');

        // Adding fields to table monorail_sent_emails
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('triggeredby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('fromemail', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('toemail', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('valuesjson', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('statusinfo', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_sent_emails
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_sent_emails
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013083000, 'theme', 'monorail');
    }

    if ($oldversion < 2013090300) {
        $tokens = $DB->get_recordset('external_tokens');
        foreach ($tokens as $token) {
            $token->tokentype = EXTERNAL_TOKEN_EMBEDDED;
            $token->validuntil = time()+(60*60*24);
            $DB->update_record('external_tokens', $token);
        }
        $tokens->close();
        
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013090300, 'theme', 'monorail');
    }
    
    if ($oldversion < 2013090900) {

        // Define table monorail_sessions to be created
        $table = new xmldb_table('monorail_sessions');

        // Adding fields to table monorail_sessions
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('sesskey', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('timestamp', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_sessions
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_sessions
        $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $table->add_index('sesskey', XMLDB_INDEX_UNIQUE, array('sesskey'));

        // Conditionally launch create table for monorail_sessions
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013090900, 'theme', 'monorail');
    }

    if ($oldversion < 2013103101) {
        //Insert data to existing tables - social links user profile fields!
        $recordid = '';
        $eliaextn = $DB->get_record('user_info_category', array('name'=>'Eliademy'));
        if (!$eliaextn) {
            $record = new stdClass();
            $record->name = 'Eliademy';
            $recordid = $DB->insert_record('user_info_category', $record);
        } else {
            $recordid = $eliaextn->id; 
        }
        //Insert data now
        if (!$DB->record_exists('user_info_field', array('shortname'=>'usertitle', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'usertitle', 'Title', 'text', '<p>Title of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
        }


        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013103101, 'theme', 'monorail');
    }
    
    if ($oldversion < 2013121100) {

        // Define table monorail_engagement_messages to be created
        $table = new xmldb_table('monorail_engagement_messages');

        // Adding fields to table monorail_engagement_messages
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '300', null, null, null, null);
        $table->add_field('identifier', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('template', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('userfilter', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('delay', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_engagement_messages
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_engagement_messages
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_engagement_users to be created
        $table = new xmldb_table('monorail_engagement_users');

        // Adding fields to table monorail_engagement_users
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('engagementmessagesid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_engagement_users
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Adding indexes to table monorail_engagement_users
        $table->add_index('useridengagementmessagesid', XMLDB_INDEX_UNIQUE, array('userid', 'engagementmessagesid'));

        // Conditionally launch create table for monorail_engagement_users
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'welcome-email'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'welcome-email';
            $message->identifier = 'return-true';
            $message->template = 'welcome';
            $message->userfilter = null;
            $message->type = 'event';
            $message->delay = 0;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }

        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'welcome-email-cohort'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'welcome-email-cohort';
            $message->identifier = 'return-true';
            $message->template = 'premium-welcome';
            $message->userfilter = null;
            $message->type = 'event';
            $message->delay = 0;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'personal-support-cohort'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'personal-support-cohort';
            $message->identifier = 'return-true';
            $message->template = 'personal-support-cohort';
            $message->userfilter = 'cohort-user-created-days-ago';
            $message->type = 'time';
            $message->delay = 2;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        
        // Define table monorail_skills to be created
        $table = new xmldb_table('monorail_skills');

        // Adding fields to table monorail_skills
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('cleanedname', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_skills
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_skills
        $table->add_index('name_uidx', XMLDB_INDEX_UNIQUE, array('name'));
        $table->add_index('cleanedname_uidx', XMLDB_INDEX_UNIQUE, array('cleanedname'));

        // Conditionally launch create table for monorail_skills
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Define table monorail_course_skills to be created
        $table = new xmldb_table('monorail_course_skills');

        // Adding fields to table monorail_course_skills
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('skillid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('modifiedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_course_skills
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_course_skills
        $table->add_index('courseskills_uidx', XMLDB_INDEX_UNIQUE, array('courseid', 'skillid'));

        // Conditionally launch create table for monorail_course_skills
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Define table monorail_user_skills to be created
        $table = new xmldb_table('monorail_user_skills');

        // Adding fields to table monorail_user_skills
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('skillid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('achievedfrom', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_user_skills
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_user_skills
        $table->add_index('userskill_uidx', XMLDB_INDEX_UNIQUE, array('userid', 'skillid'));

        // Conditionally launch create table for monorail_user_skills
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_course_payments to be created
        $table = new xmldb_table('monorail_course_payments');

        // Adding fields
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('active', XMLDB_TYPE_INTEGER, '1', null, null, null, null);
        $table->add_field('paymentdetails', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('timestamp', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));
        $table->add_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));

        // Adding indexes
        $table->add_index('useridcourseid', XMLDB_INDEX_NOTUNIQUE, array('userid', 'courseid'));

        // Conditionally launch create table
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

		// Define field status to be added to monorail_course_data
        $table = new xmldb_table('monorail_course_data');
        $field = new xmldb_field('status', XMLDB_TYPE_INTEGER, '10', null, null, null, '1', 'mainteacher');

        // Conditionally launch add field status
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Add a status to monorail_course_data for all courses
        $courses = $DB->get_records('course', array());
        foreach ($courses as $course) {
            if ($course->id == 1) {
                continue;
            }
            try {
                $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$course->id));
                if ($coursedata->enddate == 0 || $coursedata->enddate > time()) {
                    $coursedata->status = 1;
                } else {
                    $coursedata->status = 0;
                }
                $DB->update_record('monorail_course_data', $coursedata);
            } catch (Exception $ex) {
                add_to_log($course->id, 'monorail', 'upgrade', '2013112800', 'Could not set status: '.$ex->getMessage());
            }
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013121100, 'theme', 'monorail');
    }

    if ($oldversion < 2013121101)
    {
        // Define table monorail_ext_courses to be created
        $table = new xmldb_table('monorail_ext_courses');

        // Adding fields to table monorail_ext_courses
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('modulename', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('url', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('urlhash', XMLDB_TYPE_CHAR, '32', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('active', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('urlexists', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timestamp', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_ext_courses
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));

        // Adding indexes to table monorail_ext_courses
        $table->add_index('urlhash_idx', XMLDB_INDEX_NOTUNIQUE, array('urlhash'));
        $table->add_index('active_idx', XMLDB_INDEX_NOTUNIQUE, array('active'));

        // Conditionally launch create table for monorail_ext_courses
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_ext_course_crawls to be created
        $table = new xmldb_table('monorail_ext_course_crawls');

        // Adding fields to table monorail_ext_course_crawls
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('timestamp', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorail_ext_course_crawls
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_ext_course_crawls
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_ext_crawlcourses to be created
        $table = new xmldb_table('monorail_ext_crawlcourses');

        // Adding fields to table monorail_ext_crawlcourses
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('crawlid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('extcourseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('complete', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_ext_crawlcourses
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('crawlid', XMLDB_KEY_FOREIGN, array('crawlid'), 'monorail_ext_course_crawls', array('id'));
        $table->add_key('extcourseid', XMLDB_KEY_FOREIGN, array('extcourseid'), 'monorail_ext_courses', array('id'));

        // Adding indexes to table monorail_ext_crawlcourses
        $table->add_index('complete_idx', XMLDB_INDEX_NOTUNIQUE, array('complete'));

        // Conditionally launch create table for monorail_ext_crawlcourses
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013121101, 'theme', 'monorail');
    }

    if ($oldversion < 2014010800)
    {
        // Define table monorail_usertag to be created
        $table = new xmldb_table('monorail_usertag');

        // Adding fields to table monorail_usertag
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_usertag
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('cohortid', XMLDB_KEY_FOREIGN, array('cohortid'), 'cohort', array('id'));

        // Conditionally launch create table for monorail_usertag
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_usertag_user to be created
        $table = new xmldb_table('monorail_usertag_user');

        // Adding fields to table monorail_usertag_user
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('tagid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_usertag_user
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('tagid', XMLDB_KEY_FOREIGN, array('tagid'), 'monorail_usertag', array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Conditionally launch create table for monorail_usertag_user
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014010800, 'theme', 'monorail');
    }
    
    if ($oldversion < 2014010901) {
        
        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'customize-cohort-reminder'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'customize-cohort-reminder';
            $message->identifier = 'cohort-not-customized';
            $message->template = 'customize-cohort-reminder';
            $message->userfilter = 'cohort-and-user-created-days-ago';
            $message->type = 'time';
            $message->delay = 3;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'encourage-learning-together'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'encourage-learning-together';
            $message->identifier = 'return-true';
            $message->template = 'encourage-learning-together';
            $message->userfilter = 'cohort-and-user-created-days-ago';
            $message->type = 'time';
            $message->delay = 7;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'encourage-learning-together-2'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'encourage-learning-together-2';
            $message->identifier = 'cohort-no-participants-added';
            $message->template = 'encourage-learning-together';
            $message->userfilter = 'cohort-and-user-created-days-ago';
            $message->type = 'time';
            $message->delay = 14;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'manage-learning-space'));
        if (! $message) {        
            $message = new stdClass();
            $message->name = 'manage-learning-space';
            $message->identifier = 'return-true';
            $message->template = 'manage-learning-space';
            $message->userfilter = 'cohort-and-user-created-days-ago';
            $message->type = 'time';
            $message->delay = 8;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'cohort-creating-a-course'));
        if (! $message) {        
            $message = new stdClass();
            $message->name = 'cohort-creating-a-course';
            $message->identifier = 'return-true';
            $message->template = 'cohort-creating-a-course';
            $message->userfilter = 'cohort-and-user-created-days-ago';
            $message->type = 'time';
            $message->delay = 10;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'cohort-creating-a-course-2'));
        if (! $message) {        
            $message = new stdClass();
            $message->name = 'cohort-creating-a-course-2';
            $message->identifier = 'cohort-no-course-created';
            $message->template = 'cohort-creating-a-course';
            $message->userfilter = 'cohort-user-inactive-for-days';
            $message->type = 'time';
            $message->delay = 28;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        
        // Add percourse to engagements schema
        $table = new xmldb_table('monorail_engagement_messages');
        $field = new xmldb_field('percourse', XMLDB_TYPE_INTEGER, '10', null, null, null, '0', 'type');

        // Conditionally launch add field status
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Add percourse to all messages
        $messages = $DB->get_records('monorail_engagement_messages', array());
        foreach ($messages as $message) {
            try {
                $message->percourse = 0;
                $DB->update_record('monorail_engagement_messages', $messages);
            } catch (Exception $ex) {
                add_to_log($course->id, 'monorail', 'upgrade', '2013121601', 'Could not set percourse: '.$ex->getMessage());
            }
        }
        
        // Add courseid to engagement_users schema
        $table = new xmldb_table('monorail_engagement_users');
        $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'engagementmessagesid');
        $key = new xmldb_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));
        $indexold = new xmldb_index('useridengagementmessagesid', XMLDB_INDEX_UNIQUE, array('userid', 'engagementmessagesid'));
        $index = new xmldb_index('useridengagementmessagesid', XMLDB_INDEX_NOTUNIQUE, array('userid', 'engagementmessagesid'));
        $index2 = new xmldb_index('useridengmessidcourseid', XMLDB_INDEX_UNIQUE, array('userid', 'engagementmessagesid', 'courseid'));

        // Conditionally launch add field status
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // Add key
        try {
            $dbman->add_key($table, $key);
        } catch (Exception $ex) {
            // prob exists.. no 'key_exists' method
        }
        // Indexes
        if ($dbman->index_exists($table, $indexold)) {
            $dbman->drop_index($table, $indexold);
        }
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
        if (!$dbman->index_exists($table, $index2)) {
            $dbman->add_index($table, $index2);
        }
        
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'cohort-enroll-more-people'));
        if (! $message) {        
            $message = new stdClass();
            $message->name = 'cohort-enroll-more-people';
            $message->identifier = 'return-true';
            $message->template = 'cohort-enroll-more-people';
            $message->userfilter = 'cohort-and-user-created-days-ago';
            $message->type = 'time';
            $message->delay = 15;
            $message->status = 1;
            $message->percourse = 0;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'cohort-enroll-more-people-course'));
        if (! $message) {        
            $message = new stdClass();
            $message->name = 'cohort-enroll-more-people-course';
            $message->identifier = 'cohort-course-no-participants';
            $message->template = 'cohort-enroll-more-people-course';
            $message->userfilter = 'cohort-course-created-days-ago';
            $message->type = 'time';
            $message->delay = 1;
            $message->status = 1;
            $message->percourse = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }
            
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014010901, 'theme', 'monorail');
    }

    if ($oldversion < 2014011401)
    {
        // Define table monorail_usertag_course to be created
        $table = new xmldb_table('monorail_usertag_course');

        // Adding fields to table monorail_usertag_course
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('tagid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('roleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_usertag_course
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('tagid', XMLDB_KEY_FOREIGN, array('tagid'), 'monorail_usertag', array('id'));
        $table->add_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'user', array('id'));
        $table->add_key('roleid', XMLDB_KEY_FOREIGN, array('roleid'), 'user', array('id'));

        // Conditionally launch create table for monorail_usertag_course
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014011401, 'theme', 'monorail');
    }

    if ($oldversion < 2014031100)
    {
        // add field usertag to monorail_invite_users
        $table = new xmldb_table('monorail_invite_users');
        $field = new xmldb_field('usertag', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'timemodified');

        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014031100, 'theme', 'monorail');
    }

    if ($oldversion < 2014032000)
    {
        // Define table monorail_certificate to be created
        $table = new xmldb_table('monorail_certificate');

        // Adding fields to table monorail_certificate
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('state', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('issuedby', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('issuedat', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('hashurl', XMLDB_TYPE_CHAR, '32', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_certificate
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));
        $table->add_key('issuedby', XMLDB_KEY_FOREIGN, array('issuedby'), 'user', array('id'));

        // Conditionally launch create table for monorail_certificate
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014032000, 'theme', 'monorail');
    }

    if ($oldversion < 2014032700)
    {
        // Define table monorail_usertag_enrolment to be created
        $table = new xmldb_table('monorail_usertag_enrolment');

        // Adding fields to table monorail_usertag_enrolment
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('tagid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_usertag_enrolment
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('tagid', XMLDB_KEY_FOREIGN, array('tagid'), 'monorail_usertag', array('id'));
        $table->add_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Conditionally launch create table for monorail_usertag_enrolment
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014032700, 'theme', 'monorail');
    }

    if ($oldversion < 2014041602)
    {
        // Changing precision of field name on table monorail_skills to (20)
        $table = new xmldb_table('monorail_skills');

        // Define index name_uidx (unique) to be dropped form monorail_skills
        $index = new xmldb_index('name_uidx', XMLDB_INDEX_UNIQUE, array('name'));

        // Conditionally launch drop index name_uidx
        if ($dbman->index_exists($table, $index)) {
            $dbman->drop_index($table, $index);
        }
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '50', null, null, null, null, 'id');
        $dbman->change_field_precision($table, $field);

        // Conditionally launch add index name_uidx
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
        
        // Define index cleanedname_uidx (unique) to be dropped form monorail_skills
        $index1 = new xmldb_index('cleanedname_uidx', XMLDB_INDEX_UNIQUE, array('cleanedname'));

        // Conditionally launch drop index name_uidx
        if ($dbman->index_exists($table, $index1)) {
            $dbman->drop_index($table, $index1);
        }

        $field1 = new xmldb_field('cleanedname', XMLDB_TYPE_CHAR, '50', null, null, null, null, 'name');
        $dbman->change_field_precision($table, $field1);

        // Conditionally launch add index name_uidx
        if (!$dbman->index_exists($table, $index1)) {
            $dbman->add_index($table, $index1);
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014041602, 'theme', 'monorail');
    }

     if ($oldversion < 2014050500) {

        // Define table monorail_assign_kalvidres to be created
        $table = new xmldb_table('monorail_assign_kalvidres');

        // Adding fields to table monorail_assign_kalvidres
        $table->add_field('id', XMLDB_TYPE_INTEGER, '19', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('taskid', XMLDB_TYPE_INTEGER, '19', null, XMLDB_NOTNULL, null, null);
        $table->add_field('entry_id', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('options', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('contextualdata', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table monorail_assign_kalvidres
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_assign_kalvidres
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_course_videorsc to be created
        $table = new xmldb_table('monorail_course_videorsc');

        // Adding fields to table monorail_course_videorsc
        $table->add_field('id', XMLDB_TYPE_INTEGER, '19', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('filesize', XMLDB_TYPE_INTEGER, '19', null, XMLDB_NOTNULL, null, null);
        $table->add_field('entryid', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_course_videorsc
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_course_videorsc
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Define table monorail_course_delvideorsc to be created
        $table = new xmldb_table('monorail_course_delvideorsc');

        // Adding fields to table monorail_course_delvideorsc
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('entryid', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_course_delvideorsc
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_course_delvideorsc
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014050500, 'theme', 'monorail');
    }

    if ($oldversion < 2014050701)
    {
        // Define table monorail_user_activity_log to be created
        $table = new xmldb_table('monorail_user_activity_log');

        // Adding fields to table monorail_usertag_enrolment
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('section', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('taskid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('quizid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('action', XMLDB_TYPE_CHAR, '40', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timestamp', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_usertag_enrolment
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));
        $table->add_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));
        $table->add_key('taskid', XMLDB_KEY_FOREIGN, array('taskid'), 'assign', array('id'));
        $table->add_key('quizid', XMLDB_KEY_FOREIGN, array('quizid'), 'quiz', array('id'));

        // Conditionally launch create table for monorail_usertag_enrolment
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $dbman->add_index($table, new xmldb_index('section_idx', XMLDB_INDEX_NOTUNIQUE, array('section')));
        $dbman->add_index($table, new xmldb_index('action_idx', XMLDB_INDEX_NOTUNIQUE, array('action')));
        $dbman->add_index($table, new xmldb_index('timestamp_idx', XMLDB_INDEX_NOTUNIQUE, array('timestamp')));

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014050701, 'theme', 'monorail');
    }

    if ($oldversion < 2014051500)
    {
       //Insert data to existing tables - social links user profile fields!
        $recordid = '';
        $eliaextn = $DB->get_record('user_info_category', array('name'=>'Eliademy'));
        if (!$eliaextn) {
            $record = new stdClass();
            $record->name = 'Eliademy';
            $recordid = $DB->insert_record('user_info_category', $record);
        } else {
            $recordid = $eliaextn->id;
        }
        //Insert data now
        if (!$DB->record_exists('user_info_field', array('shortname'=>'videolimit', 'categoryid'=>$recordid))) {
            $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'videolimit', 'Videolimit', 'text', '<p>Video resource usage limit for user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', '1', '30', '100', '0', '', '')");
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014051500, 'theme', 'monorail');
    }

    if ($oldversion < 2014061700)
    {
        // Define table monorail_social_settings to be created
        $table = new xmldb_table('monorail_social_settings');

        // Adding fields to table monorail_social_settings
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('social', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('datakey', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('value', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '12', null, null, null, null);

        // Adding keys to table monorail_social_settings
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_social_settings
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorail_social_keys to be created
        $table = new xmldb_table('monorail_social_keys');

        // Adding fields to table monorail_social_keys
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('social', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('accesstoken', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('refresh', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('expires', XMLDB_TYPE_INTEGER, '12', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '12', null, null, null, null);

        // Adding keys to table monorail_social_keys
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_social_keys
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2014061700, 'theme', 'monorail');
    }

    if ($oldversion < 2014063000)
    {
        // Define table monorail_share_reminders to be created
        $table = new xmldb_table('monorail_share_reminders');

        // Adding fields to table monorail_share_reminders
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('entryid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('entrytype', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timestamp', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('action', XMLDB_TYPE_CHAR, '1', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_share_reminders
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Conditionally launch create table for monorail_share_reminders
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $dbman->add_index($table, new xmldb_index('entry_idx', XMLDB_INDEX_NOTUNIQUE, array('entryid', 'entrytype', 'userid')));

        upgrade_plugin_savepoint(true, 2014063000, 'theme', 'monorail');
    }

    if ($oldversion < 2014072201)
    {
        // add field mainteacher to monorail_course_data
        $table = new xmldb_table('monorail_cohort_info');
        $field = new xmldb_field('valid_until', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'userscount');

        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('is_paid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'valid_until');

        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2014072201, 'theme', 'monorail');
    }

    if ($oldversion < 2014090501)
    {
        // Define table monorail_external_courses to be created
        $table = new xmldb_table('monorail_external_courses');

        // Adding fields to table monorail_external_courses
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('extcode', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('provider', XMLDB_TYPE_CHAR, '300', null, XMLDB_NOTNULL, null, null);
        $table->add_field('intcode', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('lang', XMLDB_TYPE_CHAR, '10', null, null, null, 'en');

        // Adding keys to table monorail_external_courses
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('intcode', XMLDB_KEY_UNIQUE, array('intcode'));

        // Conditionally launch create table for monorail_external_courses
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

         // Define table monorail_user_ext_provider to be created
        $table = new xmldb_table('monorail_user_ext_provider');

        // Adding fields to table monorail_user_ext_provider
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('provider', XMLDB_TYPE_CHAR, '300', null, XMLDB_NOTNULL, null, null);
        $table->add_field('password', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_user_ext_provider
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_user_ext_provider
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2014090501, 'theme', 'monorail');
    }

    if ($oldversion < 2014091100) {

        // Define table monorail_invite_uniquetokens to be created
        $table = new xmldb_table('monorail_invite_uniquetokens');

        // Adding fields to table monorail_invite_uniquetokens
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('course', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('token', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('invitecode', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table monorail_invite_uniquetokens
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for monorail_invite_uniquetokens
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014091100, 'theme', 'monorail');
    }

    if ($oldversion < 2014091800) {

        // Define key intcode (unique) to be dropped form monorail_external_courses
        $table = new xmldb_table('monorail_external_courses');
        $key = new xmldb_key('intcode', XMLDB_KEY_UNIQUE, array('intcode'));

        // Launch drop key intcode
        $dbman->drop_key($table, $key);

        // Define field productid to be added to monorail_external_courses
        $field = new xmldb_field('productid', XMLDB_TYPE_CHAR, '50', null, null, null, null, 'lang');

        // Conditionally launch add field productid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014091800, 'theme', 'monorail');
    }

    if ($oldversion < 2014100100) {

        // Define table monorail_course_perms to be created
        $table = new xmldb_table('monorail_course_perms');

        // Adding fields to table monorail_course_perms
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('course', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('caccess', XMLDB_TYPE_INTEGER, '5', null, XMLDB_NOTNULL, null, null);
        $table->add_field('privacy', XMLDB_TYPE_INTEGER, '5', null, XMLDB_NOTNULL, null, null);
        $table->add_field('review', XMLDB_TYPE_INTEGER, '5', null, null, null, null);

        // Adding keys to table monorail_course_perms
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorail_course_perms
        $table->add_index('course', XMLDB_INDEX_UNIQUE, array('course'));

        // Conditionally launch create table for monorail_course_perms
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014100100, 'theme', 'monorail');
    }

    if ($oldversion < 2014120201)
    {
        // Define table monorail_webservice_cache to be created
        $table = new xmldb_table('monorail_webservice_cache');

        // Adding fields to table monorail_webservice_cache
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('servicename', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('arguments', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecached', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('data', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);

        // Adding keys to table
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Adding indexes to table
        $table->add_index('userservice', XMLDB_INDEX_NOTUNIQUE, array('userid', 'servicename'));
        $table->add_index('userserviceargs', XMLDB_INDEX_UNIQUE, array('userid', 'servicename', 'arguments'));

        // Conditionally launch create table
        if (!$dbman->table_exists($table))
        {
            $dbman->create_table($table);
        }

        // Define table monorail_webservice_cache_d to be created
        $table = new xmldb_table('monorail_webservice_cache_d');

        // Adding fields to table monorail_webservice_cache_d
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cacheid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('cachetoken', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('cacheid', XMLDB_KEY_FOREIGN, array('cacheid'), 'monorail_webservice_cache', array('id'));

        // Adding indexes to table
        $table->add_index('cacheid', XMLDB_INDEX_NOTUNIQUE, array('cacheid'));
        $table->add_index('cachetoken', XMLDB_INDEX_NOTUNIQUE, array('cachetoken'));

        // Conditionally launch create table
        if (!$dbman->table_exists($table))
        {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2014120201, 'theme', 'monorail');
    }

    if ($oldversion < 2014121901)
    {
        // add field mainteacher to monorail_course_data
        $table = new xmldb_table('monorail_cohort_info');

        $field = new xmldb_field('company_status', XMLDB_TYPE_INTEGER, '3', null, null, null, null, 'restrictdomain');
        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('demo_period_end', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'company_status');
        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('billing_details', XMLDB_TYPE_TEXT, null, null, null, null, null, 'demo_period_end');
        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('notes', XMLDB_TYPE_TEXT, null, null, null, null, null, 'billing_details');
        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        $table->add_index('company_status', XMLDB_INDEX_NOTUNIQUE, array('company_status'));
        $table->add_index('company_status_demo_end', XMLDB_INDEX_NOTUNIQUE, array('company_status', 'demo_period_end'));

        upgrade_plugin_savepoint(true, 2014121901, 'theme', 'monorail');
    }

    if ($oldversion < 2015020800)
    {
        $table = new xmldb_table('monorail_cohort_info');

        $field = new xmldb_field('next_billing', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'notes');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('peak_users', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'next_billing');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        // -------------------------

        $table = new xmldb_table('monorail_cohort_invoice');

        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('invoicenum', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timeissued', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timepaid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('paidby', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('paymentstatus', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('paymentmessage', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('invoicetype', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('description', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('quantity', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, null);
        $table->add_field('unitprice', XMLDB_TYPE_FLOAT, '10,2', null, XMLDB_NOTNULL, null, null);
        $table->add_field('totaltax', XMLDB_TYPE_FLOAT, '10,2', null, XMLDB_NOTNULL, null, null);
        $table->add_field('totalprice', XMLDB_TYPE_FLOAT, '10,2', null, XMLDB_NOTNULL, null, null);

        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('cohortid', XMLDB_KEY_FOREIGN, array('cohortid'), 'cohortid', array('id'));
        $table->add_key('paidby', XMLDB_KEY_FOREIGN, array('paidby'), 'user', array('id'));

        $table->add_index('cohortid', XMLDB_INDEX_NOTUNIQUE, array('cohortid'));
        $table->add_index('cohortstatus', XMLDB_INDEX_NOTUNIQUE, array('cohortid', 'paymentstatus'));
        $table->add_index('timepaid', XMLDB_INDEX_NOTUNIQUE, array('timepaid'));
        $table->add_index('invoicenum', XMLDB_INDEX_UNIQUE, array('invoicenum'));

        if (!$dbman->table_exists($table))
        {
            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2015020800, 'theme', 'monorail');
    }

    if ($oldversion < 2015021600)
    {
        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'premium-engagement-1-create-course'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'premium-engagement-1-create-course';
            $message->identifier = 'premium-engagement-1-create-course';
            $message->template = 'premium-engagement-1-create-course';
            $message->userfilter = 'premium-engagement-1-create-course';
            $message->type = 'time';
            $message->delay = 0;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }

        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'premium-engagement-2-invite-users'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'premium-engagement-2-invite-users';
            $message->identifier = 'premium-engagement-2-invite-users';
            $message->template = 'premium-engagement-2-invite-users';
            $message->userfilter = 'premium-engagement-2-invite-users';
            $message->type = 'time';
            $message->delay = 0;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }

        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'premium-engagement-3-seven-days-left'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'premium-engagement-3-seven-days-left';
            $message->identifier = 'premium-engagement-3-seven-days-left';
            $message->template = 'premium-engagement-3-seven-days-left';
            $message->userfilter = 'premium-engagement-3-seven-days-left';
            $message->type = 'time';
            $message->delay = 0;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }

        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'premium-engagement-4-one-day-left'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'premium-engagement-4-one-day-left';
            $message->identifier = 'premium-engagement-4-one-day-left';
            $message->template = 'premium-engagement-4-one-day-left';
            $message->userfilter = 'premium-engagement-4-one-day-left';
            $message->type = 'time';
            $message->delay = 0;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }

        // insert some engagement messages
        $message = $DB->get_record('monorail_engagement_messages', array('name'=>'premium-engagement-5-ask-for-feedback'));
        if (! $message) {
            $message = new stdClass();
            $message->name = 'premium-engagement-5-ask-for-feedback';
            $message->identifier = 'premium-engagement-5-ask-for-feedback';
            $message->template = 'premium-engagement-5-ask-for-feedback';
            $message->userfilter = 'premium-engagement-5-ask-for-feedback';
            $message->type = 'time';
            $message->delay = 0;
            $message->status = 1;
            $message->timemodified = time();
            $DB->insert_record('monorail_engagement_messages', $message);
        }

        upgrade_plugin_savepoint(true, 2015021600, 'theme', 'monorail');
    }

    if ($oldversion < 2015030200)
    {
        $table = new xmldb_table('monorail_course_data');

        $field = new xmldb_field('licence', XMLDB_TYPE_INTEGER, '3', null, null, null, null, 'status');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_index('licence', XMLDB_INDEX_NOTUNIQUE, array('licence'));
        }

        $field = new xmldb_field('derivedfrom', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'licence');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_key('derivedfrom', XMLDB_KEY_FOREIGN, array('derivedfrom'), 'course', array('id'));
        }

        upgrade_plugin_savepoint(true, 2015030200, 'theme', 'monorail');
    }

    if ($oldversion < 2015032400)
    {
        $table = new xmldb_table('monorail_invite_users');

        $field = new xmldb_field('status', XMLDB_TYPE_INTEGER, '3', null, null, null, null, 'usertag');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_index('status', XMLDB_INDEX_NOTUNIQUE, array('status'));
        }

        $field = new xmldb_field('transportid', XMLDB_TYPE_CHAR, '50', null, null, null, null, 'status');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_index('transportid', XMLDB_INDEX_NOTUNIQUE, array('transportid'));
        }

        upgrade_plugin_savepoint(true, 2015032400, 'theme', 'monorail');
    }

    if ($oldversion < 2015041000)
    {
        // Define table
        $table = new xmldb_table('monorail_cert_order');

        // Adding fields to table
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, null);
        $table->add_field('address', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('created', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('orderid', XMLDB_TYPE_CHAR, '100', null, null, null, null);

        // Adding keys to table
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));
        $table->add_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));

        // Adding indexes to table
        $table->add_index('status', XMLDB_INDEX_NOTUNIQUE, array('status'));
        $table->add_index('created', XMLDB_INDEX_NOTUNIQUE, array('created'));
        $table->add_index('orderid', XMLDB_INDEX_NOTUNIQUE, array('orderid'));

        // Conditionally launch create table
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2015041000, 'theme', 'monorail');
    }

    if ($oldversion < 2015042700)
    {
        $table = new xmldb_table('monorail_course_payments');

        $field = new xmldb_field('quantity', XMLDB_TYPE_INTEGER, '4', null, null, null, null, "timestamp");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('unitprice', XMLDB_TYPE_FLOAT, '10,2', null, null, null, null, "quantity");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('totaltax', XMLDB_TYPE_FLOAT, '10,2', null, null, null, null, "unitprice");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('totalprice', XMLDB_TYPE_FLOAT, '10,2', null, null, null, null, "totaltax");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2015042700, 'theme', 'monorail');
    }

    if ($oldversion < 2015060200)
    {
        $table = new xmldb_table('monorail_course_payments');

        $field = new xmldb_field('notes', XMLDB_TYPE_TEXT, null, null, null, null, null, "totalprice");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('monorail_cohort_invoice');

        $field = new xmldb_field('notes', XMLDB_TYPE_TEXT, null, null, null, null, null, "totalprice");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('monorail_cert_order');

        $field = new xmldb_field('quantity', XMLDB_TYPE_INTEGER, '4', null, null, null, null, "orderid");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('unitprice', XMLDB_TYPE_FLOAT, '10,2', null, null, null, null, "quantity");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('totaltax', XMLDB_TYPE_FLOAT, '10,2', null, null, null, null, "unitprice");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('totalprice', XMLDB_TYPE_FLOAT, '10,2', null, null, null, null, "totaltax");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('notes', XMLDB_TYPE_TEXT, null, null, null, null, null, "totalprice");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2015060200, 'theme', 'monorail');
    }

    if ($oldversion < 2015070600)
    {
        $table = new xmldb_table('monorail_cohort_info');

        $field = new xmldb_field('public_page', XMLDB_TYPE_INTEGER, '3', null, null, null, null, 'peak_users');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_index('company_public_page', XMLDB_INDEX_NOTUNIQUE, array('public_page'));
        }

        $field = new xmldb_field('public_page_path', XMLDB_TYPE_CHAR, '50', null, null, null, null, 'public_page');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_index('company_public_page_path', XMLDB_INDEX_UNIQUE, array('public_page_path'));
        }

        $field = new xmldb_field('organization_details', XMLDB_TYPE_TEXT, null, null, null, null, null, "public_page_path");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('published_courses', XMLDB_TYPE_TEXT, null, null, null, null, null, "organization_details");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2015070600, 'theme', 'monorail');
    }

    if ($oldversion < 2015073000)
    {
        $table = new xmldb_table('monorail_cohort_admin_users');

        $field = new xmldb_field('usertype', XMLDB_TYPE_INTEGER, '3', null, null, null, null, 'userid');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_index('usertype', XMLDB_INDEX_NOTUNIQUE, array('usertype'));
            $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
            $table->add_index('cohortid', XMLDB_INDEX_NOTUNIQUE, array('cohortid'));
        }

        $table = new xmldb_table('monorail_cohort_info');

        $field = new xmldb_field('ownerid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'published_courses');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
            $table->add_index('ownerid', XMLDB_INDEX_NOTUNIQUE, array('ownerid'));
        }

        upgrade_plugin_savepoint(true, 2015073000, 'theme', 'monorail');
    }

    if ($oldversion < 2015081801)
    {
        // Define table
        $table = new xmldb_table('monorail_cohort_user_link');

        // Adding fields to table
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('cohortid', XMLDB_KEY_FOREIGN, array('cohortid'), 'cohort', array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Adding indexes to table
        $table->add_index('cohortid', XMLDB_INDEX_NOTUNIQUE, array('cohortid'));
        $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $table->add_index('usercohort', XMLDB_INDEX_UNIQUE, array('userid', 'cohortid'));

        // Conditionally launch create table
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2015081801, 'theme', 'monorail');
    }

    if ($oldversion < 2015081900)
    {
        // add field to monorail_invite_users
        $table = new xmldb_table('monorail_invite_users');
        $field = new xmldb_field('userrole', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'usertag');

        // Conditionally launch add field
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2015081900, 'theme', 'monorail');
    }

    if ($oldversion < 2015100800)
    {
        $table = new xmldb_table('monorail_course_payments');

        $field = new xmldb_field('teacher_paid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, "notes");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2015100800, 'theme', 'monorail');
    }

    if ($oldversion < 2015102300) {
        // Define table
        $table = new xmldb_table('monorail_cohort_daily_users');

        // Adding fields to table
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('usertype', XMLDB_TYPE_INTEGER, '3', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timestamp', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('cohortid', XMLDB_KEY_FOREIGN, array('cohortid'), 'cohort', array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Adding indexes to table
        $table->add_index('cohortid', XMLDB_INDEX_NOTUNIQUE, array('cohortid'));
        $table->add_index('timestamp', XMLDB_INDEX_NOTUNIQUE, array('timestamp'));
        $table->add_index('userperday', XMLDB_INDEX_UNIQUE, array('timestamp', "userid", "cohortid"));

        // Conditionally launch create table
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table
        $table = new xmldb_table('monorail_cohort_inv_entry');

        // Adding fields to table
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('invoiceid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('usertype', XMLDB_TYPE_INTEGER, '3', null, XMLDB_NOTNULL, null, null);
        $table->add_field('datefrom', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('dateto', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('quantity', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, null);
        $table->add_field('unitprice', XMLDB_TYPE_FLOAT, '10,6', null, XMLDB_NOTNULL, null, null);
        $table->add_field('totaltax', XMLDB_TYPE_FLOAT, '10,6', null, null, null, null);
        $table->add_field('totalprice', XMLDB_TYPE_FLOAT, '10,6', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('invoiceid', XMLDB_KEY_FOREIGN, array('invoiceid'), 'monorail_cohort_invoice', array('id'));

        // Adding indexes to table
        $table->add_index('invoiceid', XMLDB_INDEX_NOTUNIQUE, array('invoiceid'));

        // Conditionally launch create table
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2015102300, 'theme', 'monorail');
    }

    if ($oldversion < 2015112100) {
        $table = new xmldb_table('monorail_cohort_info');

        $field = new xmldb_field('public_page_ann', XMLDB_TYPE_INTEGER, '3', null, null, null, null, 'public_page_path');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('public_page_ann_info', XMLDB_TYPE_TEXT, null, null, null, null, null, "public_page_ann");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('public_page_ad', XMLDB_TYPE_INTEGER, '3', null, null, null, null, 'public_page_ann_info');
        if (!$dbman->field_exists($table, $field))
        {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('public_page_ad_info', XMLDB_TYPE_TEXT, null, null, null, null, null, "public_page_ad");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('public_page_cats', XMLDB_TYPE_TEXT, null, null, null, null, null, "public_page_ad_info");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2015112100, 'theme', 'monorail');
    }

    if ($oldversion < 2016012400)
    {
        $table = new xmldb_table('monorail_cert_order');

        $field = new xmldb_field('teacher_paid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, "notes");
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2016012400, 'theme', 'monorail');
    }

    return true;
}

?>
