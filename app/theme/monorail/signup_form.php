<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 



defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

class login_signup_form extends moodleform {
    function definition() {
        global $USER, $CFG;
        
        $wantsurl = optional_param('wantsurl', '', PARAM_RAW); 
        $language = optional_param('lang', '', PARAM_RAW); 
        if(!empty($language)) {
            $USER->lang = $language;
        }
        $mform = $this->_form;
        $maxbytes   = $CFG->maxbytes;

        $nameordercheck = new stdClass();
        $nameordercheck->firstname = 'a';
        $nameordercheck->lastname  = 'b';
        if (fullname($nameordercheck) == 'b a' ) {  // See MDL-4325
            $mform->addElement('text', 'lastname',  null,  'maxlength="100" size="30" placeholder="'.get_string('login_name2', 'theme_monorail').'"');
            $mform->addElement('text', 'firstname', null, 'maxlength="100" size="30" placeholder="'.get_string('firstname').'"');
        } else {
            $mform->addElement('text', 'firstname', null, 'maxlength="100" size="30" placeholder="'.get_string('firstname').'"');
            $mform->addElement('text', 'lastname',  null,  'maxlength="100" size="30" placeholder="'.get_string('login_name2', 'theme_monorail').'"');
        }
        
        $mform->setType('firstname', PARAM_TEXT);
        $mform->addRule('firstname', get_string('missingfirstname'), 'required', null, 'server');
         
        $mform->setType('lastname', PARAM_TEXT);
        $mform->addRule('lastname', get_string('missinglastname'), 'required', null, 'server');

        $mform->addElement('text', 'email', null, 'maxlength="100" size="25" placeholder="'.get_string('login_email', 'theme_monorail').'"');
        $mform->setType('email', PARAM_NOTAGS);
        $mform->addRule('email', get_string('missingemail'), 'required', null, 'server');
         

        $country = get_string_manager()->get_list_of_countries();
        $default_country[''] = get_string('selectacountry');
        $country = array_merge($default_country, $country);
        $mform->addElement('select', 'country', get_string('country'), $country);
        //$mform->addRule('country', get_string('missingcountry'), 'required', null, 'server');
 
        if( !empty($CFG->country) ){
            $mform->setDefault('country', $CFG->country);
        }else{
            $mform->setDefault('country', '');
        }

        $mform->addElement('hidden', 'provider', '');
        $mform->addElement('hidden', 'city', '');
        $mform->addElement('hidden', 'wantsurl', $wantsurl);
        $mform->addElement('text', 'username', get_string('username'), 'maxlength="32" size="12"');
        $mform->setType('username', PARAM_RAW);
        
        $mform->addElement('text', 'institution', null, 'maxlength="40" size="12" placeholder="'.get_string('university', 'theme_monorail').'"');
        $mform->setType('institution', PARAM_RAW);
        

        $mform->addElement('password', 'password', null, 'maxlength="32" size="12" placeholder="'.get_string('password').'"');
        $mform->setType('password', PARAM_RAW);
        $mform->addRule('password', get_string('missingpassword'), 'required', null, 'server');

        $mform->addElement('password', 'password2', null, 'maxlength="32" size="12" placeholder="'.get_string('login_password2', 'theme_monorail').'"');
        $mform->setType('password2', PARAM_RAW);
        $mform->addRule('password2', get_string('missingpassword'), 'required', null, 'server');

        if ($this->signup_captcha_enabled()) {
            $mform->addElement('recaptcha', 'recaptcha_element', get_string('recaptcha', 'auth'), array('https' => $CFG->loginhttps));
            $mform->addHelpButton('recaptcha_element', 'recaptcha', 'auth');
        }


        if (!empty($CFG->sitepolicy)) {
            $mform->addElement('header', '', get_string('policyagreement'), '');
            $mform->addElement('static', 'policylink', '', '<a href="'.$CFG->sitepolicy.'" onclick="this.target=\'_blank\'">'.get_String('policyagreementclick').'</a>');
            $mform->addElement('checkbox', 'policyagreed', get_string('policyaccept'));
            $mform->addRule('policyagreed', get_string('policyagree'), 'required', null, 'server');
        }

        $this->add_action_buttons(true, get_string('createaccount'));

    }

    function definition_after_data(){
        $mform = $this->_form;
        $mform->applyFilter('username', 'trim');
    }

    //we dont want to allow '+' in email eventhough it is allowed as per RFC, so made this function
    function validate_email_extracharcheck($value) {
        //first check if email address is valid or not, if yes make sure that it does not contain '+'
        return ( filter_var($value, FILTER_VALIDATE_EMAIL) &&
            !preg_match('/[+]/', $value) );
    }

    function validation($data, $files) {
        global $CFG, $DB;
        $errors = parent::validation($data, $files);

        $authplugin = get_auth_plugin($CFG->registerauth);
        
        if (! $this->validate_email_extracharcheck($data['email'])) {
            $errors['email'] = get_string('invalidemail');

        }
        if (!isset($errors['email'])) {
            if ($err = email_is_not_allowed($data['email'])) {
                $errors['email'] = $err;
            }

        }


        if ($DB->record_exists('user', array('email'=>$data['email']))) {
            $url = new moodle_url($CFG->wwwroot.'/login/forgot_password.php');
            $errors['email'] = get_string('emailexists').' <a href="'.$url.'">'.get_string('newpassword').'?</a>';
        } else {
             
        }

        if ($data['password'] <> $data['password2']) {
            $errors['password'] = get_string('passwordsdiffer');
            $errors['password2'] = get_string('passwordsdiffer');
            return $errors;
        }

        $errmsg = '';
        if (!check_password_policy($data['password'], $errmsg)) {
            $errors['password'] = $errmsg;
        }

        if ($this->signup_captcha_enabled()) {
            $recaptcha_element = $this->_form->getElement('recaptcha_element');
            if (!empty($this->_form->_submitValues['recaptcha_challenge_field'])) {
                $challenge_field = $this->_form->_submitValues['recaptcha_challenge_field'];
                $response_field = $this->_form->_submitValues['recaptcha_response_field'];
                if (true !== ($result = $recaptcha_element->verify($challenge_field, $response_field))) {
                    $errors['recaptcha'] = $result;
                }
            } else {
                $errors['recaptcha'] = get_string('missingrecaptchachallengefield');
            }
        }
        // Validate customisable profile fields. (profile_validation expects an object as the parameter with userid set)
        $dataobject = (object)$data;
        $dataobject->id = 0;
        $errors += profile_validation($dataobject, $files);

        return $errors;

    }

    /**
     * Returns whether or not the captcha element is enabled, and the admin settings fulfil its requirements.
     * @return bool
     */
    function signup_captcha_enabled() {
        global $CFG;
        return !empty($CFG->recaptchapublickey) && !empty($CFG->recaptchaprivatekey) && get_config('auth/email', 'recaptcha');
    }

}
