<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

	/** config.php */
	require_once('../../config.php');

	/** Include locallib.php */
	require_once($CFG->dirroot . '/mod/assign/locallib.php');

	$id 	= required_param('id', PARAM_INT);  // Course Module ID
	$action = optional_param('action', null, PARAM_TEXT);  // Course Module ID
	$url 	= new moodle_url('/mod/assign/view.php', array('id' => $id, 'action' => $action)); // Base URL

	// get the request parameters
	$cm = get_coursemodule_from_id('assign', $id, 0, false, MUST_EXIST);
	$course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);

	// Auth
	require_login($course, true, $cm);

	$context = context_module::instance($cm->id);

	require_capability('mod/assign:view', $context);

	$assign = new assign($context,$cm,$course);

	$PAGE->set_url($url);
	$PAGE->set_title(get_string('pluginname', 'assign'));
	$PAGE->set_heading($cm->name);
	$PAGE->set_context($context);
	$PAGE->set_pagetype('theme-monorail_assign_details');
	$PAGE->set_pagelayout('incourse');
	$PAGE->add_body_class('path-theme-monorail-assign');

	// Mark as viewed
	$completion=new completion_info($course);
	$completion->set_module_viewed($cm);

	$submission 	= $DB->get_record('assign_submission', array('assignment'=>$assign->get_instance()->id, 'userid'=>$USER->id));
	$grading_info = grade_get_grades($PAGE->course->id, 'mod', 'assign', $cm->instance, $USER->id);
	if (isset($grading_info->items[0]) && !$grading_info->items[0]->grades[$USER->id]->hidden ) {
		$grade 		= $grading_info->items[0]->grades[$USER->id]->str_grade;
		$grade_obj 	= $grading_info->items[0]->grades[$USER->id];

		if ($grade == '-') {
			$grade = NULL;
		}
	}
	else {
		$grade = $grade_obj = NULL;
	}
	echo $OUTPUT->header();

	switch ($action) {
		case 'savesubmission':
			process_save_submission($mform, $assign, $grade_obj);
			$submission = $DB->get_record('assign_submission', array('assignment'=>$assign->get_instance()->id, 'userid'=>$USER->id));
		break;

		case 'submit':
			$plugins = $assign->get_submission_plugins();
			foreach ($plugins as $plugin) {
				if ($plugin->is_enabled() && $plugin->is_visible()) {
					if (!$plugin->precheck_submission($submission)) {
						echo $OUTPUT->notification(get_string('submissionnotready', 'assign'));
						break 2;
					}
				}
			}

			$continue 			= new single_button(new moodle_url('/theme/monorail/assign.php', array('id'=>$id, 'action'=>'confirmsubmit')), get_string('continue'));
			$continue->formid 	= 'action';

			$cancel 			= new single_button(new moodle_url('/theme/monorail/assign.php', array('id'=>$id)), get_string('cancel'));
			$cancel->formid 	= 'action';

			echo $OUTPUT->confirm(get_string('confirmsubmission', 'assign'), $continue, $cancel);
			echo $OUTPUT->footer();
			die;
		break;

		case 'confirmsubmit':
			// Need submit permission to submit an assignment
			require_capability('mod/assign:submit', $assign->get_context());
			require_sesskey();

			if ($submission->status != ASSIGN_SUBMISSION_STATUS_SUBMITTED) {
				// Give each submission plugin a chance to process the submission
				$plugins = $assign->get_submission_plugins();
				foreach ($plugins as $plugin) {
					$plugin->submit_for_grading();
				}

				$submission->status = ASSIGN_SUBMISSION_STATUS_SUBMITTED;
				confirm_submission($assign, $submission);
			}
		break;
	}

	// Open div Intro Section
	echo '<div class="section" id="intro-section">';

	// Render heading
	echo '<div class="course-info"><span class="course-title">'.$PAGE->course->fullname.'</span> <span class="course-code">('.$PAGE->course->idnumber.')</span>';

	// Render intro
	echo render_assign_intro($assign, $grade, $submission);

	// Close div Intro section;
	echo '</div>';


	// Render Feedback
	echo render_assign_feedback($assign, $grade_obj, $grade);

	// Render File submission
	echo render_file_submission($assign, $grade_obj, $submission, $action == 'editsubmission');

	echo $OUTPUT->footer();

	function check_submissions_open(assign $assign, $submission, $grade) {
		global $USER;

		$time = time();
		$dateopen = true;
		if ($assign->get_instance()->preventlatesubmissions && $assign->get_instance()->duedate) {
			$dateopen = ($assign->get_instance()->allowsubmissionsfromdate <= $time && $time <= $assign->get_instance()->duedate);
		} else {
			$dateopen = ($assign->get_instance()->allowsubmissionsfromdate <= $time);
		}

		if (!$dateopen) {
			return false;
		}

		// now check if this user has already submitted etc.
		if (!is_enrolled($assign->get_course_context(), $USER)) {
			return false;
		}
		if ($submission) {
			if ($assign->get_instance()->submissiondrafts && $submission->status == 'submitted') {
				// drafts are tracked and the student has submitted the assignment
				return false;
			}
		}
		if ($grade) {
			if ($grade->locked) {
				return false;
			}
		}

		if ($assign->grading_disabled($USER->id)) {
			return false;
		}

		return true;
	}

	function render_assign_intro(assign $assign, $grade, $submission) {

		$due = $assign->get_instance()->duedate;
		$str = '';
		// Check if submitted or graded
		if (($submission == NULL || $submission->status == ASSIGN_SUBMISSION_STATUS_DRAFT) && $grade == NULL) {
			if ($due) {
				$date = userdate($due);
				$str .= '<span class="task-due floatright">'.get_string('duedate', 'assign').': '.userdate($due, '%d.%m.%Y, %H:%M');

				// Calculate and show how much time is left for the assignment
				$timeleft = date_diff(new DateTime($date), new DateTime(date("m/d/Y h:i:s a", time())));
				if ($timeleft->invert == 1) {
					$str .= " <span class='bold'>(";
					if ($timeleft->days > 0) {
						$str .= $timeleft->days . ' ' . get_string('days');
					}
					else {
						$str .= $timeleft->h . ' ' . get_string('hours') . ' ' . $timeleft->i . ' ' . get_string('minutes');
					}
					$str .= ")&nbsp;</span>";
				}
				// In case of overdue assignment
				else {
					$str .= " <span class='bold urgent'>(";
					if ($timeleft->days > 0) {
						$str .= $timeleft->days . ' ' . get_string('days');
					}
					else {
						$str .= $timeleft->h . ' ' . get_string('hours') . ' ' . $timeleft->i . ' ' . get_string('minutes');
					}
					$str .= " ".get_string('late', 'theme_monorail').")</span>";
				}

				$str .= '</span>';
			} else {
				$str .= '<span class="task-due floatright">'.get_string('duedateno', 'assign').'</span>';
			}
		}
		else {
			$str .= '<span class="task-grade floatright">';
			if ($grade) {
				$str .= get_string('graded', 'theme_monorail') . $grade;
			}
			else {
				$str .= get_string('submitted', 'theme_monorail')." ".get_string('waiting_for_grade', 'theme_monorail');
			}
			$str .= '</span>';
		}
		$str .= '</div>';

		$str .= '<h3 class="instruction-text">'.get_string('instructions', 'theme_monorail').'</h3>';
        $intro = $assign->get_instance()->intro;
        $intro = str_replace('  ', '&nbsp; ', $intro);
        $intro = nl2br($intro);
		$str .= '<div id="intro">'.$intro.'</div>';

		return $str;
	}

	function render_assign_feedback(assign $assign, $grade_obj, $grade) {
		$comments 	= $assign->get_feedback_plugin_by_type('comments');
		$file		= $assign->get_feedback_plugin_by_type('file');
		$str		= '';
		if ($comments) {
			if ($comments->is_enabled() && $comments->is_visible() && $grade) {
				$str .= '<div id="assign-feedback" class="section"><h3 class="main">'.get_string('feedback', 'assign').'</h3>';
				$str .= "<div class='feedback-content'>$grade_obj->str_feedback</div>";
				$str .= '</div>';
			}
		}

		return $str;
	}

	function render_file_submission(assign $assign, $grade_obj, $submission, $edit = false) {
		global $OUTPUT, $PAGE;

		if ($assign->get_instance()->nosubmissions == '1') {
			return;
		}
		if (!has_capability('mod/assign:submit', $assign->get_context())) {
			return;
		}

		$filesubmission = $assign->get_submission_plugin_by_type('file');
		if (!$filesubmission->is_enabled() || !$filesubmission->is_visible()) {
			return;
		}

		$str = '<div id="file-submission" class="section"><h3 class="main">'.get_string('filesubmissions', 'assign').'</h3>';

		// Render uploaded files
		if ($submission && !$edit) {
			$summary = $filesubmission->view($submission);
			$str 	.= "<div id='submitted-file-list'>$summary</div>";
		}

		// Render upload box
		$showedit 	= has_capability('mod/assign:submit', $assign->get_context()) && check_submissions_open($assign, $submission, $grade_obj);
		$showsubmit = $submission && ($submission->status == ASSIGN_SUBMISSION_STATUS_DRAFT);

		// Render links
		if ($showsubmit) {
			// submission.php
			$str .= $OUTPUT->single_button(new moodle_url('/theme/monorail/assign.php',
					array('id' => $assign->get_course_module()->id, 'action'=>'submit')), get_string('submitassignment', 'assign'), 'get', array ('class' => 'submit-button'));
		}

		if ($showedit) {
			if (!$submission || $edit) {
				return render_submission_form($assign);
			} else {
				$str .= $OUTPUT->single_button(new moodle_url('/theme/monorail/assign.php',
						array('id' => $assign->get_course_module()->id, 'action' => 'editsubmission')), get_string('editsubmission', 'assign'), 'get', array ('class' => 'edit-button'));
			}
		}

		$str .= '<div class="clear"></div></div>';

		return $str;
	}

	function render_submission_form(assign $assign, $edit = FALSE) {
		global $OUTPUT, $PAGE, $CFG;

		require_once($CFG->dirroot . '/mod/assign/submission_form.php');

		$data = new stdClass();

		$mform = new mod_assign_submission_form(null, array($assign, $data));

		$str = '';
		ob_start();
		$mform->display();
		$str = ob_get_contents();
		ob_end_clean();

		return $str;
	}

	function process_save_submission(&$mform, assign $assign, $grade_obj) {
		global $USER, $CFG, $DB;

		// Include submission form
		require_once($CFG->dirroot . '/mod/assign/submission_form.php');

		// Need submit permission to submit an assignment
		require_capability('mod/assign:submit', $assign->get_context());
		require_sesskey();

		$data = new stdClass();
		$mform = new mod_assign_submission_form(null, array($assign, $data));
		if ($mform->is_cancelled()) {
			return true;
		}
		if ($data = $mform->get_data()) {
			$submission = get_user_submission($assign, $USER->id, true); //create the submission if needed & its id
			if ($grade_obj && $grade_obj->locked) {
				print_error('submissionslocked', 'assign');
				return true;
			}

			$plugin = $assign->get_submission_plugin_by_type('file');
			if ($plugin->is_enabled()) {
				if (!$plugin->save($submission, $data)) {
					print_error($plugin->get_error());
				}
			}

			$submission->timemodified = time();

			$result	= $DB->update_record('assign_submission', $submission);
			if ($result) {
				$gradebookgrade 					= array();
		        $gradebookgrade['userid']			= $submission->userid;
		        $gradebookgrade['usermodified'] 	= $submission->userid;
		        $gradebookgrade['datesubmitted'] 	= $submission->timemodified;

		        $assign_instance = clone $assign->get_instance();
		        $assign_instance->cmidnumber = $assign->get_course_module()->id;
		        assign_grade_item_update($assign_instance, $gradebookgrade);
			}


			// Logging
 			$assign->add_to_log('submit', format_submission_for_log($assign, $submission));

			if (!$assign->get_instance()->submissiondrafts) {
				notify_student_submission_receipt($assign, $submission);
				notify_graders($assign, $submission);
			}
			return true;
		}
		return false;
	}

	function get_user_submission(assign $assign, $userid, $create) {
		global $DB;

		// if the userid is not null then use userid
		$submission = $DB->get_record('assign_submission', array('assignment'=>$assign->get_instance()->id, 'userid'=>$userid));

		if ($submission) {
			return $submission;
		}
		if ($create) {
			$submission = new stdClass();
			$submission->assignment   = $assign->get_instance()->id;
			$submission->userid       = $userid;
			$submission->timecreated  = time();
			$submission->timemodified = $submission->timecreated;

			if ($assign->get_instance()->submissiondrafts) {
				$submission->status = ASSIGN_SUBMISSION_STATUS_DRAFT;
			} else {
				$submission->status = ASSIGN_SUBMISSION_STATUS_SUBMITTED;
			}
			$sid = $DB->insert_record('assign_submission', $submission);
			$submission->id = $sid;
			return $submission;
		}
		return false;
	}

	function confirm_submission(assign $assign, & $submission) {
		global $DB;

		// Update submission
		$submission->timemodified = time();

		$result= $DB->update_record('assign_submission', $submission);
		if ($result) {
			$gradebookgrade 					= array();
		        $gradebookgrade['userid']			= $submission->userid;
		        $gradebookgrade['usermodified'] 	= $submission->userid;
		        $gradebookgrade['datesubmitted'] 	= $submission->timemodified;

		        $assign_instance = clone $assign->get_instance();
		        $assign_instance->cmidnumber = $assign->get_course_module()->id;
		        assign_grade_item_update($assign_instance, $gradebookgrade);
		}

 		$assign->add_to_log('submit for grading', format_submission_for_log($assign, $submission));
 		notify_student_submission_receipt($assign, $submission);
		notify_graders($assign, $submission);
	}

	function format_submission_for_log(assign $assign, $submission) {
		$info = '';
		$info .= get_string('submissionstatus', 'assign') . ': ' . get_string('submissionstatus_' . $submission->status, 'assign') . '. <br>';
		// format_for_log here iterating every single log INFO  from either submission or grade in every assignment plugin

		foreach ($assign->get_submission_plugins() as $plugin) {
			if ($plugin->is_enabled() && $plugin->is_visible()) {
				$info .= "<br>" . $plugin->format_for_log($submission);
			}
		}

		return $info;
	}

	function notify_graders(assign $assign, $submission) {
		global $DB;

        $late = $assign->get_instance()->duedate && ($assign->get_instance()->duedate < time());

        if (!$assign->get_instance()->sendnotifications && !($late && $assign->get_instance()->sendlatenotifications)) {          // No need to do anything
            return;
        }

        $user = $DB->get_record('user', array('id'=>$submission->userid), '*', MUST_EXIST);

        // Get the list of Graders
        // potential graders
        $potentialgraders = get_enrolled_users($assign->get_context(), "mod/assign:grade");
        $graders = array();
        if (groups_get_activity_groupmode($assign->get_course_module()) == SEPARATEGROUPS) {   // Separate groups are being used
        	if ($groups = groups_get_all_groups($assign->get_course()->id, $user->id)) {  // Try to find all groups
        		foreach ($groups as $group) {
        			foreach ($potentialgraders as $grader) {
        				if ($grader->id == $user->id) {
        					continue; // do not send self
        				}
        				if (groups_is_member($group->id, $grader->id)) {
        					$graders[$grader->id] = $grader;
        				}
        			}
        		}
        	} else {
        		// user not in group, try to find graders without group
        		foreach ($potentialgraders as $grader) {
        			if ($grader->id == $user->id) {
        				continue; // do not send self
        			}
        			if (!groups_has_membership($assign->get_course_module(), $grader->id)) {
        				$graders[$grader->id] = $grader;
        			}
        		}
        	}
        } else {
        	foreach ($potentialgraders as $grader) {
        		if ($grader->id == $user->id) {
        			continue; // do not send self
        		}
        		// must be enrolled
        		if (is_enrolled($assign->get_course_context(), $grader->id)) {
        			$graders[$grader->id] = $grader;
        		}
        	}
        }

        if ($graders) {
            foreach ($graders as $teacher) {
                $assign->send_notification($user, $teacher, 'gradersubmissionupdated', 'assign_notification', $submission->timemodified);
            }
        }
	}

	function notify_student_submission_receipt(assign $assign, stdClass $submission) {
		global $DB;

		$adminconfig = $assign->get_admin_config();
		if (!$adminconfig->submissionreceipts) {
			// No need to do anything
			return;
		}
		$user = $DB->get_record('user', array('id'=>$submission->userid), '*', MUST_EXIST);
		send_assignment_notification($user, $user, 'submissionreceipt', 'assign_notification', $submission->timemodified,
				$assign->get_course_module(), $assign->get_context(), $assign->get_course(), get_string('modulename', 'assign'), $assign->get_instance()->name);
	}

	function send_assignment_notification($userfrom, $userto, $messagetype, $eventtype,
                                                        $updatetime, $coursemodule, $context, $course,
                                                        $modulename, $assignmentname) {
        global $CFG;

        $info 				= new stdClass();
        $info->username 	= fullname($userfrom, true);
        $info->assignment 	= format_string($assignmentname,true, array('context'=>$context));
        $info->url 			= $CFG->magic_ui_root.'/tasks/'.$coursemodule->id;
        $info->timeupdated 	= strftime('%c',$updatetime);

        $postsubject = get_string($messagetype . 'small', 'assign', $info);
        $posttext  	 = format_string($course->shortname, true, array('context' => $context->get_course_context())).' -> '.
                     	$modulename.' -> '.
                     	format_string($assignmentname, true, array('context' => $context))."\n";
        $posttext 	.= '---------------------------------------------------------------------'."\n";
        $posttext 	.= get_string($messagetype . 'text', "assign", $info)."\n";
        $posttext 	.= "\n---------------------------------------------------------------------\n";
        $posthtml 	 = ($userto->mailformat == 1) ? format_notification_message_html($messagetype, $info, $course, $context, $modulename, $coursemodule, $assignmentname) : '';

        $eventdata = new stdClass();
        $eventdata->modulename       = 'assign';
        $eventdata->userfrom         = $userfrom;
        $eventdata->userto           = $userto;
        $eventdata->subject          = $postsubject;
        $eventdata->fullmessage      = $posttext;
        $eventdata->fullmessageformat = FORMAT_PLAIN;
        $eventdata->fullmessagehtml  = $posthtml;
        $eventdata->smallmessage     = $postsubject;

        $eventdata->name            = $eventtype;
        $eventdata->component       = 'mod_assign';
        $eventdata->notification    = 1;
        $eventdata->contexturl      = $info->url;
        $eventdata->contexturlname  = $info->assignment;

        message_send($eventdata);
    }

    function format_notification_message_html($messagetype, $info, $course, $context, $modulename, $coursemodule, $assignmentname) {
    	global $CFG;
    	$posthtml  = '<p><font face="sans-serif">'.
    			'<a href="'.$CFG->magic_ui_root.'/courses/'.$course->id.'">'.format_string($course->shortname, true, array('context' => $context->get_course_context())).'</a> ->'.
    			'<a href="'.$CFG->magic_ui_root.'/courses/'.$course->id.'/tasks">'.$modulename.'</a> ->'.
    			'<a href="'.$CFG->magic_ui_root.'/tasks/'.$coursemodule->id.'">'.format_string($assignmentname, true, array('context' => $context)).'</a></font></p>';
    	$posthtml .= '<hr /><font face="sans-serif">';
    	$posthtml .= '<p>'.get_string($messagetype . 'html', 'assign', $info).'</p>';
    	$posthtml .= '</font><hr />';
    	return $posthtml;
    }
