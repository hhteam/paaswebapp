<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

define('NUMBER_OF_HEADER_TABS',		4);
define('MONORAIL_PAGE_COURSES', 	0);
define('MONORAIL_PAGE_CALENDAR', 	1);
define('MONORAIL_PAGE_NOTES', 		2);
define('MONORAIL_PAGE_PROFILE', 	3);