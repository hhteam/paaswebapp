<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require_once __DIR__ . "/../../../config.php";
require_once($CFG->libdir.'/clilib.php');      // cli only functions

cli_heading('Collecting payments... $$$');

require_once __DIR__ . "/../premium_payments.php";

premium_payments_process();
