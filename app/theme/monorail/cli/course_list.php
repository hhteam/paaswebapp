<?php

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";

global $DB;

echo "\"Course name\", \"Students\", \"Course URL\", \"Category\", \"Language\"\n";

$courses = $DB->get_records_sql("SELECT c.id AS id, c.fullname AS fullname, COUNT(ra.userid) AS students, cat.name AS catname" .
    " FROM {course} AS c" .
    " INNER JOIN {context} AS ctx ON c.id=ctx.instanceid" .
    " INNER JOIN {role_assignments} AS ra ON ra.contextid=ctx.id" .
    " INNER JOIN {role} AS r ON r.id=ra.roleid" .
    " INNER JOIN {course_categories} AS cat ON cat.id=c.category" .
        " WHERE ctx.contextlevel=50 AND r.shortname='student' GROUP BY c.id ORDER BY students DESC");

foreach($courses as $course)
{
    $permData = $DB->get_record_sql("SELECT * FROM {monorail_course_perms} WHERE course=?", array($course->id));

    if ($permData->privacy != 1 || $permData->caccess == 1)
    {
        // Not public or draft
        continue;
    }

    $courseData = $DB->get_record_sql("SELECT * FROM {monorail_course_data} WHERE courseid=?", array($course->id));

    echo "\"" . str_replace("\"", "'", $course->fullname) . "\", " .
            "\"" . $course->students . "\", " .
            "\"https://eliademy.com/app/a/courses/" .  $courseData->code . "\", " .
            "\"" . $course->catname . "\", " .
            $DB->get_field_sql("SELECT value FROM {monorail_data} WHERE datakey='language' AND itemid=?", array($course->id)) . "\n";
}
