<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/* Remove unused temporary files.
 *
 * Run this once in a while to delete old temporary files and empty
 * directories.
 * */

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";

global $DB;

cli_heading("CLEANING TEMPORARY FILES...");

// File age that is considered old enough.
$consideredOld = time() - 7776000;  // (3 mon from now)

// If this is true, don't really delete anything.
$safetyLock = true;


function do_dir($dir)
{
    global $consideredOld;
    global $safetyLock;

    $files = scandir($dir);
    $isempty = true;

    foreach ($files as $file)
    {
        if (substr($file, 0, 1) == ".")
        {
            continue;
        }

        $fullname = $dir . "/" . $file;

        if (is_dir($fullname))
        {
            if (!do_dir($fullname))
            {
                $isempty = false;
            }
        }
        else
        {
            if (filemtime($fullname) < $consideredOld && fileatime($fullname) < $consideredOld)
            {
                echo "Deleting file: " . $fullname;

                if (!$safetyLock)
                {
                    if (unlink($fullname))
                    {
                        echo " OK";
                    }
                }

                echo "\n";
            }
            else
            {
                $isempty = false;
            }
        }
    }

    if ($isempty)
    {
        echo "Deleting directory: " . $dir;

        if (!$safetyLock)
        {
            if (rmdir($dir))
            {
                echo " OK";
            }
        }

        echo "\n";
    }

    return $isempty;
}

do_dir($CFG->dataroot . '/temp/files');

if ($safetyLock)
{
    echo "Safety lock was ON.\n";
}
