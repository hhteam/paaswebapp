<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/* Migrate cohort licences to new 30-day trial, pay per use mode.
 *
 * Run only once!
 * */

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";

global $DB;

cli_heading("MIGRATING COHORTS...");

$demo_period_end = strtotime("+30 days midnight");

$cohorts = $DB->get_records_sql("SELECT id, userscount, valid_until, company_status FROM {monorail_cohort_info}");

foreach ($cohorts as $cohort) {
    if (!$cohort->company_status) {
        if ($cohort->valid_until) {
            // Has paid
            $cohort->company_status = 2;
        } else {
            if ($cohort->userscount == 5) {
                // Demo
                $cohort->userscount = 0;
                $cohort->company_status = 1;
                $cohort->demo_period_end = $demo_period_end;
            } else {
                // NGO (probably)
                $cohort->company_status = 3;
            }
        }

        $DB->update_record("monorail_cohort_info", $cohort);
    }
}
