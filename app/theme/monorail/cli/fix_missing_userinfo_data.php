<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


/**
 * Fix missing userinfo data
 */

define('CLI_SCRIPT', true);

require('/var/www/app/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions


global $DB;

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false),
                                               array('h'=>'help'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    $help =
"Fix missing userinfo data.

Options:
-h, --help            Print out this help

Example:
\$sudo -u www-data /usr/bin/php theme/monorail/cli/<script>.php
";

    echo $help;
    die;
}
cli_heading('Trigger engagement messages.');

//Insert data to existing tables - social links user profile fields!
$recordid = '';
$eliaextn = $DB->get_record('user_info_category', array('name'=>'Eliademy'));
if (!$eliaextn) {
    $record = new stdClass();
    $record->name = 'Eliademy';
    $recordid = $DB->insert_record('user_info_category', $record);
} else {
    $recordid = $eliaextn->id; 
}
//Insert data now
if (!$DB->record_exists('user_info_field', array('shortname'=>'facebook', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'facebook', 'Facebook', 'text', '<p>Facebook URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}
if (!$DB->record_exists('user_info_field', array('shortname'=>'linkedin', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'linkedin', 'LinkedIn', 'text', '<p>LinkedIn URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'googleplus', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'googleplus', 'Googleplus', 'text', '<p>Google plus URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'twitter', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'twitter', 'Twitter', 'text', '<p>Twitter URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'pinterest', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'pinterest', 'Pinterest', 'text', '<p>Pinterest URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'instagram', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('instagram', 'Instagram', 'text', '<p>Instagram URL of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'options', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('options', 'Options', 'text', '<p>Custom options (JSON object)</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'vk', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('vk', 'Options', 'text', '<p>VK link</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'weibo', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('weibo', 'Options', 'text', '<p>Weibo link</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'web', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES('web', 'Options', 'text', '<p>User homepage link</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

if (!$DB->record_exists('user_info_field', array('shortname'=>'usertitle', 'categoryid'=>$recordid))) {
    $DB->execute("INSERT INTO {user_info_field} (`shortname`, `name`, `datatype`, `description`, `descriptionformat`, `categoryid`, `sortorder`, `required`, `locked`, `visible`, `forceunique`, `signup`, `defaultdata`, `defaultdataformat`, `param1`, `param2`, `param3`, `param4`, `param5`) VALUES( 'usertitle', 'Title', 'text', '<p>Title of the user</p>', 1, $recordid, 1, 0, 0, 2, 0, 0, '', 0, '30', '2048', '0', '', '')");
}

exit(0); // 0 means success
