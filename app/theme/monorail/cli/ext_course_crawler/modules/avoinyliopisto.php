<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

$_modules[] = "avoinyliopisto";


function avoinyliopisto_update_course_list()
{
    $doc = new DOMDocument();

    $doc->loadHTML(file_get_contents("http://www.avoinyliopisto.fi/en-gb/urlhaku.aspx?queryid=3852361d-6e00-4b7d-a720-1069e92e5327"), LIBXML_NOWARNING);

    $path = new DOMXPath($doc);

    foreach ($path->query("/html/body/form/div/table/tr/td/div/div/div/div/small/div/div/table/tr/td[1]/a") as $link)
    {
        add_link("http://www.avoinyliopisto.fi" . $link->attributes->getNamedItem("href")->nodeValue, "avoinyliopisto");
    }
}

function avoinyliopisto_scan_course($url, $courseid)
{
    $doc = new DOMDocument();

    $doc->loadHTML(file_get_contents($url));

    $path = new DOMXPath($doc);

    $data = array();
    $monorailData = array();

    $data["title"] = trim($path->query("//*[@id=\"ctl00_body_ctl00_main_ctl02_publicViewer_fieldName\"]")->item(0)->textContent);
    $data["description"] = trim($path->query("//*[@id=\"ctl00_body_ctl00_main_ctl02_publicViewer_sectionDescription\"]/div")->item(0)->textContent);

    $monorailData["source_url"] = $url;
    $monorailData["course_type"] = "offline";

    return add_course($courseid, "avoinyliopisto", $data, $monorailData);
}
