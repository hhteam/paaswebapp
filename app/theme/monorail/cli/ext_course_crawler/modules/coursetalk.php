<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

$_modules[] = "coursetalk";

function coursetalk_update_course_list()
{
    $url = "http://coursetalk.org/";
    $count = 0;

    while ($count < 200)
    {
        $doc = new DOMDocument();

        $doc->loadHTML(file_get_contents($url), LIBXML_NOWARNING);

        $path = new DOMXPath($doc);

        foreach ($path->query("//*[@id=\"main\"]/table/tr/td[2]/table/tr/td[2]/a[1]") as $link)
        {
            $count++;

            add_link("http://coursetalk.org" . $link->attributes->getNamedItem("href")->nodeValue, "coursetalk");
        }

        $next = $path->query("//*[@id=\"main\"]/table/tr/td[2]/div/ul/li[3]/a");

        if ($next->length)
        {
            $url = "http://coursetalk.org/" . $next->item(0)->attributes->getNamedItem("href")->nodeValue;
        }
        else
        {
            break;
        }
    }
}

function coursetalk_scan_course($url, $courseid)
{
    $doc = new DOMDocument();

    $doc->loadHTML(file_get_contents($url));

    $path = new DOMXPath($doc);

    $data = array();
    $monorailData = array();

    $data["title"] = trim($path->query("//*[@id=\"main\"]/div[2]/h2/a")->item(0)->textContent);

    $doc2 = new DOMDocument();

    $doc2->loadHTML(file_get_contents("http://coursetalk.org" . $path->query("//*[@id=\"main\"]/div[2]/table/tr/td[1]/iframe")->item(0)->attributes->getNamedItem("src")->nodeValue));

    $data["description"] = trim($doc2->textContent);

    $monorailData["source_url"] = $url;
    $monorailData["course_type"] = "online";

    return add_course($courseid, "coursetalk", $data, $monorailData);
}
