<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once __DIR__ . '/../../../../config.php';
require_once $CFG->libdir . '/clilib.php';
require_once $CFG->libdir . '/enrollib.php';
require_once $CFG->libdir . '/../course/lib.php';
require_once __DIR__ . "/../../lib.php";

require_once __DIR__ . "/modlib.php";

$_modules = array();

// Load modules.
foreach (scandir(__DIR__ . "/modules/") as $item)
{
    if (substr($item, 0, 1) != ".")
    {
        require_once __DIR__ . "/modules/" . $item;
    }
}

/* Check if course crawl is currently active.
 * */
function crawl_active()
{
    global $DB;

    return $DB->record_exists("monorail_ext_crawlcourses", array("complete" => 0));
}

/* Get time of last crawl.
 * */
function last_crawl_time()
{
    global $DB;

    return $DB->get_field_sql("SELECT MAX(timestamp) FROM {monorail_ext_course_crawls}");
}

/* Start a new crawl on existing links.
 * */
function start_crawl()
{
    global $DB;

    $trans = $DB->start_delegated_transaction();

    $rec = new stdClass();

    $rec->timestamp = time();

    $crawlId = $DB->insert_record("monorail_ext_course_crawls", $rec);

    foreach ($DB->get_records("monorail_ext_courses", array("active" => 1), "", "id") as $course)
    {
        $rec = new stdClass();

        $rec->crawlid = $crawlId;
        $rec->extcourseid = $course->id;
        $rec->complete = 0;

        $DB->insert_record("monorail_ext_crawlcourses", $rec);
    }

    $trans->allow_commit();
}

/* Crawl through the next chunk of courses.
 * */
function crawl_next_chunk()
{
    global $DB;

    $trans = $DB->start_delegated_transaction();

    $records = $DB->get_records_sql("SELECT crawl.id AS id, course.id AS cid, course.modulename AS module, course.url AS url, course.courseid AS courseid " .
        "FROM {monorail_ext_courses} AS course " .
        "INNER JOIN {monorail_ext_crawlcourses} AS crawl ON course.id=crawl.extcourseid " .
        "WHERE crawl.complete=0 LIMIT 20");

    foreach ($records as $rec)
    {
        $fun = $rec->module . "_scan_course";

        $cid = $fun($rec->url, $rec->courseid);

        $up = new stdClass();

        $up->id = $rec->id;
        $up->complete = 1;

        $DB->update_record("monorail_ext_crawlcourses", $up);

        $c_up = new stdClass();

        $c_up->id = $rec->cid;
        $c_up->courseid = $cid;

        $DB->update_record("monorail_ext_courses", $c_up);
    }

    $trans->allow_commit();
}

/* Scan external course sources and save new links.
 * */
function update_course_list()
{
    global $_modules, $DB;

    $trans = $DB->start_delegated_transaction();

    foreach ($_modules as $mod)
    {
        $fun = $mod . "_update_course_list";

        $fun();
    }

    $trans->allow_commit();
}
