<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

define('CLI_SCRIPT', true);

require_once __DIR__ . "/lib.php";

if (crawl_active())
{
    crawl_next_chunk();
}
else
{
    $last = last_crawl_time();

    if (!$last || $last < time() - @$CFG->external_crawl_interval)
    {
        update_course_list();

        start_crawl();
    }
}
