<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

function add_link($link, $module)
{
    global $DB;

    $hash = md5($link);
    $exists = false;

    foreach ($DB->get_records("monorail_ext_courses", array("urlhash" => $hash), "", "id,url") as $rec)
    {
        if ($rec->url == $link)
        {
            $exists = true;
            break;
        }
    }

    if (!$exists)
    {
        $rec = new stdClass();

        $rec->modulename = $module;
        $rec->url = $link;
        $rec->urlhash = $hash;
        $rec->active = 1;
        $rec->urlexists = 1;
        $rec->timestamp = time();

        $DB->insert_record("monorail_ext_courses", $rec);
    }
}

function module_category($module)
{
    global $DB;
    static $cache = array();

    if (array_key_exists($module, $cache))
    {
        return $cache[$module];
    }

    $cat = $DB->get_record("course_categories", array("name" => $module), "id");

    if (!$cat)
    {
        $rec = new stdClass();

        $rec->name = $module;

        $cat = create_course_category($rec);
    }

    $cache[$module] = $cat->id;

    return $cat->id;
}

function random_key($len = 10)
{
    $key = "";

    for ($i=0; $i<$len; $i++)
    {
        $key .= chr(rand(ord('a'), ord('z')));
    }

    return $key;
}

function update_section($courseid, $text)
{
    global $DB;

    $section = $DB->get_record("course_sections", array("course" => $courseid, "section" => "0"), "id");

    if ($section)
    {
        $section->summary = $text;

        $DB->update_record("course_sections", $section);
    }
}

function add_course($courseid, $module, $data, $monorailData)
{
    global $DB, $CFG;

    if ($courseid)
    {
        $rec = new stdClass();

        $rec->id = $courseid;
        $rec->fullname = $data["title"];

        update_course($rec);
        update_section($rec->id, $data["description"]);

        foreach ($monorailData as $mdKey => $mdV)
        {
            monorail_data("EXT_DATA", $courseid, $mdKey, $mdV);
        }

        return $courseid;
    }
    else
    {
        $rec = new stdClass();

        $rec->fullname = $data["title"];
        $rec->category = module_category($module);

        $rec = create_course($rec);

        $enrol = enrol_get_plugin('manual');
        $instance = null;

        foreach (enrol_get_instances($rec->id, true) as $courseenrolinstance)
        {
            if ($courseenrolinstance->enrol == "manual")
            {
                $instance = $courseenrolinstance;
                break;
            }
        }

        if ($instance)
        {
            $roleid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);

            $enrol->enrol_user($instance, $CFG->default_external_teacher, $roleid, time(), 0, ENROL_USER_ACTIVE);
        }

        $dataRec = new stdClass();

        do
        {
            $dataRec->code = random_key();
        }
        while ($DB->record_exists("monorail_course_data", array("code" => $dataRec->code)));

        $dataRec->courseid = $rec->id;
        $dataRec->mainteacher = $CFG->default_external_teacher;

        $DB->insert_record("monorail_course_data", $dataRec);

        update_section($rec->id, $data["description"]);

        $DB->delete_records("course_sections", array("section" => "1"));

        foreach ($monorailData as $mdKey => $mdV)
        {
            monorail_data("EXT_DATA", $rec->id, $mdKey, $mdV);
        }

        return $rec->id;
    }
}
