<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


/**
 * Migrate all user avatars to new location
 */

define('CLI_SCRIPT', true);

require_once __DIR__ . "/../../../config.php";

require_once($CFG->libdir.'/clilib.php');      // cli only functions


global $DB;

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false),
                                               array('h'=>'help'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    $help =
"Trigger engagement messages.

Options:
-h, --help            Print out this help

Example:
\$sudo -u www-data /usr/bin/php theme/monorail/cli/<script>.php
assumptions, assumptions...
";

    echo $help;
    die;
}
cli_heading('Trigger engagement messages.');

require_once __DIR__ . "/../engagements.php";

engagements_process();

exit(0); // 0 means success
