<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/* Go through each course in magneto and make sure its' main teacher data
 * is up to date.
 * */

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";

global $DB;

cli_heading("FIXING COURSE TEACHER DATA IN MAGNETO...");

require_once __DIR__ . "/../../../local/monorailservices/mageapi.php";
$api = new MageApi($CFG->mage_api_url);

if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
{
    $products = $api->getProductList();

    $api->begin();
    $counter = 0;

    foreach ($products as $product)
    {
        $user = $DB->get_records_sql("SELECT u.id AS id, u.description AS bio, md.value AS pichash FROM {monorail_course_data} AS mcd " .
            "INNER JOIN {user} AS u ON mcd.mainteacher=u.id " .
            "INNER JOIN {monorail_data} AS md ON md.itemid=u.id " .
                "WHERE md.datakey='pichash' AND mcd.code=?", array($product["sku"]));

        if (!empty($user))
        {
            $user = reset($user);
            $soclinks = array();

            $slinks = $DB->get_records_sql("SELECT id, social, value FROM {monorail_social_settings} WHERE datakey='accounturl' AND userid=?", array($user->id));

            foreach ($slinks as $sl)
            {
                $soclinks[] = array("type" => $sl->social, "link" => $sl->value);
            }

            $api->updateProduct($product["sku"], array("additional_attributes" => array("single_data" => array(
                    "teacher_bio" => $user->bio,
                    "teacher_pichash" => $user->pichash,
                    "teacher_social_links" => json_encode($soclinks)
                ))));

            $counter++;

            if ($counter % 20 == 0)
            {
                $api->commit();
                $api->begin();
            }
        }
    }

    $api->commit();
    $api->disconnect();
}
else
{
    echo 'ERROR: unable to connect to magento catalog!';
}
