<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


/**
 * Fix missing userinfo data
 */

define('CLI_SCRIPT', true);

require('/var/www/app/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions

global $DB;

cli_heading("FLATTENING FORUM THREADS");

$allDiscussions = $DB->get_fieldset_sql("SELECT id FROM {forum_discussions} ORDER BY id");

foreach ($allDiscussions as $disc)
{
    $posts = $DB->get_records_sql("SELECT id, parent FROM {forum_posts} WHERE discussion=? ORDER BY id", array($disc));

    if (count($posts) > 1)
    {
        $toFlatten = array();
        $firstRoot = null;

        foreach ($posts as $post)
        {
            if (!$post->parent && !$firstRoot)
            {
                $firstRoot = $post->id;

                break;
            }
        }

        if ($firstRoot)
        {
            foreach ($posts as $post)
            {
                if ($post->id != $firstRoot && $post->parent != $firstRoot)
                {
                    $toFlatten[] = $post->id;
                }
            }

            if (!empty($toFlatten))
            {
                echo "Discussion " . $disc . ": flattenning: " . implode(", ", $toFlatten) . " to " . $firstRoot . "\n";

                $DB->execute("UPDATE {forum_posts} SET parent=? WHERE id IN (" . implode(", ", $toFlatten) . ")", array($firstRoot));
            }
        }
        else
        {
            echo "Invalid discussion: no root: " . $disc . "\n";
        }
    }
}
