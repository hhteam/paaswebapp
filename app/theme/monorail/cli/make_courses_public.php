<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";
require_once __DIR__ . "/../../../local/monorailservices/mageapi.php";

global $DB;

cli_heading("Moving courses to public");

echo "Moving courses...";


$startId = 1;
$endId = 500;

$bwords = file_get_contents('badwords.txt');

$file = 'movedcourses.txt';
$current = file_get_contents($file);

$cohortCourses  = implode(", ", $DB->get_fieldset_sql("SELECT DISTINCT courseid FROM {monorail_cohort_courseadmins}")); 
$cquery = empty($cohortCourses) ? "" : " AND NOT id IN (" . $cohortCourses . ")";
$query = "SELECT id FROM {course} WHERE id BETWEEN " . $startId ." AND " . $endId . $cquery;
foreach($DB->get_fieldset_sql($query) as $COURSEID)
{
   $course = $DB->get_record("course", array("id" => $COURSEID)); 
   if($course) {
      $cperm = monorail_get_course_perms($COURSEID);
      if($cperm["course_privacy"] == 1) {
        //Already public
        continue;
      }
      $fullname = $course->fullname;
      $isBad = preg_match('/('. $bwords .')/i', '"'.$fullname.'"');
      if($isBad) {
        $current .= "BAD NAME :".$course->fullname."\n";
        monorail_update_course_perms($COURSEID, $cperm["course_access"], 5); // 5 - unlisted/blocked course;
        continue;           
      }
      $coursecontext = context_course::instance($COURSEID);
      $role = $DB->get_record('role', array('shortname' => 'student'));
      $students = count(get_role_users($role->id, $coursecontext));
      if($students < 5) {
        $current .= "Less than 5 students :".$course->fullname."\n";
        monorail_update_course_perms($COURSEID, 1, 1); 
        continue;
      } else {
        monorail_update_course_perms($COURSEID, 2, 1); 
      }  
      $summary = $DB->get_record_sql("SELECT summary FROM {course_sections} WHERE section=0 AND course=?", array($COURSEID));
      if(count(explode(" ", $summary->summary)) < 100) {
        monorail_update_course_perms($COURSEID, 1, 1); 
        $current .= "Summary less than 100 :".$course->fullname."\n";
        continue;
      } else {
        monorail_update_course_perms($COURSEID, 2, 1); 
      }  
      $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($COURSEID));
      $monorailData = array();

      foreach ($moreData as $md)
      {
          $monorailData[$md->datakey] = $md->value;
      }
     $monorailData["course_privacy"] = 1; //Public;
     $monorailData["course_access"] = 2; //Invite only

     $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$course->id), '*', MUST_EXIST);
     $current .= "Publishing course :".$course->fullname."   "."Course Id:".$course->id."\n";
     $current .= "Publishing course :".$course->fullname."\n";
     magento_update_course($coursedata, $monorailData, $course); 
   }
}

file_put_contents($file, $current);
echo "\n";
