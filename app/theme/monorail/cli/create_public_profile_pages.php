<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";

global $DB;

cli_heading("CREATING PUBLIC PROFILE PAGES FOR USERS");

foreach ($DB->get_fieldset_sql("SELECT id FROM {user} WHERE confirmed AND NOT deleted AND NOT suspended") as $USERID)
{
    ob_start();

    require dirname(__FILE__) . "/../templates/profile.phtml";

    $text = ob_get_contents();

    ob_end_clean();

    if (empty($text))
    {
        @unlink($CFG->external_data_root . "/../../users/" . $USERID . ".html");
    }
    else
    {
        file_put_contents($CFG->external_data_root . "/../../users/" . $USERID . ".html", $text);
    }
}

/*
echo "\nUpdating data in magento...";

require_once __DIR__ . "/../../../local/monorailservices/mageapi.php";
$api = new MageApi($CFG->mage_api_url);

if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
{
    $api->begin();

    $products = $api->getProductList();

    foreach ($products as $product)
    {
        $course = $DB->get_records_sql("SELECT id, code, mainteacher FROM {monorail_course_data} WHERE code=?", array($product["sku"]));

        if (!empty($course))
        {
            $course = reset($course);

            $api->updateProduct($course->code,
                array("additional_attributes" => array("single_data" =>
                    array("teacher_id" => $course->mainteacher))));
        }
    }

    $api->commit();
    $api->disconnect();
}
else
{
    echo 'ERROR: unable to connect to magento catalog!';
}
*/
