<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";

global $DB;

/* 1. First admin in cohort becomes the owner (all other admins will be
 *    co-admins.).
 *
 * 2. All cohort members who are teachers in cohort courses become teachers,
 *    other members - get a tag of cohort association.
 * */

cli_heading("LOOKING FOR COHORT OWNERS...");

$cohorts = $DB->get_records_sql("SELECT id, name FROM {cohort}");

foreach ($cohorts as $coh) {
    echo "Cohort " . $coh->id . ": ";

    $admins = $DB->get_recordset_sql("SELECT id, userid, usertype FROM {monorail_cohort_admin_users} WHERE cohortid=?", array($coh->id));
    $admId = null;
    $nogo = false;
    $uids = array();

    foreach ($admins as $adm) {
        if ($adm->usertype == 1) {
            $nogo = true;
        }

        $uids[$adm->userid] = $adm->id;
    }

    if (!$nogo) {
        switch (count($uids)) {
            case 0:
                echo "no admins! ";
                break;

            case 1:
                $admId = reset($uids);
                break;

            default:
                $admId = $uids[$DB->get_field_sql("SELECT userid FROM {cohort_members} WHERE cohortid=? AND userid IN (" . implode(", ", array_keys($uids)) . ") ORDER BY timeadded ASC LIMIT 1", array($coh->id))];
                echo "\n\n" . "  " . $coh->id . " - " . $admId . "\n\n";
        }
    }

    if ($admId) {
        echo "assigned owner record: " . $admId;
        $DB->execute("UPDATE {monorail_cohort_admin_users} SET usertype=1 WHERE id=?", array($admId));

        if (count($uids) > 1) {
            $theRest = implode(", ", array_diff(array_values($uids), array($admId)));

            echo " and admin users: " . $theRest;
            $DB->execute("UPDATE {monorail_cohort_admin_users} SET usertype=2 WHERE id IN (" . $theRest . ")");
        }
    } else {
        echo "no changes with owner.";
    }

    $courseAdmins = $DB->get_recordset_sql("SELECT mcd.mainteacher AS uid " .
        "FROM {monorail_cohort_courseadmins} AS mca " .
            "INNER JOIN {monorail_course_data} AS mcd ON mca.courseid=mcd.courseid " .
                "WHERE mca.cohortid=?", array($coh->id));

    $cuids = array();

    foreach ($courseAdmins as $cadm) {
        $cuids[] = $cadm->uid;
    }

    $members = $DB->get_recordset_sql("SELECT id, userid FROM {cohort_members} WHERE cohortid=?", array($coh->id));
    $kickMembers = array();

    foreach ($members as $mem) {
        if (array_key_exists($mem->userid, $uids) || in_array($mem->userid, $cuids)) {
            // Cohort member is a cohort or course admin - leave alone.
            continue;
        }

        $kickMembers[] = $mem->userid;
    }

    if (!empty($kickMembers)) {
        foreach ($kickMembers as $kid) {
            $DB->execute("DELETE FROM {cohort_members} WHERE cohortid=? AND userid=?", array($coh->id, $kid));

            try {
                $DB->execute("INSERT INTO {monorail_cohort_user_link} (userid, cohortid) VALUES (?, ?)", array($kid, $coh->id));
            } catch (Exception $ok) { }
        }

        echo " Removing following non-teachers: " . implode(", ", $kickMembers);
    } else {
        echo " No changes with teachers (members).";
    }

    echo "\n";
}
