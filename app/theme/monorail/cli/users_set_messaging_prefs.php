<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


/**
 * Sets all users messaging preferences
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions


global $DB;

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false),
                                               array('h'=>'help'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    $help =
"Set all users messaging preferences to not receive any Moodle core email.

Options:
-h, --help            Print out this help

Example:
\$sudo -u www-data /usr/bin/php theme/monorail/cli/users_set_messaging_prefs.php
";

    echo $help;
    die;
}
cli_heading('Users set messaging prefs');

$prefs = array(
    'message_provider_mod_assign_assign_notification_loggedin' => 'none',
    'message_provider_mod_assign_assign_notification_loggedoff' => 'none',
    'message_provider_mod_assignment_assignment_updates_loggedin' => 'none',
    'message_provider_mod_assignment_assignment_updates_loggedoff' => 'none',
    'message_provider_moodle_courserequestapproved_loggedin' => 'none',
    'message_provider_moodle_courserequestapproved_loggedoff' => 'none',
    'message_provider_moodle_courserequestrejected_loggedin' => 'none',
    'message_provider_moodle_courserequestrejected_loggedoff' => 'none',
    'message_provider_mod_lesson_graded_essay_loggedin' => 'none',
    'message_provider_mod_lesson_graded_essay_loggedoff' => 'none',
    'message_provider_mod_forum_posts_loggedin' => 'none',
    'message_provider_mod_forum_posts_loggedoff' => 'none'
);

$users = $DB->get_records_sql('select * from {user}');

foreach ($users as $user) {
    echo "$user->id .. $user->firstname $user->lastname
";
    set_user_preferences($prefs, $user->id);
}

exit(0); // 0 means success
