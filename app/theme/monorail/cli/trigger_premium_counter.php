<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require_once __DIR__ . "/../../../config.php";
require_once($CFG->libdir.'/clilib.php');      // cli only functions

require_once __DIR__ . "/../premium_counter.php";

premium_counter_process();
