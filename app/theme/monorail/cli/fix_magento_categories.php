<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require(dirname(__FILE__) . '/../../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once dirname(__FILE__) . "/../lib.php";

global $DB;

cli_heading("FIXING COURSE CATEGORIES IN MAGNETO...");

/*
    WARNING: magic categories are hardcoded on top of moodle categories...

        1 => 42,    // Social science
        2 => 35,    // Arts'n design
        3 => 36,    // Business'n law
        4 => 37,    // ...
        5 => 37,    // Technology
        7 => 41,    // Education
        9 => 43,    // Life science
        10 => 44,   // Physical sciences
        11 => 45,   // Philosophy
        12 => 46,   // Humanities
        13 => 47,   // Languages
        15 => 48);  // Other

 * */

require_once __DIR__ . "/../../../local/monorailservices/mageapi.php";
$api = new MageApi($CFG->mage_api_url);

if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
{
    $products = $api->getProductList();

    $api->begin();
    $counter = 0;

    foreach ($products as $product)
    {
        $course = $DB->get_records_sql("SELECT c.id, c.fullname, c.category FROM {course} AS c " .
            "INNER JOIN {monorail_course_data} AS mcd ON c.id=mcd.courseid WHERE mcd.code=?", array($product["sku"]));

        if (!empty($course))
        {
            $course = reset($course);

            if (array_key_exists($course->category, $CFG->mage_catmap))
            {
                $newCats = array();

                $newCats[] = $CFG->mage_catmap[$course->category];

                foreach ($product["category_ids"] as $oldCat)
                {
                    if (!in_array($oldCat, $CFG->mage_catmap))
                    {
                        $newCats[] = $oldCat;
                    }
                }

                $dif1 = array_diff($product["category_ids"], $newCats);
                $dif2 = array_diff($newCats, $product["category_ids"]);

                $eq = (empty($dif1) && empty($dif2));

                if (!$eq)
                {
                    echo "Updating " . $course->fullname . "\n";

                    $api->updateProduct($product["sku"], array("categories" => $newCats));

                    $counter++;

                    if ($counter % 20 == 0)
                    {
                        $api->commit();
                        $api->begin();
                    }
                }
            }
        }
    }

    $api->commit();
    $api->disconnect();
}
else
{
    echo 'ERROR: unable to connect to magento catalog!';
}
