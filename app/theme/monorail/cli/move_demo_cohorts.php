<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require_once __DIR__ . "/../../../config.php";
require_once($CFG->libdir.'/clilib.php');      // cli only functions

cli_heading('Moving demo cohorts...');

$olderThan = strtotime("12 feb 2015 midnight");
$timeCreated = strtotime("midnight");
$demoEnds = strtotime("+30 days midnight");

$total = 0;

if ($olderThan && $timeCreated && $demoEnds) {
    $cohorts = $DB->get_records_sql("SELECT c.id AS id, c.name AS name, mci.id AS iid " .
        "FROM {cohort} AS c INNER JOIN {monorail_cohort_info} AS mci ON mci.cohortid=c.id " .
        "WHERE mci.company_status=1 AND c.timecreated<?;", array($olderThan));

    foreach ($cohorts as $cohort) {
        echo "Updating " . $cohort->name . "\n";

        $DB->execute("UPDATE {cohort} SET timecreated=? WHERE id=?", array($timeCreated, $cohort->id));
        $DB->execute("UPDATE {monorail_cohort_info} SET demo_period_end=? WHERE id=?", array($demoEnds, $cohort->iid));

        $total++;
    }
}

echo "\nTotal cohorts updated: " . $total . "\n";
