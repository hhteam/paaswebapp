<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


/**
 * Script to resend errored emails since timestamp
 */

define('CLI_SCRIPT', true);

require('/var/www/app/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions


global $DB;

// now get cli options
list($options, $unrecognized) = cli_get_params(array('since'=>null,'help'=>false),
                                               array('h'=>'help'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || (! $options['since'])) {
    $help =
"Resend errored emails since timestamp.

Options:
-h, --help            Print out this help
--since               Timestamp since to send error emails

Example:
\$sudo -u www-data /usr/bin/php theme/monorail/cli/<script>.php
";

    echo $help;
    die;
}
cli_heading('Resend errored emails since timestamp.');


$emails = $DB->get_records_select('monorail_sent_emails', "status = 'error' and timemodified > ?", array($options['since']));
echo count($emails);
foreach ($emails as $email) {
    
    echo "Processing: $email->id to $email->toemail -> ";
    // deploy to redis
    try {
        require_once("$CFG->dirroot/theme/monorail/vendor/autoload.php");
        $redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host'   => $CFG->emailer_redis_host,
            'port'   => $CFG->emailer_redis_port,
        ));
        $redis->rpush('emails', $email->valuesjson);
        // NOTE: afaik no close is needed for the connection as it comes from php connection pools
        // but worth mentioning if some problems arise and this could be the fault
        $status = 'sent';
        $statusinfo = null;
    } catch (Exception $ex) {
        $status = 'error';
        $statusinfo = 'Failed to deploy to redis';
        add_to_log(1, 'monorail', 'cli', 'resend_errored_emails', 'Exception when deploying email to redis: '.$ex->getMessage());
    }
    
    // update as sent
    if ($status == 'sent' && $email->type != 'course_messages') {
        $email->valuesjson = null;
    }
    $email->status = $status;
    $email->statusinfo = $statusinfo;
    $email->timemodified = time();
    $DB->update_record('monorail_sent_emails', $email);
    echo "status: $status
    ";
    
}


exit(0); // 0 means success
