<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


/**
 * Migrate all user avatars to new location
 */

define('CLI_SCRIPT', true);

require('/var/www/app/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions


global $DB;

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false),
                                               array('h'=>'help'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    $help =
"Migrate all user avatars to new location.
 Executed once when moving profile pics outside Moodle.

Options:
-h, --help            Print out this help

Example:
\$sudo -u www-data /usr/bin/php theme/monorail/cli/users_migrate_avatars.php
";

    echo $help;
    die;
}
cli_heading('Migrate all user avatars');

require_once(dirname(__FILE__) . '/../lib.php');

$users = $DB->get_records('user');

$fs = get_file_storage();
$count = 0;
$found = 0;
$errors = 0;
$nocontext = 0;

foreach ($users as $user) {
    echo "$user->id .. $user->firstname $user->lastname 
";
    $context = get_context_instance(CONTEXT_USER, $user->id);
    $count += 1;
    if ($context) {
        $files = $fs->get_area_files($context->id, 'user', 'icon', 0, "timemodified", false);
        foreach ($files as $file) {
            if ($file->get_filename() == 'f3.png') {
                $found += 1;
                $tempfile = $file->copy_content_to_temp();
                $result = monorail_process_avatar($user->id, $tempfile);
                if (! $result) {
                    $errors += 1;
                }
                @unlink($tempfile);
            }
        }
    } else {
        $nocontext += 1;
    }
}

echo "Count: ".$count."
Found: ".$found."
Errors: ".$errors."
No context: ".$nocontext."
";

exit(0); // 0 means success
