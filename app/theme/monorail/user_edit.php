<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

	require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
	require_once($CFG->libdir.'/gdlib.php');
	require_once('user_edit_form.php');
	require_once($CFG->dirroot.'/user/editlib.php');
	require_once($CFG->dirroot.'/user/profile/lib.php');

	$PAGE->https_required();

	$userid   = optional_param('id', $USER->id, PARAM_INT);    // user id
	$courseid = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)
	$cancelemailchange = optional_param('cancelemailchange', 0, PARAM_INT);   // course id (defaults to Site)

	$PAGE->set_url('/theme/monorail/user_edit.php', array('course'=>$courseid, 'id'=>$userid));

	if (!$course = $DB->get_record('course', array('id'=>$courseid))) {
		print_error('invalidcourseid');
	}

	if ($course->id != SITEID) {
		require_login($course);
	} else if (!isloggedin()) {
		if (empty($SESSION->wantsurl)) {
			$SESSION->wantsurl = $CFG->httpswwwroot.'/user/edit.php';
		}
		redirect(get_login_url());
	} else {
		$PAGE->set_context(get_system_context());
	}
	$PAGE->set_pagelayout('profile');

	// Guest can not edit
	if (isguestuser()) {
		print_error('guestnoeditprofile');
	}

	// The user profile we are editing
	if (!$user = $DB->get_record('user', array('id'=>$userid))) {
		print_error('invaliduserid');
	}

	// Guest can not be edited
	if (isguestuser($user)) {
		print_error('guestnoeditprofile');
	}

	// User interests separated by commas
	if (!empty($CFG->usetags)) {
		require_once($CFG->dirroot.'/tag/lib.php');
		$user->interests = tag_get_tags_array('user', $user->id);
	}

	// remote users cannot be edited
	if (is_mnet_remote_user($user)) {
		if (user_not_fully_set_up($user)) {
			$hostwwwroot = $DB->get_field('mnet_host', 'wwwroot', array('id'=>$user->mnethostid));
			print_error('usernotfullysetup', 'mnet', '', $hostwwwroot);
		}
		redirect($CFG->wwwroot . "/courses/{$course->id}/participants");
	}

	// load the appropriate auth plugin
	$userauth = get_auth_plugin($user->auth);

	if (!$userauth->can_edit_profile()) {
		print_error('noprofileedit', 'auth');
	}

	if ($editurl = $userauth->edit_profile_url()) {
		// this internal script not used
		redirect($editurl);
	}

	if ($course->id == SITEID) {
		$coursecontext = get_context_instance(CONTEXT_SYSTEM);   // SYSTEM context
	} else {
		$coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);   // Course context
	}
	$systemcontext   = get_context_instance(CONTEXT_SYSTEM);
	$personalcontext = get_context_instance(CONTEXT_USER, $user->id);

	// check access control
	if ($user->id == $USER->id) {
		//editing own profile - require_login() MUST NOT be used here, it would result in infinite loop!
		if (!has_capability('moodle/user:editownprofile', $systemcontext)) {
			print_error('cannotedityourprofile');
		}

	} else {
		// teachers, parents, etc.
		require_capability('moodle/user:editprofile', $personalcontext);
		// no editing of guest user account
		if (isguestuser($user->id)) {
			print_error('guestnoeditprofileother');
		}
		// no editing of primary admin!
		if (is_siteadmin($user) and !is_siteadmin($USER)) {  // Only admins may edit other admins
			print_error('useradmineditadmin');
		}
	}

	if ($user->deleted) {
		echo $OUTPUT->header();
		echo $OUTPUT->heading(get_string('userdeleted'));
		echo $OUTPUT->footer();
		die;
	}

	// Process email change cancellation
	if ($cancelemailchange) {
		cancel_email_update($user->id);
	}

	//load user preferences
	useredit_load_preferences($user);

	//Load custom profile fields data
	profile_load_data($user);


	// Prepare the editor and create form
	$editoroptions = array(
			'maxfiles'   => EDITOR_UNLIMITED_FILES,
			'maxbytes'   => $CFG->maxbytes,
			'trusttext'  => false,
			'forcehttps' => false,
			'context'    => $personalcontext
	);

	$user = file_prepare_standard_editor($user, 'description', $editoroptions, $personalcontext, 'user', 'profile', 0);
	// Prepare filemanager draft area.
	$draftitemid = 0;
	$filemanagercontext = $editoroptions['context'];
	$filemanageroptions = array('maxbytes'       => $CFG->maxbytes,
			'subdirs'        => 0,
			'maxfiles'       => 1,
			'accepted_types' => 'web_image');
	file_prepare_draft_area($draftitemid, $filemanagercontext->id, 'user', 'newicon', 0, $filemanageroptions);
	$user->imagefile = $draftitemid;
	//create form
	$userform = new theme_monorail_user_edit_form(null, array(
			'editoroptions' => $editoroptions,
			'filemanageroptions' => $filemanageroptions,
			'userid' => $user->id));
	if (empty($user->country)) {
		// MDL-16308 - we must unset the value here so $CFG->country can be used as default one
		unset($user->country);
	}
	$userform->set_data($user);

	$email_changed = false;

	if ($usernew = $userform->get_data()) {

		add_to_log($course->id, 'user', 'update', "view.php?id=$user->id&course=$course->id", '');

		$email_changed_html = '';

		if ($CFG->emailchangeconfirmation) {
			// Users with 'moodle/user:update' can change their email address immediately
			// Other users require a confirmation email
			if (isset($usernew->email) and $user->email != $usernew->email && !has_capability('moodle/user:update', $systemcontext)) {
				$a = new stdClass();
				$a->newemail = $usernew->preference_newemail = $usernew->email;
				$usernew->preference_newemailkey = random_string(20);
				$usernew->preference_newemailattemptsleft = 3;
				$a->oldemail = $usernew->email = $user->email;

				$email_changed_html  = $OUTPUT->box_start('generalbox', 'notice');
				$email_changed_html .= get_string('auth_changingemailaddress', 'auth', $a);
				$email_changed_html .= $OUTPUT->continue_button("$CFG->wwwroot/theme/monorail/user.php?id=$user->id&amp;course=$course->id");
				$email_changed_html .= $OUTPUT->box('', 'clear');
				$email_changed_html .= $OUTPUT->box_end();
				$email_changed = true;
			}
		}

		$authplugin = get_auth_plugin($user->auth);

		$usernew->timemodified = time();

		// description editor element may not exist!
		if (isset($usernew->description_editor)) {
			$usernew = file_postupdate_standard_editor($usernew, 'description', $editoroptions, $personalcontext, 'user', 'profile', 0);
		}

		$DB->update_record('user', $usernew);

		// pass a true $userold here
		if (! $authplugin->user_update($user, $usernew)) {
			// auth update failed, rollback for moodle
			$DB->update_record('user', $user);
			print_error('cannotupdateprofile');
		}

		//update preferences
		useredit_update_user_preference($usernew);

		//update interests
		if (!empty($CFG->usetags)) {
			useredit_update_interests($usernew, $usernew->interests);
		}

		// update mail bounces
		useredit_update_bounces($user, $usernew);

		/// update forum track preference
		useredit_update_trackforums($user, $usernew);

		// save custom profile fields data
		profile_save_data($usernew);

		// If email was changed and confirmation is required, send confirmation email now
		if ($email_changed && $CFG->emailchangeconfirmation) {
			$temp_user = fullclone($user);
			$temp_user->email = $usernew->preference_newemail;

			$a = new stdClass();
			$a->url = $CFG->wwwroot . '/theme/monorail/emailupdate.php?key=' . $usernew->preference_newemailkey . '&id=' . $user->id;
			$a->site = format_string($SITE->fullname, true, array('context' => get_context_instance(CONTEXT_COURSE, SITEID)));
			$a->fullname = fullname($user, true);

			$emailupdatemessage = get_string('emailupdatemessage', 'auth', $a);
			$emailupdatetitle = get_string('emailupdatetitle', 'auth', $a);

			//email confirmation directly rather than using messaging so they will definitely get an email
			$supportuser = generate_email_supportuser();
			if (!$mail_results = email_to_user($temp_user, $supportuser, $emailupdatetitle, $emailupdatemessage)) {
				die("could not send email!");
			}
		}

		// reload from db
		$usernew = $DB->get_record('user', array('id'=>$user->id));
		events_trigger('user_updated', $usernew);

		if ($USER->id == $user->id) {
			// Override old $USER session variable if needed
			foreach ((array)$usernew as $variable => $value) {
				$USER->$variable = $value;
			}
			// preload custom fields
			profile_load_custom_fields($USER);
		}

		if (is_siteadmin() and empty($SITE->shortname)) {
			// fresh cli install - we need to finish site settings
			redirect(new moodle_url('/admin/index.php'));
		}

		if (!$email_changed || !$CFG->emailchangeconfirmation) {
			redirect("$CFG->wwwroot/theme/monorail/user.php?id=$user->id");
		}
	}

	// make sure we really are on the https page when https login required
	$PAGE->verify_https_required();


	/// Display page header
	$streditmyprofile = get_string('editmyprofile');
	$strparticipants  = get_string('join_participants');
	$userfullname     = fullname($user, true);

	$PAGE->set_title("$course->shortname: $streditmyprofile");
	$PAGE->set_heading(fullname($user, true));
	$PAGE->add_body_class('user-edit');

	echo $OUTPUT->header();
	echo $OUTPUT->box_start('user-edit-form');
	if ($email_changed) {
		echo $email_changed_html;
	} else {
		/// Finally display THE form
		$userform->display();
	}
	echo $OUTPUT->box_end();

	$PAGE->requires->js_init_call('M.theme_monorail.initpicturebox', array(get_config('auth/googleoauth2', 'facebookclientid')));
	
	$PAGE->requires->string_for_js('upload_file', 'theme_monorail');
	$PAGE->requires->string_for_js('use_facebook_picture', 'theme_monorail');

	/// and proper footer
	echo $OUTPUT->footer();
