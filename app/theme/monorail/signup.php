<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('signup_form.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gdlib.php');

if (!is_enabled_auth('email')) {
    redirect(get_login_url());
}

if (empty($CFG->registerauth)) {
    print_error('notlocalisederrormessage', 'error', '', 'Sorry, you may not use this page.');
}


$authplugin = get_auth_plugin($CFG->registerauth);

if (!$authplugin->can_signup()) {
    print_error('notlocalisederrormessage', 'error', '', 'Sorry, you may not use this page.');
}

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$PAGE->set_url('/login/signup.php');
$PAGE->set_context(get_context_instance(CONTEXT_SYSTEM));
$PAGE->set_title(get_string('signuptomagicmoodle', 'theme_monorail'));
$PAGE->set_heading(get_string('signuptomagicmoodle', 'theme_monorail'));
$PAGE->set_pagelayout('signup');

$param = array();
$mform_signup = new login_signup_form(null, null, 'post', '', $param);

$param = $_POST;
$ebuser = false;
if(array_key_exists ('location', $param)) {
    $country = get_string_manager()->get_list_of_countries();
    $location = explode(",", $param['location']);
    if(count($location) == 2) {
        $param['city'] = $location[0];
        $param['country'] = array_search(trim($location[1]), $country);;
    } else {
        $param['country'] = array_search(trim($param['location']), $country);
    }
}
if(array_key_exists ('e4business', $param)) {
    $ebuser = true;
}
if ($mform_signup->is_cancelled()) {
    redirect(get_login_url());

} else if ($user = $mform_signup->get_data()) {
    $user->confirmed   = 0;
    $user->lang        = current_language();
    $user->firstaccess = time();
    $user->timecreated = time();
    $user->mnethostid  = $CFG->mnet_localhost_id;
    $user->secret      = random_string(15);
    $user->auth        = $CFG->registerauth;

    //Workaround
    $origusername = $user->username;
    $user->username = strtolower($user->email);
    $user->password = hash_internal_user_password($user->password);

    // check if User exists
    $checkuserexists = $DB->get_record('user', array('email' => $user->email, 'username'=>$user->username));
    if(!empty($checkuserexists)) {
        redirect(get_login_url());
    } else {
        //Create User Here -
        $user->maildisplay = 0;
        $user->id = $DB->insert_record('user', $user);

        // Store referral data to monorail data.
        if (isset($_COOKIE["referral"]) && !empty($_COOKIE["referral"]))
        {
            $refData = new stdClass();

            $refData->type = "user";
            $refData->itemid = $user->id;
            $refData->datakey = "referral";
            $refData->value = $_COOKIE["referral"];

            $DB->insert_record("monorail_data", $refData);

            setcookie("referral", "", time() - 3600);
        }

        /// Save any custom profile field information
        profile_save_data($user);
        $cuser = $DB->get_record('user', array('id'=>$user->id));
        events_trigger('user_created', $cuser);

        //Image icon
        if($user->provider == 'facebook') {
            $context = get_context_instance(CONTEXT_USER, $cuser->id, MUST_EXIST);
            $fs = get_file_storage();
            // Prepare file record object
            $fileinfo = array(
                    'contextid' => $context->id,
                    'component' => 'user',
                    'filearea' => 'draft',
                    'itemid' => 0,
                    'filepath' => '/',
                    );
            $picurl = "https://graph.facebook.com/". $origusername . "/picture?type=large";
            $file = $fs->create_file_from_url($fileinfo, $picurl);
            $iconfile = $file->copy_content_to_temp();
            $newpicture = (int)process_new_icon($context, 'user', 'icon', 0, $iconfile);
            @unlink($iconfile);
            $DB->set_field('user', 'picture', $newpicture, array('id' => $cuser->id));
        }

        // Disable all email messaging from Moodle
        $prefs = array(
            'message_provider_mod_assign_assign_notification_loggedin' => 'none',
            'message_provider_mod_assign_assign_notification_loggedoff' => 'none',
            'message_provider_mod_assignment_assignment_updates_loggedin' => 'none',
            'message_provider_mod_assignment_assignment_updates_loggedoff' => 'none',
            'message_provider_moodle_courserequestapproved_loggedin' => 'none',
            'message_provider_moodle_courserequestapproved_loggedoff' => 'none',
            'message_provider_moodle_courserequestrejected_loggedin' => 'none',
            'message_provider_moodle_courserequestrejected_loggedoff' => 'none',
            'message_provider_mod_lesson_graded_essay_loggedin' => 'none',
            'message_provider_mod_lesson_graded_essay_loggedoff' => 'none',
            'message_provider_mod_forum_posts_loggedin' => 'none',
            'message_provider_mod_forum_posts_loggedoff' => 'none'
        );
        set_user_preferences($prefs, $user->id);

        $url = new moodle_url($CFG->wwwroot.'/login/confirm.php?data='. $user->secret .'/'. $user->username);

        preg_match("/(.*)@(.*)/", $user->email, $matches);
        $domain = $matches[2];
        if (!$ebuser /*|| ($ebuser && $DB->count_records('monorail_cohort_info', array('restrictdomain'=>$domain)) > 0) (!! no idea why this was here.)*/) {
            require_once("$CFG->dirroot/theme/monorail/engagements.php");
            engagements_trigger('welcome-email', $user->id);
        }

        if ($user->wantsurl)
            $SESSION->wantsurl = $user->wantsurl;
        else
            $SESSION->wantsurl = $CFG->magic_ui_root;

        redirect($url);
    }
    redirect(get_login_url());
    exit; //never reached
}

// make sure we really are on the https page when https login required
$PAGE->verify_https_required();


$newaccount = get_string('newaccount');
$login      = get_string('login');

$PAGE->requires->string_for_js('university', 'theme_monorail');
$PAGE->requires->string_for_js('username_description', 'theme_monorail');
$PAGE->requires->string_for_js('email', 'moodle');
$PAGE->requires->string_for_js('password', 'moodle');
$PAGE->requires->string_for_js('firstname', 'moodle');
$PAGE->requires->string_for_js('lastname', 'moodle');
$PAGE->requires->string_for_js('username', 'moodle');
$PAGE->requires->string_for_js('optional', 'moodle');
$PAGE->requires->string_for_js('again', 'moodle');

/*$PAGE->navbar->add($login);
$PAGE->navbar->add($newaccount);

$PAGE->set_title($newaccount);
$PAGE->set_heading($SITE->fullname);
*/
echo $OUTPUT->header();
$mform_signup->set_data($param);
$mform_signup->display();
echo $OUTPUT->footer();

