<?php 
$string["certificate_title"] = '완료 수료증';
$string["changepassworduser"] = '사용자이름의 비밀번호를 바꾸기';
$string["course_category_arts_design"] = '아트와 디자인';
$string["course_category_business_law"] = '경영법';
$string["course_category_education"] = '교육';
$string["course_category_humanities"] = '인류학';
$string["course_category_languages"] = 'Languages(지원하는 언어)';
$string["course_category_life_science"] = '라이프 과학';
$string["course_category_other"] = '다른';
$string["course_category_philosophy"] = '철학';
$string["course_category_physical_sciences"] = '체육학';
$string["course_category_select"] = '카테고리 선택';
$string["course_category_social_science"] = '사회과학';
$string["course_category_technology"] = '기술';
$string["email_action_join"] = '초대 수락';
$string["email_engagement_subject_welcome"] = 'Eliademy에 어서 오세요';
$string["email_header_dear_username"] = '{%username%} 귀하,';
$string["email_header_welcome"] = '환영합니다';
$string["email_notif_student_cert_p2"] = '축하합니다! 방금 강좌를 완료하여 수료증을 받았습니다.';
$string["email_notif_student_cert_p4"] = '계속 사용하세요. Eliademy의 강좌 카탈로그를 체크해서 다음 도전할 것을 정하세요.';
$string["email_notif_student_cert_share"] = '{%coursename%} 강좌를 완료해서 수료증을 받았어요@Eliademy';
$string["email_notif_student_cert_subj"] = '축하합니다! 당신은 방금 {%coursename%}를 완료해 수료증을 받으셨습니다.';
$string["errorfilemaxsize"] = '서버 오류입니다. post_max_size를 늘리고 {%a%}_max_filesize 를 업로드하세요';
$string["errorfileservererror"] = '서버 오류입니다. 디렉토리가 실행또는 사용 불가능 입니다.';
$string["errorfileservererror1"] = '서버 오류가 발생했습니다. Multipart를 디폴트로 설정하십시오(true).';
$string["errorfileservererror2"] = '서버 오류입니다. 디렉토리가 실행또는 사용 불가능 입니다.';
$string["errorsaveuploadfile"] = '업로드한 파일을 저장할 수 없습니다';
$string["errorsaveuploadfilemsg"] = '업로드가 취소되었거나, 서버 오류가 발생하엿습니다.';
$string["erroruploadfailed"] = '업로드 실패';
$string["event"] = '새 이벤트를 만들었습니다';
$string["event_del"] = '이벤트를 삭제했습니다';
$string["event_new"] = '새 이벤트를 만들었습니다';
$string["event_upd"] = '이벤트를 업데이트 했습니다';
$string["file_empty"] = '파일이 비어있습니다.';
$string["fileextensioninvalid"] = '파일의 형식이 다릅니다. 이것은 {%a%}여야 합니다.';
$string["filename_empty"] = '파일이름이 비어있습니다.';
$string["follow_us"] = '우리를 팔로우하세요';
$string["for_completing"] = 'Eliademy에 온라인 코스를 완료하기 위해서';
$string["forum_general"] = '일반 포럼';
$string["grade"] = '과제를 평가했습니다.';
$string["header_about"] = '우리의 관하서';
$string["header_analytics"] = '분석';
$string["header_blog"] = '블로그';
$string["header_calendar"] = '달력';
$string["header_catalog"] = '카탈로그';
$string["header_courses"] = '강좌';
$string["header_login"] = '로그인';
$string["header_myprofile"] = '내 프로필';
$string["header_settings"] = '설정';
$string["is_awarded"] = '는 제공됩니다';
$string["label_teacher"] = '강사';
$string["login_terms2"] = '사용 조건';
$string["material"] = '강좌 내용을 업데이트했습니다.';
$string["notification"] = '알림';
$string["opensource"] = '오픈 소스';
$string["overview_topic"] = '개요';
$string["pluginname"] = '모노레일';
$string["post"] = '토론장에서 새 답변을 달았습니다';
$string["privacy"] = '개인 정보 보호 정책 ';
$string["section"] = '업데이트된 토픽';
$string["service_name"] = 'Eliademy';
$string["task_del"] = '과제를 삭제했습니다.';
$string["task_new"] = '새로운 과제를 만들었습니다';
$string["task_upd"] = '과제를 업데이트했습니다';
$string["user_completed_course"] = '가 강좌를 방금 완료했습니다.';
$string["user_enrolled"] = '등록(_E)';
$string["user_submitted_assign"] = '과제를 제출했습니다';
$string["user_submitted_quiz"] = '퀴즈를 제출했습니다';
