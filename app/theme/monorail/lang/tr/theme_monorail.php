<?php 
$string["bbb_del"] = 'webinar sildi';
$string["bbb_new"] = 'webinar oluşturdu';
$string["bbb_upd"] = 'webinar güncelledi';
$string["button_help_translate"] = 'Dil ekle';
$string["certificate"] = 'size ders tamamlama sertifikası gönderdi!';
$string["certificate_title"] = 'Tamamlama Sertifikası';
$string["changepassworduser"] = 'Kullanıcı şifresini değiştir';
$string["course_category_arts_design"] = 'Sanatsal Çalışmalar ve Tasarım';
$string["course_category_business_law"] = 'İşletme ve Hukuk';
$string["course_category_education"] = 'Eğitim';
$string["course_category_humanities"] = 'Beşeri Bilimler';
$string["course_category_languages"] = 'Dil';
$string["course_category_life_science"] = 'Canlı Bilimi';
$string["course_category_other"] = 'Diğer';
$string["course_category_philosophy"] = 'Felsefe';
$string["course_category_physical_sciences"] = 'Doğa bilimi';
$string["course_category_select"] = '- Kategori seçiniz -';
$string["course_category_social_science"] = 'Sosyal bilimler';
$string["course_category_technology"] = 'Teknoloji';
$string["currency_euro"] = '€';
$string["email_action_enroll"] = 'Öğrenmeye başla';
$string["email_action_go_to_catalogue"] = 'Yeni Birşeyler Öğrenin';
$string["email_action_go_to_eliademy"] = 'Eliademy\'e Git';
$string["email_action_join"] = 'Daveti kabul et';
$string["email_button_first_course"] = 'İlk Dersinizi Oluşturun';
$string["email_button_update_payment_details"] = 'Ödeme Bilgilerini Güncelle';
$string["email_button_view_on_eliademy"] = 'Bunu Eliademy\'de görüntüle';
$string["email_course_invite_explanation"] = 'Bu davetiye sadece bir defa kullanılabilir';
$string["email_course_message_subject"] = '{%coursename%} adlı dersten mesaj';
$string["email_engagement_subject_premium-engagement-1"] = 'İlk çevirimiçi dersinizde size yardım için buradayız';
$string["email_engagement_subject_premium-engagement-1-create-course"] = 'İlk çevirimiçi dersinizde size yardım etmek için buradayız';
$string["email_engagement_subject_premium-engagement-2"] = 'İlk dersinizde size yardım için buradayız';
$string["email_engagement_subject_premium-engagement-2-invite-users"] = 'İlk çevirimiçi dersinizde size yardımcı olmak için buradayız';
$string["email_engagement_subject_premium-engagement-3"] = 'Hatırlatıcı: Deneme süreniz 7 gün içerisinde sona erecektir';
$string["email_engagement_subject_premium-engagement-3-seven-days-left"] = 'Hatırlatıcı: Deneme süreniz 7 gün içerisinde sona erecektir';
$string["email_engagement_subject_premium-engagement-4"] = 'Hatırlatıcı: Deneme süreniz yarın sona eriyor';
$string["email_engagement_subject_premium-engagement-4-one-day-left"] = 'Hatırlatma: Deneme sürümünüzün süresi yarın doluyor';
$string["email_engagement_subject_premium-engagement-5"] = 'Eliademy yönetiminden mesaj';
$string["email_engagement_subject_premium-engagement-5-ask-for-feedback"] = 'Eliademy yönetiminden mesaj';
$string["email_engagement_subject_welcome"] = 'Eliademy\'e Hoşgeldiniz';
$string["email_engagement_subject_welcome-cohort"] = 'Eliademy Premium\'a Hoşgeldiniz';
$string["email_footer_general"] = 'Eliademy eğitimcilerin ve öğrencilerin çevirimiçi ders oluşturabilecekleri, paylaşabilecekleri ve yönetebilecekleri bir e-öğrenme platformudur.';
$string["email_header_cohort_enroll"] = 'sizi derse ekledi';
$string["email_header_cohort_invite1"] = 'sizi yönetici olarak kuruluşuna katılmanız için davet etti';
$string["email_header_cohort_invite2"] = 'sizi eğitimci olarak kuruluşuna katılmanız için davet etti - ';
$string["email_header_cohort_invite3"] = 'sizi kuruluşuna katılmanız için davet etti';
$string["email_header_cohort_invite_msg"] = 'Bu davet sadece bu e-posta adresini kullanarak kabul edilebilir';
$string["email_header_course_invite"] = 'sizi bir derse davet etti';
$string["email_header_course_message"] = 'size şu derste bir mesaj gönderdi:';
$string["email_header_dear_username"] = 'Sayın {%username%},';
$string["email_header_hello"] = 'Merhaba';
$string["email_header_welcome"] = 'Hoşgeldiniz';
$string["email_label_download"] = 'İndir';
$string["email_label_or"] = 'veya';
$string["email_label_share_on"] = 'şurada paylaş';
$string["email_message_sent_by"] = 'Mesajı Gönderen ';
$string["email_notif_student_cert_p2"] = 'Tebrikler! Şu derste tamamlama sertifikası kazandınız : ';
$string["email_notif_student_cert_p4"] = 'Öğrenmeye devam edin, Eliademy kataloğunda ilginizi çekebilecek derslere göz atın!';
$string["email_notif_student_cert_share"] = 'Eliademy üzerinde {%coursename%} dersini tamamlayarak sertifika almaya hak kazandım';
$string["email_notif_student_cert_subj"] = 'Tebrikler. {%coursename%} dersini tamamlayarak sertifika almaya hak kazandınız.';
$string["email_notif_student_cert_view"] = 'Sertifikamı Görüntüle';
$string["email_order_notification_check_knowledgebase"] = 'Aynı zamanda, bilgi veritabanımıza göz atarak yaratıcı fikiler ve uygulamaları inceleyebilir, dersinizi bir best-seller haline getirebilirsiniz.';
$string["email_order_notification_congrats"] = 'Güzel Haber! Çevirimiçi dersiniz için bir öğrenci sipariş verdi.';
$string["email_order_notification_course_name"] = 'Ders adı';
$string["email_order_notification_course_price"] = 'Tutar';
$string["email_order_notification_discount"] = 'Eğer ücret 0 ise, öğrenci ya ücretsiz kupon kodu ile katılmıştır';
$string["email_order_notification_order_details"] = 'Altta sipariş detaylarınızı görebilirsiniz.';
$string["email_order_notification_questions"] = 'Bu sipariş hakkında sorularınız varsa lütfen bize support@eliademy.com adresinden ulaşın.';
$string["email_order_notification_student_name"] = 'Satın alan ismi';
$string["email_order_notification_subject"] = '{%name%} için sipariş bilgilendirmesi';
$string["email_order_notification_tax"] = 'Vergi oranı';
$string["email_order_notification_thanks"] = 'Eliademy üzerinde satın alma işleminiz için teşekkürler.';
$string["email_premium_welcome_l2_li1"] = 'Bilgi Veritabanında kullanışlı bilgiler bulabilir ve yol haritamızı görebilirsiniz';
$string["email_premium_welcome_l2_li2"] = 'Webcast ve video eğitimleri için YouTube kanalı';
$string["email_premium_welcome_l2_li3"] = 'Kuruluşunuzda webinar ile eğitim verebilmeniz için ücretsiz eğitim';
$string["email_premium_welcome_l2_li4"] = 'Önemli özelliklerin listesi ve kullanım bilgileri';
$string["email_premium_welcome_li4"] = 'Limitsiz veri trafiği ve dosya saklayabilme;';
$string["email_premium_welcome_p1"] = '30 günlük Eliademy Premium denemesini başlattığınız için teşekkürler! Eliademy Premium ekibinize, müşterilerinize veya öğrencilerinize eğitim vermeniz için gelişmiş bir çözümdür. Tüm ücretsiz özelliklerin yanı sıra gelişmiş premium özellikleri içerir:';
$string["email_premium_welcome_p2"] = 'Şu an size önerimiz Eliademy\'i kendi verilerinizle denemenizdir.';
$string["email_premium_welcome_p4"] = 'Ek olarak, sistemi kullanmanızda size yardımcı olacak ek materyallerin listesini hazırladık:';
$string["email_premium_welcome_p5"] = '30 günlük deneme süreniz boyunca size birkaç tane daha ipuçlarını içeren e-posta göndereceğiz ve istediğiniz zaman e-posta listemizden çıkabilirsiniz. Destek veya ekibimizden biriyle iletişim kurmak isterseniz support@eliademy.com adresinden bize ulaşabilirsiniz.';
$string["email_premium_welcome_p6"] = 'İnsanlara Eliademy\'i öğretmeyi seviyoruz, dolayısıyla bize istediğiniz zaman soru sorabilirsiniz.';
$string["email_regards"] = 'Helsinki\'den saygı ve sevgilerimizle';
$string["email_subject"] = '{%coursename%} için Eliademy güncellemesi';
$string["email_subject_daily"] = 'Dersleriniz için Eliademy günlük özeti';
$string["email_subject_eliademy_invite"] = '{%username%} size Eliademy\'i denemeniz için tavsiyede bulundu';
$string["email_subject_enroll"] = '{%cohortadminName%} sizi {%courseName%} adlı derse eklemiştir.';
$string["email_subject_invite"] = '{%teacherName%} sizi {%courseName%} adlı derse davet etti';
$string["email_subject_invite_cohort"] = '{%cohortadminName%} sizi {%courseName%} adlı gizli derse eklemiştir.';
$string["email_subject_invoice"] = 'Satın Aldığınız İçin Teşekkürler';
$string["email_subject_invoice_error"] = 'Ödeme işlemi başarısız oldu';
$string["email_team"] = 'Eliademy Ekibi';
$string["email_via_eliademy"] = '- Eliademy';
$string["email_visit_support"] = 'Eliademy Bilgi Tabanı\'na Göz Atın';
$string["email_welcome_2_li1"] = 'Kullanımı kolay ders içeriği editörü;';
$string["email_welcome_2_li2"] = 'Interaktif tartışma forumları;';
$string["email_welcome_2_li3"] = 'Ödev yönetimi ve not verme araçları;';
$string["email_welcome_2_li4"] = 'Yüksek kalite hologramlı baskılı ders tamamlama sertifikası;';
$string["email_welcome_2_li5"] = 'Ücretsiz ve ücretli dersler için katalog;';
$string["email_welcome_2_li6"] = 'Canlı video ile eğitim veya webinar.';
$string["email_welcome_2_p1"] = '160 ülkede 100,000 üzerinde eğitimci ve öğrenciden oluşan bir topluluğa katıldınız. Eliademy üzerinde günde 100\'den fazla ders oluşturuluyor.';
$string["email_welcome_2_p2"] = '<b>Kendi çevirimiçi dersinizi oluşturmak ister misiniz?</b> Bunun için ihtiyaç duyacağınız herşeyi size sağlıyoruz:';
$string["email_welcome_2_p3"] = 'Şu an Eliademy\'i kendi verilerinizle deneyebilirsiniz. Ders oluşturmak ve öğrencilerinizi davet etmek son derece kolaydır.';
$string["email_welcome_2_p5"] = '<b>Ders almak mı istiyorsunuz?</b> Herkese açık ders kataloğumuza göz atın ve ilk dersinizi bugün almaya başlayın - alabileceğiniz ders sayısında hiçbir kısıtlama bulunmamaktadır!';
$string["email_welcome_2_p6"] = '<b>Sorunuz mu var?</b> support@eliademy.com adresine her zaman sorularınızı gönderebilirsiniz. Ek olarak, Eliademy\'de sayfaların sağ alt köşesindeki <b>kalın soru işareti (?)</b>\'ne tıklayarak sorunuzu yazabilirsiniz.';
$string["email_welcome_2_p7"] = 'Eliademy\'nin görevi "eğitimi teknoloji yoluyla herkese erişilebilir hale getirmektir." Eğitimin her bireyin en temel hakkı olduğuna inanıyoruz ve bu platformu tüm eğitimciler, STK\'lar ve kamu tarafından finanse edilen eğitim kurumları için ücretsiz olarak sunmaya kendimizi adadık. Lütfen bize bu yolda yardımcı olun ve ihtiyacı olanlara ücretsiz eğitim sağlamaya devam edebilmemiz için<a href="https://eliademy.com/support">bağışta bulunun.</a>';
$string["email_welcome_2_p8"] = 'Teşekkürler ve iyi öğrenmeler.';
$string["errorfilemaxsize"] = 'Sunucu hatası. post_max_size ve upload_max_filesize değerlerini şu şekilde ayarlayın: {%a%}.';
$string["errorfileservererror"] = 'Sunucu Hatası. Uploads klasörünün izinleriyle ilgili bir sorun oluştu.';
$string["errorfileservererror1"] = 'Sunucu hatası. Multipart işlemi sorunlu. Lütfen forceMultipart değerini varsayılan (true) olarak ayarlayınız.';
$string["errorfileservererror2"] = 'Sunucu hatası. Geçici klasörler yazmaya veya erişime kapalı.';
$string["errorsaveuploadfile"] = 'Dosya kaydedilemedi.';
$string["errorsaveuploadfilemsg"] = 'Dosya yükleme iptal edildi, veya sunucu hatası oluştu';
$string["erroruploadfailed"] = 'Yükleme başarısız oldu';
$string["event"] = 'akademik takvim etkinliği oluşturdu';
$string["event_del"] = 'bir etkinlik sildi';
$string["event_new"] = 'akademik takvim etkinliği oluşturdu';
$string["event_upd"] = 'bir etkinlik güncelledi';
$string["file_empty"] = 'Dosya boş';
$string["fileextensioninvalid"] = 'Dosya uzantısı geçersizdir, şu uzantılardan birisini kullanınız: {%a%}.';
$string["filename_empty"] = 'Dosya adı boş';
$string["follow_us"] = 'Bizi takip edin ';
$string["footer_slogan"] = 'Teknoloji yardımıyla erişilebilir eğitim';
$string["for_completing"] = '- Eliademy üzerinde çevirimiçi dersi tamamladığından dolayı';
$string["forum_general"] = 'Genel forum';
$string["forum_general_intro"] = 'Ders hakkında genel yorumlar. Eğer mesajınızla ilgili bir forum yoksa burayı kullanınız.';
$string["grade"] = 'bir ödeve not verdi';
$string["header_about"] = 'Hakkımızda';
$string["header_admin"] = 'Yönetici';
$string["header_analytics"] = 'Detaylı Analizler';
$string["header_blog"] = 'Blog';
$string["header_business"] = 'Premium';
$string["header_calendar"] = 'Akademik Takvim';
$string["header_catalog"] = 'Katalog';
$string["header_courses"] = 'Dersler';
$string["header_features"] = 'Özellikler';
$string["header_helpdesk"] = 'Destek';
$string["header_login"] = 'Giriş';
$string["header_logout"] = 'Çıkış';
$string["header_lti"] = 'LTI';
$string["header_my_courses"] = 'Derslerim';
$string["header_myprofile"] = 'Profilim';
$string["header_settings"] = 'Ayarlar';
$string["header_signup"] = 'Kayıt ol';
$string["is_awarded"] = '- tamamlama sertifikası almaya hak kazanmıştır -';
$string["label_link"] = 'bağlantı';
$string["label_teacher"] = 'Eğitimci';
$string["login_terms2"] = 'Kullanım Şartları';
$string["material"] = 'yeni ders materyali yüklenmiştir';
$string["notification"] = 'Bildirim';
$string["opensource"] = 'Açık Kaynak Kodu';
$string["overview_topic"] = 'Genel Görünüm';
$string["pluginname"] = 'MonoRail';
$string["post"] = 'forum mesajı ekledi';
$string["privacy"] = 'Gizlilik Politikası';
$string["section"] = 'şu başlığı güncelledi -';
$string["sent_via_service"] = '- Eliademy';
$string["service_name"] = 'Eliademy';
$string["settings_url_text"] = 'Uyarı ayarlarını değiştir veya mail listesinden çık';
$string["share_course_cert_smsg"] = 'Eliademy üzerinde  %s dersini tamamlayarak sertifika almaya hak kazandım.';
$string["share_course_complete_msg"] = 'Eliademy kullanarak %s dersini tamamladım. İlginizi çekebilir.';
$string["share_course_complete_smsg"] = 'Eliademy üzerinde %s dersini tamamladım.';
$string["share_course_msg"] = 'Eliademy üzerinde %s adlı dersi alıyorum. İlgini çekiyorsa sende bağlantıya tıklayarak göz atabilirsin.';
$string["share_course_review_msg"] = 'Eliademy üzerinde %3 dersine %2 üzerinden %1 puan verdim';
$string["share_course_smsg"] = 'Eliademy üzerinde %s dersini alıyorum. İlginizi çekebilir.';
$string["task_del"] = 'bir ödev sildi';
$string["task_new"] = 'yeni ödev oluşturdu';
$string["task_upd"] = 'bir ödev güncelledi';
$string["user_completed_course"] = 'dersinizi tamamladı';
$string["user_enrolled"] = 'derse katıldı';
$string["user_submitted_assign"] = 'bir ödev teslim etti';
$string["user_submitted_quiz"] = 'bir quiz tamamladı';
