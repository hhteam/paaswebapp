<?php 
$string["bbb_del"] = 'slettet nettseminar';
$string["course_category_other"] = 'Andre';
$string["course_category_select"] = 'Velg en kategori';
$string["course_category_technology"] = 'Teknologi';
$string["email_action_enroll"] = 'Begynn læringen';
$string["email_action_join"] = 'Godta invitasjon';
$string["email_course_invite_explanation"] = 'Denne invitasjonen kan kun brukes en gang';
$string["email_engagement_subject_premium-engagement-2"] = 'Vi er her for å hjelpe deg med ditt første onlinekurs';
$string["email_footer_general"] = 'Eliademy er en e-læringsplattform som lar lærere og studenter lage, dele og behandle onlinekurs';
$string["email_label_download"] = 'Last ned';
$string["email_notif_student_cert_subj"] = 'Gratulerer! Du har mottatt sertifikat for fullføring av kurset';
$string["email_premium_welcome_l2_li2"] = 'YouTube kanal, for vår webcast og videoopplæring';
$string["email_premium_welcome_l2_li3"] = 'Gratis kurs for å lære å bruke nettseminar trening i din organisasjon';
$string["email_premium_welcome_l2_li4"] = 'Liste av de viktigste funksjonene og instruksjoner for å bruke dem';
$string["email_subject_invoice_error"] = 'Betaling mislyktes';
$string["email_welcome_2_li1"] = 'Enkel kursinnholdseditor';
$string["email_welcome_2_li2"] = 'Interaktive diskusjonforum';
$string["email_welcome_2_li3"] = 'Oppgavebehandling og graderingsverktøy';
$string["email_welcome_2_li5"] = 'Markedsplass for gratis og betalingskurs';
$string["errorfileservererror2"] = 'Serverfeil. Deler av mappen er ikke skrivbar eller kjørbar.';
$string["errorsaveuploadfile"] = 'Kunne ikke lagre opplastet fil';
$string["file_empty"] = 'Filen er tom.';
$string["header_admin"] = 'Administrator';
$string["header_lti"] = 'LTI';
$string["label_link"] = 'lenke';
$string["opensource"] = 'Åpen kildekode';
$string["privacy"] = 'Retningslinjer angående personvern';
$string["service_name"] = 'Eliademy';
$string["task_del"] = 'sletttet en oppgave';
