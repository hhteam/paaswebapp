<?php 
$string["changepassworduser"] = 'Promjeni lozinku za korisničko ime';
$string["email_team"] = 'Eliademi tim';
$string["erroruploadfailed"] = 'Otpremanje propalo';
$string["event_del"] = 'izbrisan događaj';
$string["event_upd"] = 'Ažuriran događaj';
$string["follow_us"] = 'Slijedite nas na';
$string["forum_general"] = 'Opći forum';
$string["grade"] = 'Ocijenjen zadatak';
$string["header_about"] = 'O nama';
$string["header_blog"] = 'Blog';
$string["header_calendar"] = 'Kalendar';
$string["header_settings"] = 'Postavke';
$string["label_teacher"] = 'Moderator';
$string["login_terms2"] = 'Uvjeti usluge';
$string["material"] = 'otpremljen novi materijal tečaja';
$string["overview_topic"] = 'Sadržaj tečaja';
$string["pluginname"] = 'MonoRail';
$string["post"] = 'okačen komentar u diskusiji';
$string["privacy"] = 'Polisa privatnosti';
$string["service_name"] = 'Eliademi';
$string["task_del"] = 'izbrisati zadatak';
$string["task_new"] = 'kreiran novi zadatak';
$string["task_upd"] = 'ažuriranje zadatka';
