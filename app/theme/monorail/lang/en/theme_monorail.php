<?php

/**
 * Monorail theme
 *
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   monorail
 */

/* Common */
$string['service_name'] = 'MRDI';
$string['sent_via_service'] = 'via MRDI';
$string['pluginname'] = 'MonoRail';
//$string["self_paced"] = "Self-paced";
$string["label_link"] = "link";
$string["currency_euro"] = "€";


/* Buttons */
$string['email_button_first_course'] = 'Create Your First Course';
$string['email_button_update_payment_details'] = 'Update Payment Details';
$string['email_action_go_to_catalogue'] = 'Learn Something New';
$string['email_action_enroll'] = 'Start Learning';
$string['email_action_join'] = 'Accept Invitation';
$string['email_button_view_on_MRDI'] = 'View this on MRDI';
$string['email_action_go_to_MRDI'] = 'Go to MRDI';
//$string['email_action_go_to_admin_panel'] = 'Go to admin interface';

/* Course Categories, strangely they have be here. Do not remove.*/
$string['course_category_select'] = '- Select a category -';
$string['course_category_social_science'] = 'Social science';
$string['course_category_arts_design'] = 'Arts and Design';
$string['course_category_business_law'] = 'Business and Law';
$string['course_category_technology'] = 'Technology';
$string['course_category_education'] = 'Education';
$string['course_category_life_science'] = 'Life science';
$string['course_category_physical_sciences'] = 'Physical science';
$string['course_category_philosophy'] = 'Philosophy';
$string['course_category_humanities'] = 'Humanities';
$string['course_category_languages'] = 'Languages';
$string['course_category_other'] = 'Other';

/* Header when logged in */
$string['header_my_courses'] = 'My Courses';
$string['header_calendar'] = 'Calendar';
$string['header_catalog'] = 'Catalog';
$string['header_courses'] = 'Courses';
$string['header_admin'] = 'Admin';

/* Header profile dropdown */
$string['header_myprofile'] = 'My Profile';
$string['header_settings'] = 'Settings';
$string['header_analytics'] = 'Analytics';
$string['header_helpdesk'] = 'Helpdesk';
$string['header_logout'] = 'Logout';

/* Header not logged in dropdown */
$string['header_business'] = 'Premium';
$string['header_features'] = 'Features';
//$string['header_mobile'] = 'Mobile';
$string['header_lti'] = 'LTI';
$string['header_blog'] = 'Blog';
$string['header_about'] = 'About Us';
$string['button_help_translate'] = 'Add your language';
$string['header_signup'] = 'Sign up';
$string['header_login'] = 'Login';

/* Email/Footer (also global magic footer) */
$string['follow_us'] = 'Follow us on';
$string['text_madeinfinland']  = 'Made in Finland';
$string['settings_url_text'] = 'Change notifications or unsubscribe';
$string['email_visit_support'] = 'Access MRDI Knowledgebase';
$string['footer_slogan'] = "Democratizing education with technology";
$string['login_terms2'] = 'Terms of Use';
$string['privacy'] = 'Privacy Policy';
$string['opensource'] = 'Open Source';
$string['try_premium'] = 'Try Premium and make Live courses.';
$string['try_new_course'] = 'Or take another course to learn something new.';

/* Error messages */
$string['errorfilemaxsize'] = 'Server error. Increase post_max_size and upload_max_filesize to {%a%}.';
$string['errorfileservererror'] = "Server error. Uploads directory isn't writable or executable.";
$string['errorfileservererror1'] = 'Server error. Not a multipart request. Please set forceMultipart to default value (true).';
$string['errorfileservererror2'] = "Server error. Chunks directory isn't writable or executable.";
$string['fileextensioninvalid'] = 'File has an invalid extension, it should be one of {%a%}.';
$string['errorsaveuploadfile'] = 'Could not save uploaded file.';
$string['errorsaveuploadfilemsg'] = 'The upload was cancelled, or server error encountered';
$string['erroruploadfailed'] = 'Upload failed';
$string['filename_empty'] = 'File name is empty';
$string['changepassworduser'] = 'Change password for username';
$string['file_empty'] = 'File is empty';

/* Email template */
$string['email_subject'] = 'Update on course: {%coursename%}';
$string['email_subject_invoice'] = 'Thank You For Your Purchase';
$string['email_subject_invoice_error'] = 'Payment failed';
$string['email_subject_daily'] = 'Daily summary for your courses on MRDI';
$string['email_subject_invite'] = '{%teacherName%} Invited You to a Course {%courseName%}';
$string['email_subject_MRDI_invite'] = '{%username%} Invited You to try MRDI';
$string['email_course_message_subject'] = 'Message from the course {%courseName%}';
$string['email_subject_invite_cohort'] = '{%cohortadminName%} Invited You to a Private Learning Space {%cohortName%}';
$string['email_subject_enroll'] = '{%cohortadminName%} Enrolled You to a Course {%courseName%}';
$string['email_header_course_invite'] = 'has invited you to a course';
$string['email_header_cohort_invite1'] = 'has invited you to become Admin for organization';
$string['email_header_cohort_invite2'] = 'has invited you to become organisation instructor for';
$string['email_header_cohort_invite3'] = 'has invited you to join organization';
$string['email_header_cohort_invite_info'] = 'As an administrator, you will be able to see all courses and users on this account but can’t make changes in course content unless they were created by you. All courses you create can be confidential and will be linked with organization {%cohortName%} account.';
$string['email_header_cohort_invite_info2'] = 'All courses you create can be confidential and will be linked with organization {%cohortName%} account.';
$string['email_header_cohort_invite_msg'] = 'This invitation can be accepted only using this email address';
$string['email_course_invite_explanation'] = 'This invitation can be used only once';
$string['email_header_cohort_enroll'] = 'has enrolled you to a course';
$string['email_header_course_message'] = 'has a new message for you on the course';
$string['email_footer_general'] = 'MRDI is an e-learning platform that allows educators and students to create, share and manage online courses.';
$string['email_regards'] = 'With best regards';
$string['email_team'] = 'MRDI Team';
$string['email_header_welcome'] = 'Welcome';
$string['email_header_dear_username'] = "Dear {%username%},";
$string['email_header_hello']  = 'Hello';
$string['email_label_or'] = 'or';
$string['email_label_download'] = 'Download';
$string['email_label_share_on'] = 'share on';
$string['label_teacher']  = 'Instructor';
$string['email_message_sent_by']  = 'Message was sent by';
$string['email_via_eliademy']  = 'via MRDI';

/* Forum */
$string['forum_general'] = "General forum";
$string['forum_general_intro'] = "General comments about the course. Use this if there is not a more specific forum for your post.";

/* Social sharing */
$string['share_course_msg'] = 'I\'m taking this course %s @MRDI. Click on the link to see if it is interesting for you.';
$string['share_course_complete_msg'] = 'I\'ve completed the course %s @MRDI. You might be interested too.';
$string['share_course_review_msg'] = "I gave %1 stars to %2 @MRDI course %3";
$string['share_course_smsg'] = 'I\'m taking this course %s @MRDI. You might be interested too.';
$string['share_course_complete_smsg'] = 'I\'ve completed the course %s @MRDI.';
$string['share_course_cert_smsg'] = 'I\'ve received certificate of completion @MRDI course %s.';

/* Certificate PDF template */
$string['certificate_title'] = 'Certificate of Completion';
$string['is_awarded'] = 'is awarded to';
$string['for_completing'] = 'for completing an online course on MRDI';

/* Notification email - don't rename without double checking, names are used in monorail services */
$string['notification'] = 'Notification';
$string['event'] = 'created a new event';
$string['event_del'] = 'deleted an event';
$string['event_new'] = 'created a new event';
$string['event_upd'] = 'updated an event';
$string['grade'] = 'graded a task';
$string['comment'] = 'commented on your assignment';
$string['material'] = 'uploaded new course material';
$string['post'] = 'posted a comment in discussions';
$string['section'] = 'updated topic';
$string['task_del'] = 'deleted a task';
$string['task_new']	= 'created a new task';
$string['task_upd']	= 'updated a task';
$string['user_enrolled'] = 'enrolled';
$string['user_submitted_assign'] = 'submitted an assignment';
$string['user_submitted_quiz'] = 'submitted a quiz';
$string['user_completed_course'] = 'has just completed your course';
$string['certificate'] = 'granted you Certificate of Completion!';
$string['bbb_del'] = 'deleted webinar';
$string['bbb_new'] = 'scheduled new webinar';
$string['bbb_upd'] = 'updated webinar';
$string['overview_topic'] = 'Overview'; /* Duplicate. This is used as a name of the section in content update notification, remove when all strings are unified*/

/* Order notification email - teacher*/
$string['email_order_notification_subject'] = "Order Notification for {%name%}";
$string['email_order_notification_congrats'] = "Good news! An order for your online course just came through.";
$string['email_order_notification_order_details'] = "Below, you'll find order details.";
$string['email_order_notification_discount'] = "If Purchase Amount is 0, a student either enrolled through a free coupon code";
$string['email_order_notification_course_name'] = "Course name";
$string['email_order_notification_course_price'] = "Purchase amount";
$string['email_order_notification_student_name'] = "Buyer name";
$string['email_order_notification_check_knowledgebase'] = "Also, please remember to check our knowledge base for the best practices and ideas to make your online course a best seller.";

/* Order notification email - student*/
$string['email_order_notification_thanks'] = "Thank you for the purchase on MRDI.";
$string['email_order_notification_tax'] = "Tax amount";
$string['email_order_notification_questions'] = "If you have any question about this order, please let write us at support@eliademy.com.";

/* Delisting email */
//$string["email_course_delisted_subject"] = "Your course at MRDI requires attention";
//$string["email_course_delisted_msg"] = "Your course {%coursename%} requires attention because it violates our terms of use or course quality guidelines. If you would like to appeal this decision, please contact our support team at support@eliademy.com";
//$string["email_course_delisted_go_to_course"] = "Improve your course";
//$string["email_course_listed_subject"] = "Your course is back to MRDI catalog";
//$string["email_course_listed_msg"] = "Your course {%coursename%} is again available in MRDI catalog. Thank you for improving it!";

/* Cerfificate email */
$string['email_notif_student_cert_subj'] = "Congratulations! You have received Certificate of Completion on course {%coursename%}";
$string['email_notif_student_cert_p2'] = "Congratulations! You have received Certificate of Completion on course";
$string['email_notif_student_cert_p4'] = "Keep on learning, check MRDI course catalog to find your next challenge!";
$string['email_notif_student_cert_share'] = "I received Certificate of completion of course {%coursename%} @MRDI";
$string['email_notif_student_cert_share_msg'] = "You can share it on social media and add to your LinkedIn profile. You can also order a high quality printed copy to be delivered to your home (if course instructor allows it).";
$string['email_notif_student_cert_view'] = "View my Certificate";

/* Engagement email subjects */
$string['email_engagement_subject_welcome'] = 'Welcome to MRDI';
$string['email_engagement_subject_welcome-cohort'] = 'Welcome to MRDI Premium';

$string['email_engagement_subject_premium-engagement-1'] = 'We are here to help you with first online course';
$string['email_engagement_subject_premium-engagement-2'] = 'We are here to help you with first online course';
$string['email_engagement_subject_premium-engagement-3'] = 'Friendly reminder: Your trial expires in 7 days';
$string['email_engagement_subject_premium-engagement-4'] = 'Kind reminder: Your trial expires tomorrow';
$string['email_engagement_subject_premium-engagement-5'] = 'Letter from the Founders';

$string['email_engagement_subject_premium-engagement-1-create-course'] = 'We are here to help you with first online course';
$string['email_engagement_subject_premium-engagement-2-invite-users'] = 'We are here to help you with first online course';
$string['email_engagement_subject_premium-engagement-3-seven-days-left'] = 'Friendly reminder: Your trial expires in 7 days';
$string['email_engagement_subject_premium-engagement-4-one-day-left'] = 'Kind reminder: Your trial expires tomorrow';
$string['email_engagement_subject_premium-engagement-5-ask-for-feedback'] = 'Letter from the Founders';

//$string['email_engagement_subject_personal-support-cohort'] = 'Get the best out of your E4B account';
//$string['email_engagement_subject_customize-cohort-reminder'] = 'Customize the look and feel of your E4B account';
//$string['email_engagement_subject_encourage-learning-together'] = 'Encourage learning together with E4B';
//$string['email_engagement_subject_manage-learning-space'] = 'Manage your private learning space';
//$string['email_engagement_subject_cohort-creating-a-course'] = 'Creating a course is easy on MRDI';
//$string['email_engagement_subject_cohort-enroll-more-people'] = 'Enrol more people to your courses';
//$string['email_engagement_subject_cohort-enroll-more-people-course'] = 'Enrol more people to your course {%coursename%}';

/* Engagement email - welcome */
$string['email_welcome_2_p1'] = 'Welcome to MRDI learning platform!';
$string['email_welcome_2_p2'] = '<b>Interested to create your own online course?</b> We have everything you need to teach:';
$string['email_welcome_2_li1'] = 'Simple course content editor;';
$string['email_welcome_2_li2'] = 'Interactive discussion boards;';
$string['email_welcome_2_li3'] = 'Task management and grading tools;';
$string['email_welcome_2_li4'] = 'High quality completion certificates;';
$string['email_welcome_2_li5'] = 'Marketplace for free and paid courses;';
$string['email_welcome_2_li6'] = 'Live video tutoring and webinars.';
$string['email_welcome_2_p3'] = 'The best thing you can do right now is to try MRDI out with your own data. It\'s very easy to create a course and invite your students.';
//Following is reused amount templates
$string['email_welcome_2_p5'] = '<b>Looking for a course to take?</b> Check out our public course catalog and join your first class today - there’s no limit to the number of classes you can take!';
$string['email_welcome_2_p6'] = '<b>Got a question?</b> We are always listening at support@eliademy.com. In addition, MRDI has a <b>tiny question mark (?)</b> at the bottom right corner of each page. Type the question you have in mind and we might have an answer ready for you.';
$string['email_welcome_2_p7'] = 'The mission of MRDI is "democratize education with technology". We believe that education is a fundamental right of any individual in the world, and we are fully committed to keeping platform free for all individual instructors, NGOs and publicly funded educational institutions. Please help us with our mission and <a href="https://eliademy.com/support">donate today</a> in order for continue offering a free service to those at need.';
$string['email_welcome_2_p8'] = 'Thank you and happy learning.';

/* Engagement email - premium-welcome */
$string['email_premium_welcome_p1'] = 'Thank you for starting MRDI Premium 30 days trial! MRDI Premium is a solution for your organization to train your employees, customers or students. It includes all free functionality plus special features:';
$string['email_premium_welcome_li0'] = 'No advertisements;';
$string['email_premium_welcome_li1'] = 'Confidential courses;';
$string['email_premium_welcome_li2'] = 'Unlimited webinars;';
$string['email_premium_welcome_li3'] = 'Admin controls and analytics';
$string['email_premium_welcome_li4'] = 'Unlimited bandwidth and document storage;';
$string['email_premium_welcome_li5'] = 'Customizable completion certificates';
$string['email_premium_welcome_li6'] = '10% commision rate for course sales';
$string['email_premium_welcome_li7'] = '128-bit SSL security';
$string['email_premium_welcome_li8'] = 'Landing page for your courses';
$string['email_premium_welcome_li9'] = 'Priority customer support';

$string['email_premium_welcome_p2'] = 'The best thing you can do right now is to try MRDI out with your own data.';
//string reusal
//$string['email_button_first_course'] = 'Create Your First Course';
$string['email_premium_welcome_p4'] = 'Also, we have prepared a list of resources to help you get started:';
$string['email_premium_welcome_l2_li1'] = 'Knowledge Base, where you can search for useful tips and see our roadmap';
$string['email_premium_welcome_l2_li2'] = 'YouTube channel, for our webcast and video tutorials';
$string['email_premium_welcome_l2_li3'] = 'Free course on how to use webinar training in your organization';
$string['email_premium_welcome_l2_li4'] = 'List of most important features and instructions on how to use them';
$string['email_premium_welcome_p5'] = 'We\'ll send a couple more email tips during your 30 days trial, and you can unsubscribe any time. If you would like assistance or to talk to someone in our team, write us at support@eliademy.com.'; 
$string['email_premium_welcome_p6'] = 'We love showing people how they can get the most out of MRDI, so don\'t hesitate to ask us anytime.';




