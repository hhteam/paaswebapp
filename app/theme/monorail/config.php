<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


    date_default_timezone_set("Europe/Helsinki");

$THEME->name = 'monorail';
$THEME->parents = array('base');
$THEME->sheets = array(
	'jquery.timepicker',
    'styles',    // TODO old monorailutils stuff
    'colorbox',
    'bootstrap.min',
    'bootstrap-responsive.min',
    'datepicker',
	'pagelayout',
	'general' ,
	'calendar',
	'course' ,
	'profile' ,
	'login',
	'standard',
    'loginsignup',
	'monorail',
    'fineuploader'
);

// To get rid of export button at the bottom of calendar.
$CFG->enablecalendarexport = false;

$THEME->parents_exclude_sheets = array(
		'base'=>array(
				'admin',
				'blocks',
				'course',
				'dock',
				'editor',
				'grade',
				'message',
				'pagelayout',
				'question',
				'user',
		),
);

$THEME->layouts = array(
		// Most backwards compatible layout without the blocks - this is the layout used by default
		'base' => array(
				'file' => 'general.php',
				'regions' => array(),
		),
		// Main course page
		'course' => array(
				'file' => 'course.php',
				'regions' => array('side-post'),
				'defaultregion' => 'side-post',
				'options' => array('langmenu'=>false),
		),
		'coursecategory' => array(
				'file' => 'course.php',
				'regions' => array('side-post'),
				'defaultregion' => 'side-post',
		),
		// Standard layout with blocks, this is recommended for most pages with general information
		'standard' => array(
				'file' => 'standard.php',
				'regions' => array('side-post'),
				'defaultregion' => 'side-post',
		),
		// My dashboard page
		'mydashboard' => array(
				'file' => 'frontpage.php',
				'regions' => array('side-post'),
				'defaultregion' => 'side-post',
				'options' => array('langmenu'=>false),
		),
		// part of course, typical for modules - default page layout if $cm specified in require_login()
		'incourse' => array(
				'file' => 'course.php',
				'regions' => array('side-post'),
				'defaultregion' => 'side-post',
		),
		// The site home page.
		'frontpage' => array(
				'file' => 'frontpage.php',
				'regions' => array('side-post'),
				'defaultregion' => 'side-post',
		),
		// No blocks and minimal footer - used for legacy frame layouts only!
		'frametop' => array(
				'file' => 'general.php',
				'regions' => array(),
				'options' => array('nofooter'=>true),
		),
		// For displaying user profile information
		'profile' => array(
				'file' => 'profile.php',
				'regions' => array(),
				'options' => array('nofooter'=>true),
		),
		// For displaying user profile information
		'login' => array(
				'file' => 'login.php',
				'regions' => array(),
				'options' => array('nofooter'=>true),
		),
                'signup' => array(
				'file' => 'signup.php',
				'regions' => array(),
				'options' => array('nofooter'=>true),
		),
		'maintenance' => array(
				'file' => 'maintenance.php',
				'regions' => array()
		)
);

$THEME->enable_dock = false;

$THEME->javascripts_footer = array(
    'jquery-1.8.3.min',
	'jquery-ui-1.8.21.custom.min',
	'jquery.selectbox-0.2',
	'jquery.timepicker.min',
    'fileuploader/util',
    'fileuploader/button',
    'fileuploader/deletefile.ajax.requester',
    'fileuploader/dnd',
    'fileuploader/handler.base',
    'fileuploader/handler.form',
    'fileuploader/handler.xhr',
    'fileuploader/header',
    'fileuploader/iframe.xss.response',
    'fileuploader/jquery-plugin',
    'fileuploader/window.receive.message',
    'fileuploader/ajax.requester',
    'fileuploader/uploader.basic',
    'fileuploader/uploader',
    'monorail',
    'notifications',
	'calendar',
    'html_helpers',
    'module',    // TODO: old monorailutils js, refactor to other files
    'forum',
	'notes',
    'jquery.scrollTo-1.4.3.1-min',
    'jquery.colorbox-min',
	'bootstrap.min',
    'bootstrap-datepicker',
    'fastclick.min'
);

$THEME->rendererfactory = 'theme_overridden_renderer_factory';

$THEME->csspostprocess = 'monorail_csspostprocess';
