<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version=2017041900;
$plugin->requires  		= 2012103000; // Requires this Moodle version
$plugin->component 		= 'theme_monorail'; // Full name of the plugin (used for diagnostics)
$plugin->dependencies 	= array(
		'block_monorail_course_overview' => ANY_VERSION,
		'local_monorailfeed' => ANY_VERSION,
        'mod_forum' => ANY_VERSION,
        'mod_assign' => ANY_VERSION
		);
$plugin->cron = 21600;

