<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once __DIR__ . "/lib.php";

global $CFG;

/* Create records in monorail_cohort_daily_users for each cohort user for
 * today.
 */
function premium_counter_process() {
    global $DB;

    $cohorts = $DB->get_recordset_sql("SELECT id, name FROM {cohort}");
    $today = strtotime("today midnight");

    foreach ($cohorts as $coh) {
        $counted_today = $DB->get_recordset_sql("SELECT id, userid FROM {monorail_cohort_daily_users} WHERE cohortid=? AND timestamp=?",
            array($coh->id, $today));

        $counted = array();

        foreach ($counted_today as $ct) {
            $counted[] = $ct->userid;
        }

        $admins = $DB->get_recordset_sql("SELECT id, userid, usertype FROM {monorail_cohort_admin_users} WHERE cohortid=?",
            array($coh->id));

        foreach ($admins as $adm) {
            if (!in_array($adm->userid, $counted)) {
                $counted[] = $adm->userid;

                $DB->execute("INSERT INTO {monorail_cohort_daily_users} (cohortid, userid, usertype, timestamp) VALUES (?, ?, ?, ?)",
                    array($coh->id, $adm->userid, $adm->usertype ? $adm->usertype : 2, $today));
            }
        }

        $private_students = $DB->get_recordset_sql("SELECT ra.userid AS userid " .
            "FROM {monorail_cohort_courseadmins} AS mcc " .
                "INNER JOIN {context} AS ctx ON ctx.instanceid=mcc.courseid " .
                "INNER JOIN {role_assignments} AS ra ON ra.contextid=ctx.id " .
                "INNER JOIN {monorail_course_perms} AS mcp ON mcp.course=mcc.courseid " .
                    "WHERE mcc.cohortid=? AND ctx.contextlevel=50 AND mcp.privacy=2", array($coh->id));

        foreach ($private_students as $ps) {
            if (!in_array($ps->userid, $counted)) {
                $counted[] = $ps->userid;

                $DB->execute("INSERT INTO {monorail_cohort_daily_users} (cohortid, userid, usertype, timestamp) VALUES (?, ?, ?, ?)",
                    array($coh->id, $ps->userid, 5, $today));
            }
        }

        $members = $DB->get_recordset_sql("SELECT id, userid FROM {cohort_members} WHERE cohortid=?", array($coh->id));

        foreach ($members as $mem) {
            if (!in_array($mem->userid, $counted)) {
                $counted[] = $mem->userid;

                $DB->execute("INSERT INTO {monorail_cohort_daily_users} (cohortid, userid, usertype, timestamp) VALUES (?, ?, ?, ?)",
                    array($coh->id, $mem->userid, 4, $today));
            }
        }
    }
}
