<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


// NOTE!! This whole file can be removed once forum is fully refactored
// to work via Ajax and JS. Currently only post deletion is done via
// here and also the forum needs to be loaded for some of the JS
// to work. Jason / 15th Oct 2012

require_once('../../config.php');

// For forum module
require_once($CFG->dirroot . '/mod/forum/lib.php');
require_once($CFG->libdir.'/completionlib.php');
require_once('lib.php');
require_once('post_form.php');

$d 		= optional_param('d', null, PARAM_INT);     	// Discuss ID
$edit   = optional_param('edit', 0, PARAM_INT);

$editmode = false;
if ($edit) {
	$editmode = true;

	$parent = forum_get_post_full($edit);
	$d		= $parent->discussion;
}

if (!$d) {
	die;
}

$discussion = $DB->get_record('forum_discussions', array('id' => $d), '*', MUST_EXIST);
$course 	= $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
$forum 		= $DB->get_record('forum', array('id' => $discussion->forum), '*', MUST_EXIST);
$cm 		= get_coursemodule_from_instance('forum', $forum->id, $course->id, false, MUST_EXIST);

require_course_login($course, true, $cm);

add_to_log($course->id, 'theme_monorail', 'view discuss', 'forum.php?d='.$d, '');

$modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
require_capability('mod/forum:viewdiscussion', $modcontext, NULL, true, 'noviewdiscussionspermission', 'forum');

if ($forum->type == 'news') {
	if (!($USER->id == $discussion->userid || (($discussion->timestart == 0
			|| $discussion->timestart <= time())
			&& ($discussion->timeend == 0 || $discussion->timeend > time())))) {
		print_error('invaliddiscussionid', 'forum', "$CFG->wwwroot/mod/forum/forum.php?c=$forum->course");
	}
}

unset($SESSION->fromdiscussion);

$parent = $discussion->firstpost;

if (! $post = forum_get_post_full($parent)) {
	print_error("notexists", 'forum', "$CFG->wwwroot/theme/monorail/forum.php?c=$forum->course");
}

// discuss ID as hidden div
echo $OUTPUT->box($d, array('hide'), 'discuss_id');

echo "<table class='table-forum'><tr id='forumtable-header'><td>";

// Render the heading row
$header 		 = "<div class='forum-current'>$discussion->name</div>";
echo $header . "</td></tr>";

// Check whether user has the capability to reply the discussion
$canreply = forum_user_can_post($forum, $discussion, $USER, $cm, $course, $modcontext) && !$editmode;

echo '</table>';

// Render reply box

$coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);

if ($canreply) {

	// Make sure user can post here
	if (isset($cm->groupmode) && empty($course->groupmodeforce)) {
		$groupmode =  $cm->groupmode;
	} else {
		$groupmode = $course->groupmode;
	}
	if ($groupmode == SEPARATEGROUPS and !has_capability('moodle/site:accessallgroups', $modcontext)) {
		if ($discussion->groupid == -1) {
			break;
		} else {
			if (!groups_is_member($discussion->groupid)) {
				break;
			}
		}
	}

	if (!$cm->visible and !has_capability('moodle/course:viewhiddenactivities', $coursecontext)) {
		break;
	}

	// Load up the $post variable.

	$parent = forum_get_post_full($discussion->firstpost);

	$post = new stdClass();
	$post->course      = $course->id;
	$post->forum       = $forum->id;
	$post->discussion  = $parent->discussion;
	$post->parent      = $parent->id;
	$post->subject     = $parent->subject;
	$post->userid      = $USER->id;
	$post->message     = '';

	$post->groupid = ($discussion->groupid == -1) ? 0 : $discussion->groupid;

	$strre = get_string('re', 'forum');
	if (!(substr($post->subject, 0, strlen($strre)) == $strre)) {
		$post->subject = $strre.' '.$post->subject;
	}

	unset($SESSION->fromdiscussion);

	$mform_post = new theme_monorail_post_form('post.php', array(
			'course'		=>$course,
			'cm'			=>$cm,
			'coursecontext'	=>$coursecontext,
			'modcontext'	=>$modcontext,
			'forum'			=>$forum,
			'post'			=>$post,
			'reply'			=>$parent->id,
			'parent'		=>$parent));

	if ($USER->id != $post->userid) {   // Not the original author, so add a message to the end
		$data->date = userdate($post->modified);
		if ($post->messageformat == FORMAT_HTML) {
			$data->name = '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$USER->id.'&course='.$post->course.'">'.
					fullname($USER).'</a>';
			$post->message .= '<p>(<span class="edited">'.get_string('editedby', 'forum', $data).'</span>)</p>';
		} else {
			$data->name = fullname($USER);
			$post->message .= "\n\n(".get_string('editedby', 'forum', $data).')';
		}
	}

	if (forum_is_subscribed($USER->id, $forum->id)) {
		$subscribe = true;

	} else if (forum_user_has_posted($forum->id, 0, $USER->id)) {
		$subscribe = false;

	} else {
		// user not posted yet - use subscription default specified in profile
		$subscribe = !empty($USER->autosubscribe);
	}

	if (!empty($parent)) {
		$heading = get_string("yourreply", "forum");
	} else {
		if ($forum->type == 'qanda') {
			$heading = get_string('yournewquestion', 'forum');
		} else {
			$heading = get_string('yournewtopic', 'forum');
		}
	}

	$page_params = array('reply'=>$parent->id);

	$draftid_editor = file_get_submitted_draft_itemid('message');
	$currenttext = file_prepare_draft_area($draftid_editor, $modcontext->id, 'mod_forum', 'post', empty($post->id) ? null : $post->id, array('subdirs'=>true), $post->message);
	$mform_post->set_data(array(
			'general'=>$heading,
			'subject'=>$post->subject,
			'message'=>array(
					'text'=>$currenttext,
					'format'=>empty($post->messageformat) ? editors_get_preferred_format() : $post->messageformat,
					'itemid'=>$draftid_editor
			),
			'subscribe'=>$subscribe?1:0,
			'mailnow'=>!empty($post->mailnow),
			'userid'=>$post->userid,
			'parent'=>$post->parent,
			'discussion'=>$post->discussion,
			'course'=>$course->id) +
			$page_params +

			(isset($post->format)?array(
					'format'=>$post->format):
					array())+

			(isset($discussion->timestart)?array(
					'timestart'=>$discussion->timestart):
					array())+

			(isset($discussion->timeend)?array(
					'timeend'=>$discussion->timeend):
					array())+

			(isset($post->groupid)?array(
					'groupid'=>$post->groupid):
					array())+

			(isset($discussion->id)?
					array('discussion'=>$discussion->id):
					array()));

	forum_check_throttling($forum, $cm);

	$CFG->texteditors = 'textarea';
	echo '<div id="reply-container">';
	echo '<div id="post-form" class="indiscuss-reply">';
	$mform_post->display();
	echo '</div>';

	echo '<div id="replybox">';
	echo '<input type="button" value="'.get_string('reply', 'forum').'" onclick="monorail_forum_showreply('.$USER->id.', '.(isset($post->groupid)?$post->groupid:0).','.$course->id.','.$discussion->id.')">';
	echo '<div class="clear"></div>';
	echo '</div>';
	echo '</div>';
}
