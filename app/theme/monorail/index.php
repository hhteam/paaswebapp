<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


	require_once(dirname(dirname(dirname(__FILE__))).'/config.php');

    require_once(dirname(__FILE__).'/lib.php');

	// For participants module
	require_once($CFG->libdir.'/tablelib.php');
	require_once($CFG->libdir.'/filelib.php');

	// For tasks module
	require_once($CFG->libdir.'/gradelib.php');

	// Define constants
	define('MOD_PARTICIPANTS'	, 'participants');
	define('MOD_TASKS'			, 'tasks');

	// Define stdClass for module Participants
	$datestring 		= new stdClass();
	$datestring->year  	= get_string('year');
	$datestring->years 	= get_string('years');
	$datestring->day   	= get_string('day');
	$datestring->days  	= get_string('days');
	$datestring->hour  	= get_string('hour');
	$datestring->hours 	= get_string('hours');
	$datestring->min   	= get_string('min');
	$datestring->mins  	= get_string('mins');
	$datestring->sec   	= get_string('sec');
	$datestring->secs  	= get_string('secs');

	// Get Param from request
	$id 	= required_param('id', PARAM_INT);   	// course
	if (!$course = $DB->get_record('course', array('id'=>$id))) {
		print_error('invalidcourseid');
	}
	$mod = optional_param('mod', MOD_PARTICIPANTS, PARAM_ALPHA);	// module

	// In case of invalid module selection, change to default module
	if ($mod != MOD_PARTICIPANTS && $mod != MOD_TASKS) {
		$mod = MOD_PARTICIPANTS;
	}

	require_course_login($course);

	add_to_log($course->id, 'theme_monorail', 'view '.$mod, 'index.php?id='.$course->id.'&mod='.$mod, '');

	$context 	= get_context_instance(CONTEXT_COURSE, $course->id, MUST_EXIST);

	$PAGE->set_url('/theme/monorail/index.php', array('id' => $id, 'mod' => $mod));
	$PAGE->set_title("$course->shortname: ".get_string($mod, 'theme_monorail'));
	$PAGE->set_heading(format_string($course->fullname));
	$PAGE->set_context($context);
	$PAGE->set_course($course);
	$PAGE->set_pagetype('theme-monorail-' . $mod);

	switch ($mod) {
		case MOD_PARTICIPANTS:
			$PAGE->set_pagelayout('incourse');
			echo $OUTPUT->header();
       		require_capability('moodle/course:viewparticipants', $context);

       		/// Get the hidden field list
       		if (has_capability('moodle/course:viewhiddenuserfields', $context)) {
       			$hiddenfields = array();  // teachers and admins are allowed to see everything
       		} else {
       			$hiddenfields = array_flip(explode(',', $CFG->hiddenuserfields));
       		}

       		// we are looking for all users with this role assigned in this context or higher
       		$contextlist = get_related_contexts_string($context);

       		list($esql, $params) = get_enrolled_sql($context, NULL, NULL, true);
       		$joins = array("FROM {user} u");

       		$select = "SELECT u.id, u.username, u.firstname, u.lastname,
       			u.email, u.city, u.country, u.picture,
       			u.lang, u.timezone, u.maildisplay, u.imagealt,
       			COALESCE(ul.timeaccess, 0) AS lastaccess";
       		$joins[] = "JOIN ($esql) e ON e.id = u.id"; // course enrolled users only
       		$joins[] = "LEFT JOIN {user_lastaccess} ul ON (ul.userid = u.id AND ul.courseid = :courseid)"; // not everybody accessed course yet
       		$params['courseid'] = $course->id;

       		// performance hacks - we preload user contexts together with accounts
       		list($ccselect, $ccjoin) = context_instance_preload_sql('u.id', CONTEXT_USER, 'ctx');
       		$select 	.= $ccselect;
       		$joins[]	 = $ccjoin;

       		$from 		 = implode("\n", $joins);
       		$sort 		 = ' ORDER BY firstname ASC';
       		$where 		 = '';

       		$userlist	 = $DB->get_recordset_sql("$select $from $where $sort", $params, NULL, NULL);
       		$matchcount 	= $DB->count_records_sql("SELECT COUNT(u.id) $from $where", $params);

       		// In case there is no participant
       		if ($matchcount == 0) {
       			break;
       		}

       		$usersprinted 	= array();
       		$content		= '';
       		$teachers		= ''; // Variable to save teacher data to insert later

            foreach ($userlist as $user) {
                if (in_array($user->id, $usersprinted)) { /// Prevent duplicates by r.hidden - MDL-13935
                    continue;
                }

                $usersprinted[] = $user->id;
                $usercontent	= '';

                context_instance_preload($user);

                $context = get_context_instance(CONTEXT_COURSE, $course->id);
                $usercontext = get_context_instance(CONTEXT_USER, $user->id);

                $countries = get_string_manager()->get_list_of_countries();

                /// Get the hidden field list
                if (has_capability('moodle/course:viewhiddenuserfields', $context)) {
                	$hiddenfields = array();
                } else {
                	$hiddenfields = array_flip(explode(',', $CFG->hiddenuserfields));
                }

                $usercontent .= $OUTPUT->container_start('floatleft center user-block');

                // Profile picture
                $usercontent .= '<a class="userlink" href="'.$CFG->wwwroot.'/theme/monorail/user.php?id='.$user->id.'&amp;course='.$course->id.'">';
                $usercontent .= $OUTPUT->user_picture($user, array('size' => 100, 'courseid'=>$course->id, 'link'=>false));

                // User fullname
                $usercontent .= $OUTPUT->container(fullname($user, has_capability('moodle/site:viewfullnames', $context)), 'username center');
                $usercontent .= '</a>';

                // User role - display the role to UI
                $roles = get_user_roles($context , $user->id);

                $usercontent .= $OUTPUT->container_end(); // close tag user-block


                // Classify the user's role
            	if (reset($roles)->name == get_string('defaultcourseteacher')) {
            		$teachers .= $usercontent; // teacher profile
            	}
            	else {
                	$content .= $usercontent; // student profile
                }
            }

            $content = $teachers . $content . $OUTPUT->container('', 'clear');

            // Display module header
            if (count($usersprinted) > 1) {
            	$strparticipants = get_string('join_participants', 'theme_monorail');
            }
            else {
            	$strparticipants = get_string('participant', 'theme_monorail');
            }

            $content = $OUTPUT->heading(count($usersprinted) . ' ' . $strparticipants, 3) . $content;

            $content  = $OUTPUT->container_start('', 'participants-block') . $content;
            $content .= $OUTPUT->container_end(); // close tag participants-block

            echo $content;
		break;

		case MOD_TASKS:
			require_once($CFG->dirroot . '/mod/assign/locallib.php');

			$PAGE->set_pagelayout('incourse');
			echo $OUTPUT->header();

			// Get all the appropriate data

			$timenow = time();

			$table 			= new html_table();
			$table->id 		= 'tasks-table';
			$table->size	= array('50%', '50%');

			$cell			= NULL;
			$row			= new html_table_row();

			// Creating the header row
			$cell			= new html_table_cell();
			$cell->attributes['class'] = "task-header incompletedtasks";
			$cell->text		='<h3 class="main">' . get_string('incompletedtasks', 'theme_monorail') . '</h3>';
			$row->cells[]	= $cell;

			$cell			= new html_table_cell();
			$cell->attributes['class'] = "task-header completedtasks";
			$cell->text		='<h3 class="main">' . get_string('completedtasks', 'theme_monorail') . '</h3>';
			$row->cells[]	= $cell;

			$table->data[]  = $row;

			$count  = array(0, 0);
			$selcol	= 0;

			$assignments = get_all_instances_in_course("assign", $course);
            $quizzes = get_all_instances_in_course("quiz", $course);

            // unify data
            $tasks = array();
            foreach ($quizzes as $quiz) {
                $quiz->duedate = $quiz->timeclose;
                $quiz->modulename = 'quiz';
                $tasks[$quiz->coursemodule] = $quiz;
            }
            foreach ($assignments as $assign) {
                $assign->modulename = 'assign';
                $tasks[$assign->coursemodule] = $assign;
            }

            // sort with duedate
            uasort($tasks, "sort_using_due_date");

			foreach ($tasks as $task) {

                // once monorail has a quiz.php, replace this whole if else with one line using $task->modulename
                if ($task->modulename == 'assign')
                    $link 		= html_writer::link(new moodle_url('/theme/monorail/assign.php', array('id' => $task->coursemodule)), $task->name);
                else if ($task->modulename == 'quiz')
                    $link 		= html_writer::link(new moodle_url('/mod/quiz/view.php', array('id' => $task->coursemodule)), $task->name);
                else
                    continue;

				$date 		= userdate($task->duedate);

                if ($task->modulename == 'assign')
                    $submitted	= $DB->get_record('assign_submission', array('assignment'=>$task->id, 'userid'=>$USER->id));
                else {
                	$submitted	= $DB->get_records('quiz_attempts', array('quiz'=>$task->id, 'userid'=>$USER->id));
                	$submitted  = reset($submitted);
                }

				$grading_info = grade_get_grades($course->id, 'mod', $task->modulename, $task->id, $USER->id);
				if (isset($grading_info->items[0]) && !$grading_info->items[0]->grades[$USER->id]->hidden ) {
					$grade = $grading_info->items[0]->grades[$USER->id]->str_grade;

					if ($grade == '-') {
						$grade = NULL;
					}
				}
				else {
					$grade =  NULL;
				}

				// Outside container
				$content  = $OUTPUT->container_start('task-info');

				// Task name
				$content .= $OUTPUT->container_start('task-name');
				$content .= $link;
				$content .= $OUTPUT->container_end();

				// Task due
				if (($submitted == NULL ||
                    (isset($submitted->status) && $submitted->status == ASSIGN_SUBMISSION_STATUS_DRAFT) ||
                    (isset($submitted->timefinish) && ($submitted->timefinish == NULL || $submitted->timefinish == 0))) && $grade == NULL) {
					$selcol = 0;

					$content .= $OUTPUT->container_start('task-due');
                    if ($task->duedate != NULL && $task->duedate > 0) {
                        $content .= get_string('duedate', 'theme_monorail') . ": ".userdate($task->duedate, '%d.%m.%Y, %H:%M');

                        // Calculate and show how much time is left for the assignment
                        $timeleft = date_diff(new DateTime($date), new DateTime(date("m/d/Y h:i:s a", time())));
                        if ($timeleft->invert == 1) {
                            $content .= " <span class='bold'>(";
                            if ($timeleft->days > 0) {
                                $content .= $timeleft->days . ' ' . get_string('days');
                            }
                            else {
                                $content .= $timeleft->h . ' ' . get_string('hours') . ' ' . $timeleft->i . ' ' . get_string('minutes');
                            }
                            $content .= ")</span>";
                        }
                        // In case of overdue assignment
                        else {
                            $content .= " <span class='bold overdue'>(";
                            if ($timeleft->days > 0) {
                                $content .= $timeleft->days . ' ' . get_string('days');
                            }
                            else {
                                $content .= $timeleft->h . ' ' . get_string('hours') . ' ' . $timeleft->i . ' ' . get_string('minutes');
                            }
                            $content .= " ".get_string('late', 'theme_monorail').")</span>";
                        }
                    }
					$content .= $OUTPUT->container_end();
				}
				// In case the user has submitted the assignment
				else {
					$selcol = 1;

					$content .= $OUTPUT->container_start('task-submitted');
					if ($grade and $grade != '-') {
						$content .= get_string('graded', 'theme_monorail') . $grade;
					}
					else {
						$content .= get_string('submitted', 'theme_monorail')." ".get_string('waiting_for_grade', 'theme_monorail');
					}
					$content .= $OUTPUT->container_end();
				}

				// Close task-info
				$content .= $OUTPUT->container_end();

				$count[$selcol]++;

				// Put data into the table
				if (count($table->data) <= $count[$selcol]) {
					$row 						= new html_table_row();
					$cell						= new html_table_cell();
					$cell->attributes['class'] 	= 'task-box clickable ntask_'.$task->coursemodule;
                    if ($submitted == null && $grade == null) {
                        $cell->attributes['class'] .= ' ntodo';
                    } else  {
                        $cell->attributes['class'] .= ' ndone';
                    }
					$cell->text 				= $content;
					if ($selcol == 1) {
						$row->cells[0] = '';
					}
					$row->cells[$selcol]		= $cell;
					$table->data[]  			= $row;
				}
				else {
					$cell						= new html_table_cell();
					$cell->attributes['class'] 	= 'task-box clickable ntask_'.$task->coursemodule;
                    if ($submitted == null && $grade == null) {
                        $cell->attributes['class'] .= ' ntodo';
                    } else  {
                        $cell->attributes['class'] .= ' ndone';
                    }
					$cell->text 				= $content;
					$table->data[$count[$selcol]]->cells[$selcol] = $cell;
				}
			}
            // for notification insertion, let's put some containers
            for ($i=1; $i<count($table->data); $i++) {
                if ($table->data[$i]->cells[0] == '') {
                    $cell = new html_table_cell();
                    $cell->attributes['class'] .= 'ncontainer';
                    $cell->text = '';
                    $table->data[$i]->cells[0] = $cell;
                }
            }

            // No incompleted tasks
            if ($count[0] == 0) {
            	$cell 		= new html_table_cell();
            	$cell->text = get_string('no_incompletedtasks', 'theme_monorail');
            	$cell->attributes['class'] 	= 'task-box empty';

            	if (count($table->data) == 1) {
            		$row = new html_table_row();
            		$row->cells[] 	= $cell;
            		$table->data[] 	= $row;
            	}
            	else {
            		$table->data[1]->cells[0] = $cell;
            	}
            }
            // No completed tasks
            if ($count[1] == 0) {
            	$cell 		= new html_table_cell();
            	$cell->text = get_string('no_completedtasks', 'theme_monorail');
            	$cell->attributes['class'] 	= 'task-box empty';

            	if (count($table->data) == 1) {
            		$row = new html_table_row();
            		$row->cells[] 	= '';
            		$row->cells[] 	= $cell;
            		$table->data[] 	= $row;
            	}
            	else {
            		$table->data[1]->cells[1] = $cell;
            	}
            }

			echo html_writer::table($table);
		;
		break;
	}

	echo $OUTPUT->footer();
