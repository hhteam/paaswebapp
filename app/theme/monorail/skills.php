<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


// Add skill, return skill ID or false if existing
function monorail_add_skill($name) {
    global $USER, $DB;

    if (strlen($name) > 50) {
        // too long, don't even attempt, this should not have gotten past the UI checks
        return false;
    }
    if (! $DB->record_exists('monorail_skills', array('cleanedname'=>monorail_cleaned_skill_name($name)))) {
        $record = new stdClass();
        $record->name = ucwords($name);
        $record->cleanedname = monorail_cleaned_skill_name($name);
        $record->timemodified = time();
        $record->modifiedby = $USER->id;
        return $DB->insert_record('monorail_skills', $record);
    } else {
        return false;
    }
}

// Get skill by name or id
function monorail_get_skill($id, $name = null) {
    global $DB;
    if ($name) {
        return $DB->get_record('monorail_skills', array('name'=>$name));
    } else {
        return $DB->get_record('monorail_skills', array('id'=>$id));
    }
}

// Return string in lower case with all non-alphanumerics + _ removed
// For matching purposes
function monorail_cleaned_skill_name($name) {
    return strtolower(preg_replace("/[\W|_]/", "", $name));
}

// Add skill to course
function monorail_add_course_skill($skillid, $courseid) {
    global $USER, $DB;
    if (! $DB->record_exists('monorail_course_skills', array('skillid'=>$skillid, 'courseid'=>$courseid))) {
        $record = new stdClass();
        $record->courseid = $courseid;
        $record->skillid = $skillid;
        $record->timemodified = time();
        $record->modifiedby = $USER->id;
        return $DB->insert_record('monorail_course_skills', $record);
    } else {
        return false;
    }
}

// Remove skill from course
function monorail_remove_course_skill($skillid, $courseid) {
    global $DB;
    if ($DB->record_exists('monorail_course_skills', array('skillid'=>$skillid, 'courseid'=>$courseid))) {
        $DB->delete_records('monorail_course_skills', array('skillid'=>$skillid, 'courseid'=>$courseid));
        return true;
    } else {
        return false;
    }
}

// Get course skills
function monorail_get_course_skills($courseid) {
    global $DB;
    $skills = $DB->get_records('monorail_course_skills', array('courseid'=>$courseid));
    return $skills;
}

// Grant skill to user
function monorail_grant_skill($skillid, $userid, $courseid) {
    global $DB, $USER;
    if (! $DB->record_exists('monorail_user_skills', array('skillid'=>$skillid, 'userid'=>$userid))) {
        $record = new stdClass();
        $record->userid = $userid;
        $record->skillid = $skillid;
        $record->timemodified = time();
        $course = $DB->get_record('course', array('id'=>$courseid));
        $achievedfrom = array(
            $courseid => array(
                "coursename"=> $course->fullname,
                "timestamp"=> time(),
            )
        );
        $record->achievedfrom = json_encode($achievedfrom);
        return $DB->insert_record('monorail_user_skills', $record);
    } else {
        // update achievedfrom
        $record = $DB->get_record('monorail_user_skills', array('skillid'=>$skillid, 'userid'=>$userid));
        $achievedfrom = json_decode($record->achievedfrom);
        if (! isset($achievedfrom->$courseid)) {
            $course = $DB->get_record('course', array('id'=>$courseid));
            $achievedfrom->$courseid = array(
                "coursename"=> $course->fullname,
                "timestamp"=> time(),
            );
            $record->achievedfrom = json_encode($achievedfrom);
            $record->timemodified = time();
            $DB->update_record('monorail_user_skills', $record);
            return true;
        } else {
            return false;
        }
    }
}

// Get user skills
function monorail_get_user_skills($userid, $names = false) {
    global $DB;
    if ($names) {
        $skills = $DB->get_fieldset_sql('select distinct ms.name from {monorail_skills} ms join {monorail_user_skills} mus on mus.userid = ? and mus.skillid = ms.id', array($userid));
    } else {
        $skills = $DB->get_records('monorail_user_skills', array('userid'=>$userid));
    }
    return $skills;
}

// Get skills report for group of users
function monorail_get_skills_report($userids) {
    global $DB, $CFG;
    require_once($CFG->dirroot . "/user/lib.php");
    $result = array();
    list($insql, $params) = $DB->get_in_or_equal($userids);
    $skills = $DB->get_records_sql("select mus.*, ms.name from {monorail_skills} ms join {monorail_user_skills} mus on mus.userid $insql and mus.skillid = ms.id", $params);
    foreach ($skills as $skill) {
        if (! isset($result[$skill->name])) {
            $result[$skill->name] = array();
        }
        $user = $DB->get_record('user', array('id'=>$skill->userid));
        $result[$skill->name][] = array(
            'userid' => $skill->userid,
            'username' => fullname($user),
            'achievedfrom' => json_decode($skill->achievedfrom),
        );
    }
    array_multisort(array_map('count', $result), SORT_DESC, $result);
    return $result;
}

// Check for achieved skills in course
// Skills will be given if
//  1) course has ended and all tasks have been completed (when applicable)
//  2) all tasks have been completed and course has no end date
function monorail_check_achieved_skills($courseid, $userid = null, $ignoreNoSkills = false) {
    // for debugging we will time this function
    $starttime = microtime();
    global $DB, $CFG;
    require_once($CFG->libdir . '/gradelib.php');
    if (!$ignoreNoSkills)
    {
        // first check if course has skills
        $skills = monorail_get_course_skills($courseid);
        if (! count($skills)) {
            return false;
        }
    }
    $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$courseid));
    // and that it either has ended or has no end date
    if ($coursedata->enddate != 0 && $coursedata->enddate > time()) {
        return false;
    }
    // does this course have any tasks?
    $courseassign = $DB->get_records('assign', array('course'=>$courseid));
    $coursequiz = $DB->get_records('quiz', array('course'=>$courseid));
    if (! count($courseassign) && ! count($coursequiz)) {
        return false;
    }
    // Compile list of tasks that need to be submitted
    $assigns = array();
    foreach ($courseassign as $assign) {
        if (! $assign->nosubmissions) { // meaning yes :P
            $assigns[] = $assign;
        }
    }
    // all quizzes must be taken
    $quizzes = $coursequiz;
    // if only one user, check for that one, otherwise loop all students
    $users = array();
    if ($userid) {
        $users[] = $DB->get_record('user', array('id'=>$userid));
    } else {
        $context = get_context_instance(CONTEXT_COURSE, $courseid);
        $roles = array(5);
        foreach ($roles as $role) {
            $list = get_role_users($role, $context);
            $users = array_merge($users, $list);
        }
    }
    $grantskills = array();
    foreach ($users as $user) {
        try {
            foreach ($assigns as $assign) {
                try {
                    $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assign->id, 'userid'=> $user->id), '*', MUST_EXIST);
                    if ($submitinfo->status === 'draft') {
                        throw new Exception("Submission in draft");
                    }
                    $grading_info = grade_get_grades($courseid, 'mod', 'assign', $assign->id, $user->id);
                    $gradingitem = $grading_info->items[0];
                    $gradebookgrade = $gradingitem->grades[$user->id];
                    if (! $gradebookgrade->dategraded || ! $gradebookgrade->grade) {
                        throw new Exception("Not graded");
                    }
                } catch (Exception $ex) {
                    if (!empty($CFG->debug) && $CFG->debug >= DEBUG_DEVELOPER) {
                        add_to_log($courseid, 'monorail', 'skills', 'monorail_check_achieved_skills', "Exited check for assign $assign->id submissions check user $user->id because: ".$ex->getMessage());
                    }
                    //No submissions, throw out of loop
                    throw new Exception("No submissions");
                }
            }
            foreach ($quizzes as $quiz) {
                $attempts = $DB->count_records("monorail_data", array("type" => "quiz_attempt", "itemid" => $quiz->id, "datakey" => "user" . $user->id));
                if (! $attempts) {
                    throw new Exception("Not attempted");
                }
            }
            $grantskills[] = $user->id;
        } catch (Exception $ex) {
            // no skills for this user
            if (!empty($CFG->debug) && $CFG->debug >= DEBUG_DEVELOPER) {
                add_to_log($courseid, 'monorail', 'skills', 'monorail_check_achieved_skills', "Exited check for submissions check user $user->id because: ".$ex->getMessage());
            }
        }
    }
    if (!empty($CFG->debug) && $CFG->debug >= DEBUG_DEVELOPER) {
        // debug timing
        $endtime = microtime();
        add_to_log($courseid, 'monorail', 'skills', 'monorail_check_achieved_skills', "Runtime: ".$endtime-$starttime."ms");
    }
    // return list of users to grant skills to
    if (count($grantskills)) {
        return $grantskills;
    } else {
        return false;
    }
}

// Grant skills from course for one or all users
function monorail_grant_skills_from_course($courseid, $userid = null) {
    global $CFG;
    if ($users = monorail_check_achieved_skills($courseid, $userid, true)) {
        $skills = monorail_get_course_skills($courseid);
        require_once($CFG->dirroot . '/local/monorailfeed/lib.php');
        require_once($CFG->dirroot . '/theme/monorail/sociallib.php');
        foreach ($users as $user) {
            foreach ($skills as $skill) {
                monorail_grant_skill($skill->skillid, $user, $courseid);
                add_to_log($courseid, 'monorail', 'skills', 'grant_skills_from_course', "Granted skill $skill->skillid to $user");
                //TODO: fire up notification for the user in the future?
            }

            // If skills are granted, it must mean that user has completed the course.
            monorailfeed_create_notification($courseid, 'user_completed', 'teacher', $courseid, $user, null, null, null, array(3));
            social_share_course_complete($courseid, $user);
        }
        return true;
    } else {
        return false;
    }
}

?>
