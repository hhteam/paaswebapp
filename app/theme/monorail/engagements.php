<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * WARNING: all of this will fall appart when users will be able to have more than one cohort.
 */ 


function engagements_user_filter($name, $daysago, $engagementmessagesid = null, $percourse = false) {
    global $DB, $CFG;
    // get users according to user filter
    $query = "select u.id from {user} u ";
    $sendOnlyOnce = true;
    $params = array();
    switch ($name) {
        case "cohort-user-created-days-ago":
        case "cohort-user-inactive-for-days":
            $query .= "join {monorail_cohort_admin_users} mcau on u.id = mcau.userid ";
            break;
        case "cohort-and-user-created-days-ago":
            $date = getdate(time()-(60*60*24*$daysago));
            $params[] = mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]);
            $params[] = mktime(23, 59, 59, $date["mon"], $date["mday"], $date["year"]);
            $query .= "join {monorail_cohort_admin_users} mcau on u.id = mcau.userid join {cohort} c on mcau.cohortid = c.id and c.timecreated between ? and ? ";
            break;
        case "cohort-course-created-days-ago":
            $date = getdate(time()-(60*60*24*$daysago));
            $params[] = mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]);
            $params[] = mktime(23, 59, 59, $date["mon"], $date["mday"], $date["year"]);
            $query .= "join {course} c on c.timecreated between ? and ? join {monorail_cohort_courseadmins} mcca on c.id = mcca.courseid join {context} ct on c.id = ct.instanceid join {role_assignments} ra on ra.contextid = ct.id and u.id = ra.userid and ra.roleid = 3 ";
            break;

        case "premium-engagement-1-create-course":
        case "premium-engagement-2-invite-users":
            $sendOnlyOnce = false;
        case "premium-engagement-3-seven-days-left":
        case "premium-engagement-4-one-day-left":
        case "premium-engagement-5-ask-for-feedback":
            $query .= "INNER JOIN {monorail_cohort_admin_users} mcau on u.id = mcau.userid " .
                "INNER JOIN {monorail_cohort_info} AS mci ON mci.cohortid=mcau.cohortid ";
            break;

        default:
            // this is an error situation - all messages must have a user filter!
            // log and return empty result
            add_to_log(1, 'monorail', 'engagements', 'engagements_user_filter', 'ERROR: No user filter recognized: '.$name.' - message id is '.$engagementmessagesid);
            return array();
    }
    if ($engagementmessagesid && $sendOnlyOnce) {
        $params[] = $engagementmessagesid;
        $query .= "left join {monorail_engagement_users} meu on u.id = meu.userid and meu.engagementmessagesid = ? ";
        if ($percourse) {
            $query .= "and (meu.courseid = null or c.id <> meu.courseid) ";
        }
    }
    $query .= "where ";
    $firstcondition = true;
    switch ($name) {
        case "cohort-user-created-days-ago":
            $date = getdate(time()-(60*60*24*$daysago));
            $params[] = mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]);
            $params[] = mktime(23, 59, 59, $date["mon"], $date["mday"], $date["year"]);
            $query .= "u.timecreated between ? and ? ";
            $firstcondition = false;
            break;
        case "cohort-and-user-created-days-ago":
            $date = getdate(time()-(60*60*24*$daysago));
            $params[] = mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]);
            $params[] = mktime(23, 59, 59, $date["mon"], $date["mday"], $date["year"]);
            $query .= "u.timecreated between ? and ? ";
            $firstcondition = false;
            break;
        case "cohort-user-inactive-for-days":
            $params[] = time()-(60*60*24*$daysago);
            $query .= "u.lastaccess < ? ";
            $firstcondition = false;
            break;

        case "premium-engagement-1-create-course":
        case "premium-engagement-2-invite-users":
        case "premium-engagement-3-seven-days-left":
        case "premium-engagement-4-one-day-left":
        case "premium-engagement-5-ask-for-feedback":
            $firstcondition = false;
            $query .= "mci.company_status=1 ";
            break;
    }
    if ($engagementmessagesid && $sendOnlyOnce) {
        $query .= (($firstcondition) ? "" : "and ") . "(meu.status != 1 or meu.status is null) ";
    }
    if (!empty($CFG->debug) && $CFG->debug >= DEBUG_DEVELOPER) {
        require_once("$CFG->dirroot/theme/monorail/lib.php");
        monorail_data('engagements_debug', 1, 'user_filter','Userfilter: '.$name.', daysago '.$daysago.' - engagementmessagesid: '.$engagementmessagesid.' -- query: '.$query.' // params '.print_r($params, true), false);
    }

    return $DB->get_records_sql($query, $params);
}

/* return true or false for a user */
function engagements_logic($message, $userid) {
    global $DB;
    switch ($message->identifier) {
        case 'cohort-not-customized':
            // If cohort is not customized, return true
            try {
                $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);
                $cohort = $DB->get_record('cohort', array('id'=>$cohortid), 'timecreated, timemodified', MUST_EXIST);
                if ($cohort->timecreated == $cohort->timemodified) {
                    return true;
                }
            } catch (Exception $ex) {
                return false;
            }
            break;
        case 'cohort-no-participants-added':
            // If cohort still only has original creator user
            try {
                $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);
                if ($DB->count_records('cohort_members', array('cohortid'=>$cohortid)) < 2) {
                    return true;
                }
            } catch (Exception $ex) {
                return false;
            }
            break;
        case 'cohort-no-course-created':
            // If cohort still has no courses
            try {
                $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);
                if ($DB->count_records('monorail_cohort_courseadmins', array('cohortid'=>$cohortid)) == 0) {
                    return true;
                }
            } catch (Exception $ex) {
                return false;
            }
            break;

        case "premium-engagement-1-create-course":
            $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);

            if ($DB->record_exists("monorail_cohort_courseadmins", array("cohortid" => $cohortid))) {
                // Cohort has courses.
                return false;
            }

            if (strtotime("midnight") < ((int) $DB->get_field("monorail_engagement_users", "timemodified", array("userid" => $userid, "engagementmessagesid" => $message->id)))) {
                // This message already was sent today.
                return false;
            }

            $daysPassed = ceil((strtotime("midnight") - ((int) $DB->get_field("cohort", "timecreated", array("id" => $cohortid)))) / 86400);

            if ($daysPassed == 3 || $daysPassed == 5 || $daysPassed == 10 || $daysPassed == 15 || $daysPassed == 20) {
                return true;
            } else {
                return false;
            }

        case "premium-engagement-2-invite-users":
            $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);

            if (!$DB->record_exists("monorail_cohort_courseadmins", array("cohortid" => $cohortid))) {
                // Cohort has no courses.
                return false;
            }

            if ($DB->record_exists("monorail_invites", array("type" => "cohort", "targetid" => $cohortid))) {
                // Invitations sent.
                return false;
            }

            if ($DB->count_records("cohort_members", array("cohortid" => $cohortid)) > 1) {
                // Cohort has more users.
                return false;
            }

            if (strtotime("midnight") < ((int) $DB->get_field("monorail_engagement_users", "timemodified", array("userid" => $userid, "engagementmessagesid" => $message->id)))) {
                // This message already was sent today.
                return false;
            }

            $daysPassed = ceil((strtotime("midnight") - ((int) $DB->get_field("cohort", "timecreated", array("id" => $cohortid)))) / 86400);

            if ($daysPassed == 3 || $daysPassed == 5 || $daysPassed == 10 || $daysPassed == 15 || $daysPassed == 20) {
                return true;
            } else {
                return false;
            }

        case "premium-engagement-3-seven-days-left":
            $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);

            if (strtotime("midnight") < ((int) $DB->get_field("monorail_engagement_users", "timemodified", array("userid" => $userid, "engagementmessagesid" => $message->id)))) {
                // This message already was sent today.
                return false;
            }

            $daysPassed = ceil((strtotime("midnight") - ((int) $DB->get_field("cohort", "timecreated", array("id" => $cohortid)))) / 86400);

            if ($daysPassed == 23) {
                return true;
            } else {
                return false;
            }

        case "premium-engagement-4-one-day-left":
            $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);

            if (strtotime("midnight") < ((int) $DB->get_field("monorail_engagement_users", "timemodified", array("userid" => $userid, "engagementmessagesid" => $message->id)))) {
                // This message already was sent today.
                return false;
            }

            $daysPassed = ceil((strtotime("midnight") - ((int) $DB->get_field("cohort", "timecreated", array("id" => $cohortid)))) / 86400);

            if ($daysPassed == 29) {
                return true;
            } else {
                return false;
            }

        case "premium-engagement-5-ask-for-feedback":
            $cohortid = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$userid), MUST_EXIST);

            if (strtotime("midnight") < ((int) $DB->get_field("monorail_engagement_users", "timemodified", array("userid" => $userid, "engagementmessagesid" => $message->id)))) {
                // This message already was sent today.
                return false;
            }

            $daysPassed = ceil((strtotime("midnight") - ((int) $DB->get_field("cohort", "timecreated", array("id" => $cohortid)))) / 86400);

            if ($daysPassed == 40) {
                return true;
            } else {
                return false;
            }

        case 'cohort-course-no-participants':   // special case, courses handled later
        case "return-true":
            return true;
    }
    return false;
}

function engagements_trigger($name, $userid) {
    global $DB;
    $message = $DB->get_record('monorail_engagement_messages', array('name'=>$name, 'status'=>1, 'type'=>'event'));
    if ($message) {
        if (engagements_logic($message, $userid)) {
            engagements_send($userid, array($message));
        }
    }
}

function engagements_data($match, $message, $user, $course) {
    global $DB;
    switch ($match) {
        case "username":
            return fullname($user);
        case "supportnamefull":
            if ($user->lang == 'es') {
                return "Jose Manuel";
            } else {
                return "Pasi Husso";
            }
        case "supportnamefirst":
            if ($user->lang == 'es') {
                return "Jose";
            } else {
                return "Pasi";
            }
        case "coursecode":
            try {
                return $DB->get_field('monorail_course_data', 'code', array('courseid'=>$course), MUST_EXIST);
            } catch (Exception $ex) {
                return "undefined";
            }
    }
    return "";
}

// When engagement message user filter and identifier logic have been processed,
// for messages with percourse==1, find courses to send for
function engagements_filter_courses($message, $user) {
    global $DB;
    $courses = array();
    switch ($message->identifier) {
        case "cohort-course-no-participants":
            try {
                $cohortid = $DB->get_field('cohort_members', 'cohortid', array('userid'=>$user->id), MUST_EXIST);
                $courseids = $DB->get_fieldset_select('monorail_cohort_courseadmins', 'courseid', "cohortid = ?", array($cohortid));
                $role = $DB->get_record('role', array('shortname' => 'student'));
                foreach ($courseids as $course) {
                    $context = get_context_instance(CONTEXT_COURSE, $course);
                    if ($message->userfilter == "cohort-course-created-days-ago") {
                        $date = getdate(time()-(60*60*24*$message->delay));
                        $courserec = $DB->get_record('course', array('id'=>$course));
                        if ($courserec->timecreated < mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]) ||
                            $courserec->timecreated > mktime(23, 59, 59, $date["mon"], $date["mday"], $date["year"])) {
                            continue;
                        }
                    }
                    $students = get_role_users($role->id, $context);
                    if (! count($students)) {
                        if (! $DB->record_exists('monorail_engagement_users', array('userid'=>$user->id, 'courseid'=>$course, 'engagementmessagesid'=>$message->id))) {
                            $courses[] = $course;
                        }
                    }
                }
            } catch (Exception $ex) {
                // none then
            }
            break;
    }
    return $courses;
}

function engagements_send($userid, $messages) {
    global $DB, $CFG;
    require_once("$CFG->dirroot/theme/monorail/lib.php");
    
    $user = $DB->get_record('user', array('id'=>$userid));
    $basetpl = monorail_get_template('mail/base');
    
    foreach ($messages as $message) {
        $courses = array();
        if ($message->percourse) {
            // per course messages - so need to find courses which to send for
            $courses = engagements_filter_courses($message, $user);
        } else {
            $courses[] = 0; // not percourse - just store zero for a single loop
        }
        foreach ($courses as $course) {
            preg_match_all("/{%([a-z_\-]*)%}/", monorail_get_template('mail/engagements/'.$message->template), $matches); 
            $params = array();
            foreach ($matches[1] as $match) {
                $params[$match] = engagements_data($match, $message, $user, $course);
            }

            $body = monorail_template_compile($basetpl, array(
               'title' => get_string('email_engagement_subject_'.$message->template, 'theme_monorail'),
               'body' => monorail_template_compile(monorail_get_template('mail/engagements/'.$message->template), $params),
               'footer_additional_link' => '',
               'block_header' => '' 
            ));
            
            add_to_log(1, 'monorail', 'engagements', 'send', "Sending engagement message $message->name to $userid");
            
            if ((! $message->percourse && $DB->record_exists('monorail_engagement_users', array('engagementmessagesid'=>$message->id, 'userid'=>$userid))) ||
                    ($message->percourse && $DB->record_exists('monorail_engagement_users', array('engagementmessagesid'=>$message->id, 'userid'=>$userid, 'courseid'=>$course)))) {
                if ($message->percourse) {
                    $record = $DB->get_record('monorail_engagement_users', array('engagementmessagesid'=>$message->id, 'userid'=>$userid, 'courseid'=>$course));
                } else {
                    $record = $DB->get_record('monorail_engagement_users', array('engagementmessagesid'=>$message->id, 'userid'=>$userid));
                }
                if ($message->percourse) {
                    $record->courseid = $course;
                }
                $record->status = 1;
                $record->timemodified = time();
                $DB->update_record('monorail_engagement_users', $record);
            } else {
                $record = new stdClass();
                $record->userid = $userid;
                $record->engagementmessagesid = $message->id;
                if ($message->percourse) {
                    $record->courseid = $course;
                }
                $record->status = 1;
                $record->timemodified = time();
                $DB->insert_record('monorail_engagement_users', $record);
            }
            if ($message->percourse) {
                $subject = get_string_manager()->get_string('email_engagement_subject_'.$message->template.'_course', 'theme_monorail', null, $user->lang);
                $courserec = $DB->get_record('course', array('id'=>$course));
                $subject = str_replace('{%coursename%}', $courserec->fullname, $subject);
            } else {
                $subject = get_string_manager()->get_string('email_engagement_subject_'.$message->template, 'theme_monorail', null, $user->lang);
            }
            monorail_send_email('engagement', $subject, $body, $userid, $user->lang);
        }
    }
    return true;
}

function engagements_process() {
    global $DB, $CFG;
    add_to_log(1, 'monorail', 'engagements', 'process', 'Start engagement messages processing');
    $starttime = time();
    // collect
    $messages = $DB->get_records('monorail_engagement_messages', array('status'=>1, 'type'=>'time'));
    $tosend = array();
    foreach ($messages as $message) {
        $users = engagements_user_filter($message->userfilter, $message->delay, $message->id);
        if (!empty($CFG->debug) && $CFG->debug >= DEBUG_DEVELOPER) {
            add_to_log(1, 'monorail', 'engagements', 'process', 'Message: '.$message->name.', userfilter '.$message->userfilter.' - Found users: '.count($users));
        }
        foreach ($users as $user) {
            $send = engagements_logic($message, $user->id);
            if ($send) {
                if (isset($tosend[$user->id])) {
                    $tosend[$user->id][] = $message;
                } else {
                    $tosend[$user->id] = array($message);
                }
            }
        }
    }
    // do actual sending
    foreach ($tosend as $user => $messarr) {
        engagements_send($user, $messarr);
    }
    // end
    $endtime = time();
    add_to_log(1, 'monorail', 'engagements', 'process', 'End engagement messages processing - run took '.($endtime - $starttime).' seconds');
}


?>
