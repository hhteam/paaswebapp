<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

$header_tab = $OUTPUT->get_header_tab();
$returnlink = $OUTPUT->get_return_link();
$tabbuttons = $OUTPUT->get_tab_buttons();
$titleblock = $OUTPUT->get_title_block();

echo $OUTPUT->doctype();
?>
<html <?php echo $OUTPUT->htmlattributes()?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=982, initial-scale=1.0">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese" rel="stylesheet">
	<title><?php echo $PAGE->title ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
    <meta name="description" content="<?php p(strip_tags(format_text($SITE->summary, FORMAT_HTML))) ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses) ?>">

<?php
    // Notifications dump
    require_once(dirname(__FILE__) .'/../../../local/monorailfeed/lib.php');
    global $USER;
    echo '<script type="text/javascript">';
    echo 'M.cfg.monorailfeed = {};';
    echo 'M.cfg.monorailfeed.notifJson = '.monorailfeed_get_notifications($USER->id).';';
    echo 'M.cfg.magic_ui_root = "'.$CFG->magic_ui_root.'";';
    echo '</script>';
?>

<?php echo $OUTPUT->standard_top_of_body_html()?>
	<div id="page">

        <?php require dirname(__FILE__) . "/common_header.php"; ?>

		<div id="page-content">
            <div class="container">
                <?php echo $OUTPUT->main_content()?>
            </div>
		</div>

        <?php require dirname(__FILE__) . "/common_footer.php"; ?>

	</div>
	<?php echo $OUTPUT->standard_end_of_body_html() ?>

<script type="text/javascript">
    // UserVoice Javascript SDK (only needed once on a page)
    (function(){
        var uv=document.createElement('script');
        uv.type='text/javascript';
        uv.async=true;
        uv.src='//widget.uservoice.com/eTEcdS7m6UK1wuohZOcX0Q.js';
        var s=document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(uv,s)
    })();
    UserVoice = window.UserVoice || [];
    
    // UserVoice Javascript SDK developer documentation:
    // https://www.uservoice.com/o/javascript-sdk

    UserVoice.push(['identify', {
    }]);

    // Add default trigger to the bottom-right corner of the window:
    UserVoice.push(['addTrigger', {}]);

    // Autoprompt users for Satisfaction and SmartVote
    // (only displayed when certain conditions are met)
    UserVoice.push(['autoprompt', {}]);
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38321548-1']);
  _gaq.push(['_setDomainName', 'eliademy.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>
