<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 
?>
<div id="page-footer">
    <div class="container">
        <div class="mcols">
            <div id="footer-copyright" class="mleft">
               <a href="http://cbtec.fi" target="_blank">© CBTec 2016 | <?php echo(get_string("text_madeinfinland", "theme_monorail")); ?></a>
            </div>
            <div id="footer-menu" class="mcenter">
                 <a href="https://eliademy.com/terms" style="padding: 0px 5px;"><?php echo(get_string("login_terms2", "theme_monorail")); ?></a>
                 <a href="https://eliademy.com/privacy" style="padding: 0px 5px;"><?php echo get_string('privacy', 'theme_monorail')?></a>
                 <a href="https://eliademy.com/opensource" style="padding: 0px 5px;"><?php echo get_string('opensource', 'theme_monorail')?></a>
            </div>
            <div id="footer-follow" class="mright">
               <span><?php echo get_string('follow_us', 'theme_monorail')?></span>
               <span>
                <a href="http://www.facebook.com/eliademy" target="_blank"><img src="https://eliademy.com/img/logo-facebook.png" alt="Facebook" style="border-radius: 10px; width: 20px; height: 20px;"></a>
                <a href="https://twitter.com/eliademy" target="_blank"><img src="https://eliademy.com/img/logo-twitter.png" alt="Twitter" style="border-radius: 10px; width: 20px; height: 20px;"></a>
                <a href="https://plus.google.com/+Eliademy" target="_blank"><img src="https://eliademy.com/img/logo-gplus.png" alt="Google+" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
                <a href="http://pinterest.com/eliademy/" target="_blank"><img src="https://eliademy.com/img/logo-pinterest.png" alt="Pinterest" style="border-radius: 10px; width: 20px; height: 20px;"></a>
                <a href="http://www.linkedin.com/company/eliademy" target="_blank"><img src="https://eliademy.com/img/logo-linkedin.png" alt="LinkedIn" style="border-radius: 10px; width: 20px; height: 20px;"></a>
                <a href="https://www.youtube.com/c/Eliademy" target="_blank"><img src="https://eliademy.com/img/logo-youtube.png" alt="YouTube" style="border-radius: 10px; width: 20px; height: 20px;"></a>  
               </span>
            </div>
        </div>        
    </div>
</div>
