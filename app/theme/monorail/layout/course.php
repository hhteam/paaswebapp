<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


global $DB, $USER;

$header_tab = $OUTPUT->get_header_tab();
$returnlink = $OUTPUT->get_return_link();
$tabbuttons = $OUTPUT->get_tab_buttons();
$titleblock = $OUTPUT->get_title_block();

$coursecbackground = monorail_get_course_background();
$course 	= $PAGE->course;
$courseData = $DB->get_record('monorail_course_data', array('courseid'=>$course->id));
$activity 	= $PAGE->activityrecord;
$param_id 	= optional_param('id', 0, PARAM_INT);
$participants = get_course_participants($course->id);

$courselogodata = monorail_data('TEXT', $course->id, 'course_logo');
$public_course = monorail_data('BOOLEAN', $course->id, 'public');
$rating_stars = monorail_data("INTEGER", $course->id, "course_rating");

if (!empty($public_course))
{
    $public_course = reset($public_course);
    $public_course = (int) $public_course->value;
}
else
{
    $public_course = 0;
}

if (!empty($rating_stars))
{
    $rating_stars = reset($rating_stars);
    $rating_stars = (int) $rating_stars->value;
}
else
{
    $rating_stars = null;
}

$course_logo = '/app/a/app/img/logo-course-title.png';
if (!empty($courselogodata)) {
    $courselogodata = reset($courselogodata);
    if(!empty($courselogodata->value) && ($courselogodata->value != '/')) { 
        $course_logo = $courselogodata->value;
    }
}



$selected = monorail_get_current_page();

$CFG->texteditors = 'textarea';

if ($PAGE->pagetype == 'mod-resource-view') {
    // Resource pages are not supported in MonoRail any more, redirect to course overview
    redirect($CFG->magic_ui_root.'/courses/'.$courseData->code);
}

        $sql = "SELECT u.firstname, u.lastname, u.id
                  FROM {user} AS u, {role} AS r, {role_assignments} AS a, {context} AS c
                  WHERE r.shortname='editingteacher' AND r.id=a.roleid AND a.userid=u.id
                    AND a.contextid=c.id AND c.instanceid=:courseid LIMIT 1";

        // XXX: picking a random teacher from all teachers...

        $params['courseid'] = $course->id;
        $enrolledusers = $DB->get_records_sql($sql, $params);

    if (reset($enrolledusers) === FALSE)
    {
        $teacherName = "";
        $teacherPicture = "";
    }
    else
    {
        $enrolledusers = array_values($enrolledusers);

        $usr = new stdClass();
        $usr->id = $enrolledusers[0]->id;

	    $teacherPicture = $OUTPUT->user_picture($usr, array('size' => 31, 'link'=>false));

        $teacherName = $enrolledusers[0]->firstname . " " . $enrolledusers[0]->lastname;
    }

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=982, initial-scale=1.0">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese" rel="stylesheet">
	<title><?php echo $PAGE->title ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
    <meta name="description" content="<?php p(strip_tags(format_text($SITE->summary, FORMAT_HTML))) ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="pagelayout-course <?php p($PAGE->bodyclasses) ?>">

<?php
    // Notifications dump
    require_once(dirname(__FILE__) .'/../../../local/monorailfeed/lib.php');
    global $USER;
    echo '<script type="text/javascript">';
    echo 'M.cfg.monorailfeed = {};';
    echo 'M.cfg.monorailfeed.notifJson = '.monorailfeed_get_notifications($USER->id).';';
    echo 'M.cfg.magic_ui_root = "'.$CFG->magic_ui_root.'";';
    echo '</script>';
?>

<?php echo $OUTPUT->standard_top_of_body_html() ?>
	<div id="page">

        <?php require dirname(__FILE__) . "/common_header.php"; ?>

		<div id="page-content">
         <div class="container">
            <a href="/" class="back-button"><i class="icon-chevron-left"></i><?php echo(get_string("button_back", "theme_monorail")); ?></a>
            <div id="course_page_header" class="row-fluid">
		    <div class="span12">
	            <div id="course-background" class="span8 panel-left" style="background-image: url('<?php echo($coursecbackground); ?>'); background-position: 50% 50%; background-repeat: no-repeat no-repeat; height: 170px;">
				</div>
				<div id="course-information" class="span4 panel-right">
                                    <div class="logo-organization" style="position:relative; text-align:center;"><img src="<?php echo $course_logo; ?>" style="vertical-align:center;"></div>
				<div class="teacher-information">

                        <a href="#" class="teacher-profile">
                            <?php echo($teacherPicture); ?>
                            <span class="teacher-detail" style="margin-left: 10px; vertical-align: middle">
                                <span class="teacher-name"><strong><?php echo($teacherName); ?></strong></span><br>
                                <span class="teacher-title"><?php echo(get_string("label_teacher", "theme_monorail")); ?></span>
                            </span>
                        </a>

                    </div>
					<div class="rating-information">
						<span><i style="color:#4189DD" class="fa fa-users"></i></span>
						<span style="color:#4189DD;font-size:12px;"><?php echo count($participants);?></span>
                        <?php if ($public_course) {
                        $url = $CFG->catalog_url . "/catalog/product/view/sku/" . $courseData->code;
                        $message = str_replace("%s", $course->fullname, get_string('share_course_msg', 'theme_monorail'));
                        $img = $CFG->landing_page . "/extimg/" . $courseData->code . "_course_background.jpg";
                        ?>
                        <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-527241bd3919e52b"></script>
						<div class="dropdown pull-right">
						  <a class="dropdown-toggle ignored_link" data-toggle="dropdown" href="#" style="text-decoration: none; color: rgb(71, 143, 225); position: relative; top: 3px;"><i class="fa fa-share-square-o" style="font-size: 16px;"></i></a>
						  <ul class="dropdown-menu pull-left" role="menu" aria-labelledby="dLabel">
							<li><a href="https://www.facebook.com/dialog/feed?app_id=<?php echo $CFG->FBAppID; ?>&display=page&name=<?php echo urlencode($course->fullname) ?>&link=<?php echo urlencode($url) ?>&picture=<?php echo urlencode($img) ?>&redirect_uri=<?php echo urlencode("https://facebook.com") ?>" target="_blank"><i class="fa fa-facebook" style="width: 20px; display: inline-block; text-align: center;"></i><?php echo get_string("share_facebook", "theme_monorail") ?></a></li>
							<li><a href="https://plus.google.com/share?url=<?php echo urlencode($url) ?>" target="_blank"><i class="fa fa-google-plus" style="width: 20px; display: inline-block; text-align: center;"></i><?php echo get_string("share_google", "theme_monorail") ?></a></li>
							<li><a href="https://twitter.com/intent/tweet?url=<?php echo urlencode($url) ?>&text=<?php echo urlencode($message) ?>" target="_blank"><i class="fa fa-twitter" style="width: 20px; display: inline-block; text-align: center;"></i><?php echo get_string("share_twitter", "theme_monorail") ?></a></li>
							<li><a href="http://www.linkedin.com/shareArticle?mini=true&title=<?php echo urlencode($course->fullname) ?>&url=<?php echo urlencode($url) ?>" target="_blank"><i class="fa fa-linkedin" style="width: 20px; display: inline-block; text-align: center;"></i><?php echo get_string("share_linkedin", "theme_monorail") ?></a></li>
							<li><a href="mailto:?subject=<?php echo urlencode($course->fullname) ?>&body=<?php echo urlencode($message . "\n\n" . $url) ?>"><i class="fa fa-envelope-o" style="width: 20px; display: inline-block; text-align: center;"></i><?php echo get_string("share_email", "theme_monorail") ?></a></li>
							<li><a href="#" onclick="addthis_sendto('more');return false;"><i class="fa fa-share-square-o" style="width: 20px; display: inline-block; text-align: center;"></i><?php echo get_string("course_category_other", "theme_monorail"); ?></a></li>
						  </ul>
						</div>
                        <div style="text-align: center; font-size: 12px; margin-top: -35px;">
                            <?php if ($rating_stars !== null) { ?>
                            <a href="<?php echo $CFG->catalog_url; ?>/catalog/product/view/sku/<?php echo($courseData->code); ?>?review=1" style="color: rgb(71, 143, 225); text-decoration: none;">
                                <?php $num = 0; while ($num++ < $rating_stars) { ?>
                                <i class="fa fa-star"></i>
                                <?php } ?><?php while ($num++ < 6) { ?>
                                <i class="fa fa-star-o"></i>
                                <?php } ?>
                            </a>
                            <?php } else { ?>
                            <a href="<?php echo $CFG->catalog_url; ?>/catalog/product/view/sku/<?php echo($courseData->code); ?>?review=1" style="color: silver; text-decoration: none;">
                              <i class="fa fa-star-o"></i>
                              <i class="fa fa-star-o"></i>
                              <i class="fa fa-star-o"></i>
                              <i class="fa fa-star-o"></i>
                              <i class="fa fa-star-o"></i>
                            </a>
                            <?php } ?>
                        </div>
                        <?php } ?>
					</div>
				</div>
			</div>
			<div class="span12" style="margin-left:0">
				<div class="span8 panel-left">
					<div class="editable noempty full-name caption" data-field="fullname"><?php echo $course->fullname; ?></div>
				</div>
				<div class="span4 panel-right" style="height:60px">
					<div class="editable noempty short-name" data-field="shortname"><?php echo $course->idnumber?></div>
				</div>
			 	<div class="clearfix"></div>
		 	</div>
		</div>
        <div class="block-shadow"></div>

        <div id="course-menu" class="btn-group" data-toggle="buttons-radio">
		    <a type="button" id="course-button-overview" href="<?php echo($CFG->magic_ui_root); ?>/courses/<?php echo($courseData->code); ?>" class="btn"><?php echo get_string('overview','theme_monorail')?></a>
		    <a type="button" id="course-button-overview" href="<?php echo($CFG->magic_ui_root); ?>/courses/<?php echo($courseData->code); ?>/tasks" class="btn"><?php echo get_string('tasks','theme_monorail')?></a>
            <?php if (has_capability('moodle/course:update', context_course::instance($course->id))) { ?>
		    <a type="button" id="course-button-progress" href="<?php echo($CFG->magic_ui_root); ?>/courses/<?php echo($courseData->code); ?>/progress" class="btn"><?php echo(get_string("progress", "theme_monorail")); ?></a>
            <?php } ?>
		    <a type="button" id="course-button-forum" href="<?php echo($CFG->wwwroot); ?>/theme/monorail/forum.php?c=<?php echo($course->id); ?>" class="btn active"><?php echo get_string('forum','theme_monorail')?></a>
		    <a type="button" id="course-button-participants" href="<?php echo($CFG->magic_ui_root); ?>/courses/<?php echo($courseData->code); ?>/participants" class="btn"><?php echo(get_string("join_participants",'theme_monorail')); ?></a>
            <?php if (has_capability('moodle/course:update', context_course::instance($course->id))) { ?>
		    <a type="button" id="course-button-settings" href="<?php echo($CFG->magic_ui_root); ?>/courses/<?php echo($courseData->code); ?>/settings" class="btn"><?php echo(get_string("settings",'theme_monorail')); ?></a>
            <?php } ?>
	    </div>

			<?php echo $OUTPUT->main_content()?>
         </div>
	</div>

        <?php require dirname(__FILE__) . "/common_footer.php"; ?>

	</div>
	<?php echo $OUTPUT->standard_end_of_body_html()?>
<script type="text/javascript">
    // UserVoice Javascript SDK (only needed once on a page)
    (function(){
        var uv=document.createElement('script');
        uv.type='text/javascript';
        uv.async=true;
        uv.src='//widget.uservoice.com/eTEcdS7m6UK1wuohZOcX0Q.js';
        var s=document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(uv,s)
    })();
    UserVoice = window.UserVoice || [];
    
    // UserVoice Javascript SDK developer documentation:
    // https://www.uservoice.com/o/javascript-sdk

    UserVoice.push(['identify', {
    }]);

    // Add default trigger to the bottom-right corner of the window:
    UserVoice.push(['addTrigger', {}]);

    // Autoprompt users for Satisfaction and SmartVote
    // (only displayed when certain conditions are met)
    UserVoice.push(['autoprompt', {}]);
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38321548-1']);
  _gaq.push(['_setDomainName', 'eliademy.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>
