<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$showsidepre = $hassidepre && !$PAGE->blocks->region_completely_docked('side-pre', $OUTPUT);
$showsidepost = $hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT);

$bodyclasses = array();
if ($showsidepre && !$showsidepost) {
	$bodyclasses[] = 'side-pre-only';
} else if ($showsidepost && !$showsidepre) {
	$bodyclasses[] = 'side-post-only';
} else if (!$showsidepost && !$showsidepre) {
	$bodyclasses[] = 'content-only';
}

$currentuser = '';
if (strstr($PAGE->bodyclasses, 'currentuser')) {
	$currentuser = 'select';
}

$selectedtab = array('view' => '', 'edit' => '');
if (strstr($PAGE->bodyclasses, 'user-edit')) {
	$selectedtab['edit'] = 'selected';
	$currentuser = 'select';
}
else {
	$selectedtab['view'] = 'selected';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes()?> class="pagelayout-profile">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=982, initial-scale=1.0">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese" rel="stylesheet">
	<title><?php echo $PAGE->title ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
    <meta name="description" content="<?php p(strip_tags(format_text($SITE->summary, FORMAT_HTML))) ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">

<?php
    // Notifications dump
    require_once(dirname(__FILE__) .'/../../../local/monorailfeed/lib.php');
    global $USER;
    echo '<script type="text/javascript">';
    echo 'M.cfg.monorailfeed = {};';
    echo 'M.cfg.monorailfeed.notifJson = '.monorailfeed_get_notifications($USER->id).';';
    echo 'M.cfg.magic_ui_root = "'.$CFG->magic_ui_root.'";';
    echo '</script>';
?>

<?php echo $OUTPUT->standard_top_of_body_html()?>
	<div id="page">
		<div id="page-header">
			<div id="logo"><img src="<?php echo $OUTPUT->pix_url('header-logo', 'theme'); ?>"></div>
			<?php if (isloggedin()) { ?>
				<div class="bt-separate floatleft"></div>
				<div id="bt-course" class="big-button clickable">
					<div class="big-button-content">
						<span id="courses_ncount" class="notification btcourse"></span>
						<a class="big-bt-text" href="<?php echo $CFG->magic_ui_root?>"><?php echo get_string('courses')?></a>
					</div>
                </div>
                <div class="bt-separate floatleft"></div>
				<div id="bt-calendar" class="big-button clickable">
					<div class="big-button-content">
						<span id="calendar_ncount" class="notification btcalendar"></span>
                        <?php $now = getdate(); ?>
						<a class="big-bt-text" href="<?php echo $CFG->wwwroot.'/calendar/view.php?view=day&subview=week&cal_d=' . $now["mday"] . "&cal_m=" . $now["mon"] . "&cal_y=" . $now["year"] ?>"><?php echo get_string('calendar','calendar')?></a>
					</div>
				</div>
				<div class="bt-separate floatleft"></div>
				<div id="bt-notes" class="big-button clickable">
					<div style="width : 150px" class="big-button-content">
						<span id="notes_ncount" class="notification btnotes"></span>
						<a class="big-bt-text" href="<?php echo $CFG->wwwroot.'/theme/monorail/notes.php' ?>"><?php echo get_string('notes','theme_monorail')?></a>
					</div>
				</div>
				<div class="bt-separate floatleft"></div>

				<div id="profile-block" class="floatright <?php echo $currentuser?>">
					<div class="bt-separate floatleft"></div>
					<div id="user-pic" class="floatleft"><?php echo monorail_get_user_picture()?></div>
					<div id="bt-profile" class="floatleft clickable"><?php echo monorail_get_user_fullname()?></div>
					<div id="bt-settings" class="floatright">
						<a id="bt-settings-menu" href="javascript:void(0);"><img src='<?php echo $OUTPUT->pix_url('settings-arrow', 'theme')?>'></a>
						<div id="profile-menu">
							<div class="big-button clickable">
								<div class="big-button-content">
									<a href="<?php echo $CFG->magic_ui_root.'/profile'?>"><?php echo get_string('editmyprofile')?></a>
								</div>
			                </div>
			                <div class="bt-separate floatleft"></div>
			                <div class="big-button clickable">
								<div class="big-button-content">
								<a href="<?php echo $CFG->magic_ui_root.'/settings'?>"><?php echo get_string('settings')?></a>
								</div>
			                </div>
			                <div class="bt-separate floatleft"></div>
			                <div class="big-button clickable">
								<div class="big-button-content">
									<?php echo monorail_get_logout_url()?>
								</div>
			                </div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div id="page-content">
			<div id="main-col">
               	<div class="main-block">
               		<div id="course-header">
               			<div class="box-icon"><a href="<?php echo monorail_goback_link()?>" class="icon"></a></div>
               			<span class="title-block"><?php echo $PAGE->heading ?></span>
               		</div>
               		<div id="course-menu">
                        <?php if ($currentuser || $selectedtab['edit']) {?>
                        	<div class="course-button clickable floatleft <?php echo $selectedtab['view']?>">
	                            <a href="<?php echo $CFG->magic_ui_root.'/profile'?>">View Profile</a>
	                        </div>
	               			<div class="course-button clickable floatleft <?php echo $selectedtab['edit']?>">
	                            <a href="<?php echo $CFG->magic_ui_root.'/profile'?>"><?php echo get_string('editmyprofile')?></a>
	                        </div>
                        <?php }
                        else {?>
                        	<div class="course-button clickable floatleft selected">
	                            <a href="">View Profile</a>
	                        </div>
                        <?php }?>
               			<div class="clear"></div>
               		</div>
               		<div id="content-box">
               			<?php echo $OUTPUT->main_content()?>
               			<div class="clear"></div>
               		</div>
               	</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php echo $OUTPUT->standard_end_of_body_html() ?>

<script type="text/javascript">
    // UserVoice Javascript SDK (only needed once on a page)
    (function(){
        var uv=document.createElement('script');
        uv.type='text/javascript';
        uv.async=true;
        uv.src='//widget.uservoice.com/eTEcdS7m6UK1wuohZOcX0Q.js';
        var s=document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(uv,s)
    })();
    UserVoice = window.UserVoice || [];
    
    // UserVoice Javascript SDK developer documentation:
    // https://www.uservoice.com/o/javascript-sdk

    UserVoice.push(['identify', {
    }]);

    // Add default trigger to the bottom-right corner of the window:
    UserVoice.push(['addTrigger', {}]);

    // Autoprompt users for Satisfaction and SmartVote
    // (only displayed when certain conditions are met)
    UserVoice.push(['autoprompt', {}]);
</script>
</body>
</html>
