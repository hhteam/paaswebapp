<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?> class="pagelayout-signup">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=982, initial-scale=1.0">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese" rel="stylesheet">
	<title><?php echo $PAGE->title ?></title>
	<link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
	<meta name="description" content="<?php p(strip_tags(format_text($SITE->summary, FORMAT_HTML))) ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<?php 
    echo '<script type="text/javascript">';
    echo 'M.cfg.fbclientid = '.get_config('auth/googleoauth2', 'facebookclientid').';';
    echo '</script>';
?>
<body id="<?php p($PAGE->bodyid) ?>"
	class="<?php p($PAGE->bodyclasses) ?>">
	<?php echo $OUTPUT->standard_top_of_body_html()?>
	<div id="page">
        <div id="signupouterdiv">

            <div id="main-col">
                <div class="main-block">
                    <div class="columnview">
                        <div class="providerpanel">
                            <div id="signupmagicmoodle">
                                <?php echo get_string('signuptomagicmoodle', 'theme_monorail') ?>
                            </div>
                            <div id="prefillinfo">
                                <?php echo get_string('prefillinfo', 'theme_monorail') ?>
                            </div>
                            <div id="fbbuttoncontainer">
                            <input type="button" id='bt_getdatafacebook'
                                value='<?php echo get_string('getdatafacebook', 'theme_monorail')?>' />
                            </div>
                        </div>
                        <div class="contentpanel">
                            <?php echo $OUTPUT->main_content()?>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </div>
    <?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
