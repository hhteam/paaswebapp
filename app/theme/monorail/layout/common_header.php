<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB;

$auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id));

if (!$auser)
{
    $cusers = $DB->get_records_sql("SELECT cohortid FROM {cohort_members} WHERE userid=?" , array($USER->id));
}
else
{
    $cusers = array();
}

?><div id="page-header">

    <?php if (isloggedin()) { ?>

    <div class="container">
    <img id="logo" class="pull-left" style="padding-left: 0; margin-top: 13px; margin-right: 14px; height: 25px;" src="https://eliademy.com/img/logo-eliademy-header.png" alt="Eliademy"/>
    <ul class="nav pull-left">
     <li style="width: 67px;"><a href="<?php echo $CFG->magic_ui_root?>" id="header_link_courses" style="position:relative;" class="header-button"><label><?php echo(get_string("courses", "theme_monorail")); ?></label><span id="courses_ncount" class="notification"></span></a></li>
    <?php $now = getdate(); ?>
     <li style="width: 67px;"><a href="<?php echo $CFG->wwwroot.'/calendar/view.php?view=day&subview=week&cal_d=' . $now["mday"] . "&cal_m=" . $now["mon"] . "&cal_y=" . $now["year"] ?>" id="header_link_calendar"><label><?php echo(get_string("calendar", "theme_monorail")); ?></label><span id="calendar_ncount" class="notification"></span></a></li>
     <li style="width: 67px;"><a href="<?php echo $CFG->wwwroot.'/theme/monorail/notes.php' ?>" id="header_link_notes"><label><?php echo(get_string("notes", "theme_monorail")); ?></label></a></li>
<?php if ($auser || !empty($cusers)): ?>
     <li style="width: 67px;"><a href="<?php echo $CFG->magic_ui_root.'/catalog' ?>" id="header_link_catalog"><label><?php echo(get_string("catalog", "theme_monorail")); ?></label></a></li>
<?php endif ?>
    </ul>

    <ul class="nav pull-right">
      <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" id="header_link_settings" href="#">
                <?php echo monorail_get_user_picture()?>
                <span style="margin-left:10px; margin-right:4px; color:#fff">
                  <?php echo monorail_get_user_fullname()?> <i class="icon-chevron-down icon-white"></i>
                </span>               
           </a>
           <ul aria-labelledby="header_link_settings" role="menu" class="dropdown-menu pull-right">
              <li><a href="<?php echo $CFG->magic_ui_root.'/profile'?>" tabindex="-1" id="profile-btn" ><i class="icon-user"></i> <?php echo get_string('profile_myprofile', 'theme_monorail')?></a></li>
              <li><a href="<?php echo $CFG->magic_ui_root.'/settings'?>" tabindex="-1" id="settings-link"><i class="icon-cog"></i> <?php echo get_string('settings')?></a></li>
              <li><a href="<?php echo $CFG->magic_ui_root.'/analytics'?>" tabindex="-1" id="settings-link"><i class="fa fa-bar-chart-o"></i> <?php echo get_string('analytics', 'theme_monorail')?></a></li>
              <li><a href="http://helpdesk.eliademy.com/knowledgebase" target="_blank" tabindex="-1" id="support-link"><i class="icon-question-sign"></i> <?php echo get_string('support', "theme_monorail")?></a></li>
              <li class="divider"></li>
              <li><a href="<?php echo "$CFG->wwwroot/theme/monorail/cleanup.php"?>" tabindex="-1" id="header-logout-link"><i class="icon-off"></i> <?php echo get_string('logout')?></a></li>
          </ul>
      </li>
    </ul>

<?php if ($auser): ?>
    <ul class="pull-right nav">
     <li style="width: 67px; text-align: center; margin-right: 4px;">
      <a href="<?php echo $CFG->magic_ui_root.'/organization-details' ?>" id="header_link_admin" style="line-height: normal;">
       <i class="fa fa-cogs" style="font-size: 23px; color: #478FE1; margin-top: 6px;"></i>
       <label style="margin: 0px;"><?php echo(get_string("admin")); ?></label></a>
     </li>
    </ul>
<?php endif ?>

    </div>
    <?php } ?>
</div>
