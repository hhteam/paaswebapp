<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$showsidepre = $hassidepre && !$PAGE->blocks->region_completely_docked('side-pre', $OUTPUT);
$showsidepost = $hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT);

$bodyclasses = array();
if ($showsidepre && !$showsidepost) {
	$bodyclasses[] = 'side-pre-only';
} else if ($showsidepost && !$showsidepre) {
	$bodyclasses[] = 'side-post-only';
} else if (!$showsidepost && !$showsidepre) {
	$bodyclasses[] = 'content-only';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=982, initial-scale=1.0">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese" rel="stylesheet">
	<title><?php echo $PAGE->title ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
    <meta name="description" content="<?php p(strip_tags(format_text($SITE->summary, FORMAT_HTML))) ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">

<?php echo $OUTPUT->standard_top_of_body_html() ?>
	<div id="page">
		<div id="logo"><img src="<?php echo $OUTPUT->pix_url('header-logo', 'theme'); ?>"></div>
		<div id="page-content">
			<div id="main-col">
               	<div class="main-block">
					<img class="maintenance-icon" src="<?php echo $OUTPUT->pix_url('maintenance', 'theme')?>">
               		<?php echo $OUTPUT->main_content()?>
               	</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
