<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?> class="pagelayout-login">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=982, initial-scale=1.0">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic,latin-ext,cyrillic-ext,greek-ext,greek,vietnamese" rel="stylesheet">
	<title><?php echo $PAGE->title ?></title>
	<link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
	<meta name="description" content="<?php p(strip_tags(format_text($SITE->summary, FORMAT_HTML))) ?>" />
	<?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses) ?>">
<?php echo $OUTPUT->standard_top_of_body_html()?>
	<div id="page">

        <?php require dirname(__FILE__) . "/common_header.php"; ?>

		<div id="page-content">
            <div class="container" style="width: 400px;">
              <div class="content-block">
                <div>
					<?php echo $OUTPUT->main_content()?>

                    <div style="clear: both;"></div>
                </div>
              </div>
            </div>
		</div>

<script>
window.onload = function()
{
    try
    {
        document.getElementById("id_password").setAttribute("placeholder", "<?php echo get_string('oldpassword'); ?>");
        document.getElementById("id_newpassword1").setAttribute("placeholder", "<?php echo get_string('newpassword'); ?>");
        document.getElementById("id_newpassword2").setAttribute("placeholder", "<?php echo get_string('newpassword').' ('.get_String('again').')' ?>");
    }
    catch (err) { }
};
</script>

        <?php require dirname(__FILE__) . "/common_footer.php"; ?>

	</div>
	<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
