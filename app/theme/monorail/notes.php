<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(dirname(dirname(__FILE__))).'/theme/monorail/evernote/functions.php');

require_once(dirname(__FILE__).'/lib.php');

require_once('define.php');


// Get Param from request
$note_id 	= optional_param('noteid', null, PARAM_TEXT);

require_login();
$PAGE->set_context(get_system_context());

$PAGE->set_url('/theme/monorail/notes.php');
$PAGE->set_title('Eliademy: ' . get_string('notes', 'theme_monorail'));
$PAGE->set_pagetype('theme-monorail-notes');
$PAGE->set_pagelayout('standard');

$OUTPUT->set_header_tab(MONORAIL_PAGE_NOTES);

// FIXME: this line causes errors when a user with non-monorail theme is
// trying to access this page.
$OUTPUT->set_title_block(get_string('notes', 'theme_monorail'));

echo $OUTPUT->header();

// Status variables
$lastError = null;
$currentStatus = null;

// Request dispatching. If a function fails, $lastError will be updated.
if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if ($action == 'callback') {
        if (handleCallback()) {
            if (getTokenCredentials()) {
                //redirect;
                header('Location: '.$CFG->wwwroot.'/theme/monorail/notes.php');
            }
        }
    } else if ($action == 'authorize') {
        if (getTemporaryCredentials()) {
            // We obtained temporary credentials, now redirect the user to evernote.com to authorize access
            header('Location: ' . getAuthorizationUrl());
        }
    } else if ($action == 'reset') {
        resetSession();
    }
}

echo '<div class="title-block"><div><span>'. get_string('notes', 'theme_monorail') .'</span></div></div>';

if (!notes_login()) {

    echo "<div class=\"content-block\"><div style=\"text-align: center;\">";
    echo "<span class=\"green_title\">" . get_string("notes_notsignin", "theme_monorail") . "</span>";
    echo '<a id="btn_notes_signin" class="btn btn-success" href="?action=authorize">'.get_string('notes_signinbutton', 'theme_monorail').'</a>';
    echo "</div></div>";

	echo $OUTPUT->footer();

	die;
}

//$notebooks = notes_fetch_all_notebooks();
$notebooks = listNotebooks();

echo "<table id='notes-content'><tr>";
echo '<td id="notebook-box">';
if ($notebooks)
{
	foreach ($notebooks as $notebook)
    {
        echo "<div class=\"notebook-header\">" . $notebook->name . "</div>";

		// Get all notes in notebook
		if ($notes = notes_fetch_all_notes_from_notebooks($notebook->id)) {

			foreach ($notes as $note) {
				$select = '';
				if ($note->id == $note_id) {
					$select = 'selected';
				}

				$link = '<a class="note-name" id="'.$note->id.'" href="javascript:void(0)" onclick="return false;">'.$note->name.'</a>';

				echo "<div id='noteid_$note->id' class='note-header $select clickable' onclick=\"monorail_notes_show_note('$note->id'); return false;\">";
				echo $link;
				echo "<div>".get_string('notes_lastsaved', 'theme_monorail')." ". date('j.n.Y G:i', $note->timestamp) ."</div>";
				echo "<div class=\"notifybarleft hide\"> </div>";
				echo '</div>'; // Close discussion-box
			}
		}
	}
}
echo '</td>';

echo '<td id="noteview-box">';
if ($note_id) {
	$note = notes_fetch_note_by_id($note_id);

	echo "<div id='noteview-name'>$note->name</div>";
	echo "<div id='noteview-content'>$note->content</div>";
}
else {
	echo '<div class="forumnodiscuss">'.get_string('notes_noselectednote', 'theme_monorail').'</div>';
}
echo '</td>';

echo '</tr></table>';
echo "<div class=\"block-shadow\"></div>";

echo $OUTPUT->footer();
