<?php

namespace Papagei\Api;
/**
* + \Papagei\Api\Hash
*
* <code>
* $hash = new \Papagei\Api\Hash();
*
* $hash->setSecret('s3cr3t');
* $hash->setAlgorithm('whirlpool');
*
* $params = array(
*
'action' => 'login',
*
'params' => array(
*
'username' => 'testname',
*
'password' => 'password',
*
),
*
'history' => array(
*
2013, 2012, 2000, 2014
*
)
* );
*
* echo $hash->createHash('/rest/login', 'post', $params);
* </code>
*
* @author Alexander Klein <alexander.klein@papagei.com>
*/
class Hash {
/**
* @var string
*/
protected
$secret
= null;/**
* @var string
*/
protected
$path = null;
/**
* @var string
*/
protected
$verb = null;
/**
* @var array
*/
protected
$params = array();
/**
* @var string
*
* @see print_r(hash_algos());
* @see http://www.php.net/manual/en/function.hash-algos.php
*/
protected
$algorithm
= 'sha256';
/**
* @return the $secret
*/
public function getSecret () {
return $this->secret;
}
/**
* @return the $path
*/
public function getPath () {
return $this->path;
}
/**
* @return the $verb
*/
public function getVerb () {
return $this->verb;
}
/**
* @return the $params
*/
public function getParams () {
return $this->params;
}
/**
* @return the $algorithm
*/
public function getAlgorithm () {
return $this->algorithm;}
/**
* @param string $secret
* @return $this
*/
public function setSecret ($secret) {
$this->secret = $secret;
return $this;
}
/**
* @param string $path
* @return $this
*/
public function setPath ($path) {
$this->path = $path;
return $this;
}
/**
* @param string $verb
* @return $this
*/
public function setVerb ($verb) {
$this->verb = strtolower($verb);
return $this;
}
/**
* @param array $params
* @return $this
*/
public function setParams ($params) {
$this->params = $this->sortByArrayKeys($params);
return $this;
}
/**
* @param string $algorithm
* @return $this
* @throws \Exception
*/
public function setAlgorithm ($algorithm) {
if (in_array($algorithm, hash_algos())) {
$this->algorithm = $algorithm;
}
else {
throw new \Exception('Algorithm not supported');
}
return $this;
}
/**
* Create a hash of given multi dimensional array
*
* @param string $uriPath
* @param string $restVerb
* @param array $postParams
* @throws \Exception* @return string
*/
public function createHash($uriPath = null, $restVerb = null, $postParams =
array()) {
if ($uriPath != null) {
$this->setPath($uriPath);
}
if ($restVerb != null) {
$this->setVerb($restVerb);
}
if (count($postParams) > 0) {
$this->setParams($postParams);
}
/**
*
*/
if ($this->getPath() == null) {
throw new \Exception('Rest-Path not provided');
}
if ($this->getSecret() == null) {
throw new \Exception('Api-Secret not provided');
}
/**
* Prepare the input
*/
$string = $this->getPath() . ':' .$this->getVerb() . '?'
.http_build_query($this->getParams(), '', '&');
/**
* Create the hash
*/
return hash_hmac($this->getAlgorithm(), $string, $this->getSecret());
}
/**
* Sort params by key
*
* @param array $params
* @return array
*/
protected function sortByArrayKeys($params) {
ksort($params);
foreach($params as $key => $value) {
if (is_array($value)) {
$params[$key] = $this->sortByArrayKeys($value);
}
}
return $params;
}}
