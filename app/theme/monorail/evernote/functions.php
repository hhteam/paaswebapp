<?php

  /**
   * @package    theme
   * @subpackage monorail
   * @copyright  2012 Cloudberry Tec
   */

  defined('MOODLE_INTERNAL') || die;
  /*
   * Copyright 2011-2012 Evernote Corporation.
   *
   * This file contains some functions used by Evernote's PHP OAuth samples.
   */

  // Include the Evernote API from the lib subdirectory.
  // lib simply contains the contents of /php/lib from the Evernote API SDK
  define("EVERNOTE_LIBS", dirname(__FILE__) . DIRECTORY_SEPARATOR . "lib");
  ini_set("include_path", ini_get("include_path") . PATH_SEPARATOR . EVERNOTE_LIBS);

  //Put below keys in admin settings and read from thr
  define('OAUTH_CONSUMER_KEY', 'cloudberry-0861');
  define('OAUTH_CONSUMER_SECRET', 'cf2bd5f23db6643d');

  // Replace this value with https://www.evernote.com to use Evernote's production server
  //define('EVERNOTE_SERVER', 'https://sandbox.evernote.com');
  define('EVERNOTE_SERVER', 'https://www.evernote.com');

  // Replace this value with www.evernote.com to use Evernote's production server
  //define('NOTESTORE_HOST', 'sandbox.evernote.com');
  define('NOTESTORE_HOST', 'www.evernote.com');
  define('NOTESTORE_PORT', '443');
  define('NOTESTORE_PROTOCOL', 'https');

  // Evernote server URLs. You should not need to change these values.
  define('REQUEST_TOKEN_URL', EVERNOTE_SERVER . '/oauth');
  define('ACCESS_TOKEN_URL', EVERNOTE_SERVER . '/oauth');
  define('AUTHORIZATION_URL', EVERNOTE_SERVER . '/OAuth.action');
  //end of keys

  require_once("Thrift.php");
  require_once("transport/TTransport.php");
  require_once("transport/THttpClient.php");
  require_once("protocol/TProtocol.php");
  require_once("protocol/TBinaryProtocol.php");
  require_once("packages/Types/Types_types.php");
  require_once("packages/UserStore/UserStore.php");
  require_once("packages/NoteStore/NoteStore.php");

  // Import the classes that we're going to be using
  use EDAM\NoteStore\NoteStoreClient;
  use EDAM\UserStore\UserStoreClient;
  use EDAM\Types\NoteSortOrder;
  use EDAM\Error\EDAMSystemException, EDAM\Error\EDAMUserException, EDAM\Error\EDAMErrorCode;

  // Verify that you successfully installed the PHP OAuth Extension
  if (!class_exists('OAuth')) {
    die("<span style=\"color:red\">The PHP OAuth Extension is not installed</span>");
  }

  // Verify that you have configured your API key
  if (strlen(OAUTH_CONSUMER_KEY) == 0 || strlen(OAUTH_CONSUMER_SECRET) == 0) {
    $configFile = dirname(__FILE__) . '/config.php';
    die("<span style=\"color:red\">Before using this sample code you must edit the file $configFile " .
        "and fill in OAUTH_CONSUMER_KEY and OAUTH_CONSUMER_SECRET with the values that you received from Evernote. " .
        "If you do not have an API key, you can request one from " .
        "<a href=\"http://dev.evernote.com/documentation/cloud/\">http://dev.evernote.com/documentation/cloud/</a></span>");
  }

  /*
   * The first step of OAuth authentication: the client (this application)
   * obtains temporary credentials from the server (Evernote).
   *
   * After successfully completing this step, the client has obtained the
   * temporary credentials identifier, an opaque string that is only meaningful
   * to the server, and the temporary credentials secret, which is used in
   * signing the token credentials request in step 3.
   *
   * This step is defined in RFC 5849 section 2.1:
   * http://tools.ietf.org/html/rfc5849#section-2.1
   *
   * @return boolean TRUE on success, FALSE on failure
   */
  function getTemporaryCredentials() {
    global $lastError, $currentStatus;
    try {
      $oauth = new OAuth(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET);
      $requestTokenInfo = $oauth->getRequestToken(REQUEST_TOKEN_URL, getCallbackUrl());
      if ($requestTokenInfo) {
        $_SESSION['requestToken'] = $requestTokenInfo['oauth_token'];
        $_SESSION['requestTokenSecret'] = $requestTokenInfo['oauth_token_secret'];
        $currentStatus = 'Obtained temporary credentials';
        return TRUE;
      } else {
        $lastError = 'Failed to obtain temporary credentials: ' . $oauth->getLastResponse();
      }
    } catch (OAuthException $e) {
      $lastError = 'Error obtaining temporary credentials: ' . $e->getMessage();
    }
    return false;
  }

  /*
   * The completion of the second step in OAuth authentication: the resource owner
   * authorizes access to their account and the server (Evernote) redirects them
   * back to the client (this application).
   *
   * After successfully completing this step, the client has obtained the
   * verification code that is passed to the server in step 3.
   *
   * This step is defined in RFC 5849 section 2.2:
   * http://tools.ietf.org/html/rfc5849#section-2.2
   *
   * @return boolean TRUE if the user authorized access, FALSE if they declined access.
   */
  function handleCallback() {
    global $lastError, $currentStatus;
    if (isset($_GET['oauth_verifier'])) {
      $_SESSION['oauthVerifier'] = $_GET['oauth_verifier'];
      $currentStatus = 'Content owner authorized the temporary credentials';
      return TRUE;
    } else {
      // If the User clicks "decline" instead of "authorize", no verification code is sent
      $lastError = 'Content owner did not authorize the temporary credentials';
      return FALSE;
    }
  }

  /*
   * The third and final step in OAuth authentication: the client (this application)
   * exchanges the authorized temporary credentials for token credentials.
   *
   * After successfully completing this step, the client has obtained the
   * token credentials that are used to authenticate to the Evernote API.
   * In this sample application, we simply store these credentials in the user's
   * session. A real application would typically persist them.
   *
   * This step is defined in RFC 5849 section 2.3:
   * http://tools.ietf.org/html/rfc5849#section-2.3
   *
   * @return boolean TRUE on success, FALSE on failure
   */
  function getTokenCredentials() {
    global $lastError, $currentStatus,$USER, $DB;
    if (isset($_SESSION['accessToken'])) {
      $lastError = 'Temporary credentials may only be exchanged for token credentials once';
      return FALSE;
    }

    try {
      $oauth = new OAuth(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET);
      $oauth->setToken($_SESSION['requestToken'], $_SESSION['requestTokenSecret']);
      $accessTokenInfo = $oauth->getAccessToken(ACCESS_TOKEN_URL, null, $_SESSION['oauthVerifier']);
      if ($accessTokenInfo) {
        $_SESSION['accessToken'] = $accessTokenInfo['oauth_token'];
        $_SESSION['accessTokenSecret'] = $accessTokenInfo['oauth_token_secret'];
        $_SESSION['noteStoreUrl'] = $accessTokenInfo['edam_noteStoreUrl'];
        $_SESSION['webApiUrlPrefix'] = $accessTokenInfo['edam_webApiUrlPrefix'];
        // The expiration date is sent as a Java timestamp - milliseconds since the Unix epoch
        $_SESSION['tokenExpires'] = (int)($accessTokenInfo['edam_expires'] / 1000);
        $_SESSION['userId'] = $accessTokenInfo['edam_userId'];
        $currentStatus = 'Exchanged the authorized temporary credentials for token credentials';
        $sessinfo  = new stdClass();
        $sessinfo->userid   = $USER->id;
        $sessinfo->oauthtoken = $_SESSION['accessToken'];
        $sessinfo->expires = $_SESSION['tokenExpires'];
        $evernote = $DB->get_record("monorail_evernote_users", array('userid'=>$USER->id));
        if($evernote) { 
            $sessinfo->id = $evernote->id;
            $DB->update_record("monorail_evernote_users", $sessinfo);
        } else {
            $DB->insert_record("monorail_evernote_users", $sessinfo);
        }
        return TRUE;
      } else {
        $lastError = 'Failed to obtain token credentials: ' . $oauth->getLastResponse();
      }
    } catch (OAuthException $e) {
      $lastError = 'Error obtaining token credentials: ' . $e->getMessage();
    }
    return FALSE;
  }

  /*
   * list Notebooks.
   *
   *
   * @return array of notebookid, title.
   */

  function listNotebooks() {
      global $lastError, $currentStatus;

      $result = array();
      try {
          setSessionParams();
          $parts = parseUrl();

          $noteStoreTrans = new THttpClient($parts['host'], $parts['port'], $parts['path'], $parts['scheme']);
          $noteStoreProt = new TBinaryProtocol($noteStoreTrans);
          $noteStore = new NoteStoreClient($noteStoreProt, $noteStoreProt);
          $authToken = $_SESSION['accessToken'];
          $notebooks = $noteStore->listNotebooks($authToken);

          if (!empty($notebooks)) {
              foreach ($notebooks as $notebook) {
                  $nbook  = new stdClass();
                  $nbook->id   = $notebook->guid;
                  $nbook->name = $notebook->name;
                  //$nbook->timestamp =  $notebook->serviceCreated;
                  $result[] = $nbook;
              }
          }

          $currentStatus = 'Successfully listed content owner\'s notebooks';
          return $result;
      } catch (EDAMSystemException $e) {
          if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
              $lastError = 'Error listing notebooks: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
          } else {
              $lastError = 'Error listing notebooks: ' . $e->getCode() . ": " . $e->getMessage();
          }
      } catch (EDAMUserException $e) {
          if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
              $lastError = 'Error listing notebooks: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
          } else {
              $lastError = 'Error listing notebooks: ' . $e->getCode() . ": " . $e->getMessage();
          }
      } catch (EDAMNotFoundException $e) {
          if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
              $lastError = 'Error listing notebooks: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
          } else {
              $lastError = 'Error listing notebooks: ' . $e->getCode() . ": " . $e->getMessage();
          }
    } catch (Exception $e) {
        $lastError = 'Error listing notebooks: ' . $e->getMessage();
    }

    return $result;
  }

  /*
   * Check if user token is valid or user needs to resign.
   *
   *
   * @return true if valid else false.
   */

  function notes_login() {
      global $USER, $DB;
      // get user record from database , check if token exists and is valid
      $evernote = $DB->get_record("monorail_evernote_users", array('userid'=>$USER->id));
      $error = false;
      if($evernote) {
          $oauthtoken = $evernote->oauthtoken;
          $userStoreHttpClient = new THttpClient(NOTESTORE_HOST, NOTESTORE_PORT, "/edam/user", NOTESTORE_PROTOCOL);
          $userStoreProtocol = new TBinaryProtocol($userStoreHttpClient);
          $userStore = new UserStoreClient($userStoreProtocol, $userStoreProtocol);
          try {
              $userinfo = $userStore->getUser($oauthtoken);//Does not return any error!
              $_SESSION['accessToken'] = $oauthtoken;
              setSessionParams();
              $parts = parseUrl();
              $noteStoreTrans = new THttpClient($parts['host'], $parts['port'], $parts['path'], $parts['scheme']);
              $noteStoreProt = new TBinaryProtocol($noteStoreTrans);
              $noteStore = new NoteStoreClient($noteStoreProt, $noteStoreProt);
              $notebooks = $noteStore->listNotebooks($oauthtoken);
          } catch (EDAMUserException $e) {
              if(($e->errorCode == 3) || ($e->errorCode == 9)) // PERMISSION_DENIED/AUTH_EXPIRED
                  $error = true ; // Maybe redirect to get credentials!
          } catch (Exception $e) {
              $lastError = 'Error listing notebooks: ' . $e->getMessage();
              $error = true;
          }


      } else {
          return false;
      }
      if($error){
          unset($_SESSION['accessToken']);
          unset($_SESSION['noteStoreUrl']);
          return false; 
      }
      $_SESSION['accessToken'] = $evernote->oauthtoken;
      return true;
  }

  function setSessionParams() {
      if (!isset($_SESSION['noteStoreUrl']) && isset($_SESSION['accessToken'])) {
              // we are using the stored authtoken need to get the url
              $userStoreHttpClient = new THttpClient(NOTESTORE_HOST, NOTESTORE_PORT, "/edam/user", NOTESTORE_PROTOCOL);
              $userStoreProtocol = new TBinaryProtocol($userStoreHttpClient);
              $userStore = new UserStoreClient($userStoreProtocol, $userStoreProtocol);
              $_SESSION['noteStoreUrl'] = $userStore->getNoteStoreUrl($_SESSION['accessToken']);
      }
  }

  function parseUrl() {
      $parts = parse_url($_SESSION['noteStoreUrl']);
      if (!isset($parts['port'])) {
          if ($parts['scheme'] === 'https') {
              $parts['port'] = 443;
          } else {
              $parts['port'] = 80;
          }
      }
      return $parts;
  }
  /*
  *  Get list of notes for particular notebook with offset and max notes to fetch.
  *
  * @return array of notes with id,title,created details
  */
  function listNotes($notebookid, $offset, $maxnotes) {
      global $lastError, $currentStatus;
      $notes = array();
      try {

          setSessionParams();
          $parts = parseUrl();


          $noteStoreTrans = new THttpClient($parts['host'], $parts['port'], $parts['path'], $parts['scheme']);
          $noteStoreProt = new TBinaryProtocol($noteStoreTrans);
          $noteStore = new NoteStoreClient($noteStoreProt, $noteStoreProt);

          $authToken = $_SESSION['accessToken'];
          $filter = new \EDAM\NoteStore\NoteFilter();
          $filter->notebookGuid = $notebookid;
          $notesmetalist = $noteStore->findNotes($authToken, $filter, $offset, $maxnotes);
          $result = array();
          $result['totalnotes'] =  $notesmetalist->totalNotes;
          $notesmetadata = $notesmetalist->notes;
          if (!empty($notesmetadata)) {
              foreach ($notesmetadata as $notemeta) {
                  $note  = new stdClass();
                  $note->id   = $notemeta->guid;
                  $note->name = $notemeta->title;
                  $note->timestamp =  $notemeta->created;
                  $notes[] = $note;
              }
          }
          $result['notes'] = $notes;
          $currentStatus = 'Successfully listed content owner\'s notes';
          return $result;
      } catch (EDAMSystemException $e) {
          if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
              $lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
          } else {
              $lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
          }
      } catch (EDAMUserException $e) {
          if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
              $lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
          } else {
              $lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
          }
      } catch (EDAMNotFoundException $e) {
          if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
              $lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
          } else {
              $lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
          }
      } catch (Exception $e) {
          $lastError = 'Error listing notes: ' . $e->getMessage();
      }

      return $notes;
  }

    /*
   *  Get particular note with id.
   *
   *
   * @return note data title,content, created time for note on success else empty
   */
  function getNote($guid) {
    global $lastError, $currentStatus;

    try {
      setSessionParams();
      $parts = parseUrl();

      $noteStoreTrans = new THttpClient($parts['host'], $parts['port'], $parts['path'], $parts['scheme']);
      $noteStoreProt = new TBinaryProtocol($noteStoreTrans);
      $noteStore = new NoteStoreClient($noteStoreProt, $noteStoreProt);

      $authToken = $_SESSION['accessToken'];
      $notedata = $noteStore->getNote($authToken, $guid, true, true, true, false);

      if (!empty($notedata)) {
/*
           $content = $notedata->content;
           preg_match('/<en-note(.*?)>(.*?)<\/en-note>/s',$content,$match);
           $body = $match[2];
           preg_match_all('/<en-media(.*?)>(.*?)<\/en-media>/s', $body, $media_matches);
           foreach ($media_matches[0] as $media_match) {
               $body = str_replace($media_match, '', $body);
           }
           preg_match_all('/<en-media(.*?)\/>/', $body, $media_matches);
           foreach ($media_matches[0] as $media_match) {
             $body = str_replace($media_match, '', $body);
           }
           $notedata->content = $body;*/
      }
      $currentStatus = 'Successfully listed content owner\'s notes';
      return $notedata;
    } catch (EDAMSystemException $e) {
      if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
        $lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
      } else {
        $lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
      }
    } catch (EDAMUserException $e) {
      if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
        $lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
      } else {
        $lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
      }
    } catch (EDAMNotFoundException $e) {
      if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
        $lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
      } else {
        $lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
      }
    } catch (Exception $e) {
      $lastError = 'Error listing notes: ' . $e->getMessage();
    }
    return array();
  }

  function notes_fetch_all_notes_from_notebooks($notebookid) {
  	global $lastError, $currentStatus;
  	try {
  	    setSessionParams();
  	    $parts = parseUrl();

  		$noteStoreTrans = new THttpClient($parts['host'], $parts['port'], $parts['path'], $parts['scheme']);
  		$noteStoreProt = new TBinaryProtocol($noteStoreTrans);
  		$noteStore = new NoteStoreClient($noteStoreProt, $noteStoreProt);

  		$authToken = $_SESSION['accessToken'];
  		$filter = new \EDAM\NoteStore\NoteFilter();
  		$filter->notebookGuid = $notebookid;

  		$totalnotes = $noteStore->findNoteCounts($authToken, $filter, false);
  		$count = $totalnotes->notebookCounts[$notebookid];

                $filter->order = NoteSortOrder::UPDATED; 
                $filter->ascending = false;
  		$notesmetalist = $noteStore->findNotes($authToken, $filter, 0, $count);
  		$result = array();
  		$result['totalnotes'] =  $notesmetalist->totalNotes;
  		$notesmetadata = $notesmetalist->notes;
  		if (!empty($notesmetadata)) {
  			foreach ($notesmetadata as $notemeta) {
  				$note  = new stdClass();
  				$note->id   = $notemeta->guid;
  				$note->name = $notemeta->title;
                                $note->timestamp = substr($notemeta->created, 0, -3);
                                $notes[] = $note;
  			}
  		}
  		$result['notes'] = $notes;
  		$currentStatus = 'Successfully listed content owner\'s notes';
  		return $notes;
  	} catch (EDAMSystemException $e) {
  		if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
  			$lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
  		} else {
  			$lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
  		}
  	} catch (EDAMUserException $e) {
  		if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
  			$lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
  		} else {
  			$lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
  		}
  	} catch (EDAMNotFoundException $e) {
  		if (isset(EDAMErrorCode::$__names[$e->errorCode])) {
  			$lastError = 'Error listing notes: ' . EDAMErrorCode::$__names[$e->errorCode] . ": " . $e->parameter;
  		} else {
  		    $lastError = 'Error listing notes: ' . $e->getCode() . ": " . $e->getMessage();
  		}
  	} catch (Exception $e) {
  	    $lastError = 'Error listing notes: ' . $e->getMessage();
  	}
  }


  function notes_fetch_note_by_id($note_id) {
      global $CFG,$USER, $DB;
      $notedata = getNote($note_id);
      $resources = $notedata->resources;
//      $dirpath =  $CFG->httpswwwroot."/temp/files/".$USER->id."/evernote/";
      $contentbody = '';
      if(!empty($resources)){
          $context = get_context_instance(CONTEXT_USER, $USER->id);
          $token = $DB->get_record('external_tokens', array('userid'=>$USER->id));
          //Delete any existiung files of this user in evernote
          $fs = get_file_storage();
          $fs->delete_area_files($context->id, 'evernote_userfile', 'evernote_files', $USER->id);
          foreach($resources as $resource){
              $contentbody = $contentbody.'<body><br>';
              $rsc = new EDAM\Types\Resource();
              $data = new EDAM\Types\Data();
              $attr = new EDAM\Types\ResourceAttributes();
              $rsc = $resource;
              $data = $rsc->data;
              $attr = $rsc->attributes;
                // Prepare file record object
                $fileinfo = array(
                    'contextid' => $context->id,
                    'component' => 'evernote_userfile',
                    'filearea' => 'evernote_files',
                    'itemid' => $USER->id,
                    'userid' => $USER->id,
                    'filepath' => '/',
                    'filename' => $attr->fileName,
                    'name' => $attr->fileName,
              );
              try {
                  $file = $fs->create_file_from_string($fileinfo, $data->body);
                  $url = file_encode_url("$CFG->wwwroot/local/monorailservices/pluginfile.php?token=".$token->token."&file=path=",$file->get_pathnamehash(),false);
                  if (strlen(stristr($rsc->mime,'image'))>0) {
                      $contentbody = $contentbody.'<img style="width:80%" src='.$url.'><br>';
                  } else if(strlen(stristr($rsc->mime,'pdf'))>0){
                      $urlparts = parse_url($url);
                      $pdfurl = str_replace($urlparts['scheme'].'://'.$urlparts['host'], '', $url);
                      $contentbody =  $contentbody.'<a href="/pdfjs/web/viewer.html?original='.$pdfurl.'&file='.$pdfurl.'">'.$attr->fileName.'</a><br>';
                  } else {
                      $contentbody = $contentbody.'<embed src='.$url.' type='.$rsc->mime. '><br>';
                  }
              } catch (Exception $e) { 
                  //Some problem with resource move to next one
                  add_to_log(1, 'monorail', 'lib.evernote.resource', '', 'Error! Failed to create rsource: '.get_class($e).': '.$e->getMessage());
                  continue;
              }
          }
      }
      if($contentbody != '')
          $contentbody = $contentbody.'</body><br>';
      $note = new stdClass();
      $note->id = $notedata->guid;
      $note->name = $notedata->title;
      $note->timestamp = substr($notedata->created, 0, -3);
      $note->content = $notedata->content.$contentbody;

        return $note;
  }
  /*
   * Reset the current session.
   */
  function resetSession() {
    if (isset($_SESSION['requestToken'])) {
      unset($_SESSION['requestToken']);
    }
    if (isset($_SESSION['requestTokenSecret'])) {
      unset($_SESSION['requestTokenSecret']);
    }
    if (isset($_SESSION['oauthVerifier'])) {
      unset($_SESSION['oauthVerifier']);
    }
    if (isset($_SESSION['accessToken'])) {
      unset($_SESSION['accessToken']);
    }
    if (isset($_SESSION['accessTokenSecret'])) {
      unset($_SESSION['accessTokenSecret']);
    }
    if (isset($_SESSION['noteStoreUrl'])) {
      unset($_SESSION['noteStoreUrl']);
    }
    if (isset($_SESSION['webApiUrlPrefix'])) {
      unset($_SESSION['webApiUrlPrefix']);
    }
    if (isset($_SESSION['tokenExpires'])) {
    	unset($_SESSION['tokenExpires']);
    }
    if (isset($_SESSION['userId'])) {
    	unset($_SESSION['userId']);
    }
    if (isset($_SESSION['notebooks'])) {
      unset($_SESSION['notebooks']);
    }
    if (isset($_SESSION['noteStoreUrl'])) {
        unset($_SESSION['noteStoreUrl']);
    }
  }

  /*
   * Get the URL of this application. This URL is passed to the server (Evernote)
   * while obtaining unauthorized temporary credentials (step 1). The resource owner
   * is redirected to this URL after authorizing the temporary credentials (step 2).
   */
  function getCallbackUrl() {
  	global $CFG;
    //$thisUrl = (empty($_SERVER['HTTPS'])) ? "http://" : "https://";
    //$thisUrl .= $_SERVER['SERVER_NAME'];
    //$thisUrl .= ($_SERVER['SERVER_PORT'] == 80 || $_SERVER['SERVER_PORT'] == 443) ? "" : (":".$_SERVER['SERVER_PORT']);
    $thisUrl  = $CFG->wwwroot."/theme/monorail/notes.php"; //$_['SCRIPT_NAME'];
    $thisUrl .= '?action=callback';
    return $thisUrl;
  }

  /*
   * Get the Evernote server URL used to authorize unauthorized temporary credentials.
   */
  function getAuthorizationUrl() {
    $url = AUTHORIZATION_URL;
    $url .= '?oauth_token=';
    $url .= urlencode($_SESSION['requestToken']);
    return $url;
  }
?>
