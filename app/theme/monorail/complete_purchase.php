<?php
/**
 * Monorail theme
 * 
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

/* Complete purchase process: users who already paid for course are enroled
 * to the course by opening this link. */

define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once(dirname(__FILE__) . '/sociallib.php');

global $DB, $CFG, $USER;

// check login
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}

try {
    $courseCode = required_param('course', PARAM_TEXT);
    $courseId = $DB->get_field('monorail_course_data', 'courseid', array('code' => $courseCode), MUST_EXIST);
    $payment = $DB->get_record('monorail_course_payments', array('userid' => $USER->id, 'courseid' => $courseId), "*", MUST_EXIST);
} catch (Exception $ex)
{
    exit(0);
}

if (!$payment->active)
{
    // User has not paid for the course.
    exit(0);
}

require_once($CFG->libdir . '/enrollib.php');

$enrol = enrol_get_plugin('manual');
$enrolinstances = enrol_get_instances($courseId, true);

foreach ($enrolinstances as $courseenrolinstance)
{
    if ($courseenrolinstance->enrol == "manual")
    {
        $instance = $courseenrolinstance;
        break;
    }
}

if ($instance)
{
    $roleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST);
    $enrol->enrol_user($instance, $USER->id, $roleid, time(), 0, ENROL_USER_ACTIVE);
    social_share_course_enroll($courseId, $USER->id);
    $payment->active = 0;
    $DB->update_record("monorail_course_payments", $payment);

    // XXX: is this used anywhere?
    monorail_data("ADDRESS", $USER->id, "user_address", json_encode($_POST));

    require_once __DIR__ . "/../../local/monorailservices/cachelib.php";

    wscache_reset_by_dependency("course_info_" . $courseId);
    wscache_reset_by_dependency("user_courses_" . $USER->id);

    $courseInfo = $DB->get_record_sql("SELECT c.fullname AS fullname, mcd.mainteacher AS mainteacher " .
        "FROM {course} AS c INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=c.id WHERE c.id=?", array($courseId));

    // Send receipt.
    require_once __DIR__ . "/lib.php";
    $subject = "Thank you for your purchase";
    $amount = $payment->totalprice;
    $tax = $payment->totaltax;

    $body = monorail_template_compile(monorail_get_template('mail/base'), array(
       'title' => $subject,

       'body' => monorail_template_compile(monorail_get_template("mail/courseinvoice"),
            array(
                "COURSENAME" => $courseInfo->fullname,
                "SUBTOTAL" => number_format($amount - $tax, 2),
                "VAT" => number_format($tax, 2),
                "TOTAL" => number_format($amount, 2),
                "INVOICENUMBER" => $payment->id,
                "INVOICEDATE" =>  date("j F Y", $payment->timestamp),
            )),

       'footer_additional_link' => '',
       'block_header' => '',
       "username" => $DB->get_field_sql("SELECT firstname FROM {user} WHERE id=?", array($USER->id))
    ), "EN");

    monorail_send_email('receipt', $subject, $body, $USER->id, "EN");

    // Send info to teacher.
    monorail_send_info_mail('mail/coursepurchasenotification',
        str_replace("{%name%}", $courseInfo->fullname, get_string('email_order_notification_subject', 'theme_monorail')),
        array(
            "COURSE_NAME" => $courseInfo->fullname,
            "TOTALPRICE" => number_format($amount, 2),
            "VAT" => number_format($tax, 2),
            "STUDENT_NAME" => $DB->get_field_sql("SELECT CONCAT(firstname, ' ', lastname) FROM {user} WHERE id=?", array($USER->id)),
            "STUDENT_EMAIL" => $DB->get_field_sql("SELECT email FROM {user} WHERE id=?", array($USER->id))
        ),
        $courseInfo->mainteacher);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title></title>
 <meta http-equiv="refresh" content="1;URL=<?php echo $CFG->magic_ui_root . "/courses/" . $courseCode ?>">
</head>
<body>
<!-- Google Code for Purchase Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 999934223;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "nZq0CIm6oAgQj5Ln3AM";
var google_conversion_value = 10.00;
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/999934223/?value=10.00&amp;label=nZq0CIm6oAgQj5Ln3AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
<?php

if ($instance)
{
    fastcgi_finish_request();
    $DB->dispose();

    if (pcntl_fork())
    {
        exit(0);
    }

    unset($DB);
    setup_DB();

    if (isset($CFG->mage_api_url))
    {
        require_once __DIR__ . "/../../local/monorailservices/mageapi.php";
        $api = new MageApi($CFG->mage_api_url);

        if ($api->connect($CFG->mage_api_user, $CFG->mage_api_key))
        {
            $api->updateParticipants($courseId);
            $api->disconnect();
        }
    }
}
?>
