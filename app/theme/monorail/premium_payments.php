<?php
/**
 * Monorail theme
 *
 * @package   monorail
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once __DIR__ . "/braintree/lib/Braintree.php";
require_once __DIR__ . "/lib.php";

global $CFG;

Braintree_Configuration::environment($CFG->braintree_environment);
Braintree_Configuration::merchantId($CFG->braintree_merchantId);
Braintree_Configuration::publicKey($CFG->braintree_publicKey);
Braintree_Configuration::privateKey($CFG->braintree_privateKey);

function premium_payments_process()
{
    global $DB;

    $cohorts = $DB->get_records_sql("SELECT mci.id AS id, mci.cohortid AS cohortid, c.name AS name FROM {monorail_cohort_info} AS mci " .
        "INNER JOIN {cohort} AS c ON c.id=mci.cohortid " .
        "WHERE mci.company_status=2 AND mci.next_billing<UNIX_TIMESTAMP()");

    $results = "";
    $hasErrors = false;

    // FIXME: seems that braintree can only process one payment per run.
    // Consequent payments fail with unknown errors. But it's kind of ok,
    // because we try to get that money many times, until we get it...

    foreach ($cohorts as $cohort) {
        $status = premium_payments_process_payment($cohort->cohortid);

        $results .= $cohort->name . " (http://eliademy.com/app/admin/tool/monorail_admin_tool/admin_e4b.php?cid=" . $cohort->cohortid . "):\n";

        if ($status["ok"]) {
            $results .= " SUCCESS: ";
        } else {
            $results .= " FAILURE: ";
            $hasErrors = true;
        }

        $results .= $status["msg"] . "\n\n";
    }

    if (!empty($results)) {
        mail("aurelijus@cloudberrytec.com,info@cloudberrytec.com",
            "Premium payment collection report" . ($hasErrors ? " (CONTAINS ERRORS!)" : ""),
            $results);
        //error_log($results);
    }
}

function premium_payments_process_payment($cohortid)
{
    global $DB, $CFG;

    $price = 0;
    $entries = premium_payments_calculate_payment($cohortid);

    foreach ($entries as $ent) {
        $price += $ent["count"] * $ent["price"];
    }

    $price = round($price, 2);

    //$price = premium_payments_calculate_users($cohortid);
    //$entries = array();

    if ($price == 0) {
        premium_payments_register_payment($cohortid, $entries, 0, 0, null);

        return array(
            "ok" => true,
            "msg" => "Don't need to pay this time (has enough prepaid licences)."
        );
    }

    $admins = $DB->get_recordset_sql("SELECT id, userid FROM {monorail_cohort_admin_users} " .
        "WHERE cohortid=? AND usertype=1", array($cohortid));

    $paymentMethod = null;
    $bt_userid = null;
    $userid = null;

    foreach ($admins as $admin) {
        $userid = $admin->userid;
        $bt_userid = str_replace(".", "_", $CFG->my_host) . "-" . $admin->userid;
        $paymentMethod = premium_payments_get_payment_method($bt_userid);

        if ($paymentMethod) {
            break;
        }
    }

    try {
        $billing_details = json_decode($DB->get_field_sql("SELECT billing_details FROM {monorail_cohort_info} WHERE cohortid=?",
            array($cohortid)));
    } catch (Exception $err) { }

    if ($paymentMethod) {
        $tax = premium_payments_get_vat_amount($cohortid, $price);

        $result = Braintree_Transaction::sale(array(
            'amount' => $price + $tax,
            'taxAmount' => $tax,
            'customerId' => $bt_userid,
            'paymentMethodToken' => $paymentMethod,
            'options' => array(
                'submitForSettlement' => true
            ),
            'customFields' => array(
                  'itemcode' => 'premium',
                  'country' => @$billing_details->country
            )
        ));

        if ($result->success) {
            premium_payments_register_payment($cohortid, $entries, $price, $tax, $userid);

            return array(
                "ok" => true,
                "msg" => "Payment successful! (total: " . ($price + $tax) .")"
            );
        } else {
            premium_payments_register_payment_error($cohortid, $userid);

            return array(
                "ok" => false,
                "msg" => "Error while processing payment!"
            );
        }
    } else {
        premium_payments_register_payment_error($cohortid, $userid);

        return array(
            "ok" => false,
            "msg" => "No valid payment method found!"
        );
    }
}

function premium_payments_register_payment($cohortid, $entries, $totalprice, $totaltax, $userid)
{
    global $DB;

    $cinfo = $DB->get_record("monorail_cohort_info", array("cohortid" => $cohortid));
    $cinfo->next_billing = strtotime("+1 month midnight");
    $DB->update_record("monorail_cohort_info", $cinfo);

    $inv = new stdClass();

    $inv->cohortid = $cohortid;
    $inv->invoicenum = ((int) $DB->get_field_sql("SELECT MAX(invoicenum) FROM {monorail_cohort_invoice}")) + 1;
    $inv->timeissued = time();
    $inv->timepaid = $inv->timeissued;
    $inv->paidby = $userid;
    $inv->paymentstatus = 2;
    $inv->invoicetype = 1;
    $inv->quantity = 1;
    $inv->unitprice = $totalprice;
    $inv->totaltax = $totaltax;
    $inv->totalprice = $totalprice + $totaltax;

    $invid = $DB->insert_record("monorail_cohort_invoice", $inv);

    foreach ($entries as $ent) {
        $invEntry = new stdClass();

        $invEntry->invoiceid = $invid;
        $invEntry->usertype = $ent["type"];
        $invEntry->datefrom = $ent["start"];
        $invEntry->dateto = $ent["end"];
        $invEntry->quantity = $ent["count"];
        $invEntry->unitprice = $ent["price"];
        $invEntry->totalprice = $ent["count"] * $ent["price"];
        // VAT per item?
        //$invEntry->totaltax = -1;

        $DB->insert_record("monorail_cohort_inv_entry", $invEntry);
    }

    if ($totalprice > 0) {
        $subject = get_string_manager()->get_string('email_subject_invoice', 'theme_monorail', null, "EN");

        $body = monorail_template_compile(monorail_get_template('mail/base'), array(
           'title' => $subject,
           'body' => monorail_template_compile(monorail_get_template("mail/cohortinvoice"),
                array("INVOICENUM" => $inv->invoicenum, "DATE" => date("F j, Y"), "AMOUNT" => ($totalprice + $totaltax), "INVOICEID" => $invid)),
           'footer_additional_link' => '',
           'block_header' => '',
           "username" => $DB->get_field_sql("SELECT firstname FROM {user} WHERE id=?", array($userid))
        ), "EN");

        monorail_send_email('receipt', $subject, $body, $userid, "EN");
    }

    // Reset invalid payment notification state for cohort.
    $DB->execute("DELETE FROM {monorail_data} WHERE type='PAYNOTIF' AND itemid=?", array($cohortid));
}

function premium_payments_register_payment_error($cohortid, $userid)
{
    global $DB;

    $notificationSent = $DB->get_field_sql("SELECT value FROM {monorail_data} WHERE datakey=? AND itemid=?",
        array("invalid_premium_payment_user-" . $userid, $cohortid));

    if (!$notificationSent) {
        // Save invalid payment notification state for user.
        $DB->execute("INSERT INTO {monorail_data} (type, itemid, datakey, value, timemodified) VALUES (?, ?, ?, ?, ?)",
            array("PAYNOTIF", $cohortid, "invalid_premium_payment_user-" . $userid, "1", time()));

        $subject = get_string_manager()->get_string('email_subject_invoice_error', 'theme_monorail', null, "EN");

        $body = monorail_template_compile(monorail_get_template('mail/base'), array(
           'title' => $subject,
           'body' => monorail_template_compile(monorail_get_template("mail/cohortinvoicefailed"), array()),
           'footer_additional_link' => '',
           'block_header' => '',
           "username" => $DB->get_field_sql("SELECT firstname FROM {user} WHERE id=?", array($userid))
        ), "EN");

        monorail_send_email('receipt', $subject, $body, $userid, "EN");
    }
}

function premium_payments_get_vat_amount($cohortid, $amount)
{
    global $CFG, $DB;

    $billing_details = json_decode($DB->get_field_sql("SELECT billing_details FROM {monorail_cohort_info} WHERE cohortid=?", array($cohortid)));

    if (isset($billing_details->country) && isset($CFG->vat_percent[$billing_details->country]))
    {
        $vat_percent = $CFG->vat_percent[$billing_details->country];

        if ($billing_details->country != "FI" && $billing_details->vat_id)
        {
            $vat = $billing_details->vat_id;
            $clen = strlen($billing_details->country);

            if (strtolower(substr($vat, 0, $clen)) == strtolower($billing_details->country)) {
                $vat = substr($vat, $clen);
            }

            try {
                $text = trim(file_get_contents("http://isvat.appspot.com/" . $billing_details->country . "/" . $vat . "/"));
            } catch (Exception $e) {
                $text = "???";
            }

            if ($text != "false") {
                $vat_percent = 0;
            }
        }

        if ($vat_percent) {
            return round($amount * $vat_percent / 100, 2);
        }
    }

    return 0;
}

function premium_payments_calculate_payment($cohortid)
{
    global $DB;

    $cohortinfo = $DB->get_record("monorail_cohort_info", array("cohortid" => $cohortid));

    // 0. Count how many and what kind of users we had during last billing
    // period...

    $endDate = strtotime("midnight", $cohortinfo->next_billing);
    $startDate = strtotime("-1 month", $endDate);
    $endDate = strtotime("-1 day", $endDate);
    $day = $endDate;
    $firstCount = null;
    $userCounts = array();

    while ($day >= $startDate) {
        $users = $DB->get_recordset_sql("SELECT id, usertype FROM {monorail_cohort_daily_users} " .
            "WHERE cohortid=? AND timestamp=?", array($cohortid, $day));

        $usercount = array(
            "owners" => 0,
            "admins" => 0,
            "students" => 0
        );

        foreach ($users as $usr) {
            switch ($usr->usertype) {
                case 1:
                    $usercount["owners"]++;
                    break;

                case 2: case 3: case 4:
                    $usercount["admins"]++;
                    break;

                default:
                    $usercount["students"]++;
            }
        }

        if ($usercount["owners"] > 0) {
            if ($firstCount == null) {
                $firstCount = $usercount;
            }

            $userCounts[$day] = $usercount;
        } else {
            $userCounts[$day] = null;
        }

        $day = strtotime("-1 day", $day);
    }

    ksort($userCounts);

    $entries = array();

    // 1. Make owner entry - should always be one owner (if not, there is
    // an error somewhere).

    if ((int) $cohortinfo->userscount > 0 && $cohortinfo->valid_until >= $startDate) {
        if ($cohortinfo->valid_until < $endDate) {
            // Prepaid licences exipred during billing month
            $entires[] = array("start" => $startDate, "end" => strtotime("midnight", $cohortinfo->valid_until), "type" => 1, "count" => 1, "price" => 0);
            $entires[] = array("start" => strtotime("+1 day midnight", $cohortinfo->valid_until), "end" => $endDate, "type" => 1, "count" => 1,
                "price" => premium_payments_get_period_price(5, $startDate, $endDate, strtotime("+1 day midnight", $cohortinfo->valid_until), $endDate));
        } else {
            $entires[] = array("start" => $startDate, "end" => $endDate, "type" => 1, "count" => 1, "price" => 0);
        }
    } else {
        $entires[] = array("start" => $startDate, "end" => $endDate, "type" => 1, "count" => 1, "price" => 5);
    }

    // 2. Make entries for admins - no payment (admins are free for now (bug 2398)), just for user information.

    $lastAdminCount = 0;
    $lastAdminCountDay = null;

    foreach ($userCounts as $day => $cnt) {
        if ($cnt == null) {
            $cnt = $firstCount;
        }

        if ($lastAdminCount != $cnt["admins"]) {
            if ($lastAdminCountDay != null) {
                $entires[] = array("start" => $lastAdminCountDay, "end" => $day, "type" => 2, "count" => $lastAdminCount, "price" => 0);
            }

            $lastAdminCount = $cnt["admins"];
            $lastAdminCountDay = $day;
        }
    }

    if ($lastAdminCountDay != null && $lastAdminCount > 0) {
        $entires[] = array("start" => $lastAdminCountDay, "end" => $endDate, "type" => 2, "count" => $lastAdminCount, "price" => 0);
    }

    // 3. Make entires for students... Teh fun part.

    $lastStudentCount = 0;
    $lastStudentCountDay = null;

    foreach ($userCounts as $day => $cnt) {
        if ($cnt == null) {
            $cnt = $firstCount;
        }

        if ($lastStudentCount != $cnt["students"]) {
            if ($lastStudentCountDay != null) {
                if ((int) $cohortinfo->userscount > 1 && $cohortinfo->valid_until >= $lastStudentCountDay) {
                    if ($cohortinfo->valid_until < $day) {
                        // Prepaid licences exipred during billing month
                        $licenceEndDay = strtotime("midnight", $cohortinfo->valid_until);
                        $licenceEndNextDay = strtotime("+1 day midnight", $cohortinfo->valid_until);

                        if ($cohortinfo->userscount <= $lastStudentCount) {
                            // Must pay for some students
                            $entires[] = array("start" => $lastStudentCountDay, "end" => $licenceEndDay, "type" => 3, "count" => $cohortinfo->userscount - 1, "price" => 0);

                            $entires[] = array("start" => $lastStudentCountDay, "end" => $licenceEndDay, "type" => 3, "count" => ($lastStudentCount - $cohortinfo->userscount) + 1,
                                "price" => premium_payments_get_period_price(1, $startDate, $endDate, $lastStudentCountDay, $licenceEndDay));
                        } else {
                            // Enough licences for all students.
                            $entires[] = array("start" => $lastStudentCountDay, "end" => $licenceEndDay, "type" => 3,
                                "count" => $lastStudentCount, "price" => 0);
                        }

                        // And for the rest of period... full price
                        $entires[] = array("start" => $licenceEndNextDay, "end" => $day, "type" => 3, "count" => $lastStudentCount,
                            "price" => premium_payments_get_period_price(1, $startDate, $endDate, $licenceEndNextDay, $day));

                    } else {
                        // Prepaid licences valid through entire period

                        if ($cohortinfo->userscount <= $lastStudentCount) {
                            // Must pay for some students
                            $entires[] = array("start" => $lastStudentCountDay, "end" => $day, "type" => 3, "count" => $cohortinfo->userscount - 1, "price" => 0);

                            $entires[] = array("start" => $lastStudentCountDay, "end" => $day, "type" => 3, "count" => ($lastStudentCount - $cohortinfo->userscount) + 1,
                                "price" => premium_payments_get_period_price(1, $startDate, $endDate, $lastStudentCountDay, $day));
                        } else {
                            // Enough licences for all students
                            $entires[] = array("start" => $lastStudentCountDay, "end" => $day, "type" => 3, "count" => $lastStudentCount, "price" => 0);
                        }
                    }
                } else {
                    // No prepaid licences

                    $entires[] = array("start" => $lastStudentCountDay, "end" => $day, "type" => 3, "count" => $lastStudentCount,
                        "price" => premium_payments_get_period_price(1, $startDate, $endDate, $lastStudentCountDay, $day));
                }
            }

            $lastStudentCount = $cnt["students"];
            $lastStudentCountDay = $day;
        }
    }

    if ($lastStudentCountDay != null && $lastStudentCount > 0) {
        if ((int) $cohortinfo->userscount > 1 && $cohortinfo->valid_until >= $lastStudentCountDay) {
            if ($cohortinfo->valid_until < $endDate) {
                // Prepaid licences exipred during billing month
                $licenceEndDay = strtotime("midnight", $cohortinfo->valid_until);
                $licenceEndNextDay = strtotime("+1 day midnight", $cohortinfo->valid_until);

                if ($cohortinfo->userscount <= $lastStudentCount) {
                    // Must pay for some students
                    $entires[] = array("start" => $lastStudentCountDay, "end" => $licenceEndDay, "type" => 3, "count" => $cohortinfo->userscount - 1, "price" => 0);

                    $entires[] = array("start" => $lastStudentCountDay, "end" => $licenceEndDay, "type" => 3, "count" => ($lastStudentCount - $cohortinfo->userscount) + 1,
                        "price" => premium_payments_get_period_price(1, $startDate, $endDate, $lastStudentCountDay, $licenceEndDay));
                } else {
                    // Enough licences for all students.
                    $entires[] = array("start" => $lastStudentCountDay, "end" => $licenceEndDay, "type" => 3,
                        "count" => $lastStudentCount, "price" => 0);
                }

                // And for the rest of period... full price
                $entires[] = array("start" => $licenceEndNextDay, "end" => $endDate, "type" => 3, "count" => $lastStudentCount,
                    "price" => premium_payments_get_period_price(1, $startDate, $endDate, $licenceEndNextDay, $endDate));

            } else {
                // Prepaid licences valid through entire period

                if ($cohortinfo->userscount <= $lastStudentCount) {
                    // Must pay for some students
                    $entires[] = array("start" => $lastStudentCountDay, "end" => $endDate, "type" => 3, "count" => $cohortinfo->userscount - 1, "price" => 0);

                    $entires[] = array("start" => $lastStudentCountDay, "end" => $endDate, "type" => 3, "count" => ($lastStudentCount - $cohortinfo->userscount) + 1,
                        "price" => premium_payments_get_period_price(1, $startDate, $endDate, $lastStudentCountDay, $endDate));
                } else {
                    // Enough licences for all students
                    $entires[] = array("start" => $lastStudentCountDay, "end" => $endDate, "type" => 3, "count" => $lastStudentCount, "price" => 0);
                }
            }
        } else {
            // No prepaid licences

            $entires[] = array("start" => $lastStudentCountDay, "end" => $endDate, "type" => 3, "count" => $lastStudentCount,
                "price" => premium_payments_get_period_price(1, $startDate, $endDate, $lastStudentCountDay, $endDate));
        }
    }

    return $entires;
}

function premium_payments_get_payment_method($bt_userid)
{
    try {
        $customer = Braintree_Customer::find($bt_userid);

        // Credit cards.
        foreach ($customer->creditCards as $i) {
            if ($i->default) {
                return $i->token;
            }
        }

        // Paypal accounts
        foreach ($customer->paypalAccounts as $i) {
            if ($i->default) {
                return $i->token;
            }
        }

        // Apple cards?
        /*
        foreach ($customer->applePayCards as $i) {
            if ($i->default) {
                return $i->token;
            }
        }
        */

        // And so on?

    } catch (Braintree_Exception_NotFound $no) {
    }

    return null;
}

function premium_payments_get_period_price($base, $start, $end, $from, $to) {
    $totalDays = ($end - $start) / 86400;
    $paidDays = ($to - $from) / 86400;

    return $base * ($paidDays / $totalDays);
}

/* Not used any more
function premium_payments_calculate_users($cohortid)
{
    global $DB;

    $cohortusers = $DB->get_fieldset_select("cohort_members", 'userid', "cohortid = ?", array($cohortid));

    $cohortextusers = $DB->get_fieldset_sql("SELECT DISTINCT ra.userid FROM {monorail_cohort_courseadmins} AS mcc " .
        "INNER JOIN {context} AS ctx ON ctx.instanceid=mcc.courseid " .
        "INNER JOIN {role_assignments} AS ra ON ra.contextid=ctx.id " .
        "INNER JOIN {monorail_course_perms} AS mcp ON mcp.course=mcc.courseid " .
            "WHERE mcc.cohortid=? AND ctx.contextlevel=50 AND mcp.privacy=2" .
                (empty($cohortusers) ? "" : " AND NOT ra.userid IN (" . implode(", ", $cohortusers) . ")"), array($cohortid));

    $cohortinfo = $DB->get_record("monorail_cohort_info", array("cohortid" => $cohortid));

    $endDate = $cohortinfo->next_billing;
    $startDate = strtotime("-1 month", $endDate);
    $maxUsers = count($cohortusers) + count($cohortextusers);

    if ($maxUsers < (int) $cohortinfo->peak_users) {
        $maxUsers = $cohortinfo->peak_users;
    }

    // Subtract prepaid licences.
    if ((int) $cohortinfo->userscount > 0) {
        if ($cohortinfo->valid_until >= $startDate) {
            $maxUsers -= $cohortinfo->userscount;

            if ($maxUsers < 0) {
                $maxUsers = 0;
            }
        }
    }

    return $maxUsers;
}
*/
