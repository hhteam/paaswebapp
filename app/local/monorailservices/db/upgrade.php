<?php

function xmldb_local_monorailservices_upgrade($oldversion = 0) {

    global $DB;

    if ($oldversion < 2013080201) {

        // Remove timezone force
        $conf = $DB->get_record('config', array('name'=>'forcetimezone'));
        $conf->value = '99';
        $DB->update_record('config', $conf);

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013080201, 'local', 'monorailservices');
    }

    if ($oldversion < 2013082100) {

        $DB->delete_records('monorail_data', array('type'=>'assign', 'datakey'=>'latesubmissionsuntil'));
        $DB->delete_records('monorail_data', array('type'=>'assign', 'datakey'=>'latesubmissionsdays'));

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013082100, 'local', 'monorailservices');
    }

    if ($oldversion < 2013082200) {

        // save monorail_data nosubmissions data in to assign table where it belongs
        $assigns = $DB->get_records('assign', array());
        foreach ($assigns as $assign) {
            $data = $DB->get_record('monorail_data', array('itemid'=>$assign->id, 'type'=>'assign', 'datakey'=>'nosubmissions'));
            if ($data) {
                // log just in case
                add_to_log($assign->course, 'monorail', 'upgrade', '2013082200', 'assign '.$assign->id.' set nosubmissions from '.$assign->nosubmissions.' to '.$data->value);
                $assign->nosubmissions = $data->value;
                $DB->update_record('assign', $assign);
            }
        }
        // delete old monorail_data nosubmissions keys
        $DB->delete_records('monorail_data', array('type'=>'assign', 'datakey'=>'nosubmissions'));

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013082200, 'local', 'monorailservices');
    }

    if ($oldversion < 2013090902) {

        // Fix invalid assignment submissions
        $submissions = $DB->get_records('assign_submission');
        foreach ($submissions as $submission) {
            $count = $DB->count_records('assign_submission', array('assignment'=>$submission->assignment, 'userid'=>$submission->userid));
            $files = $DB->get_record('assignsubmission_file', array('assignment'=>$submission->assignment, 'submission'=>$submission->id));
            $onlinetext = $DB->get_record('assignsubmission_onlinetext', array('assignment'=>$submission->assignment, 'submission'=>$submission->id));
            if ($count > 1 && ! $files && (! $onlinetext || $onlinetext->onlinetext == '')) {
                add_to_log(1, 'monorailservices', 'upgrade', '2013090902', 'Found invalid assign_submission row: id '.$submission->id.' assignment '.$submission->assignment.' userid '.$submission->userid);
                // not deleting automatically :P - instead print a ready to run delete statement to log
                add_to_log(1, 'monorailservices', 'upgrade', '2013090902-sql', 'delete from mdl_assign_submission where id = '.$submission->id.';');
            }
        }

        // monorail savepoint reached
        upgrade_plugin_savepoint(true, 2013090902, 'local', 'monorailservices');
    }

    return true;
}
