<?php
/**
 * Eliademy.com web services
 *invite_send
 * @package   monorail_services
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * Monorail web services
 * @package    local_monorailservices
 * @subpackage db
 */
$functions = array(
        'local_monorailservices_get_assignments' => array(
                'classname'   => 'local_monorailservices_external',
                'methodname'  => 'get_assignments',
                'classpath'   => 'local/monorailservices/externallib.php',
                'description' => 'Get the course assignments for the users capability',
                'type'        => 'read'
        ),
        'local_monorailservices_upd_assignments' => array(
                'classname'   => 'local_monorailservices_external',
                'methodname'  => 'update_assignments',
                'classpath'   => 'local/monorailservices/externallib.php',
                'description' => 'Updates the courses and assignments for the users capability',
                'type'        => 'write'
        ),
        'local_monorailservices_del_assignments' => array(
                'classname'   => 'local_monorailservices_external',
                'methodname'  => 'delete_assignments',
                'classpath'   => 'local/monorailservices/externallib.php',
                'description' => 'Delete assignments for user',
                'type'        => 'write'
        ),
        'local_monorailservices_getcrs_categories' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'course_categories',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get course categories',
            'type'        => 'read'
        ),
        'local_monorailservices_update_courses' => array(
                'classname'   => 'local_monorailservices_external',
                'methodname'  => 'update_courses',
                'classpath'   => 'local/monorailservices/externallib.php',
                'description' => 'Update course',
                'type'        => 'write',
                'capabilities'=> 'moodle/course:update,moodle/course:visibility',
        ),
        'local_monorailservices_upd_course_res' => array(
                'classname'   => 'local_monorailservices_external',
                'methodname'  => 'update_course_resources',
                'classpath'   => 'local/monorailservices/externallib.php',
                'description' => 'Update course resources',
                'type'        => 'write',
                'capabilities'=> 'moodle/course:update',
        ),
        'local_monorailservices_upd_course_sect' => array(
                'classname'   => 'local_monorailservices_external',
                'methodname'  => 'update_course_sections',
                'classpath'   => 'local/monorailservices/externallib.php',
                'description' => 'Update course sections',
                'type'        => 'write',
                'capabilities'=> 'moodle/course:update,moodle/course:manageactivities',
        ),
        'local_monorailservices_del_course_sect' => array(
                'classname'   => 'local_monorailservices_external',
                'methodname'  => 'delete_course_sections',
                'classpath'   => 'local/monorailservices/externallib.php',
                'description' => 'Delete course sections',
                'type'        => 'write',
                'capabilities'=> 'moodle/course:update,moodle/course:manageactivities',
        ),
        'local_monorailservices_course_editable' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'course_editable',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'check if course/activity editable by user',
            'type'        => 'read',
        ),
        'local_monorailservices_add_submissions' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_submissions',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'add file submissions',
            'type'        => 'write',
        ),
        'local_monorailservices_get_submissions' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_submissions',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'get file submissions',
            'type'        => 'read',
        ),
        'local_monorailservices_del_submissions' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'del_submissions',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'remove file submissions',
            'type'        => 'write',
        ),
        'local_monorailservices_create_forums' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'create_forums',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'create forums for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_update_forums' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_forums',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'update forums for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_del_forums' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'del_forums',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'remove forums for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_del_fpost' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_forum_post',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'remove post from forum discussion',
            'type'        => 'write',
        ),
        'local_monorailservices_del_fdiscussion' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_forum_discussion',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'remove discussion from forum - including all its posts',
            'type'        => 'write',
        ),
        // Borrowed from Moodle 2.5
        // Used by huxley
        'local_monorailservices_get_forums' => array(
            'classname' => 'local_monorailservices_external',
            'methodname' => 'get_forums_by_courses',
            'classpath' => 'local/monorailservices/externallib.php',
            'description' => 'Returns a list of forum instances in a provided set of courses, if
                no courses are provided then all the forum instances the user has access to will be
                returned.',
            'type' => 'read',
            'capabilities' => 'mod/forum:viewdiscussion'
        ),
        // Used by magic ui
        'local_monorailservices_forums' => array(
            'classname' => 'local_monorailservices_external',
            'methodname' => 'forums',
            'classpath' => 'local/monorailservices/externallib.php',
            'description' => 'Returns a list of forum instances in a provided set of courses, if
                no courses are provided then all the forum instances the user has access to will be
                returned.',
            'type' => 'read',
            'capabilities' => 'mod/forum:viewdiscussion'
        ),
        // Used by huxley
        'local_monorailservices_get_forum_posts' => array(
            'classname' => 'local_monorailservices_external',
            'methodname' => 'get_forum_posts',
            'classpath' => 'local/monorailservices/externallib.php',
            'description' => 'Returns a list of posts in the provided discussion.',
            'type' => 'read',
            'capabilities' => 'mod/forum:viewdiscussion'
        ),
        // Used by magic ui
        'local_monorailservices_forum_posts' => array(
            'classname' => 'local_monorailservices_external',
            'methodname' => 'forum_posts',
            'classpath' => 'local/monorailservices/externallib.php',
            'description' => 'Returns a list of posts in the provided discussion.',
            'type' => 'read',
            'capabilities' => 'mod/forum:viewdiscussion'
        ),
        'local_monorailservices_upd_forum_discussion' => array(
            'classname' => 'local_monorailservices_external',
            'methodname' => 'update_forum_discussion',
            'classpath' => 'local/monorailservices/externallib.php',
            'description' => 'Create/update topic in forum.',
            'type' => 'write',
            'capabilities' => 'mod/forum:viewdiscussion'
        ),
        'local_monorailservices_upd_forum_post' => array(
            'classname' => 'local_monorailservices_external',
            'methodname' => 'update_forum_post',
            'classpath' => 'local/monorailservices/externallib.php',
            'description' => 'Create/update post in forum.',
            'type' => 'write',
            'capabilities' => 'mod/forum:viewdiscussion'
        ),
        'local_monorailservices_create_filerscs' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'create_filerscs',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'create file resources for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_del_filerscs' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'del_filerscs',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'remove file resources for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_update_filename' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_filename',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update file name',
            'type'        => 'write',
        ),
        'local_monorailservices_add_assignfiles' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_assignfiles',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Add attachments to assignments',
            'type'        => 'write',
        ),
        'local_monorailservices_del_assignfiles' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'del_assignfiles',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete attachments to assignments',
            'type'        => 'write',
        ),
        'local_monorailservices_add_course_rurls' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_course_rurls',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'create course urls for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_upd_course_rurls' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'upd_course_rurls',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'update course urls for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_del_course_rurls' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'del_course_rurls',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'remove course urls for courses',
            'type'        => 'write',
        ),
        'local_monorailservices_get_page' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_page',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get page resource data',
            'type'        => 'read',
        ),
        'local_monorailservices_get_list_of_background_picture' => array(
        		'classname'   => 'local_monorailservices_external',
        		'methodname'  => 'get_list_of_background_picture',
        		'classpath'   => 'local/monorailservices/externallib.php',
        		'description' => 'Get the list of available background picture',
        		'type'        => 'read',
        ),
        'local_monorailservices_upd_ctxdata' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_ctxdata',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'update file resource contextual data',
            'type'        => 'write',
        ),
        'local_monorailservices_get_tasks_info' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_tasksubmissions',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'get task submissions',
            'type'        => 'read',
        ),
        'local_monorailservices_get_task_details' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_task_details',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'get task (or quiz, etc.) details',
            'type'        => 'read',
        ),
        'local_monorailservices_get_users_by_id' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_users_by_id',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get users by id.',
            'type'        => 'read',
            'capabilities'=> 'moodle/user:viewdetails, moodle/user:viewhiddendetails, moodle/course:useremail, moodle/user:update',
        ),
        'local_monorailservices_update_users' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_users',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update users.',
            'type'        => 'write',
            'capabilities'=> 'moodle/user:update',
        ),
        'local_monorailservices_update_grades' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_grades',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update grades.',
            'type'        => 'write'
        ),
        'local_monorailservices_assign_roles' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'assign_roles',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Assign role.',
            'type'        => 'write'
        ),
        'local_monorailservices_unassign_roles' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'unassign_roles',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Un assign role.',
            'type'        => 'write'
        ),
        'local_monorailservices_enrol_users' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'enrol_users',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Enrol users to course',
            'type'        => 'write'
        ),
        'local_monorailservices_unenrol_users' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'unenrol_users',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Un Enrol users to course',
            'type'        => 'write'
        ),
        'local_monorailservices_supported_langs' => array(
        		'classname'   => 'local_monorailservices_external',
        		'methodname'  => 'supported_langs',
        		'classpath'   => 'local/monorailservices/externallib.php',
        		'description' => 'Languages supported',
        		'type'        => 'read'
        ),
        'local_monorailservices_get_countries' => array(
        		'classname'   => 'local_monorailservices_external',
        		'methodname'  => 'get_countries',
        		'classpath'   => 'local/monorailservices/externallib.php',
        		'description' => 'Countries',
        		'type'        => 'read'
        ),
        'local_monorailservices_course_get_cont' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_contents',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get course contents, duplicate of core_course_get_contents',
            'type'        => 'read',
            'capabilities'=> 'moodle/course:update,moodle/course:viewhiddencourses'
        ),
        'local_monorailservices_set_enrollment' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'course_set_enrollment',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Set course enrollment status',
            'type'        => 'write'
        ),
        'local_monorailservices_get_invited' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_sent_invitations',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get invited emails for an invitation code',
            'type'        => 'read'
        ),
        'local_monorailservices_invite_send' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'invite_send',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Send an invite to an email',
            'type'        => 'write'
        ),
        'local_monorailservices_verify_captcha' => array(
            'classname' => 'local_monorailservices_external',
            'methodname' => 'verify_captcha',
            'classpath' => 'local/monorailservices/externallib.php',
            'description' => 'Verify the captcha',
            'type' => 'write'
        ),
        'local_monorailservices_create_courses' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'create_courses',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Create new courses',
            'type'        => 'write',
            'capabilities'=> 'moodle/course:create,moodle/course:visibility',
        ),
        'local_monorailservices_get_courses' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_courses',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return course details',
            'type'        => 'read'
//            'capabilities'=> 'moodle/course:view,moodle/course:update,moodle/course:viewhiddencourses',
        ),
        'local_monorailservices_get_submission' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_submission',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'get submission',
            'type'        => 'read',
        ),
        'local_monorailservices_task_submission' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'task_submission',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Submit or update task submission',
            'type'        => 'write',
        ),
        'local_monorailservices_get_user_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_user_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get user current events',
            'type'        => 'read',
        ),
        'local_monorailservices_user_logout' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'user_logout',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Logout the user',
            'type'        => 'write',
        ),
        'local_monorailservices_import_course' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'import_course',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Import a course archive',
            'type'        => 'write',
        ),
        'local_monorailservices_get_notifications' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_notifications',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get user notifications',
            'type'        => 'read',
        ),
        'local_monorailservices_add_cohorts' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_cohorts',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Add user group(cohort)',
            'type'        => 'write',
        ),
        'local_monorailservices_update_cohorts' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_cohorts',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update user group(cohort)',
            'type'        => 'write',
        ),
        'local_monorailservices_add_cohort_members' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_cohort_members',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Add group(cohort) members',
            'type'        => 'write',
        ),
        'local_monorailservices_remove_cohort_members' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'remove_cohort_members',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'suspend/remove group(cohort) members',
            'type'        => 'write',
        ),
        'local_monorailservices_cohorts_info' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'cohorts_info',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get basic information about the cohort',
            'type'        => 'read',
        ),
        'local_monorailservices_update_cohort_admins' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_cohort_admins',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update cohort admin information',
            'type'        => 'write',
        ),
        'local_monorailservices_get_cohort_users' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cohort_users',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get cohort users info',
            'type'        => 'write',
        ),
        'local_monorailservices_get_cohort_extusers' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cohort_extusers',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get cohort ext users info',
            'type'        => 'write',
        ),
        'local_monorailservices_cohort_courses' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'cohort_courses',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get cohort courses info',
            'type'        => 'read',
        ),
        'local_monorailservices_cohort_course_users' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'cohort_course_users',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get cohort course users info',
            'type'        => 'read',
        ),
        'local_monorailservices_revoke_inviteduser' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'revoke_inviteduser',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Remove invitation for user',
            'type'        => 'write',
        ),
        'local_monorailservices_del_course_invite' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_course_invite',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Remove course invitation for user',
            'type'        => 'write',
        ),
        'local_monorailservices_cohort_course_enrol' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'cohort_course_enrol',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Enrol user to cohort course',
            'type'        => 'write',
        ),
        'local_monorailservices_cohort_gapp_users' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'cohort_gapp_users',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get gapp users',
            'type'        => 'read',
        ),
        'local_monorailservices_delete_course' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_course',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete a course',
            'type'        => 'write',
        ),
        'local_monorailservices_get_cal_url' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cal_url',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get calendar export url',
            'type'        => 'read',
        ),
        'local_monorailservices_set_course_status' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'course_set_status',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Set course status',
            'type'        => 'write',
            'capabilities'=> 'moodle/course:update'
        ),
        'local_monorailservices_get_timezones' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_timezones',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get Timezones available',
            'type'        => 'read'
        ),
        'local_monorailservices_get_ltitools' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_ltitools',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get configured external LTI tools list',
            'type'        => 'read'
        ),
        'local_monorailservices_add_ltitool' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_ltitool',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Add LTI tool to a course',
            'type'        => 'write'
        ),
        'local_monorailservices_remove_ltitool' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'remove_ltitool',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Remove LTI tool ',
            'type'        => 'write'
        ),
        'local_monorailservices_update_ltitool' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_ltitool',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update LTI tool to a course',
            'type'        => 'write'
        ),
        'local_monorailservices_remove_ltimod' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'remove_ltimod',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Remove LTI mod to a course',
            'type'        => 'write'
        ),
        'local_monorailservices_add_ltimod' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_ltimod',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Add LTI mod to a course',
            'type'        => 'write'
        ),
        'local_monorailservices_update_ltimod' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_ltimod',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update LTI mod to a course',
            'type'        => 'write'
        ),
        'local_monorailservices_send_message' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'send_message',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Send message to course participants',
            'type'        => 'write'
        ),
        'local_monorailservices_enrolled_users' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_enrolled_users',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get enrolled users',
            'type'        => 'read',
            'capabilities'=> 'moodle/course:viewparticipants'
        ),
        'local_monorailservices_clone_course' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'clone_course',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Clone a complete course for reuse',
            'type'        => 'write'
        ),
        'local_monorailservices_get_feed' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_feed',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get feed items',
            'type'        => 'read',
        ),
        'local_monorailservices_get_skills' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_skills',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get list of available skills',
            'type'        => 'read'
        ),
        'local_monorailservices_validate_skill' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'validate_skill',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Backend pre-validate a skill name',
            'type'        => 'read'
        ),
        'local_monorailservices_save_skills' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'save_skills',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Save course skills',
            'type'        => 'write',
            'capabilities'=> 'moodle/course:update'
        ),
        'local_monorailservices_get_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get list of user tags (aka groups) for current cohort',
            'type'        => 'read'
        ),
        'local_monorailservices_create_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'create_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Create user tags (aka groups) for current cohort',
            'type'        => 'write'
        ),
        'local_monorailservices_update_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update user tags (aka groups) for current cohort',
            'type'        => 'write'
        ),
        'local_monorailservices_delete_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete user tags (aka groups) for current cohort',
            'type'        => 'write'
        ),
        'local_monorailservices_assign_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'assign_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Assign tags (aka groups) to users for current cohort',
            'type'        => 'write'
        ),
        'local_monorailservices_unassign_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'unassign_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Unassign tags (aka groups) from users for current cohort',
            'type'        => 'write'
        ),
        'local_monorailservices_cohort_crs_usrts' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'cohort_course_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get cohort course users info',
            'type'        => 'read'
        ),
        'local_monorailservices_enrol_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'enrol_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Enrol tags (with their current/future users) to courses',
            'type'        => 'write'
        ),
        'local_monorailservices_unenrol_usertags' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'unenrol_usertags',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Enrol tags (with their current/future users) to courses',
            'type'        => 'write'
        ),
        'local_monorailservices_course_grades' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_grades',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return task/quiz/... grades for all students in a course',
            'type'        => 'read'
        ),
        'local_monorailservices_update_cert' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_certificate',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Create/issue/revoke/... certificates.',
            'type'        => 'write'
        ),
        'local_monorailservices_get_cohort_stats' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cohort_stats',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return course/user statistics for current cohort.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_cohort_activity' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cohort_activity',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return course/user activity statistics for current cohort.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_cohort_enrollments' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cohort_enrollments',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return enrollment statistics for current cohort.',
            'type'        => 'read'
        ),
        'local_monorailservices_add_vfilerscs' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_vfilerscs',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Create a video resource entry for course.',
            'type'        => 'write'
        ),
        'local_monorailservices_send_support_mail' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'send_support_mail',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Send support mail.',
            'type'        => 'read'
        ),
        'local_monorailservices_del_vfilerscs' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'del_vfilerscs',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete a video resource entry for course.',
            'type'        => 'write'
        ),
        'local_monorailservices_get_crs_stats' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_stats',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return course/user statistics for current teacher.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_crs_activity' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_activity',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return course/user activity statistics for current teacher.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_crs_enrollments' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_enrollments',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return enrollment statistics for current teacher.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_social_settings' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_social_settings',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Read social settings for the user.',
            'type'        => 'read'
        ),
        'local_monorailservices_update_social_settings' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_social_settings',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update social settings for the user.',
            'type'        => 'write'
        ),
        'local_monorailservices_delete_social_account' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_social_account',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete social settings for the user.',
            'type'        => 'write'
        ),
        'local_monorailservices_get_cohort_userstat' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cohort_userstat',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Return cohort user statistics',
            'type'        => 'read'
        ),
        'local_monorailservices_rate_course' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'rate_course',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Send course rating to magneto.',
            'type'        => 'write'
        ),
        'local_monorailservices_get_share_reminder' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_share_reminder',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get/create share reminder record.',
            'type'        => 'write'
        ),
        'local_monorailservices_save_share_reminder' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'save_share_reminder',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update share reminder record.',
            'type'        => 'write'
        ),
        'local_monorailservices_get_user_courses' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_user_courses',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get user courses.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_user_tasks' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_user_tasks',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get user tasks.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_course_info' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_info',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get course additional info.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_course_settings' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_settings',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Course settings.',
            'type'        => 'read'
        ),
        'local_monorailservices_delete_calendar_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_calendar_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete calendar events',
            'type'        => 'write'
        ),
        'local_monorailservices_create_calendar_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'create_calendar_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Create new calendar events',
            'type'        => 'write'
        ),
        'local_monorailservices_create_bbb_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'create_bigblue_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Create new BB events',
            'type'        => 'write'
        ),
        'local_monorailservices_update_bbb_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_bigblue_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update BB events',
            'type'        => 'write'
        ),
        'local_monorailservices_get_bbb_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_bigblue_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Fetch BB events',
            'type'        => 'write'
        ),
        'local_monorailservices_delete_bbb_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'delete_bigblue_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete BB events',
            'type'        => 'write'
        ),
        'local_monorailservices_reset_tasks' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'reset_tasks',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Reset User Tasks',
            'type'        => 'write'
        ),
        'local_monorailservices_unenroll_cohort_extuser' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'unenroll_cohort_extuser',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Delete external course participants',
            'type'        => 'write'
        ),
        'local_monorailservices_update_calendar_events' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'update_calendar_events',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Update existing calendar events',
            'type'        => 'write'
        ),
        'local_monorailservices_get_invoices' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_invoices',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get invoices',
            'type'        => 'read'
        ),
        'local_monorailservices_get_cc_courses' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cc_courses',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get CC courses.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_material' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_course_material',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get course sections and tasks.',
            'type'        => 'read'
        ),
        'local_monorailservices_get_cert_orders' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'get_cert_orders',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Get printed certificate orders.',
            'type'        => 'read'
        ),
        'local_monorailservices_add_cert_order' => array(
            'classname'   => 'local_monorailservices_external',
            'methodname'  => 'add_cert_order',
            'classpath'   => 'local/monorailservices/externallib.php',
            'description' => 'Add printed certificate order.',
            'type'        => 'write'
        )
);

$services = array(
   'services'  => array(
        'functions' => array (
            // FIXME: these three do not belong to the plugin, but are defined
            // in the core library and enabled in {external_services_functions}
            // table bound to this service in both plato and olive.
            'core_enrol_get_enrolled_users',
            'core_webservice_get_site_info',
            'enrol_manual_enrol_users',

            'local_monorailservices_get_assignments',
            'local_monorailservices_upd_assignments',
            'local_monorailservices_del_assignments',
            'local_monorailservices_getcrs_categories',
            'local_monorailservices_update_courses',
            'local_monorailservices_upd_course_res',
            'local_monorailservices_upd_course_sect',
            'local_monorailservices_del_course_sect',
            'local_monorailservices_course_editable',
            'local_monorailservices_add_submissions',
            'local_monorailservices_get_submissions',
            'local_monorailservices_del_submissions',
            'local_monorailservices_create_forums',
            'local_monorailservices_update_forums',
            'local_monorailservices_del_forums',
            'local_monorailservices_del_fpost',
            'local_monorailservices_del_fdiscussion',
            'local_monorailservices_get_forums',
            'local_monorailservices_forums',
            'local_monorailservices_get_forum_posts',
            'local_monorailservices_forum_posts',
            'local_monorailservices_upd_forum_discussion',
            'local_monorailservices_upd_forum_post',
            'local_monorailservices_create_filerscs',
            'local_monorailservices_del_filerscs',
            'local_monorailservices_update_filename',
            'local_monorailservices_add_assignfiles',
            'local_monorailservices_del_assignfiles',
            'local_monorailservices_add_course_rurls',
            'local_monorailservices_upd_course_rurls',
            'local_monorailservices_del_course_rurls',
            'local_monorailservices_get_page',
            'local_monorailservices_get_list_of_background_picture',
            'local_monorailservices_upd_ctxdata',
            'local_monorailservices_get_tasks_info',
            'local_monorailservices_get_task_details',
            'local_monorailservices_get_users_by_id',
            'local_monorailservices_update_users',
            'local_monorailservices_update_grades',
            'local_monorailservices_assign_roles',
            'local_monorailservices_unassign_roles',
            'local_monorailservices_enrol_users',
            'local_monorailservices_unenrol_users',
            'local_monorailservices_supported_langs',
            'local_monorailservices_get_countries',
            'local_monorailservices_course_get_cont',
            'local_monorailservices_set_enrollment',
            'local_monorailservices_get_invited',
            'local_monorailservices_invite_send',
            'local_monorailservices_verify_captcha',
            'local_monorailservices_create_courses',
            'local_monorailservices_get_courses',
            'local_monorailservices_get_submission',
            'local_monorailservices_task_submission',
            'local_monorailservices_get_user_events',
            'local_monorailservices_user_logout',
            'local_monorailservices_import_course',
            'local_monorailservices_get_notifications',
            'local_monorailservices_add_cohorts',
            'local_monorailservices_update_cohorts',
            'local_monorailservices_add_cohort_members',
            'local_monorailservices_remove_cohort_members',
            'local_monorailservices_cohorts_info',
            'local_monorailservices_update_cohort_admins',
            'local_monorailservices_get_cohort_users',
            'local_monorailservices_get_cohort_extusers',
            'local_monorailservices_cohort_courses',
            'local_monorailservices_cohort_course_users',
            'local_monorailservices_revoke_inviteduser',
            'local_monorailservices_del_course_invite',
            'local_monorailservices_cohort_course_enrol',
            'local_monorailservices_cohort_gapp_users',
            'local_monorailservices_delete_course',
            'local_monorailservices_get_cal_url',
            'local_monorailservices_set_course_status',
            'local_monorailservices_get_timezones',
            'local_monorailservices_get_ltitools',
            'local_monorailservices_add_ltitool',
            'local_monorailservices_remove_ltitool',
            'local_monorailservices_update_ltitool',
            'local_monorailservices_remove_ltimod',
            'local_monorailservices_add_ltimod',
            'local_monorailservices_update_ltimod',
            'local_monorailservices_send_message',
            'local_monorailservices_enrolled_users',
            'local_monorailservices_clone_course',
            'local_monorailservices_get_feed',
            'local_monorailservices_get_skills',
            'local_monorailservices_validate_skill',
            'local_monorailservices_save_skills',
            'local_monorailservices_get_usertags',
            'local_monorailservices_create_usertags',
            'local_monorailservices_update_usertags',
            'local_monorailservices_delete_usertags',
            'local_monorailservices_assign_usertags',
            'local_monorailservices_unassign_usertags',
            'local_monorailservices_cohort_crs_usrts',
            'local_monorailservices_enrol_usertags',
            'local_monorailservices_unenrol_usertags',
            'local_monorailservices_course_grades',
            'local_monorailservices_update_cert',
            'local_monorailservices_get_cohort_stats',
            'local_monorailservices_get_cohort_activity',
            'local_monorailservices_get_cohort_enrollments',
            'local_monorailservices_add_vfilerscs',
            'local_monorailservices_send_support_mail',
            'local_monorailservices_del_vfilerscs',
            'local_monorailservices_get_crs_stats',
            'local_monorailservices_get_crs_activity',
            'local_monorailservices_get_crs_enrollments',
            'local_monorailservices_get_social_settings',
            'local_monorailservices_update_social_settings',
            'local_monorailservices_delete_social_account',
            'local_monorailservices_get_cohort_userstat',
            'local_monorailservices_rate_course',
            'local_monorailservices_get_share_reminder',
            'local_monorailservices_save_share_reminder',
            'local_monorailservices_get_user_courses',
            'local_monorailservices_get_user_tasks',
            'local_monorailservices_get_course_info',
            'local_monorailservices_get_course_settings',
            'local_monorailservices_delete_calendar_events',
            'local_monorailservices_create_calendar_events',
            'local_monorailservices_create_bbb_events',
            'local_monorailservices_update_bbb_events',
            'local_monorailservices_get_bbb_events',
            'local_monorailservices_delete_bbb_events',
            'local_monorailservices_reset_tasks',
            'local_monorailservices_unenroll_cohort_extuser',
            'local_monorailservices_update_calendar_events',
            'local_monorailservices_get_invoices',
            'local_monorailservices_get_cc_courses',
            'local_monorailservices_get_material',
            'local_monorailservices_get_cert_orders',
            'local_monorailservices_add_cert_order',
        ),
        'enabled'           => 0,
        'restrictedusers'   => 0,
        'shortname'         => 'services',
        'downloadfiles'     => 1
    ),
);
