<?php
/**
 * Eliademy.com web services
 *
 * @package   monorail_services
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * External assign API
 *
 * @package    local_monorailservices
 */

defined('MOODLE_INTERNAL') || die;

require_once(dirname(__FILE__) . "/lib.php");
require_once("$CFG->libdir/externallib.php");
require_once($CFG->dirroot . '/theme/monorail/lib.php');
require_once($CFG->dirroot . '/theme/monorail/sociallib.php');
require_once($CFG->dirroot . '/local/monorailfeed/lib.php');

/**
 * Assign functions
 */
class local_monorailservices_external extends external_api
{

    private static function assignment()
    {
        return new external_single_structure(
            array(
                'id'        => new external_value(PARAM_INT, 'assignment id'),
                'course'    => new external_value(PARAM_INT, 'course id'),
                'modulename'    => new external_value(PARAM_TEXT, 'Module name', VALUE_OPTIONAL),
                'instanceid'=> new external_value(PARAM_INT, 'assignment instance id'),
                'intro'  => new external_value(PARAM_RAW, 'Intro to assignment'),
                'introformat' => new external_value(PARAM_INT, 'Intro format'),
                'name'      => new external_value(PARAM_TEXT, 'assignment name'),
                'visible' => new external_value(PARAM_INT, 'Visibility info'),
                'nosubmissions' => new external_value(PARAM_INT, 'no submissions'),
                'submissiondrafts' => new external_value(PARAM_INT, 'submissions drafts'),
                'sendnotifications' => new external_value(PARAM_INT, 'send notifications'),
                'sendlatenotifications' => new external_value(PARAM_INT, 'send notifications'),
                'duedate'   => new external_value(PARAM_INT, 'assignment due date'),
                'allowsubmissionsfromdate' => new external_value(PARAM_INT, 'allow submissions from date'),
                'grade'     => new external_value(PARAM_INT, 'grade type'),
                'timemodified'     => new external_value(PARAM_INT, 'last time assignment was modified'),
                'fileinfo' => new external_multiple_structure(self::fileinfo(), 'list of file attachments to assignments'),
                'grades'   => new external_multiple_structure(self::grades(), 'grade information'),
                'canedit'   => new external_value(PARAM_INT, 'can user edit this task'),
                'submitcount' => new external_value(PARAM_INT, 'submissions count', VALUE_OPTIONAL),
                'gradedcount' => new external_value(PARAM_INT, 'graded submissions count', VALUE_OPTIONAL),
                'haveSubmissions'   => new external_value(PARAM_INT, 'user has submitted something'),
                'section_visible' => new external_value(PARAM_INT, 'visibility of the section this task belongs to')
            ), 'assignment information object');
    }


    private static function grades()
    {
        return new external_single_structure(
            array(
                'grade'  => new external_value(PARAM_TEXT, 'Grade scored', VALUE_OPTIONAL),
                'grademax'        => new external_value(PARAM_TEXT, 'Max grade for assignment', VALUE_OPTIONAL),
                'gradestring'     => new external_value(PARAM_TEXT, 'string for grade', VALUE_OPTIONAL),
                'feedback' =>         new external_value(PARAM_RAW, 'feedback', VALUE_OPTIONAL),
                'dategraded' => new external_value(PARAM_INT, 'date the submission was graded', VALUE_OPTIONAL),
            ), 'grade information object');
    }

    private static function quiz()
    {
        return new external_single_structure(
            array(
                'id'        => new external_value(PARAM_INT, 'assignment id'),
                'instanceid'=> new external_value(PARAM_INT, 'assignment instance id'),
                'course'    => new external_value(PARAM_INT, 'course id'),
                'name'      => new external_value(PARAM_TEXT, 'assignment name'),
                'modulename'      => new external_value(PARAM_TEXT, 'assignment name', VALUE_OPTIONAL),
                'visible' => new external_value(PARAM_INT, 'Visibility info'),
                'nosubmissions' => new external_value(PARAM_INT, 'no submissions'),
                'submissiondrafts' => new external_value(PARAM_INT, 'submissions drafts', VALUE_OPTIONAL),
                'attempts' => new external_value(PARAM_INT, 'no of attempts for quiz', VALUE_OPTIONAL),
                'attemptsleft' => new external_value(PARAM_INT, 'no of attempts', VALUE_OPTIONAL),
                'endquiz' => new external_value(PARAM_INT, 'Quiz ended by user', VALUE_OPTIONAL),
                'sendnotifications' => new external_value(PARAM_INT, 'send notifications', VALUE_OPTIONAL),
                'sendlatenotifications' => new external_value(PARAM_INT, 'send notifications', VALUE_OPTIONAL),
                'duedate'   => new external_value(PARAM_INT, 'assignment due date'),
                'allowsubmissionsfromdate' => new external_value(PARAM_INT, 'allow submissions from date', VALUE_OPTIONAL),
                'grade'     => new external_value(PARAM_INT, 'grade type', VALUE_OPTIONAL),
                'grades'   => new external_multiple_structure(self::grades(), 'grade information'),
                'timemodified'     => new external_value(PARAM_INT, 'last time assignment was modified'),
                'canedit'   => new external_value(PARAM_INT, 'can user edit this quiz'),
                'submitcount' => new external_value(PARAM_INT, 'submissions count', VALUE_OPTIONAL),
                'gradedcount' => new external_value(PARAM_INT, 'graded submissions count', VALUE_OPTIONAL),
                'haveSubmissions'   => new external_value(PARAM_INT, 'user has submitted something'),
                'lastscore' => new external_value(PARAM_TEXT, 'last attempt quiz score', VALUE_OPTIONAL),
                'score' => new external_value(PARAM_TEXT, 'quiz score', VALUE_OPTIONAL)
            ), 'quiz information object');
    }


    private static function course() {
        return new external_single_structure(
            array(
                'id'        => new external_value(PARAM_INT, 'course id'),
                'fullname'  => new external_value(PARAM_TEXT, 'course full name'),
                'shortname' => new external_value(PARAM_TEXT, 'course short name'),
                'code' => new external_value(PARAM_TEXT, 'course code'),
                'timemodified' => new external_value(PARAM_INT, 'last time modified'),
                'completed' => new external_value(PARAM_INT, 'completed'),
                'quizes'  => new external_multiple_structure(self::quiz(), 'list of quiz information'),
                'assignments'  => new external_multiple_structure(self::assignment(), 'list of assignment information'),
            	'coursebackground' => new external_value(PARAM_TEXT, 'course background picture'),
                'inviteurl' => new external_value(PARAM_URL, 'public invitation url for self enrollment'),
                'invitesopen' => new external_value(PARAM_INT, 'public invitation url status'),
                'invitecode' => new external_value(PARAM_TEXT, 'public invitation code'),
                'canedit' => new external_value(PARAM_INT, 'can user edit course, 1 for yes'),
                'students' => new external_value(PARAM_INT, 'students count in course', VALUE_OPTIONAL),
            	'categoryid' => new external_value(PARAM_INT, 'category id'),
            	'course_privacy' => new external_value(PARAM_INT, 'course public=1, private=2', VALUE_DEFAULT, 2),
            	'course_review' => new external_value(PARAM_INT, 'course feature status', VALUE_DEFAULT, 0),
            	'course_access' => new external_value(PARAM_INT, 'course access draft=1, invite=2, open=3', VALUE_DEFAULT, 1),
                'course_logo' => new external_value(PARAM_TEXT, 'course logo image', VALUE_OPTIONAL),
                'startdate' => new external_value(PARAM_INT, 'start date timestamp', VALUE_OPTIONAL),
                'course_rating' => new external_value(PARAM_INT, 'course rating', VALUE_OPTIONAL),
                'enddate' => new external_value(PARAM_INT, 'end date timestamp', VALUE_OPTIONAL),
                'mainteacher' => new external_value(PARAM_INT, 'main teacher', VALUE_OPTIONAL),
                'course_price' => new external_value(PARAM_NUMBER, 'Price for course enrollment', VALUE_OPTIONAL),
                'course_paypal_account' => new external_value(PARAM_TEXT, 'PayPal account that receives payments for course', VALUE_OPTIONAL),
                'review_submit_time' => new external_value(PARAM_TEXT, 'Time when course was submitted for review (for pub. catalog)', VALUE_OPTIONAL),
                'cohortid'        => new external_value(PARAM_INT, 'Cohort id if course belongs to cohort',VALUE_OPTIONAL),
                'ccprivate'       => new external_value(PARAM_INT, 'If teacher can edit public/private access restrictions',VALUE_OPTIONAL),
                'ccprivateext'       => new external_value(PARAM_INT, 'If teacher can edit public/private access restrictions',VALUE_OPTIONAL),
                'teacher_name' => new external_value(PARAM_TEXT, 'teacher name', VALUE_OPTIONAL),
                'durationstr' => new external_value(PARAM_TEXT, 'course duration string', VALUE_OPTIONAL),
                'teacher_title' => new external_value(PARAM_TEXT, 'teacher title', VALUE_OPTIONAL),
                'teacher_avatar' => new external_value(PARAM_TEXT, 'teacher avatar', VALUE_OPTIONAL),
                'skills'    => new external_multiple_structure(
                    new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'list of skills', VALUE_DEFAULT, array()),
                'progress' => new external_value(PARAM_FLOAT, 'course progress for logged in student', VALUE_DEFAULT, 0),
                'cert_enabled' => new external_value(PARAM_INT, 'Certificates enabled for course', VALUE_OPTIONAL),
                "cert_hashurl" => new external_value(PARAM_TEXT, "Certificate URL hash (if issued)"),
                "cert_issuetime" => new external_value(PARAM_TEXT, "Certificate issue time", VALUE_OPTIONAL),
                "cert_orglogo" => new external_value(PARAM_TEXT, "Organization logo for certificate (if set)", VALUE_OPTIONAL),
                "cert_sigpic" => new external_value(PARAM_TEXT, "Teacher signature for certificate (if set)", VALUE_OPTIONAL),
                'cert_custom' => new external_value(PARAM_INT, 'Custom certificates', VALUE_OPTIONAL),
                'cert_custom_info' => new external_value(PARAM_TEXT, 'Custom certificate info', VALUE_OPTIONAL)
            ), 'course information object' );
    }

    private static function course_info() {
        return new external_single_structure(
            array(
                'skills'    => new external_multiple_structure(
                    new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'list of skills', VALUE_DEFAULT, array()),
                'mainteacher' => new external_value(PARAM_INT, 'main teacher', VALUE_OPTIONAL),
                'teacher_name' => new external_value(PARAM_TEXT, 'teacher name', VALUE_OPTIONAL),
                'teacher_title' => new external_value(PARAM_TEXT, 'teacher title', VALUE_OPTIONAL),
                'teacher_avatar' => new external_value(PARAM_TEXT, 'teacher avatar', VALUE_OPTIONAL),
                "cert_hashurl" => new external_value(PARAM_TEXT, "Certificate URL hash (if issued)", VALUE_OPTIONAL),
                "cert_issuetime" => new external_value(PARAM_TEXT, "Certificate issue time", VALUE_OPTIONAL)
            ), 'Course information object' );
    }

    private static function course_basic() {
        return new external_single_structure(
            array(
                'id'        => new external_value(PARAM_INT, 'course id'),
                'category'        => new external_value(PARAM_INT, 'course category'),
                'code' => new external_value(PARAM_TEXT, 'course code'),
                'fullname'  => new external_value(PARAM_TEXT, 'course full name'),
            	'coursebackground' => new external_value(PARAM_TEXT, 'course background picture'),
            	'coursebackgroundtn' => new external_value(PARAM_TEXT, 'course background picture'),
                'course_logo' => new external_value(PARAM_TEXT, 'course logo image', VALUE_OPTIONAL),
                'durationstr' => new external_value(PARAM_TEXT, 'course duration string', VALUE_OPTIONAL),
                'progress' => new external_value(PARAM_FLOAT, 'course progress for logged in student', VALUE_DEFAULT, 0),
                'students' => new external_value(PARAM_INT, 'students count in course', VALUE_OPTIONAL),
                'stars' => new external_value(PARAM_INT, 'stars given to course (-1 if not applicable)', VALUE_OPTIONAL),
                'price' => new external_value(PARAM_FLOAT, 'course price (-1 if not applicable)', VALUE_OPTIONAL),
                'course_access' => new external_value(PARAM_INT, 'course access', VALUE_OPTIONAL),
                'course_privacy' => new external_value(PARAM_INT, 'course privacy', VALUE_OPTIONAL),
                'can_edit' => new external_value(PARAM_INT, 'can edit?', VALUE_OPTIONAL),
                'teacher_name' => new external_value(PARAM_TEXT, 'teacher name', VALUE_OPTIONAL),
                'teacher_title' => new external_value(PARAM_TEXT, 'teacher title', VALUE_OPTIONAL),
                'teacher_avatar' => new external_value(PARAM_TEXT, 'teacher avatar', VALUE_OPTIONAL),
                'licence' => new external_value(PARAM_INT, 'course licence', VALUE_OPTIONAL),
                'keywords' => new external_value(PARAM_TEXT, 'course keywords', VALUE_OPTIONAL),
                'language' => new external_value(PARAM_TEXT, 'course language', VALUE_OPTIONAL),
                'course_country' => new external_value(PARAM_TEXT, 'course country', VALUE_OPTIONAL),
                'derivedfrom' => new external_value(PARAM_INT, 'Course derivedfrom', VALUE_OPTIONAL),
                'derivedfrom_name' => new external_value(PARAM_TEXT, 'Course derivedfrom name', VALUE_OPTIONAL),
                'derivedfrom_code' => new external_value(PARAM_TEXT, 'Course derivedfrom code', VALUE_OPTIONAL),
                'cert_enabled' => new external_value(PARAM_INT, 'Certificates enabled for course', VALUE_OPTIONAL)
            ), 'Course basic information object' );
    }

    private static function course_settings() {
        return new external_single_structure(
            array(
                'id'        => new external_value(PARAM_INT, 'course id'),
                'code' => new external_value(PARAM_TEXT, 'course code'),
                'startdate' => new external_value(PARAM_INT, 'start date timestamp', VALUE_OPTIONAL),
                'enddate' => new external_value(PARAM_INT, 'end date timestamp', VALUE_OPTIONAL),
                'inviteurl' => new external_value(PARAM_URL, 'public invitation url for self enrollment'),
                'invitesopen' => new external_value(PARAM_INT, 'public invitation url status'),
                'invitecode' => new external_value(PARAM_TEXT, 'public invitation code'),
            	'categoryid' => new external_value(PARAM_INT, 'category id'),
                'course_privacy' => new external_value(PARAM_INT, 'course public=1, private=2', VALUE_DEFAULT, 2),
                'course_review' => new external_value(PARAM_INT, 'course feature status', VALUE_DEFAULT, 0),
                'course_access' => new external_value(PARAM_INT, 'course access draft=1, invite=2, open=3', VALUE_DEFAULT, 1),
                'course_rating' => new external_value(PARAM_INT, 'course rating', VALUE_OPTIONAL),
                'course_price' => new external_value(PARAM_NUMBER, 'Price for course enrollment', VALUE_OPTIONAL),
                'course_paypal_account' => new external_value(PARAM_TEXT, 'PayPal account that receives payments for course', VALUE_OPTIONAL),
                'review_submit_time' => new external_value(PARAM_TEXT, 'Time when course was submitted for review (for pub. catalog)', VALUE_OPTIONAL),
                'cohortid'        => new external_value(PARAM_INT, 'Cohort id if course belongs to cohort',VALUE_OPTIONAL),
                'ccprivate'       => new external_value(PARAM_INT, 'If teacher can edit public/private access restrictions',VALUE_OPTIONAL),
                'ccprivateext'       => new external_value(PARAM_INT, 'If teacher can edit public/private access restrictions',VALUE_OPTIONAL),
                'cert_enabled' => new external_value(PARAM_INT, 'Certificates enabled for course', VALUE_OPTIONAL),
                "cert_orglogo" => new external_value(PARAM_TEXT, "Organization logo for certificate (if set)", VALUE_OPTIONAL),
                "cert_sigpic" => new external_value(PARAM_TEXT, "Teacher signature for certificate (if set)", VALUE_OPTIONAL)
            ), 'Course settings information object' );
    }

    private static function course_tasks() {
        return new external_single_structure(
            array(
                'id'        => new external_value(PARAM_INT, 'course id'),
                'progress' => new external_value(PARAM_FLOAT, 'course progress for logged in student', VALUE_DEFAULT, 0),
                'quizes'  => new external_multiple_structure(self::quiz(), 'list of quiz information'),
                'assignments'  => new external_multiple_structure(self::assignment(), 'list of assignment information'),
            ), 'Course tasks information object' );
    }

    private static function tasksubmissions() {
        return new external_single_structure(
            array(
                'studentid'        => new external_value(PARAM_INT, 'student id'),
                'studentname'  => new external_value(PARAM_TEXT, 'student full name'),
                'assignname'  => new external_value(PARAM_TEXT, 'assignment full name'),
                'visible'        => new external_value(PARAM_INT, 'visibility of course module'),
                'profileimageurl' => new external_value(PARAM_URL, 'profile image url',VALUE_OPTIONAL),
                'profilepichash' => new external_value(PARAM_TEXT, 'profile image hash',VALUE_OPTIONAL),
                'assignid'        => new external_value(PARAM_INT, 'assignment id'),
                'courseid'        => new external_value(PARAM_INT, 'course id'),
                'duedate' => new external_value(PARAM_INT, 'due date for assignment',VALUE_OPTIONAL),
                'lastsubmissiondate' => new external_value(PARAM_INT, 'Time stamp for last submitted work',VALUE_OPTIONAL),
                'nosubmissions' => new external_value(PARAM_INT, 'no submissions', VALUE_OPTIONAL),
                'submissions'  => new external_multiple_structure(self::fileinfo(), 'File submission information', VALUE_OPTIONAL),
                'grades'   => new external_multiple_structure(self::grades(), 'grade information if available', VALUE_OPTIONAL),
                'text' => new external_value(PARAM_RAW, 'submission text from student', VALUE_OPTIONAL)
            ), 'task submissions information object' );
    }

    private static function fileinfo() {
        return new external_single_structure(
            array(
                'id'    => new external_value(PARAM_INT, 'file id'),
                'name'  => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'name'),
                'filename'  => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'file name', VALUE_OPTIONAL),
                'timecreated' => new external_value(PARAM_INT, 'time created'),
                'url' => new external_value(PARAM_URL, 'url for the file'),
                //'convertedpath' => new external_value(PARAM_URL, 'url for the converted file'),
                'contextualdata' => new external_value(PARAM_RAW, 'contextual data'),
                'filetype' => new external_value(PARAM_ALPHA, 'file type', VALUE_OPTIONAL),
                'mimetype' => new external_value(PARAM_NOTAGS, 'mimetype', VALUE_OPTIONAL),
                'options' => new external_value(PARAM_TEXT, 'file attachment options', VALUE_OPTIONAL)
            ), 'file information object' );
    }

    private static function vfileinfo() {
        return new external_single_structure(
            array(
                'id'    => new external_value(PARAM_TEXT, 'file id'),
                'contextualdata' => new external_value(PARAM_RAW,'Contextual data for the file resource', VALUE_DEFAULT, '{"links":null,"visible":false}
'),
                'options' => new external_value(PARAM_TEXT, 'file attachment options', VALUE_OPTIONAL)
            ), 'Video file information object' );
    }

    private static function monorailservices_get_file_contextdata($pathhash, $fileid) {
        global $DB;
        $result = '';
        if(empty($pathhash) || empty($fileid)) {
            return $result;
        }
        if($DB->record_exists('monorail_file_context_data', array('fileid'=>$fileid))){
            $result = $DB->get_field('monorail_file_context_data', 'contextdata',
                array('fileid'=>$fileid));
        }
        return $result;
    }

    public static function update_assignments_parameters() {
      return new external_function_parameters(
          array('assignments' => new external_multiple_structure (
              new external_single_structure(
                  array(
                      'id'        => new external_value(PARAM_INT, 'assignment id', VALUE_DEFAULT, 0),
                      'instanceid' => new external_value(PARAM_INT, 'assignment instance id', VALUE_DEFAULT, 0),
                      'attempt' => new external_value(PARAM_INT, 'is this an attempt to complete task?', VALUE_DEFAULT, 0),
                      'attempts' => new external_value(PARAM_INT, 'no of attemps for quiz', VALUE_OPTIONAL),
                      'endquiz' => new external_value(PARAM_INT, 'End quiz - see results', VALUE_DEFAULT, 0),
                      'current_question' => new external_value(PARAM_INT, 'Current question (when saving answers)', VALUE_OPTIONAL),
                      'cmid'      => new external_value(PARAM_INT, 'module id',VALUE_OPTIONAL),
                      'intro'  => new external_value(PARAM_RAW, 'Intro to assignment', VALUE_OPTIONAL),
                      'introformat' => new external_value(PARAM_INT, 'Intro format', VALUE_OPTIONAL),
                      'sectionid' => new external_value(PARAM_INT, 'section id', VALUE_OPTIONAL),
                      'course'    => new external_value(PARAM_INT, 'course id', VALUE_OPTIONAL),
                      'name'      => new external_value(PARAM_RAW, 'assignment name', VALUE_OPTIONAL),
                      'modulename'   => new external_value(PARAM_TEXT, 'assignment mofule name'),
                      'questions' => new external_multiple_structure(new external_single_structure(array(
                            'id' => new external_value(PARAM_INT, 'question id', VALUE_OPTIONAL),
                            'instanceid' => new external_value(PARAM_INT, 'question instance id', VALUE_OPTIONAL),
                            'text' => new external_value(PARAM_RAW, 'question text', VALUE_OPTIONAL),
                            'name' => new external_value(PARAM_RAW, 'question name', VALUE_OPTIONAL),
                            'explanation' => new external_value(PARAM_TEXT, 'question explanation', VALUE_OPTIONAL),
                            'type' => new external_value(PARAM_TEXT, 'question type', VALUE_OPTIONAL),
                            'answers' => new external_multiple_structure(new external_single_structure(array(
                               'id' => new external_value(PARAM_INT, 'answer id', VALUE_OPTIONAL),
                               'text' => new external_value(PARAM_RAW, 'answer text', VALUE_OPTIONAL),
                               'correct' => new external_value(PARAM_INT, 'answer correct', VALUE_OPTIONAL),
                               'student_answer' => new external_value(PARAM_TEXT, 'student answer', VALUE_OPTIONAL))))
                      )), 'list for quiz questions', VALUE_OPTIONAL),
                      'visible'      => new external_value(PARAM_INT, 'assignment visibility', VALUE_OPTIONAL),
                      'nosubmissions' => new external_value(PARAM_INT, 'no submissions', VALUE_OPTIONAL),
                      'submissiondrafts' => new external_value(PARAM_INT, 'submissions drafts', VALUE_OPTIONAL),
                      'sendnotifications' => new external_value(PARAM_INT, 'send notifications', VALUE_OPTIONAL),
                      'sendlatenotifications' => new external_value(PARAM_INT, 'send notifications', VALUE_OPTIONAL),
                      'preventsubmissions' =>  new external_value(PARAM_INT, 'prevent submissions', VALUE_OPTIONAL),
                      'duedate'   => new external_value(PARAM_TEXT, 'assignment due date (text format)', VALUE_OPTIONAL),
                      'startdate'   => new external_value(PARAM_TEXT, 'assignment start date (text format)', VALUE_OPTIONAL),
                      'nolatesubmissions'   => new external_value(PARAM_INT, 'no late submissions', VALUE_OPTIONAL),
                      'allowsubmissionsfromdate' => new external_value(PARAM_INT, 'allow submissions from date', VALUE_OPTIONAL),
                      'grade'     => new external_value(PARAM_INT, 'grade type', VALUE_OPTIONAL ),
                      'quiet' => new external_value(PARAM_INT, 'quiet save, do not trigger notifications', VALUE_OPTIONAL),
                      'options' => new external_value(PARAM_RAW, 'extra optios for task/quiz', VALUE_OPTIONAL)
                  )), 'list of assignment information')
          )

      );
    }


    public static function update_assignments($assignments) {
        global $USER, $DB, $CFG;
        return self::util_update_assignments($assignments);
    }
    /**
     * Update/Modify assignments for courses in which the user is enrolled, optionally filtered
     * by course id and/or capability
     * @param array of  $assignments
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function util_update_assignments($assignments, $validatecontext=true) {
        global $USER, $DB, $CFG;

        $params = self::validate_parameters(self::update_assignments_parameters(),
            array('assignments' => $assignments));
        $rassignments = array();
        $warnings = array();

        foreach ($assignments as $assignment) {
            if (array_key_exists('name', $assignment)) {
              $assignment['name'] = clean_param($assignment['name'], PARAM_TEXT);
            }

            if (array_key_exists('duedate', $assignment)) {
                $assignment["duedate"] = strtotime($assignment["duedate"]);
            } else {
                $assignment["duedate"] = 0;
            }

            if (array_key_exists('startdate', $assignment) && $assignment["startdate"]) {
                $assignment["allowsubmissionsfromdate"] = strtotime($assignment["startdate"]);
            } else {
                $assignment["allowsubmissionsfromdate"] = time();
            }

            if (array_key_exists('intro', $assignment)) {
                $assignment["intro"] = preg_replace("/<script\b[^>]*>(.*?)<\/script>/is", "", $assignment['intro']);
            } 
            //Get the assignment record
            $insert = false;
            $courseid = 0;
            if($assignment['id'] <= 0) {
                //add assignment
                $insert = true;
                $courseid = $assignment['course'];
            } else {
                //update assignment
                $oldassignment = $DB->get_record($assignment["modulename"], array("id"=>$assignment['id']), '*', MUST_EXIST);
                $courseid = $oldassignment->course;
            }

            if ($assignment["modulename"] == "quiz" && $assignment["attempt"]) {
                // User is attempting quiz.
                // XXX: cutting corners around moodle quiz engine...

                if ((int) $assignment["attempt"] == 2) {
                    // Restarting quiz.

                    $attempts_count  = reset(monorail_data('quiz_attempts_count', $assignment["id"], "user" . $USER->id));

                    if (!$attempts_count) {
                        $attempts_count = 0;
                    } else {
                        $attempts_count = $attempts_count->value;
                    }

                    $attempts_count++;
                    monorail_data('quiz_attempts_count', $assignment["id"], "user" . $USER->id, $attempts_count);

                    if ($oldassignment->attempts && $attempts_count >= $oldassignment->attempts) {
                        // Can't restart - end instead!
                        monorail_data('quiz_end', $assignment["id"], "user" . $USER->id, 1);

                        $DB->execute("DELETE FROM {monorail_data} WHERE type='courseprogress' AND itemid=? AND datakey=?",
                            array($courseid, "progress-" . $USER->id));

                        wscache_reset_by_user($USER->id);

                        // trigger notification, no check for 'quiet' param for user_submitted
                        $cm = get_coursemodule_from_instance('quiz', $assignment['id'], $courseid, false, MUST_EXIST);
                        monorailfeed_create_notification($courseid, 'user_submitted', 'teacher', $cm->id, $USER->id, null, null, null, array(3));
                    } else {
                        // Deleting current attempt.
                        $DB->delete_records('monorail_data', array('datakey'=>"user" . $USER->id, 'itemid'=>$assignment["id"], 'type'=>'quiz_attempt'));
                    }
                } else {
                    // Saving next step.
                    $attemptData = monorail_data('quiz_attempt', $assignment["id"], "user" . $USER->id);

                    if ($attemptData) {
                        $attemptData = reset($attemptData);
                        $attemptData = unserialize($attemptData->value);
                    }

                    if (!$attemptData) {
                        $attemptData = array();
                    }

                    $current_question = (int) @$assignment["current_question"];

                    $attemptQuestion = array();

                    if ($assignment["questions"][$current_question]["type"] == 2) {
                        $correct = false;
                        $num = 0;

                        foreach ($assignment["questions"][$current_question]["answers"] as $answerData) {
                            if ($num) {
                                if (strtolower(trim($answerData["text"])) == strtolower(trim($assignment["questions"][$current_question]["answers"][0]["student_answer"]))) {
                                    $correct = true;
                                    break;
                                }
                            }

                            $num++;
                        }
                    } else {
                        $correct = true;

                        foreach ($assignment["questions"][$current_question]["answers"] as $answerData) {
                            if ((int) $answerData["correct"] != (int) $answerData["student_answer"]) {
                                $correct = false;
                                break;
                            }
                        }
                    }

                    foreach ($assignment["questions"][$current_question]["answers"] as $answerData) {
                        $attemptQuestion[] = array("answer" => $answerData["id"], "correct" => $answerData["correct"], "checked" => $answerData["student_answer"]);
                    }

                    $attemptData[$current_question] = array("question" => $assignment["questions"][$current_question]["id"], "answers" => $attemptQuestion, "correct" => $correct);

                    monorail_data('quiz_attempt', $assignment["id"], "user" . $USER->id, serialize($attemptData));

                    if (count($attemptData) >= count($assignment["questions"])) {
                        // All questions answered.
                        $attempts_count  = reset(monorail_data('quiz_attempts_count', $assignment["id"], "user" . $USER->id));

                        if (!$attempts_count) {
                            $attempts_count = 0;
                        } else {
                            $attempts_count = $attempts_count->value;
                        }

                        if ($oldassignment->attempts) {
                            if ($attempts_count + 1 >= $oldassignment->attempts) {
                                // Last attempt - closing quiz.
                                monorail_data('quiz_end', $assignment["id"], "user" . $USER->id, 1);

                                $DB->execute("DELETE FROM {monorail_data} WHERE type='courseprogress' AND itemid=? AND datakey=?",
                                    array($courseid, "progress-" . $USER->id));

                                wscache_reset_by_user($USER->id);

                                // trigger notification, no check for 'quiet' param for user_submitted
                                $cm = get_coursemodule_from_instance('quiz', $assignment['id'], $courseid, false, MUST_EXIST);
                                monorailfeed_create_notification($courseid, 'user_submitted', 'teacher', $cm->id, $USER->id, null, null, null, array(3));
                            }
                        }
                    }
                }

                // Don't try to save anything else...
                continue;
            } else if ($assignment["endquiz"] == 1) { //User wants to end quiz and check results
                monorail_data('quiz_end', $assignment["id"], "user" . $USER->id, 1);

                $DB->execute("DELETE FROM {monorail_data} WHERE type='courseprogress' AND itemid=? AND datakey=?",
                    array($courseid, "progress-" . $USER->id));

                wscache_reset_by_user($USER->id);

                // trigger notification, no check for 'quiet' param for user_submitted
                $cm = get_coursemodule_from_instance('quiz', $assignment['id'], $courseid, false, MUST_EXIST);
                monorailfeed_create_notification($courseid, 'user_submitted', 'teacher', $cm->id, $USER->id, null, null, null, array(3));

                // Don't try to save anything else...
                continue;
            }

            // ---------------------------------------------------------------------

            $context = context_course::instance($courseid);

            try {
                if($validatecontext)
                    self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
                if($insert)
                    require_capability('mod/assignment:addinstance', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $courseid;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context';
                $warnings[] = $warning;
                continue;
            }
            $rassign = array();
            $rassign['courseid'] =  $courseid;
            $rassign['assignid'] =  $assignment['id'];

            // Ok iterate through the keys if update and update keys provided , retail old values
            // for others
            if($insert) {
                if (!isset($assignment["sectionid"]))
                {
                    // When sectionid is not specified, use the first one.
                    $firstSection = $DB->get_record_sql("SELECT id FROM {course_sections} WHERE course=?"
                        . " ORDER BY section LIMIT 1", array($assignment["course"]));
                    $assignment["sectionid"] = $firstSection->id;
                }

                $data = array('instance' => $assignment['id'],
                        'name'=> $assignment['name'],
                        'sectionid' => $assignment['sectionid'],
                        'allowsubmissionsfromdate'=> $assignment["allowsubmissionsfromdate"],
                        'duedate'=> $assignment['duedate'],
                        'alwaysshowdescription' => 1,
                        'preventlatesubmissions' => 0,
                        'latesubmissionsuntil' => 0,
                        'submissiondrafts'=> 0,
                        'sendnotifications'=> 1,
                        'sendlatenotifications'=> 1,
                        'course'=> $assignment['course'],
                        'grade'=> 100, //TODO: what is our grading scale?
                        'intro' => clean_param($assignment['intro'], PARAM_RAW_TRIMMED),
                        'introformat' =>  1,
                );
                if ($assignment['nosubmissions'] == 0) {
                    $data['assignsubmission_onlinetext_enabled'] = 1;
                    $data['assignsubmission_file_maxfiles'] = 10;
                    $data['assignsubmission_file_enabled'] = 1;
                    $data['assignsubmission_file_maxsizebytes'] = $CFG->monorail_upload_limit;
                    $data['assignsubmission_comments_enabled'] = 0;
                    $data['assignfeedback_comments_enabled'] = 0;
                    $data['assignfeedback_file_enabled'] = 0;
                }
                $data = (object) $data;
                $section = $DB->get_record("course_sections", array("id"=>$data->sectionid));
                if(!$section) {
                    //User cannot create assignment for these courses
                    $warning = array();
                    $warning['item'] = 'course';
                    $warning['itemid'] = $courseid;
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'Unable to find course section to add assignment';
                    $warnings[] = $warning;
                    $rassignments[] = $rassign;
                    continue;//continue with next module error getting section
                }
                $moduleid = $DB->get_field('modules', 'id', array('name'=> $assignment["modulename"]));
                $newcm = new stdClass();
                $newcm->course   = $data->course;
                $newcm->module   = $moduleid;
                $newcm->instance = 0; // not known yet, will be updated later
                if(array_key_exists('visible', $assignment)) {
                    $newcm->visible  = $assignment['visible'];
                } else {
                    $newcm->visible  = 1;
                }
                $newcm->section  = $section->id;
                $newcm->coursemodule  = 0;
                $newcm->added    = time();
                $newcmid = $DB->insert_record('course_modules', $newcm);
                $context = context_module::instance($newcmid);
                $data->coursemodule = $newcmid;
                $data->cmidnumber = $newcmid;
                require_once($CFG->dirroot . '/course/lib.php');

                if ($assignment["modulename"] == "assign")
                {
                    // New assignment.
                    require_once($CFG->dirroot . '/mod/assign/locallib.php');
                    $newassign = new assign($context, null, null);
                    $newcm->coursemodule = $newassign->add_instance($data, true);
                    if($newcm->coursemodule <= 0) {
                        $DB->delete_records('course_modules', array('id' => $newcmid));
                        $warning = array();
                        $warning['item'] = 'course';
                        $warning['itemid'] = $courseid;
                        $warning['warningcode'] = '1';
                        $warning['message'] = 'Unable to add course record';
                        $warnings[] = $warning;
                        $assignments[] = $rassign;
                        continue; //error adding course module
                    }
                    $rassign['assignid'] = $newcmid;
                    $rassign["id"] = $newcm->coursemodule;
                    $rassignments[] = $rassign;
                    //Update the instance field in course_modules
                    $DB->set_field('course_modules', 'instance', $newcm->coursemodule, array('id'=>$newcmid));

                    //section info update
                    $newcm->coursemodule  = $newcmid;
                    $newcm->section = $section->section;
                    add_mod_to_section($newcm);

                    $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcm->coursemodule));
                    set_coursemodule_visible($newcm->coursemodule,$newcm->visible);
                    rebuild_course_cache($data->course);

                    if ((! isset($assignment['quiet']) || $assignment['quiet'] != 1) && ($newcm->visible != 0)) {
                        //send add events
                        $eventdata = new stdClass();
                        $eventdata->modulename = "assign";
                        $eventdata->name       = 'mod_created';
                        $eventdata->cmid       = $newcm->coursemodule;
                        $eventdata->courseid   = $data->course;
                        $eventdata->userid     = $USER->id;
                        events_trigger("mod_created", $eventdata);
                    }
                }
                else if ($assignment["modulename"] == "quiz")
                {
                    // New quiz
                    require_once($CFG->dirroot . '/mod/quiz/lib.php');
                    require_once($CFG->dirroot . '/mod/quiz/editlib.php');
                    require_once($CFG->dirroot . '/question/engine/lib.php');
                    require_once($CFG->dirroot . '/question/type/questiontypebase.php');

                    $quizData = new stdClass();
                    $quizData->course = $assignment["course"];
                    $quizData->name = $assignment["name"];
                    $quizData->section = $assignment["sectionid"];
                    $quizData->coursemodule = $newcmid;
                    $quizData->intro = "";
                    $quizData->introformat = 1;
                    $quizData->quizpassword = "";
                    $quizData->timeclose = $assignment["duedate"];
                    $quizData->timeopen = $assignment["allowsubmissionsfromdate"];
                    $quizData->subnet = "";
                    $quizData->attempts = $assignment["attempts"];
                    $quizData->browsersecurity = "-";
                    $quizData->preferredbehaviour = "deferredfeedback";

                    $quizData->feedbackboundarycount = 0;
                    $quizData->feedbacktext = array(array("text" => "", "format" => 1));
                    $quizData->feedbackboundaries = array(-1 => 0, 0 => 0);

                    $quizData->id = quiz_add_instance($quizData);
                    $category = question_make_default_categories(array($context));

                    foreach ($assignment["questions"] as $questionData)
                    {
                        $question = new stdClass();
                        $form = new stdClass();
                        $questionData["text"] = str_replace(array("\r", "\n"), '',clean_param($questionData["text"], PARAM_RAW_TRIMMED));
                        $question->id = 0;
                        $form->category = $category->id;
                        $form->name = clean_param(mb_substr($questionData["text"], 0, 10), PARAM_TEXT);
                        $form->single = 0;
                        $form->answernumbering = "abc";
                        $form->shuffleanswers = 0;
                        $form->answer = array();
                        $form->fraction = array();
                        $form->feedback = array();
                        $form->correctfeedback = array("format" => 1, "text" => clean_param($questionData["explanation"], PARAM_TEXT));
                        $form->partiallycorrectfeedback = array("format" => 1, "text" => (int) $questionData["type"]);
                        $form->incorrectfeedback = array("format" => 1, "text" => "");
                        $form->questiontext = array("format" => 1, "text" => preg_replace("/<script\b[^>]*>(.*?)<\/script>/is", "", $questionData["text"]));

                        $correctCount = 0;

                        foreach ($questionData["answers"] as $answerData)
                        {
                            if ($answerData["correct"])
                            {
                                $correctCount++;
                            }
                        }

                        if ($correctCount > 0)
                        {
                            $correctPoints = 1.0 / $correctCount;
                        }
                        else
                        {
                            // whoops, return warning to UI
                            $warning = array();
                            $warning['item'] = 'quiz';
                            $warning['itemid'] = $quizData->id;
                            $warning['warningcode'] = '1';
                            $warning['message'] = 'No correct answers selected for question!';
                            $warnings[] = $warning;

                            continue;
                        }

                        foreach ($questionData["answers"] as $answerData)
                        {
                            $form->answer[] = array(
                                "format" => 1,
                                "text" => clean_param($answerData["text"], PARAM_RAW_TRIMMED));

                            $form->fraction[] = $answerData["correct"] ? $correctPoints : 0;

                            $form->feedback[] = array(
                                "format" => 1,
                                "text" => "");
                        }

                        $qtypeobj = question_bank::get_qtype("multichoice");
                        $question = $qtypeobj->save_question($question, $form);

                        quiz_add_quiz_question($question->id, $quizData);
                    }

                    //section info update
                    $newcm->coursemodule  = $newcmid;
                    $newcm->section = $section->section;
                    add_mod_to_section($newcm);

                    $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcm->coursemodule));
                    set_coursemodule_visible($newcm->coursemodule, $newcm->visible);
                    rebuild_course_cache($data->course);

                    $rassign['assignid'] = $newcmid;
                    $rassign["id"] = $quizData->id;
                    $rassignments[] = $rassign;

                    if (! isset($assignment['quiet']) || $assignment['quiet'] != 1 && ($newcm->visible != 0)) {
                        //send add events
                        $eventdata = new stdClass();
                        $eventdata->modulename = "quiz";
                        $eventdata->name       = 'mod_created';
                        $eventdata->cmid       = $newcmid;
                        $eventdata->courseid   = $assignment["course"];
                        $eventdata->userid     = $USER->id;
                        events_trigger("mod_created", $eventdata);
                    }
                }

                wscache_reset_by_dependency("course_tasks_" . $courseid);
                wscache_reset_by_dependency("course_info_" . $courseid);
                wscache_reset_by_dependency("course_content_" . $courseid);

                $DB->execute("DELETE FROM {monorail_data} WHERE type='courseprogress' AND itemid=?",
                    array($courseid));
            } else {
                //Update
                $keys=array_keys((array)$oldassignment);
                $keyid = array_search('id', $keys);
                if (false !== $keyid) {
                    unset($keys[$keyid]);
                }
                foreach($keys as $keytoupdate){
                    if(array_key_exists($keytoupdate, $assignment))
                        $oldassignment->$keytoupdate = $assignment[$keytoupdate];
                }
                $data = array();
                $data = (array)$oldassignment;
                $data['instance'] = $oldassignment->id;
                if ($oldassignment->nosubmissions == 0) {
                    $data['assignsubmission_onlinetext_enabled'] = 1;
                    $data['assignsubmission_file_maxfiles'] = 10;
                    $data['assignsubmission_file_enabled'] = 1;
                    $data['assignsubmission_file_maxsizebytes'] = $CFG->monorail_upload_limit;
                    $data['assignsubmission_comments_enabled'] = 0;
                    $data['assignfeedback_comments_enabled'] = 0;
                    $data['assignfeedback_file_enabled'] = 0;
                }
                $oldassignment = (object) $data;

                switch ($assignment["modulename"])
                {
                    case "assign":
                        // Update assignment.
                        require_once($CFG->dirroot . '/mod/assign/locallib.php');
                        $course = $DB->get_record('course', array('id'=>$oldassignment->course));
                        $mid = $DB->get_field('modules', 'id', array('name'=>'assign'));
                        $coursemodule =  $DB->get_record('course_modules', array('course'=>$course->id, 'instance'=>$oldassignment->id, 'module'=>$mid));
                        $assignup = new assign(null, $coursemodule, $course);
                        $retval = $assignup->update_instance($oldassignment, false);
                        if(!$retval) {
                            //User cannot update
                            $warning = array();
                            $warning['item'] = 'assignment';
                            $warning['itemid'] = $courseid;
                            $warning['warningcode'] = '1';
                            $warning['message'] = 'Assignment cannot be added or updated';
                            $warnings[] = $warning;
                            $rassignments[] = $rassign;
                            continue;
                        }
                        $cm = get_coursemodule_from_instance('assign', $assignup->get_instance()->id, $course->id, false, MUST_EXIST);
                        if(array_key_exists('visible', $assignment)) {
                          $DB->set_field('course_modules', 'visible', $assignment["visible"], array('id'=>$cm->id));
                        } else {
                            $assignment['visible'] = $cm->visible;
                        }
                        $rassign['assignid'] = $cm->id;
                        $rassign["id"] = $assignment["id"];
                        $rassignments[] = $rassign;

                        break;

                    case "quiz":
                        // Update quiz.
                        require_once($CFG->dirroot . '/mod/quiz/lib.php');
                        require_once($CFG->dirroot . '/mod/quiz/editlib.php');

                        $quizData = new stdClass();
                        $quizData->instance = $assignment["id"];
                        $quizData->name = $assignment["name"];
                        $quizData->timeclose = $assignment["duedate"];
                        $quizData->timeopen = $assignment["allowsubmissionsfromdate"];
                        $quizData->feedbacktext = array(array("text" => "", "format" => 1));
                        $quizData->course = $oldassignment->course;
                        $quizData->quizpassword = "";
                        if(isset($assignment["attempts"])) {
                          $quizData->attempts = $assignment["attempts"];
                        }
                        // Get current quiz questions.
                        $oldQuestions = $DB->get_records_sql("SELECT q.id, q.name, q.questiontext, qi.id AS instanceid" .
                            " FROM {question} AS q, {quiz_question_instances} AS qi WHERE q.id=qi.question AND qi.quiz=?",
                                array($assignment["id"]));

                        $oldQuestionIds = array();
                        $remainingOldQuestions = array();

                        foreach ($oldQuestions as $oq)
                        {
                            $oldQuestionIds[] = $oq->id;
                        }

                        try
                        {
                            quiz_update_instance($quizData);
                        }
                        catch (Exception $err)
                        {
                            if (get_class($err) == "file_exception")
                            {
                                // It's ok to fail with this (if user has
                                // facebook picture)
                            }
                            else
                            {
                                throw $err;
                            }
                        }

                        $category = question_make_default_categories(array($context));
                        $cm = get_coursemodule_from_instance('quiz', $assignment["id"], $oldassignment->course, false, MUST_EXIST);
                        if(array_key_exists('visible', $assignment)) {
                          $DB->set_field('course_modules', 'visible', $assignment["visible"], array('id'=>$cm->id));
                        } else {
                            $assignment['visible'] = $cm->visible;
                        }

                        // Update/create questions.
                        foreach ($assignment["questions"] as $questionData)
                        {
                            $questionData["text"] = str_replace(array("\r", "\n"), '',clean_param($questionData["text"], PARAM_RAW_TRIMMED));
                            $question = new stdClass();
                            $form = new stdClass();

                            $question->id = $questionData["id"];
                            $form->name = clean_param(mb_substr($questionData["text"], 0, 10), PARAM_TEXT);
                            $form->category = $category->id;
                            $form->single = 0;
                            $form->answernumbering = "abc";
                            $form->shuffleanswers = 0;
                            $form->answer = array();
                            $form->fraction = array();
                            $form->feedback = array();
                            $form->correctfeedback = array("format" => 1, "text" => clean_param($questionData["explanation"], PARAM_TEXT));
                            $form->partiallycorrectfeedback = array("format" => 1, "text" => (int) $questionData["type"]);
                            $form->incorrectfeedback = array("format" => 1, "text" => "");
                            $form->questiontext = array("format" => 1, "text" => preg_replace("/<script\b[^>]*>(.*?)<\/script>/is", "", $questionData["text"]));

                            $correctCount = 0;

                            foreach ($questionData["answers"] as $answerData)
                            {
                                if ($answerData["correct"])
                                {
                                    $correctCount++;
                                }
                            }

                            if ($correctCount > 0)
                            {
                                $correctPoints = 1.0 / $correctCount;
                            }
                            else
                            {
                                // whoops, return warning to UI
                                $warning = array();
                                $warning['item'] = 'quiz';
                                $warning['itemid'] = $quizData->id;
                                $warning['warningcode'] = '1';
                                $warning['message'] = 'No correct answers selected for question!';
                                $warnings[] = $warning;

                                continue;
                            }

                            foreach ($questionData["answers"] as $answerData)
                            {
                                $form->answer[] = array(
                                    "format" => 1,
                                    "text" => clean_param($answerData["text"], PARAM_RAW_TRIMMED));

                                $form->fraction[] = $answerData["correct"] ? $correctPoints : 0;

                                $form->feedback[] = array(
                                    "format" => 1,
                                    "text" => "");
                            }

                            $qtypeobj = question_bank::get_qtype("multichoice");
                            $question = $qtypeobj->save_question($question, $form);

                            if (in_array($question->id, $oldQuestionIds))
                            {
                                $remainingOldQuestions[] = $question->id;
                            }

                            quiz_add_quiz_question($question->id, $quizData);
                        }

                        foreach ($oldQuestionIds as $oq)
                        {
                            if (!in_array($oq, $remainingOldQuestions))
                            {
                                quiz_remove_question($quizData, $oq);

                                $qtypeobj = question_bank::get_qtype("multichoice");
                                $question = $qtypeobj->delete_question($oq, $context->id);
                            }
                        }

                        $rassign["id"] = $assignment["id"];
                        $rassign['assignid'] = $cm->id;
                        $rassignments[] = $rassign;
                        break;
                }

                if (isset($assignment["sectionid"]) && $assignment["sectionid"] && $assignment["sectionid"] != $cm->section) {
                    $oldSequence = $DB->get_field_sql("SELECT sequence FROM {course_sections} WHERE id=?", array($cm->section));
                    $oldSequence = explode(",", $oldSequence);
                    $oldSequence = array_diff($oldSequence, array("" . $cm->id));
                    $oldSequence = implode(",", array_filter($oldSequence));

                    $newSequence = $DB->get_field_sql("SELECT sequence FROM {course_sections} WHERE id=?", array($assignment["sectionid"]));
                    $newSequence = explode(",", $newSequence);
                    $newSequence = array_diff($newSequence, array("" . $cm->id));
                    $newSequence[] = $cm->id;
                    $newSequence = implode(",", array_filter($newSequence));

                    $DB->execute("UPDATE {course_sections} SET sequence=? WHERE id=?", array($oldSequence, $cm->section));
                    $DB->execute("UPDATE {course_sections} SET sequence=? WHERE id=?", array($newSequence, $assignment["sectionid"]));
                    $DB->execute("UPDATE {course_modules} SET section=? WHERE id=?", array($assignment["sectionid"], $cm->id));
                }

                rebuild_course_cache($oldassignment->course);

                if ((! isset($assignment['quiet']) || $assignment['quiet'] != 1) && ($assignment['visible'] != 0)) {
                    //send update events
                    $eventdata = new stdClass();
                    $eventdata->modulename = $assignment["modulename"];
                    $eventdata->name       = 'mod_updated';
                    $eventdata->cmid       = $cm->id;
                    $eventdata->courseid   = $oldassignment->course;
                    $eventdata->userid     = $USER->id;
                    events_trigger("mod_updated", $eventdata);
                }

                wscache_reset_by_dependency("course_content_" . $oldassignment->course);
            }

            $nolateData = $DB->get_record_sql("SELECT id, value FROM {monorail_data} WHERE type=? AND itemid=? AND datakey=?",
                array("TASKINFO", $rassign['assignid'], "nolatesubmissions"));

            if ($nolateData) {
                error_log(".... " . $assignment["nolatesubmissions"] . " - " . $nolateData->id . " - " . $rassign['assignid']);
                if ($nolateData->value != $assignment["nolatesubmissions"]) {
                    $DB->execute("UPDATE {monorail_data} SET value=? WHERE id=?",
                        array($assignment["nolatesubmissions"], $nolateData->id));
                }
            } else {
                $DB->execute("INSERT INTO {monorail_data} (type, itemid, datakey, value) VALUES (?, ?, ?, ?)",
                    array("TASKINFO", $rassign['assignid'], "nolatesubmissions", $assignment["nolatesubmissions"]));
            }

            if (isset($assignment["options"])) {
                monorail_data('TEXT', $rassign["assignid"], 'task_options', $assignment['options'], true);
            }
        }
        $result = array();
        $result['assignments'] = $rassignments;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for update_assignments
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_assignments_returns() {
        return new external_single_structure(
            array(
                'assignments' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'courseid' => new external_value(PARAM_ALPHANUM, 'Course id', VALUE_OPTIONAL),
                            'assignid' => new external_value(PARAM_INT, 'Assignment Id', VALUE_OPTIONAL),
                            'id' => new external_value(PARAM_INT, 'Real assignment Id')
                        )),'Course and assignment id'),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function delete_video_resource($entryid)
    {
        global $CFG, $USER, $DB;
        require_once($CFG->dirroot . '/local/kaltura/locallib.php');
        $kclient = local_kaltura_login(true);
        if ($kclient) {
          try {
            add_to_log(1, 'monorail', 'externallib.del_assignment', '', 'Deleting task video rsc :'.$entryid);
            $DB->delete_records('monorail_course_videorsc', array('entryid'=>$entryid));
            $resp = $kclient->media->delete($entryid);
          } catch (Exception $ex) {
             //Maybe failed to delete
             $vfdata = new stdClass();
             $vfdata->entryid  = $entryid;
             $DB->insert_record('monorail_course_delvideorsc', $vfdata);
             add_to_log(1, 'monorail', 'externallib.del_course', '', 'ERROR: '.$ex->getMessage());
          }
        } else {
             //Need to be deleted manually
             $vfdata = new stdClass();
             $vfdata->entryid  = $entryid;
             $DB->insert_record('monorail_course_delvideorsc', $vfdata);
             $DB->delete_records('monorail_course_videorsc', array('entryid'=>$entryid));
        }
    }

    public static function add_video_resource($token, $filename)
    {
        global $CFG, $USER, $DB;
        require_once($CFG->dirroot . '/local/kaltura/locallib.php');
        $kclient = local_kaltura_login(true);
        $result = null;
        if ($kclient) {
          try {
           $entry = new KalturaMediaEntry();
           $entry->name = $filename;
           $entry->mediaType = KalturaMediaType::VIDEO;
           $result = $kclient->media->addFromUploadedFile($entry, $token);
          } catch (Exception $ex) {
             //Maybe failed to add
             add_to_log(1, 'monorail', 'externallib.add_video_resource', '', 'ERROR: '.$ex->getMessage());
          }
        }
        return $result;
    }

    public static function delete_assignments_parameters()
    {
        return new external_function_parameters(
                array(
                        'assignids' => new external_multiple_structure(
                                new external_value(PARAM_INT, 'assignment id'),
                                '0 or more assignment ids',
                                VALUE_DEFAULT, array())
                )
        );
    }

    /**
     * Delete assignments
     * @param array of  $ids
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function delete_assignments($assignids) {
        global $CFG, $USER, $DB;
        $params = self::validate_parameters(self::delete_assignments_parameters(),
                array('assignids' => $assignids));

        $warnings = array();

        require_once($CFG->dirroot . '/course/lib.php');
        foreach ($assignids as $assignid)
        {
            $instanceInfo = $DB->get_record_sql("SELECT cm.instance AS id, m.name AS module".
                " FROM {course_modules} AS cm, {modules} AS m WHERE m.id=cm.module AND cm.id=?",
                array($assignid), MUST_EXIST);

            $cm = get_coursemodule_from_instance($instanceInfo->module, $instanceInfo->id, 0, false, MUST_EXIST);
            $context = context_module::instance($cm->id);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            }catch (Exception $e) {
                    $warning = array();
                    $warning['item'] = 'course';
                    $warning['itemid'] = $instanceInfo->id;
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'No access rights in course context';
                    $warnings[] = $warning;
                    continue;
            }

            switch ($instanceInfo->module)
            {
                case "assign":
                    require_once($CFG->dirroot . '/mod/assign/locallib.php');
                    $assignment = new assign($context, null, null);
                    $retval = $assignment->delete_instance();
                    if(!$retval) {
                        $warning = array();
                        $warning['item'] = 'assign';
                        $warning['itemid'] = $instanceInfo->id;
                        $warning['warningcode'] = '1';
                        $warning['message'] = 'Unable to delete assignment';
                        $warnings[] = $warning;
                        continue;
                    } else {
                      delete_course_module($cm->id);
                    }
                    break;

                case "quiz":
                    require_once($CFG->dirroot . '/mod/quiz/lib.php');

                    if (!quiz_delete_instance($instanceInfo->id))
                    {
                        $warning = array();
                        $warning['item'] = 'quiz';
                        $warning['itemid'] = $instanceInfo->id;
                        $warning['warningcode'] = '1';
                        $warning['message'] = 'Unable to delete quiz';
                        $warnings[] = $warning;
                        continue;
                    } else {
                      delete_course_module($cm->id);
                    }
                    break;
            }
            //Delete the video resources here, taskid here is the module id
            $vrecords = $DB->get_records("monorail_assign_kalvidres", array("courseid"=>$courseid, "taskid" => $cm->id));
            foreach ($vrecords as $vrecord) {
                //delete entry from kaltura here, if fail keep a record
                self::delete_video_resource($vrecord->entryid);
            }

            rebuild_course_cache($cm->course);
            //send update events
            $eventdata = new stdClass();
            $eventdata->modulename = $instanceInfo->module;
            $eventdata->name       = 'mod_deleted';
            $eventdata->cmid       = $cm->id;
            $eventdata->instance   = $instanceInfo->id;     # extra attr needed for finding revision in monorailfeed
            $eventdata->courseid   = $cm->course;
            $eventdata->userid     = $USER->id;
            events_trigger("mod_deleted", $eventdata);

            wscache_reset_by_dependency("course_tasks_" . $cm->course);
        }
        return $warnings;
    }

    /**
     * Describes the return value for delete_assignments
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function delete_assignments_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function update_courses_parameters() {
        $courseconfig = get_config('moodlecourse'); //needed for many default values
        return new external_function_parameters(
            array(
                'courses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'Id'),
                            'fullname' => new external_value(PARAM_RAW, 'full name', VALUE_OPTIONAL),
                            'shortname' => new external_value(PARAM_TEXT, 'course short name', VALUE_OPTIONAL),
                            'categoryid' => new external_value(PARAM_INT, 'category id', VALUE_OPTIONAL),
                            'idnumber' => new external_value(PARAM_INT, 'id number', VALUE_OPTIONAL),
                            'summary' => new external_value(PARAM_RAW, 'summary', VALUE_OPTIONAL),
                            'summaryformat' => new external_format_value('summary', VALUE_OPTIONAL),
                            'format' => new external_value(PARAM_PLUGIN,
                                'course format: weeks, topics, social, site,..',
                                VALUE_DEFAULT, $courseconfig->format),
                            'showgrades' => new external_value(PARAM_INT,
                                '1 if grades are shown, otherwise 0', VALUE_OPTIONAL,
                                $courseconfig->showgrades),
                            'newsitems' => new external_value(PARAM_INT,
                                'number of recent items appearing on the course page',
                                VALUE_DEFAULT, $courseconfig->newsitems),
                            'startdate' => new external_value(PARAM_INT,
                                'timestamp when the course starts', VALUE_OPTIONAL),
                            'enddate' => new external_value(PARAM_INT,
                                'timestamp when the course ends', VALUE_OPTIONAL),
                            'numsections' => new external_value(PARAM_INT, 'number of weeks/topics',
                                VALUE_OPTIONAL),
                            'maxbytes' => new external_value(PARAM_INT,
                                'largest size of file that can be uploaded into the course',
                                VALUE_DEFAULT, $courseconfig->maxbytes),
                            'showreports' => new external_value(PARAM_INT,
                                'are activity report shown (yes = 1, no =0)', VALUE_OPTIONAL,
                                $courseconfig->showreports),
                            'visible' => new external_value(PARAM_INT,
                                '1: available to student, 0:not available', VALUE_OPTIONAL),
                            'hiddensections' => new external_value(PARAM_INT,
                                'How the hidden sections in the course are displayed to students',
                                VALUE_DEFAULT, $courseconfig->hiddensections),
                            'groupmode' => new external_value(PARAM_INT, 'no group, separate, visible',
                                VALUE_DEFAULT, $courseconfig->groupmode),
                            'groupmodeforce' => new external_value(PARAM_INT, '1: yes, 0: no',
                                VALUE_DEFAULT, $courseconfig->groupmodeforce),
                            'defaultgroupingid' => new external_value(PARAM_INT, 'default grouping id',
                                VALUE_DEFAULT, 0),
                            'enablecompletion' => new external_value(PARAM_INT,
                                'Enabled, control via completion and activity settings. Disabled,
                                not shown in activity settings.',
                                VALUE_OPTIONAL),
                            'completionstartonenrol' => new external_value(PARAM_INT,
                                '1: begin tracking a student\'s progress in course completion after
                                course enrolment. 0: does not',
                                VALUE_OPTIONAL),
                            'completionnotify' => new external_value(PARAM_INT,
                                '1: yes 0: no', VALUE_OPTIONAL),
                            'lang' => new external_value(PARAM_SAFEDIR,
                                'forced course language', VALUE_OPTIONAL),
                            'forcetheme' => new external_value(PARAM_PLUGIN,
                                'name of the force theme', VALUE_OPTIONAL),
                            'coursebackground' => new external_value(PARAM_TEXT,
                                'course backgrounf file name', VALUE_OPTIONAL),
                            'course_privacy' => new external_value(PARAM_INT, 'Course public=1, private=2', VALUE_OPTIONAL),
                            'course_access' => new external_value(PARAM_INT, 'course access draft=1, invite=2, open=3', VALUE_OPTIONAL),
                            'course_logo' => new external_value(PARAM_TEXT, 'course logo URL', VALUE_OPTIONAL),
                            'mainteacher' => new external_value(PARAM_INT, 'Main teacher for course page view', VALUE_OPTIONAL),
                            'course_price' => new external_value(PARAM_NUMBER, 'Price for course enrollment', VALUE_OPTIONAL),
                            'course_paypal_account' => new external_value(PARAM_TEXT, 'PayPal account that receives payments for course', VALUE_OPTIONAL),
                            'catalogfeature' => new external_value(PARAM_INT, 'If course was submitted for featured', VALUE_OPTIONAL),
                            'blacklistremove' => new external_value(PARAM_INT, 'If course was submitted for blacklist removal', VALUE_OPTIONAL),
                            'cert_enabled' => new external_value(PARAM_INT, 'Are certificates enabled for this course', VALUE_OPTIONAL),
                            'language' => new external_value(PARAM_TEXT, 'Course language', VALUE_OPTIONAL),
                            'course_country' => new external_value(PARAM_TEXT, 'Course country', VALUE_OPTIONAL),
                            'keywords' => new external_value(PARAM_TEXT, 'Course keywords', VALUE_OPTIONAL),
                            'licence' => new external_value(PARAM_INT, 'Course licence', VALUE_OPTIONAL),
                            'participants_hidden' => new external_value(PARAM_INT, 'Participants hidden', VALUE_OPTIONAL),
                            'cert_custom' => new external_value(PARAM_INT, 'Custom certificates', VALUE_OPTIONAL),
                            'cert_custom_info' => new external_value(PARAM_TEXT, 'Custom certificate info', VALUE_OPTIONAL)
                        )
                    ), 'courses to update'
                )
            )
        );
    }

    /**
     * Update  courses
     *
     * @param array $courses
     * @return array courses (id and shortname only)
     * @since Moodle 2.2
     */
    public static function update_courses($courses) {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . '/course/lib.php');

        $params = self::validate_parameters(self::update_courses_parameters(),
            array('courses' => $courses));

        $nonupdatekeys = array('shortname');
        foreach ($params['courses'] as $course) {
            $courseinfo = $DB->get_record('course', array('id' => $course['id']), "*", MUST_EXIST);
            // Ensure the current user is allowed to run this function
            $context = get_context_instance(CONTEXT_COURSE, $courseinfo->id);
            try {
                self::validate_context($context);
                require_capability('moodle/course:update', $context);
            } catch (Exception $e) {
                $exceptionparam = new stdClass();
                $exceptionparam->message = $e->getMessage();
                $exceptionparam->catid = $course['id'];
                throw new moodle_exception('errorcontextnotvalid', 'webservice', '', $exceptionparam);
            }

            if (array_key_exists('fullname', $course)) {
              $course['fullname'] = clean_param($course['fullname'], PARAM_TEXT);
            }
            $keys=array_keys((array)$courseinfo);
            $keyid = array_search('id', $keys);
            if (false !== $keyid) {
                unset($keys[$keyid]);
            }
            foreach($keys as $keytoupdate){
                if(array_key_exists($keytoupdate, $course) && !in_array($keytoupdate,$nonupdatekeys))
                    $courseinfo->$keytoupdate = $course[$keytoupdate];
            }

            $data = (array)$courseinfo;
            if(array_key_exists('categoryid', $course))
                $data['category'] = $course['categoryid'];
            if(array_key_exists('summaryformat', $course))
                $data['summaryformat'] = external_validate_format($course['summaryformat']);

            update_course((object) $data);
            rebuild_course_cache($courseinfo->id);

            // Update course background to database
            $result = null;
            if (isset($course['coursebackground'])) {
            	try {
	            	$DB->delete_records('monorail_data', array('type'=>'TEXT', 'itemid'=>$course['id'], 'datakey'=>'coursebackground'));

	            	$data = new stdClass();
	            	$data->type 		= 'TEXT';
	            	$data->itemid 		= $course['id'];
	            	$data->datakey 		= 'coursebackground';
	            	$data->value 		= $course['coursebackground'];
	            	$data->timemodified = time();
	            	$result = $DB->insert_record('monorail_data', $data);

            	} catch (Exception $e) {
            		$result = false;
            	}
            }

            // Update course logo to database
            if (isset($course['course_logo'])) {
                monorail_data('TEXT', $course['id'], 'course_logo', $course['course_logo'], true);
            }
            
            $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$course['id']), '*', MUST_EXIST);
            $coursedatachanged = false;
            // Update end date if given
            if (isset($course['enddate']) && $course['enddate'] != $coursedata->enddate) {
                $coursedata->enddate = $course['enddate'];
                $coursedatachanged = true;
            }
            // Update mainteacher if given
            if (isset($course['mainteacher']) && $course['mainteacher'] !== 0 && $course['mainteacher'] != $coursedata->mainteacher) {
                $coursedata->mainteacher = $course['mainteacher'];
                $coursedatachanged = true;
            }
            // Update licence if given
            if (isset($course['licence']) && $course['licence'] != $coursedata->licence) {
                $coursedata->licence = $course['licence'];
                $coursedatachanged = true;
            }
            if ($coursedatachanged) {
                $coursedata->timemodified = time();
                $DB->update_record('monorail_course_data', $coursedata);
                if ($coursedata->enddate && $coursedata->enddate < time()) {
                    // course has ended, do that too
                    monorail_process_course_end($course['id']);
                }
            }

            // Pulling some information about the course...
            $teacher = $DB->get_record('user', array('id'=>$coursedata->mainteacher));
            require_once($CFG->dirroot . '/course/lib.php');
            require_once($CFG->dirroot . '/user/lib.php');

            $teacherDetails = user_get_user_details($teacher);
            $teacherDetails["profileimageurl"] = "$CFG->wwwroot/public_images/user_picture/".monorail_get_userpic_hash($teacher->id)."_32.jpg";

            $invite = $DB->get_records_sql("SELECT id, code FROM {monorail_invites} WHERE type='course' AND active AND targetid=?",
                array($course['id']));
            $invite = reset($invite);

            $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($course['id']));
            $monorailData = array();

            foreach ($moreData as $md)
            {
                $monorailData[$md->datakey] = $md->value;
            }

            $enrolledUsers = array();
            $students = $DB->get_records_sql("SELECT DISTINCT u.id FROM {user} AS u, {role_assignments} AS a" .
                    " WHERE a.userid=u.id AND a.contextid=?", array($context->id));

            foreach ($students as $student)
            {
                $enrolledUsers[] = $student->id;
            }

            $courseinvites = monorail_get_or_create_invitation('course', $course["id"]);

            if (isset($course["course_privacy"]) && isset($course["course_access"]))
            {
                monorail_update_course_perms($course["id"], $course['course_access'], $course['course_privacy']);
            }

            $cperm = monorail_get_course_perms($course["id"]);

            $monorailData["course_privacy"] = $cperm['course_privacy'];
            $monorailData["course_access"] = $cperm['course_access'];

            if (isset($course["course_price"]))
            {
                monorail_data('FLOAT', $course['id'], 'course_price', $course['course_price'], true);

                $monorailData["course_price"] = $course['course_price'];
            }

            if (isset($course["course_paypal_account"]))
            {
                monorail_data('TEXT', $course['id'], 'course_paypal_account', $course['course_paypal_account'], true);

                $monorailData["course_paypal_account"] = $course['course_paypal_account'];
            }

            if (isset($course["cert_enabled"]))
            {
                monorail_data('BOOLEAN', $course['id'], 'cert_enabled', $course['cert_enabled'], true);

                $monorailData["cert_enabled"] = $course['cert_enabled'];
            }

            if (isset($course['cert_custom'])) {
                monorail_data('BOOLEAN', $course['id'], 'cert_custom', $course['cert_custom'], true);
            }

            if (isset($course['cert_custom_info'])) {
                monorail_data('TEXT', $course['id'], 'cert_custom_info', $course['cert_custom_info'], true);
            }

            if (isset($course["language"]))
            {
                monorail_data('TEXT', $course['id'], 'language', $course['language'], true);

                $monorailData["language"] = $course['language'];
            }

            if (isset($course["course_country"])) {
                monorail_data('TEXT', $course['id'], 'course_country', $course['course_country'], true);

                $monorailData["course_country"] = $course['course_country'];
            }

            if (isset($course["keywords"]))
            {
                monorail_data('TEXT', $course['id'], 'keywords', $course['keywords'], true);

                $monorailData["keywords"] = $course['keywords'];
            }

            if (isset($course["participants_hidden"]))
            {
                monorail_data('INTEGER', $course['id'], 'participants_hidden', $course['participants_hidden'], true);

                $monorailData["participants_hidden"] = $course['participants_hidden'];
            }

            if (isset($course["blacklistremove"]))
            {
                 mail("catalog_request@eliademy.com", "Course for removal of black listing",
                      "A teacher has requested his/her course to be removed from blaclisting.\n\n" .
                      "Course: " . $courseinfo->fullname . " (" . $coursedata->code . " id: " . $course['id'] . ")\n" .
                      "Teacher: " . $teacher->username . " (" . $teacher->email . " id: " . $coursedata->mainteacher . ")\n",
                      "From: magento@eliademy.com\r\n" .
                      "Reply-To: magento@eliademy.com\r\n");
            }
            if (isset($course["catalogfeature"]))
            {
              $cperm = monorail_get_course_perms($course["id"]);
              if (($course["course_privacy"] == 1) && ($course["course_access"] == 3) && !$cperm["review"])
              {
                    monorail_set_course_review($course["id"], 1);
                    mail("catalog_request@eliademy.com", "Course for listing in public catalog",
                         "A teacher has requested his/her course to be listed in public catalog.\n\n" .
                         "Course: " . $courseinfo->fullname . " (" . $CFG->landing_page . "/" . $coursedata->code . " id: " . $course['id'] . ")\n" .
                         "Teacher: " . $teacher->username . " (" . $teacher->email . " id: " . $coursedata->mainteacher . ")\n",
                         "From: magento@eliademy.com\r\n" .
                         "Reply-To: magento@eliademy.com\r\n");
                    monorail_data('TEXT', $course['id'], 'review_submit_time', userdate(time(), "%d.%m.%Y, %H:%M", $USER->timezone), true);
              }
            }

            if (array_key_exists('categoryid', $course))
            {
                $courseinfo->category = $course['categoryid'];
            }

            wscache_reset_by_dependency("course_info_" . $courseinfo->id);

            foreach ($enrolledUsers as $uid) {
                wscache_reset_by_user($uid);
            }

            $resultcourses[] = array('id' => $courseinfo->id, 'shortname' => $courseinfo->shortname, 'coursebackground' => $result);

            addBackgroundCall(function () use ($coursedata, $monorailData, $courseinfo)
            {
                require_once __DIR__ . "/mageapi.php";

                magento_update_course($coursedata, $monorailData, $courseinfo);

                self::update_public_profile_page($coursedata->mainteacher);
            });
        }

        return $resultcourses;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function update_courses_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'       => new external_value(PARAM_INT, 'course id'),
                    'shortname' => new external_value(PARAM_TEXT, 'short name'),
                	'coursebackground' => new external_value(PARAM_TEXT, 'course background', false),
                )
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_assignments_parameters() {
        return new external_function_parameters(
            array(
                'courseids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'course id'),
                    '0 or more course ids',
                    VALUE_DEFAULT, array()),
                'capabilities'  => new external_multiple_structure(
                    new external_value(PARAM_CAPABILITY, 'capability'),
                    'ANDed list of capabilities used to filter courses',
                    VALUE_DEFAULT, array())
            )
        );
    }

    /**
     * Return assignments for courses in which the user is enrolled, optionally filtered
     * by course id and/or capability
     * @param array of ints $courseids specify course ids to be returned
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return array of courses and warnings
     * @since  Moodle 2.4
     */
    public static function get_assignments($courseids = array(), $capabilities = array()) {
        global $USER, $DB, $CFG;

        require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');

        $params = self::validate_parameters(self::get_assignments_parameters(),
                        array('courseids' => $courseids, 'capabilities' => $capabilities));

        $warnings = array();
        $courses = array();
        $fields = 'sortorder,shortname,fullname,timemodified,startdate';
        
        //~ $courses = enrol_get_users_courses($USER->id, true, $fields);
        // TEMP HACK to maybe fix #427 - above line is correct
        $courses = enrol_get_users_courses($USER->id, false, $fields);
        //~ add_to_log(1, 'monorail', 'externallib.get_assignments', '', var_export($courses, true));
        // Used to test for ids that have been requested but can't be returned.
        $unavailablecourseids = array();
        if (count($params['courseids'])>0) {
            $unavailablecourseids = $params['courseids'];
            foreach ($courses as $id => $course) {
                $found = in_array($id, $params['courseids']);
                if ($found) {
                    $unavailablecourseids = array_diff($unavailablecourseids, array($id));
                } else {
                    unset($courses[$id]);
                }
            }
        }

        foreach ($courses as $id => $course) {
            $context = context_course::instance($id);
            try {
                self::validate_context($context);
                require_capability('moodle/course:viewparticipants', $context);
            } catch (Exception $e) {
                unset($courses[$id]);
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context';
                $warnings[] = $warning;
                continue;
            }
            foreach ($params['capabilities'] as $cap) {
                if (!has_capability($cap, $context)) {
                    unset($courses[$id]);
                }
            }
        }

        $assign_module_info = $DB->get_record("modules", array("name" => "assign"));
        $quiz_module_info = $DB->get_record("modules", array("name" => "quiz"));

        $coursearray = array();
        require_once($CFG->libdir . '/gradelib.php');
        foreach ($courses as $id => $course) {
            $totaltasks = 0;
            $completedtasks = 0;
            $coursecontext = context_course::instance($id);
            $canedit = (has_capability('moodle/course:manageactivities', $coursecontext)) ? 1 : 0;
            if($canedit){
               $role = $DB->get_record('role', array('shortname' => 'student'));
               $students = get_role_users($role->id, $coursecontext);
               $usercount = count($students); 
            }
            $assignmentarray = array();
            $quizarray = array();
            // Get a list of assignments for the course.

            $extrafields='m.id as assignmentid, m.course, m.nosubmissions, m.submissiondrafts, m.sendnotifications, '.
                         'm.sendlatenotifications, m.duedate, m.allowsubmissionsfromdate, m.grade, m.timemodified,
                          m.intro, m.introformat ';

            $modules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, $extrafields
                                   FROM {course_modules} cm, {modules} md, {assign} m
                                   WHERE cm.course = $id AND
                                        cm.instance = m.id AND
                                        md.name = 'assign' AND
                                        md.id = cm.module ORDER BY m.duedate ASC", array());
            if ($modules) {
                foreach ($modules as $module) {
                    $context = context_module::instance($module->id);
                    try {
                        self::validate_context($context);
                        require_capability('mod/assign:view', $context);
                    } catch (Exception $e) {
                        $warning = array();
                        $warning['item'] = 'module';
                        $warning['itemid'] = $module->id;
                        $warning['warningcode'] = '1';
                        $warning['message'] = 'No access rights in module context';
                        $warnings[] = $warning;
                        continue;
                    }
                    $configarray = array();

                    $instance_rec = @reset($DB->get_records("course_modules", array("course" => $module->course, "instance" => $module->assignmentid, "module" => $assign_module_info->id)));
                    $section_rec = @reset($DB->get_records("course_sections", array("id"=>$instance_rec->section)));

                    try {
                        $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$module->assignmentid, 'userid'=> $USER->id), '*', MUST_EXIST);
                        if ($submitinfo->status === 'draft') {
                            throw new Exception("");
                        }
                        //ok get the num of files submitted
                        $submitfile = $DB->get_record('assignsubmission_file', array('assignment'=>$module->assignmentid, 'submission'=> $submitinfo->id));
                        $submittext = $DB->get_record('assignsubmission_onlinetext', array('assignment'=>$module->assignmentid, 'submission'=> $submitinfo->id));
                        if (isset($submittext->onlinetext) && strlen($submittext->onlinetext) > 0) {
                            if (! isset($submitfile) || ! $submitfile) {
                                $submitfile = new stdClass();
                                $submitfile->numfiles = 0;
                            }
                            $submitfile->numfiles += 1;
                        }
                        $haveSubmissions = ($submitfile->numfiles > 0 ? 1 : 0);
                    } catch (Exception $e) {
                        //No submissions
                        $haveSubmissions = 0;
                    }
                    if ($canedit) {
                        $gradedCount = $DB->count_records('assign_grades', array('assignment'=>$module->assignmentid));
                        $submitcount = 0;
                        $submitusers = $DB->get_records('assign_submission', array('assignment'=>$module->assignmentid),'userid');
                        $course_context = context_course::instance($module->course);
                        $mroleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST); 
                        foreach ($submitusers as $submituser) {
                            if ($submituser->status === 'submitted') {
                                if (is_enrolled($course_context, $submituser->userid) && user_has_role_assignment($submituser->userid, $mroleid, $course_context->id)) {

                                        $submitcount = $submitcount + 1;
                                }
                            }
                        } 
                    }

                    $gradearray = array();
                    $grading_info = grade_get_grades($module->course, 'mod', 'assign', $module->assignmentid, $USER->id);
                    $gradingitem = $grading_info->items[0];
                    $gradebookgrade = $gradingitem->grades[$USER->id];
                    $gradearray[] = array(
                                 'grade'=> (($gradebookgrade->grade)?intval($gradebookgrade->grade):null),
                                 'grademax'=> intval($gradingitem->grademax),
                                 'gradestring'=> str_replace(' ','',get_string('gradelong', 'grades', array('grade'=>intval($gradebookgrade->grade),
                                 'max'=>intval($gradingitem->grademax)))),
                                 'feedback' => $gradebookgrade->str_feedback,
                                 'dategraded' => $gradebookgrade->dategraded
                                );

                    $assignarray= array('id'=>$module->assignmentid,
                        'cmid'=>$module->id,
                        'instanceid'=>$instance_rec->id,
                        'course'=>$module->course,
                        'visible' => $module->visible,
                        'name'=>$module->name,
                        'nosubmissions'=>$module->nosubmissions,
                        'submissiondrafts'=>$module->submissiondrafts,
                        'sendnotifications'=>$module->sendnotifications,
                        'sendlatenotifications'=>$module->sendlatenotifications,
                        'duedate'=>$module->duedate,
                        'allowsubmissionsfromdate'=>$module->allowsubmissionsfromdate,
                        'grade'=>$module->grade,
                        'timemodified'=>$module->timemodified,
                        'intro' => $module->intro,
                        'introformat' => $module->introformat,
                        'grades' => $gradearray,
                        'gradedcount' => ($canedit) ? $gradedCount : 0,
                        'submitcount' => ($canedit) ? @$submitCount : 0,
                        'fileinfo' => array(),
                        'canedit'=>$canedit,
                        'haveSubmissions'=>$haveSubmissions,
                        'section_visible'=>$section_rec->visible
                    );
                    if($canedit){
                        $assignarray['submitcount'] = $submitcount;
                    } 
                    $assignmentarray[]= $assignarray; 
                    // count completion
                    if (! $canedit) {
                        $totaltasks = $totaltasks +1;
                        if (! $module->nosubmissions) {
                            if ($gradearray[0]['dategraded']) {
                                $completedtasks = $completedtasks +1;
                            }
                        } else if ($module->duedate < time()) {
                            $completedtasks = $completedtasks +1;
                        }
                    }
                }
            }

            $extrafields='m.id as assignmentid, m.course, m.timemodified, m.timeclose AS duedate ';
            $qmodules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, $extrafields
                                   FROM {course_modules} cm, {modules} md, {quiz} m
                                   WHERE cm.course = $id AND
                                        cm.instance = m.id AND
                                        md.name = 'quiz' AND
                                        md.id = cm.module ORDER BY m.timeclose ASC", array());

            // Get a list of quizes for the course.
            if ($modules = array_merge($qmodules)) {
                foreach ($modules as $module) {
                    $context = context_module::instance($module->id);
                    try {
                        self::validate_context($context);
                        require_capability('mod/assign:view', $context);
                    } catch (Exception $e) {
                        $warning = array();
                        $warning['item'] = 'module';
                        $warning['itemid'] = $module->id;
                        $warning['warningcode'] = '1';
                        $warning['message'] = 'No access rights in module context';
                        $warnings[] = $warning;
                        continue;
                    }
                    $configarray = array();

                    $instance_rec = @reset($DB->get_records("course_modules", array("course" => $module->course, "instance" => $module->assignmentid, "module" => $quiz_module_info->id)));

                    // quizzes have no grades, just push out an empty record since our UI might need this record to exist..
                    $gradearray = array(
                        array(
                         'grade'=> 0,
                         'grademax'=> 0,
                         'gradestring'=> str_replace(' ','',get_string('gradelong', 'grades', array('grade'=>0, 'max'=>0))),
                         'feedback' => null,
                         'dategraded' => null
                        )
                    );

                    $submitCount = 0;
                    $gradedCount = 0;
                    $haveSubmissions = 0;

                    if ($canedit) {
                        $gradedCount = $DB->count_records('quiz_grades', array('quiz'=>$module->assignmentid));
                        $quiztakers = $DB->get_records('monorail_data',  array("itemid" => $module->assignmentid, "type" => 'quiz_attempt'));
                        $mroleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST);
                        $course_context = context_course::instance($module->course);

                        foreach ($quiztakers as $quiztaker) {
                            $qtid = substr($quiztaker->datakey, 4);

                            if (!is_enrolled($course_context, $qtid) || !user_has_role_assignment($qtid, $mroleid, $course_context->id)) {
                                continue;
                            }

                            $stateData = monorail_data('quiz_end', $module->assignmentid, "user" . $qtid);

                            if ($stateData) {
                                $stateData = reset($stateData);
                                if ((int) $stateData->value) {
                                    $submitCount++;
                                }
                            }
                        }
                    } else {
                        $stateData = monorail_data('quiz_end', $module->assignmentid, "user" . $USER->id);
                        if ($stateData) {
                            $stateData = reset($stateData);
                            if ((int) $stateData->value) {
                                $haveSubmissions = 1;
                            }
                        }
                    }

                    if ($haveSubmissions) {
                        $attemptData = monorail_data('quiz_attempt', $module->assignmentid, "user" . $USER->id);
                        $attemptData = reset($attemptData);
                        $attemptData = unserialize($attemptData->value);

                        $quizTotal = 0;
                        $quizCorrect = 0;

                        foreach ($attemptData as $question)
                        {
                            $allOk = true;

                            if (isset($question["correct"])) {
                                $allOk = $question["correct"];
                            } else {
                                foreach ($question["answers"] as $answer)
                                {
                                    if ($answer["correct"] != $answer["checked"])
                                    {
                                        $allOk = false;
                                    }
                                }
                            }

                            $quizTotal++;

                            if ($allOk)
                            {
                                $quizCorrect++;
                            }
                        }

                        $lastscore = $quizCorrect . '/' . $quizTotal;
                        $bestAttemptData = monorail_data('quiz_attempt_best', $module->assignmentid, "user" . $USER->id);
                        //Score is of best attempt if available
                        if($bestAttemptData) {
                          $bestAttemptData = reset($bestAttemptData);
                          $bestAttemptData = unserialize($bestAttemptData->value);
                          $quizTotal = 0;
                          $quizCorrect = 0;

                          foreach ($bestAttemptData as $question)
                          {
                            $allOk = true;

                            foreach ($question["answers"] as $answer)
                            {
                                if ($answer["correct"] != $answer["checked"])
                                {
                                    $allOk = false;
                                }
                            }

                            $quizTotal++;

                            if ($allOk)
                            {
                                $quizCorrect++;
                            }
                          }
                        } 
                        $score = $quizCorrect . '/' . $quizTotal;
                    } else {
                        $score = "";
                        $lastscore = "";
                    }
                    $attempts = $DB->get_field('quiz', 'attempts' , array('id'=>$module->assignmentid));
                    $attemptscount  = reset(monorail_data('quiz_attempts_count', $module->assignmentid, "user" . $USER->id));
                    if(!$attemptscount) {
                        $attemptscount = 0;
                    } else {
                        $attemptscount = $attemptscount->value;
                    }

                    $endquiz  = reset(monorail_data('quiz_end', $module->assignmentid, "user" . $USER->id));
                    if(!$endquiz) {
                        $endquiz = 0;
                    } else {
                        $endquiz = $endquiz->value;
                    } 

                    $quizarray[]= array('id'=>$module->assignmentid,
                                              'cmid'=>$module->id,
                                              'instanceid'=>$instance_rec->id,
                                              'course'=>$module->course,
                                              'name'=>$module->name,
                                              'visible' => $module->visible,
                                              'nosubmissions'=>0,
                                              'submissiondrafts'=>(isset($module->submissiondrafts)) ? $module->submissiondrafts : null,
                                              'sendnotifications'=>(isset($module->sendnotifications)) ? $module->sendnotifications : null,
                                              'sendlatenotifications'=>(isset($module->sendlatenotifications)) ? $module->sendlatenotifications : null,
                                              'duedate'=>$module->duedate,
                                              'allowsubmissionsfromdate'=>(isset($module->allowsubmissionsfromdate)) ? $module->allowsubmissionsfromdate : null,
                                              'grade'=>(isset($module->grade)) ? $module->grade : null,
                                              'grades' => $gradearray,
                                              'timemodified'=>$module->timemodified,
                                              'gradedcount' => ($canedit) ? $gradedCount : 0,
                                              'submitcount' => ($canedit) ? $submitCount : 0,
                                              'haveSubmissions' => $haveSubmissions,
                                              'canedit' => $canedit,
                                              'attempts' =>  $attempts,
                                              'attemptsleft' => max(($attempts - $attemptscount), 0),
                                              'endquiz' => $endquiz,
                                              'lastscore' => $lastscore,
                                              'score' => $score
                    );
                    // count completion
                    if (! $canedit) {
                        $totaltasks = $totaltasks +1;
                        if ($haveSubmissions) {
                            $completedtasks = $completedtasks +1;
                        }
                    }
                }
            }

            $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($courses[$id]->id));
            $monorailData = array();

            foreach ($moreData as $md)
            {
                $monorailData[$md->datakey] = $md->value;
            }

            // Load background picture
            $backgroundimage = @$monorailData["coursebackground"];

            if (!$backgroundimage)
            {
                $backgroundimage = "/";
            }

            // Load course logo
            $course_logo = @$monorailData["course_logo"];

            if (!$course_logo)
            {
                $course_logo = "/";
            }

            // invitations
            $courseinvites = monorail_get_or_create_invitation('course', $courses[$id]->id);
            $courseinviteurl = monorail_format_invite_link($courseinvites->code);

            $canedit = (has_capability('moodle/course:update', $coursecontext)) ? 1 : 0;

            $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$courses[$id]->id));

            if ($courses[$id]->startdate)
            {
                $datestring = userdate($courses[$id]->startdate, "%d %b %Y")." - "; 
                if($courseData->enddate) {
                   $datestring .= userdate($courseData->enddate, "%d %b %Y"); 
                }
            }
            else
            {
                $datestring = get_string("self_paced", "theme_monorail");
            }

            $cohortid = 0;//init
            $cohort = $DB->get_field('monorail_cohort_courseadmins', 'cohortid', array('courseid'=>$courses[$id]->id), IGNORE_MULTIPLE); 
            if($cohort) {
                $cohortid=$cohort;
                //Ok cohort course can have privacy settings set by admin which teacher cannot change 
                $csetting = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohortid, 'name' => 'cohort_course_privacy')); 
                $ccprivate = (!empty($csetting)) ? $csetting->value: 0;
                $csetting = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohortid, 'name' => 'cohort_course_privacy_ext')); 
                $ccprivateext = (!empty($csetting)) ? $csetting->value: 0;
            }

            $cert_hashurl = "";
            $cert_issuetime = 0;

            $certificates = $DB->get_records_sql("SELECT id, hashurl, issuedat FROM {monorail_certificate} WHERE userid=? AND courseid=? AND state",
                array($USER->id, $courses[$id]->id));

            foreach ($certificates as $cert)
            {
                $cert_hashurl = $cert->hashurl;
                $cert_issuetime = $cert->issuedat;
            }

            $cperm = monorail_get_course_perms($courses[$id]->id);
            $crsarray = array('id'=>$courses[$id]->id,
                'fullname'=>$courses[$id]->fullname,
                'shortname'=>$courses[$id]->shortname,
                'timemodified'=>$courses[$id]->timemodified,
                'completed' => ($courseData->status) ? 0 : 1,
                'quizes'=>$quizarray,
                'assignments'=>$assignmentarray,
                'coursebackground' => $backgroundimage,
                'inviteurl' => $courseinviteurl,
                'invitesopen' => $courseinvites->active,
                'invitecode' => $courseinvites->code,
                'canedit' => $canedit,
                'categoryid' => $courses[$id]->category,
                'course_privacy' => $cperm['course_privacy'],
                'course_access' => $cperm['course_access'],
                'course_review' => $cperm['review'],
                'course_price' => (double) @$monorailData["course_price"],
                'course_paypal_account' => @$monorailData["course_paypal_account"],
                'review_submit_time' => @$monorailData["review_submit_time"],
                'course_rating' => (int) @$monorailData["course_rating"],
                'cohortid' => (!empty($cohortid)) ? $cohortid : 0,
                'ccprivate' => (!empty($ccprivate)) ? $ccprivate: 0,
                'ccprivateext' => (!empty($ccprivateext)) ? $ccprivateext: 0,
                'course_logo' => $course_logo,
                'startdate' => $courses[$id]->startdate,
                'enddate' => $courseData->enddate,
                'durationstr' => $datestring,
                'code' => $courseData->code,
                'mainteacher' => $courseData->mainteacher,
                'progress' => ($totaltasks) ? ($completedtasks/$totaltasks)*100 : 0,
                "cert_enabled" => (int) (@$monorailData["cert_enabled"]),
                "cert_custom" => (int) (@$monorailData["cert_custom"]),
                "cert_custom_info" => @$monorailData["cert_custom_info"],
                "cert_hashurl" => $cert_hashurl,
                "cert_issuetime" => $cert_issuetime,
                "cert_orglogo" => (@$monorailData["cert_orglogo"]),
                "cert_sigpic" => (@$monorailData["cert_sigpic"])
            );
            if($canedit){
             $crsarray['students'] = $usercount;
             if (isset($courseData->mainteacher) && $courseData->mainteacher)
             {
             $teacher = $DB->get_record_sql("SELECT firstname, lastname FROM {user} WHERE id=?", array($courseData->mainteacher));
             $teacherData = array();
             foreach ($DB->get_recordset_sql("SELECT datakey, value FROM {monorail_data} WHERE itemid=?" .
                 " AND datakey IN ('pichash')", array($courseData->mainteacher)) as $item)
             {
                 $teacherData[$item->datakey] = $item->value;
             }
             $teacherName = $teacher->firstname . " " . $teacher->lastname;
             $teacherTitle = get_string('label_teacher','theme_monorail');
             $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);
             if($fieldid) {
                 $title = $DB->get_field('user_info_data', 'data', array('userid'=>$courseData->mainteacher, 'fieldid' => $fieldid), IGNORE_MULTIPLE);
                 if($title) {
                     $teacherTitle = $title;
                 }
             }
             // Teacher logo.
             if (array_key_exists("pichash", $teacherData))
             {
                 $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/" . $teacherData["pichash"] . "_90.jpg";
             }
             $crsarray['teacher_name'] = $teacherName;
             $crsarray['teacher_title'] = $teacherTitle;
             $crsarray['teacher_avatar'] = $teacherLogo;
            }
            }
            // skills
            try {
                require_once("$CFG->dirroot/theme/monorail/skills.php");
                $skills = monorail_get_course_skills($courses[$id]->id);
                $crsarray['skills'] = array();
                foreach ($skills as $skill) {
                    $skillrec = monorail_get_skill($skill->skillid);
                    $crsarray['skills'][] = $skillrec->name;
                }
            } catch (Exception $ex) {
                add_to_log($courses[$id]->id, 'monorailservices', 'externallib', 'get_assignments', 'Exception when trying to get skills: '.$ex->getMessage());
            }

            $coursearray[] = $crsarray;
        }

        $result = array();
        $result['courses'] = $coursearray;

        if (count($unavailablecourseids)>0) {
            foreach ($unavailablecourseids as $unavailablecourseid) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $unavailablecourseid;
                $warning['warningcode'] = '2';
                $warning['message'] = 'User is not enrolled or does not have requested capability';
                $warnings[] = $warning;
            }
        }
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for get_assignments
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_assignments_returns() {
        return new external_single_structure(
            array(
                'courses'   => new external_multiple_structure(self::course(), 'list of courses containing assignments'),
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function update_course_resources_parameters() {
        return new external_function_parameters(
                array('resources' => new external_multiple_structure (
                        new external_single_structure(
                                array(
                                        'instance' => new external_value(PARAM_INT, 'content id'),
                                        'coursemodule' => new external_value(PARAM_INT, 'Course module id'),
                                        'files' => new external_value(PARAM_INT, 'Id of the file uploaded to drafts', VALUE_OPTIONAL),
                                        'display' => new external_value(PARAM_INT, 'Display Type(popup)', VALUE_OPTIONAL),
                                        'popupwidth' => new external_value(PARAM_INT, 'Pop up width', VALUE_OPTIONAL),
                                        'popupheight' => new external_value(PARAM_INT, 'Pop up height', VALUE_OPTIONAL),
                                        'printheading' => new external_value(PARAM_INT, 'Print heading', VALUE_OPTIONAL),
                                        'printintro' =>  new external_value(PARAM_INT, 'Print intro', VALUE_OPTIONAL),
                                        'pagetext' => new external_value(PARAM_RAW, 'Page content', VALUE_OPTIONAL),
                                        'pageformat' => new external_format_value(PARAM_INT, 'Page format', VALUE_OPTIONAL)
                                 )), 'resource information to update')
                )
        );
    }


    /**
     * Return warnings if any
     * @param array of $course resources to be updated
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return array of warnings
     * @since  Moodle 2.4
     */
    public static function update_course_resources($resources = array(), $capabilities = array()) {
        global $USER, $DB;
        $params = self::validate_parameters(self::update_course_resources_parameters(),
                        array('resources' => $resources));

        $warnings = array();
        foreach ($resources as $resource) {
            $cm = get_coursemodule_from_instance('', $resource['coursemodule'], 0, false, MUST_EXIST);

            $context = context_course::instance($cm->course);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            }catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $assignid;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context';
                $warnings[] = $warning;
                continue;
            }
            if (in_array($cm->modname, array("url", "resource", "page", "folder" ))) {
                $updatefunction = $cm->modname.'_update_instance';
                if (function_exists($updatefunction)) {
                    //Use case if key specified overwrite else retain old data
                    $moddata = $DB->get_record($cm->modname, array('id'=>$cm->instance), '*', MUST_EXIST);
                    $array_keys = array_keys((array)$moddata);
                    $keyid = array_search('id', $array_keys);
                    if (false !== $keyid) {
                        unset($array_keys[$keyid]);
                    }
                    foreach($array_keys as $key) {
                        if(array_key_exists($key, $resource))
                            $moddata[$key] = $resource[$key];
                    }
                    if($cm->modname == 'page') {
                        $pagecontent = array();
                        if(array_key_exists('content', $resource) && array_key_exists('contentformat', $resource)){
                            $pagecontent['text'] = $moddata['content'];
                            $pagecontent['format'] = $moddata['contentformat'];
                        }
                        if(array_key_exists('draftitemid', $resource))
                            $pagecontent['draftitemid'] = $moddata['draftitemid'];
                        $tmpdata = (array)$moddata;
                        $tmpdata['page'] = $pagecontent;
                        $moddata = (object)$tmpdata;
                    }
                    require_once($CFG->dirroot . '/mod/' . $cm->modname . '/lib.php');
                    if (!$updatefunction((object)$moddata, (object)array())) {
                        $warning = array();
                        $warning['item'] = $cm->modname;
                        $warning['itemid'] = $cm->id;
                        $warning['warningcode'] = '1';
                        $warning['message'] = 'Error Updating module';
                        $warnings[] = $warning;
                        continue;
                    }
                    //send update events
                    $eventdata = new stdClass();
                    $eventdata->modulename = $cm->modname;
                    $eventdata->name       = 'mod_updated';
                    $eventdata->cmid       = $cm->id;
                    $eventdata->courseid   = $cm->course;
                    $eventdata->userid     = $USER->id;
                    events_trigger("mod_updated", $eventdata);
                }
            } else {
                $warning = array();
                $warning['item'] = $cm->modname;
                $warning['itemid'] = $cm->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'Updating module not suported';
                $warnings[] = $warning;
                continue;
            }

        }
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for update_course_resources
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_course_resources_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function update_course_sections_parameters() {
        return new external_function_parameters(
            array('sections' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'section id'),
                        'course' => new external_value(PARAM_INT, 'course id', VALUE_OPTIONAL),
                        'section' => new external_value(PARAM_INT, 'course section', VALUE_OPTIONAL),
                        'name' => new external_value(PARAM_RAW, 'section name', VALUE_OPTIONAL),
                        'summary' => new external_value(PARAM_RAW, 'section summary', VALUE_OPTIONAL),
                        'summaryformat' => new external_value(PARAM_INT, 'summary format', VALUE_OPTIONAL),
                        'sequence' => new external_value(PARAM_TEXT, 'sequence', VALUE_OPTIONAL),
                        'visible' => new external_value(PARAM_INT, 'section visibility', VALUE_OPTIONAL),
                        'availablefrom' => new external_value(PARAM_INT, 'Available from', VALUE_OPTIONAL),
                        'availableuntil' => new external_value(PARAM_INT, 'Available to', VALUE_OPTIONAL),
                        'showavailability' => new external_value(PARAM_INT, 'Show availability', VALUE_OPTIONAL),
                        'groupingid' => new external_value(PARAM_INT, 'Grouping id', VALUE_OPTIONAL),
                        'tempid' => new external_value(PARAM_TEXT, 'Temporary ID of section in UI', VALUE_OPTIONAL),
                        'isnew' => new external_value(PARAM_INT, 'Is this a new section', VALUE_OPTIONAL),
                    )))
            )
        );
    }


    /**
     * Update or create course sections
     * @param array of $course sections to be updated or created
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return ids of sections and array of warnings
     */
    public static function update_course_sections($sections = array(), $capabilities = array(), $silent = false) {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::update_course_sections_parameters(),
            array('sections' => $sections));
        $warnings = array();
        $sectionresult = array();
        $forums = array();
        $newSections = false;
        $nonFirstSections = false;
        $courseId = null;

        foreach ($sections as $section)
        {
            if (array_key_exists('name', $section)) {
              $section['name'] = clean_param($section['name'], PARAM_TEXT);
            }
            if ($section['id'] == 0)
            {
                // create
                if (! $section['course']) {
                    // we must have course id on create
                    $warning = array();
                    $warning['item'] = 'section';
                    $warning['itemid'] = $section->id;
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'When creating a section course ID must be supplied';
                    $warnings[] = $warning;
                    continue;
                }
                $sectioncount = $DB->count_records('course_sections', array('course'=>$section['course']));

                if ($sectioncount > 0)
                {
                    $sects = $DB->get_recordset_sql("SELECT MAX(section) AS m FROM {course_sections} WHERE course=:course", array("course" => $section['course']));
                    $sects = $sects->current();

                    $sectionnr = $sects->m + 1;
                }
                else
                {
                    $sectionnr = $sectioncount;
                }

                $newsection = new stdClass();
                $newsection->course        = $section['course'];
                $newsection->section       = $sectionnr;
                $newsection->summaryformat = FORMAT_HTML;
                $newsectionid = $DB->insert_record('course_sections', $newsection);
                $section['id'] = $newsectionid;
                
                // set course section counter to section count
                $DB->set_field('course', 'numsections', $sectioncount, array('id'=>$section['course']));

                if ($sectionnr == 0)
                {
                    // Create a new forum for each new first section.
                    $forums[] = array(
                            'sectionid' => $newsectionid,
                            'course' => $section['course'],
                            'type'  => 'general',
                            'name'  => get_string('forum_general',"theme_monorail"),
                            'intro' => get_string('forum_general_intro',"theme_monorail"),
                            'introformat' => 1,
                            'assessed'  => 0,
                            'assesstimestart'  => 0,
                            'assesstimefinish'  => 0,
                            'scale'  => 0,
                            'maxbytes'  => 51200,
                            'maxattachments'  => 2,
                            'forcesubscribe'  => 0,
                            'trackingtype'  => 1,
                            'rsstype'  => 0,
                            'rssarticles'  => 0,
                            'warnafter'  => 0,
                            'blockafter'  => 0,
                            'blockperiod'  => 0,
                            'completiondiscussions'  => 0,
                            'completionreplies' => 0,
                            'completionposts' => 0);
                }

                $newSections = true;
            }

            $sectioninfo  = $DB->get_record('course_sections', array('id' => $section['id']), "*", MUST_EXIST);
            $context = context_course::instance($sectioninfo->course);

            if ($courseId === null)
            {
                $courseId = $sectioninfo->course;
            }
            else if ($courseId && $courseId != $sectioninfo->course)
            {
                $courseId = 0;
            }

            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            }catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'section';
                $warning['itemid'] = $sectioninfo->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to update section';
                $warnings[] = $warning;
                continue;
            }
            $keys=array_keys((array)$sectioninfo);
            $keyid = array_search('id', $keys);
            if (false !== $keyid) {
                unset($keys[$keyid]);
            }

            $oldSection = $sectioninfo->section;

            foreach($keys as $keytoupdate){
                if(array_key_exists($keytoupdate, $section))
                    $sectioninfo->$keytoupdate = $section[$keytoupdate];
            }

            if ($sectioninfo->section == 0)
            {
                // If this is section 0, update public course info.

                if (!$silent && isset($CFG->mage_api_url))
                {
                    addBackgroundCall(function () use ($sectioninfo)
                    {
                        global $DB;

                        $api = magento_get_instance();

                        if ($api)
                        {
                            $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?",
                                array($sectioninfo->course));

                            $catalogItem = $api->getProductBySku($courseData->code);

                            if (!empty($catalogItem))
                            {
                                $summary_html = monorail_export_attachments($sectioninfo->summary);

                                $api->begin();
                                $api->updateProduct($courseData->code, array(
                                    "description" => $summary_html,
                                    "short_description" => $summary_html,
                                    "meta_description" => $summary_html));
                            }
                        }
                    });
                }
            }
            else
            {
                $nonFirstSections = true;

                // Changing section position...
                if ($sectioninfo->section != $oldSection)
                {
                    if ($sectioninfo->section > $oldSection)
                    {
                        $sectioninfo->section++;
                    }

                    if ($DB->record_exists("course_sections", array("course" => $sectioninfo->course, "section" => $sectioninfo->section)))
                    {
                        // If another section already in this position, push it
                        // (and others) below.

                        $sectBelow = $DB->get_records_sql("SELECT id, section FROM {course_sections} WHERE section>=? AND course=? ORDER BY section DESC",
                                array($sectioninfo->section, $sectioninfo->course));

                        foreach ($sectBelow as $sect)
                        {
                            $sect->section++;
                            $DB->update_record("course_sections", $sect);
                        }
                    }
                }
            }

            $DB->update_record('course_sections', $sectioninfo);

            // Defragment section numbers...
            if ($sectioninfo->section != $oldSection)
            {
                $sects = $DB->get_records_sql("SELECT id, section FROM {course_sections} WHERE course=? AND section>0 ORDER BY section ASC", array($sectioninfo->course));
                $idx = 1;

                foreach ($sects as $sect)
                {
                    if ($sect->section != $idx)
                    {
                        $sect->section = $idx;
                        $DB->update_record("course_sections", $sect);
                    }

                    $idx++;
                }
            }

            if (!$silent) {
                //update the course cache
                rebuild_course_cache($sectioninfo->course);

                // get revision
                $revid = null;
                try {
                    $rev = monorailfeed_get_revision('section', $sectioninfo->id);
                    if (monorailfeed_compare_with_rev('section', $sectioninfo, $rev)) {
                        throw new Exception();
                    }
                } catch (Exception $ex) {
                    // save a new revision
                    try {
                        $revid = monorailfeed_save_revision('section', $sectioninfo->id, null, $USER->id);
                    } catch (Exception $ex) {
                        add_to_log($sectioninfo->course, 'monorailservices', 'externallib.update_course_sections', '', 'Error! '.$ex->getMessage());
                    }
                }

                // create notification if changes
                if (! isset($section['isnew']) && $revid && $sectioninfo->visible) {
                  try {
                    monorailfeed_create_notification($sectioninfo->course, 'section', 'section', $sectioninfo->id, $USER->id, null, $revid);
                  } catch (Exception $ex) {
                    add_to_log($courseId, 'monorailservices', 'externallib', 'upd_course_sections', 'Exception create notifications: '.$ex->getMessage());
                  }
                }
            }

            if (isset($section['tempid']))
                $sectionresult[] = array('sectionid' => $section['id'], 'tempid' => $section['tempid']);
            else
                $sectionresult[] = array('sectionid' => $section['id']);
        }

        if ($courseId)
        {
            wscache_reset_by_dependency("course_content_" . $courseId);
        }

        if (!$silent && !$newSections && $nonFirstSections && $courseId)
        {
            try
            {
                if (isset($CFG->mage_api_url))
                {
                    addBackgroundCall(function () use ($courseId)
                    {
                        global $DB;

                        $api = magento_get_instance();

                        if ($api)
                        {
                            $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?", array($courseId));

                            $catalogItem = $api->getProductBySku($courseData->code);

                            if (!empty($catalogItem))
                            {
                                $topics = array();
                                $sections = $DB->get_records_sql("SELECT id, name FROM {course_sections} WHERE course=? AND visible AND section>0 ORDER BY section ASC",
                                    array($courseId));

                                foreach ($sections as &$sect)
                                {
                                    $topics[] = $sect->name;
                                }

                                $api->begin();
                                $api->updateProduct($courseData->code,
                                    array("additional_attributes" => array("single_data" =>
                                        array("topic_list" => json_encode($topics)))));
                            }
                        }
                    });
                }
            }
            catch (Exception $e)
            {
                add_to_log(1, 'monorail', 'externallib.update_course', '', 'ERROR: '.$e->getMessage());
            }
        }

        if (!empty($forums))
        {
            self::create_forums($forums);
        }

        $result['section'] = $sectionresult;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for update_course_sections
     * @return external_single_structure
     */
    public static function update_course_sections_returns() {
        return new external_single_structure(
            array(
                'section' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'sectionid' => new external_value(PARAM_INT, 'Section ID that has been updated'),
                            'tempid' => new external_value(PARAM_TEXT, 'Temporary ID in UI, we pass it back', VALUE_OPTIONAL)
                        )),'section id'),
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function delete_course_sections_parameters() {
        return new external_function_parameters(
            array('sections' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'section id'),
                    )), 'resource information to update')
            )
        );
    }


    /**
     * Delete course sections
     * @param array of $course sections to be deleted
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return ids of sections and array of warnings
     */
    public static function delete_course_sections($sections = array(), $capabilities = array()) {
        global $CFG, $USER, $DB;
        $params = self::validate_parameters(self::update_course_sections_parameters(),
            array('sections' => $sections));

        require_once("$CFG->dirroot/course/lib.php");

        $warnings = array();
        $sectionresult = array();
        $forums = array();
        $courseId = null;

        foreach ($sections as $section)
        {
            try {
                $sectioninfo  = $DB->get_record('course_sections', array('id' => $section['id']), "*", MUST_EXIST);
            } catch (Exception $e) {
                // No section - no problem.
                continue;
            }

            $context = context_course::instance($sectioninfo->course);

            if ($courseId === null)
            {
                $courseId = $sectioninfo->course;
            }
            else if ($courseId && $courseId != $sectioninfo->course)
            {
                $courseId = 0;
            }

            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            }catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'section';
                $warning['itemid'] = $sectioninfo->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to update section';
                $warnings[] = $warning;
                continue;
            }

            $DB->delete_records('course_sections', array("id" => $section["id"]));
            // delete all modules if they exist
            if (!empty($sectioninfo->sequence) ) {
                $modids = explode(',', $sectioninfo->sequence);
                foreach($modids AS $modid) {
                    if ($cm = $DB->get_record("course_modules", array("id" => $modid))) {
                        if ($modulename = $DB->get_field('modules', 'name', array('id' => $cm->module))) {
                            if(strcmp($modulename,"kalvidres") == 0) {
                                //deleting module does not delete video file resource do delete here
                                $entryid = $DB->get_field('kalvidres', 'entry_id', array('id'=>$cm->instance));
                                if ($entryid) {
                                    self::delete_video_resource($entryid);
                                }
                            }
                            $modlib = "{$CFG->dirroot}/mod/{$modulename}/lib.php";
                            if (file_exists($modlib)) {
                                include_once($modlib);
                                $deleteinstancefunction = $modulename."_delete_instance";
                                $deleteinstancefunction($cm->instance);
                            }
                        }
                        delete_course_module($modid);
                    }
                }
            }

            // Fixing section numbers.
            $sects = $DB->get_records_sql("SELECT id, section FROM {course_sections} WHERE course=? AND section>0 ORDER BY section ASC", array($sectioninfo->course));
            $idx = 1;

            foreach ($sects as $sect)
            {
                if ($sect->section != $idx)
                {
                    $sect->section = $idx;
                    $DB->update_record("course_sections", $sect);
                }

                $idx++;
            }

            rebuild_course_cache($sectioninfo->course);
            add_to_log($sectioninfo->course, "monorailservices", "externallib.delete_course_sections", "", "Section ".$sectioninfo->section." deleted by ".$USER->id);

            // delete any notifications
            $notifications = monorailfeed_get_unread_instance('section', $sectioninfo->id);
            foreach ($notifications as $notification) {
                monorailfeed_remove_notification($notification->id);
            }
        }

        if ($courseId)
        {
            wscache_reset_by_dependency("course_content_" . $courseId);

            if (isset($CFG->mage_api_url))
            {
                addBackgroundCall(function () use ($courseId)
                {
                    global $DB;

                    $api = magento_get_instance();

                    if ($api)
                    {
                        $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?", array($courseId));

                        $catalogItem = $api->getProductBySku($courseData->code);

                        if (!empty($catalogItem))
                        {
                            $topics = array();
                            $sections = $DB->get_records_sql("SELECT id, name FROM {course_sections} WHERE course=? AND visible AND section>0 ORDER BY section ASC",
                                array($courseId));

                            foreach ($sections as &$sect)
                            {
                                $topics[] = $sect->name;
                            }

                            $api->begin();
                            $api->updateProduct($courseData->code,
                                array("additional_attributes" => array("single_data" =>
                                    array("topic_list" => json_encode($topics)))));
                        }
                    }
                });
            }
        }

        $result['section'] = $sectionresult;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for delete_course_sections
     * @return external_single_structure
     */
    public static function delete_course_sections_returns() {
        return new external_single_structure(
            array(
                'section' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'sectionid' => new external_value(PARAM_INT, 'Section ID that has been deleted'),
                        )),'section id'),
                'warnings'  => new external_warnings()
            )
        );
    }


    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.4
     */
    public static function course_editable_parameters() {
        return new external_function_parameters(
            array('id'        => new external_value(PARAM_INT, 'course id'),
                'context'   => new external_value(PARAM_TEXT, 'context course/activity')
            )
        );
    }
    
    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.4
     */
    public static function verify_captcha_parameters() {
        return new external_function_parameters(
            array('id' => new external_value(PARAM_INT, 'course id'),
            'challenge' => new external_value(PARAM_TEXT, 'challenge string'),
            'response' => new external_value(PARAM_TEXT, 'response for the challenge'),
            )
        );
    }

    /**
     * Describes the return value for verify_captcha
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function verify_captcha_returns() {
        return new external_single_structure(
            array(
            'success' => new external_value(PARAM_INT, '1 for success, 0 for failed'),
            )
        );
    }

    /**
     * Return warnings if any
     * @param $moduleinfo information required to check permissions
     * @return array of warnings
     * @since  Moodle 2.4
     */
    public static function course_editable($id, $contextstr) {
        global $USER, $DB;

        $params = self::validate_parameters(self::course_editable_parameters(),
            array('id' => $id, 'context' => $contextstr));

        $haspermissions = 1;
        $warnings = array();
        $courseinfo  = $DB->get_record('course', array('id' => $id), "*", MUST_EXIST);
        $context = context_course::instance($id);
        try {
            self::validate_context($context);
            if($contextstr == 'course'){
                require_capability('moodle/course:update', $context);
            } else if($contextstr == 'activity')
                require_capability('moodle/course:manageactivities', $context);
        } catch (Exception $e) {
            $warning = array();
            $warning['item'] = $contextstr;
            $warning['itemid'] = $id;
            $warning['warningcode'] = '1';
            $warning['message'] = 'No access rights';
            $warnings[] = $warning;
            $haspermissions = 0;
        }

        $result['haspermissions'] = $haspermissions;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for course_editable
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function course_editable_returns() {
        return new external_single_structure(
            array(
                'haspermissions'  =>  new external_value(PARAM_INT, 'true if user has enogh permissions for requested activity'),
                'warnings'        =>  new external_warnings()
            )
        );
    }
    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_page_parameters() {
        return new external_function_parameters(
            array(
                'courseids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'course id'),
                    '0 or more course ids',
                    VALUE_DEFAULT, array()),
                'capabilities'  => new external_multiple_structure(
                    new external_value(PARAM_CAPABILITY, 'capability'),
                    'ANDed list of capabilities used to filter courses',
                    VALUE_DEFAULT, array())
            )
        );
    }

    /**
     * Return page resource info for courses in which the user is enrolled, optionally filtered
     * by course id and/or capability
     * @param array of ints $courseids specify course ids to be returned
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return array of pageinfo and warnings
     * @since  Moodle 2.4
     */
    public static function get_page($courseids = array(), $capabilities = array()) {
        global $USER, $DB;
        $params = self::validate_parameters(self::get_page_parameters(),
            array('courseids' => $courseids, 'capabilities' => $capabilities));


        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for get_assignments
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_page_returns() {
        return new external_single_structure(
            array(
                'courses'   => new external_multiple_structure(self::course(), 'list of courses containing assignments'),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function add_submissions_parameters()
    {
        return new external_function_parameters(
            array('submissions' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'assignid' => new external_value(PARAM_INT, 'assignment id'),
                        'filename' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'file name of the file uploaded'),
                        'name' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'name of the file uploaded')
                    )), 'submission details to update')
            )
        );
    }

    /**
     * Add file submissions
     * @param array of  $submissions
     * @return file url and an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function add_submissions($submissions) {
        global $CFG, $DB, $USER;
        require_once("$CFG->dirroot/theme/monorail/lib.php");

        $params = self::validate_parameters(self::add_submissions_parameters(),
            array('submissions' => $submissions));

        $warnings = array();
        $returndata = array();

        foreach ($submissions as $submission) {
            // Ok get the course from the assignment id
            $fileitemid = 0;
            $assignment = $DB->get_record('assign', array('id' => $submission['assignid']), "*", MUST_EXIST);
            if ($DB->record_exists('assign_submission', array('assignment'=>$assignment->id, 'userid'=> $USER->id)))
            {
                //User has submitted some stuff for this assignments
                $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assignment->id, 'userid'=> $USER->id));
                // Now get the no of existing submissions
                $fileitemid = $submitinfo->id;
                $existingsubmissions = $DB->get_record('assignsubmission_file',
                    array('assignment'=>$assignment->id, 'submission'=> $submitinfo->id));
                $filesubmissionenabled = false;
                $maxfiles = 10;
                $config_records = $DB->get_records('assign_plugin_config', array('assignment'=>$assignment->id));

                foreach ($config_records as $config_record) {
                    if($config_record->plugin == 'file' && $config_record->subtype == 'assignsubmission') {
                        if($config_record->name == 'enabled' && $config_record->value = 1) {
                            $filesubmissionenabled = true;
                        }
                        if($config_record->name == 'maxfilesubmissions') {
                            $maxfiles = $config_record->value;
                        }
                    }
                }

                if (! empty($existingsubmissions) && $existingsubmissions->numfiles >=  $maxfiles) {
                    $warning = array();
                    $warning['item'] = 'submission';
                    $warning['itemid'] = $assignment->id;
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'No file submissions possible';
                    $warnings[] = $warning;
                    continue;

                }
            } else {
                // Insert submission record
                $srecord = new  stdClass;
                $srecord->assignment = $assignment->id;
                $srecord->userid = $USER->id;
                $srecord->timecreated = time();
                $srecord->timemodified = time();
                $srecord->status = 'draft';
                $fileitemid = $DB->insert_record("assign_submission", $srecord);

            }
            // now to deal with the file submission
            // get the course module id
            $mid = $DB->get_field('modules', 'id', array('name'=>'assign'));
            $cm = $DB->get_record('course_modules', array('course' => $assignment->course, 'instance' => $assignment->id, 'module'=>$mid), "*", MUST_EXIST);
            $context = context_module::instance($cm->id);

            $fs = get_file_storage();
            // Prepare file record object
            $fileinfo = array(
                'contextid' => $context->id,
                'component' => 'assignsubmission_file',
                'filearea' => 'submission_files',
                'itemid' => $fileitemid,
                'filepath' => '/',
                'filename' => $submission['filename'],
                'name' => $submission['name'],
            );

            //Possible that path hash is existing
            $pathnamehash = $fs->get_pathname_hash($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'], $fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);
            $file_exists = $DB->get_record('files', array('pathnamehash' => $pathnamehash));
            if(!empty($file_exists)) {
                //The file uploaded may have the same name , and it will fail with same pathhash 'duplicate entry exception'
                //Generate a new non existing file name , using current unix timestamp as prefix
                $fileinfo['filename'] = strtotime("now").'-'.$submission['filename'];
            }
            $fileurl = $CFG->dataroot."/temp/files/".$USER->id."/".$submission['filename'];
            $file = $fs->create_file_from_pathname($fileinfo, $fileurl);
            //monorail_trigger_document_conversion($file, $fileurl);
            if(!empty($submission['contextualdata'])) {
                $cdata = new stdClass();
                $cdata->pathhash   = $file->get_pathnamehash();
                $cdata->contextdata = clean_param($submission['contextualdata'],PARAM_RAW_TRIMMED);
                $cdata->fileid   = $file->get_id();
                $cdata->timemodified = time();
                $DB->insert_record('monorail_file_context_data',$cdata);
            }
            // clear temp files
            unlink($fileurl);
            @unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$submission['filename']);

            //ok now that the file is added, update the submission record
            $srecord = $DB->get_record("assign_submission", array('id' => $fileitemid), "*", MUST_EXIST);
            $srecord->timemodified = time();
            $DB->update_record("assign_submission", $srecord);
            if($DB->record_exists('assignsubmission_file',array('assignment'=>$assignment->id, 'submission'=> $fileitemid))){
                $uprecord = $DB->get_record('assignsubmission_file',
                    array('assignment'=>$assignment->id, 'submission'=> $fileitemid));
                $uprecord->numfiles = $uprecord->numfiles + 1;
                $DB->update_record("assignsubmission_file", $uprecord);
            } else {
                //create record
                $frecord = new stdClass;
                $frecord->assignment = $assignment->id;
                $frecord->submission = $fileitemid;
                $frecord->numfiles = 1;
                $DB->insert_record("assignsubmission_file", $frecord);
            }
            //Store file display name in monorail_data
            monorail_data('assignfile', $file->get_id(), 'name', $submission['name']);
            // return some data to UI
            $url = file_encode_url("$CFG->wwwroot/webservice/pluginfile.php?token=WSTOKEN&file=",
                '/'.$context->id.'/assignsubmission_file/submission_files/'.$fileitemid.'/'.$file->get_filepath().$file->get_filename());
            $resultfile = array();
            $resultfile['id'] = $file->get_id();
            $resultfile['name'] = $file->get_filename();
            $resultfile['url'] = $url;
            $resultfile['filetype'] = $file->get_mimetype();
            $returndata[] = $resultfile;

        }
        $result = array();
        $result['files'] = $returndata;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for add_submissions
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function add_submissions_returns() {
        return new external_single_structure(
            array(
                'files' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id'   => new external_value(PARAM_INT, 'id of file'),
                            'url'   => new external_value(PARAM_URL, 'url of file'),
                            'name'   => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'name of file'),
                            'filetype' => new external_value(PARAM_TEXT, 'type of file')
                        )
                    )
                ),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function get_submissions_parameters() {
        return new external_function_parameters(
            array('assignid'   => new external_value(PARAM_INT, 'assignment id'),
                  'studentid'   => new external_value(PARAM_INT, 'student id', VALUE_OPTIONAL)
            )
        );
    }
    /**
     * get file submissions
     * @param array of  $submissions
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function get_submissions($assignid, $studentid=0) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::get_submissions_parameters(),
            array('assignid' => $assignid, 'studentid' => $studentid));

        $warnings = array();
        $result = array();
        if($studentid > 0) {
            $uid = $studentid;
        } else {
            $uid = $USER->id;
        }

        $assignment = $DB->get_record('assign', array('id' => $assignid), "*", MUST_EXIST);
        if (isset($assignment->id)) {
            try {
                $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assignment->id, 'userid'=> $uid), '*', MUST_EXIST);
                //ok get the num of files submitted
                $submitfile = $DB->get_record('assignsubmission_file', array('assignment'=>$assignment->id, 'submission'=> $submitinfo->id), '*', MUST_EXIST);
            } catch (Exception $e) {
                //No submissions
                return $result;
            }
        } else {
            return $result;
        }


        if($submitfile->numfiles <= 0) {
            $warning = array();
            $warning['item'] = 'submission';
            $warning['itemid'] = $assignid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'No submissions';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }
        //get the context id / file info
        $mid = $DB->get_field('modules', 'id', array('name'=>'assign'));
        $cm = $DB->get_record('course_modules', array('course' => $assignment->course, 'instance' => $assignment->id, 'module'=>$mid), "*", MUST_EXIST);
        $context = context_module::instance($cm->id, $uid);

        $fileinfo = array();
        $fs = get_file_storage();

        //$files = $fs->get_area_files($context->id, 'assignsubmission_file', 'submission_files');
        $files = $fs->get_area_files($context->id, 'assignsubmission_file', 'submission_files', $submitinfo->id, "timemodified", false);
        foreach ($files as $file) {
            $names = monorail_data('assignfile', $file->get_id(), 'name');
            foreach ($names as $name) {
                $filedisplayname = $name->value;
                break;
            }
            if ($filedisplayname == null) {
                $filedisplayname = $file->get_filename();
            }
            if(!($file->get_filename() === '.')) {
                $url = file_encode_url("$CFG->wwwroot/webservice/pluginfile.php?token=WSTOKEN&file=",
                    '/'.$context->id.'/assignsubmission_file/submission_files/'.$submitinfo->id.'/'.$file->get_filepath().$file->get_filename());
                $resultfile = array();
                $resultfile['id'] = $file->get_id();
                $resultfile['name'] = $filedisplayname;
                $resultfile['timecreated'] = $file->get_timecreated();
                $resultfile['url'] = $url;
                //$resultfile['convertedpath'] =  monorail_converted_fileurl($file, true);
                $resultfile['contextualdata'] = self::monorailservices_get_file_contextdata($file->get_pathnamehash(), $file->get_id());
                $resultfile['filetype'] = monorail_get_file_type($file);
                $resultfile['mimetype'] = $file->get_mimetype();
                $fileinfo[] = $resultfile;
            }
        }
        $result['files'] = $fileinfo;
        $result['warnings'] = $warnings;
        return $result;

    }

    /**
     * Describes the return value for get_submissions
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_submissions_returns() {
        return new external_single_structure(
            array(
                'files'   => new external_multiple_structure(self::fileinfo(), 'file data name, id , created', VALUE_OPTIONAL),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function del_submissions_parameters() {
        return new external_function_parameters(
            array('fileitems' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'assignmentid'   => new external_value(PARAM_INT, 'assignment id'),
                        'fileids' => new external_multiple_structure(
                            new external_value(PARAM_INT, 'fileitem id'))
                    )), 'fileitems to remove')
            )
        );
    }

    /**
     * remove file submissions
     * @param array of fileitem ids
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function del_submissions($fileitems) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::del_submissions_parameters(),
            array('fileitems' => $fileitems));

        $warnings = array();
        $result = array();

        foreach ($fileitems as $fileitem) {
            $assignment = $DB->get_record('assign', array('id' => $fileitem['assignmentid']), "*", MUST_EXIST);
            $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assignment->id, 'userid'=> $USER->id), "*", MUST_EXIST);
            $submitfile = $DB->get_record('assignsubmission_file', array('assignment'=>$fileitem['assignmentid'], 'submission'=> $submitinfo->id), "*", MUST_EXIST);
            //get the context id / file info
            $cm = $DB->get_record('course_modules', array('course' => $assignment->course, 'instance' => $assignment->id), "*", MUST_EXIST);
            $context = context_module::instance($cm->id);

            $fileids = $fileitem['fileids'];
            $fileidsdeleted = array();
            $fs = get_file_storage();
            $count = 0;
            $files = $fs->get_area_files($context->id, 'assignsubmission_file', 'submission_files', $submitinfo->id, "timemodified", false);
            foreach ($files as $file) {
                if(in_array($file->get_id(), $fileids)){
                    $finstance = $fs->get_file_by_id($file->get_id());
                    $finstance->delete();
                    array_push($fileidsdeleted,$file->get_id());
                    $count = $count+1;

                }
            }
            if($count > 0) {
                $submitfile->numfiles = $submitfile->numfiles - $count;
                $DB->update_record('assignsubmission_file', $submitfile);
            }
            $adiff = array_diff($fileids, $fileidsdeleted);

            if (count($adiff) > 0){
                $warning = array();
                $warning['item'] = 'deletesubmission';
                $warning['itemid'] = $fileitem['assignmentid'];
                $warning['warningcode'] = implode(",", $adiff);
                $warning['message'] = 'Could not delete some file submissions';
                $warnings[] = $warning;
            }

        }

        $result['warnings'] = $warnings;
        return $result;

    }

    /**
     * Describes the return value for add_submissions
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function del_submissions_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }


    public static function create_forums_parameters() {
        return new external_function_parameters(
            array('forums' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'sectionid' => new external_value(PARAM_INT, 'section id'),
                        'course' => new external_value(PARAM_INT, 'course id'),
                        'type'  => new external_value(PARAM_TEXT, 'forum type ', VALUE_DEFAULT, 'general'),
                        'name'  => new external_value(PARAM_TEXT, 'forum name'),
                        'intro' => new external_value(PARAM_RAW, 'forum description', VALUE_DEFAULT, '<p>A general forum</p>'),
                        'introformat' => new external_value(PARAM_INT, 'forum description format ', VALUE_DEFAULT, 1),
                        'assessed'  => new external_value(PARAM_INT, 'forum assessed', VALUE_DEFAULT, 0),
                        'assesstimestart'  => new external_value(PARAM_INT, 'assessment time start ', VALUE_DEFAULT, 0),
                        'assesstimefinish'  => new external_value(PARAM_INT, 'assessment time finish ', VALUE_DEFAULT, 0),
                        'scale'  => new external_value(PARAM_INT, 'forum grade scale', VALUE_DEFAULT, 0),
                        'maxbytes'  => new external_value(PARAM_INT, 'forum max size ', VALUE_DEFAULT, 51200),
                        'maxattachments'  => new external_value(PARAM_INT, 'forum max attachments', VALUE_DEFAULT, 2),
                        'forcesubscribe'  => new external_value(PARAM_INT, 'forum force subscription', VALUE_DEFAULT, 0),
                        'trackingtype'  => new external_value(PARAM_INT, 'forum tracking type ', VALUE_DEFAULT, 1),
                        'rsstype'  => new external_value(PARAM_INT, 'forum rss type ', VALUE_DEFAULT, 0),
                        'rssarticles'  => new external_value(PARAM_INT, 'forum rss articles', VALUE_DEFAULT, 0),
                        'warnafter'  => new external_value(PARAM_INT, 'forum warn after', VALUE_DEFAULT, 0),
                        'blockafter'  => new external_value(PARAM_INT, 'forum block after', VALUE_DEFAULT, 0),
                        'blockperiod'  => new external_value(PARAM_INT, 'forum block period', VALUE_DEFAULT, 0),
                        'completiondiscussions'  => new external_value(PARAM_INT, 'forum completion discussions', VALUE_DEFAULT, 0),
                        'completionreplies' => new external_value(PARAM_INT, 'forum completion replies', VALUE_DEFAULT, 0),
                        'completionposts' => new external_value(PARAM_INT, 'forum completion posts', VALUE_DEFAULT, 0),
                    )), 'list of forum information')
            )
        );
    }


    /**
     * Create forum for a course
     *
     * @param array of  $forums - forum related info
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function create_forums($forums) {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::create_forums_parameters(),
            array('forums' => $forums));

        $warnings = array();
        $foruminfo = array();
        foreach ($forums as $forum) {
            //Check if user has permissions
            $context = context_course::instance($forum['course']);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
                require_capability('mod/forum:addinstance', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $forum['course'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to add forum';
                $warnings[] = $warning;
                continue;
            }
            //Get the course section info
            $section = $DB->get_record("course_sections", array("id"=>$forum['sectionid']));
            if(!$section) {
                //User cannot create assignment for these courses
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $forum['course'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Unable to find course section to add forum';
                $warnings[] = $warning;
                continue;//continue with next module error getting section
            }

            $moduleid = $DB->get_field('modules', 'id', array('name'=>'forum'));
            // create a new course module for forum
            $newcm = new stdClass();
            $newcm->course   = $forum['course'];
            $newcm->module   = $moduleid;
            $newcm->instance = 0;
            $newcm->visible  = 1;
            $newcm->section  = $section->id;
            $newcm->coursemodule  = 0;
            $newcm->added    = time();
            $newcmid = $DB->insert_record('course_modules', $newcm);



            $data = array(
                'course'=> $forum['course'],
                'type'=> $forum['type'],
                'name'=> $forum['name'],
                'intro'=> clean_param($forum['intro'],PARAM_RAW_TRIMMED),
                'introformat'=> $forum['introformat'],
                'assessed'=> $forum['assessed'],
                'assesstimestart'=> $forum['assesstimestart'],
                'assesstimefinish'=> $forum['assesstimefinish'],
                'scale'=> $forum['scale'],
                'maxbytes'=> $forum['maxbytes'],
                'maxattachments'=> $forum['maxattachments'],
                'forcesubscribe'=> $forum['forcesubscribe'],
                'trackingtype'=> $forum['trackingtype'],
                'rsstype'=> $forum['rsstype'],
                'rssarticles'=> $forum['rssarticles'],
                'warnafter'=> $forum['warnafter'],
                'blockafter'=> $forum['blockafter'],
                'blockperiod'=> $forum['blockperiod'],
                'completiondiscussions'=> $forum['completiondiscussions'],
                'completionreplies'=> $forum['completionreplies'],
                'completionposts'=> $forum['completionposts'],
                'coursemodule' => $newcmid,
                'cmidnumber' => $newcmid,
            );
            $data = (object) $data;

            require_once($CFG->dirroot . '/mod/forum/lib.php');
            require_once($CFG->dirroot . '/course/lib.php');
            $forumid = forum_add_instance($data);
            $newcm->coursemodule = $forumid;


            //Update the instance field in course_modules
            $DB->set_field('course_modules', 'instance', $newcm->coursemodule, array('id'=>$newcmid));

            //section info update
            $newcm->coursemodule  = $newcmid;
            $newcm->section = $section->section;
            add_mod_to_section($newcm);

            $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcm->coursemodule));
            set_coursemodule_visible($newcm->coursemodule, 1);
            rebuild_course_cache($data->course);

            //send add events
            $eventdata = new stdClass();
            $eventdata->modulename = "forum";
            $eventdata->name       = 'mod_created';
            $eventdata->cmid       = $newcm->coursemodule;
            $eventdata->courseid   = $data->course;
            $eventdata->userid     = $USER->id;
            events_trigger("mod_created", $eventdata);
            $foruminf = array();
            $foruminf['forumid'] = $forumid;
            $foruminfo[] = $foruminf;

        }
        $result = array();
        $result['forum'] = $foruminfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for create_forums
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function create_forums_returns() {
        return new external_single_structure(
            array(
                'forum' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'forumid' => new external_value(PARAM_INT, 'Forum Id')
                        )),'forum id'),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function update_forums_parameters() {
        return new external_function_parameters(
            array('forums' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'forum id', VALUE_DEFAULT, 0),
                        'moduleid' => new external_value(PARAM_INT, 'module id', VALUE_DEFAULT, 0),
                        'course' => new external_value(PARAM_INT, 'course id', VALUE_OPTIONAL),
                        'type'  => new external_value(PARAM_TEXT, 'forum type ', VALUE_OPTIONAL),
                        'name'  => new external_value(PARAM_TEXT, 'forum name', VALUE_OPTIONAL),
                        'intro' => new external_value(PARAM_RAW, 'forum description', VALUE_OPTIONAL),
                        'introformat' => new external_value(PARAM_INT, 'forum description format ', VALUE_OPTIONAL),
                        'assessed'  => new external_value(PARAM_INT, 'forum assessed', VALUE_OPTIONAL),
                        'assesstimestart'  => new external_value(PARAM_INT, 'assessment time start ', VALUE_OPTIONAL),
                        'assesstimefinish'  => new external_value(PARAM_INT, 'assessment time finish ', VALUE_OPTIONAL),
                        'scale'  => new external_value(PARAM_INT, 'forum grade scale', VALUE_OPTIONAL),
                        'maxbytes'  => new external_value(PARAM_INT, 'forum max size ', VALUE_OPTIONAL),
                        'maxattachments'  => new external_value(PARAM_INT, 'forum max attachments', VALUE_OPTIONAL),
                        'forcesubscribe'  => new external_value(PARAM_INT, 'forum force subscription', VALUE_OPTIONAL),
                        'trackingtype'  => new external_value(PARAM_INT, 'forum tracking type ', VALUE_OPTIONAL),
                        'rsstype'  => new external_value(PARAM_INT, 'forum rss type ', VALUE_OPTIONAL),
                        'rssarticles'  => new external_value(PARAM_INT, 'forum rss articles', VALUE_OPTIONAL),
                        'warnafter'  => new external_value(PARAM_INT, 'forum warn after', VALUE_OPTIONAL),
                        'blockafter'  => new external_value(PARAM_INT, 'forum block after', VALUE_OPTIONAL),
                        'blockperiod'  => new external_value(PARAM_INT, 'forum block period', VALUE_OPTIONAL),
                        'completiondiscussions'  => new external_value(PARAM_INT, 'forum completion discussions', VALUE_OPTIONAL),
                        'completionreplies' => new external_value(PARAM_INT, 'forum completion replies', VALUE_OPTIONAL),
                        'completionposts' => new external_value(PARAM_INT, 'forum completion posts', VALUE_OPTIONAL),
                    )), 'list of forum information')
            )
        );
    }


    /**
     * Update forum for a course
     *
     * @param array of  $forums - forum related info
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function update_forums($forums) {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::update_forums_parameters(),
            array('forums' => $forums));

        $warnings = array();
        $foruminfo = array();
        foreach ($forums as $forum) {
            //Ok either we have the forum id or module id
            $forumid = $forum['id'];
            if($forumid == 0) {
                //get forum id from module
                $cm = $DB->get_record('course_modules', array('id' => $forum['moduleid']), "*", MUST_EXIST);
                $forumid = $cm->instance;
            }
            $oldforum = $DB->get_record('forum', array('id'=>$forumid), "*", MUST_EXIST);
            $keys=array_keys((array)$oldforum);
            $keyid = array_search('id', $keys);
            if (false !== $keyid) {
                unset($keys[$keyid]);
            }
            foreach($keys as $keytoupdate){
                if(array_key_exists($keytoupdate, $forum))
                    $oldforum->$keytoupdate = $forum[$keytoupdate];
            }
            $mid = $DB->get_field('modules', 'id', array('name'=>'forum'));
            $cm = $DB->get_record('course_modules', array('course' => $oldforum->course, 'instance' => $oldforum->id, 'module'=>$mid), "*", MUST_EXIST);
            $data = array();
            $data = (array)$oldforum;
            $data['instance'] = $oldforum->id;
            $data['cmidnumber'] = $cm->id; // Removes warning
            $oldforum = (object) $data;

            require_once($CFG->dirroot . '/mod/forum/lib.php');

            $retval = forum_update_instance($oldforum, null);
            if(!$retval) {
                //User cannot update
                $warning = array();
                $warning['item'] = 'forum';
                $warning['itemid'] = $oldforum->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'Forum cannot be  updated';
                $warnings[] = $warning;
                $rassignments[] = $rassign;
                continue;
            }
            $foruminf = array();
            $foruminf['forumid'] = $oldforum->id;
            $foruminfo[] = $foruminf;
            rebuild_course_cache($oldforum->course);
        }
        $result = array();
        $result['forum'] = $foruminfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for update_forums
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_forums_returns() {
        return new external_single_structure(
            array(
                'forum' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'forumid' => new external_value(PARAM_INT, 'Forum Id that has been updated')
                        )),'forum id'),
                'warnings'  => new external_warnings()
            )
        );
    }
    /**
     * Describes the params for del_forums
     * @return external_single_structure
     * @since  Moodle 2.4
     */

    public static function del_forums_parameters() {
        return new external_function_parameters(
            array('forums' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'forumid' => new external_value(PARAM_INT, 'forum id', VALUE_DEFAULT, 0),
                        'moduleid' => new external_value(PARAM_INT, 'moduleid id', VALUE_DEFAULT, 0),
                    )), 'list of file resource information')
            )
        );
    }

    /**
     * remove forum
     * @param array of $forums info
     *
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function del_forums($forums) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::del_forums_parameters(),
            array('forums' => $forums));

        $warnings = array();
        $result = array();

        foreach ($forums as $forum) {
            $forumid = $forum['forumid'];
            if($forumid == 0) {
                $cm = $DB->get_record('course_modules', array('id' => $forum['moduleid']), "*", MUST_EXIST);
                $forumid = $cm->instance;
            }
            $foruminfo = $DB->get_record('forum', array('id' => $forumid), "*", MUST_EXIST);

            $context = context_course::instance($foruminfo->course);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $foruminfo->course;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to remove forum';
                $warnings[] = $warning;
                continue;
            }
            require_once($CFG->dirroot . '/mod/forum/lib.php');
            //Ok remove the course module and update cache
            $cm = get_coursemodule_from_instance('forum', $forumid, 0, false, MUST_EXIST);
            $DB->delete_records('course_modules', array('id'=>$cm->id));
            forum_delete_instance($forumid);
            rebuild_course_cache($cm->course);
        }

        $result['warnings'] = $warnings;
        return $result;

    }

    /**
     * Describes the return value for del_forums
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function del_forums_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Describes the parameters for get_forum.
     *
     * @return external_external_function_parameters
     * @since Moodle 2.5
     */
    public static function get_forums_by_courses_parameters() {
        return new external_function_parameters (
            array(
                'courseids' => new external_multiple_structure(new external_value(PARAM_INT, 'course ID',
                        '', VALUE_REQUIRED, '', NULL_NOT_ALLOWED), 'Array of Course IDs', VALUE_DEFAULT, array()),
            )
        );
    }

    /**
     * Returns a list of forums in a provided list of courses,
     * if no list is provided all forums that the user can view
     * will be returned.
     *
     * @param array $courseids the course ids
     * @return array the forum details
     * @since Moodle 2.5
     */
    public static function get_forums_by_courses($courseids = array()) {
        global $CFG, $DB, $USER;

        require_once($CFG->dirroot . "/mod/forum/lib.php");

        $params = self::validate_parameters(self::get_forums_by_courses_parameters(), array('courseids' => $courseids));

        if (empty($params['courseids'])) {
            // Get all the courses the user can view.
            $courseids = array_keys(enrol_get_my_courses());
        } else {
            $courseids = $params['courseids'];
        }

        // Array to store the forums to return.
        $arrforums = array();

        // Ensure there are courseids to loop through.
        if (!empty($courseids)) {
            // Go through the courseids and return the forums.
            foreach ($courseids as $cid) {
                // Get the course context.
                $context = context_course::instance($cid);
                // Check the user can function in this context.
                self::validate_context($context);
                // Get the forums in this course.
                if ($forums = $DB->get_records('forum', array('course' => $cid))) {
                    // Get the modinfo for the course.
                    $courseObj = (object) array('id' => $cid, 'modinfo' => null, 'sectioncache' => null);
                    $modinfo = get_fast_modinfo($courseObj);
                    // Get the forum instances.
                    $foruminstances = $modinfo->get_instances_of('forum');
                    // Loop through the forums returned by modinfo.
                    foreach ($foruminstances as $forumid => $cm) {
                        // If it is not visible or present in the forums get_records call, continue.
                        if (!$cm->uservisible || !isset($forums[$forumid])) {
                            continue;
                        }
                        // Set the forum object.
                        $forum = $forums[$forumid];
                        // Get the module context.
                        $context = context_module::instance($cm->id);
                        // Check they have the view forum capability.
                        require_capability('mod/forum:viewdiscussion', $context);
                        // Format the intro before being returning using the format setting.
                        list($forum->intro, $forum->introformat) = external_format_text($forum->intro, $forum->introformat,
                            $context->id, 'mod_forum', 'intro', 0);
                        // Add the course module id to the object, this information is useful.
                        $forum->cmid = $cm->id;
                        $forum->discussions = array();

                        // No HTML in forum description.
                        $forum->intro = strip_tags($forum->intro);

                        // Get the module context.
                        $modcontext = context_module::instance($cm->id);
                        // Check they have the view forum capability.
                        require_capability('mod/forum:viewdiscussion', $modcontext);
                        // Check if they can view full names.
                        $canviewfullname = has_capability('moodle/site:viewfullnames', $modcontext);
                        // Get the unreads array, this takes a forum id and returns data for all discussions.
                        $unreads = array();
                        if ($cantrack = forum_tp_can_track_forums($forum)) {
                            if ($forumtracked = forum_tp_is_tracked($forum)) {
                                $unreads = forum_get_discussions_unread($cm);
                            }
                        }
                        // The forum function returns the replies for all the discussions in a given forum.
                        $replies = forum_count_discussion_replies($forumid);
                        // Get the discussions for this forum.
                        if ($discussions = $DB->get_records('forum_discussions', array('forum' => $forumid))) {
                            foreach ($discussions as $discussion) {
                                // If the forum is of type qanda and the user has not posted in the discussion
                                // we need to ensure that they have the required capability.
                                if ($forum->type == 'qanda' && !forum_user_has_posted($discussion->forum, $discussion->id, $USER->id)) {
                                    require_capability('mod/forum:viewqandawithoutposting', $modcontext);
                                }
                                // If we don't have the users details then perform DB call.
                                if (empty($arrusers[$discussion->userid])) {
                                    $arrusers[$discussion->userid] = $DB->get_record('user', array('id' => $discussion->userid),
                                        'firstname, lastname, email, picture, imagealt', MUST_EXIST);
                                }
                                // Get the subject.
                                $subject = $DB->get_field('forum_posts', 'subject', array('id' => $discussion->firstpost), MUST_EXIST);
                                // Create object to return.
                                $return = new stdClass();
                                $return->id = (int) $discussion->id;
                                $return->course = $discussion->course;
                                $return->forum = $discussion->forum;
                                $return->name = $discussion->name;
                                $return->userid = $discussion->userid;
                                $return->groupid = $discussion->groupid;
                                $return->assessed = $discussion->assessed;
                                $return->timemodified = (int) $discussion->timemodified;
                                $return->usermodified = $discussion->usermodified;
                                $return->timestart = $discussion->timestart;
                                $return->timeend = $discussion->timeend;
                                $return->firstpost = (int) $discussion->firstpost;
                                $return->firstuserfullname = fullname($arrusers[$discussion->userid], $canviewfullname);
                                $return->firstuserimagealt = $arrusers[$discussion->userid]->imagealt;
                                $return->firstuserpicture = $arrusers[$discussion->userid]->picture;
                                $return->firstuseremail = $arrusers[$discussion->userid]->email;
                                $return->subject = $subject;
                                $return->numunread = '';
                                if ($cantrack && $forumtracked) {
                                    if (isset($unreads[$discussion->id])) {
                                        $return->numunread = (int) $unreads[$discussion->id];
                                    }
                                }
                                // Check if there are any replies to this discussion.
                                if (!empty($replies[$discussion->id])) {
                                     $return->numreplies = (int) $replies[$discussion->id]->replies;
                                     $return->lastpost = (int) $replies[$discussion->id]->lastpostid;
                                 } else { // No replies, so the last post will be the first post.
                                    $return->numreplies = 0;
                                    $return->lastpost = (int) $discussion->firstpost;
                                 }
                                // Get the last post as well as the user who made it.
                                $lastpost = $DB->get_record('forum_posts', array('id' => $return->lastpost), '*', MUST_EXIST);
                                if (empty($arrusers[$lastpost->userid])) {
                                    $arrusers[$lastpost->userid] = $DB->get_record('user', array('id' => $lastpost->userid),
                                        'firstname, lastname, email, picture, imagealt', MUST_EXIST);
                                }
                                $return->lastuserid = $lastpost->userid;
                                $return->lastuserfullname = fullname($arrusers[$lastpost->userid], $canviewfullname);
                                $return->lastuserimagealt = $arrusers[$lastpost->userid]->imagealt;
                                $return->lastuserpicture = $arrusers[$lastpost->userid]->picture;
                                $return->lastuseremail = $arrusers[$lastpost->userid]->email;
                                // Add the discussion statistics to the array to return.
                                $forum->discussions[$return->id] = (array) $return;
                            }
                        }

                        // Add the forum to the array to return.
                        $arrforums[$forum->id] = (array) $forum;
                    }
                }
            }
        }

        return $arrforums;
    }

    /**
     * Describes the get_forum return value.
     *
     * @return external_single_structure
     * @since Moodle 2.5
     */
     public static function get_forums_by_courses_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Forum id'),
                    'course' => new external_value(PARAM_TEXT, 'Course id'),
                    'type' => new external_value(PARAM_TEXT, 'The forum type'),
                    'name' => new external_value(PARAM_TEXT, 'Forum name'),
                    'intro' => new external_value(PARAM_RAW, 'The forum intro'),
                    'introformat' => new external_format_value('intro'),
                    'assessed' => new external_value(PARAM_INT, 'Aggregate type'),
                    'assesstimestart' => new external_value(PARAM_INT, 'Assess start time'),
                    'assesstimefinish' => new external_value(PARAM_INT, 'Assess finish time'),
                    'scale' => new external_value(PARAM_INT, 'Scale'),
                    'maxbytes' => new external_value(PARAM_INT, 'Maximum attachment size'),
                    'maxattachments' => new external_value(PARAM_INT, 'Maximum number of attachments'),
                    'forcesubscribe' => new external_value(PARAM_INT, 'Force users to subscribe'),
                    'trackingtype' => new external_value(PARAM_INT, 'Subscription mode'),
                    'rsstype' => new external_value(PARAM_INT, 'RSS feed for this activity'),
                    'rssarticles' => new external_value(PARAM_INT, 'Number of RSS recent articles'),
                    'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                    'warnafter' => new external_value(PARAM_INT, 'Post threshold for warning'),
                    'blockafter' => new external_value(PARAM_INT, 'Post threshold for blocking'),
                    'blockperiod' => new external_value(PARAM_INT, 'Time period for blocking'),
                    'completiondiscussions' => new external_value(PARAM_INT, 'Student must create discussions'),
                    'completionreplies' => new external_value(PARAM_INT, 'Student must post replies'),
                    'completionposts' => new external_value(PARAM_INT, 'Student must post discussions or replies'),
                    'cmid' => new external_value(PARAM_INT, 'Course module id'),
                    'discussions' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'id' => new external_value(PARAM_INT, 'Forum id'),
                                'course' => new external_value(PARAM_INT, 'Course id'),
                                'forum' => new external_value(PARAM_INT, 'The forum id'),
                                'name' => new external_value(PARAM_TEXT, 'Discussion name'),
                                'userid' => new external_value(PARAM_INT, 'User id'),
                                'groupid' => new external_value(PARAM_INT, 'Group id'),
                                'assessed' => new external_value(PARAM_INT, 'Is this assessed?'),
                                'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                                'usermodified' => new external_value(PARAM_INT, 'The id of the user who last modified'),
                                'timestart' => new external_value(PARAM_INT, 'Time discussion can start'),
                                'timeend' => new external_value(PARAM_INT, 'Time discussion ends'),
                                'firstpost' => new external_value(PARAM_INT, 'The first post in the discussion'),
                                'firstuserfullname' => new external_value(PARAM_TEXT, 'The discussion creators fullname'),
                                'firstuserimagealt' => new external_value(PARAM_TEXT, 'The discussion creators image alt'),
                                'firstuserpicture' => new external_value(PARAM_INT, 'The discussion creators profile picture'),
                                'firstuseremail' => new external_value(PARAM_TEXT, 'The discussion creators email'),
                                'subject' => new external_value(PARAM_TEXT, 'The discussion subject'),
                                'numreplies' => new external_value(PARAM_TEXT, 'The number of replies in the discussion'),
                                'numunread' => new external_value(PARAM_TEXT, 'The number of unread posts, blank if this value is
                                    not available due to forum settings.'),
                                'lastpost' => new external_value(PARAM_INT, 'The id of the last post in the discussion'),
                                'lastuserid' => new external_value(PARAM_INT, 'The id of the user who made the last post'),
                                'lastuserfullname' => new external_value(PARAM_TEXT, 'The last person to posts fullname'),
                                'lastuserimagealt' => new external_value(PARAM_TEXT, 'The last person to posts image alt'),
                                'lastuserpicture' => new external_value(PARAM_INT, 'The last person to posts profile picture'),
                                'lastuseremail' => new external_value(PARAM_TEXT, 'The last person to posts email')
                            ), 'discussion'
                        )
                    )
                ), 'forum'
            )
        );
    }

    /**
     * Describes the parameters for get_forum.
     *
     * @return external_external_function_parameters
     * @since Moodle 2.5
     */
    public static function forums_parameters() {
        return new external_function_parameters (
            array(
                'courseid' => new external_value(PARAM_INT, 'Course id')
            )
        );
    }

    /**
     * Returns a list of forums in a provided list of courses,
     * if no list is provided all forums that the user can view
     * will be returned.
     *
     * @param array $courseids the course ids
     * @return array the forum details
     * @since Moodle 2.5
     */
    public static function forums($courseid)
    {
        global $CFG, $DB, $USER;

        require_once($CFG->dirroot . "/mod/forum/lib.php");

        $params = self::validate_parameters(self::forums_parameters(), array('courseid' => $courseid));
        $courseid = $params['courseid'];

        $forumContext = null;

        // Array to store the forums to return.
        $arrforums = array();

        // List of ids of discussions of from all matching forums.
        $discussionIds = array();

        // Teacher ids in given courses (may give wrong data when multiple
        // courses selected. But no one uses multiple courses...).
        $teacherIds = array();

        // Get the course context.
        $context = context_course::instance($courseid);
        // Check the user can function in this context.
        self::validate_context($context);

        $users = get_enrolled_users($context);

        foreach ($users as $user)
        {
            $roles = get_user_roles($context , $user->id);

            foreach ($roles as $role)
            {
                if ($role->shortname == 'editingteacher' || $role->shortname == "manager")
                {
                    $teacherIds[] = $user->id;
                    break;
                }
            }
        }

        // Get user notifications - apparently we track unread posts this
        // way...
        $unreadArr = array();
        $forumNotifs = $DB->get_records_sql("SELECT p.discussion AS discussion, COUNT(*) AS cnt " .
            "FROM {monorailfeed_users} AS mfu " .
                "INNER JOIN {monorailfeed_notifications} AS mfn ON mfu.monorailfeednotificationsid=mfn.id " .
                "INNER JOIN {forum_posts} AS p ON p.id=mfn.instance " .
                    "WHERE mfu.userid=? AND NOT mfu.timeread AND mfn.type='forum' GROUP BY p.discussion",
                        array($USER->id));

        foreach ($forumNotifs as $fn)
        {
            $unreadArr[$fn->discussion] = $fn->cnt;
        }

        // Get the forums in this course.
        if ($forums = $DB->get_records('forum', array('course' => $courseid)))
        {
            // Get the modinfo for the course.
            $courseObj = (object) array('id' => $courseid, 'modinfo' => null, 'sectioncache' => null);
            $modinfo = get_fast_modinfo($courseObj);
            // Get the forum instances.
            $foruminstances = $modinfo->get_instances_of('forum');
            // Loop through the forums returned by modinfo.
            foreach ($foruminstances as $forumid => $cm) {
                // If it is not visible or present in the forums get_records call, continue.
                if (!$cm->uservisible || !isset($forums[$forumid])) {
                    continue;
                }
                // Set the forum object.
                $forum = $forums[$forumid];
                // Get the module context.
                $context = context_module::instance($cm->id);
                $forumContext = $context;
                // Check they have the view forum capability.
                require_capability('mod/forum:viewdiscussion', $context);
                // Format the intro before being returning using the format setting.
                list($forum->intro, $forum->introformat) = external_format_text($forum->intro, $forum->introformat,
                    $context->id, 'mod_forum', 'intro', 0);
                // Add the course module id to the object, this information is useful.
                $forum->cmid = $cm->id;
                $forum->discussions = array();

                // No HTML in forum description.
                $forum->intro = strip_tags($forum->intro);
                $forum->total_posts = 0;
                $forum->last_post_time = 0;

                // Get the module context.
                $modcontext = context_module::instance($cm->id);
                // Check they have the view forum capability.
                require_capability('mod/forum:viewdiscussion', $modcontext);
                // Check if they can view full names.
                $canviewfullname = has_capability('moodle/site:viewfullnames', $modcontext);
                // Get the unreads array, this takes a forum id and returns data for all discussions.
                $unreads = array();
                if ($cantrack = forum_tp_can_track_forums($forum)) {
                    if ($forumtracked = forum_tp_is_tracked($forum)) {
                        $unreads = forum_get_discussions_unread($cm);
                    }
                }
                // The forum function returns the replies for all the discussions in a given forum.
                $replies = forum_count_discussion_replies($forumid);
                // Get the discussions for this forum.
                if ($discussions = $DB->get_records('forum_discussions', array('forum' => $forumid)))
                {
                    $forumDiscussionIds = array();

                    foreach ($discussions as $discussion) {
                        // If the forum is of type qanda and the user has not posted in the discussion
                        // we need to ensure that they have the required capability.
                        if ($forum->type == 'qanda' && !forum_user_has_posted($discussion->forum, $discussion->id, $USER->id)) {
                            require_capability('mod/forum:viewqandawithoutposting', $modcontext);
                        }
                        // If we don't have the users details then perform DB call.
                        if (empty($arrusers[$discussion->userid])) {
                            $arrusers[$discussion->userid] = $DB->get_record('user', array('id' => $discussion->userid),
                                'firstname, lastname, email, picture, imagealt', MUST_EXIST);
                        }
                        // Get the subject.
                        $subject = $DB->get_field('forum_posts', 'subject', array('id' => $discussion->firstpost), MUST_EXIST);

                        $discussionIds[] = $discussion->id;
                        $forumDiscussionIds[] = $discussion->id;

                        // Create object to return.
                        $return = new stdClass();
                        $return->id = (int) $discussion->id;
                        $return->course = $discussion->course;
                        $return->forum = $discussion->forum;
                        $return->name = $discussion->name;
                        $return->userid = $discussion->userid;
                        $return->is_teacher = in_array($discussion->userid, $teacherIds) ? 1 : 0;
                        $return->groupid = $discussion->groupid;
                        $return->assessed = $discussion->assessed;
                        $return->timemodified = (int) $discussion->timemodified;
                        $return->usermodified = $discussion->usermodified;
                        $return->timestart = $discussion->timestart;
                        $return->timeend = $discussion->timeend;
                        $return->firstpost = (int) $discussion->firstpost;
                        $return->firstuserfullname = fullname($arrusers[$discussion->userid], $canviewfullname);
                        $return->firstuserimagealt = $arrusers[$discussion->userid]->imagealt;
                        $return->firstuserpicture = $arrusers[$discussion->userid]->picture;
                        $return->firstuseremail = $arrusers[$discussion->userid]->email;
                        $return->subject = $subject;
                        $return->numunread = @$unreadArr[$discussion->id];
                        // Check if there are any replies to this discussion.
                        if (!empty($replies[$discussion->id])) {
                             $return->numreplies = (int) $replies[$discussion->id]->replies;
                             $return->lastpost = (int) $replies[$discussion->id]->lastpostid;
                         } else { // No replies, so the last post will be the first post.
                            $return->numreplies = 0;
                            $return->lastpost = (int) $discussion->firstpost;
                         }
                        // Get the last post as well as the user who made it.
                        $lastpost = $DB->get_record('forum_posts', array('id' => $return->lastpost), '*', MUST_EXIST);
                        if (empty($arrusers[$lastpost->userid])) {
                            $arrusers[$lastpost->userid] = $DB->get_record('user', array('id' => $lastpost->userid),
                                'firstname, lastname, email, picture, imagealt', MUST_EXIST);
                        }
                        $return->lastuserid = $lastpost->userid;
                        $return->lastuserfullname = fullname($arrusers[$lastpost->userid], $canviewfullname);
                        $return->lastuserimagealt = $arrusers[$lastpost->userid]->imagealt;
                        $return->lastuserpicture = $arrusers[$lastpost->userid]->picture;
                        $return->lastuseremail = $arrusers[$lastpost->userid]->email;
                        $return->lastposttime = $lastpost->modified;
                        // Add the discussion statistics to the array to return.
                        $forum->discussions[$return->id] = (array) $return;
                    }

                    if (!empty($forumDiscussionIds))
                    {
                        $forum->total_posts = $DB->get_field_sql("SELECT COUNT(*) FROM {forum_posts} WHERE discussion IN (" . implode(", ", $forumDiscussionIds) . ")");
                        $forum->last_post_time = $DB->get_field_sql("SELECT MAX(modified) FROM {forum_posts} WHERE discussion IN (" . implode(", ", $forumDiscussionIds) . ")");
                    }
                }

                // Add the forum to the array to return.
                $arrforums[$forum->id] = (array) $forum;
            }
        }

        $arrRecentPosts = array();

        if (!empty($discussionIds))
        {
            // Collect the recent posts in selected forums.
            $recentPosts = $DB->get_records_sql("SELECT p.id AS id, p.discussion AS discussion, p.parent AS parent, p.userid AS userid, p.created AS created, ".
                "p.modified AS modified, p.mailed AS mailed, p.subject AS subject, p.message AS message, p.messageformat AS messageformat, p.messagetrust AS messagetrust, ".
                "p.attachment AS attachment, p.totalscore AS totalscore, p.mailnow AS mailnow, u.firstname AS firstname, u.lastname AS lastname, d.forum AS forum " .
                    "FROM {forum_posts} AS p INNER JOIN {user} AS u ON u.id=p.userid INNER JOIN {forum_discussions} AS d ON d.id=p.discussion " .
                    "WHERE p.discussion IN (" . implode(", ", $discussionIds) . ") ORDER BY p.modified DESC LIMIT 10");

            $fs = get_file_storage();

            foreach ($recentPosts as $rp)
            {
                $rp->profileimageurlsmall = "$CFG->wwwroot/public_images/user_picture/".monorail_get_userpic_hash($rp->userid)."_32.jpg";
                $rp->profileimageurl = "$CFG->wwwroot/public_images/user_picture/".monorail_get_userpic_hash($rp->userid)."_90.jpg";
                $rp->is_teacher = in_array($rp->userid, $teacherIds) ? 1 : 0;
                $rp->attachments = array();

                $cms = $DB->get_records_sql("SELECT cm.id AS id, d.course AS course FROM {modules} AS m, {forum_discussions} AS d, {course_modules} AS cm" .
                    " WHERE cm.module=m.id AND cm.instance=d.forum AND m.name='forum' AND d.id=?", array($rp->discussion));

                $cms = reset($cms);

                $modcontext = context_module::instance($cms->id);

                $files = $fs->get_area_files($modcontext->id, 'mod_forum', 'attachment', $rp->id);

                foreach ($files as $file)
                {
                    if ($file->get_filename() !== '.')
                    {
                        $attach = new stdClass();

                        $attach->url =  monorail_converted_fileurl($file, true);

                        if (!$attach->url)
                        {
                            $attach->url = "{$CFG->wwwroot}/pluginfile.php/{$file->get_contextid()}/{$file->get_component()}/{$file->get_filearea()}{$file->get_filepath()}{$file->get_itemid()}/{$file->get_filename()}";
                            $attach->mimetype = $file->get_mimetype();
                            $attach->converted = 0;
                        }
                        else
                        {
                            $attach->mimetype = "application/pdf";
                            $attach->converted = 1;
                        }

                        $attach->filename = $file->get_filename();

                        $rp->attachments[] = (array) $attach;
                    }
                }

                $arrRecentPosts[] = (array) $rp;
            }
        }
        
        $arrRecentDiscussions = self::recent_discussions($discussionIds);
        return array("forums" => $arrforums, "recent_posts" => $arrRecentPosts, "recent_discussions" => $arrRecentDiscussions);
    }
    
    /*
     * returns the recently active discussions
     * Note: No security check is done, the caller has to do those
     */
    private static function recent_discussions($discussionIds)
    {
        global $DB;
        if (empty($discussionIds)) {
            return array();
        }
        $rd = array();
        $offset = 0;
        $limit = 500; //batch size
        $recent_discussion_size = 10; //number of recent discussions to return
        $allowed_batch_reads = 5; //we will read this many batches or records at the maximum
        $read_count = 0;
        //read in batches of records and try to find 10 individual discussions
        while( (count($rd) < $recent_discussion_size) &&($read_count < $allowed_batch_reads) ) {
            $read_count = $read_count + 1;
            $recentDiscussions = $DB->get_records_sql("SELECT id, discussion, modified  FROM {forum_posts} " .
                    "WHERE discussion IN (" . implode(", ", $discussionIds) . ") ORDER BY modified DESC LIMIT ". $limit . " OFFSET " . $offset);
            if(!$recentDiscussions) {
                break;
            }
            $offset = $offset + $limit;
            foreach ($recentDiscussions as $recent_discussion) {
                if(!array_key_exists($recent_discussion->discussion, $rd )) {
                    $rd[$recent_discussion->discussion] = $recent_discussion->modified;
                    if((count($rd) >= $recent_discussion_size)) {
                        break;
                    }
                }
            }
        }
        $ret = array();
        if(count($rd)) {
            $recentDiscussionIDs = array_keys($rd);
            $discussionDetails = $DB->get_records_sql("SELECT id, name, forum  FROM {forum_discussions} " .
                    "WHERE id IN (" . implode(", ", $recentDiscussionIDs) . ")");
            foreach ($discussionDetails as $discussionDetail) {
                $discussionDetail->modified = $rd[$discussionDetail->id];
                $ret[] = (array)$discussionDetail;
            }
        }
        return $ret;
    }

    /**
     * Describes the get_forum return value.
     *
     * @return external_single_structure
     * @since Moodle 2.5
     */
     public static function forums_returns() {
        return new external_single_structure(array(
         "forums" => new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Forum id'),
                    'course' => new external_value(PARAM_TEXT, 'Course id'),
                    'type' => new external_value(PARAM_TEXT, 'The forum type'),
                    'name' => new external_value(PARAM_TEXT, 'Forum name'),
                    'intro' => new external_value(PARAM_RAW, 'The forum intro'),
                    'introformat' => new external_format_value('intro'),
                    'total_posts' => new external_value(PARAM_INT, 'Total number of posts'),
                    'last_post_time' => new external_value(PARAM_INT, 'Time of last post'),
                    'assessed' => new external_value(PARAM_INT, 'Aggregate type'),
                    'assesstimestart' => new external_value(PARAM_INT, 'Assess start time'),
                    'assesstimefinish' => new external_value(PARAM_INT, 'Assess finish time'),
                    'scale' => new external_value(PARAM_INT, 'Scale'),
                    'maxbytes' => new external_value(PARAM_INT, 'Maximum attachment size'),
                    'maxattachments' => new external_value(PARAM_INT, 'Maximum number of attachments'),
                    'forcesubscribe' => new external_value(PARAM_INT, 'Force users to subscribe'),
                    'trackingtype' => new external_value(PARAM_INT, 'Subscription mode'),
                    'rsstype' => new external_value(PARAM_INT, 'RSS feed for this activity'),
                    'rssarticles' => new external_value(PARAM_INT, 'Number of RSS recent articles'),
                    'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                    'warnafter' => new external_value(PARAM_INT, 'Post threshold for warning'),
                    'blockafter' => new external_value(PARAM_INT, 'Post threshold for blocking'),
                    'blockperiod' => new external_value(PARAM_INT, 'Time period for blocking'),
                    'completiondiscussions' => new external_value(PARAM_INT, 'Student must create discussions'),
                    'completionreplies' => new external_value(PARAM_INT, 'Student must post replies'),
                    'completionposts' => new external_value(PARAM_INT, 'Student must post discussions or replies'),
                    'cmid' => new external_value(PARAM_INT, 'Course module id'),
                    'discussions' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'id' => new external_value(PARAM_INT, 'Forum id'),
                                'course' => new external_value(PARAM_INT, 'Course id'),
                                'forum' => new external_value(PARAM_INT, 'The forum id'),
                                'name' => new external_value(PARAM_TEXT, 'Discussion name'),
                                'userid' => new external_value(PARAM_INT, 'User id'),
                                'is_teacher' => new external_value(PARAM_INT, 'Is user a teacher'),
                                'groupid' => new external_value(PARAM_INT, 'Group id'),
                                'assessed' => new external_value(PARAM_INT, 'Is this assessed?'),
                                'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                                'usermodified' => new external_value(PARAM_INT, 'The id of the user who last modified'),
                                'timestart' => new external_value(PARAM_INT, 'Time discussion can start'),
                                'timeend' => new external_value(PARAM_INT, 'Time discussion ends'),
                                'firstpost' => new external_value(PARAM_INT, 'The first post in the discussion'),
                                'firstuserfullname' => new external_value(PARAM_TEXT, 'The discussion creators fullname'),
                                'firstuserimagealt' => new external_value(PARAM_TEXT, 'The discussion creators image alt'),
                                'firstuserpicture' => new external_value(PARAM_INT, 'The discussion creators profile picture'),
                                'firstuseremail' => new external_value(PARAM_TEXT, 'The discussion creators email'),
                                'subject' => new external_value(PARAM_TEXT, 'The discussion subject'),
                                'numreplies' => new external_value(PARAM_TEXT, 'The number of replies in the discussion'),
                                'numunread' => new external_value(PARAM_TEXT, 'The number of unread posts, blank if this value is
                                    not available due to forum settings.'),
                                'lastpost' => new external_value(PARAM_INT, 'The id of the last post in the discussion'),
                                'lastuserid' => new external_value(PARAM_INT, 'The id of the user who made the last post'),
                                'lastuserfullname' => new external_value(PARAM_TEXT, 'The last person to posts fullname'),
                                'lastuserimagealt' => new external_value(PARAM_TEXT, 'The last person to posts image alt'),
                                'lastuserpicture' => new external_value(PARAM_INT, 'The last person to posts profile picture'),
                                'lastuseremail' => new external_value(PARAM_TEXT, 'The last person to posts email'),
                                'lastposttime' => new external_value(PARAM_TEXT, 'Time of last post')
                            ), 'discussion'
                        )
                    )
                ), 'forum'
            )),
         "recent_posts" => new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'Post id'),
                'discussion' => new external_value(PARAM_INT, 'discussion id'),
                'forum' => new external_value(PARAM_INT, 'forum id'),
                'parent' => new external_value(PARAM_INT, 'parent id'),
                'userid' => new external_value(PARAM_INT, 'user id'),
                'is_teacher' => new external_value(PARAM_INT, 'User is teacher'),
                'created' => new external_value(PARAM_INT, 'created'),
                'modified' => new external_value(PARAM_INT, 'modified'),
                'mailed' => new external_value(PARAM_INT, 'mailed'),
                'subject' => new external_value(PARAM_TEXT, 'Subject'),
                'message' => new external_value(PARAM_RAW, 'Message'),
                'messageformat' => new external_value(PARAM_INT, 'format'),
                'messagetrust' => new external_value(PARAM_INT, 'trust'),
                'attachment' => new external_value(PARAM_RAW, 'attachment'),
                'totalscore' => new external_value(PARAM_INT, 'totalscore'),
                'mailnow' => new external_value(PARAM_INT, 'mailnow'),
                'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                'profileimageurlsmall' => new external_value(PARAM_URL, 'profileimageurlsmall'),
                'profileimageurl' => new external_value(PARAM_URL, 'profileimageurl'),
                'attachments' => new external_multiple_structure(new external_single_structure(array(
                    'filename' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'filename'),
                    'mimetype' => new external_value(PARAM_TEXT, 'mimetype'),
                    'url' => new external_value(PARAM_RAW, 'url'),
                    'converted' => new external_value(PARAM_INT, 'converted to pdf?')), "attachment"))
            ))),
            "recent_discussions" => new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'discussion id'),
                'name' => new external_value(PARAM_TEXT, 'discussion name'),
                'forum' => new external_value(PARAM_TEXT, 'parent forum name'),
                'modified' => new external_value(PARAM_INT, 'time of last post in this discussion'),
            )))
        ));
    }

    public static function get_forum_posts_parameters()
    {
        return new external_function_parameters (
            array(
                'discussionid' => new external_value(PARAM_INT, 'discussion ID', '', VALUE_REQUIRED, '', NULL_NOT_ALLOWED)
            )
        );
    }

    public static function get_forum_posts($discussionid)
    {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::get_forum_posts_parameters(), array('discussionid' => $discussionid));

        require_once($CFG->dirroot . "/mod/forum/lib.php");
        require_once($CFG->dirroot . '/user/lib.php');

        $cms = $DB->get_records_sql("SELECT cm.id AS id, d.course AS course FROM {modules} AS m, {forum_discussions} AS d, {course_modules} AS cm" .
            " WHERE cm.module=m.id AND cm.instance=d.forum AND m.name='forum' AND d.id=?", array($params["discussionid"]));

        $cms = reset($cms);

        $modcontext = context_module::instance($cms->id);
        require_capability('mod/forum:viewdiscussion', $modcontext);

        $fs = get_file_storage();

        $posts = forum_get_all_discussion_posts($params["discussionid"], "created");
        $result = array();

        $forumid = $DB->get_field_sql("SELECT forum FROM {forum_discussions} WHERE id=?", array($discussionid));

        foreach ($posts as $post)
        {
            $user = new stdClass();
            $user->id = $post->userid;
            $course = new stdClass();
            $course->id = $cms->course;

            $userData = user_get_user_details($user, $course, array('profileimageurlsmall', 'profileimageurl', 'roles'));

            $post->profileimageurlsmall = $userData["profileimageurlsmall"];
            $post->profileimageurl = $userData["profileimageurl"];
            $post->roles = $userData["roles"];
            $post->attachments = array();
            $post->forum = $forumid;
            $post->is_teacher = 0;

            foreach ($post->roles as $pr)
            {
                if ($pr["shortname"] == "editingteacher" || $pr["shortname"] == "manager")
                {
                    $post->is_teacher = 1;
                    break;
                }
            }

            $files = $fs->get_area_files($modcontext->id, 'mod_forum', 'attachment', $post->id);

            foreach ($files as $file)
            {
                if ($file->get_filename() !== '.')
                {
                    $attach = new stdClass();

                    $attach->url = file_encode_url("$CFG->wwwroot/local/monorailservices/pluginfile.php?token=WSTOKEN&file=path=",$file->get_pathnamehash(),false);
                    $attach->filename = $file->get_filename();
                    $attach->mimetype = $file->get_mimetype();

                    $post->attachments[] = (array) $attach;
                }
            }

            $result[] = (array) $post;
        }

        return $result;
    }

    public static function get_forum_posts_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Post id'),
                    'discussion' => new external_value(PARAM_INT, 'discussion id'),
                    'forum' => new external_value(PARAM_INT, 'forum id'),
                    'parent' => new external_value(PARAM_INT, 'parent id'),
                    'userid' => new external_value(PARAM_INT, 'user id'),
                    'created' => new external_value(PARAM_INT, 'created'),
                    'modified' => new external_value(PARAM_INT, 'modified'),
                    'mailed' => new external_value(PARAM_INT, 'mailed'),
                    'subject' => new external_value(PARAM_TEXT, 'Subject'),
                    'message' => new external_value(PARAM_RAW, 'Message'),
                    'messageformat' => new external_value(PARAM_INT, 'format'),
                    'messagetrust' => new external_value(PARAM_INT, 'trust'),
                    'attachment' => new external_value(PARAM_RAW, 'attachment'),
                    'totalscore' => new external_value(PARAM_INT, 'totalscore'),
                    'mailnow' => new external_value(PARAM_INT, 'mailnow'),
                    'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                    'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                    'email' => new external_value(PARAM_RAW, 'email', VALUE_OPTIONAL),
                    'maildisplay' => new external_value(PARAM_INT, 'email display', VALUE_OPTIONAL),
                    'picture' => new external_value(PARAM_RAW, 'picture'),
                    'imagealt' => new external_value(PARAM_RAW, 'imagealt'),
                    'profileimageurlsmall' => new external_value(PARAM_RAW, 'profileimageurlsmall'),
                    'profileimageurl' => new external_value(PARAM_RAW, 'profileimageurl'),
                    'is_teacher' => new external_value(PARAM_INT, 'teacher posted?'),
                    'roles' => new external_multiple_structure(new external_single_structure(array(
                        'roleid' => new external_value(PARAM_INT, 'role id'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                        'sortorder' => new external_value(PARAM_INT, 'sortorder')), "role")),
                    'attachments' => new external_multiple_structure(new external_single_structure(array(
                        'filename' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'filename'),
                        'mimetype' => new external_value(PARAM_TEXT, 'mimetype'),
                        'url' => new external_value(PARAM_RAW, 'url')), "attachment"))
                ), 'post'
            )
        );
    }

    public static function forum_posts_parameters()
    {
        return new external_function_parameters (
            array(
                'discussionid' => new external_value(PARAM_INT, 'discussion ID', '', VALUE_REQUIRED, '', NULL_NOT_ALLOWED)
            )
        );
    }

    public static function forum_posts($discussionid)
    {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::forum_posts_parameters(), array('discussionid' => $discussionid));

        require_once($CFG->dirroot . "/mod/forum/lib.php");
        require_once($CFG->dirroot . '/user/lib.php');

        $cms = $DB->get_records_sql("SELECT cm.id AS id, d.course AS course FROM {modules} AS m, {forum_discussions} AS d, {course_modules} AS cm" .
            " WHERE cm.module=m.id AND cm.instance=d.forum AND m.name='forum' AND d.id=?", array($params["discussionid"]));

        $cms = reset($cms);

        $modcontext = context_module::instance($cms->id);
        require_capability('mod/forum:viewdiscussion', $modcontext);

        $fs = get_file_storage();

        $posts = forum_get_all_discussion_posts($params["discussionid"], "created");
        $result = array();

        $forumid = $DB->get_field_sql("SELECT forum FROM {forum_discussions} WHERE id=?", array($discussionid));
        $notifIds = array();

        foreach ($posts as $post)
        {
            $user = new stdClass();
            $user->id = $post->userid;
            $course = new stdClass();
            $course->id = $cms->course;

            $userData = user_get_user_details($user, $course, array('profileimageurlsmall', 'profileimageurl', 'roles'));

            $post->profileimageurlsmall = $userData["profileimageurlsmall"];
            $post->profileimageurl = $userData["profileimageurl"];
            $post->roles = $userData["roles"];
            $post->attachments = array();
            $post->forum = $forumid;
            $post->is_teacher = 0;
            $post->is_new = 0;

            foreach ($post->roles as $pr)
            {
                if ($pr["shortname"] == "editingteacher" || $pr["shortname"] == "manager")
                {
                    $post->is_teacher = 1;
                    break;
                }
            }

            $notif = $DB->get_records_sql("SELECT mfn.id AS id, mfu.timeread AS timeread FROM {monorailfeed_notifications} AS mfn " .
                "INNER JOIN {monorailfeed_users} AS mfu ON mfu.monorailfeednotificationsid=mfn.id " .
                    "WHERE mfu.userid=? AND mfn.type='forum' AND mfn.instance=?", array($USER->id, $post->id));

            foreach ($notif as $nf)
            {
                if (!$nf->timeread)
                {
                    $post->is_new = 1;
                    $notifIds[] = $nf->id;
                }
                else if (time() - $nf->timeread < 4)
                {
                    $post->is_new = 1;
                }
            }

            $files = $fs->get_area_files($modcontext->id, 'mod_forum', 'attachment', $post->id);

            foreach ($files as $file)
            {
                if ($file->get_filename() !== '.')
                {
                    $attach = new stdClass();

                    $attach->url =  monorail_converted_fileurl($file, true);

                    if (!$attach->url)
                    {
                        $attach->url = "{$CFG->wwwroot}/pluginfile.php/{$file->get_contextid()}/{$file->get_component()}/{$file->get_filearea()}{$file->get_filepath()}{$file->get_itemid()}/{$file->get_filename()}";
                        $attach->mimetype = $file->get_mimetype();
                        $attach->converted = 0;
                    }
                    else
                    {
                        $attach->mimetype = "application/pdf";
                        $attach->converted = 1;
                    }

                    $attach->filename = $file->get_filename();
                    $attach->fileid = $file->get_id();

                    $post->attachments[] = (array) $attach;
                }
            }

            $result[] = (array) $post;
        }

        // Mark all posts as read.
        if (!empty($notifIds))
        {
            $DB->execute("UPDATE {monorailfeed_users} SET timeread=" . time() . " WHERE userid=" . $USER->id .
                " AND monorailfeednotificationsid IN (" . implode(", ", $notifIds) . ")");
        }

        return $result;
    }

    public static function forum_posts_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Post id'),
                    'discussion' => new external_value(PARAM_INT, 'discussion id'),
                    'forum' => new external_value(PARAM_INT, 'forum id'),
                    'parent' => new external_value(PARAM_INT, 'parent id'),
                    'userid' => new external_value(PARAM_INT, 'user id'),
                    'created' => new external_value(PARAM_INT, 'created'),
                    'modified' => new external_value(PARAM_INT, 'modified'),
                    'mailed' => new external_value(PARAM_INT, 'mailed'),
                    'subject' => new external_value(PARAM_TEXT, 'Subject'),
                    'message' => new external_value(PARAM_RAW, 'Message'),
                    'messageformat' => new external_value(PARAM_INT, 'format'),
                    'messagetrust' => new external_value(PARAM_INT, 'trust'),
                    'attachment' => new external_value(PARAM_RAW, 'attachment'),
                    'totalscore' => new external_value(PARAM_INT, 'totalscore'),
                    'mailnow' => new external_value(PARAM_INT, 'mailnow'),
                    'firstname' => new external_value(PARAM_TEXT, 'firstname'),
                    'lastname' => new external_value(PARAM_TEXT, 'lastname'),
                    'email' => new external_value(PARAM_RAW, 'email', VALUE_OPTIONAL),
                    'maildisplay' => new external_value(PARAM_INT, 'email display', VALUE_OPTIONAL),
                    'picture' => new external_value(PARAM_RAW, 'picture'),
                    'imagealt' => new external_value(PARAM_RAW, 'imagealt'),
                    'profileimageurlsmall' => new external_value(PARAM_RAW, 'profileimageurlsmall'),
                    'profileimageurl' => new external_value(PARAM_RAW, 'profileimageurl'),
                    'is_teacher' => new external_value(PARAM_INT, 'teacher posted?'),
                    'is_new' => new external_value(PARAM_INT, 'is post new?'),
                    'roles' => new external_multiple_structure(new external_single_structure(array(
                        'roleid' => new external_value(PARAM_INT, 'role id'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                        'sortorder' => new external_value(PARAM_INT, 'sortorder')), "role")),
                    'attachments' => new external_multiple_structure(new external_single_structure(array(
                        'filename' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'filename'),
                        'mimetype' => new external_value(PARAM_TEXT, 'mimetype'),
                        'url' => new external_value(PARAM_RAW, 'url'),
                        'converted' => new external_value(PARAM_INT, 'converted to pdf?'),
                        'fileid' => new external_value(PARAM_INT, 'file id', VALUE_OPTIONAL)), "attachment"))
                ), 'post'
            )
        );
    }

    public static function update_forum_discussion_parameters()
    {
        return new external_function_parameters(array(
            'discussions' => new external_multiple_structure(
                new external_single_structure(array(
                    "id" => new external_value(PARAM_INT, 'topic ID', VALUE_OPTIONAL),
                    'forum' => new external_value(PARAM_INT, 'forum id', VALUE_OPTIONAL),
                    'course' => new external_value(PARAM_INT, 'course id', VALUE_OPTIONAL),
                    'name' => new external_value(PARAM_TEXT, 'Discussion name'),
                    'message' => new external_value(PARAM_RAW, 'First message', VALUE_OPTIONAL),
                    'new_attachments' => new external_multiple_structure(
                        new external_single_structure(array(
                            "name" => new external_value(PARAM_RAW, 'Attachment file'),
                            'id' => new external_value(PARAM_INT, 'Attachment id', VALUE_DEFAULT, 0)
                        )), "New post attachments", VALUE_DEFAULT, array()),
                    'deleted_attachments' => new external_multiple_structure(new external_value(PARAM_INT, 'Attachment file id'), "Deleted post attachments", VALUE_DEFAULT, array())
                )), "discussion")
            ));
    }

    public static function update_forum_discussion($discussions)
    {
        global $USER, $DB, $CFG;

        $params = self::validate_parameters(self::update_forum_discussion_parameters(),
            array('discussions' => $discussions));

        $warnings = array();
        $discInfo = array();

        foreach ($discussions as $discussion)
        {
            $context = context_course::instance($discussion['course']);

            try
            {
                self::validate_context($context);
                require_capability('mod/forum:startdiscussion', $context);
            }
            catch (Exception $e)
            {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $url['course'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to start discussions';
                $warnings[] = $warning;
                continue;
            }

            if (!isset($discussion["id"]) && isset($discussion["forum"]) && isset($discussion["course"]))
            {
                // Creating new discussion.
                require_once($CFG->dirroot . "/mod/forum/lib.php");

                $data = new stdClass();

                $data->course = $discussion["course"];
                $data->forum = $discussion["forum"];
                $data->name = $discussion["name"];
                $data->message = strip_tags($discussion["message"], "<p><a><br><b><u><strong><em><ul><li><ol><i><sub><sup><blockquote>");
                $data->messageformat = 1;
                $data->messagetrust = 0;
                $data->mailnow = 0;

                $discId = forum_add_discussion($data);

                $discInfo[] = array("id" => $discId);

                $postId = $DB->get_field_sql("SELECT id FROM {forum_posts} WHERE discussion=? ORDER BY id DESC LIMIT 1", array($discId));

                try {
                    monorailfeed_create_notification($discussion["course"], 'post', 'forum', $postId, $USER->id);
                } catch (Exception $ex) {
                    add_to_log($courseId, 'monorail', 'lib.monorail_forum_save_post', '', 'Error! Failed to create a notification: '.get_class($ex).': '.$ex->getMessage());
                }

                if ($postId && isset($discussion["new_attachments"]) && !empty($discussion["new_attachments"]))
                {
                    // context
                    $cm = get_coursemodule_from_instance('forum', $discussion["forum"], $discussion["course"], false, MUST_EXIST);
                    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

                    // save attachment
                    $fs = get_file_storage();

                    $user = $DB->get_record('user', array('id' => $USER->id));

                    foreach ($discussion["new_attachments"] as $attachfilename)
                    {
                        if ($attachfilename["id"]) {
                            continue;
                        }
                        // Prepare file record object
                        $fileinfo = array(
                            'contextid' => $context->id, // ID of context
                            'component' => 'mod_forum',     // usually = table name
                            'filearea' => 'attachment',     // usually = table name
                            'itemid' => $postId,               // usually = ID of row in table
                            'userid' => $USER->id,
                            'source' => $attachfilename["name"],
                            'author' => fullname($USER),
                            'license' => 'allrightsreserved',
                            'filepath' => '/',           // any path beginning and ending in /
                            'filename' => $attachfilename["name"]); // any filename

                        $path = $CFG->dataroot.'/temp/files/'.$USER->id.'/'.$attachfilename["name"];
                        $file = $fs->create_file_from_pathname($fileinfo, $path);

                        //monorail_trigger_document_conversion($file, $path);

                        // remove temp files
                        unlink($path);
                        unlink($CFG->dataroot.'/temp/files/'.$USER->id.'/thumbnail/'.$attachfilename["name"]);
                    }

                    // update post
                    $postRec = new stdClass();
                    $postRec->id = $postId;
                    $postRec->attachment = count($discussion["new_attachments"]);
                    $DB->update_record('forum_posts', $postRec);
                }
            }
            else if (isset($discussion["id"]))
            {
                // Updating discussion.

                $data = $DB->get_record("forum_discussions", array("id" => $discussion["id"]));

                $data->name = $discussion["name"];
                $data->usermodified = $USER->id;
                $data->timemodified = time();

                $DB->update_record("forum_discussions", $data);

                $discInfo[] = array("id" => $discussion["id"]);
            }
            else
            {
                // Not enough data to create new discussion.

                $warning = array();
                $warning['item'] = 'forum';
                $warning['itemid'] = $resource['forum'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Unable to create forum discussion.';
                $warnings[] = $warning;
            }
        }

        $result = array();
        $result['discussions'] = $discInfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    public static function update_forum_discussion_returns()
    {
        return new external_single_structure(array(
                'discussions' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'Discussion Id')
                        )),'discussion'),
                'warnings'  => new external_warnings()));
    }

    public static function update_forum_post_parameters()
    {
        return new external_function_parameters(array(
            'posts' => new external_multiple_structure(
                new external_single_structure(array(
                    'id' => new external_value(PARAM_INT, 'Post id', VALUE_OPTIONAL),
                    'discussion' => new external_value(PARAM_INT, 'discussion id', VALUE_OPTIONAL),
                    'subject' => new external_value(PARAM_TEXT, 'Subject', VALUE_OPTIONAL),
                    'parent' => new external_value(PARAM_INT, 'Parent post', VALUE_DEFAULT, 0),
                    'message' => new external_value(PARAM_RAW, 'Message'),
                    'new_attachments' => new external_multiple_structure(
                        new external_single_structure(array(
                            "name" => new external_value(PARAM_RAW, 'Attachment file'),
                            'id' => new external_value(PARAM_INT, 'Attachment id', VALUE_DEFAULT, 0)
                        )), "New post attachments", VALUE_DEFAULT, array()),
                    'deleted_attachments' => new external_multiple_structure(new external_value(PARAM_INT, 'Attachment file id'), "Deleted post attachments", VALUE_DEFAULT, array())
                )), "post")
            ));
    }

    public static function update_forum_post($posts)
    {
        global $USER, $DB, $CFG;

        $params = self::validate_parameters(self::update_forum_post_parameters(),
            array('posts' => $posts));

        $warnings = array();
        $postInfo = array();

        foreach ($posts as $post)
        {
            $postId = null;
            $discussionId = null;
            $forumId = null;
            $courseId = null;

            if (!isset($post["id"]) && isset($post["discussion"]))
            {
                // Creating new post.

                require_once($CFG->dirroot . "/mod/forum/lib.php");

                $data = new stdClass();

                $data->discussion = $post["discussion"];
                $data->userid = $USER->id;
                $data->created = time();
                $data->modified = time();
                $data->subject = strip_tags(@$post["subject"], "<p><a><br><b><u><strong><em><ul><li><ol><i><sub><sup><blockquote><s>");
                $data->message = strip_tags(@$post["message"], "<p><a><br><b><u><strong><em><ul><li><ol><i><sub><sup><blockquote><s>");
                $data->messageformat = 1;
                $data->messagetrust = 0;
                $data->mailed = "0";
                $data->attachment = "";
                $data->parent = $post["parent"];

                $discussion = $DB->get_record('forum_discussions', array('id' => $data->discussion));
                $forum = $DB->get_record('forum', array('id' => $discussion->forum));
                $firstpost = $DB->get_record("forum_posts", array("id" => $discussion->firstpost));

                $discussionId = $data->discussion;
                $forumId = $discussion->forum;
                $courseId = $discussion->course;

                $context = context_course::instance($discussion->course);

                try
                {
                    self::validate_context($context);
                    require_capability('mod/forum:replypost', $context);
                }
                catch (Exception $e)
                {
                    $warning = array();
                    $warning['item'] = 'course';
                    $warning['itemid'] = $discussion->course;
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'No access rights in course context to post to forums';
                    $warnings[] = $warning;
                    continue;
                }

                if (!$data->subject)
                {
                    $data->subject = $firstpost->subject;
                }

                $data->id = $DB->insert_record("forum_posts", $data);

                $postId = $data->id;

                // Update discussion modified date
                $DB->set_field("forum_discussions", "timemodified", $data->modified, array("id" => $data->discussion));
                $DB->set_field("forum_discussions", "usermodified", $data->userid, array("id" => $data->discussion));

                if (forum_tp_can_track_forums($forum) && forum_tp_is_tracked($forum)) {
                    forum_tp_mark_post_read($data->userid, $data, $data->forum);
                }

                $postInfo[] = array("id" => $data->id);

                try {
                    monorailfeed_create_notification($courseId, 'post', 'forum', $data->id, $USER->id);
                } catch (Exception $ex) {
                    add_to_log($courseId, 'monorail', 'lib.monorail_forum_save_post', '', 'Error! Failed to create a notification: '.get_class($ex).': '.$ex->getMessage());
                }
            }
            else if (isset($post["id"]))
            {
                // Updating discussion.

                $data = $DB->get_record("forum_posts", array("id" => $post["id"]));

                $postId = $post["id"];
                $discussionId = $data->discussion;
                $discussion = $DB->get_record("forum_discussions", array("id" => $discussionId));
                $forumId = $discussion->forum;
                $courseId = $discussion->course;

                if ($data->userid != $USER->id)
                {
                    $warning = array();
                    $warning['item'] = 'post';
                    $warning['itemid'] = $post["id"];
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'No access rights to modify this post';
                    $warnings[] = $warning;
                    continue;
                }

                $data->modified = time();
                $data->subject = strip_tags($post["subject"], "<p><a><br><b><u><strong><em><ul><li><ol><i><sub><sup><blockquote>");
                $data->message = strip_tags($post["message"], "<p><a><br><b><u><strong><em><ul><li><ol><i><sub><sup><blockquote>");

                $DB->update_record("forum_posts", $data);

                $postInfo[] = array("id" => $post["id"]);
            }
            else
            {
                // Not enough data to create new discussion.

                $warning = array();
                $warning['item'] = 'discussion';
                $warning['itemid'] = $resource['discussion'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Unable to create forum post.';
                $warnings[] = $warning;
            }

            if ($postId)
            {
                // context
                $cm = get_coursemodule_from_instance('forum', $forumId, $courseId, false, MUST_EXIST);
                $context = get_context_instance(CONTEXT_MODULE, $cm->id);

                // save attachment
                $fs = get_file_storage();

                $user = $DB->get_record('user', array('id' => $USER->id));

                if (!empty($post["deleted_attachments"]))
                {
                    $files = $fs->get_area_files($context->id, 'mod_forum', 'attachment', $postId);

                    foreach ($files as $file) {
                        if (in_array($file->get_id(), $post["deleted_attachments"])) {
                            $file->delete();
                        }
                    }
                }

                if (isset($post["new_attachments"]) && !empty($post["new_attachments"]))
                {
                    foreach ($post["new_attachments"] as $attachfilename)
                    {
                        if ($attachfilename["id"]) {
                            continue;
                        }
                        $path = $CFG->dataroot.'/temp/files/'.$USER->id.'/'.$attachfilename["name"];

                        if (!file_exists($path)) {
                            continue;
                        }

                        // Prepare file record object
                        $fileinfo = array(
                            'contextid' => $context->id, // ID of context
                            'component' => 'mod_forum',     // usually = table name
                            'filearea' => 'attachment',     // usually = table name
                            'itemid' => $postId,               // usually = ID of row in table
                            'userid' => $USER->id,
                            'source' => $attachfilename["name"],
                            'author' => fullname($USER),
                            'license' => 'allrightsreserved',
                            'filepath' => '/',           // any path beginning and ending in /
                            'filename' => $attachfilename["name"]); // any filename

                        $file = $fs->create_file_from_pathname($fileinfo, $path);

                        //monorail_trigger_document_conversion($file, $path);

                        // remove temp files
                        unlink($path);
                        unlink($CFG->dataroot.'/temp/files/'.$USER->id.'/thumbnail/'.$attachfilename["name"]);
                    }
                }

                // update post
                $postRec = new stdClass();
                $postRec->id = $postId;
                $postRec->attachment = count($post["new_attachments"]);
                $DB->update_record('forum_posts', $postRec);
            }
        }

        $result = array();
        $result['posts'] = $postInfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    public static function update_forum_post_returns()
    {
        return new external_single_structure(array(
                'posts' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'Post Id')
                        )),'posts'),
                'warnings'  => new external_warnings()));
    }

    public static function delete_forum_post_parameters()
    {
      return new external_function_parameters(array(
            'postid' => new external_value(PARAM_INT, 'Post id'),
            'forumid' => new external_value(PARAM_INT, 'Forum id')
        ));

    }

    public static function delete_forum_post($postid, $forumid)
    {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::delete_forum_post_parameters(),
            array('postid' => $postid, 'forumid' => $forumid));
        $warnings = array();
        try {
          $post = $DB->get_record("forum_posts", array("id" => $postid),'*', MUST_EXIST);
          $forum = $DB->get_record('forum', array('id'=>$forumid),'*', MUST_EXIST);
          $cm = get_coursemodule_from_instance('forum', $forum->id);
          if(!$cm) {
            throw new Exception("Course module failure");
          }
          $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
          if ($modcontext && (($post->userid == $USER->id && has_capability('mod/forum:deleteownpost', $modcontext))
                || has_capability('mod/forum:deleteanypost', $modcontext)) ) {
            $course = $DB->get_record('course', array('id'=>$cm->course),'*', MUST_EXIST);
            require_once($CFG->dirroot . '/mod/forum/lib.php');
            forum_delete_post($post, true, $course, $cm, $forum);
          } else {
            $warning = array();
            $warning['item'] = 'course';
            $warning['itemid'] = $postid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Does not have capability to delete post!';
            $warnings[] = $warning;
          }
        } catch (Exception $ex) {
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $postid;
          $warning['warningcode'] = '1';
          $warning['message'] = 'Post / Forum not found : '.$ex->getMessage();
          $warnings[] = $warning;
        }
        $result = array();
        $result['warnings'] = $warnings;
        return $result;
    }

    public static function delete_forum_post_returns()
    {
        return new external_single_structure(array(
                'warnings'  => new external_warnings()));
    }


    public static function delete_forum_discussion_parameters()
    {
      return new external_function_parameters(array(
            'discussionid' => new external_value(PARAM_INT, 'Discussion id'),
            'forumid' => new external_value(PARAM_INT, 'Forum id')
        ));

    }

    public static function delete_forum_discussion($discussionid, $forumid)
    {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::delete_forum_discussion_parameters(),
            array('discussionid' => $discussionid, 'forumid' => $forumid));
        $warnings = array();
        try {
          $discussion = $DB->get_record("forum_discussions", array("id" => $discussionid),'*', MUST_EXIST);
          $forum = $DB->get_record('forum', array('id'=>$forumid),'*', MUST_EXIST);
          $cm = get_coursemodule_from_instance('forum', $forum->id);
          if(!$cm) {
            throw new Exception("Course module failure");
          }
           $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
          if ($modcontext && has_capability('mod/forum:deleteanypost', $modcontext)) {
            $course = $DB->get_record('course', array('id'=>$cm->course),'*', MUST_EXIST);
            require_once($CFG->dirroot . '/mod/forum/lib.php');
            forum_delete_discussion($discussion, true, $course, $cm, $forum);
          } else {
            $warning = array();
            $warning['item'] = 'course';
            $warning['itemid'] = $postid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Does not have capability to delete discussion!';
            $warnings[] = $warning;
          }
        } catch (Exception $ex) {
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $postid;
          $warning['warningcode'] = '1';
          $warning['message'] = 'Discussion / Forum not found : '.$ex->getMessage();
          $warnings[] = $warning;
          $warnings[] = $warning;
        }
        $result = array();
        $result['warnings'] = $warnings;
        return $result;
    }

    public static function delete_forum_discussion_returns()
    {
        return new external_single_structure(array(
                'warnings'  => new external_warnings()));
    }

    public static function create_filerscs_parameters() {
        return new external_function_parameters(
            array('resources' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'sectionid' => new external_value(PARAM_INT, 'section id'),
                        'course' => new external_value(PARAM_INT, 'course id'),
                        'name'  => new external_value(PARAM_TEXT, 'file resource name'),
                        'filename' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'file name of the file uploaded'),
                        'intro' => new external_value(PARAM_RAW, 'file resource description', VALUE_DEFAULT, '<p>Please check file attachment</p>'),
                        'introformat' => new external_value(PARAM_INT, 'resource description format ', VALUE_DEFAULT, 1),
                        'tobemigrated' => new external_value(PARAM_INT, 'resource to be migrated', VALUE_DEFAULT, 0),
                        'legacyfiles' => new external_value(PARAM_INT, 'legacy files', VALUE_DEFAULT, 0),
                        'legacyfileslast' => new external_value(PARAM_INT, 'legacy files last', VALUE_DEFAULT, 0),
                        'display' => new external_value(PARAM_INT, 'display', VALUE_DEFAULT, 0),
                        'printheading' => new external_value(PARAM_INT, 'display heading', VALUE_DEFAULT, 0),
                        'printintro' => new external_value(PARAM_INT, 'display intro', VALUE_DEFAULT, 0),
                        'showsize' => new external_value(PARAM_INT, 'display file size', VALUE_DEFAULT, 0),
                        'showtype' => new external_value(PARAM_INT, 'display file type', VALUE_DEFAULT, 0),
                        'filterfiles' => new external_value(PARAM_INT, 'files filter', VALUE_DEFAULT, 0),
                        'revision' => new external_value(PARAM_INT, 'revision', VALUE_DEFAULT, 0),
                        'contextualdata' => new external_value(PARAM_RAW,'Contextual data for the file resource', VALUE_DEFAULT, null),
                    )), 'list of file resource information')
            )
        );
    }

    /**
     * Create file resource for a course
     *
     * @param array of  $resources - file resource related info
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function create_filerscs($resources) {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::create_filerscs_parameters(),
            array('resources' => $resources));


        $warnings = array();
        $rscinfo = array();
        foreach ($params['resources'] as $resource) {
            try {
                //Check if user has permissions
                $context = context_course::instance($resource['course']);
                try {
                    self::validate_context($context);
                    require_capability('moodle/course:manageactivities', $context);
                } catch (Exception $e) {
                    $warning = array();
                    $warning['item'] = 'course';
                    $warning['itemid'] = $resource['course'];
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'No access rights in course context to add file resource';
                    $warnings[] = $warning;
                    continue;
                }
                //Get the course section info
                $section = $DB->get_record("course_sections", array("id"=>$resource['sectionid']));
                if(!$section) {
                    //User cannot create assignment for these courses
                    $warning = array();
                    $warning['item'] = 'course';
                    $warning['itemid'] = $resource['course'];
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'Unable to find course section to add resource';
                    $warnings[] = $warning;
                    continue;//continue with next module error getting section
                }

                $moduleid = $DB->get_field('modules', 'id', array('name'=>'resource'));
                // create a new course module for forum
                $newcm = new stdClass();
                $newcm->course   = $resource['course'];
                $newcm->module   = $moduleid;
                $newcm->instance = 0; // not known yet, will be updated later
                $newcm->visible  = 1;
                $newcm->section  = $section->id;
                $newcm->coursemodule  = 0;
                $newcm->added    = time();
                $newcmid = $DB->insert_record('course_modules', $newcm);

                $data = array(
                    'course' => $resource['course'],
                    'name' => $resource['name'],
                    'intro' => clean_param($resource['intro'], PARAM_RAW_TRIMMED),
                    'introformat' => $resource['introformat'],
                    'tobemigrated' => $resource['tobemigrated'],
                    'legacyfiles' => $resource['legacyfiles'],
                    'legacyfileslast' => $resource['legacyfileslast'],
                    'display' => $resource['display'],
                    'filterfiles' => $resource['filterfiles'],
                    'revision' => $resource['revision'],
                    'printheading' => $resource['printheading'],
                    'printintro' => clean_param($resource['printintro'], PARAM_RAW_TRIMMED),
                    'showsize' => $resource['showsize'],
                    'showtype'  => $resource['showtype'],
                );
                $data = (object) $data;

                require_once($CFG->dirroot . '/mod/resource/lib.php');
                require_once($CFG->dirroot . '/course/lib.php');
                require_once($CFG->libdir . '/resourcelib.php');
                resource_set_display_options($data);
                $resourceid = $DB->insert_record('resource', $data);
                $DB->set_field('course_modules', 'instance', $resourceid, array('id'=>$newcmid));

                //section info update
                $newcm->coursemodule  = $newcmid;
                $newcm->section = $section->section;
                add_mod_to_section($newcm);

                $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcmid));
                set_coursemodule_visible($newcm->coursemodule, 1);

                // now to deal with the file submission
                // get the course module id
                $context = context_module::instance($newcmid);

                $fs = get_file_storage();
                // Prepare file record object
                $fileinfo = array(
                    'contextid' => $context->id,
                    'component' => 'mod_resource',
                    'filearea' => 'content',
                    'itemid' => 0,
                    'filepath' => '/',
                    'filename' => $resource['filename'],
                    'name' => $resource['name'],
                );
                $fileurl = $CFG->dataroot."/temp/files/".$USER->id."/".$resource['filename'];
                try {
                    $file = $fs->create_file_from_pathname($fileinfo, $fileurl);
                    //monorail_trigger_document_conversion($file, $fileurl);
                    //If any contextual data present add it to the contextual_data
                    if(!empty($resource['contextualdata'])) {
                        $cdata = new stdClass();
                        $cdata->pathhash   = $file->get_pathnamehash();
                        $cdata->fileid   = $file->get_id();
                        $cdata->contextdata = clean_param($resource['contextualdata'], PARAM_RAW_TRIMMED);
                        $cdata->timemodified = time();
                        $DB->insert_record('monorail_file_context_data',$cdata);
                    }
                } catch (Exception $e) {
                    $warning = array();
                    $warning['item'] = 'course';
                    $warning['itemid'] = $resource['course'];
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'Could not create file from given tmp file';
                    $warnings[] = $warning;
                    //delete course module created and resource record
                    $DB->delete_records('course_modules', array('id'=>$newcmid));
                    resource_delete_instance($resourceid);
                    continue;
                }
                // clean temp files regardless
                unlink($fileurl);
                unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$resource['filename']);

                $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder', false);
                if (count($files) == 1) {
                    // only one file attached, set it as main file automatically
                    $file = reset($files);
                    file_set_sortorder($context->id, 'mod_resource', 'content', 0, $file->get_filepath(), $file->get_filename(), 1);
                }

                rebuild_course_cache($data->course);

                //send add events
                $eventdata = new stdClass();
                $eventdata->modulename = "resource";
                $eventdata->name       = 'mod_created';
                $eventdata->cmid       = $newcm->coursemodule;
                $eventdata->courseid   = $data->course;
                $eventdata->userid     = $USER->id;
                events_trigger("mod_created", $eventdata);
                $rscinf = array();
                $rscinf['resourceid'] = $newcmid; //$resourceid;
                $rscinf["fileurl"] = file_encode_url("$CFG->wwwroot/local/monorailservices/pluginfile.php?token=WSTOKEN&file=path=",$file->get_pathnamehash(),false);
                $rscinfo[] = $rscinf;
            } catch (Exception $ex) {
                add_to_log(1, 'monorail', 'externallib.create_filerscs', '', 'ERROR: '.$ex->getMessage());
                add_to_log(1, 'monorail', 'externallib.create_filerscs', '', 'TRACE: '.$ex->getTraceAsString());
            }
        }
        $result = array();
        $result['filerscs'] = $rscinfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for create resources
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function create_filerscs_returns() {
        return new external_single_structure(
            array(
                'filerscs' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'fileurl' => new external_value(PARAM_URL, 'File url'),
                            'resourceid' => new external_value(PARAM_INT, 'File resource Id')
                        )),'resource id'),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function del_filerscs_parameters() {
        return new external_function_parameters(
            array('filerscs' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'resourceid' => new external_value(PARAM_INT, 'resource id', VALUE_DEFAULT, 0),
                        'moduleid' => new external_value(PARAM_INT, 'moduleid id', VALUE_DEFAULT, 0),
                    )), 'list of file resource information')
            )
        );
    }

    /**
     * remove course resources
     * @param array of resource ids
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function del_filerscs($filerscs) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::del_filerscs_parameters(),
            array('filerscs' => $filerscs));

        $warnings = array();
        $result = array();

        foreach ($filerscs as $filersc) {
            $resourceid = $filersc['resourceid'];
            try {
              if($resourceid == 0) {
                $cm = $DB->get_record('course_modules', array('id' => $filersc['moduleid']), "*", MUST_EXIST);
                $resourceid = $cm->instance;
              }
              $resourceinfo = $DB->get_record('resource', array('id' => $resourceid), "*", MUST_EXIST);
            } catch (Exception $ex) {
                add_to_log(1, 'monorailservices', 'externallib.del_filercs', '', 'Error! Failed to deleted resource id '.$resourceid.': '.$ex->getMessage());
                continue; //Cannot find resource to delete
            }
            $context = context_course::instance($resourceinfo->course);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $resourceinfo->course;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to remove file resource';
                $warnings[] = $warning;
                continue;
            }
            require_once($CFG->dirroot . '/mod/resource/lib.php');
            //Ok remove the course module and update cache
            $cm = get_coursemodule_from_instance('resource', $resourceid, 0, false, MUST_EXIST);
            $DB->delete_records('course_modules', array('id'=>$cm->id));
            resource_delete_instance($resourceid);
            rebuild_course_cache($cm->course);

            // trigger event about deletion
            try {
                monorail_trigger_event('mod_deleted', 'resource', $cm->id, $cm->course, $USER->id);
            } catch (Exception $ex) {
                // don't die
                add_to_log(1, 'monorailservices', 'externallib.del_filercs', '', 'Error! Failed to trigger an event for deleted resource cmid '.$cm->id.': '.$ex->getMessage());
            }
        }

        $result['warnings'] = $warnings;
        return $result;

    }

    /**
     * Describes the return value for del_filerscs
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function del_filerscs_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }


    public static function update_filename_parameters() {
        return new external_function_parameters(
            array('fileitems' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'fileid'   => new external_value(PARAM_INT, 'fileid id', VALUE_DEFAULT, 0),
                        'moduleid'   => new external_value(PARAM_INT, 'module id', VALUE_DEFAULT, 0),
                        'filename' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'fileitem new name')
                    )), 'fileitems to update')
            )
        );
    }

    /**
     * remove course resources
     * @param array of resource ids
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function update_filename($fileitems) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::update_filename_parameters(),array('fileitems' => $fileitems));

        $warnings = array();
        $fs = get_file_storage();
        $fileobj = false;
        foreach ($fileitems as $fileitem) {
            $fileid = $fileitem['fileid'];
            if($fileid == 0) {
                //get fileid from moduleid , only works for file resource
                $cm = $DB->get_record('course_modules', array('id' => $fileitem['moduleid']));
                if(!$cm) {
                    continue;
                }
                $context = context_module::instance($cm->id);
                $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, "timemodified", false);
                foreach ($files as $file) {
                    if(!($file->get_filename() === '.')) {
                        $fileid = $file->get_id();
                        break; // get out of here , shld be only one file
                    }
                }
            } else {
                 $contextid = $DB->get_field('files', 'contextid', array('id' => $fileid));
                 $instanceid = $DB->get_field('context', 'instanceid', array('id' => $contextid));
                 //Thr should be a course module associated with this id
                 $cm = $DB->get_record('course_modules', array('id' => $instanceid));
            }
            if($cm) {
                // Update resource name.
                $res_rec = $DB->get_record('resource', array('id' => $cm->instance));
                if($res_rec) {
                    $res_rec->name = $fileitem['filename'];
                    $DB->update_record('resource', $res_rec);
                }
                monorail_data('assignfile', $fileid, 'name', $fileitem['filename']);
            }
        }
        return $warnings;
    }

    /**
     * Describes the return value for update_filename
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_filename_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }


    public static function add_assignfiles_parameters() {
        return new external_function_parameters(
            array('assignfiles' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'assignmentid' => new external_value(PARAM_INT, 'assignment id'),
                        'options' => new external_value(PARAM_TEXT, 'extra information about attachment', VALUE_OPTIONAL),
                        'filename' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'file name of the file uploaded'),
                        'contextualdata' => new external_value(PARAM_RAW,'Contextual data for the file resource', VALUE_DEFAULT, null),
                        'name' => new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'name of the file uploaded')
                    )), 'list of assignment file information')
            )
        );
    }


    /**
     * Create file resource for a assignment
     *
     * @param array of  $assignfiles - file resource related info
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function add_assignfiles($assignfiles) {
        global $USER, $DB, $CFG;
        require_once($CFG->dirroot.'/theme/monorail/lib.php');

        $params = self::validate_parameters(self::add_assignfiles_parameters(),
            array('assignfiles' => $assignfiles));

        $warnings = array();
        $assigninfo = array();

        foreach ($assignfiles as $assignfile)
        {
            $instanceInfo = $DB->get_record_sql("SELECT cm.instance AS id, m.name AS module".
                " FROM {course_modules} AS cm, {modules} AS m WHERE m.id=cm.module AND cm.id=?",
                array($assignfile["assignmentid"]), MUST_EXIST);

            //Check if user has permissions
            $assignment = $DB->get_record($instanceInfo->module, array('id' => $instanceInfo->id), "*", MUST_EXIST);
            $mid = $DB->get_field('modules', 'id', array('name'=>$instanceInfo->module));
            $cm = $DB->get_record('course_modules', array('course' => $assignment->course, 'instance' => $assignment->id, 'module'=>$mid), "*", MUST_EXIST);
            $context = context_module::instance($cm->id);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'assignment';
                $warning['itemid'] = $assignment->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to add file resource for assignment';
                $warnings[] = $warning;
                continue;
            }

            $fs = get_file_storage();
            // Prepare file record object
            $fileinfo = array(
                'contextid' => $context->id,
                'component' => 'mod_' . $instanceInfo->module,
                'filearea' => 'content',
                'itemid' => 0,
                'filepath' => '/',
                'filename' => $assignfile['filename'],
                'name' => $assignfile['name'],
            );
            $fileurl = $CFG->dataroot."/temp/files/".$USER->id."/".$assignfile['filename'];
            try {
                $file = $fs->create_file_from_pathname($fileinfo, $fileurl);
                //monorail_trigger_document_conversion($file, $fileurl);
                if(!empty($assignfile['contextualdata'])) {
                    $cdata = new stdClass();
                    $cdata->pathhash   = $file->get_pathnamehash();
                    $cdata->contextdata = clean_param($assignfile['contextualdata'], PARAM_RAW_TRIMMED);
                    $cdata->fileid   = $file->get_id();
                    $DB->insert_record('monorail_file_context_data',$cdata);
                }

                $asinf = array();
                $asinf['fileid'] = $file->get_id();
                $asinf['mimetype'] = $file->get_mimetype();
                $asinf["fileurl"] = file_encode_url("$CFG->wwwroot/local/monorailservices/pluginfile.php?token=WSTOKEN&file=path=",$file->get_pathnamehash(),false);
                // save name to monoraildata
                monorail_data('assignfile', $asinf['fileid'], 'name', $assignfile['name']);

                if (isset($assignfile["options"]))
                {
                    monorail_data('assignfile', $asinf['fileid'], 'options', $assignfile['options']);
                }

                $assigninfo[] = $asinf;
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'assignment';
                $warning['itemid'] =  $assignment->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'Could not create file from given tmp file';
                $warnings[] = $warning;
                continue;
            }
            // clean temp files regardless
            @unlink($fileurl);
            @unlink($CFG->dataroot."/temp/files/".$USER->id."/thumbnail/".$assignfile['filename']);
        }

        $result = array();
        $result['filerscs'] = $assigninfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for create resources
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function add_assignfiles_returns() {
        return new external_single_structure(
            array(
                'filerscs' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'fileid' => new external_value(PARAM_INT, 'File Id'),
                            'fileurl' => new external_value(PARAM_URL, 'new real url of the file'),
                            'mimetype' => new external_value(PARAM_TEXT, 'Mimetype', VALUE_OPTIONAL)
                        )),'file id'),
                'warnings'  => new external_warnings()
            )
        );
    }



    public static function del_assignfiles_parameters() {
        return new external_function_parameters(
            array('fileitems' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'assignmentid'   => new external_value(PARAM_INT, 'assignment id'),
                        'fileids' => new external_multiple_structure(
                            new external_value(PARAM_INT, 'fileitem id'))
                    )), 'fileitems to remove')
            )
        );
    }

    /**
     * remove file attachments to assignments
     * @param array of fileitem ids
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function del_assignfiles($fileitems) {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot.'/theme/monorail/lib.php');

        $params = self::validate_parameters(self::del_assignfiles_parameters(),
            array('fileitems' => $fileitems));

        $warnings = array();
        $result = array();

        foreach ($fileitems as $fileitem) {
            $instanceInfo = $DB->get_record_sql("SELECT cm.instance AS id, m.name AS module".
                " FROM {course_modules} AS cm, {modules} AS m WHERE m.id=cm.module AND cm.id=?",
                array($fileitem["assignmentid"]), MUST_EXIST);

            $assignment = $DB->get_record($instanceInfo->module, array('id' => $instanceInfo->id), "*", MUST_EXIST);
            //get the context id / file info
            $mid = $DB->get_field('modules', 'id', array('name'=>$instanceInfo->module));
            $cm = $DB->get_record('course_modules', array('course' => $assignment->course, 'instance' => $assignment->id, 'module'=>$mid), "*", MUST_EXIST);
            $context = context_module::instance($cm->id);

            $fileids = $fileitem['fileids'];
            $fileidsdeleted = array();
            $fs = get_file_storage();
            $count = 0;
            $files = $fs->get_area_files($context->id, 'mod_' . $instanceInfo->module, 'content', 0, "timemodified", false);
            foreach ($files as $file) {
                if(in_array($file->get_id(), $fileids)){
                    $finstance = $fs->get_file_by_id($file->get_id());
                    $finstance->delete();
                    // delete from monorail_data too
                    monorail_data('assignfile', $file->get_id(), 'name', null, false, true);
                    array_push($fileidsdeleted,$file->get_id());
                    $count = $count+1;
                }
            }

            $adiff = array_diff($fileids, $fileidsdeleted);

            if (count($adiff) > 0){
                $warning = array();
                $warning['item'] = 'deleteassignmentfile';
                $warning['itemid'] = $fileitem['assignmentid'];
                $warning['warningcode'] = implode(",", $adiff);
                $warning['message'] = 'Could not delete some file attachments';
                $warnings[] = $warning;
            }

            if ($count > 0) {
                // trigger updated event
                try {
                    monorail_trigger_event('mod_updated', 'assign', $cm->id, $assignment->course, $USER->id);
                } catch (Exception $ex) {
                    // don't die
                }
            }
        }

        $result['warnings'] = $warnings;
        return $result;

    }

    /**
     * Describes the return value for del attachments to assignments
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function del_assignfiles_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function get_list_of_background_picture_parameters() {
    	return new external_function_parameters(
    			array(
    					'courseid' => new external_multiple_structure(
    							new external_value(PARAM_INT, 'course id'),
    							'0 or more course ids',
    							VALUE_DEFAULT, array())
    			)
    	);
    }

    public static function get_list_of_background_picture($courseid) {
    	global $CFG;

    	$result = array();

    	$directory = $CFG->dirroot . '/theme/monorail/pix/coursebackground/';

    	//get all image files with a .jpg extension.
    	$images = glob($directory . "*.{jpg,gif,png}", GLOB_BRACE);

    	for($i=0; $i < count($images); $i++) {
    		$images[$i] = basename($images[$i]);
    		$images[$i] = preg_replace("/\\.[^.\\s]{3,4}$/", "", $images[$i]);
    	}

    	$result['images'] = $images;

    	return $result;
    }

    public static function get_list_of_background_picture_returns() {
    	return new external_single_structure(
    			array(
    					'images'   => new external_multiple_structure(
                                new external_value(PARAM_TEXT, 'image'))
    			)
    	);
    }


    public static function add_course_rurls_parameters() {
        return new external_function_parameters(
            array('urls' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'sectionid' => new external_value(PARAM_INT, 'section id'),
                        'course' => new external_value(PARAM_INT, 'course id'),
                        'name'  => new external_value(PARAM_TEXT, 'url name'),
                        'intro' => new external_value(PARAM_RAW, 'description', VALUE_DEFAULT, '<p>A general</p>'),
                        'introformat' => new external_value(PARAM_INT, 'url description format ', VALUE_DEFAULT, 1),
                        'externalurl' => new external_value(PARAM_URL, 'external url', VALUE_DEFAULT, '<p>Check url</p>'),
                        'display' => new external_value(PARAM_INT, 'display', VALUE_DEFAULT, 0),
                        'popupwidth' => new external_value(PARAM_INT, 'Pop up width', VALUE_OPTIONAL),
                        'popupheight' => new external_value(PARAM_INT, 'Pop up height', VALUE_OPTIONAL),
                        'printheading' => new external_value(PARAM_INT, 'Print heading', VALUE_OPTIONAL),
                        'printintro' =>  new external_value(PARAM_INT, 'Print intro', VALUE_OPTIONAL),
                        'parameters' => new external_multiple_structure (
                            new external_single_structure(
                                array(
                                    'parameter' => new external_value(PARAM_INT, 'assign_plugin_config id'),
                                    'value' => new external_value(PARAM_INT, 'assignment id'))
                            ),'list of extra parameters', VALUE_DEFAULT, array())

                    )), 'list of url information')
            )
        );
    }

    /**
     * Create url for a course
     *
     * @param array of  $urls - url related info
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function add_course_rurls($urls) {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::add_course_rurls_parameters(),
            array('urls' => $urls));
        $warnings = array();
        $urlinfo = array();
        foreach ($params['urls'] as $url) {
            //Check if user has permissions
            $context = context_course::instance($url['course']);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $url['course'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to add url';
                $warnings[] = $warning;
                continue;
            }
            //Get the course section info
            $section = $DB->get_record("course_sections", array("id"=>$url['sectionid']));
            if(!$section) {
                //User cannot create assignment for these courses
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $url['course'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Unable to find course section to add url';
                $warnings[] = $warning;
                continue;//continue with next module error getting section
            }

            $moduleid = $DB->get_field('modules', 'id', array('name'=>'url'));
            // create a new course module for url
            $newcm = new stdClass();
            $newcm->course   = $url['course'];
            $newcm->module   = $moduleid;
            $newcm->instance = 0;
            $newcm->visible  = 1;
            $newcm->section  = $section->id;
            $newcm->coursemodule  = 0;
            $newcm->added    = time();
            $newcmid = $DB->insert_record('course_modules', $newcm);

            $data = array(
                'course'=> $url['course'],
                'name'=> $url['name'],
                'intro'=> clean_param($url['intro'], PARAM_RAW_TRIMMED),
                'introformat'=> $url['introformat'],
                'externalurl'=> $url['externalurl'],
                'display' => $url['display'],
                'coursemodule' => $newcmid,
                'cmidnumber' => $newcmid,
            );
            if($url['display'] > 0) {
                $data['popupwidth'] = $url['popupwidth'];
                $data['popupheight'] = $url['popupheight'];
                $data['printheading'] = $url['printheading'];
                $data['printintro'] = $url['printintro'];
            }
            $count = 0;
            foreach($url['parameters'] as $param) {
                $key = 'parameter_'.$count;
                $value = 'variable_'.$count;
                $data[$key] = $param['parameter'];
                $data[$value] = $param['value'];
                $count++;
            }
            $data = (object) $data;

            require_once($CFG->dirroot . '/mod/url/lib.php');
            require_once($CFG->dirroot . '/course/lib.php');
            $urlid = url_add_instance($data, null);
            $newcm->coursemodule = $urlid;

            //Update the instance field in course_modules
            $DB->set_field('course_modules', 'instance', $newcm->coursemodule, array('id'=>$newcmid));

            //section info update
            $newcm->coursemodule  = $newcmid;
            $newcm->section = $section->section;
            add_mod_to_section($newcm);

            $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcm->coursemodule));
            set_coursemodule_visible($newcm->coursemodule, 1);
            rebuild_course_cache($data->course);

            //send add events
            $eventdata = new stdClass();
            $eventdata->modulename = "url";
            $eventdata->name       = 'mod_created';
            $eventdata->cmid       = $newcm->coursemodule;
            $eventdata->courseid   = $data->course;
            $eventdata->userid     = $USER->id;
            events_trigger("mod_created", $eventdata);
            $urlinf = array();
            $urlinf['urlid'] = $urlid;
            $urlinfo[] = $urlinf;

        }
        $result = array();
        $result['url'] = $urlinfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for add_course_rurls
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function add_course_rurls_returns() {
        return new external_single_structure(
            array(
                'url' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'urlid' => new external_value(PARAM_INT, 'url Id')
                        )),'url id'),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function upd_course_rurls_parameters() {
        return new external_function_parameters(
            array('urls' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'id', VALUE_DEFAULT, 0),
                        'moduleid' => new external_value(PARAM_INT, 'id', VALUE_DEFAULT, 0),
                        'course' => new external_value(PARAM_INT, 'course id', VALUE_OPTIONAL),
                        'name'  => new external_value(PARAM_TEXT, ' name', VALUE_OPTIONAL),
                        'intro' => new external_value(PARAM_RAW, ' description', VALUE_OPTIONAL),
                        'introformat' => new external_value(PARAM_INT, 'url description format ', VALUE_OPTIONAL),
                        'externalurl' => new external_value(PARAM_URL, 'external url', VALUE_OPTIONAL),
                        'display' => new external_value(PARAM_INT, 'display', VALUE_OPTIONAL),
                        'popupwidth' => new external_value(PARAM_INT, 'Pop up width', VALUE_OPTIONAL),
                        'popupheight' => new external_value(PARAM_INT, 'Pop up height', VALUE_OPTIONAL),
                        'printheading' => new external_value(PARAM_INT, 'Print heading', VALUE_OPTIONAL),
                        'printintro' =>  new external_value(PARAM_INT, 'Print intro', VALUE_OPTIONAL),
                        'params' => new external_multiple_structure (
                            new external_single_structure(
                                array(
                                    'parameter' => new external_value(PARAM_INT, 'assign_plugin_config id'),
                                    'value' => new external_value(PARAM_INT, 'assignment id'))
                            ),'list of extra parameters', VALUE_OPTIONAL)

                    )), 'list of url information')
            )
        );
    }

    /**
     * Update url for a course
     *
     * @param array of  $urls - url related info
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function upd_course_rurls($urls) {
        global $USER, $DB, $CFG;
        $params = self::validate_parameters(self::upd_course_rurls_parameters(),
            array('urls' => $urls));

        $warnings = array();
        $urlinfo = array();
        foreach ($urls as $url) {
            //Ok either we have the url id or module id
            $urlid = $url['id'];
            if($urlid == 0) {
                //get url id from module
                $cm = $DB->get_record('course_modules', array('id' => $url['moduleid']), "*", MUST_EXIST);
                $urlid = $cm->instance;
            }
            $oldurl = $DB->get_record('url', array('id'=>$urlid), "*", MUST_EXIST);
            $keys=array_keys((array)$oldurl);
            $keyid = array_search('id', $keys);
            if (false !== $keyid) {
                unset($keys[$keyid]);
            }
            foreach($keys as $keytoupdate){
                if(array_key_exists($keytoupdate, $url))
                    $oldurl->$keytoupdate = $url[$keytoupdate];
            }
            $mid = $DB->get_field('modules', 'id', array('name'=>'url'));
            $cm = $DB->get_record('course_modules', array('course' => $oldurl->course, 'instance' => $oldurl->id, 'module'=>$mid), "*", MUST_EXIST);
            $data = array();
            $data = (array)$oldurl;
            $data['instance'] = $oldurl->id;
            $data['cmidnumber'] = $cm->id; // Removes warning
            $oldurl = (object) $data;
            //TODO: display params
            require_once($CFG->dirroot . '/mod/url/lib.php');

            $retval = url_update_instance($oldurl, null);
            if(!$retval) {
                //User cannot update
                $warning = array();
                $warning['item'] = 'url';
                $warning['itemid'] = $oldurl->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'url cannot be  updated';
                $warnings[] = $warning;
                $rassignments[] = $rassign;
                continue;
            }
            $urlinf = array();
            $urlinf['urlid'] = $oldurl->id;
            $urlinfo[] = $urlinf;
            rebuild_course_cache($oldurl->course);

            //send update events
            $eventdata = new stdClass();
            $eventdata->modulename = "url";
            $eventdata->name       = 'mod_updated';
            $eventdata->cmid       = $cm->id;
            $eventdata->courseid   = $oldurl->course;
            $eventdata->userid     = $USER->id;
            events_trigger("mod_updated", $eventdata);
        }
        $result = array();
        $result['url'] = $urlinfo;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for update_course_rurls
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function upd_course_rurls_returns() {
        return new external_single_structure(
            array(
                'url' => new external_multiple_structure (
                    new external_single_structure(
                        array(
                            'urlid' => new external_value(PARAM_INT, 'url Id that has been updated')
                        )),'url id'),
                'warnings'  => new external_warnings()
            )
        );
    }



    /**
     * Describes the params for del_course_rurls
     * @return external_single_structure
     * @since  Moodle 2.4
     */

    public static function del_course_rurls_parameters() {
        return new external_function_parameters(
            array('urls' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'urlid' => new external_value(PARAM_INT, 'url id', VALUE_DEFAULT, 0),
                        'moduleid' => new external_value(PARAM_INT, 'moduleid id', VALUE_DEFAULT, 0),
                    )), 'list of file resource information')
            )
        );
    }

    /**
     * remove url
     * @param array of $urls info
     *
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function del_course_rurls($urls) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::del_course_rurls_parameters(),
            array('urls' => $urls));

        $warnings = array();
        $result = array();

        foreach ($urls as $url) {
            $urlid = $url['urlid'];
            if($urlid == 0) {
                $cm = $DB->get_record('course_modules', array('id' => $url['moduleid']), "*", MUST_EXIST);
                $urlid = $cm->instance;
            }
            $urlinfo = $DB->get_record('url', array('id' => $urlid), "*", MUST_EXIST);

            $context = context_course::instance($urlinfo->course);
            try {
                self::validate_context($context);
                require_capability('moodle/course:manageactivities', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $urlinfo->course;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to remove url';
                $warnings[] = $warning;
                continue;
            }
            require_once($CFG->dirroot . '/mod/url/lib.php');
            //Ok remove the course module and update cache
            $cm = get_coursemodule_from_instance('url', $urlid, 0, false, MUST_EXIST);
            $DB->delete_records('course_modules', array('id'=>$cm->id));
            url_delete_instance($urlid);
            rebuild_course_cache($cm->course);

            //send delete events
            $eventdata = new stdClass();
            $eventdata->modulename = "url";
            $eventdata->name       = 'mod_deleted';
            $eventdata->cmid       = $cm->id;
            $eventdata->courseid   = $cm->course;
            $eventdata->userid     = $USER->id;
            events_trigger("mod_deleted", $eventdata);
        }

        $result['warnings'] = $warnings;
        return $result;

    }

    /**
     * Describes the return value for del_course_rurls
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function del_course_rurls_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function get_course_contents_parameters() {
        return new external_function_parameters(
                array('courseid' => new external_value(PARAM_INT, 'course id'),
                      'cohortid' => new external_value(PARAM_INT, 'cohort id', VALUE_DEFAULT, 0),
                      'options' => new external_multiple_structure (
                              new external_single_structure(
                                    array('name' => new external_value(PARAM_ALPHANUM, 'option name'),
                                          'value' => new external_value(PARAM_TEXT, 'the value of the option, this param is personaly validated in the external function.')
                              )
                      ), 'Options, not used yet, might be used in later version', VALUE_DEFAULT, array())
                )
        );
    }

    /**
     * Get course contents
     *
     * @param int $courseid course id
     * @param array $options These options are not used yet, might be used in later version
     * @return array
     * @since Moodle 2.2
     */
    public static function get_course_contents($courseid, $cohortid,$options = array()) {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . "/course/lib.php");

        //validate parameter
        $params = self::validate_parameters(self::get_course_contents_parameters(),
                        array('courseid' => $courseid, 'options' => $options));

        //retrieve the course
        $course = $DB->get_record('course', array('id' => $params['courseid']), '*', MUST_EXIST);

        //check course format exist
        if (!file_exists($CFG->dirroot . '/course/format/' . $course->format . '/lib.php')) {
            throw new moodle_exception('cannotgetcoursecontents', 'webservice', '', null, get_string('courseformatnotfound', 'error', '', $course->format));
        } else {
            require_once($CFG->dirroot . '/course/format/' . $course->format . '/lib.php');
        }

        //One of the use cases here is that the user belongs to cohort and user is interested
        //to check details of a cohort catalog course
         $cohortcourse = false;

        // now security checks
        if (!self::can_view_course($course->id, $USER->id)) {
            if($cohortid > 0) {
              $iscohortmember = $DB->get_record('cohort_members', array('userid'=>$USER->id, 'cohortid' => $cohortid));
              //Check if course belongs to cohort catalog and is published in business catalog
              if($iscohortmember) {
                $cadmins = $DB->get_records('monorail_cohort_courseadmins', array('cohortid' => $cohortid));
                foreach($cadmins as $cadmin){
                  if($cadmin->courseid == $courseid) {
                    $cohortcourse = true;
                    break;
                  }
                }
              }
            }
            if(!$cohortcourse) {
              $exceptionparam = new stdClass();
              $exceptionparam->message = "Access denied";
              $exceptionparam->courseid = $course->id;
              throw new moodle_exception('errorcoursecontextnotvalid', 'webservice', '', $exceptionparam);
            }
        }

        $context = get_context_instance(CONTEXT_COURSE, $course->id);
        $canupdatecourse = has_capability('moodle/course:update', $context);

        //create return value
        $coursecontents = array();

        if ($canupdatecourse or $course->visible
                or has_capability('moodle/course:viewhiddencourses', $context)) {

            //retrieve sections
            $modinfo = get_fast_modinfo($course);
            $sections = get_all_sections($course->id);

            //for each sections (first displayed to last displayed)
            foreach ($sections as $key => $section) {
                // XXX: Apparently we're not using hiddensections feature -
                // all courses have it set to 0, so the visible property has no effect here
                // (hiding invisible sections on client side is not safe).
                $showsection = (has_capability('moodle/course:viewhiddensections', $context) or $section->visible /*or !$course->hiddensections*/);
                if (!$showsection) {
                    continue;
                }

                // reset $sectioncontents
                $sectionvalues = array();
                $sectionvalues['id'] = $section->id;
                $sectionvalues['name'] = get_section_name($course, $section);
                $sectionvalues['visible'] = $section->visible;
                $sectionvalues['section'] = $section->section;
                list($sectionvalues['summary'], $sectionvalues['summaryformat']) =
                        external_format_text($section->summary, $section->summaryformat,
                                $context->id, 'course', 'section', $section->id);
                $sectioncontents = array();

                //for each module of the section
                if (isset($modinfo->sections[$section->section])) {
                    foreach ($modinfo->sections[$section->section] as $cmid) { //matching /course/lib.php:print_section() logic
                        $cm = $modinfo->cms[$cmid];
                        // stop here if the module is not visible to the user
                        if (!$cm->uservisible) {
                            continue;
                        }
                        if(strcmp($cm->modname,"bigbluebuttonbn") == 0) {
                         continue;
                        }
                        $module = array();

                        //common info (for people being able to see the module or availability dates)
                        $module['id'] = $cm->id;
                        $module['name'] = format_string($cm->name, true);
                        $module['modname'] = $cm->modname;
                        $module['modplural'] = $cm->modplural;
                        $module['modicon'] = $cm->get_icon_url()->out(false);
                        $module['indent'] = $cm->indent;

                        $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);

                        if (!empty($cm->showdescription)) {
                            $module['description'] = $cm->get_content();
                        }

                        $duedate = null;
                        $startdate = null;

                        if ($cm->modname == "quiz") {
                            $rec = $DB->get_record_sql("SELECT timeclose, timeopen FROM {quiz} WHERE id=? LIMIT 1", array($cm->instance));
                            $duedate = (int) $rec->timeclose;
                            $startdate = (int) $rec->timeopen;
                        } else if ($cm->modname == "assign") {
                            $rec = $DB->get_record_sql("SELECT duedate, allowsubmissionsfromdate FROM {assign} WHERE id=? LIMIT 1", array($cm->instance));
                            $duedate = (int) $rec->duedate;
                            $startdate = (int) $rec->allowsubmissionsfromdate;
                        }

                        if (!$canupdatecourse && $startdate && $startdate > time()) {
                            // This task/quiz should not be visible to
                            // students yet.
                            continue;
                        }

                        if ($duedate > 0) {
                            $module["duedate"] = date(DATE_ISO8601, $duedate);
                        }

                        //url of the module
                        $url = $cm->get_url();
                        if ($url) { //labels don't have url
                            $module['url'] = $cm->get_url()->out();
                            if($module['modname'] == 'lti') {
                                //Get the tool url
                                require_once($CFG->dirroot . '/mod/lti/locallib.php');
                                $lti = $DB->get_record('lti', array('id' => $cm->instance), '*', MUST_EXIST);
                                $ltitype = $DB->get_record('lti_types', array('id' => $lti->typeid));
                                $lti->cmid = $module['id'];
                                if($ltitype && $ltitype->createdby != 2) {
                                    $murl = new moodle_url('/mod/lti/launch.php', array('id'=>$module['id']));
                                    $module['url'] = $murl->out();
                                } else if ($ltitype) {
                                    $exurl = lti_view_embed($lti, $course);
                                    $link = str_replace(" ", "%20", $exurl['Location']);
                                    $module['url'] = $link;
                                } else {     //Ignore this module
                                     continue;
                                }
                            }
                        }

                        $canviewhidden = has_capability('moodle/course:viewhiddenactivities',
                                            get_context_instance(CONTEXT_MODULE, $cm->id));
                        //user that can view hidden module should know about the visibility
                        $module['visible'] = $cm->visible;

                        //availability date (also send to user who can see hidden module when the showavailabilyt is ON)
                        if ($canupdatecourse or ($CFG->enableavailability && $canviewhidden && $cm->showavailability)) {
                            $module['availablefrom'] = $cm->availablefrom;
                            $module['availableuntil'] = $cm->availableuntil;
                        }

                        $baseurl = 'webservice/pluginfile.php';

                        if ($cm->modname == "kalvidres") {
                            $module["entry_id"] = $DB->get_field("kalvidres", "entry_id", array("id" => $cm->instance));
                        }

                        //call $modulename_export_contents
                        //(each module callback take care about checking the capabilities)
                        require_once($CFG->dirroot . '/mod/' . $cm->modname . '/lib.php');
                        $getcontentfunction = $cm->modname.'_export_contents';
                        if (function_exists($getcontentfunction)) {
                            if ($contents = $getcontentfunction($cm, $baseurl)) {
                                // CBTEC
                                require_once($CFG->dirroot . '/theme/monorail/lib.php');
                                if ($cm->modname == 'resource') {
                                    $resource = $DB->get_record('resource', array('id'=>$cm->instance), '*', MUST_EXIST);
                                    $fs = get_file_storage();
                                    $files = $fs->get_area_files($modcontext->id, 'mod_resource', 'content');
                                    foreach ($files as $file) {
                                        $record = monorail_file_embed_record($file, null, null);
                                        if ($record) {
                                            $record['fileid'] = $file->get_id();
                                            $contextualdata = self::monorailservices_get_file_contextdata($file->get_pathnamehash(), $file->get_id());
                                            if($contextualdata)
                                                $record['contextualdata'] = $contextualdata;
                                            break;
                                        }
                                    }
                                    foreach ($contents as $content) {
                                        if ($content['filename'] != '.') {
                                            $reset = false;
                                            if (isset($record['contextualdata'])) {
                                                $content['contextualdata'] = $record['contextualdata'];
                                                $reset = true;
                                            }
                                            if ($record['fileid']) {
                                                $content['fileid'] = $record['fileid'];
                                                $reset = true;
                                            }
                                            /*
                                            if ($record['converted']) {
                                                $content['convertedpath'] = $record['convertedpath'];
                                                $reset = true;
                                            }
                                            */
                                            if ($record['type'] == 'image') {
                                                $content['type'] = $record['type'];
                                            }
                                            if ($record['mimetype']) {
                                                $content['mimetype'] = $record['mimetype'];
                                            }
                                            $content['name'] = format_string($cm->name, true);

                                            if ($reset) {
                                                unset($contents);
                                                $contents[] = $content;
                                                break;
                                            }
                                        }
                                    }
                                } else if ($cm->modname == 'url') {
                                    foreach ($contents as $content) {
                                        $content['filename'] = '';
                                        unset($contents);
                                        $contents[] = $content;
                                        break;
                                    }
                                }
                                // CBTEC end
                                $module['contents'] = $contents;
                            }
                        }

                        //assign result to $sectioncontents
                        $sectioncontents[] = $module;

                    }
                }
                $sectionvalues['modules'] = $sectioncontents;

                // assign result to $coursecontents
                $coursecontents[] = $sectionvalues;
                if($cohortcourse) {
                    //we need only one section
                    break;
                }
            }
        }

        wscache_add_cache_dependency("course_content_" . $courseid);

        return $coursecontents;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function get_course_contents_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'Section ID'),
                    'name' => new external_value(PARAM_TEXT, 'Section name'),
                    'visible' => new external_value(PARAM_INT, 'is the section visible', VALUE_OPTIONAL),
                    'section' => new external_value(PARAM_INT, 'section index', VALUE_OPTIONAL),
                    'summary' => new external_value(PARAM_RAW, 'Section description'),
                    'summaryformat' => new external_format_value('summary'),
                    'modules' => new external_multiple_structure(
                            new external_single_structure(
                                array(
                                    'id' => new external_value(PARAM_INT, 'activity id'),
                                    'url' => new external_value(PARAM_URL, 'activity url', VALUE_OPTIONAL),
                                    'name' => new external_value(PARAM_TEXT, 'activity module name'),
                                    'description' => new external_value(PARAM_RAW, 'activity description', VALUE_OPTIONAL),
                                    'visible' => new external_value(PARAM_INT, 'is the module visible', VALUE_OPTIONAL),
                                    'modicon' => new external_value(PARAM_URL, 'activity icon url'),
                                    'modname' => new external_value(PARAM_PLUGIN, 'activity module type'),
                                    'modplural' => new external_value(PARAM_TEXT, 'activity module plural name'),
                                    'entry_id' => new external_value(PARAM_TEXT, 'entry id (for video resources)', VALUE_OPTIONAL),
                                    'availablefrom' => new external_value(PARAM_INT, 'module availability start date', VALUE_OPTIONAL),
                                    'availableuntil' => new external_value(PARAM_INT, 'module availability en date', VALUE_OPTIONAL),
                                    'duedate' => new external_value(PARAM_TEXT, 'assignment due date', VALUE_OPTIONAL),
                                    'indent' => new external_value(PARAM_INT, 'number of identation in the site'),
                                    'contents' => new external_multiple_structure(
                                          new external_single_structure(
                                              array(
                                                  // content info
                                                  'type'=> new external_value(PARAM_TEXT, 'a file or a folder or external link'),
                                                  'filename'=> new external_value(/*PARAM_FILE*/PARAM_NOTAGS, 'filename'),
                                                  'name' => new external_value(PARAM_TEXT, 'name', VALUE_OPTIONAL),
                                                  'filepath'=> new external_value(PARAM_PATH, 'filepath'),
                                                  'filesize'=> new external_value(PARAM_INT, 'filesize'),
                                                  'fileurl' => new external_value(PARAM_URL, 'downloadable file url', VALUE_OPTIONAL),
                                                  //CBTEC
                                                  'fileid' => new external_value(PARAM_INT, 'file id', VALUE_OPTIONAL),
                                                  //'convertedpath' => new external_value(PARAM_TEXT, 'converted pdf url', VALUE_OPTIONAL),
                                                  'contextualdata' => new external_value(PARAM_RAW, 'contextual data for the resource', VALUE_OPTIONAL),
                                                  //CBTEC end
                                                  'content' => new external_value(PARAM_RAW, 'Raw content, will be used when type is content', VALUE_OPTIONAL),
                                                  'timecreated' => new external_value(PARAM_INT, 'Time created'),
                                                  'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                                                  'sortorder' => new external_value(PARAM_INT, 'Content sort order'),

                                                  // copyright related info
                                                  'userid' => new external_value(PARAM_INT, 'User who added this content to moodle'),
                                                  'author' => new external_value(PARAM_TEXT, 'Content owner'),
                                                  'license' => new external_value(PARAM_TEXT, 'Content license'),
                                                  'mimetype' => new external_value(PARAM_TEXT, 'Mimetype', VALUE_OPTIONAL)
                                              )
                                          ), VALUE_DEFAULT, array()
                                      )
                                )
                            ), 'list of module'
                    )
                )
            )
        );

    }

    public static function update_ctxdata_parameters() {
        return new external_function_parameters(
            array('fileitems' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'fileid'   => new external_value(PARAM_INT, 'fileid id', VALUE_DEFAULT, 0),
                        'moduleid'   => new external_value(PARAM_INT, 'module id', VALUE_DEFAULT, 0),
                        'modname'   => new external_value(PARAM_TEXT, 'module name', VALUE_DEFAULT, 'resource'),
                        'contextualdata' => new external_value(PARAM_RAW, 'Contextualdata')
                    )), 'fileitems to update')
            )
        );
    }

    /**
     * Update contextual data for file rsc
     * @param array of fileitems
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function update_ctxdata($fileitems) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::update_ctxdata_parameters(),array('fileitems' => $fileitems));

        $warnings = array();
        $fs = get_file_storage();
        $fileobj = false;
        $cm = false;
        foreach ($fileitems as $fileitem) {
            $fileid = $fileitem['fileid'];
            if($fileid == 0) {
                //get fileid from moduleid , only works for file resource
                $cm = $DB->get_record('course_modules', array('id' => $fileitem['moduleid']), "*", MUST_EXIST);
                $context = context_module::instance($cm->id);
                $modname = 'mod_'.$fileitem['modname'];
                $files = $fs->get_area_files($context->id, $modname, 'content', 0, "timemodified", false);
                foreach ($files as $file) {
                    if(!($file->get_filename() === '.')) {
                        $fileid = $file->get_id();
                        $fileobj = $file;
                        break; // get out of here , shld be only one file
                    }
                }
            }
            if(!$cm) {
                $contextid = $DB->get_field('files', 'contextid', array('id' => $fileid));
                $instanceid = $DB->get_field('context', 'instanceid', array('id' => $contextid));
                //Thr should be a course module associated with this id
                $cm = $DB->get_record('course_modules', array('id' => $instanceid), "*", MUST_EXIST);
            }

            if($DB->record_exists('monorail_file_context_data', array('fileid'=>$fileid))){
                $result = $DB->set_field('monorail_file_context_data', 'contextdata',clean_param($fileitem['contextualdata'], PARAM_RAW_TRIMMED), array('fileid'=>$fileid));
            } else {
                $cdata = new stdClass();
                if(!$fileobj) {
                    $fileobj = $fs->get_file_by_id($fileid);
                }
                $cdata->pathhash   = $fileobj->get_pathnamehash();
                $cdata->contextdata = clean_param($fileitem['contextualdata'], PARAM_RAW_TRIMMED);
                $cdata->fileid   = $fileobj->get_id();
                $cdata->timemodified = time();
                $DB->insert_record('monorail_file_context_data',$cdata);
            }
            //not created its just the file src has now some contextual data
            monorail_trigger_event('mod_updated', $fileitem['modname'], $cm->id, $cm->course, $USER->id);
        }
        return $warnings;
    }

    /**
     * Describes the return value for update_ctxdata
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_ctxdata_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }
    //=============update grades====
    public static function update_grades_parameters() {
        return new external_function_parameters(
            array('grades' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'assignid'   => new external_value(PARAM_INT, 'assignment id'),
                        'userid' => new external_value(PARAM_INT, 'user id'),
                        'modname'   => new external_value(PARAM_TEXT, 'module name', VALUE_DEFAULT, 'assign'),
                        'grade' => new external_value(PARAM_RAW, 'Grade for the assignment', VALUE_OPTIONAL),
                        'feedback' => new external_value(PARAM_RAW, 'feedback', VALUE_OPTIONAL),
                        'feedbackformat' => new external_format_value('Format code is 0 for plain text, 1 for HTML etc', VALUE_DEFAULT, 1),
                        'revert_to_draft' => new external_value(PARAM_INT, 'Revert submission to draft', VALUE_DEFAULT, 0),
                    )), 'grades to update/create')
            )
        );
    }

    /**
     * Update grades
     * @param array of fileitems
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function update_grades($grades) {
        global $CFG, $DB, $USER, $CFG;

        $params = self::validate_parameters(self::update_grades_parameters(),array('grades' => $grades));
        $warnings = array();
        require_once($CFG->libdir.'/gradelib.php');
        foreach ($grades as $grade) {
            if (! array_key_exists('grade', $grade) && ! array_key_exists('feedback', $grade)) {
                continue;
            }
            try {
                $assign = $DB->get_record($grade['modname'], array('id' => $grade['assignid']), "*", MUST_EXIST);
                $mid = $DB->get_field('modules', 'id', array('name'=>$grade['modname']));
                $assign->cmidnumber= $DB->get_field('course_modules', 'id',array('course'=>$assign->course, 'instance'=>$assign->id, 'module'=>$mid), MUST_EXIST);
            } catch (Exception $ex) {
                $warning = array();
                $warning['item'] = $grade['modname'];
                $warning['itemid'] = $grade['assignid'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Error updating grade: could not find assignment information';
                $warnings[] = $warning;
                continue;
            }

            if (!isset($assign->courseid)) {
                $assign->courseid = $assign->course;
            }

            $params = array('itemname'=>$assign->name, 'idnumber'=>$assign->cmidnumber);

            if ($assign->grade > 0) {
                $params['gradetype'] = GRADE_TYPE_VALUE;
                $params['grademax']  = $assign->grade;
                $params['grademin']  = 0;

            } else if ($assign->grade < 0) {
                $params['gradetype'] = GRADE_TYPE_SCALE;
                $params['scaleid']   = $assign->grade;

            } else {
                $params['gradetype'] = GRADE_TYPE_TEXT; // allow text comments only
            }

            $newgrade = new stdClass();
            $newgrade->dategraded = time();
            $newgrade->usermodified = $USER->id;
            $newgrade->userid = $grade['userid'];

            if (array_key_exists('grade', $grade)) {
                if ($grade['grade'] === "") {
                    $newgrade->rawgrade = null; // clear
                } else {
                    // strip decimals and make sure it's number
                    $newgrade->rawgrade = floatval(intval($grade['grade']));
                }
            }
            if (array_key_exists('feedback', $grade)) {
                $newgrade->feedback = clean_param($grade['feedback'], PARAM_RAW_TRIMMED);
            }
            if (!$newgrade->feedback && array_key_exists("revert_to_draft", $grade) && $grade["revert_to_draft"] == 1) {
                $newgrade->feedback = " ";
            }
            if (array_key_exists('feedbackformat', $grade)) {
                $newgrade->feedbackformat = $grade['feedbackformat'];
            }
            $newgrade->datesubmitted = time();

            if(GRADE_UPDATE_OK != grade_update('mod/'.$grade['modname'], $assign->course, 'mod', 'assign',
                $assign->id, 0, $newgrade, $params)){
                $warning = array();
                $warning['item'] = $grade['modname'];
                $warning['itemid'] = $assign->id;
                $warning['warningcode'] = '1';
                $warning['message'] = 'Error updating grade';
                $warnings[] = $warning;
                continue;
            }
            //OK update the assign_grade table or insert !
            if($DB->record_exists($grade['modname'].'_grades', array('assignment'=>$assign->id, 'userid'=>$grade['userid']))){
                $grec=$DB->get_record('assign_grades', array('assignment'=>$assign->id, 'userid'=>$grade['userid']));
                $grec->grader =  $USER->id;
                $grec->timemodified = time();
                if (array_key_exists('grade', $grade)) {
                    $grec->grade = clean_param($grade['grade'], PARAM_TEXT);
                }
                $DB->update_record('assign_grades',$grec);
            } else {
                $grec = new stdClass();
                $grec->assignment=$assign->id;
                $grec->userid=$grade['userid'];
                $grec->timecreated=time();
                $grec->timemodified=time();
                $grec->grader=$USER->id;
                $grec->grade=clean_param($grade['grade'], PARAM_TEXT);
                $grec->locked=0;//TODO
                $grec->mailed=0;//TODO
                $DB->insert_record($grade['modname'].'_grades',$grec);
            }
            $query = <<<EOD
                SELECT gi.idnumber
                   FROM {grade_grades} gg,
                        {grade_items} gi
                  WHERE gg.userid = ?
                        AND gi.idnumber = ?
                        AND gg.itemid = gi.id
EOD;

            // Reset to draft while leaving feedback.
            if (array_key_exists("revert_to_draft", $grade) && $grade["revert_to_draft"] == 1) {
                $DB->execute("UPDATE {assign_submission} SET status='draft' WHERE assignment=? AND userid=?",
                    array($grade["assignid"], $grade["userid"]));
            }

            $DB->execute("DELETE FROM {monorail_data} WHERE type='courseprogress' AND itemid=? AND datakey=?",
                array($assign->courseid, "progress-" . $grade['userid']));

            wscache_reset_by_user($grade["userid"]);

            if (array_key_exists("revert_to_draft", $grade) && $grade["revert_to_draft"] == 1) {
                monorailfeed_create_notification($assign->course, $grade['modname'], 'comment', $assign->cmidnumber, $USER->id, $grade['userid']);
            } else {
                monorailfeed_create_notification($assign->course, $grade['modname'], 'grade', $assign->cmidnumber, $USER->id, $grade['userid']);
            }

            // trigger check for skill achievements
            /* XXX: All skills disabled!
            try {
                require_once("$CFG->dirroot/theme/monorail/skills.php");
                monorail_grant_skills_from_course($assign->course, $grade['userid']);
            } catch (Exception $ex) {
                add_to_log($assign->course, 'monorailservices', 'externallib', 'update_grades', 'Exception when trying to grant skills: '.$ex->getMessage());
            }
            */
        }
        return $warnings;
    }

    /**
     * Describes the return value for update_ctxdata
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_grades_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    private static function assigntasksubmissions($assignids) {
        global $CFG, $DB, $USER;
        $warnings = array();
        $result = array();
        $tasksubmissions = array();
        $role = $DB->get_record('role', array('shortname' => 'student'));
        foreach ($assignids as $assignid) {
            $assignment = $DB->get_record('assign', array('id' => $assignid), "*", MUST_EXIST);
            //get the context id / file info
            $mid = $DB->get_field('modules', 'id', array('name'=>'assign'));
            $cm = $DB->get_record('course_modules', array('course' => $assignment->course, 'module' => $mid,'instance' => $assignment->id), "*", MUST_EXIST);
            $context = context_module::instance($cm->id);
            try {
                self::validate_context($context);
                require_capability('mod/assign:grade', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $assignment->course;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to grade assignment';
                $warnings[] = $warning;
                continue;
            }
            //Ok fetch the users , for each user the submission info and the grade
            require_once($CFG->dirroot . '/enrol/externallib.php');
            require_once($CFG->libdir . '/gradelib.php');
            require_once($CFG->dirroot . '/user/lib.php');
            $users = get_role_users($role->id, context_course::instance($assignment->course), false, "", "u.firstname, u.lastname");
            foreach($users as $cuser) {
                $student = user_get_user_details($cuser, null, array('id', 'fullname', 'profileimageurl'));
                $submission = array();
                $lastsubmissiondate = 0;
                $text = null;
                $grade = array();
                try {
                    $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assignid, 'userid'=> $student['id']));
                    $grading = grade_get_grades($assignment->course, 'mod', 'assign', $assignid, $student['id']);
                    foreach($grading->items as $gitem) {
                        $bookgrade = $gitem->grades[$student['id']];
                        $grade[] = array(
                            'grade'=> (($bookgrade->grade)?intval($bookgrade->grade):null),
                            'grademax'=>intval($gitem->grademax),
                            'gradestring'=> str_replace(' ','',get_string('gradelong', 'grades', array('grade'=>intval($bookgrade->grade),
                                'max'=>intval($gitem->grademax)))),
                            //'gradestring'=>$bookgrade->str_long_grade,
                            'feedback' => $bookgrade->str_feedback,
                            'dategraded' => $bookgrade->dategraded
                        );
                    }
                    if ($submitinfo && $submitinfo->id) {
                        $onlinetext = $DB->get_record('assignsubmission_onlinetext', array('assignment'=>$assignid, 'submission'=>$submitinfo->id));
                        if (isset($onlinetext->onlinetext)) {
                            $text = $onlinetext->onlinetext;
                        }

                    }
                    $tasksub = self::get_submissions($assignid, $student['id']);
                    if(array_key_exists('files', $tasksub)) {
                        $submission = $tasksub['files'];
                    }
                    if (! $submitinfo || $submitinfo->status !== 'submitted') {
                        // https://cdn.meme.am/instances/500x/69356479.jpg
                        throw new Exception();  // no submission or draft, skipping
                    } else {
                        $lastsubmissiondate = $submitinfo->timemodified;
                    }
                    $tasksubmissions[] =  array(
                        'studentid'=> $student['id'],
                        'studentname'=> $student['fullname'],
                        'assignname' => $assignment->name,
                        'profileimageurl' => $student['profileimageurl'],
                        'profilepichash' => monorail_get_userpic_hash($student['id']),
                        'visible' => $cm->visible,
                        'courseid' => $assignment->course,
                        'duedate' => $assignment->duedate,
                        'lastsubmissiondate' => $lastsubmissiondate,
                        'assignid' => $assignid,
                        'nosubmissions'  => $assignment->nosubmissions,
                        'submissions'  => $submission,
                        'grades' => $grade,
                        'text' => $text
                    );
                } catch (Exception $ex) {
                    // no submission or in draft mode
                    $tasksubmissions[] =  array(
                        'studentid'=> $student['id'],
                        'studentname'=> $student['fullname'],
                        'assignname' => $assignment->name,
                        'profileimageurl' => $student['profileimageurl'],
                        'profilepichash' => monorail_get_userpic_hash($student['id']),
                        'visible' => $cm->visible,
                        'courseid' => $assignment->course,
                        'assignid' => $assignid,
                        'nosubmissions'  => $assignment->nosubmissions,
                        'submissions'  => $submission,
                        'duedate' => $assignment->duedate,
                        'lastsubmissiondate' => $lastsubmissiondate,
                        'grades' => $grade,
                        'text' => $text
                    );
                }
            }
        }
        $result['tasksubmissions'] = $tasksubmissions;
        $result['warnings'] = $warnings;
        return $result;
    }

    private static function usertasksubmissions($courseid, $userid) {
        global $CFG, $DB, $USER;

        $warnings = array();
        $result = array();
        $tasksubmissions = array();
        $userenrol = false;
        require_once($CFG->dirroot . '/enrol/externallib.php');
        $courses = core_enrol_external::get_users_courses($userid);
        foreach($courses as $course) {
            if($course['id'] == $courseid){
                $userenrol = true;
                break;
            }
        }
        if(!$userenrol){
            $warning = array();
            $warning['item'] = 'course';
            $warning['itemid'] = $userid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'User not enrolled in the course';
            $warnings[] = $warning;
            $result['tasksubmissions'] = $tasksubmissions;
            $result['warnings'] = $warnings;
            return $result;
        }
        //get all assignments in the course
        $cms = get_coursemodules_in_course('assign', $courseid);
        foreach ($cms as $cm) {
            $assignment = $DB->get_record('assign', array('id' => $cm->instance), "*", MUST_EXIST);
            $context = context_module::instance($cm->id);
            try {
                self::validate_context($context);
                require_capability('mod/assign:grade', $context);
            } catch (Exception $e) {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $assignment->course;
                $warning['warningcode'] = '1';
                $warning['message'] = 'No access rights in course context to grade assignment';
                $warnings[] = $warning;
                continue;
            }
            //user the submission info and the grade
            require_once($CFG->libdir . '/gradelib.php');
            $tasksub = self::get_submissions($assignment->id, $userid);
            $submission = array();
            if(array_key_exists('files', $tasksub)) {
                $submission = $tasksub['files'];
            }
            $grade = array();
            //Ok without submissions also ythe assignment can be graded 0
            $grading = grade_get_grades($assignment->course, 'mod', 'assign', $assignment->id, $userid);

            foreach($grading->items as $gitem) {
                $bookgrade = $gitem->grades[$userid];
                $grade[] = array(
                    'grade'=> (($bookgrade->grade)?intval($bookgrade->grade):null),
                    'grademax'=>intval($gitem->grademax),
                    'gradestring'=> str_replace(' ','',get_string('gradelong', 'grades', array('grade'=>intval($bookgrade->grade),
                        'max'=>intval($gitem->grademax)))),
                    //'gradestring'=>$bookgrade->str_long_grade,
                    'feedback' => $bookgrade->str_feedback,
                    'dategraded' => $bookgrade->dategraded
                );
            }
            require_once($CFG->dirroot . '/user/lib.php');
            $userinfo = user_get_users_by_id(array($userid));
            $username = $userinfo[$userid]->firstname.' '.$userinfo[$userid]->lastname;
            $tasksubmissions[] =  array('studentid'=> $userid,
                'studentname'=> $username,
                'assignname' => $assignment->name,
                'duedate' => $assignment->duedate,
                'visible' => $cm->visible,
                'courseid' => $assignment->course,
                'assignid' => $assignment->id,
                'submissions'  => $submission,
                'grades' => $grade,
            );
        }
        $result['tasksubmissions'] = $tasksubmissions;
        $result['warnings'] = $warnings;
        return $result;
    }

    public static function get_tasksubmissions_parameters() {
        return new external_function_parameters(
            array(
                'tasksubmissions' => new external_single_structure(
                    array(
                        'assignids' => new external_multiple_structure(
                            new external_value(PARAM_INT, 'assignment id'),
                            '0 or more assignment ids',VALUE_OPTIONAL),
                        'usertasks' => new external_single_structure(
                            array(
                                'userid' => new external_value(PARAM_INT, 'user id', VALUE_OPTIONAL),
                                'courseid' => new external_value(PARAM_INT, 'course id')),
                            'User tasks',VALUE_OPTIONAL)),
                    'Task submissions request parameters'))
        );
    }

    /**
     * get tasksubmissions
     * @param array of  $assignids
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function get_tasksubmissions($tasksubmissions) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::get_tasksubmissions_parameters(),
            array('tasksubmissions' => $tasksubmissions));

        $tsparams =  $params['tasksubmissions'];
        if(array_key_exists('assignids',$tsparams)){
            return self::assigntasksubmissions($tsparams['assignids']);
        } else if(array_key_exists('usertasks',$tsparams)) {
            $utasks = $tsparams['usertasks'];
            //If userid is 0 then return all tasks/grades for all students in course
            if(!array_key_exists('userid',$utasks)){
                //get all assignments in the course
                $taskids = array();
                $cms = get_coursemodules_in_course('assign', $utasks['courseid']);
                foreach($cms as $cm){
                    array_push($taskids,$cm->instance);
                }
                return self::assigntasksubmissions($taskids);
            } else {
                return self::usertasksubmissions($utasks['courseid'],$utasks['userid']);
            }
        }
    }

    /**
     * Describes the return value for add_submissions
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_tasksubmissions_returns() {
        return new external_single_structure(
            array(
                'tasksubmissions'   => new external_multiple_structure(self::tasksubmissions(), 'Task submission details for users in course'),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function get_task_details_parameters()
    {
        return new external_function_parameters(
            array(
                'instanceid' => new external_value(PARAM_INT, 'instance id of the item')
            )
        );
    }

    public static function get_task_details($instanceid)
    {
        global $CFG, $DB, $USER;
        return self::util_get_task_details($instanceid);
    }

    public static function util_get_task_details($instanceid, $validatecontext=true)
    {
        global $CFG, $DB, $USER;
        $params = self::validate_parameters(self::get_task_details_parameters(),
            array('instanceid' => $instanceid));

        $instanceInfo = $DB->get_record_sql("SELECT cm.instance AS id, m.name AS module, cm.course AS courseid".
            " FROM {course_modules} AS cm, {modules} AS m WHERE m.id=cm.module AND cm.id=?",
            array($instanceid), MUST_EXIST);

        $result = array(
            "id" => $instanceInfo->id,
            "instanceid" => $params["instanceid"],
            "modulename" => $instanceInfo->module);

        $moduleInfo = $DB->get_record_sql("SELECT * FROM {" . $instanceInfo->module . "} WHERE id=?",
            array($instanceInfo->id), MUST_EXIST);
        $can_edit = (has_capability('moodle/course:manageactivities',
            context_course::instance($moduleInfo->course))) ? 1 : 0;

        $context = context_module::instance($params["instanceid"]);

        /*
        try
        {
          if($validatecontext)
            self::validate_context($context);
          require_capability('mod/assign:view', $context);
        }
        catch (Exception $e)
        */

        $canViewCourse = self::can_view_course($instanceInfo->courseid, $USER->id);

        if (!$canViewCourse) {
          $warning = array();
          $warning['item'] = 'module ';
          $warning['itemid'] = $moduleInfo->id;
          $warning['warningcode'] = '1';
          $warning['message'] = 'No access rights in module context';
          $result["warnings"] = array($warning);

          return $result;
        }

        $result["visible"] = $DB->get_field('course_modules', 'visible', array('id'=>$instanceid));
        $result["can_edit"] = $can_edit;

        if (!$result["visible"] && !$result["can_edit"]) {
          $warning = array();
          $warning['item'] = 'module ';
          $warning['itemid'] = $moduleInfo->id;
          $warning['warningcode'] = '1';
          $warning['message'] = 'No access rights in module context';
          $result["warnings"] = array($warning);

          return $result;
        }

        $result["name"] = $moduleInfo->name;
        $result["courseid"] = $moduleInfo->course;

        $result["intro"] = $moduleInfo->intro;
        $result["questions"] = array();
        $result["answers"] = array();

        $filearray = array();
        $fs = get_file_storage();

        $files = $fs->get_area_files($context->id, 'mod_' . $instanceInfo->module, 'content');

        foreach ($files as $file)
        {
            if(!($file->get_filename() === '.'))
            {
                $names = monorail_data('assignfile', $file->get_id(), 'name');
                foreach ($names as $name) {
                    $filedisplayname = $name->value;
                    break;
                }
                if ($filedisplayname == null) {
                    $filedisplayname = $file->get_filename();
                }
                $url = file_encode_url("$CFG->wwwroot/local/monorailservices/pluginfile.php?token=WSTOKEN&file=path=",$file->get_pathnamehash(),false);
                $resultfile = array();
                $resultfile['id'] = $file->get_id();
                $resultfile['name'] = $filedisplayname;
                $resultfile['filename'] = $file->get_filename();
                $resultfile['timecreated'] = $file->get_timecreated();
                $resultfile['url'] =  $url;
                //$resultfile['convertedpath'] =  monorail_converted_fileurl($file, true);
                $resultfile['contextualdata'] = self::monorailservices_get_file_contextdata($file->get_pathnamehash(), $file->get_id());
                $resultfile['mimetype'] = $file->get_mimetype();
                $optionData = monorail_data('assignfile', $file->get_id(), 'options');
                $optionData = reset($optionData);

                if ($optionData !== FALSE)
                {
                    $resultfile["options"] = $optionData->value;
                }

                $filearray[] = $resultfile;
            }
        }

        $result["fileinfo"] = $filearray;

        $vfilearray = array();
        $vfilerscs = $DB->get_records("monorail_assign_kalvidres", array("courseid" => $moduleInfo->course, "taskid" => $result['instanceid']));
        foreach($vfilerscs as $vfilersc) {
            $resultfile = array();
            $resultfile['id'] = $vfilersc->entry_id;
            $vfilearray[] = $resultfile;
        }
        $result["vfileinfo"] = $vfilearray;
        switch ($instanceInfo->module)
        {
            case "quiz":
                if ($moduleInfo->timeclose > 0) {
                    $result["duedate"] = date(DATE_ISO8601, $moduleInfo->timeclose);
                }

                if ($moduleInfo->timeopen > 0) {
                    $result["startdate"] = date(DATE_ISO8601, $moduleInfo->timeopen);
                }

                $result["attempts"] = $DB->get_field('quiz', 'attempts' , array('id'=>$instanceInfo->id));
                $questionIds = explode(",", $moduleInfo->questions);

                $questions = $DB->get_records_sql("SELECT q.id, q.name, q.questiontext, qi.id AS instanceid, mcq.correctfeedback AS correctfeedback, mcq.partiallycorrectfeedback AS partiallycorrectfeedback" .
                    " FROM {question} AS q" .
                        " INNER JOIN {quiz_question_instances} AS qi ON q.id=qi.question" .
                        " LEFT JOIN {question_multichoice} AS mcq ON mcq.question=q.id" .
                            " WHERE qi.quiz=?",

                        array($instanceInfo->id));

                if ($can_edit) {
                    // Teacher of the quiz, get all attempts
                    $submittedTakers = $DB->get_fieldset_sql("SELECT datakey FROM {monorail_data} WHERE itemid=? AND type='quiz_end' AND value='1'", array($instanceInfo->id));

                    if (!empty($submittedTakers)) {
                        $takers = $DB->get_records_sql("SELECT id, value, datakey, timemodified FROM {monorail_data} WHERE " .
                            "itemid=? AND type='quiz_attempt' AND datakey IN (" . implode(", ", array_map(function($iu) { return "'" . $iu . "'"; }, $submittedTakers)) . ")",
                                array($instanceInfo->id));
                    } else {
                        $takers = array();
                    }
                } else {
                    // Student, get only own attempt

                    $takers = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE type='quiz_attempt' AND itemid=? AND datakey=?",
                        array("id" => $instanceInfo->id, "datakey" => "user" . $USER->id));

                    $attempts_count  = reset(monorail_data('quiz_attempts_count', $instanceInfo->id, "user" . $USER->id));
                    if($attempts_count) {
                      $result["attemptsleft"] = max($result["attempts"] - $attempts_count->value, 0);
                    } else {
                      $result["attemptsleft"] = $result["attempts"];
                    }
                    $endquiz  = reset(monorail_data('quiz_end', $instanceInfo->id, "user" . $USER->id));
                    if($endquiz) {
                      $result["endquiz"] = $endquiz->value;
                    } else {
                      $result["endquiz"] = 0;
                    }
                }

                foreach ($takers as $taker) {
                    $datetaken = $taker->timemodified;
                    $usebest = false;
                    $result["isgraded"] = 1;

                    // Best attempts are being removed (this is here for
                    // compatibility with old data...)
                    $bestAttempt = $DB->get_record_sql("SELECT id, value, timemodified FROM {monorail_data} WHERE type='quiz_attempt_best' AND itemid=? AND datakey=?",
                        array($instanceInfo->id, "user" . substr($taker->datakey, 4)));

                    if($bestAttempt) {
                        $endquiz  = reset(monorail_data('quiz_end', $instanceInfo->id, "user" . substr($taker->datakey, 4)));
                        if($endquiz && $endquiz->value == 0) {
                           $usebest = true;
                           $datetaken = $bestAttempt->timemodified;
                        }
                    }

                    if($usebest) {
                         $result["answers"][] = array("student" => substr($taker->datakey, 4), "datetaken" => $datetaken, "questions" => unserialize($bestAttempt->value));
                    } else {
                        $result["answers"][] = array("student" => substr($taker->datakey, 4), "datetaken" => $datetaken, "questions" => unserialize($taker->value));
                    }
                }

                foreach ($questions as $question)
                {
                    if (in_array($question->id, $questionIds))
                    {
                        $answers = $DB->get_records_sql("SELECT a.id, a.answer, a.fraction" .
                            " FROM {question_answers} AS a WHERE a.question=?", array($question->id));

                        $answ = array();

                        foreach ($answers as $answer)
                        {
                            $answ[] = array(
                                "id" => $answer->id,
                                "text" => $answer->answer,
                                // TODO: if user is a student and this quiz
                                // hasn't been attempted, don't provide
                                // this information (student might check
                                // raw request data... and cheat)
                                "correct" => ($answer->fraction > 0) ? 1 : 0);
                        }

                        $result["questions"][] = array(
                            "id" => $question->id,
                            "instanceid" => $question->instanceid,
                            "explanation" => $question->correctfeedback,
                            "type" => (int) $question->partiallycorrectfeedback,
                            //"name" => $question->name,
                            "text" => $question->questiontext,
                            "answers" => $answ);
                    }
                }

                break;

            case "assign":
                if($moduleInfo->duedate > 0) {
                    $result["duedate"] = date(DATE_ISO8601, $moduleInfo->duedate);
                }
                if ($moduleInfo->allowsubmissionsfromdate > 0) {
                    $result["startdate"] = date(DATE_ISO8601, $moduleInfo->allowsubmissionsfromdate);
                }
                $result["nosubmissions"] = $moduleInfo->nosubmissions;
                if(!$moduleInfo->nosubmissions) {
                  //Task if gradeD
                  $result["isgraded"] = $DB->record_exists("assign_submission", array("assignment" => $result["id"], "status" => "submitted")) ? 1 : 0;
                }
                break;
        }

        $monorailData = $DB->get_records_sql("SELECT id, datakey, value FROM {monorail_data} WHERE itemid=?", array($result["instanceid"]));

        foreach ($monorailData as $md) {
            switch ($md->datakey) {
                case "nolatesubmissions":
                    $result["nolatesubmissions"] = $md->value;
                    break;

                case "task_options":
                    $result["options"] = $md->value;
                    break;
            }
        }

        return $result;
    }

    public static function get_task_details_returns()
    {
        return new external_single_structure(
            array(
                'id'   => new external_value(PARAM_INT, 'item id', VALUE_OPTIONAL),
                'instanceid'                => new external_value(PARAM_INT, 'instanceid', VALUE_OPTIONAL),
                'modulename'                => new external_value(PARAM_TEXT, 'name of task module', VALUE_OPTIONAL),
                'name'                      => new external_value(PARAM_TEXT, 'name of task', VALUE_OPTIONAL),
                'intro'                     => new external_value(PARAM_RAW, 'intro text of task', VALUE_OPTIONAL),
                'duedate'                   => new external_value(PARAM_TEXT, 'assignment due date', VALUE_OPTIONAL),
                'startdate'                 => new external_value(PARAM_TEXT, 'assignment start date', VALUE_OPTIONAL),
                'nolatesubmissions'         => new external_value(PARAM_INT, 'disable late submissions', VALUE_OPTIONAL),
                'courseid'                  => new external_value(PARAM_INT, 'id of the course', VALUE_OPTIONAL),
                'can_edit'                  => new external_value(PARAM_INT, 'can this user edit this task', VALUE_OPTIONAL),
                'visible'                  => new external_value(PARAM_INT, 'IS task visible', VALUE_OPTIONAL),
                'nosubmissions'             => new external_value(PARAM_INT, 'no submissions', VALUE_OPTIONAL),
                'attempts'             => new external_value(PARAM_INT, 'no of attempts', VALUE_OPTIONAL),
                'attemptsleft'             => new external_value(PARAM_INT, 'no of attempts', VALUE_OPTIONAL),
                'endquiz'             => new external_value(PARAM_INT, 'If user has closed the quiz', VALUE_DEFAULT,0),
                'isgraded'             => new external_value(PARAM_INT, 'If task has submissions.', VALUE_DEFAULT,0),

                'questions'                 => new external_multiple_structure(new external_single_structure(
                        array(
                            'id'  => new external_value(PARAM_INT, 'Question id'),
                            'instanceid' => new external_value(PARAM_INT, 'Question instance id'),
                            'name' => new external_value(PARAM_RAW, 'Question name'),
                            'text' => new external_value(PARAM_RAW, 'Question text'),
                            'explanation' => new external_value(PARAM_TEXT, 'Question explanation', VALUE_OPTIONAL),
                            'type' => new external_value(PARAM_INT, 'Question type', VALUE_OPTIONAL),

                            'answers' => new external_multiple_structure(new external_single_structure(array(
                                'id' => new external_value(PARAM_INT, 'Answer id'),
                                'text' => new external_value(PARAM_RAW, 'Answer text'),
                                'correct' => new external_value(PARAM_INT, 'Is answer correct')
                            )), "Answers for the question.", VALUE_OPTIONAL)
                        )
                    ), 'Quiz questions (available for quizzes)', VALUE_OPTIONAL),

                'answers' => new external_multiple_structure(new external_single_structure(
                    array(
                        "student" => new external_value(PARAM_INT, "Student id"),
                        "datetaken" => new external_value(PARAM_INT, 'Date when quiz taken', VALUE_OPTIONAL),
                        "questions" => new external_multiple_structure(new external_single_structure(array(
                            "question" => new external_value(PARAM_INT, "Question id"),
                            "answers" => new external_multiple_structure(new external_single_structure(array(
                                "answer" => new external_value(PARAM_INT, "Answer id"),
                                "correct" => new external_value(PARAM_INT, "Is answer correct"),
                                "checked" => new external_value(PARAM_TEXT, "Did student checked this answer")
                            )))
                        ))),
                    )), "answers from students", VALUE_OPTIONAL),

                'fileinfo' => new external_multiple_structure(self::fileinfo(), 'list of file attachments', VALUE_OPTIONAL),
                'vfileinfo' => new external_multiple_structure(self::vfileinfo(), 'list of video file attachments', VALUE_OPTIONAL),
                'options' => new external_value(PARAM_RAW, 'extra task options', VALUE_OPTIONAL),

                'warnings'                  => new external_warnings()
            )
        );
    }

    //=========================================================
    //core_user modified API CBTEC
    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function get_users_by_id_parameters() {
        return new external_function_parameters(
            array(
                'userids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'user ID'),'list of user ids',VALUE_DEFAULT,array())
            )
        );
    }

    /*
     * returns the first record from a recordset, useful when we are expecting only one record
     */
    private static function get_first_record_from_recordset($recordset)
    {
        $ret = NULL;
        if($recordset) {
            foreach($recordset as $record) {
                $ret = $record;
                break;
            }
        }
        return $ret;
    }

/** calculate the video limit for the user
    //for premium users(and for old users) it is 1GB and for others it is 200MB
    //premium user is someone who has paid or an NGO admin owner
*/
    public static function get_user_video_limit($userid) {
        global $DB;
        $premium_user_video_limit = 1024; //MB
        $other_users_video_limit = 200; //MB
        $newUserTime = strtotime("15 March 2017"); //anyone who joined after this date is new user

        $userInfos = $DB->get_recordset_sql("SELECT  deleted, suspended, timecreated  FROM {user} " .
                                "WHERE  id = ?" ,array($userid)) ;
        $userInfo = self::get_first_record_from_recordset($userInfos);
        if(!$userInfo || $userInfo->deleted || $userInfo->suspended) { //not a valid user
            return $other_users_video_limit;
        } else if ($userInfo->timecreated < $newUserTime) { //old users
            return $premium_user_video_limit;
        }

        //usertype==1 means user is the owner, there can be many admins, but there can be only one owner
        //company_status (1 - demo, 2 - regular, 3 - ngo, 4 - suspended, 5 - ngo review pending, 6 - demo ended, 7 - payment missed
        $cohortAdminInfos = $DB->get_recordset_sql("SELECT  mcau.cohortid, mci.company_status, mci.next_billing ".
            "FROM {monorail_cohort_admin_users} AS mcau " .
                            "INNER JOIN {monorail_cohort_info} AS mci ON mci.cohortid=mcau.cohortid " .
                                "WHERE mcau.usertype = 1 AND mci.company_status IN (2,3) AND mcau.userid = ?" ,array($userid)) ;
        $ret = $other_users_video_limit;
        $cohortAdminInfo = self::get_first_record_from_recordset($cohortAdminInfos);
        if(!$cohortAdminInfo) { //not an admin, no records found
        } else {
            if($cohortAdminInfo->company_status == 3) { //this is an NGO
                $ret = $premium_user_video_limit;
            } else if($cohortAdminInfo->company_status == 2) { //this is a regular user
                if($cohortAdminInfo->next_billing && ($cohortAdminInfo->next_billing < time())) { //user has paid
                    $ret = $premium_user_video_limit;
                }
            }
        }
        return $ret;
    }

    /**
     * Get user information
     * - This function is matching the permissions of /user/profil.php
     * - It is also matching some permissions from /user/editadvanced.php for the following fields:
     *   auth, confirmed, idnumber, lang, theme, timezone, mailformat
     *
     * @param array $userids  array of user ids
     * @return array An array of arrays describing users
     * @since Moodle 2.2
     */
    public static function get_users_by_id($userids) {
        global $CFG, $USER, $DB;
        require_once($CFG->dirroot . "/user/lib.php");
        require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');

        $params = self::validate_parameters(self::get_users_by_id_parameters(),
            array('userids'=>$userids));

        if(empty($userids)){
            array_push($userids, $USER->id);
        }
        $users = $DB->get_records_list('user', 'id', $userids);

        $studentRoleIds = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN ('student')");
        $teacherRoleIds = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN ('teacher', 'editingteacher', 'manager')");

        if (empty($studentRoleIds)) {
            $studentRoleIds[] = 0;
        }

        if (empty($teacherRoleIds)) {
            $teacherRoleIds[] = 0;
        }

        $result = array();
        $hasuserupdatecap = has_capability('moodle/user:update', get_system_context());
        foreach ($users as $user) {
            if (!empty($user->deleted)) {
                continue;
            }
            try {
                context_instance_preload($user);
                $usercontext = get_context_instance(CONTEXT_USER, $user->id);
                self::validate_context($usercontext);
            } catch (Exception $ex) {
                continue;
            }
            $currentuser = ($user->id == $USER->id);

            if ($userarray  = user_get_user_details($user, null,
                array('id', 'username', 'fullname', 'firstname', 'lastname', 'email',
                        'skype',
                        'institution', 'interests', 'firstaccess', 'lastaccess',
                        'lang', 'timezone', 'description',
                        'city', 'url', 'country', 'profileimageurlsmall', 'profileimageurl', 'customfields',
                        'roles', 'preferences'))) {
                //fields needed by cohort admin
                $userarray['cohortid'] = 0;
                $userarray["orgname"] = "";
                $cohortadmin = false;
                $cohort = $DB->get_field('cohort_members', 'cohortid', array('userid'=>$user->id), IGNORE_MULTIPLE);
                //Need this as institution field is not povided by user_get_user_details for non  current/admin user
                $urec = $DB->get_record('user' ,array('id'=>$user->id));
                $userarray['institution'] = $urec->institution;
                $userarray['maildisplay'] = $urec->maildisplay;
                if($cohort) {
                    // This user is a cohort member!
                    $userarray['cohortid']=$cohort;
                    $userarray["orgname"] = $DB->get_field_sql("SELECT name FROM {cohort} WHERE id=?", array($cohort));
                    $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohort));

                    if ($auser) {
                        $cohortadmin = true;
                        //user_get_user_details returns email/description only for self and for course admin, here we need it for cohort
                        //admin user
                        $userarray['email'] = $urec->email;
                        $userarray['description'] = $urec->description;

                        if ($currentuser) {
                            $userarray["cohortusertype"] = $auser->usertype;
                        }
                    }
                } else {
                    $linkedCohorts = $DB->get_fieldset_sql("SELECT cohortid FROM {monorail_cohort_user_link} WHERE userid=?", array($user->id));

                    foreach ($linkedCohorts as $cohId) {
                        if ($DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohId))) {
                            $cohortadmin = true;
                            $userarray['email'] = $urec->email;
                            $userarray['description'] = $urec->description;
                            break;
                        }
                    }
                }

                if (! $currentuser && ! $cohortadmin && ! monorailservices_is_member_own_courses($USER->id, $user->id)) {
                    // no access to any info
                    continue;
                }

                //fields matching permissions from /user/editadvanced.php
                if ($currentuser) {
                    $userarray['lang']       = $user->lang;
                    $userarray['timezone']   = ($user->timezone == '99') ? 'Europe/Helsinki' : $user->timezone;
                    $userarray['timeoffset'] = (int) get_user_timezone_offset($user->timezone)*3600 + dst_offset_on(time(),$user->timezone);
                    $userarray['timezonecustom'] = $user->timezone == '99' ? 0 : 1;
                    $userarray['fcid'] 		= get_config('auth/googleoauth2', 'facebookclientid');

                    $tutorial = $DB->get_record('monorail_data', array(
                    								'itemid' => $user->id,
                    								'datakey'=> 'tutorial'
                    							));
                    if ($tutorial) {
                    	$userarray['tutorial'] = $tutorial->value;
                    }
                    else {
                    	$userarray['tutorial'] = 'tutorial';
                    }
                    //Current video usage
                    $userarray['videousage'] = 0; //MB
                    $userarray['videolimit'] = self::get_user_video_limit($user->id);
                    $vrecords = $DB->get_records("monorail_course_videorsc", array("userid"=>$USER->id));
                    foreach($vrecords as $vrecord) {
                      $userarray['videousage'] = $userarray['videousage'] + $vrecord->filesize; //MB
                    }
                    $userarray['videousage'] = round($userarray['videousage']/(1024 * 1024)); //MB
                    //Get email notification settings
                    $notificationsettings = monorailfeed_user_email_settings($user->id);
                    $nsettings = array();
                    foreach ($notificationsettings as $nsetting) {
                        $nsettings[] = array(
                            'type' => $nsetting->type,
                            'value' => $nsetting->value
                        );
                    }
                    $userarray['notificationsettings'] = $nsettings;

                    //Used in mobile client for logout of the user
                    $userarray['sessionkey'] = sesskey();
                } else {
                    unset($userarray['username']);
                    unset($userarray['lang']);
                    unset($userarray['timezone']);
                    unset($userarray['preferences']);

                    if (!$userarray['maildisplay'] && !$cohortadmin) {
                        // Also, check if this user is not a student in one
                        // of the courses where current user is a teacher
                        // (for teachers must see their student emails):

                        $commonCourses = $DB->get_fieldset_sql("SELECT ctx.instanceid AS id FROM {context} AS ctx " .
                            "INNER JOIN {role_assignments} AS ra1 ON ra1.contextid=ctx.id AND ctx.contextlevel=50 AND " .
                                "ra1.userid=? AND ra1.roleid IN (" . implode(", ", $teacherRoleIds) . ") " .
                            "INNER JOIN {role_assignments} AS ra2 ON ra2.contextid=ctx.id AND ctx.contextlevel=50 AND " .
                                "ra2.userid=? AND ra2.roleid IN (" . implode(", ", $studentRoleIds) . ") " .
                                    "LIMIT 1", array($USER->id, $user->id));

                       if (empty($commonCourses)) {
                           $userarray['email'] = "*******";
                       } else {
                           $userarray['email'] = $user->email;
                       }
                    }
                }
                if (!$currentuser && !$cohortadmin) {
                    unset($userarray['lastaccess']);
                    unset($userarray['firstaccess']);
                }

                $userarray["enrolledcourses"] = array();
                $cohortPart = array();

                // XXX: This is a faster way to get user-course lists... contextlevel 50 means course.
                $userCourses = $DB->get_records_sql("SELECT c.id AS id, GROUP_CONCAT(r.shortname SEPARATOR ';') AS userrole " .
                    "FROM {role_assignments} AS ra " .
                        "INNER JOIN {role} AS r ON r.id=ra.roleid " .
                        "INNER JOIN {context} AS ctx ON ctx.id=ra.contextid AND ctx.contextlevel=50 " .
                        "INNER JOIN {course} AS c ON c.id=ctx.instanceid " .
                            "WHERE ra.userid=? GROUP BY id", array($user->id));

                foreach ($userCourses as $uc)
                {
                    wscache_add_cache_dependency("course_info_" . $uc->id);

                    $uci = (array) monorail_get_course_details($uc->id);

                    $uci["userrole"] = implode(";", array_diff(explode(";", $uc->userrole), array("manager")));
                    $uci["isteacher"] = ($uci["userrole"] == "teacher" || $uci["userrole"] == "editingteacher" || $uci["userrole"] == "coursecreator") ? 1 : 0;

                    // Same user and cohort admin can see all courses.
                    // Others can see only public courses where this user
                    // is a teacher.
                    if ($currentuser || $cohortadmin || ($uci["isteacher"] && $uci["course_privacy"] == 1 && $uci["course_access"] != 1))
                    {
                        $courseCohortId = $DB->get_record_sql("SELECT c.id AS id, c.name AS name FROM {cohort} AS c" .
                            " INNER JOIN {monorail_cohort_courseadmins} AS mca ON mca.cohortid=c.id " .
                                "WHERE mca.courseid=?", array($uc->id));

                        if ($courseCohortId) {
                            $cohortPart[$courseCohortId->id] = $courseCohortId->name;
                            $uci["cohortid"] = $courseCohortId->id;
                        }

                        $userarray["enrolledcourses"][] = $uci;
                    }
                }

                /*
                if ($currentuser) {
                    $userarray['skills'] = array();
                    require_once("$CFG->dirroot/theme/monorail/skills.php");
                    $skills = monorail_get_user_skills($user->id);
                    foreach ($skills as $skill) {
                        $skillrec = monorail_get_skill($skill->skillid);
                        $record = array(
                            'name'=> $skillrec->name,
                            'courses'=> array(),
                        );
                        $courses = json_decode($skill->achievedfrom);
                        foreach ($courses as $course) {
                            $record['courses'][] = $course->coursename;
                        }
                        $userarray['skills'][] = $record;
                    }
                }
                */

                // Profile pic
                $userarray['profilepichash'] = monorail_get_userpic_hash($user->id);

                $cohortParts = array();
                foreach ($cohortPart as $key => $val) {
                    $cohIcon = monorail_data("cohort", $key, "iconfilename");

                    if (empty($cohIcon)) {
                        $cohIcon = "";
                    } else {
                        $cohIcon = reset($cohIcon);
                        $cohIcon = $cohIcon->value;
                    }

                    $cohortParts[] = array(
                        "cohortid" => $key,
                        "orgname" => $val,
                        'cohorticonurl' => $cohIcon
                    );
                }

                $userarray["cohortparticipant"] = $cohortParts;

                wscache_add_cache_dependency("user_info_" . $user->id);
                wscache_add_cache_dependency("user_courses_" . $user->id);

                $result[] = $userarray;
            }
        }

        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function get_users_by_id_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'    => new external_value(PARAM_NUMBER, 'ID of the user'),
                    'username'    => new external_value(PARAM_RAW, 'Username policy is defined in Moodle security config', VALUE_OPTIONAL),
                    'firstname'   => new external_value(PARAM_NOTAGS, 'The first name(s) of the user', VALUE_OPTIONAL),
                    'lastname'    => new external_value(PARAM_NOTAGS, 'The family name of the user', VALUE_OPTIONAL),
                    'fullname'    => new external_value(PARAM_NOTAGS, 'The fullname of the user'),
                    'email'       => new external_value(PARAM_TEXT, 'An email address - allow email as root@localhost', VALUE_OPTIONAL),
                    'skype'       => new external_value(PARAM_NOTAGS, 'skype id', VALUE_OPTIONAL),
                    'institution' => new external_value(PARAM_TEXT, 'institution', VALUE_OPTIONAL),
                    'fcid'        => new external_value(PARAM_ALPHANUM, 'FB client id', VALUE_OPTIONAL),
                    'interests'   => new external_value(PARAM_TEXT, 'user interests (separated by commas)', VALUE_OPTIONAL),
                    'firstaccess' => new external_value(PARAM_INT, 'first access to the site (0 if never)', VALUE_OPTIONAL),
                    'lastaccess'  => new external_value(PARAM_INT, 'last access to the site (0 if never)', VALUE_OPTIONAL),
                    'lang'        => new external_value(PARAM_SAFEDIR, 'Language code such as "en", must exist on server', VALUE_OPTIONAL),
                    'timezone'    => new external_value(PARAM_TEXT, 'Timezone code such as Australia/Perth, or Europe/Helsinki for default', VALUE_OPTIONAL),
                    'timeoffset'  => new external_value(PARAM_INTEGER, 'Time offset from UTC in seconds', VALUE_OPTIONAL),
                    'timezonecustom' => new external_value(PARAM_INTEGER, '0/1 - is timezone set?', VALUE_OPTIONAL),
                    'description' => new external_value(PARAM_RAW, 'User profile description', VALUE_OPTIONAL),
                    'city'        => new external_value(PARAM_NOTAGS, 'Home city of the user', VALUE_OPTIONAL),
                    'url'         => new external_value(PARAM_URL, 'URL of the user', VALUE_OPTIONAL),
                    'country'     => new external_value(PARAM_ALPHA, 'Home country code of the user, such as AU or CZ', VALUE_OPTIONAL),
                    'sessionkey'     => new external_value(PARAM_TEXT, 'Session key for the user , used for logout', VALUE_OPTIONAL),
                    'profileimageurlsmall' => new external_value(PARAM_URL, 'User image profile URL - small version'),
                    'profileimageurl' => new external_value(PARAM_URL, 'User image profile URL - big version'),
                    'profilepichash' => new external_value(PARAM_TEXT, 'Profile pic hash', VALUE_OPTIONAL),
                    'tutorial'   => new external_value(PARAM_TEXT, 'Tutorials viewed by user', VALUE_OPTIONAL),
                    'cohortid'   => new external_value(PARAM_INT, 'Cohort id of the user', VALUE_OPTIONAL),
                    'cohortusertype' => new external_value(PARAM_INT, 'Cohort the user (if admin)', VALUE_DEFAULT, 0),
                    'orgname'   => new external_value(PARAM_TEXT, 'Cohort name of the user', VALUE_OPTIONAL),
                    'maildisplay'   => new external_value(PARAM_INT, 'Mail display', VALUE_OPTIONAL),
                    'videousage'   => new external_value(PARAM_INT, 'Video upload resource usage value(MB)', VALUE_OPTIONAL),
                    'videolimit'   => new external_value(PARAM_INT, 'Video upload resource usage value(MB)', VALUE_OPTIONAL),
                    "cohortparticipant" => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'cohortid'  => new external_value(PARAM_INT, 'Cohort id of the user', VALUE_OPTIONAL),
                                'orgname'   => new external_value(PARAM_TEXT, 'Cohort name of the user', VALUE_OPTIONAL),
                                'cohorticonurl' => new external_value(PARAM_TEXT, 'Cohort name of the user', VALUE_OPTIONAL)
                            )
                        ), 'Cohorts where user has courses', VALUE_OPTIONAL),
                    'notificationsettings' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'type'  => new external_value(PARAM_ALPHANUMEXT, 'notification type'),
                                'value' => new external_value(PARAM_TEXT, '2=always, 1=daily, 0=never')
                            )
                        ), 'User email notification settings', VALUE_OPTIONAL),
                    'customfields' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'type'  => new external_value(PARAM_ALPHANUMEXT, 'The type of the custom field - text field, checkbox...'),
                                'value' => new external_value(PARAM_TEXT, 'The value of the custom field'),
                                'name' => new external_value(PARAM_TEXT, 'The name of the custom field'),
                                'shortname' => new external_value(PARAM_TEXT, 'The shortname of the custom field - to be able to build the field class in the code'),
                            )
                        ), 'User custom fields (also known as user profil fields)', VALUE_OPTIONAL),
                    'preferences' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'name'  => new external_value(PARAM_ALPHANUMEXT, 'The name of the preferences'),
                                'value' => new external_value(PARAM_TEXT, 'The value of the custom field'),
                            )
                        ), 'User preferences', VALUE_OPTIONAL),
                    'enrolledcourses' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'id'  => new external_value(PARAM_INT, 'Id of the course'),
                                'fullname'  => new external_value(PARAM_TEXT, 'Fullname of the course'),
                                'shortname' => new external_value(PARAM_TEXT, 'Shortname of the course'),
                                'code'  => new external_value(PARAM_TEXT, 'Course code'),
                                'userrole' => new external_value(PARAM_TEXT, 'Role of the user in this course'),
                                'coursestatus' => new external_value(PARAM_TEXT, 'Course status'),
                                'coursebackgroundtn' => new external_value(PARAM_TEXT, 'course background picture', VALUE_OPTIONAL),
                                'students' => new external_value(PARAM_INT, 'students count in course', VALUE_OPTIONAL),
                                'stars' => new external_value(PARAM_INT, 'stars given to course (-1 if not applicable)', VALUE_OPTIONAL),
                                'price' => new external_value(PARAM_FLOAT, 'course price (-1 if not applicable)', VALUE_OPTIONAL),
                                'teachername' => new external_value(PARAM_TEXT, 'course info', VALUE_OPTIONAL),
                                'teachertitle' => new external_value(PARAM_TEXT, 'course info', VALUE_OPTIONAL),
                                'teacherlogo' => new external_value(PARAM_TEXT, 'course info', VALUE_OPTIONAL),
                                'cohortid'  => new external_value(PARAM_INT, 'Id of the course cohort', VALUE_OPTIONAL),
                                'durationstr' => new external_value(PARAM_TEXT, 'course duration string', VALUE_OPTIONAL),
                                'cert_enabled' => new external_value(PARAM_INT, 'certificates enabled', VALUE_OPTIONAL),
                                'course_privacy' => new external_value(PARAM_INT, 'course public=1, private=2', VALUE_OPTIONAL),
                                'course_access' => new external_value(PARAM_INT, 'course access draft=1, invite=2, open=3', VALUE_OPTIONAL),
                                'isteacher' => new external_value(PARAM_INT, 'is a teacher in this course', VALUE_OPTIONAL)
                            )
                        ), 'Courses where the user is enrolled - limited by which courses the user is able to see', VALUE_OPTIONAL
                    ),
                    'skills' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'name' => new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL),
                                'courses' => new external_multiple_structure(
                                    new external_value(PARAM_TEXT, 'course name', VALUE_OPTIONAL)
                                )
                            )
                        ), 'skill list', VALUE_OPTIONAL
                    ),
                )
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function update_users_parameters() {
        global $CFG;
        return new external_function_parameters(
            array(
                'users' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id'    => new external_value(PARAM_NUMBER, 'ID of the user'),
                            'username'    => new external_value(PARAM_USERNAME, 'Username policy is defined in Moodle security config. Must be lowercase.', VALUE_OPTIONAL, '',NULL_NOT_ALLOWED),
                            'password'    => new external_value(PARAM_TEXT, 'Plain text password consisting of any characters', VALUE_OPTIONAL, '',NULL_NOT_ALLOWED),
                            'firstname'   => new external_value(PARAM_NOTAGS, 'The first name(s) of the user', VALUE_OPTIONAL, '',NULL_NOT_ALLOWED),
                            'lastname'    => new external_value(PARAM_NOTAGS, 'The family name of the user', VALUE_OPTIONAL),
                            'email'       => new external_value(PARAM_EMAIL, 'A valid and unique email address', VALUE_OPTIONAL, '',NULL_NOT_ALLOWED),
                            'auth'        => new external_value(PARAM_PLUGIN, 'Auth plugins include manual, ldap, imap, etc', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                            'idnumber'    => new external_value(PARAM_TEXT, 'An arbitrary ID code number perhaps from the institution', VALUE_OPTIONAL),
                            'lang'        => new external_value(PARAM_SAFEDIR, 'Language code such as "en", must exist on server', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                            'theme'       => new external_value(PARAM_PLUGIN, 'Theme name such as "standard", must exist on server', VALUE_OPTIONAL),
                            'timezone'    => new external_value(PARAM_TEXT, 'Timezone code such as Australia/Perth, or 99 for default', VALUE_OPTIONAL),
                            'mailformat'  => new external_value(PARAM_INTEGER, 'Mail format code is 0 for plain text, 1 for HTML etc', VALUE_OPTIONAL),
                            'description' => new external_value(PARAM_TEXT, 'User profile description', VALUE_OPTIONAL),
                            'city'        => new external_value(PARAM_NOTAGS, 'Home city of the user', VALUE_OPTIONAL),
                            'country'     => new external_value(PARAM_ALPHA, 'Home country code of the user, such as AU or CZ', VALUE_OPTIONAL),
                            'maildisplay'     => new external_value(PARAM_INT, 'Mail display setting for the user', VALUE_OPTIONAL),
                            //CBTEC start
                            'phone1'      => new external_value(PARAM_NOTAGS, 'Phone 1', VALUE_OPTIONAL),
                            'phone2'      => new external_value(PARAM_NOTAGS, 'Phone 2', VALUE_OPTIONAL),
                            'icq'         => new external_value(PARAM_NOTAGS, 'icq number', VALUE_OPTIONAL),
                            'skype'       => new external_value(PARAM_NOTAGS, 'skype id', VALUE_OPTIONAL),
                            'yahoo'       => new external_value(PARAM_NOTAGS, 'yahoo id', VALUE_OPTIONAL),
                            'aim'         => new external_value(PARAM_NOTAGS, 'aim id', VALUE_OPTIONAL),
                            'msn'         => new external_value(PARAM_NOTAGS, 'msn number', VALUE_OPTIONAL),
                            'department'  => new external_value(PARAM_NOTAGS, 'department', VALUE_OPTIONAL),
                            'institution' => new external_value(PARAM_NOTAGS, 'institution', VALUE_OPTIONAL),
                            'url'         => new external_value(PARAM_URL, 'URL of the user', VALUE_OPTIONAL),
                            'interests'   => new external_value(PARAM_TEXT, 'user interests (separated by commas)', VALUE_OPTIONAL),
                        	'tutorial'    => new external_value(PARAM_TEXT, 'Tutorials viewed by user', VALUE_OPTIONAL),
                            'notificationsettings' => new external_multiple_structure(
                                new external_single_structure(
                                    array(
                                        'type'  => new external_value(PARAM_ALPHANUMEXT, 'notification type'),
                                        'value' => new external_value(PARAM_TEXT, '2=always, 1=daily, 0=never')
                                    )
                                ), 'User email notification settings', VALUE_OPTIONAL),
                            //CBTEC End
                            'customfields' => new external_multiple_structure(
                                new external_single_structure(
                                    array(
                                        'type'  => new external_value(PARAM_ALPHANUMEXT, 'The name of the custom field'),
                                        'value' => new external_value(PARAM_TEXT, 'The value of the custom field'),
                                        'name' => new external_value(PARAM_TEXT, 'The name of the custom field'),
                                        'shortname' => new external_value(PARAM_TEXT, 'The shortname of the custom field - to be able to build the field class in the code'),
                                    )
                                ), 'User custom fields (also known as user profil fields)', VALUE_OPTIONAL),
                            'preferences' => new external_multiple_structure(
                                new external_single_structure(
                                    array(
                                        'type'  => new external_value(PARAM_ALPHANUMEXT, 'The name of the preference'),
                                        'value' => new external_value(PARAM_TEXT, 'The value of the preference')
                                    )
                                ), 'User preferences', VALUE_OPTIONAL),
                        )
                    )
                )
            )
        );
    }

    /**
     * Update users own profile
     *
     * @param array $users
     * @return null
     * @since Moodle 2.2
     */
    public static function update_users($users) {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot."/user/lib.php");
        require_once($CFG->dirroot."/tag/lib.php");
        require_once($CFG->dirroot."/user/profile/lib.php"); //required for customfields related function

        $params = self::validate_parameters(self::update_users_parameters(), array('users'=>$users));

        $transaction = $DB->start_delegated_transaction();
        foreach ($params['users'] as $user) {
            // ONLY ALLOW TO UPDATE SELF
            if ($USER->id != $user['id']) {
                // No
                continue;
            }

            user_update_user($user);
            if(!empty($user['interests'])) {
                tag_set('user', $user['id'], explode(',',trim($user['interests'])));
            }
            //update user custom fields
            if(!empty($user['customfields'])) {

                foreach($user['customfields'] as $customfield) {
                    $user["profile_field_".$customfield['type']] = $customfield['value']; //profile_save_data() saves profile file
                    $user["profile_field_".$customfield['shortname']] = $customfield['value']; //profile_save_data() saves profile file
                                                                                               //and custom field to be named profile_field_"shortname"
                }
                profile_save_data((object) $user);
            }

            //update tutorial
            if(!empty($user['tutorial'])) {
            	$DB->delete_records('monorail_data', array('itemid' => $user['id'], 'datakey' => 'tutorial'));

            	$tutorial = new stdClass();
            	$tutorial->datakey = 'tutorial';
            	$tutorial->itemid  = $user['id'];
            	$tutorial->value   = $user['tutorial'];
            	$tutorial->type	   = 'TEXT';
            	$tutorial->timemodified = time();

            	$DB->insert_record('monorail_data', $tutorial);
            }

            //update user email notification settings
            if (!empty($user['notificationsettings'])) {
                // convert to array
                $newsettings = array();
                foreach($user['notificationsettings'] as $setting) {
                    $newsettings[$setting['type']] = $setting['value'];
                }
                monorailfeed_user_email_settings($user['id'], $newsettings);
            }

            //preferences
            if (!empty($user['preferences'])) {
                foreach($user['preferences'] as $preference) {
                    set_user_preference($preference['type'], $preference['value'],$user['id']);
                }
            }
            if(!empty($user['lang'])){
                $USER->lang = $user['lang'];

                wscache_reset_by_dependency("user_language_" . $USER->id);
            }

            // Update public magento courses where user is the main teacher.

            if (isset($CFG->mage_api_url))
            {
                addBackgroundCall(function ()
                {
                    global $USER;

                    $api = magento_get_instance();

                    if ($api)
                    {
                        $api->begin();

                        if ($api->updateCourseTeacherInfo($USER->id))
                        {
                            self::update_public_profile_page();
                        }
                    }
                });
            }

            wscache_reset_by_dependency("user_info_" . $user["id"]);

            $userCourses = $DB->get_fieldset_sql("SELECT courseid FROM {monorail_course_data} WHERE mainteacher=?", array($user["id"]));

            foreach ($userCourses as $uc) {
                wscache_reset_by_dependency("course_info_" . $uc);
            }
        }

        $transaction->allow_commit();

        return null;
    }

    /**
     * Returns description of method result value
     *
     * @return null
     * @since Moodle 2.2
     */
    public static function update_users_returns() {
        return null;
    }

    public static function assign_roles_parameters() {
        return new external_function_parameters(
            array(
                'assignments' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'rolename'    => new external_value(PARAM_TEXT, 'Role to assign to the user - coursecreator,editingteacher,frontpage,
                                guest,manager,student,teacher,teachercopy,user,wuser'),
                            'userid'    => new external_value(PARAM_INT, 'The user that is going to be assigned'),
                            'context'   => new external_value(PARAM_TEXT, 'Context - course/module', VALUE_DEFAULT, 'course'),
                            'id' => new external_value(PARAM_INT, 'The course id or module id'),
                        )
                    )
                )
            )
        );
    }

    /**
     * Manual role assignments to users
     *
     * @param array $assignments An array of manual role assignment
     */
    public static function assign_roles($assignments) {
        global $DB, $CFG;

        $params = self::validate_parameters(self::assign_roles_parameters(), array('assignments'=>$assignments));
        $inputs = array();
        $rinputs = array();
        $studentroleid = $DB->get_field('role', 'id', array('shortname'=>'student'),MUST_EXIST);
        foreach ($params['assignments'] as $assignment) {
            // Ensure the current user is allowed to run this function in the enrolment context
            if($assignment['context'] == 'course'){
                $context = context_course::instance($assignment['id']);

                wscache_reset_by_dependency("course_info_" . $assignment["id"]);

            } else if ($assignment['context'] == 'module'){
                $context = context_module::instance($assignment['id']);
            } else {
                continue;
            }

            if($assignment['rolename'] == 'editingteacher' || $assignment['rolename'] == 'teacher') { //assigning teacher role, remove student role
                $rinput = array();
                $rinput['roleid'] = $studentroleid;
                $rinput['userid'] = $assignment['userid'];
                $rinput['contextid'] = $context->id;
                $rinputs[] = $rinput;
            }

            $roleid = $DB->get_field('role', 'id', array('shortname'=>$assignment['rolename']),MUST_EXIST);
            $input = array();
            $input['roleid'] = $roleid;
            $input['userid'] = $assignment['userid'];
            $input['contextid'] = $context->id;
            $inputs[] = $input;

            wscache_reset_by_dependency("course_info_" . $assignment['id']);
            wscache_reset_by_user($assignment['userid']);
        }
        require_once($CFG->dirroot . '/enrol/externallib.php');
        core_role_external::assign_roles($inputs);
        if(!empty($rinputs)) {
            core_role_external::unassign_roles($rinputs);
        }
    }

    /**
     * Returns description of method result value
     *
     * @return null
     */
    public static function assign_roles_returns() {
        return null;
    }


    public static function unassign_roles_parameters() {
        return new external_function_parameters(
            array(
                'assignments' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'rolename'    => new external_value(PARAM_TEXT, 'Role to assign to the user - coursecreator,editingteacher,frontpage,
                                guest,manager,student,teacher,teachercopy,user,wuser'),
                            'userid'    => new external_value(PARAM_INT, 'The user that is going to be assigned'),
                            'context'   => new external_value(PARAM_TEXT, 'Context - course/module', VALUE_DEFAULT, 'course'),
                            'id' => new external_value(PARAM_INT, 'The course id or module id'),
                        )
                    )
                )
            )
        );
    }

    /**
     * Manual role assignments to users
     *
     * @param array $assignments An array of manual role assignment
     */
    public static function unassign_roles($assignments) {
        global $DB, $CFG;

        $params = self::validate_parameters(self::unassign_roles_parameters(), array('assignments'=>$assignments));
        $inputs = array();
        $rinputs = array();
        $studentroleid = $DB->get_field('role', 'id', array('shortname'=>'student'),MUST_EXIST);
        foreach ($params['assignments'] as $assignment) {
            // Ensure the current user is allowed to run this function in the enrolment context
            if($assignment['context'] == 'course'){
                $context = context_course::instance($assignment['id']);

                wscache_reset_by_dependency("course_info_" . $assignment["id"]);

            } else if ($assignment['context'] == 'module'){
                $context = context_module::instance($assignment['id']);
            } else {
                continue;
            }

            if($assignment['rolename'] == 'editingteacher' || $assignment['rolename'] == 'teacher' || $assignment['rolename'] == "manager") { //unassigning teacher role, give student role
                $rinput = array();
                $rinput['roleid'] = $studentroleid;
                $rinput['userid'] = $assignment['userid'];
                $rinput['contextid'] = $context->id;
                $rinputs[] = $rinput;
            }

            $roleid = $DB->get_field('role', 'id', array('shortname'=>$assignment['rolename']),MUST_EXIST);
            $input = array();
            $input['roleid'] = $roleid;
            $input['userid'] = $assignment['userid'];
            $input['contextid'] = $context->id;
            $inputs[] = $input;
        }
        require_once($CFG->dirroot . '/enrol/externallib.php');
        core_role_external::unassign_roles($inputs);
        if(!empty($rinputs)) {
            core_role_external::assign_roles($rinputs);
        }

        // check if this user was the main teacher of the course
        if ($assignment['context'] == 'course' && $assignment['rolename'] == 'editingteacher') {
            $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$assignment['id']));
            if ($coursedata->mainteacher == $assignment['userid']) {
                // choose another main teacher
                try {
                    $teacher = $DB->get_records_sql("SELECT u.* FROM {user} AS u, {role} AS r, {role_assignments} AS a" .
                            " WHERE r.shortname='editingteacher' AND r.id=a.roleid AND a.userid=u.id AND a.contextid=? LIMIT 1",
                            array($context->id));
                    $teacher = reset($teacher);
                    $coursedata->mainteacher = $teacher->id;
                    $DB->update_record('monorail_course_data', $coursedata);
                } catch (Exception $ex) {
                    add_to_log($assignment['id'], 'monorailservices', 'externallib', 'unassign_roles', 'Error, failed to set new main teacher: '.$ex->getMessage());
                }
            }
        }
    }

    /**
     * Returns description of method result value
     *
     * @return null
     */
    public static function unassign_roles_returns() {
        return null;
    }


    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function enrol_users_parameters() {
        return new external_function_parameters(
            array(
                'enrolments' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'rolename'    => new external_value(PARAM_TEXT, 'Role to assign to the user - coursecreator,editingteacher,frontpage,
                                guest,manager,student,teacher,teachercopy,user,wuser'),
                            'inviteid' => new external_value(PARAM_TEXT, 'Invitation id sent to user', VALUE_OPTIONAL),
                            'token' => new external_value(PARAM_TEXT, 'Invitation token sent to user', VALUE_OPTIONAL),
                            'email' => new external_value(PARAM_EMAIL, 'Invitee email', VALUE_OPTIONAL),
                            'courseid' => new external_value(PARAM_INT, 'Course id'),
                            'cohortid' => new external_value(PARAM_INT, 'Cohort id',VALUE_OPTIONAL),
                            'userid' => new external_value(PARAM_INT, 'The user that is going to be enrolled'),
                            'timestart' => new external_value(PARAM_INT, 'Timestamp when the enrolment start', VALUE_OPTIONAL),
                            'timeend' => new external_value(PARAM_INT, 'Timestamp when the enrolment end', VALUE_OPTIONAL),
                            'suspend' => new external_value(PARAM_INT, 'set to 1 to suspend the enrolment', VALUE_OPTIONAL)
                        )
                    )
                ),
                'internal' => new external_value(PARAM_INT, 'If local or remote call', VALUE_DEFAULT, 0)
            )
        );
    }

    /**
     * Enrolment of users
     *
     * Function throw an exception at the first error encountered.
     * @param array $enrolments  An array of user enrolment
     * @since Moodle 2.2
     */
    public static function enrol_users($enrolments, $internal) {
        global $DB, $CFG, $USER;

        require_once($CFG->libdir . '/enrollib.php');
        $params = self::validate_parameters(self::enrol_users_parameters(),
            array('enrolments' => $enrolments, 'internal' => $internal));

        $transaction = $DB->start_delegated_transaction(); //rollback all enrolment if an error occurs
        //(except if the DB doesn't support it)
        //retrieve the manual enrolment plugin
        $enrol = enrol_get_plugin('manual');
        if (empty($enrol)) {
            throw new moodle_exception('manualpluginnotinstalled', 'enrol_manual');
        }
        $warnings = array();
        $result = array();
        require_once($CFG->dirroot . '/enrol/externallib.php');
        foreach ($params['enrolments'] as $enrolment) {
            $enrolment['roleid'] = $DB->get_field('role', 'id', array('shortname'=>$enrolment['rolename']),MUST_EXIST);
            $removeInvTok = false;
            if (isset($enrolment['inviteid']) && $enrolment["inviteid"]) {
                // check invitation code for permissions
                $invite = $DB->get_record('monorail_invites', array('code'=>$enrolment['inviteid']), '*', MUST_EXIST);
                if(!$invite->active){
                    //Invite expired !
                    $warning = array();
                    $warning['item'] = 'enrol';
                    $warning['itemid'] = $enrolment['inviteid'];
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'Invite has expired, cannot enrol';
                    $warnings[] = $warning;
                    continue;
                }
                if(!$internal && strcmp($invite->type,'course') == 0) {
                  //Check if token exists
                  if(!$DB->record_exists('monorail_invite_uniquetokens', array('token'=>$enrolment['token'], 'course' => $enrolment['courseid'],
                                                     'type'=>'course' , 'email' => $enrolment['email'], 'invitecode' => $enrolment['inviteid']))) {
                    $preventEnroll = true;
                    if($USER->id == $enrolment['userid']) {
                      //Self enrollment , if course cohort and usr is in cohort and self enrollment allowed!
                      $cohortid = $DB->get_field('monorail_cohort_courseadmins', 'cohortid', array('courseid'=> $enrolment['courseid']), IGNORE_MULTIPLE);
                      if($cohortid) {
                        if($DB->record_exists('cohort_members', array('userid'=> $USER->id, 'cohortid' => $cohortid))) {
                          //User can enroll
                          $cperm = monorail_get_course_perms($enrolment['courseid']);
                          if(($cperm['course_privacy'] == 2) && ($cperm['course_access'] == 3)) {
                            $preventEnroll = false;
                          }
                        }
                      }
                    }
                    if($preventEnroll) {
                      //Invite user token invalid !
                      $warning = array();
                      $warning['item'] = 'enrol';
                      $warning['itemid'] = $enrolment['inviteid'];
                      $warning['warningcode'] = '1';
                      $warning['message'] = 'User enrol token invalid, cannot enrol';
                      $warnings[] = $warning;
                      continue;
                   }
                  } else {
                    $removeInvTok = true;
                  }
                }
                if ($enrolment['cohortid']) {
                    require_once($CFG->dirroot . "/course/lib.php");
                    require_once($CFG->dirroot . "/lib/accesslib.php");
                    //If cohort id is set and invite is active check if user is admin or teacher to enrol
                    $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $enrolment['cohortid']));
                    if(!$auser) {
                        $admin = false;
                        $course = $DB->get_record('course', array('id'=>$enrolment['courseid']), '*', MUST_EXIST);
                        $context = context_course::instance($enrolment['courseid']);
                        try {
                            self::validate_context($context);
                            // We cannot check for course:delete since no one has that except admins
                            require_capability('moodle/course:create', $context);
                            // Check user is enrolled as teacher
                            $users = get_enrolled_users($context);
                            $teacher = false;
                            foreach ($users as $user) {
                                if ($user->id === $USER->id) {
                                    // get user role
                                    $roles = get_user_roles($context , $user->id);
                                    foreach ($roles as $role) {
                                        if ($role->name === 'Teacher') {
                                            $teacher = true;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            if (! $teacher) {
                                throw Exception();
                            }
                        } catch (Exception $e) {
                            $warning = array();
                            $warning['item'] = 'enrol';
                            $warning['itemid'] = $enrolment['cohortid'];
                            $warning['warningcode'] = '1';
                            $warning['message'] = 'User not admin or teacher for cohort, cannot enrol';
                            $warnings[] = $warning;
                            continue;
                        }
                    } else {
                        $admin = true;
                    }
                    if(!$admin && !$teacher) {
                        $warning = array();
                        $warning['item'] = 'enrol';
                        $warning['itemid'] = $enrolment['cohortid'];
                        $warning['warningcode'] = '1';
                        $warning['message'] = 'User not admin for cohort, cannot enrol';
                        $warnings[] = $warning;
                        continue;
                    }
                }
            } else {
                // logged in user must have permissions
                $context = get_context_instance(CONTEXT_COURSE, $enrolment['courseid']);

                //check that the user has the permission to manual enrol
                require_capability('enrol/manual:enrol', $context, $USER->id);

                //throw an exception if user is not able to assign the role
                $roles = get_assignable_roles($context,ROLENAME_ALIAS, false, $USER->id);
                if (!array_key_exists($enrolment['roleid'], $roles)) {
                    $warning = array();
                    $warning['item'] = 'enrol';
                    $warning['itemid'] = $USER->id;
                    $warning['warningcode'] = '1';
                    $warning['message'] = 'User does not have access to enrol user to course';
                    $warnings[] = $warning;
                    continue;
                }
            }
            $ucourses = core_enrol_external::get_users_courses($enrolment['userid'], array());
            $userenrolled = false;
            foreach($ucourses as $ucourse) {
                if($ucourse['id'] == $enrolment['courseid']) {
                    //User enrolled in course !
                    $context = context_course::instance($enrolment['courseid']);
                    if(!user_has_role_assignment($enrolment["userid"], $enrolment["roleid"], $context->id)) {
                        role_assign($enrolment["roleid"], $enrolment["userid"], $context->id);
                    }
                    $userenrolled = true;
                    break;
                }
            }

            if($userenrolled){
                continue;
            }

            //check manual enrolment plugin instance is enabled/exist
            $enrolinstances = enrol_get_instances($enrolment['courseid'], true);
            foreach ($enrolinstances as $courseenrolinstance) {
                if ($courseenrolinstance->enrol == "manual") {
                    $instance = $courseenrolinstance;
                    break;
                }
            }
            if (empty($instance)) {
                $warning = array();
                $warning['item'] = 'enrol';
                $warning['itemid'] = $enrolment['courseid'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'No enrollment plugin/instance';
                $warnings[] = $warning;
            }
            //check that the plugin accept enrolment (it should always the case, it's hard coded in the plugin)
            if (!$enrol->allow_enrol($instance)) {
                $warning = array();
                $warning['item'] = 'enrol';
                $warning['itemid'] = $enrolment['courseid'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Plugin does not support enrollment';
                $warnings[] = $warning;
            }

            //finally proceed the enrolment
            $enrolment['timestart'] = isset($enrolment['timestart']) ? $enrolment['timestart'] : time();
            $enrolment['timeend'] = isset($enrolment['timeend']) ? $enrolment['timeend'] : 0;
            $enrolment['status'] = (isset($enrolment['suspend']) && !empty($enrolment['suspend'])) ?
            ENROL_USER_SUSPENDED : ENROL_USER_ACTIVE;
            $enrol->enrol_user($instance, $enrolment['userid'], $enrolment['roleid'],
                $enrolment['timestart'], $enrolment['timeend'], $enrolment['status']);

            //User enrolled , remove any one time token token
            if($removeInvTok) {
              $DB->delete_records('monorail_invite_uniquetokens', array('token'=>$enrolment['token'], 'course' => $enrolment['courseid'],
                                        'type'=>'course' , 'email' => $enrolment['email'], 'invitecode' => $enrolment['inviteid']));
            }

            // check if this invite has cohort group id... Actually, we
            // check if this user email has invites with groups...
            $tagsForUser = $DB->get_fieldset_sql("SELECT usertag FROM {monorail_invite_users} WHERE email=? AND usertag",
                array($enrolment["email"]));

            foreach ($tagsForUser as $userTag) {
                if (!$DB->get_field_sql("SELECT id FROM {monorail_usertag_user} WHERE tagid=? AND userid=?", array($userTag, $enrolment["userid"]))) {
                    $DB->execute("INSERT INTO {monorail_usertag_user} (tagid, userid) VALUES (?, ?)", array($userTag, $enrolment["userid"]));
                }
            }

            //Socially share this info
            if($USER->id == $enrolment['userid']) {
              social_share_course_enroll($enrolment["courseid"], $enrolment['userid']);
            }

            wscache_reset_by_dependency("course_info_" . $enrolment["courseid"]);
            wscache_reset_by_dependency("user_courses_" . $enrolment["userid"]);

            if (isset($CFG->mage_api_url) && self::is_course_public($enrolment["courseid"]))
            {
                addBackgroundCall(function () use ($enrolment)
                {
                    $api = magento_get_instance();

                    if ($api)
                    {
                        $api->begin();
                        $api->updateParticipants($enrolment['courseid']);
                    }
                });
            }
        }

        $transaction->allow_commit();
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return null
     * @since Moodle 2.2
     */
    public static function enrol_users_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }


    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function supported_langs_parameters() {
    	return new external_function_parameters(array());
    }

    /**
     * Categories for courses
     *
     * @since Moodle 2.2
     */
    public static function supported_langs() {
    	global $DB;

    	$result = array();
    	$languages = array();
    	$langs = get_string_manager()->get_list_of_translations();

    	foreach($langs as $key => $value){
    		$lng = array();
    		$lng['key'] = $key;
    		$lng['value'] = $value;
    		$languages[] = $lng;
    	}

    	$result['languages'] = $languages;
    	return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return $categories
     *
     */
    public static function supported_langs_returns() {
    	return new external_single_structure(
    			array('languages' => new external_multiple_structure (
    					new external_single_structure(
    							array(
    									'key' => new external_value(PARAM_TEXT, 'language key'),
    									'value' => new external_value(PARAM_TEXT, 'Display name')
    							)), 'list of language name and id')
    			)
    	);
    }

//===========================================/
    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function get_countries_parameters() {
    	return new external_function_parameters(array());
    }

    /**
     * Categories for courses
     *
     * @since Moodle 2.2
     */
    public static function get_countries() {
    	global $DB, $CFG;

    	$result = array();
    	$countries = array();
    	$scountries =  get_string_manager()->get_list_of_countries();

    	foreach($scountries as $key => $value){
    		$country = array();
    		$country['key'] = $key;
    		$country['value'] = $value;
            $country["vat"] = array_key_exists($key, $CFG->vat_percent) ? $CFG->vat_percent[$key] : 0;

    		$countries[] = $country;
    	}

    	$result['countries'] = $countries;
    	return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return $categories
     *
     */
    public static function get_countries_returns() {
    	return new external_single_structure(
    	    array('countries' => new external_multiple_structure (
    		new external_single_structure(
    			array(
    		'key' => new external_value(PARAM_TEXT, 'language key'),
    		'value' => new external_value(PARAM_TEXT, 'Display name'),
    		'vat' => new external_value(PARAM_FLOAT, 'EU VAT percentage')
    		)), 'list of countries name and id')
    	)
    	);
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     */
    public static function unenrol_users_parameters() {
        return new external_function_parameters(
            array(
                'enrolments' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'courseid' => new external_value(PARAM_INT, 'Course Id'),
                            'userid' => new external_value(PARAM_INT, 'The user that is going to be enrolled'),
                            'suspend' => new external_value(PARAM_INT, 'set to 1 to suspend the enrolment', VALUE_DEFAULT, 0)
                        )
                    )
                )
            )
        );
    }

    /**
     * Un Enrolment of users
     *
     * @param array $enrolments  An array of user enrolment
     */
    public static function unenrol_users($enrolments) {
        global $DB, $CFG, $USER;

        require_once($CFG->libdir . '/enrollib.php');
        $params = self::validate_parameters(self::unenrol_users_parameters(),
            array('enrolments' => $enrolments));

        $transaction = $DB->start_delegated_transaction(); //rollback all enrolment if an error occurs
        //(except if the DB doesn't support it)
        //retrieve the manual enrolment plugin
        $enrol = enrol_get_plugin('manual');
        if (empty($enrol)) {
            throw new moodle_exception('manualpluginnotinstalled', 'enrol_manual');
        }
        $warnings = array();
        $result = array();
        foreach ($params['enrolments'] as $enrolment) {
            //Ok fetch the users , for each user the submission info and the grade
            require_once($CFG->dirroot . '/enrol/externallib.php');
            $ucourses = core_enrol_external::get_users_courses($enrolment['userid'], array());
            $userenrolled = false;
            foreach($ucourses as $ucourse) {
                if($ucourse['id'] == $enrolment['courseid']) {
                    //User enrolled in course !
                    $userenrolled = true;
                    break;
                }
            }
            if(!$userenrolled){
                continue;
            }
            //If course is public and published in eliademy catalog unenroll should not be possible
            // ...AND course has to be paid
            $courseInfo = monorail_get_course_details($enrolment['courseid']);
            if ($courseInfo->course_privacy == 1 && $courseInfo->course_access == 3 && ((float) $courseInfo->price) > 0) {
                $warning = array();
                $warning['item'] = 'uenrol';
                $warning['itemid'] = $enrolment['courseid'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Public course - user cannot be enrolled';
                $warnings[] = $warning;
                continue;
            }

            // Ensure the current user is allowed to run this function in the enrolment context

            $context = get_context_instance(CONTEXT_COURSE, $enrolment['courseid']);

            //check that the user has the permission to manual enrol
            require_capability('enrol/manual:unenrol', $context);

            //check manual enrolment plugin instance is enabled/exist
            $unenrolinstances = enrol_get_instances($enrolment['courseid'], true);
            foreach ($unenrolinstances as $unenrolinstance) {
                if ($unenrolinstance->enrol == "manual") {
                    $instance = $unenrolinstance;
                    break;
                }
            }
            if (empty($instance)) {
                $warning = array();
                $warning['item'] = 'enrol';
                $warning['itemid'] = $enrolment['courseid'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'No unenrollment plugin/instance';
                $warnings[] = $warning;
            }

            //check that the plugin accept enrolment (it should always the case, it's hard coded in the plugin)
            if (!$enrol->allow_unenrol($instance)) {
                $warning = array();
                $warning['item'] = 'enrol';
                $warning['itemid'] = $enrolment['inviteid'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Plugin does not support unenrollment';
                $warnings[] = $warning;
            }


            //finally proceed the unenrolment
            $enrol->unenrol_user($instance, $enrolment['userid']);

            // check if this user was the main teacher of the course
            $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$enrolment['courseid']));
            if ($coursedata->mainteacher == $enrolment['userid']) {
                // choose another main teacher
                try {
                    $teacher = $DB->get_records_sql("SELECT u.* FROM {user} AS u, {role} AS r, {role_assignments} AS a" .
                            " WHERE r.shortname='editingteacher' AND r.id=a.roleid AND a.userid=u.id AND a.contextid=? LIMIT 1",
                            array($context->id));
                    $teacher = reset($teacher);
                    $coursedata->mainteacher = $teacher->id;
                    $DB->update_record('monorail_course_data', $coursedata);
                } catch (Exception $ex) {
                    add_to_log($enrolment['courseid'], 'monorailservices', 'externallib', 'unenrol_users', 'Error, failed to set new main teacher: '.$ex->getMessage());
                }
            }

            wscache_reset_by_dependency("course_info_" . $enrolment['courseid']);
            wscache_reset_by_user($enrolment['userid']);
        }
        $transaction->allow_commit();
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return null
     * @since Moodle 2.2
     */
    public static function unenrol_users_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function course_categories_parameters() {
        return new external_function_parameters(array());
    }

    /**
     * Categories for courses
     *
     * @since Moodle 2.2
     */
    public static function course_categories() {
        global $DB, $USER;

        $result = array();
        $categories = array();
        $allcategories = $DB->get_records('course_categories', null,
            'id', 'id, name, description, idnumber');

        $ctg = array();
        $ctg['id'] = 0;
        $ctg['name'] = get_string('course_category_select','theme_monorail');
        $ctg['stock_backgrounds'] = array();
        $ctg['description'] = "";
        $ctg['idnumber'] = 0;
        $categories[] = $ctg;

        foreach($allcategories as $category){
            $ctg = array();
            $ctg['id'] 	= $category->id;
            switch($category->id) {
                case 1:
                     $ctg['name'] = get_string('course_category_social_science','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' => "hotel1.jpg", 'id' => 1));
                     break;
                case 2:
                     $ctg['name'] = get_string('course_category_arts_design','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"art_and_design1.jpg", 'id' => 8), array( 'file' => "art_and_design2.jpg", 'id' => 9));
                     break;
                case 3:
                     $ctg['name'] = get_string('course_category_business_law','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file'=>"business1.jpg", 'id'=> 10), array('file' => "business2.jpg", 'id' => 11), array('file'=> "business3.jpg", 'id' => 12));
                     break;
                case 4:
                case 5:
                     $ctg['name'] = get_string('course_category_technology','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"engineering1.jpg", 'id' =>  13), array( 'file' => "engineering2.jpg", 'id' => 14), array('file' => "engineering3.jpg", 'id' => 15));
                     break;
                case 7:
                     $ctg['name'] = get_string('course_category_education','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"education1.jpg", 'id' =>  18));
                     break;
                case 9:
                     $ctg['name'] = get_string('course_category_life_science','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"medicine1.jpg", 'id' =>  20), array( 'file' => "medicine2.jpg", 'id' => 21));
                     break;
                case 10:
                     $ctg['name'] = get_string('course_category_physical_sciences','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"nature1.jpg", 'id' =>  2), array( 'file' => "nature2.jpg", 'id' => 3));
                     break;
                case 11:
                     $ctg['name'] = get_string('course_category_philosophy','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"philosophy1.jpg", 'id' =>  4));
                     break;
                case 12:
                     $ctg['name'] = get_string('course_category_humanities','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"history1.jpg", 'id' =>  5));
                     break;
                case 13:
                     $ctg['name'] = get_string('course_category_languages','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"languages1.jpg", 'id' =>  6));
                     break;
                case 15:
                     $ctg['name'] = get_string('course_category_other','theme_monorail');
                     $ctg['stock_backgrounds'] = array(array('file' =>"other.jpg", 'id' =>  7));
                     break;
                default:
                    $ctg['name'] = $category->name;
                    $ctg['stock_backgrounds'] = array();
                    break;
            }
            $ctg['description'] = $category->description;
            $ctg['idnumber'] 	= $category->idnumber;
            $categories[] = $ctg;
        }

        $result['categories'] = $categories;

        wscache_add_cache_dependency("user_language_" . $USER->id);

        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return $categories
     *
     */
    public static function course_categories_returns() {
        return new external_single_structure(
            array('categories' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'category id'),
                        'name' => new external_value(PARAM_TEXT, 'Category Name'),
                        'stock_backgrounds' => new external_multiple_structure (
                             new external_single_structure(
                               array(
                                        'file' => new external_value(PARAM_TEXT, 'File Name'),
                                        'id' => new external_value(PARAM_INT, 'File id')
                                    )),'Stock backgrounds data',VALUE_DEFAULT,array()),
                        'description' => new external_value(PARAM_TEXT, 'Category description'),
                    	'idnumber' => new external_value(PARAM_INT, 'Customized ID Number'),
                    )), 'list of category name and ids')
            )
        );
    }
    //Core_user modified API END

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function course_set_enrollment_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID'),
                'status' => new external_value(PARAM_INT, 'Enrollment status')
            )
        );
    }

    /**
     * Set course enrollment status
     */
    public static function course_set_enrollment($courseid, $status) {
        global $DB, $CFG;

        $params = self::validate_parameters(self::course_set_enrollment_parameters(),
            array('courseid' => $courseid, 'status' => $status));

        $warnings = array();
        $result = array();

        // check access to course
        $context = context_course::instance($courseid);
        try {
            self::validate_context($context);
            require_capability('moodle/course:update', $context);
        } catch (Exception $e) {
            $warning = array();
            $warning['item'] = 'course';
            $warning['itemid'] = $courseid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'No access rights in course context to set enrollment';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }

        $courseinvite = monorail_get_or_create_invitation('course', $courseid);

        if (! monorail_set_invite_status($courseinvite->code, $status)) {
            $warning = array();
            $warning['item'] = 'course_set_enrollment';
            $warning['itemid'] = $courseid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Failed to set course enrollment status';
            $warnings[] = $warning;
        }
        else
        {
            wscache_reset_by_dependency("course_info_" . $courseid);

            if (isset($CFG->mage_api_url))
            {
                addBackgroundCall(function () use ($courseid, $status)
                {
                    global $DB;

                    $api = magento_get_instance();

                    if ($api)
                    {
                        $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?",
                            array($courseid));

                        $catalogItem = $api->getProductBySku($courseData->code);

                        if (!empty($catalogItem))
                        {
                            $api->begin();
                            $api->updateProduct($courseData->code, array(
                                "additional_attributes" => array("single_data" => array("enrollment_active" => $status ? 1 : 0))));
                        }
                    }
                });
            }
        }

        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return null
     * @since Moodle 2.2
     */
    public static function course_set_enrollment_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Get sent invitations parameters
     */
    public static function get_sent_invitations_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'Course ID/cohort id'),
                'code' => new external_value(PARAM_TEXT, 'Invitation code'),
                'type' => new external_value(PARAM_TEXT, 'Invitation type cohort/course', VALUE_DEFAULT,'course')
            )
        );
    }

    /**
     * Get sent invitations
     * We need courseid only to check access
     */
    public static function get_sent_invitations($id, $code, $type) {
        global $DB, $CFG;

        $params = self::validate_parameters(self::get_sent_invitations_parameters(),
            array('id' => $id, 'code' => $code, 'type'=>$type));

        $warnings = array();
        $result = array();
        $courseinvite = monorail_get_invitation($code);

        if($type == 'course') {
            // check access to course
            $context = context_course::instance($id);
            try {
                self::validate_context($context);
                require_capability('moodle/course:update', $context);
            } catch (Exception $e) {
                return $result;
            }
        }

        $invites = monorail_get_invited_emails($code);

        foreach ($invites as $invite)
        {
            $link = "";
            $tok = $DB->get_field_sql("SELECT token FROM {monorail_invite_uniquetokens} " .
                "WHERE type=? AND course=? AND email=? AND invitecode=?",
                    array($type, $id, $invite->email, $code));

            if ($tok) {
                $link = $CFG->magic_ui_root . '/invitation/' . $code . '/' . $invite->email . '/enroll/' . $tok;
            } else if ($type == "course") {
                continue;
            }

            $item = array(
                'id' => $invite->id,
                'email' => $invite->email,
                'invitedby' => $invite->invitedby,
                'invitedwhen' => $invite->timemodified,
                'status' => $invite->status,
                'link' => $link);

            $result[] = $item;
        }

        return $result;
    }

    /**
     * Get sent invitations returns
     */
    public static function get_sent_invitations_returns() {
        return new external_multiple_structure(
            new external_single_structure (
                array(
                    'id' => new external_value(PARAM_INT, 'Invite id', VALUE_OPTIONAL),
                    'email' => new external_value(PARAM_TEXT, 'an email', VALUE_OPTIONAL),
                    'invitedby' => new external_value(PARAM_INT, 'invited by userid', VALUE_OPTIONAL),
                    'invitedwhen' => new external_value(PARAM_INT, 'invited when timestamp', VALUE_OPTIONAL),
                    'status' => new external_value(PARAM_INT, 'Invitation status (0 - no data, 1 - sending, 2 - sent, 3 - delivered, 4 - failed)', VALUE_DEFAULT, 0),
                    'link' => new external_value(PARAM_TEXT, 'invitation link', VALUE_OPTIONAL)
                )
            )
        );
    }

    /**
     * Invite send parameters
     */
    public static function invite_send_parameters() {
        return new external_function_parameters(
            array(
                'id' => new external_value(PARAM_INT, 'Course ID/Cohort Id'),
                'code' => new external_value(PARAM_TEXT, 'Invitation code'),
                'email' => new external_value(PARAM_TEXT, 'Email to send to'),
                'resend' => new external_value(PARAM_INT, 'Resend invite', VALUE_DEFAULT, 0),
                'summary' => new external_value(PARAM_RAW, 'Summary text', VALUE_DEFAULT,''),
                'userTag' => new external_value(PARAM_INT, 'Default user group', VALUE_DEFAULT, 0),
                'userRole' => new external_value(PARAM_INT, 'Default user role', VALUE_DEFAULT, 0)
            )
        );
    }
    
    /**
     * Verify captcha
     */
    public static function verify_captcha($id, $challenge, $response) {
        global $CFG;
        require_once($CFG->libdir . '/filelib.php');
        $curl = new curl();

        //verifying the response
        $verificationstatus = 0;
        try {
            $params = array();
            $verifiationUrl = "http://verify.solvemedia.com/papi/verify";
            $params['response'] = $response;
            $params['challenge'] = $challenge;
            $params['privatekey'] = $CFG->solvemedia_verification_key;
            $params['remoteip'] = $_SERVER['REMOTE_ADDR'];
            $postreturnvalues = $curl->post($verifiationUrl, $params);
            $responses = explode("\n", $postreturnvalues);
            if ($responses[0] == "true") {
                $verificationstatus = 1;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
        }

        $result = array();
        $result['success'] = $verificationstatus;
        return $result;
    }

    /**
     * Send an invitation to an email
     */
    public static function invite_send($id, $code, $email, $resend, $summary, $userTag = 0, $userRole = 0) {
        global $DB, $USER;

        $params = self::validate_parameters(self::invite_send_parameters(),
            array('id' => $id, 'code' => $code, 'email' => $email, 'resend' => $resend, 'userTag' => $userTag, 'userRole' => $userRole));

        $warnings = array();
        $result = array();

        $summary = fix_utf8($summary);
        $summary = nl2br(clean_text($summary, FORMAT_HTML));
        if($code == 'eliademy') {
           //Its a general invite to friends to checkout eliademy
           // The id here corresponds to user id check if user exists
           $user = $DB->get_record('user', array('id' => $id));
           if($user) {
               //Need to send eliademy invite
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  $subject = monorail_template_compile(get_string('email_subject_eliademy_invite','theme_monorail'),
                                              array('username' => fullname($USER)));

                  $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
                  'body' => monorail_template_compile(monorail_get_template('mail/eliademyinvite'), array(
                                             'invite_message' => $summary, 'username' => fullname($USER))),
                  'title' => $subject,
                  'footer_additional_link' => '',
                  'block_header' => ''
                 ));

                 monorail_send_system_email_adv($email, $subject, $template_content, null, 'invitation', true, null, $USER->email, fullname($USER));
                } else {
                 $warning = array();
                 $warning['item'] = 'invite_send - '.$email;
                 $warning['itemid'] = $id;
                 $warning['warningcode'] = '1';
                 $warning['message'] = 'Failed to send invitation , invalid email addrsss';
                 $warnings[] = $warning;
                }
           }
           $result['warnings'] = $warnings;
           return $result;
        }
        $invite = monorail_get_invitation($code);
        $error = false;
        $errorstr = '';
        $errorcode = '1';
        $cohortid = 0;

        if ($invite->type == "cohort" && !$invite->active) {
            // Invites to cohorts should always be active.
            $invite->active = true;
            $DB->update_record("monorail_invites", $invite);
        }

        $inviteduser = $DB->get_record('monorail_invite_users', array('monorailinvitesid' => $invite->id, 'email' => strtolower($email)));
        if($inviteduser && !$resend && ($invite->type != 'course')) {
            $error = true;
            $errorstr = 'User has already been sent invitation';
        }

       //Check if user exists in eliademy and if he is in suspended state, if suspended do not send any invite
        $iuser = $DB->get_record('user', array('email'=>strtolower($email)));
        if($iuser && $iuser->suspended) {
            $error = true;
            $errorstr = 'User has been suspended in eliademy!';
        }

        if ($invite->targetid != $id || $invite->active == 0) {
            $error = true;
            $errorstr = 'Invitation code is invalid or invitations are closed for this '. $invite->type;
        }

        if (!$error)
        {
            if ($invite->type == 'course') {
                //Check if course belongs to cohort
                $ccourse = $DB->get_records('monorail_cohort_courseadmins', array('courseid' => $id));
                if($ccourse) { //Course belongs to cohort
                    $cohortid = $ccourse->cohortid;
                }
                // check access to course
                $context = context_course::instance($id);
                try {
                    self::validate_context($context);
                    require_capability('moodle/course:update', $context);
                    if($iuser) {
                        if (is_enrolled($context, $iuser->id)) {
                            $error = true;
                            $errorstr = 'Member is already enrolled to course, ignore send invite ';
                        }
                    }
                } catch (Exception $e) {
                    $error = true;
                    $errorstr = 'No access rights in course context to send invitations';
                }
            } else if ($invite->type == 'cohort') {
                $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $id));
                if(!$auser) {
                    $error = true;
                    $errorstr = 'No access rights in cohort context to send invitations';
                }
                $cohortid = $id;
                //Check if user is already member of cohort
                $cohortuser = $DB->count_records("cohort_members", array('userid'=>$iuser->id ));
                if($cohortuser > 0) {
                    $error = true;
                    $errorcode = '2';
                    $errorstr = $email;
                }
            } else {
                $error = true;
                $errorstr = 'Only allowed to send cohort/course invitations';
            }
        }

        if(!$error && ($cohortid > 0)) { //Either if course belongs to cohort  or cohort invite
            if($iuser) {
                //Check if user is suspended from cohort, if yes do not send invite
                $suser = $DB->get_record('monorail_cohort_susers', array('userid'=>$iuser->id, 'cohortid' => $cohortid));
                if($suser) {
                    $error = true;
                    $errorstr = 'Member is a suspended member for cohort, not sending invite';
                }
            }
        }

        if ($error) {
            $error = true;
            $warning = array();
            $warning['item'] = $invite->type;
            $warning['itemid'] = $id;
            $warning['warningcode'] = $errorcode;
            $warning['message'] = $errorstr;
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }

        if ($resend && !$userRole && $inviteduser) {
            $userRole = $inviteduser->userrole;
        }

        if (!monorail_send_invitation($invite, strtolower($email), $summary, $userTag, $userRole)) {
            $warning = array();
            $warning['item'] = 'invite_send';
            $warning['itemid'] = $id;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Failed to send invitation';
            $warnings[] = $warning;
        }
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return null
     * @since Moodle 2.2
     */
    public static function invite_send_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function create_courses_parameters() {
        $courseconfig = get_config('moodlecourse'); //needed for many default values
        return new external_function_parameters(
            array(
                'courses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'fullname' => new external_value(PARAM_RAW, 'full name', VALUE_OPTIONAL),
                            'shortname' => new external_value(PARAM_TEXT, 'course short name', VALUE_OPTIONAL),
                            'categoryid' => new external_value(PARAM_INT, 'category id', VALUE_OPTIONAL),
                            'idnumber' => new external_value(PARAM_TEXT, 'id number', VALUE_OPTIONAL),
                            'cohortid' => new external_value(PARAM_INT, 'cohortid number', VALUE_OPTIONAL),
                            'summary' => new external_value(PARAM_RAW, 'summary', VALUE_OPTIONAL),
                            'format' => new external_value(PARAM_PLUGIN,
                                'course format: weeks, topics, social, site,..',
                                VALUE_DEFAULT, $courseconfig->format),
                            'showgrades' => new external_value(PARAM_INT,
                                '1 if grades are shown, otherwise 0', VALUE_OPTIONAL,
                                $courseconfig->showgrades),
                            'newsitems' => new external_value(PARAM_INT,
                                'number of recent items appearing on the course page',
                                VALUE_DEFAULT, $courseconfig->newsitems),
                            'startdate' => new external_value(PARAM_INT,
                                'timestamp when the course starts', VALUE_OPTIONAL),
                            'enddate' => new external_value(PARAM_INT,
                                'timestamp when the course ends', VALUE_OPTIONAL),
                            'numsections' => new external_value(PARAM_INT, 'number of weeks/topics',
                                VALUE_OPTIONAL),
                            'maxbytes' => new external_value(PARAM_INT,
                                'largest size of file that can be uploaded into the course',
                                VALUE_DEFAULT, $courseconfig->maxbytes),
                            'showreports' => new external_value(PARAM_INT,
                                'are activity report shown (yes = 1, no =0)', VALUE_OPTIONAL,
                                $courseconfig->showreports),
                            'hiddensections' => new external_value(PARAM_INT,
                                'How the hidden sections in the course are displayed to students',
                                VALUE_DEFAULT, $courseconfig->hiddensections),
                            'groupmode' => new external_value(PARAM_INT, 'no group, separate, visible',
                                VALUE_DEFAULT, $courseconfig->groupmode),
                            'groupmodeforce' => new external_value(PARAM_INT, '1: yes, 0: no',
                                VALUE_DEFAULT, $courseconfig->groupmodeforce),
                            'defaultgroupingid' => new external_value(PARAM_INT, 'default grouping id',
                                VALUE_DEFAULT, 0),
                            'coursebackground' => new external_value(PARAM_TEXT,
                                'course backgrounf file name', VALUE_OPTIONAL),
                            'course_privacy' => new external_value(PARAM_INT, 'is course public, 0=no, 1=yes', VALUE_OPTIONAL),
                            'course_access' => new external_value(PARAM_INT, 'course access draft=0, invite=1, open=3', VALUE_OPTIONAL),
                            'course_logo' => new external_value(PARAM_TEXT, 'course logo URL', VALUE_OPTIONAL),
                            'mainteacher' => new external_value(PARAM_INT, 'Main teacher for course page view', VALUE_OPTIONAL),
                            "cert_enabled" => new external_value(PARAM_INT, "Certificates enabled for course", VALUE_OPTIONAL),
                            "language" => new external_value(PARAM_TEXT, "Course language", VALUE_OPTIONAL),
                            "course_country" => new external_value(PARAM_TEXT, "Course course_country", VALUE_OPTIONAL),
                            "keywords" => new external_value(PARAM_TEXT, "Course keywords", VALUE_OPTIONAL),
                            "licence" => new external_value(PARAM_INT, "Course licence", VALUE_OPTIONAL),
                            "participants_hidden" => new external_value(PARAM_TEXT, "Participants hidden", VALUE_OPTIONAL)
                        )
                    ), 'courses to update'
                )
            )
        );
    }

    /**
     * Create  courses
     *
     * @param array $courses
     * @return array courses (id and shortname only)
     * @since Moodle 2.2
     */
    public static function create_courses($courses) {
        global $CFG, $DB;
        require_once($CFG->dirroot . "/course/lib.php");

        $params = self::validate_parameters(self::create_courses_parameters(),
                        array('courses' => $courses));

        $availablethemes = get_plugin_list('theme');
        $availablelangs = get_string_manager()->get_list_of_translations();

        $transaction = $DB->start_delegated_transaction();

        foreach ($params['courses'] as $course) {

            // Ensure the current user is allowed to run this function
            $context = get_context_instance(CONTEXT_COURSECAT, $course['categoryid']);
            try {
                self::validate_context($context);
            } catch (Exception $e) {
                $exceptionparam = new stdClass();
                $exceptionparam->message = $e->getMessage();
                $exceptionparam->catid = $course['categoryid'];
                throw new moodle_exception('errorcatcontextnotvalid', 'webservice', '', $exceptionparam);
            }
            require_capability('moodle/course:create', $context);

            // Make sure lang is valid
            if (array_key_exists('lang', $course) and empty($availablelangs[$course['lang']])) {
                throw new moodle_exception('errorinvalidparam', 'webservice', '', 'lang');
            }

            // Make sure theme is valid
            if (array_key_exists('forcetheme', $course)) {
                if (!empty($CFG->allowcoursethemes)) {
                    if (empty($availablethemes[$course['forcetheme']])) {
                        throw new moodle_exception('errorinvalidparam', 'webservice', '', 'forcetheme');
                    } else {
                        $course['theme'] = $course['forcetheme'];
                    }
                }
            }

            if (array_key_exists('fullname', $course)) {
              $course['fullname'] = clean_param($course['fullname'], PARAM_TEXT);
            }
            //force visibility if ws user doesn't have the permission to set it
            $category = $DB->get_record('course_categories', array('id' => $course['categoryid']));
            if (!has_capability('moodle/course:visibility', $context)) {
                $course['visible'] = $category->visible;
            }

            //set default value for completion
            $course['enablecompletion'] = 0;
            $course['completionstartonenrol'] = 0;

            $course['category'] = $course['categoryid'];

            // Summary format.
            $course['summaryformat'] = external_validate_format(1);

            // Create a course without shortname, then save new shortname later
            $shortname = $course['shortname'];
            $course['shortname'] = '';

            if (!array_key_exists('startdate', $course)) {
                $course['startdate'] = time();
            }

            //Note: create_course() core function check shortname, idnumber, category
            $course['id'] = create_course((object) $course)->id;


            // Update course background to database
            if (isset($course['coursebackground'])) {
                try {
                    $DB->delete_records('monorail_data', array('type'=>'TEXT', 'itemid'=>$course['id'], 'datakey'=>'coursebackground'));
                    $data = new stdClass();
                    $data->type         = 'TEXT';
                    $data->itemid       = $course['id'];
                    $data->datakey      = 'coursebackground';
                    $data->value        = $course['coursebackground'];
                    $data->timemodified = time();
                    $DB->insert_record('monorail_data', $data);
                } catch (Exception $e) {
                }
            }

            // automatically enrol current user to course as teacher
            require_once($CFG->libdir . '/enrollib.php');
            global $USER;
            $enrol = enrol_get_plugin('manual');
            $enrolinstances = enrol_get_instances($course['id'], true);
            foreach ($enrolinstances as $courseenrolinstance) {
                if ($courseenrolinstance->enrol == "manual") {
                    $instance = $courseenrolinstance;
                    break;
                }
            }
            $roleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
            //finally proceed the enrolment
            $enrol->enrol_user($instance, $USER->id, $roleid, time(), 0, ENROL_USER_ACTIVE);
            // No need to update participant count for newly created course
            // - it can't be moved to magento without saving again.

            // delete default section
            $DB->delete_records('course_sections', array('course'=>$course['id']));

            // Create a unique code for the course
            $codeData = new stdClass();
            $codeData->courseid = $course["id"];
            do
            {
                $codeData->code = substr(md5($codeData->courseid . rand()), 0, 10);
            }
            while ($DB->record_exists("monorail_course_data", array("code" => $codeData->code)));
            $codeData->enddate = (isset($course['enddate'])) ? $course['enddate'] : 0;
            $codeData->status = 1;
            $codeData->mainteacher = $USER->id;
            $codeData->timemodified = time();
            $codeData->modifiedby = $USER->id;
            if (isset($course["licence"]) && $course["licence"]) {
                $codeData->licence = $course['licence'];
            }
            $DB->insert_record('monorail_course_data', $codeData);

            // Update shortname
            $course['shortname'] = $codeData->code;
            update_course((object) $course);

            $participantUsers = array();
            $participantUsers[] = $USER->id;

            $resultcourses[] = array('id' => $course['id'], 'shortname' => $course['shortname'], 'code' => $codeData->code);
            // Check if user cohort id is specified then course by default belongs to cohort
            if (array_key_exists('cohortid', $course)) {
                //The user who created the course is already a teacher for the course
                //Inserting the cohort admin as the course manager
                $ausers = $DB->get_records_sql("select mcau.userid as userid, mu.deleted as deleted, mu.suspended as suspended from mdl_monorail_cohort_admin_users as mcau inner join mdl_user as mu on mu.id=mcau.userid where mcau.cohortid=?", array($course['cohortid']));
                $auserid = 0;
                define("NEW_COHORT_COURSE", $course["id"]);
                foreach($ausers as $auser) {
                    if ($auser->deleted || $auser->suspend) {
                        continue;
                    }
                    //Enrol admin user as course manager
                    // XXX: very inefficient...
                    $mroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);
                    $enrol->enrol_user($instance, $auser->userid, $mroleid, time(), 0, ENROL_USER_ACTIVE);
                    $participantUsers[] = $auser->userid;
                    //Require one particular admin userid to associate course with cohort
                    if(($auserid == 0) || ($auser->userid == $USER->id)) {
                        $auserid = $auser->userid;
                    }
                }
                $record = new stdClass;
                $record->cohortid = $course["cohortid"];
                $record->courseid = $course["id"];
                $record->adminid = $auserid;
                $DB->insert_record('monorail_cohort_courseadmins', $record);
            }

            if (isset($course["cert_enabled"]))
            {
                monorail_data('BOOLEAN', $course['id'], 'cert_enabled', $course['cert_enabled'], true);
            }

            if (isset($course["language"])) {
                monorail_data('TEXT', $course['id'], 'language', $course['language'], true);
            }

            if (isset($course["course_country"])) {
                monorail_data('TEXT', $course['id'], 'course_country', $course['course_country'], true);
            }

            if (isset($course["keywords"]))
            {
                monorail_data('TEXT', $course['id'], 'keywords', $course['keywords'], true);
            }

            if (isset($course["participants_hidden"]))
            {
                monorail_data('INTEGER', $course['id'], 'participants_hidden', $course['participants_hidden'], true);
            }

            if (isset($course["course_access"]) && isset($course["course_privacy"]))
            {
                monorail_update_course_perms($course['id'], $course['course_access'], $course['course_privacy']);
            }
            else
            {
                //BY DEFAULT COURSES ARE PUBLIC INVITATIONA ONLY!! CHANGE WHEN NEEDED
                if (array_key_exists('cohortid', $course)) {
                    $cinfo = $DB->get_record('monorail_cohort_info', array('cohortid' => $course["cohortid"]));
                    $cstat = $cinfo->company_status;

                    if ($cinfo->company_status == 1 && $cinfo->demo_period_end < time()) {
                        // Demo period ended
                        $cstat = 6;
                    } else if ($cinfo->company_status == 2 && $cinfo->next_billing < time() &&
                               $DB->record_exists("monorail_data", array("type" => "PAYNOTIF", "itemid" => $course["cohortid"]))) {
                        // User failed to pay
                        $cstat = 7;
                    }

                    if ($cstat == 4 || $cstat == 6 || $cstat == 7) {
                        monorail_update_course_perms($course['id'], 1, 1);
                    } else {
                        monorail_update_course_perms($course['id'], 2, 2);
                    }
                } else {
                    monorail_update_course_perms($course['id'], 1, 1);
                }
            }

            $participantUsers = array_unique($participantUsers);

            foreach ($participantUsers as $uid) {
                wscache_reset_by_dependency("user_courses_" . $uid);
            }

            //CBTEC end
        }

        $transaction->allow_commit();

        return $resultcourses;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function create_courses_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'       => new external_value(PARAM_INT, 'course id'),
                    'shortname' => new external_value(PARAM_TEXT, 'short name'),
                    'code' => new external_value(PARAM_TEXT, 'course code')
                )
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function get_courses_parameters() {
        return new external_function_parameters(
                array('options' => new external_single_structure(
                            array('ofset' => new external_value(PARAM_INT, "Start listing with this entry", VALUE_DEFAULT, 0),
                                  'limit' => new external_value(PARAM_INT, "List no more than this number", VALUE_DEFAULT, 50)
                            ), 'options - operator OR is used', VALUE_DEFAULT, array())
                )
        );
    }

    /**
     * Get courses
     *
     * @param array $options It contains an array (list of ids)
     * @return array
     * @since Moodle 2.2
     */
    public static function get_courses($options = array()) {
        global $CFG, $DB;
        require_once($CFG->dirroot . "/course/lib.php");

        //validate parameter
        $params = self::validate_parameters(self::get_courses_parameters(),
                        array('options' => $options));

        $from = "FROM {course} AS c, {monorail_course_perms} AS cp "
                . "WHERE c.visible AND cp.course=c.id AND cp.access=3 AND cp.privacy=1";

        //$from = "FROM {course} AS c";

        $total_courses = $DB->get_records_sql("SELECT COUNT(c.id) AS cnt " . $from);
        $total_courses = reset($total_courses);

        $from .= " ORDER BY c.fullname";

        $ofset = $params["options"]["ofset"];

        if (array_key_exists("limit", $params["options"]))
        {
            $from .= " LIMIT " . $params["options"]["limit"];
        }

        if ($ofset)
        {
            $from .= " OFFSET " . $ofset;
        }

        $courses = $DB->get_records_sql("SELECT c.id, c.fullname, c.shortname " . $from);

        //create return value
        $coursesinfo = array();
        foreach ($courses as $course)
        {
            $courseinfo = array();
            $courseinfo['id'] = $course->id;
            $courseinfo['fullname'] = $course->fullname;
            $courseinfo['shortname'] = $course->shortname;

            $coursesinfo[] = $courseinfo;
        }

        return array (
            "total_courses" => $total_courses->cnt,
            "ofset" => $ofset,
            "courses" => $coursesinfo);
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function get_courses_returns() {
        return
            new external_single_structure(
                array(
            "total_courses" => new external_value(PARAM_INT, "total number of courses"),
            "ofset" => new external_value(PARAM_INT, "index of the first course"),
            "courses" => new external_multiple_structure(
                new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'shortname' => new external_value(PARAM_TEXT, 'course short name'),
                            'fullname' => new external_value(PARAM_TEXT, 'full name')
                        ), 'course'
                )
        )));
    }

    public static function get_submission_parameters() {
        return new external_function_parameters(
            array('assignid'   => new external_value(PARAM_INT, 'assignment id')
            )
        );
    }
    /**
     * get submission
     * @param array of  $submissions
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function get_submission($assignid) {
        global $CFG, $DB, $USER;
        require_once($CFG->libdir . '/gradelib.php');

        $params = self::validate_parameters(self::get_submission_parameters(),
            array('assignid' => $assignid));

        $warnings = array();
        $result = array();

        $assignment = $DB->get_record('assign', array('id' => $assignid), "*", MUST_EXIST);
        if (isset($assignment->id)) {
            try {
                $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assignment->id, 'userid'=> $USER->id), '*', MUST_EXIST);
                $onlinetext = $DB->get_record('assignsubmission_onlinetext', array('assignment'=>$assignment->id, 'submission'=>$submitinfo->id), '*', MUST_EXIST);
                $result['text'] = $onlinetext->onlinetext;
                $result['status'] = (($submitinfo->status == 'submitted')? 1 : 0);
                $result['timesubmitted'] = (($submitinfo->status == 'submitted')? $submitinfo->timemodified : 0);

                $assign = $DB->get_record('assign', array('id'=>$assignment->id), '*', MUST_EXIST);
                $grading_info = grade_get_grades($assign->course, 'mod', 'assign', $assign->id, $USER->id);
                $gradingitem = $grading_info->items[0];
                $gradebookgrade = $gradingitem->grades[$USER->id];
                $gradearray = array(
                    'grade'=> (($gradebookgrade->grade)?intval($gradebookgrade->grade):null),
                    'grademax'=> intval($gradingitem->grademax),
                    'gradestring'=> str_replace(' ','',get_string('gradelong', 'grades', array('grade'=>intval($gradebookgrade->grade),
                    'max'=>intval($gradingitem->grademax)))),
                    'feedback' => $gradebookgrade->str_feedback,
                    'dategraded' => $gradebookgrade->dategraded
                );
                $result['grade'] = $gradearray;
            } catch (Exception $e) {
                //No submissions
                $result['text'] = '';
                $result['status'] = 0;
                $result['timesubmitted'] = 0;
                $result['grade'] = array();
                return $result;
            }
        } else {
            return $result;
        }

        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for get_submission
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_submission_returns() {
        return new external_single_structure(
            array(
                "text" => new external_value(PARAM_RAW, "submission text"),
                "status" => new external_value(PARAM_INT, "status of submission"),
                "timesubmitted" => new external_value(PARAM_INT, "time when submitted"),
                "grade"   => self::grades(),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function task_submission_parameters()
    {
        return new external_function_parameters(
            array('assignid' => new external_value(PARAM_INT, 'assignment id'),
                'text' => new external_value(PARAM_RAW, 'submission text'),
                'status' => new external_value(PARAM_INT, 'submission status'),
            ), 'submission details to update');
    }

    /**
     * Submit or update task submission
     * @param submission record
     * @return an array of warnings if any
     * @since  Moodle 2.4
     */
    public static function task_submission($assignid, $text, $status) {
        global $CFG, $DB, $USER;
        require_once("$CFG->dirroot/theme/monorail/lib.php");

        $params = self::validate_parameters(self::task_submission_parameters(),
            array('assignid' => $assignid, 'text' => $text, 'status' => $status));

        $warnings = array();
        $result = array();

        $assignment = $DB->get_record('assign', array('id' => $assignid), "*", MUST_EXIST);
        if ($DB->record_exists('assign_submission', array('assignment'=>$assignment->id, 'userid'=> $USER->id))) {
            //User has submitted some stuff for this assignments
            $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assignment->id, 'userid'=> $USER->id));
        } else {
            // Insert submission record
            $record = new stdClass;
            $record->assignment = $assignment->id;
            $record->userid = $USER->id;
            $record->timecreated = time();
            $record->timemodified = time();
            $record->status = 'draft';
            $submitinfoid = $DB->insert_record("assign_submission", $record);
            $submitinfo = $DB->get_record('assign_submission', array('id'=>$submitinfoid));
        }
        // online text
        if ($DB->record_exists('assignsubmission_onlinetext', array('assignment'=>$assignment->id, 'submission'=>$submitinfo->id))) {
            $onlinetext = $DB->get_record('assignsubmission_onlinetext', array('assignment'=>$assignment->id, 'submission'=>$submitinfo->id));
            $onlinetext->onlinetext = $text;
            $DB->update_record('assignsubmission_onlinetext', $onlinetext);
        } else {
            $record = new stdClass;
            $record->assignment = $assignment->id;
            $record->submission = $submitinfo->id;
            $record->onlinetext = $text;
            $record->onlineformat = 1;  //HTML
            $onlinetextid = $DB->insert_record("assignsubmission_onlinetext", $record);
        }
        // submission status change
        if (($status === 1 && $submitinfo->status === 'draft') ||
            ($status === 0 && $submitinfo->status === 'submitted')) {
            $submitinfo->status = (($status === 1) ? 'submitted' : 'draft');
            $submitinfo->timemodified = time();
            $DB->update_record("assign_submission", $submitinfo);
            if ($status === 1 && $submitinfo->status === 'submitted') {
                $result['timesubmitted'] = $submitinfo->timemodified;
                // trigger notification
                $cm = get_coursemodule_from_instance('assign', $assignment->id, $assignment->course, false, MUST_EXIST);
                monorailfeed_create_notification($assignment->course, 'user_submitted', 'teacher', $cm->id, $USER->id, null, null, null, array(3));

                wscache_reset_by_user($USER->id);
            }
        }
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for add_submissions
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function task_submission_returns() {
        return new external_single_structure(
            array(
                'timesubmitted' => new external_value(PARAM_INT, 'submission timestamp', VALUE_OPTIONAL),
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function user_logout_parameters() {
        return new external_function_parameters(
            array(
                'sessionkey' => new external_value(PARAM_TEXT, 'The user sessionkey that is going to be logged out')
                )
        );
    }

    /**
     * Logout user
     *
     * @param array $sessionkey
     */
    public static function user_logout($sessionkey) {
        global $DB, $CFG, $USER;

        $params = self::validate_parameters(self::user_logout_parameters(), array('sessionkey'=>$sessionkey));
        $userparams = $USER;
        $token = $DB->get_record('external_tokens', array('userid'=>$USER->id));
        if (@$token->token) {
            //Remove the user tokens
            $DB->delete_records('external_tokens', array('userid'=>$USER->id));
        }
        events_trigger('user_logout', $userparams);
        session_get_instance()->terminate_current();
        unset($userparams);
    }

    /**
     * Returns description of method result value
     *
     * @return null
     */
    public static function user_logout_returns() {
        return null;
    }

    //Mobile client Now View
    /**
    * Returns description of method parameters
    * @return external_function_parameters
    */
    public static function get_user_events_parameters() {
     return new external_function_parameters(
       array(
         'timestart' => new external_value(PARAM_INT, 'start timestamp', VALUE_DEFAULT, 0),
         'untiltime' => new external_value(PARAM_INT, 'until timestamp', VALUE_DEFAULT, 0)
       )
     );
    }

    /**
     * Return events for the user which are in range
     * @param timestart Events whose starttime/duedate is greater than this timestamp
     * @param untiltime Events whose starttime/duedate is less than this timestamp
     * @return array of user events
     */
    public static function get_user_events($timestart=0, $untiltime=0) {
    global $USER, $DB, $CFG;

    $params = self::validate_parameters(self::get_user_events_parameters(), array('timestart'=>$timestart, 'untiltime'=>$untiltime));

    $courses = array();
    $fields = 'sortorder,shortname,fullname,timemodified';


    $courses = enrol_get_users_courses($USER->id, false, $fields);
    require_once($CFG->dirroot . '/calendar/lib.php');
    $calendar = new calendar_information();
    if(empty($timestart)){
        $timestart = $calendar->timestamp_today();
    }
    if(empty($untiltime)){
        //         $untiltime = $calendar->timestamp_tomorrow();
        $untiltime = $calendar->timestamp_tomorrow() * (7 * 24 *60 *60);
    }
    $events = calendar_get_events($timestart, $untiltime, $USER->id, true, array_keys($courses));

    $revents = array();
    foreach ($events as $id => $event) {
        $revent = array();
        $isvisible = $event->visible;
        require_once($CFG->libdir . '/gradelib.php');
        if(!empty($event->modulename)) {
            if(strcmp($event->modulename, "assign") == 0) {
                // see if assignment is graded!!
                $grading_info = grade_get_grades($event->courseid, 'mod', $event->modulename, $event->instance, $USER->id);
                $gradingitem = $grading_info->items[0];
                $gradebookgrade = $gradingitem->grades[$USER->id];
                if($gradebookgrade->dategraded) {
                    continue;
                }
            }
            $moduleinfo = $DB->get_record($event->modulename, array("id"=>$event->instance));
            if($moduleinfo) {
                $revent['name'] = $moduleinfo->name;
                if(strcmp($event->modulename, "assign") == 0) {
                    $revent['timedue'] = $moduleinfo->duedate;
                } else if (strcmp($event->modulename, "quiz") == 0){
                    $revent['timedue'] = $moduleinfo->timeclose;
                }
                if($event->courseid)
                {
                    $revent["instanceid"] = $DB->get_field_sql("SELECT cm.id FROM {course_modules} AS cm INNER JOIN {modules} AS m ON cm.module=m.id " .
                        "WHERE cm.instance=? AND cm.course=? AND m.name=?", array($event->instance, $event->courseid, $event->modulename));
                    $coursecontext = context_course::instance($event->courseid);
                    if(!(has_capability('moodle/course:update', $coursecontext))) {
                      $isvisible = $DB->get_field_sql("SELECT cm.visible FROM {course_modules} AS cm INNER JOIN {modules} AS m ON cm.module=m.id " .
                        "WHERE cm.instance=? AND cm.course=? AND m.name=?", array($event->instance, $event->courseid, $event->modulename));
                    }
                }
                if(strcmp($event->modulename, "bigbluebuttonbn") == 0) {
                    $revent['description'] = $CFG->wwwroot.'/mod/bigbluebuttonbn/view.php?id='.$revent["instanceid"];
                } else {
                    $revent['description'] = $moduleinfo->intro;
                }


            } else {
                if(((strcmp($event->modulename, "assign") == 0) || (strcmp($event->modulename, "quiz") == 0))) {
                  //Deleted task event orphan - delete event
                  continue;
                }
                $revent['name'] = $event->name;
                $revent['description'] = $event->description;
            }
        } else {
            $revent['name'] = $event->name;
            $revent['description'] = $event->description;
        }

        // We have list of course events

        $revent['id'] = $id;
        $revent['courseid'] = $event->courseid;

        if ($event->courseid)
        {
            $courseData = $DB->get_record_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?",
                    array($event->courseid));

            $revent['coursecode'] = $courseData->code;

            // Load background picture
            $backgrounddata = monorail_data('TEXT', $event->courseid, 'coursebackground');
            if (empty($backgrounddata))
            {
                $backgroundimage =  "/";
            }
            else
            {
                $backgrounddata = reset($backgrounddata);
                $backgroundimage = $backgrounddata->value;
            }

            $revent["coursebackground"] = $backgroundimage;
        }
        else
        {
            $revent['coursecode'] = "";
            $revent["coursebackground"] = "";
        }

        $revent['modulename'] = $event->modulename;
        $revent['moduleid'] = $event->instance;
        $revent['eventtype'] = $event->eventtype;
        $revent['userid'] = $event->userid;
        $revent['timestart'] = date(DATE_ISO8601, $event->timestart);
        $revent['timeend'] = date(DATE_ISO8601, $event->timestart + $event->timeduration);
        $revent['visible'] = $isvisible;
        if($event->courseid != 0) {
            $courseinfo = $DB->get_record("course", array("id"=>$event->courseid), 'fullname', MUST_EXIST);
            $revent['coursename'] = $courseinfo->fullname;
        }

        if($event->instance != 0) {
            //ok monorail_data info for the module if any
            $mrecords = $DB->get_records("monorail_data", array("itemid" => $event->instance));
            $mrecs = array();
            foreach ($mrecords as $mdata) {
                $mdat = array();
                $mdat['type'] =  $mdata->type;
                $mdat['datakey'] =  $mdata->datakey;
                $mdat['value'] =  $mdata->value;
                $mrecs[] = $mdat;
                if(strcmp($mdata->datakey, "name") != 0) {
                  $revent[$mdata->datakey] = $mdata->value;
                }
            }
            $revent['cdata'] = $mrecs;
        }

        $eventLocation = monorail_data("TEXT", $event->id, "event_location");

        if (!empty($eventLocation))
        {
            $eventLocation = reset($eventLocation);

            $revent["location"] = $eventLocation->value;
        }
        else
        {
            $revent["location"] = "";
        }

        $revents[]= $revent;
    }
    $result = array();
    $result['events'] = $revents;
    return $result;
    }

    /**
     * Describes the return value for get_user_events
     */
    public static function get_user_events_returns() {
     return new external_function_parameters(
       array(
         'events' => new external_multiple_structure(
           new external_single_structure(
             array(
               'id'    => new external_value(PARAM_INT, 'The event id'),
               'courseid' => new external_value(PARAM_INT, 'The course id, is 0 if user personal event'),
               'userid' => new external_value(PARAM_INT, 'User id of event owner.'),
               'coursecode' => new external_value(PARAM_TEXT, 'The course code, is "" if user personal event'),
               'coursebackground' => new external_value(PARAM_RAW, 'The course background, is "" if user personal event'),
               'moduleid' => new external_value(PARAM_INT, 'The module id , id of the instance in module (quiz/assign)'),
               'instanceid' => new external_value(PARAM_INT, 'The id of the instance in course module', VALUE_OPTIONAL),
               'modulename' => new external_value(PARAM_TEXT, 'Name of the module e.g quiz, assign if empty either user/course event'),
               'coursename' => new external_value(PARAM_TEXT, 'Name of the course , if event belongs to course', VALUE_OPTIONAL),
               'description' =>  new external_value(PARAM_RAW, 'Description of the event event'),
               'name' => new external_value(PARAM_TEXT, 'Name of the module e.g quiz, assign if empty either user/course event'),
               'location' =>  new external_value(PARAM_RAW, 'Location of the event'),
               'timestart' => new external_value(PARAM_TEXT, 'Start time of the event'),
               'timeend' => new external_value(PARAM_TEXT, 'End time of the event'),
               'timedue' => new external_value(PARAM_INT, 'Due date for the module quiz/assign etc', VALUE_OPTIONAL),
               'visible' => new external_value(PARAM_INT, 'Visibility of the event'),
               'eventtype' => new external_value(PARAM_TEXT, 'Event type - due-event has timedue set , user/class event has timestart and timeend'),
               "nosubmissions" => new external_value(PARAM_TEXT, 'nosubmissions value',VALUE_OPTIONAL),
               'cdata' => new external_multiple_structure(
                 new external_single_structure(
                 array(
                   "type" => new external_value(PARAM_TEXT, 'Data type',VALUE_OPTIONAL),
                 "datakey" => new external_value(PARAM_TEXT, 'Data Key',VALUE_OPTIONAL),
                 "value" => new external_value(PARAM_RAW, 'Data value',VALUE_OPTIONAL),
                   VALUE_OPTIONAL),"Custom monorail_data for the module if any" ,VALUE_OPTIONAL),VALUE_DEFAULT, array())
             )
           )

         )
       )
     );
    }

    public static function import_course_parameters() {
        return new external_function_parameters(
            array(
                'filename' => new external_value(PARAM_TEXT, 'Course archive filename that was uploaded by user')
                )
        );
    }


    /**
     * Import a course
     *
     * @param text $filename
     */
    public static function import_course($filename) {
        global $CFG, $USER, $DB;

        $params = self::validate_parameters(self::import_course_parameters(), array('filename'=>$filename));

        require_once(dirname(__FILE__). '/importlib.php');

        $warnings = array();
        $return = array();
        $coursecode = 0;

        try {
            $courseid = monorailservices_restore_from_file($CFG->dataroot."/temp/files/".$USER->id."/".$filename);
            if ($courseid == 0) {
                throw new Exception();
            }
            $transaction = $DB->start_delegated_transaction();
            try {
                $coursecode = monorailservices_finalize_course($courseid);
            } catch (Exception $ex) {
                $transaction->rollback($ex);
                // also delete course since we failed to finalize it
                $course = $DB->get_record('course', array('id'=>$courseid));
                delete_course($course, false);
                throw new Exception();
            }
            $transaction->allow_commit();
            $return['coursecode'] = $coursecode;

            wscache_reset_by_dependency("user_courses_" . $USER->id);

        } catch (Exception $ex) {
            $warning = array();
            $warning['item'] = 'import_course';
            $warning['itemid'] = 1;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Error when importing course';
            $warnings[] = $warning;
            add_to_log(1, 'monorailservices', 'externallib.import_course', '', 'ERROR: Failed to import course from backup: '.$USER->id.'/'.$filename.' - '.$ex->getMessage());
        }
        @unlink($CFG->dataroot."/temp/files/".$USER->id."/".$filename);

        $return['warnings'] = $warnings;

        return $return;
    }


    public static function import_course_returns() {
        return new external_single_structure(
            array(
                'coursecode' => new external_value(PARAM_TEXT, 'course code that was created', VALUE_OPTIONAL),
                'warnings'  => new external_warnings()
            )
        );
    }


    public static function delete_course_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID to be deleted')
                )
        );
    }

    /**
     * Delete a course
     *
     * @param int $courseid
     */
    public static function delete_course($courseid) {
        global $CFG, $USER, $DB;

        $params = self::validate_parameters(self::delete_course_parameters(), array('courseid'=>$courseid));

        $warnings = array();
        $return = array();

        require_once($CFG->dirroot . "/course/lib.php");
        require_once($CFG->dirroot . "/lib/accesslib.php");

        try {
            $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
        } catch (Exception $ex) {
            // No such course
            add_to_log(1, 'monorailservices', 'externallib.delete_course', '', 'Tried to delete course '.$courseid.' but such a course does not exist');
            return $return;
        }

        $context = context_course::instance($courseid);
        try {
            self::validate_context($context);
            $deleteok = false;
            if (! has_capability('moodle/course:delete', $context)) {
                // not admin, check if teacher or manager
                require_capability('moodle/course:create', $context);
                if (is_enrolled($context, null, "", true)) {
                    $managerid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);
                    $teacherid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
                    if (user_has_role_assignment($USER->id, $managerid, $context->id) || user_has_role_assignment($USER->id, $teacherid, $context->id)) {
                        $deleteok = true;
                    }
                }
            } else {
                // admin user
                $deleteok = true;
            }
            if (! $deleteok) {
                throw new Exception();
            }
        } catch (Exception $e) {
            $warning = array();
            $warning['item'] = 'delete_course';
            $warning['itemid'] = $courseid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'No access rights to delete course';
            $warnings[] = $warning;
        }
        try {
            if (! monorailservices_is_course_deletable($courseid)) {
                $deleteok = false;
                throw new Exception();
            }
        } catch (Exception $e) {
            $warning = array();
            $warning['item'] = 'delete_course';
            $warning['itemid'] = $courseid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Cannot delete course that is public, published in public catalog and has students';
            $warnings[] = $warning;
        }

        if ($deleteok) {
            $transaction = $DB->start_delegated_transaction();
            try {
                // Delete course from public catalog.
                if (isset($CFG->mage_api_url) && self::is_course_public($courseid))
                {
                    $courseCode = $DB->get_field_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?", array($courseid));

                    addBackgroundCall(function () use ($courseCode)
                    {
                        $api = magento_get_instance();

                        if ($api)
                        {
                            $catalogItem = $api->getProductBySku($courseCode);

                            if (!empty($catalogItem))
                            {
                                $api->begin();
                                $api->deleteProduct($catalogItem["product_id"]);
                            }
                        }
                    });
                }
                //delete video resources of the course
                $vrecords = $DB->get_records("monorail_course_videorsc", array("courseid"=>$courseid));
                foreach ($vrecords as $vrecord) {
                    self::delete_video_resource($vrecord->entryid);
                }
                //All course related video files should be deleted including for the task
                // as table 'monorail_course_videorsc' stores all course related records
                $DB->delete_records('monorail_assign_kalvidres', array("courseid" => $courseid));
                $result = delete_course($course, false);
                add_to_log(1, 'monorailservices', 'externallib.delete_course', '', 'Delete course '.$courseid.' result: '.$result);
                // delete any extra monorail data
                wscache_reset_by_dependency("course_info_" . $courseid);
                monorailservices_delete_course_data($courseid);
            } catch (Exception $ex) {
                $transaction->rollback($ex);
                $warning = array();
                $warning['item'] = 'delete_course';
                $warning['itemid'] = 1;
                $warning['warningcode'] = '1';
                $warning['message'] = 'Error when deleting course';
                $warnings[] = $warning;
                add_to_log(1, 'monorailservices', 'externallib.delete_course', '', 'ERROR: Failed to delete course: '.$ex->getMessage());
            }
            $transaction->allow_commit();
        }

        $return['warnings'] = $warnings;

        return $return;
    }

    public static function delete_course_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function get_notifications_parameters() {
        return new external_function_parameters(
            array(
                     'lastfetchcount' => new external_value(PARAM_INT, 'Last fetch count(sorted)', VALUE_DEFAULT, 0),
                     'maxcount' => new external_value(PARAM_INT, 'Max notifications to fetch', VALUE_DEFAULT, 20),
                     'lasttimetamp' => new external_value(PARAM_INT, 'Timestamp when the enrolment start', VALUE_DEFAULT, 0)
                )
        );
    }


    /**
     * Get notifications for the user
     *
     */
    /* TODO: this is only used by huxley now and should be refactored to use get_feed + UI side rendering like magic_ui */
    public static function get_notifications($lastfetchcount=0, $maxcount=20, $lasttimetamp=0) {
        global $USER, $DB;
        $return = array();
        $rns = array();

        $params = self::validate_parameters(self::get_notifications_parameters(),
		array('lastfetchcount'=>$lastfetchcount, 'maxcount'=>$maxcount, 'lasttimetamp'=>$lasttimetamp));
        $afterdate = ($lasttimetamp == 0) ? null : strval(lasttimetamp);
        $notifications = monorailfeed_compilefeed($lastfetchcount+$maxcount+20, null, $afterdate);
        usort($notifications, 'monorailfeed_sort_news_by_time');

	$ncount = count($notifications);
        if($ncount <= lastfetchcount)
        {
            $return['notifications'] = $rns;
            return $return;
        }

	if (!empty($notifications)) {
        $current = 0;
        $csize = 0;
        foreach ($notifications as $item) {
            if($current < $lastfetchcount) {
                continue;
            }
            if($csize >= $maxcount) {
                break;
            }
            $csize = $csize+1;
            $ns = array();
            $ns['userpic'] = $item->userpic;
            $ns['id'] = $item->id;
            $ns['read'] = $item->read;
            $ns['sectionid'] = $item->sectionid;
            $ns['coursename'] = $item->coursename;
            $ns['author'] = $item->author;
            $ns['content'] = $item->content;
            $ns['time'] = $item->time;
            $ns['type'] = $item->type;
            $ns['link'] = $item->link;
            $ns['picurl'] = $item->picurl;
            $ns['authroles'] = $item->authroles;
            $rns[] = $ns;
        }
        $return['notifications'] = $rns;
	return $return;
      }
    }

    public static function get_notifications_returns() {
       return new external_function_parameters(
       array(
         'notifications' => new external_multiple_structure(
           new external_single_structure(
             array(
               'id'    => new external_value(PARAM_INT, 'The notification id'),
               'time' => new external_value(PARAM_INT, 'Timestamp of notification'),
               'type' => new external_value(PARAM_TEXT, 'Type of event notification'),
               'read' => new external_value(PARAM_TEXT, 'User read property'),
               'content'    => new external_value(PARAM_RAW, 'The content', VALUE_OPTIONAL),
               'sectionid' => new external_value(PARAM_INT, 'The course section id', VALUE_OPTIONAL),
               'link' => new external_value(PARAM_URL, 'The link', VALUE_OPTIONAL),
               'picurl' => new external_value(PARAM_URL, 'The pic url ', VALUE_OPTIONAL),
               'coursename' => new external_value(PARAM_TEXT, 'Course name', VALUE_OPTIONAL),
               'author' => new external_value(PARAM_TEXT, 'Author name', VALUE_OPTIONAL),
               'userpic' => new external_value(PARAM_URL, 'User pic url',VALUE_OPTIONAL),
               'authroles' => new external_multiple_structure(new external_single_structure(array(
                    'roleid' => new external_value(PARAM_INT, 'role id'),
                    'name' => new external_value(PARAM_TEXT, 'name'),
                    'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                    'sortorder' => new external_value(PARAM_INT, 'sortorder')), "role"))
           )
         ))));
    }

//====================Cohort=========================
    public static function add_cohorts_parameters()
    {
        return new external_function_parameters(
            array('cohorts' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'name' => new external_value(PARAM_TEXT, 'name of the group'),
                        'iconfilename' => new external_value(PARAM_NOTAGS, 'filename',VALUE_OPTIONAL),
                        'domain' => new external_value(PARAM_TEXT, 'domain',VALUE_DEFAULT,''),
                        'movecourses' => new external_value(PARAM_INT, 'Courses to move',VALUE_DEFAULT,0),
                        'restrictdomain' => new external_value(PARAM_INT, 'Restrict users to this domain',VALUE_DEFAULT,0)
                    )), 'Group details')
            )
        );
    }

    /**
     * Add group(cohort)
     * @param array of  $cohorts - group details
     * @return success
     * @since  Moodle 2.4
     */
    public static function add_cohorts($cohorts) {
        global $CFG, $DB, $USER;
      

        $params = self::validate_parameters(self::add_cohorts_parameters(),
            array('cohorts' => $cohorts));

        $warnings = array();
        $returndata = array();
        if($DB->record_exists('cohort_members', array('userid'=>$USER->id))) {
            //User already a member in an cohort
            $warning = array();
            $warning['item'] = 'add_cohorts';
            $warning['itemid'] = $USER->id;
            $warning['warningcode'] = '1';
            $warning['message'] = 'The user is already a member in a cohort!!';
            $warnings[] = $warning;
            $returndata['warnings'] = $warnings;
            return $returndata;
        } 
        require_once($CFG->dirroot . '/cohort/lib.php');
        foreach ($cohorts as $cohort) {
           // Insert record
            $record = new stdClass;
            $record->name = $cohort['name'];
            $record->description = "";
            $record->descriptionformat = 1;
            $record->timecreated = time();
            $record->timemodified = time();
            $record->contextid = context_system::instance()->id;
            $cohortid = cohort_add_cohort($record);

            //Add admin record - default user who created the cohort is the admin
            $adminrecord = new stdClass;
            $adminrecord->cohortid = $cohortid;
            $adminrecord->userid = $USER->id;
            $adminrecord->usertype = 1;      // This user will be the owner.
            $DB->insert_record('monorail_cohort_admin_users', $adminrecord); 

            //Add admin user as cohort member
            cohort_add_member($cohortid, $USER->id);
            $DB->set_field('user', 'institution', $cohort['name'], array('id'=>$USER->id));

            $inforecord = new stdClass;
            $inforecord->fileid = 0;//TODO;
            //Cohort info
            $inforecord->cohortid = $cohortid;
            $inforecord->userid = $USER->id;
            $inforecord->userscount = 0;
            $inforecord->company_status = 1;
            $inforecord->demo_period_end = strtotime("+30 days midnight");
            //Extract user domain info email if not provided 
            $domain = $cohort['domain'];
            if(empty($domain)) {
               preg_match("/(.*)@(.*)/", $USER->email, $matches); 
               $domain = $matches[2];
            }
            $inforecord->domain = $domain;
            if (isset($cohort['restrictdomain'])) {
                $inforecord->domainrestrict = $cohort['restrictdomain'];
            } else {
                $cohort['restrictdomain'] = 0;
            }
            $DB->insert_record('monorail_cohort_info', $inforecord);
         
            if (isset($cohort['movecourses']) && $cohort['movecourses']) {
               //Move courses of this user to cohort , for which he is a teacher
               $usercourses = enrol_get_users_courses($USER->id, false);
               $mroleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
               foreach ($usercourses as $id => $course) {
                   $context = context_course::instance($id);
                   if(user_has_role_assignment($USER->id, $mroleid, $context->id)) {
                       if(!$DB->record_exists('monorail_cohort_courseadmins', array('cohortid' => $cohortid, 'courseid'=> $id))) {
                           $record = new stdClass;
                           $record->cohortid = $cohortid;
                           $record->courseid = $id;
                           $record->adminid = $USER->id;
                           $DB->insert_record('monorail_cohort_courseadmins', $record);

                           // clear cache!
                           wscache_reset_by_dependency("course_info_" . $id);
                       }
                   } 
               }                 
            } 

            $dsettings = array(
                'cohort_course_privacy' => 1,
                'cohort_course_privacy_ext' => 0,
                'cohort_enroll_insecure' => 0);

            foreach ($dsettings as $dsetting => $dvalue)
            {
                //put default settings
                $srecord = new stdClass;
                $srecord->cohortid = $cohortid;
                $srecord->name = $dsetting;
                $srecord->value = $dvalue;
                $srecord->modifiedby = $USER->id;
                $srecord->timemodified = time();
                $DB->insert_record('monorail_cohort_settings', $srecord);
            }

            $returndata[] = $cohortid;

            // Open invites to cohort.
            $invite = monorail_get_or_create_invitation('cohort', $cohortid);
            $DB->set_field('monorail_invites', 'active', 1, array('code'=>$invite->code));

            // send welcome email
            require_once("$CFG->dirroot/theme/monorail/engagements.php");
            engagements_trigger('welcome-email-cohort', $USER->id);
            
        }

        // Just to be sure...
        wscache_reset_by_user($USER->id);

        $result = array();
        $result['cohortids'] = $returndata;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for add_cohorts
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function add_cohorts_returns() {
        return new external_single_structure(
            array(
                'cohortids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'cohort id'),
                    'cohort ids',VALUE_OPTIONAL),
                'warnings'  => new external_warnings()
            )
        );
    }


    public static function update_cohorts_parameters()
    {
        return new external_function_parameters(
            array('cohorts' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'id of the group'),
                        'name' => new external_value(PARAM_TEXT, 'name of the group',VALUE_OPTIONAL),
                        'billing_details' => new external_value(PARAM_TEXT, 'Company billing details',VALUE_OPTIONAL),
                        'organization_details' => new external_value(PARAM_TEXT, 'Company details',VALUE_OPTIONAL),
                        'published_courses' => new external_value(PARAM_TEXT, 'Published course list',VALUE_OPTIONAL),
                        'public_page' => new external_value(PARAM_INT, 'Is public page enabled', VALUE_OPTIONAL),
                        'public_page_ann' => new external_value(PARAM_INT, 'Is public page announcement enabled', VALUE_OPTIONAL),
                        'public_page_ann_info' => new external_value(PARAM_TEXT, 'Public page announcement info', VALUE_OPTIONAL),
                        'public_page_ad' => new external_value(PARAM_INT, 'Is public page announcement enabled', VALUE_OPTIONAL),
                        'public_page_ad_info' => new external_value(PARAM_TEXT, 'Public page announcement info', VALUE_OPTIONAL),
                        'public_page_cats' => new external_value(PARAM_TEXT, 'Public page announcement info', VALUE_OPTIONAL),
                        'iconfilename' => new external_value(PARAM_NOTAGS, 'filename',VALUE_OPTIONAL),
                        'settings' => new external_multiple_structure(
                            new external_single_structure(
                                array(
                                    'name'  => new external_value(PARAM_TEXT, 'The name of the custom field'),
                                    'value' => new external_value(PARAM_TEXT, 'The value of the custom field'),
                                )
                        ), 'Cohort custom settings', VALUE_OPTIONAL),
                    )), 'Group details')
            )
        );
    }

    /**
     * Add group(cohort)
     * @param array of  $cohorts - group details
     * @return success 
     * @since  Moodle 2.4
     */
    public static function update_cohorts($cohorts) {
        global $CFG, $DB, $USER;
     global $CFG, $DB, $USER;
      

        $params = self::validate_parameters(self::update_cohorts_parameters(),
            array('cohorts' => $cohorts));
        $warnings = array();
        require_once($CFG->dirroot . '/cohort/lib.php');
        foreach ($cohorts as $cohort) {
            $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohort['id']));
            if(!$auser) {
                //Not an admin user!
                $warning = array();
                $warning['item'] = 'update_cohorts';
                $warning['itemid'] = $cohort['id'];
                $warning['warningcode'] = '1';
                $warning['message'] = 'Only Admin access for this API';
                $warnings[] = $warning;
                continue;
            }
            //Get record
            $changed = false;
            $crec = $DB->get_record('cohort', array('id'=>$cohort['id']));
            if (@$cohort['name'] && ($crec->name != @$cohort['name'])) {
                //Ok org name change
                $crec->name = $cohort['name'];
                $changed = true;
                $DB->execute("UPDATE {user} SET institution=? WHERE id IN (SELECT userid FROM {cohort_members} WHERE cohortid={$cohort['id']})", array($cohort["name"]));
            }
            if (@$cohort['iconfilename']) {
                $changed = true;
                monorail_data('cohort', $cohort['id'], 'iconfilename', $cohort['iconfilename']);
            }
            if ($changed) {
                cohort_update_cohort($crec);
            }
            if (@$cohort["settings"]) {
                foreach($cohort['settings']  as $setting) {
                    $erec = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohort['id'], 'name' =>$setting['name']));
                    if($erec) {//already existing setting
                        $erec->modifiedby = $USER->id;
                        $erec->timemodified = time();
                        $erec->value = clean_param($setting['value'], PARAM_RAW_TRIMMED);
                        $DB->update_record('monorail_cohort_settings', $erec);
                    } else {
                        $srecord = new stdClass;
                        $srecord->cohortid = $cohort['id'];
                        $srecord->name = $setting['name'];
                        $srecord->value = clean_param($setting['value'], PARAM_RAW_TRIMMED);
                        $srecord->modifiedby = $USER->id;
                        $srecord->timemodified = time();
                        $DB->insert_record('monorail_cohort_settings', $srecord);
                   }

                    if ($setting["name"] == "cohort_enroll_insecure") {
                        $DB->execute("UPDATE {monorail_invites} SET active=? WHERE type='cohort' AND targetid=?",
                            array((int) $setting["value"], $cohort['id']));
                    }
                }

                // Clear cache for affected courses...
                $courses = $DB->get_fieldset_sql("SELECT courseid FROM {monorail_cohort_courseadmins} WHERE cohortid=?", array($cohort["id"]));

                foreach ($courses as $cid) {
                    wscache_reset_by_dependency("course_info_" . $cid);
                }
            }
            if (@$cohort['iconfilename']) {
                // icon was changed. We must update to catalog all courses that have been using the default cohort icon
                $courses = $DB->get_records('monorail_cohort_courseadmins', array('cohortid'=>$cohort['id']), null, 'courseid');
                foreach ($courses as $course) {
                    try {
                        $cperm = monorail_get_course_perms($course->courseid);
                        if (($cperm["course_privacy"] == 1) && ($cperm["course_access"] == 3)) {
                            $courselogo = reset(monorail_data('TEXT', $course->courseid, 'course_logo'));
                            if (empty($courselogo) || $courselogo->value == '/') {
                                // needs updating
                                $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$course->courseid), '*', MUST_EXIST);
                                $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($course->courseid));
                                $monorailData = array();
                                foreach ($moreData as $md)
                                {
                                    $monorailData[$md->datakey] = $md->value;
                                }
                                $monorailData['course_access'] = $cperm['course_access'];
                                $monorailData['course_privacy'] = $cperm['course_privacy'];
                                $courseinfo = $DB->get_record('course', array('id' => $course->courseid), "*", MUST_EXIST);

                                addBackgroundCall(function () use ($coursedata, $monorailData, $courseinfo)
                                {
                                    require_once __DIR__ . "/mageapi.php";
                                    magento_update_course($coursedata, $monorailData, $courseinfo);
                                });
                            }
                        }
                    } catch (Exception $ex) {
                        add_to_log($course->courseid, 'monorailservices', 'externallib', 'update_cohorts', 'Exception when trying to update course in catalog after icon change: '.$ex->getMessage());
                    }
                }
            }

            if (@$cohort["billing_details"]) {
                $DB->execute("UPDATE {monorail_cohort_info} SET billing_details=? WHERE cohortid=?",
                    array($cohort["billing_details"], $cohort["id"]));
            }

            if (@$cohort["organization_details"]) {
                $DB->execute("UPDATE {monorail_cohort_info} SET organization_details=? WHERE cohortid=?",
                    array($cohort["organization_details"], $cohort["id"]));
            }

            if (@$cohort["published_courses"]) {
                try {
                    $pcList = json_decode($cohort["published_courses"]);

                    foreach ($pcList as $pcId) {
                        // Make sure published course info is in redis.
                        monorail_get_course_details($pcId);
                    }
                } catch (Exception $err) { }

                $DB->execute("UPDATE {monorail_cohort_info} SET published_courses=? WHERE cohortid=?",
                    array($cohort["published_courses"], $cohort["id"]));
            }

            if (isset($cohort["public_page"])) {
                $DB->execute("UPDATE {monorail_cohort_info} SET public_page=? WHERE cohortid=?",
                    array($cohort["public_page"], $cohort["id"]));

                if ($cohort["public_page"] && !$DB->get_field_sql("SELECT public_page_path FROM {monorail_cohort_info} WHERE id=?", array($cohort["id"]))) {
                    $path = strtolower(preg_replace('/[^a-zA-Z0-9]+/', "", $crec->name));
                    $orPath = $path;
                    $num = 0;
                    $ok = false;

                    while ($num < 2000 && !$ok) {
                        switch (monorail_check_org_public_page_path($path)) {
                            case 0:
                                $ok = true;
                                break;

                            case 1:
                                $path .= "0";
                                $orPath = $path;
                                break;

                            case 2:
                                // No invalid charaters should be here...
                                $num = 10000;
                                break;

                            default:
                                $num++;
                                $path = $orPath . $num;
                        }
                    }

                    if ($ok) {
                        $DB->execute("UPDATE {monorail_cohort_info} SET public_page_path=? WHERE cohortid=?",
                            array($path, $cohort["id"]));
                    } else {
                        error_log("Failed to generate unique path for org. pub. page?");
                    }
                }
            }

            if (isset($cohort["public_page_ann"])) {
                $DB->execute("UPDATE {monorail_cohort_info} SET public_page_ann=? WHERE cohortid=?",
                    array((int) $cohort["public_page_ann"], $cohort["id"]));
            }

            if (isset($cohort["public_page_ann_info"])) {
                $DB->execute("UPDATE {monorail_cohort_info} SET public_page_ann_info=? WHERE cohortid=?",
                    array($cohort["public_page_ann_info"], $cohort["id"]));
            }

            if (isset($cohort["public_page_ad"])) {
                $DB->execute("UPDATE {monorail_cohort_info} SET public_page_ad=? WHERE cohortid=?",
                    array((int) $cohort["public_page_ad"], $cohort["id"]));
            }

            if (isset($cohort["public_page_ad_info"])) {
                $DB->execute("UPDATE {monorail_cohort_info} SET public_page_ad_info=? WHERE cohortid=?",
                    array($cohort["public_page_ad_info"], $cohort["id"]));
            }

            if (isset($cohort["public_page_cats"])) {
                $DB->execute("UPDATE {monorail_cohort_info} SET public_page_cats=? WHERE cohortid=?",
                    array($cohort["public_page_cats"], $cohort["id"]));
            }
        }

        $result = array();
        $result['warnings'] = $warnings;
        return $result;  
    }

    /**
     * Describes the return value for update_cohorts
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_cohorts_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function add_cohort_members_parameters()
    {
        return new external_function_parameters(
            array(
                'userids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'user id'),
                    '0 or more user ids',
                    VALUE_DEFAULT, array()),
                'cohortid' => new external_value(PARAM_INT, 'id of the group'),
                'inviteid' => new external_value(PARAM_TEXT, 'code of the invite'),
                'suspend' => new external_value(PARAM_INT, '1 for suspended account', VALUE_DEFAULT, 0)
            )
        );
    }

    /**
     * Add group(cohort)
     * @param array of  $cohorts - group details
     * @return success 
     * @since  Moodle 2.4
     */
    public static function add_cohort_members($userids, $cohortid, $inviteid, $suspend) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::add_cohort_members_parameters(),
            array('userids' => $userids, 'cohortid' => $cohortid, 'inviteid' => $inviteid, 'suspend' => $suspend));

        require_once($CFG->dirroot . '/cohort/lib.php');

        $invite = $DB->get_record('monorail_invites', array('code'=>$inviteid), '*', MUST_EXIST);
        //$cinfo = $DB->get_record('monorail_cohort_info', array('cohortid' => $cohortid));
        //$cohortusers = $DB->count_records("cohort_members", array("cohortid" => $cohortid));
        // $invitedusers is redefiend below - this call has no effect
        //$invitedusers = $DB->count_records('monorail_invite_users', array('monorailinvitesid' => $invite->id));
        //$openinvites = $cinfo->userscount - $cohortusers;
      
        $result = array();
        $warnings = array();
        /*
        if (($cinfo->userscount != -1 && (($openinvites <= 0) || ($openinvites < count($userids)))) || !($invite->active)) {
            $warning = array();
            $warning['item'] = 'add_cohort_member';
            $warning['itemid'] = $cohortid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Invite invalid !!';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }
        */

        $invitedusers = array();
        $inviteRecords = $DB->get_records('monorail_invite_users', array('monorailinvitesid' => $invite->id));
        $userTags = array();
        $userRoles = array();

        foreach ($inviteRecords as $ir)
        {
            $invitedusers[] = strtolower($ir->email);
            $userTags[strtolower($ir->email)] = $ir->usertag;
            $userRoles[strtolower($ir->email)] = $ir->userrole;
        }

        foreach ($userids as $userid) {
            $userrec = $DB->get_record('user', array('id' => $userid),'username, email');
            $existingmember = $DB->count_records("cohort_members", array('userid'=>$userid ));
            if(!$userrec || ($existingmember > 0)) {
                $warning = array();
                $warning['item'] = 'add_cohort_member';
                $warning['itemid'] = $cohortid;
                $warning['warningcode'] = '1';
                $warning['message'] = 'User record does not exist in DB/Already member of some cohort!!!';
                $warnings[] = $warning;
                $result['warnings'] = $warnings;
                return $result;
            }
            $suser = $DB->get_record('monorail_cohort_susers', array('userid'=>$userid, 'cohortid' => $cohortid));

            $csetting = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohortid, 'name' => 'cohort_enroll_insecure'));
            $insecureenroll = (!empty($csetting)) ? $csetting->value: 0; 
            $inviteduser = $insecureenroll || (in_array(strtolower($userrec->email), $invitedusers) || in_array(strtolower($userrec->username), $invitedusers)); 
            $userTag = $userTags[strtolower($userrec->email)];
            $userRole = $userRoles[strtolower($userrec->email)];

            if(!$suser && !$inviteduser){
               //User not invited !!
                $warning = array();
                $warning['item'] = 'add_cohort_member';
                $warning['itemid'] = $cohortid;
                $warning['warningcode'] = '1';
                $warning['message'] = 'User not in invited list of users for this cohort';
                $warnings[] = $warning;
                $result['warnings'] = $warnings;
                return $result;
            } else {
                if ($userTag)
                {
                    // Assign tag to new user if one was specified when invite was sent
                    if (!$DB->record_exists("monorail_usertag_user", array("tagid" => $userTag, "userid" => $userid)))
                    {
                        $rec = new stdClass();

                        $rec->tagid = $userTag;
                        $rec->userid = $userid;

                        $DB->insert_record("monorail_usertag_user", $rec);

                        // Automatically enrol to courses where this tag is
                        // enrolled.
                        $tagCourses = $DB->get_records_sql("SELECT uc.courseid AS courseid, role.shortname AS role" .
                                " FROM {monorail_usertag_course} AS uc INNER JOIN {role} AS role ON role.id=uc.roleid" .
                                " WHERE uc.tagid=?", array($userTag));

                        $enrolments = array();

                        foreach ($tagCourses as $tc) {
                            $enrolments[] = array(
                                "rolename" => $tc->role,
                                "courseid" => $tc->courseid,
                                "userid" => $userid,
                                "inviteid" => $inviteid
                            );

                            // TODO: check if user already enroled to this
                            // course
                            $enrolRec = new stdClass();

                            $enrolRec->tagid = $userTag;
                            $enrolRec->courseid = $tc->courseid;
                            $enrolRec->userid = $userid;

                            $DB->insert_record("monorail_usertag_enrolment", $enrolRec);
                        }

                        if (!empty($enrolments))
                        {
                            self::cohort_course_enrol($enrolments, array());
                        }
                    }
                }

                if ($userRole == 2 || $userRole == 3) {
                    // Make user admin or observer
                    $cohortcourses = self::cohort_courses($cohortid);
                    $mroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);

                    //Add admin record - 
                    $adminrecord = new stdClass;
                    $adminrecord->cohortid = $cohortid;
                    $adminrecord->userid = $userid;
                    $adminrecord->usertype = $userRole;
                    $DB->insert_record('monorail_cohort_admin_users', $adminrecord);

                    require_once($CFG->libdir . '/enrollib.php');

                    foreach ($cohortcourses['courses'] as $ccourse) {
                        $enrol = enrol_get_plugin('manual');
                        $enrolinstances = enrol_get_instances($ccourse['id'], true);
                        foreach ($enrolinstances as $courseenrolinstance) {
                            if ($courseenrolinstance->enrol == "manual") {
                                $instance = $courseenrolinstance;
                                break;
                            }
                        }
                        $context = context_course::instance($ccourse['id']);
                        if (is_enrolled($context, $userid)) {
                            //User is enrolled to course
                            //Assign manager role
                            /* Don't interfere with other roles (student?)
                            if(!user_has_role_assignment($userid, $mroleid, $context->id)) {
                                role_assign($mroleid, $userid, $context->id);
                            } 
                            */
                        } else {
                            //Enrol admin user as course manager 
                            $enrol->enrol_user($instance, $userid, $mroleid, time(), 0, ENROL_USER_ACTIVE);
                        }
                    }
                }

                //Remove invite record as user is added now to group
                $DB->delete_records('monorail_invite_users', array('email' => $userrec->email, 'monorailinvitesid' => $invite->id));
            }

            if ($userRole == 5) {
                // Not a real cohort member.
                try {
                    $DB->execute("INSERT INTO {monorail_cohort_user_link} (cohortid, userid) VALUES (?, ?)",
                        array($cohortid, $userid));
                } catch (Exception $ex) {}
            } else {
                cohort_add_member($cohortid, $userid);
                $cohort = $DB->get_record('cohort', array('id' => $cohortid));
                $DB->set_field('user', 'institution', $cohort->name, array('id'=>$userid));
            }

            $suser = $DB->get_record('monorail_cohort_susers', array('userid'=>$userid, 'cohortid' => $cohortid));
            if($suser) {
                //TODO: Auto enroll to courses! 
                $DB->delete_records('monorail_cohort_susers', array('id'=>$suser->id, 'cohortid' => $cohortid)); 
            } else {
                //As member is being added to cohort , check if any pending course enrollment and enrol user  
                $invrecs = $DB->get_records('monorail_cohort_crsinvitee', array('email' => $userrec->email));
                $enrolments = array();
                $adduserids = array();
                foreach ($invrecs as $invrec) { 
                    //Get the invitation for the course for user which is active
                    $invite = $DB->get_record('monorail_invites', array('targetid'=>$invrec->courseid, 'type' => 'course', 'active' => 1, 'code'=>$invrec->invitecode)); 
                    if($invite) {
                        $rec = array();
                        $rec['userid'] = $userrec->id;
                        $rec['courseid'] = $invrec->courseid;
                        $rec['inviteid'] = $invite->code;
                        $rec['rolename'] = $invrec->rolename;
                        $rec['timestart'] = $invrec->timestart;
                        $rec['timeend'] = $invrec->timeend;
                        $rec['suspend'] = $invrec->suspend;
                        $enrolments[] = $rec;
                        $adduserids[] = $invrec->id; 
                    }  
                }
                if(!empty($enrolments)) {
                    self::enrol_users(array('enrolments' => $enrolments, 'internal' => 1));
                    //Delete now the records of the users which have enrolled to eliademy course
                    $DB->delete_records_select('monorail_cohort_crsinvitee', "id IN $adduserids"); 
                }
            }

            wscache_reset_by_dependency("user_info_" . $userid);
            wscache_reset_by_dependency("user_courses_" . $userid);
            wscache_reset_by_user($userid);

            /*
            $openinvites = $openinvites - 1;
            if(($openinvites <= 0) && $invite->active && $cinfo->userscount != -1) {
                $DB->set_field('monorail_invites', 'active', 0, array('code'=>$invite->code));
                break;
            }
            */
        }

        $result['warnings'] = $warnings;
        return $result;
    }


 public static function unenroll_cohort_extuser_parameters()
    {
        return new external_function_parameters(
            array(
                'userids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'user id'),
                    '0 or more user ids',
                    VALUE_DEFAULT, array()),
                'cohortid' => new external_value(PARAM_INT, 'id of the group')
            )
        );
    }

    /**
     * A group(cohort)
     * @param array of  $userids - userids 
     * @return success 
     * @since  Moodle 2.4
     */
    public static function unenroll_cohort_extuser($userids, $cohortid) {
        global $CFG, $DB, $USER;
        $params = self::validate_parameters(self::unenroll_cohort_extuser_parameters(),
            array('userids' => $userids, 'cohortid' => $cohortid));
        $warnings = array();
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            //No admin access!!
            $warning = array();
            $warning['item'] = 'remove_cohort_members';
            $warning['itemid'] = $cohortid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Only admin user can access this API';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }
        require_once($CFG->dirroot . '/cohort/lib.php');
        $mroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);
        $sroleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST);
        $teacherid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);

        foreach ($userids as $userid) {
            $userrec = $DB->get_record('user', array('id' => $userid),'username, email'); 
                $ucourses = self::cohort_courses($cohortid);
                foreach($ucourses['courses'] as $ucourse) {
                    $context = context_course::instance($ucourse['id']);
                    if(user_has_role_assignment($userid, $sroleid, $context->id)) {
                        role_unassign($sroleid, $userid, $context->id);
                    }
                    if(user_has_role_assignment($userid, $teacherid, $context->id)) {
                        role_unassign($teacherid, $userid, $context->id);
                    }
                    $roles = get_user_roles($context , $userid);
                    if(!$roles) {
                        $enrol = enrol_get_plugin('manual');
                        $enrolinstances = enrol_get_instances($ucourse['id'], true);
                        foreach ($enrolinstances as $courseenrolinstance) {
                            if ($courseenrolinstance->enrol == "manual") {
                                $instance = $courseenrolinstance;
                                break;
                            }
                        } 
                        $enrol->unenrol_user($instance, $userid);
                    }  
                }
        }

        $result = array();
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for remove_cohort_members
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function unenroll_cohort_extuser_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }


    /**
     * Describes the return value for remove_cohort_members
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function add_cohort_members_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function remove_cohort_members_parameters()
    {
        return new external_function_parameters(
            array(
                'userids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'user id'),
                    '0 or more user ids',
                    VALUE_DEFAULT, array()),
                'cohortid' => new external_value(PARAM_INT, 'id of the group')
            )
        );
    }

    /**
     * A group(cohort)
     * @param array of  $userids - userids
     * @return success
     * @since  Moodle 2.4
     */
    public static function remove_cohort_members($userids, $cohortid) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::remove_cohort_members_parameters(),
            array('userids' => $userids, 'cohortid' => $cohortid));

        $warnings = array();
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            //No admin access!!
            $warning = array();
            $warning['item'] = 'remove_cohort_members';
            $warning['itemid'] = $cohortid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Only admin user can access this API';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }
        require_once($CFG->dirroot . '/cohort/lib.php');
        $mroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);
        $sroleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST);
        $troleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
        foreach ($userids as $userid) {
            //Admin not allowed to suspend/delete self
            if($userid == $USER->id) {
                continue;
            }
            $userrec = $DB->get_record('user', array('id' => $userid),'username, email');
            //TODO: Check if user is in suspended list then remove from the same

            /*
             * XXX: suspend functionality not used anymore?
            $suser = $DB->get_record('monorail_cohort_susers', array('userid'=>$userid, 'cohortid' => $cohortid));
            if($suser) {
                //User already suspended from cohort, if account delete remove record from suspended users
                if(!$suspend){
                    $DB->delete_records('monorail_cohort_susers', array('id'=>$suser->id, 'cohortid' => $cohortid));
                }
                continue;
            }
            */

            cohort_remove_member($cohortid, $userid);

            $DB->execute("DELETE FROM {monorail_cohort_user_link} WHERE cohortid=? AND userid=?",
                array($cohortid, $userid));

            $cadmins = $DB->get_records('monorail_cohort_courseadmins', array('adminid'=>$userid, 'cohortid' => $cohortid));
            foreach ($cadmins as $cadmin) {
                $DB->set_field('monorail_cohort_courseadmins', 'adminid', $USER->id, array('id'=>$cadmin->id));
            }

            $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$userid, 'cohortid' => $cohortid));
            if ($auser) {
                $DB->delete_records('monorail_cohort_admin_users', array('id'=>$auser->id));
            }

            require_once($CFG->dirroot . '/enrol/externallib.php');
            require_once($CFG->libdir . '/enrollib.php');

            // Unenroll from all cohort courses...
            $ucourses = self::cohort_courses($cohortid);
            $enrol = enrol_get_plugin('manual');

            foreach ($ucourses["courses"] as $ucourse) {
                $enrolinstances = enrol_get_instances($ucourse['id'], true);
                $instance = null;

                foreach ($enrolinstances as $courseenrolinstance) {
                    if ($courseenrolinstance->enrol == "manual") {
                        $instance = $courseenrolinstance;
                        break;
                    }
                }

                if ($instance) {
                    $enrol->unenrol_user($instance, $userid);
                }

                $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$ucourse['id']), '*', MUST_EXIST);

                if ($coursedata->mainteacher == $userid) {
                    // This user is mainteacher for the course update. Find
                    // first other teacher to replace as main teacher, or fall
                    // back to admin as main teacher.

                    $coursedata->mainteacher = null;
                    $context = context_course::instance($ucourse['id']);
                    $teachers = get_role_users($troleid, $context);

                    foreach ($teachers as $ot) {
                        if ($ot->id != $userid) {
                            $coursedata->mainteacher = $ot->id;
                            break;
                        }
                    }

                    if (!$coursedata->mainteacher) {
                        $coursedata->mainteacher = $USER->id; //Make admin the main teacher
                    }

                    $DB->update_record('monorail_course_data', $coursedata);
                }
                wscache_reset_by_dependency("course_info_" . $ucourse["id"]);
            }

            /*
                //$ucourses = core_enrol_external::get_users_courses($auser->id, array());
            } else { //Not admin user remove enrollments as student and teacher
                foreach($ucourses['courses'] as $ucourse) {

                    $hadrole = false;
                    if(user_has_role_assignment($userid, $troleid, $context->id)) {
                      $hadrole = true;
                      $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$ucourse['id']), '*', MUST_EXIST);
                      role_unassign($troleid, $userid, $context->id);
                      if($coursedata->mainteacher == $userid) {
                        //This user is mainteacher for the course update
                        $teachers = get_role_users($troleid, $context);
                        $mt = reset($teachers);
                        if ($mt !== FALSE) {
                          $coursedata->mainteacher = $mt->id;
                        } else {
                          $coursedata->mainteacher = $USER->id; //Make admin the main teacher
                        }
                        $DB->update_record('monorail_course_data', $coursedata);
                      }
                    }
                    if(user_has_role_assignment($userid, $sroleid, $context->id)) {
                        $hadrole = true;
                        role_unassign($sroleid, $userid, $context->id);
                    }
                    if($hadrole) {
                        $roles = get_user_roles($context , $userid);
                        if(!$roles) {
                        }
                    }
                }
            }
            */

            /*
            if($suspend) {
                //TODO: Account is suspended remove user from cohort owned courses in which user is enrolled
                //Remove any pending course invites
                $DB->delete_records('monorail_invite_users', array('email'=>$userrec->email));
                $inforecord = new stdClass;
                $inforecord->cohortid = $cohortid;
                $inforecord->userid = $userid;
                //$inforecord->courseids = TODO: comma separated list of course ids;
                $DB->insert_record('monorail_cohort_susers', $inforecord);
            }
            */

            wscache_reset_by_user($userid);
        }

        //After removing users if we have some users available for admin enable invite code!
        /* We never close invites now
        $invite = monorail_get_or_create_invitation('cohort', $cohortid);
        if(!$invite->active) {
            $cinfo = $DB->get_record('monorail_cohort_info', array('cohortid' => $cohortid));
            $cohortusers = $DB->count_records("cohort_members", array("cohortid" => $cohortid));
            $invitedusers = $DB->count_records('monorail_invite_users', array('monorailinvitesid' => $invite->id));
            $openinvites = $cinfo->userscount - $cohortusers;
            if($openinvites > 0) {
                $DB->set_field('monorail_invites', 'active', 1, array('code'=>$invite->code));
            }
        }
        */
        $result = array();
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for remove_cohort_members
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function remove_cohort_members_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function cohorts_info_parameters()
    {
        return new external_function_parameters(
            array(
                'cohortids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'cohort ID'),'list of cohort ids',VALUE_DEFAULT,array())
            )
        );
    }

    /**
     * Get cohort info 
     * @param array of  $cohorts - group details
     * @return success 
     * @since  Moodle 2.4
     */
    public static function cohorts_info($cohortids) {
        global $CFG, $DB, $USER;

        $params = self::validate_parameters(self::cohorts_info_parameters(),
            array('cohortids' => $cohortids));

        $result = array();
        $rids = array();
       //Allow only admin or cohort group user
       // FIXME: What about external users?
        foreach($cohortids as $cohortid) {
            $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
            if(!$auser) {
                $cohortuser = $DB->get_record("cohort_members", array("cohortid" => $cohortid,'userid'=>$USER->id ));
                if(!$cohortuser){
                    //neither admin nor cohort group member
                    array_push($rids, $cohortid);
                }
            }
        }
        //Remove cohortids for which user does not have access
        $cohortids = array_diff($cohortids, $rids);
        if(empty($cohortids)){
            return $result;
        }
        list($sqlcohortids, $params) = $DB->get_in_or_equal($cohortids);
        $cohortsql = "SELECT c.* 
        FROM {cohort} c 
        WHERE c.id $sqlcohortids";
        $cohorts = $DB->get_recordset_sql($cohortsql, $params);
        foreach($cohorts as $cohort) {

            //TODO: Get icon for cohort
            $cdata = array();
            $uinfo = $DB->get_record('user', array('id' => $USER->id));
            if($uinfo->auth == 'googleapps') {
                $cdata['googleapp'] = 1;
            }

            $cdata['name'] = $cohort->name;
            $cdata['id'] = $cohort->id;

            // Get the userids of the admins for the cohort
            //If user has been admin , remove the record
            $admins = '';
            $ausers = $DB->get_records('monorail_cohort_admin_users', array('cohortid' => $cohort->id)); 
            $cdata['isadmin'] = 0;
            foreach ($ausers as $auser) {  
                if($USER->id == $auser->userid) {
                    $cdata['isadmin'] = 1;

                    if ($auser->usertype == 1) {
                        $cdata["isowner"] = 1;
                    }
                }
                $admins = $admins . $auser->userid . ',';
            }

            $cdata['cohorticonurl'] = monorail_data("cohort", $cohort->id, "iconfilename");
            if (empty($cdata['cohorticonurl']))
            {
                $cdata['cohorticonurl'] = "";
            }
            else
            {
                $cdata['cohorticonurl'] = reset($cdata['cohorticonurl']);
                $cdata['cohorticonurl'] = $cdata['cohorticonurl']->value;
            }

            $cinfo = $DB->get_record('monorail_cohort_info', array('cohortid' => $cohort->id));

            $cdata["company_status"] = $cinfo->company_status;

            if ($cinfo->company_status == 1 && $cinfo->demo_period_end < time()) {
                // Demo period ended
                $cdata["company_status"] = 6;
            } else if ($cinfo->company_status == 2 && $cinfo->next_billing < time() &&
                       $DB->record_exists("monorail_data", array("type" => "PAYNOTIF", "itemid" => $cohort->id))) {
                // User failed to pay
                $cdata["company_status"] = 7;
            }

            if ($cdata['isadmin'])
            {
              $cdata['admins'] = $admins;

              $invite = monorail_get_or_create_invitation('cohort', $cohort->id);
              $cohortusers = $DB->get_fieldset_select("cohort_members", 'userid', "cohortid = ?", array($cohort->id));
              $invitedusers = $DB->count_records('monorail_invite_users', array('monorailinvitesid' => $invite->id));     
              //$openinvites = $cinfo->userscount - count($cohortusers);

              $cdata['licences'] = $cinfo->userscount;
              $cdata['valid_until'] = $cinfo->valid_until;

              if ($cinfo->userscount && $cinfo->valid_until < time()) {
                // Licences have expired.
                $cdata["licences"] = 0;
              }

                $lastInvoice = $DB->get_record_sql("SELECT id, invoicenum, timepaid, totalprice FROM {monorail_cohort_invoice} " .
                        "WHERE cohortid=? AND paymentstatus=2 ORDER BY timepaid DESC LIMIT 1", array($cohort->id));

                if ($lastInvoice) {
                    $cdata["last_inv_date"] = $lastInvoice->timepaid;
                    $cdata["last_inv_num"] = $lastInvoice->invoicenum;
                    $cdata["last_inv_sum"] = $lastInvoice->totalprice;
                    $cdata["last_inv_id"] = $lastInvoice->id;
                }

              $cdata['next_billing'] = $cinfo->next_billing;
              $cdata['is_paid'] = $cinfo->is_paid;
              $cdata['domain'] = $cinfo->domain;
              $cdata['restrictdomain'] = $cinfo->restrictdomain;
              $cdata['inviteCode'] = $invite->code;
              $cdata['invitesopen'] = $invite->active;

              $cdata["billing_details"] = $cinfo->billing_details;
              $cdata["demo_period_end"] = $cinfo->demo_period_end;
              $cdata["organization_details"] = $cinfo->organization_details;
              $cdata["published_courses"] = $cinfo->published_courses;
              $cdata["public_page"] = (int) $cinfo->public_page;
              $cdata["public_page_ann"] = (int) $cinfo->public_page_ann;
              $cdata["public_page_ann_info"] = $cinfo->public_page_ann_info;
              $cdata["public_page_ad"] = (int) $cinfo->public_page_ad;
              $cdata["public_page_ad_info"] = $cinfo->public_page_ad_info;
              $cdata["public_page_cats"] = $cinfo->public_page_cats;
              $cdata["public_page_path"] = $cinfo->public_page_path;

              $cohort_course_users = $DB->get_fieldset_sql("SELECT DISTINCT ra.userid FROM {monorail_cohort_courseadmins} AS mcc " .
                    "INNER JOIN {context} AS ctx ON ctx.instanceid=mcc.courseid " .
                    "INNER JOIN {role_assignments} AS ra ON ra.contextid=ctx.id " .
                    "INNER JOIN {monorail_course_perms} AS mcp ON mcp.course=mcc.courseid " .
                        "WHERE mcc.cohortid=? AND ctx.contextlevel=50 AND mcp.privacy=2" .
                            (empty($cohortusers) ? "" : " AND NOT ra.userid IN (" . implode(", ", $cohortusers) . ")"), array($cohort->id));

              $cdata["externalusers"] = count($cohort_course_users);
              $cdata['inviteurl'] = monorail_format_invite_link($invite->code);

              // Invites are always open (except for those who missed a payment)
              /*
              if(($openinvites <= 0) && $invite->active) {
                //Deactivate the invite - users limit reached 
                $DB->set_field('monorail_invites', 'active', 0, array('code'=>$invite->code));
                $cdata['invitesopen'] = 0;
              } else {
                $DB->set_field('monorail_invites', 'active', 1, array('code'=>$invite->code));
              }
              */

              //Cohort settings
              $rsettings = array();
              $dsettings = array('cohort_course_privacy','cohort_course_privacy_ext', 'cohort_enroll_insecure'); 
              $settings = $DB->get_records('monorail_cohort_settings', array('cohortid'=>$cohort->id));
              foreach ($settings as $setting) {
                $rset = array();
                $rset['name'] = $setting->name; 
                $rset['value'] = $setting->value; 
                $rsettings[] = $rset; 
                $dsettings = array_diff($dsettings, array($setting->name));
              }
              //put default settings for settings not in db
              foreach ($dsettings as $dsetting) {
                $rset = array();
                $rset['name'] = $dsetting; 
                $rset['value'] = 0; 
                $rsettings[] = $rset; 
              }
              $cdata['settings'] = $rsettings;
              if (count($cohortusers)) {
                require_once("$CFG->dirroot/theme/monorail/skills.php");
                $cdata['skillreport'] = json_encode(monorail_get_skills_report($cohortusers));
                $cdata['activeusercount'] = count($cohortusers);
              } else {
                $cdata['activeusercount'] = 0;
              }
            }
            $result[] = $cdata;
        }
        $cohorts->close();
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.3
     */
    public static function cohorts_info_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'    => new external_value(PARAM_NUMBER, 'ID of the cohort'),
                    'name'    => new external_value(PARAM_TEXT, 'Name of the cohort'),
                    'admins'    => new external_value(PARAM_TEXT, 'Comma separated ids for admin of cohort',VALUE_OPTIONAL),
                    'googleapp' => new external_value(PARAM_INT, 'If cohort associated with gapp', VALUE_DEFAULT,0),
                    'cohorticonurl' => new external_value(PARAM_URL, 'Cohort icon URL', VALUE_OPTIONAL),
                    'inviteCode' => new external_value(PARAM_TEXT, 'cohort invite code', VALUE_OPTIONAL),
                    'licences' => new external_value(PARAM_INT, 'Total Number of invitations available for admin', VALUE_OPTIONAL),
                    'valid_until' => new external_value(PARAM_INT, 'Licence validity period end', VALUE_OPTIONAL),
                    'is_paid' => new external_value(PARAM_INT, 'Has user paid for the licences', VALUE_OPTIONAL),
                    'inviteurl' => new external_value(PARAM_URL, 'public invitation url for self enrollment',VALUE_OPTIONAL),
                    'invitesopen' => new external_value(PARAM_INT, 'cohort invitation status', VALUE_OPTIONAL),
                    'domain' => new external_value(PARAM_TEXT, 'domain',VALUE_DEFAULT,''),
                    'isadmin' => new external_value(PARAM_INT, 'If the user is admin of this cohort',VALUE_OPTIONAL),
                    'isowner' => new external_value(PARAM_INT, 'If user is the owner of this cohort',VALUE_OPTIONAL),
                    'restrictdomain' => new external_value(PARAM_INT, 'Restrict users to this domain',VALUE_DEFAULT,0),
                    'settings' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'name'  => new external_value(PARAM_TEXT, 'The name of the field'),
                                'value' => new external_value(PARAM_TEXT, 'The value of the custom field'),
                            )
                    ), 'Cohort custom settings', VALUE_OPTIONAL),
                    'skillreport' => new external_value(PARAM_RAW, 'cohort skills report as json object',VALUE_OPTIONAL),
                    'activeusercount' => new external_value(PARAM_INT, 'Total Number of users active, not including invited users', VALUE_DEFAULT, 0),
                    'company_status' => new external_value(PARAM_INT, 'Cohort status (1 - demo, 2 - regular, 3 - ngo, 4 - suspended, 5 - ngo review pending, 6 - demo ended, 7 - payment missed)', VALUE_DEFAULT, 0),
                    'billing_details' => new external_value(PARAM_RAW, 'Billing details', VALUE_OPTIONAL),
                    'demo_period_end' => new external_value(PARAM_INT, 'End date for demo period', VALUE_DEFAULT, 0),
                    'externalusers' => new external_value(PARAM_INT, 'Number of external users (course members, excluding cohort members)', VALUE_DEFAULT, 0),
                    'next_billing' => new external_value(PARAM_INT, 'Date of next billig (monthly billing)', VALUE_OPTIONAL),

                    'last_inv_date' => new external_value(PARAM_INT, 'Last invoice date', VALUE_OPTIONAL),
                    'last_inv_num' => new external_value(PARAM_INT, 'Last invoice number', VALUE_OPTIONAL),
                    'last_inv_sum' => new external_value(PARAM_FLOAT, 'Last invoice sum', VALUE_OPTIONAL),
                    'last_inv_id' => new external_value(PARAM_INT, 'Last invoice id', VALUE_OPTIONAL),

                    'organization_details' => new external_value(PARAM_RAW, 'Organization details', VALUE_OPTIONAL),
                    'published_courses' => new external_value(PARAM_RAW, 'Published course list', VALUE_OPTIONAL),
                    'public_page' => new external_value(PARAM_INT, 'Public page enabled', VALUE_OPTIONAL),
                    'public_page_ann' => new external_value(PARAM_INT, 'Public page announcement enabled', VALUE_OPTIONAL),
                    'public_page_ann_info' => new external_value(PARAM_TEXT, 'Public page announcement info', VALUE_OPTIONAL),
                    'public_page_ad' => new external_value(PARAM_INT, 'Public page announcement enabled', VALUE_OPTIONAL),
                    'public_page_ad_info' => new external_value(PARAM_TEXT, 'Public page announcement info', VALUE_OPTIONAL),
                    'public_page_cats' => new external_value(PARAM_TEXT, 'Public page announcement info', VALUE_OPTIONAL),
                    'public_page_path' => new external_value(PARAM_TEXT, 'Public page path', VALUE_OPTIONAL)

                ),VALUE_DEFAULT, array())
        );
    }

    public static function update_cohort_admins_parameters()
    {
        return new external_function_parameters(
            array(
                'userids' => new external_multiple_structure(
                    new external_value(PARAM_INT, 'user id'),
                    '0 or more user ids',
                    VALUE_DEFAULT, array()),
                'cohortid' => new external_value(PARAM_INT, 'id of the group'),
                'admin' => new external_value(PARAM_INT, 'Promote to admin if 1 else remove admin access', VALUE_DEFAULT, 0),
                'usertype' => new external_value(PARAM_INT, 'Use as usertype (if given) when making admin.', VALUE_DEFAULT, 0)
            )
        );
    }

    /**
     * Add group(cohort)
     * @param array of  $cohorts - group details
     * @return success
     * @since  Moodle 2.4
     */
    public static function update_cohort_admins($userids, $cohortid, $admin, $usertype) {
        global $CFG, $DB, $USER;

        require_once($CFG->dirroot . '/cohort/lib.php');

        $params = self::validate_parameters(self::update_cohort_admins_parameters(),
            array('userids' => $userids, 'cohortid' => $cohortid, 'admin' => $admin, 'usertype' => $usertype));

        $warnings = array();
        $result = array();
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            //Not an admin user!
            $result['warnings'] = $warnings;
            return $result;
        }

        $cohortcourses = self::cohort_courses($cohortid);
        $mroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);

        foreach ($userids as $userid)
        {
            $allUserCohorts = $DB->get_fieldset_sql("SELECT cohortid FROM {cohort_members} WHERE userid=?", array($userid));
            $okToUpdate = true;

            foreach ($allUserCohorts as $auc) {
                if ($auc != $cohortid) {
                    $okToUpdate = false;
                    break;
                }
            }

            if (!$okToUpdate) {
                $warning = array();
                $warning['item'] = 'update_cohort_admins';
                $warning['itemid'] = $userid;
                $warning['warningcode'] = '2';
                $warning['message'] = 'User belongs to another cohort';
                $warnings[] = $warning;
                continue;
            }

            $iscohortmember = $DB->get_record('cohort_members', array('userid'=>$userid, 'cohortid' => $cohortid));

            if (!$iscohortmember && $usertype != 5) {
                // Make cohort member first.
                cohort_add_member($cohortid, $userid);
            }

            if ($iscohortmember && $usertype == 5) {
                cohort_remove_member($cohortid, $userid);
                // Make sure user stays in the list.
                try {
                    $DB->execute("INSERT INTO {monorail_cohort_user_link} (cohortid, userid) VALUES (?, ?)",
                        array($cohortid, $userid));
                } catch (Exception $ex) {}
            }

            $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$userid, 'cohortid' => $cohortid));

            if (!$admin) {
                if ($auser) {
                    //Removing user as admin, make sure assign current user as admins of those courses!
                    $cadmins = $DB->get_records('monorail_cohort_courseadmins', array('adminid'=>$userid, 'cohortid' => $cohortid));
                    foreach($cadmins as $cadmin){
                        $DB->set_field('monorail_cohort_courseadmins', 'adminid', $USER->id, array('id'=>$cadmin->id));
                    }
                    $DB->delete_records('monorail_cohort_admin_users', array('id'=>$auser->id));
                }
            } else {
                if (!$auser) {
                    //Add admin record -
                    $adminrecord = new stdClass;
                    $adminrecord->cohortid = $cohortid;
                    $adminrecord->userid = $userid;
                    $adminrecord->usertype = ($usertype ? $usertype : 2);
                    $DB->insert_record('monorail_cohort_admin_users', $adminrecord);
                } else {
                    if ($auser->usertype != 1) {
                        $auser->usertype = ($usertype ? $usertype : 2);
                        $DB->update_record('monorail_cohort_admin_users', $auser);
                    }
                }
            }

            require_once($CFG->libdir . '/enrollib.php');
            foreach($cohortcourses['courses'] as $ccourse) {
                $enrol = enrol_get_plugin('manual');
                $enrolinstances = enrol_get_instances($ccourse['id'], true);
                foreach ($enrolinstances as $courseenrolinstance) {
                    if ($courseenrolinstance->enrol == "manual") {
                        $instance = $courseenrolinstance;
                        break;
                    }
                }
                $context = context_course::instance($ccourse['id']);
                if(is_enrolled($context, $userid)) {
                    //User is enrolled to course
                    if (!$admin) {
                        //UnAssign manager role
                        if(user_has_role_assignment($userid, $mroleid, $context->id)) {
                            role_unassign($mroleid, $userid, $context->id);
                            $roles = get_user_roles($context , $userid);
                            if(!$roles) {
                                $enrol->unenrol_user($instance, $userid);
                            }
                        }
                    }
                } else {
                    if($admin) {
                        //Enrol admin user as course manager
                        $enrol->enrol_user($instance, $userid, $mroleid, time(), 0, ENROL_USER_ACTIVE);
                    }
                }
            }

            wscache_reset_by_user($userid);
        }

        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Describes the return value for update_cohort_members
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function update_cohort_admins_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function get_cohort_extusers_parameters() {
        return new external_function_parameters(
            array(
                'cohortid' => new external_value(PARAM_INT, 'cohort ID') 
            )
        );
    }

    /**
     * @param array $userids  array of user ids
     * @return array An array of arrays describing users
     * @since Moodle 2.2
     */
    public static function get_cohort_extusers($cohortid) {
        global $CFG, $USER, $DB;

        $params = self::validate_parameters(self::get_cohort_extusers_parameters(),
            array('cohortid'=>$cohortid));

        $returndata = array();
        $result = array();
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            // XXX: non-admins should not even see ui for this...
            $admin = false;
            //Not an admin user of the cohort
            // Check if allowed to view users for other reasons, like teacher in a cohort course
            $sauser = $DB->get_record('monorail_cohort_susers', array('userid'=>$USER->id, 'cohortid'=>$cohortid));
            if ($sauser) {
                return $returndata;
            }
            require_once($CFG->dirroot . "/course/lib.php");
            require_once($CFG->dirroot . "/lib/accesslib.php");
            // OK there really should be a simpler way than this horrible loop ...
            $adminrecs = $DB->get_records('monorail_cohort_courseadmins', array('cohortid' => $cohortid));
            $teacher = false;
            $mroleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
            foreach ($adminrecs as $adminrec) {
                $course = $DB->get_record('course', array('id'=>$adminrec->courseid));
                 if(!$course) {
                    //course may have been deleted,  
                    continue;
                 }

                $context = context_course::instance($course->id);
                try {
                    self::validate_context($context);
                    // Check user is enrolled as teacher
                    if(user_has_role_assignment($USER->id, $mroleid, $context->id)){
                        $teacher = true;
                        break;
                    }
                } catch (Exception $ex) {
                    // nada
                }
            }
            if (! $teacher) {
                return $returndata;
            }
        } else {
            $admin = true;
        }

        $paidUsers = array();
        $teamMembers = array();
        $allUsers = array();
        $adminUsers = array();

        $cohortCourseMembers = $DB->get_recordset_sql("SELECT ra.userid AS userid, mcp.privacy AS privacy " .
            "FROM {role_assignments} AS ra " .
                "INNER JOIN {role} AS r ON r.id=ra.roleid " .
                "INNER JOIN {context} AS ctx ON ctx.id=ra.contextid AND ctx.contextlevel=50 " .
                "INNER JOIN {monorail_cohort_courseadmins} AS mcca ON mcca.courseid=ctx.instanceid " .
                "INNER JOIN {monorail_course_perms} AS mcp ON mcp.course=ctx.instanceid " .
                    "WHERE mcca.cohortid=? AND r.shortname <> 'manager'", array($cohortid));

        foreach ($cohortCourseMembers as $cm) {
            if ($cm->privacy == 2) {
                $paidUsers[] = $cm->userid;
            }

            $allUsers[] = $cm->userid;
        }

        $internalUsers = $DB->get_fieldset_sql("SELECT userid FROM {cohort_members} WHERE cohortid=?", array($cohortid));

        foreach ($internalUsers as $iu) {
            $allUsers[] = $iu;
            $paidUsers[] = $iu;
            $teamMembers[] = $iu;
        }

        $otherUsers = $DB->get_fieldset_sql("SELECT userid FROM {monorail_cohort_user_link} WHERE cohortid=?", array($cohortid));

        foreach ($otherUsers as $ou) {
            $allUsers[] = $ou;
        }

        $adminUserList = $DB->get_recordset_sql("SELECT userid, usertype FROM {monorail_cohort_admin_users} WHERE cohortid=?", array($cohortid));

        foreach ($adminUserList as $au) {
            $adminUsers[$au->userid] = $au->usertype;
        }

        $allUsers = array_unique($allUsers);
        $paidUsers = array_unique($paidUsers);
        $teamMembers = array_unique($teamMembers);

        $users = $DB->get_recordset_sql("SELECT id, firstname, lastname, lastaccess FROM {user} WHERE id IN (" . implode(", ", $allUsers) . ") ORDER BY lastname");

        $result = array();

        foreach ($users as $user)
        {
            $usertags = array();
            $tags = $DB->get_records_sql("SELECT t.id AS id, t.name AS name, ut.id AS relid FROM {monorail_usertag} AS t " .
                    "INNER JOIN {monorail_usertag_user} AS ut ON ut.tagid=t.id WHERE ut.userid=? AND t.cohortid=?",
                        array($user->id, $cohortid));

            foreach ($tags as $tag)
            {
                $usertags[] = array("id" => $tag->id, "name" => $tag->name, "relid" => $tag->relid);
            }

            $res = array(
                "id" => $user->id,
                "firstname" => $user->firstname,
                "lastname" => $user->lastname,
                "fullname" => $user->firstname . " " . $user->lastname,
                "lastaccess" => $user->lastaccess,
                "profilepichash" => monorail_get_userpic_hash($user->id),
                "tags" => $usertags,
                "ispaid" => in_array($user->id, $paidUsers) ? 1 : 0,
                "isteammember" => in_array($user->id, $teamMembers) ? 1 : 0
            );

            if (array_key_exists($user->id, $adminUsers)) {
                $res["usertype"] = $adminUsers[$user->id];
            } else {
                if ($res["isteammember"]) {
                    $res["usertype"] = 4;
                } else {
                    $res["usertype"] = 5;
                }
            }

            $result[] = $res;
        }

        $returndata = array();
        $returndata['users'] = $result;

        return $returndata;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function get_cohort_extusers_returns() {
        return new external_function_parameters(
            array('users' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id'    => new external_value(PARAM_NUMBER, 'ID of the user'),
                        'username'    => new external_value(PARAM_RAW, 'Username policy is defined in Moodle security config', VALUE_OPTIONAL),
                        'firstname'   => new external_value(PARAM_NOTAGS, 'The first name(s) of the user', VALUE_OPTIONAL),
                        'lastname'    => new external_value(PARAM_NOTAGS, 'The family name of the user', VALUE_OPTIONAL),
                        'fullname'    => new external_value(PARAM_NOTAGS, 'The fullname of the user'),
                        'email'       => new external_value(PARAM_TEXT, 'An email address - allow email as root@localhost', VALUE_OPTIONAL),
                        'icq'         => new external_value(PARAM_NOTAGS, 'icq number', VALUE_OPTIONAL),
                        'skype'       => new external_value(PARAM_NOTAGS, 'skype id', VALUE_OPTIONAL),
                        'yahoo'       => new external_value(PARAM_NOTAGS, 'yahoo id', VALUE_OPTIONAL),
                        'aim'         => new external_value(PARAM_NOTAGS, 'aim id', VALUE_OPTIONAL),
                        'msn'         => new external_value(PARAM_NOTAGS, 'msn number', VALUE_OPTIONAL),
                        'department'  => new external_value(PARAM_TEXT, 'department', VALUE_OPTIONAL),
                        'institution' => new external_value(PARAM_TEXT, 'institution', VALUE_OPTIONAL),
                        'interests'   => new external_value(PARAM_TEXT, 'user interests (separated by commas)', VALUE_OPTIONAL),
                        'courses'   => new external_value(PARAM_TEXT, 'Courses as string with fullname', VALUE_OPTIONAL),
                        'firstaccess' => new external_value(PARAM_INT, 'first access to the site (0 if never)', VALUE_OPTIONAL),
                        'lastaccess'  => new external_value(PARAM_INT, 'last access to the site (0 if never)', VALUE_OPTIONAL),
                        'lastaccessdate'  => new external_value(PARAM_TEXT, 'last access to the site datestring', VALUE_OPTIONAL),
                        'city'        => new external_value(PARAM_NOTAGS, 'Home city of the user', VALUE_OPTIONAL),
                        'profilepichash' => new external_value(PARAM_TEXT, 'Profile pic hash', VALUE_OPTIONAL),
                        'country'     => new external_value(PARAM_ALPHA, 'Home country code of the user, such as AU or CZ', VALUE_OPTIONAL),
                        'maildisplay'     => new external_value(PARAM_INT, 'Display mail of he user', VALUE_OPTIONAL),
                        'suspended' => new external_value(PARAM_INT, '1 if user suspended else 0'),
                        'admin' => new external_value(PARAM_INT, '1 if user suspended else 0'),
                        'ispaid'  => new external_value(PARAM_INT, 'Is user paid (participates in private courses)?', VALUE_OPTIONAL),
                        'isteammember'  => new external_value(PARAM_INT, 'Is user a team member?', VALUE_OPTIONAL),
                        'usertype' => new external_value(PARAM_INT, 'user role in cohort (1 - owner, 2 - admin, 3 - observer, 4 - org. teacher, 5 - non-cohort member)', VALUE_OPTIONAL),
                        'customfields' => new external_multiple_structure(
                            new external_single_structure(
                                array(
                                    'type'  => new external_value(PARAM_ALPHANUMEXT, 'The type of the custom field - text field, checkbox...', VALUE_OPTIONAL),
                                    'value' => new external_value(PARAM_TEXT, 'The value of the custom field', VALUE_OPTIONAL),
                                    'name' => new external_value(PARAM_TEXT, 'The name of the custom field', VALUE_OPTIONAL),
                                    'shortname' => new external_value(PARAM_TEXT, 'The shortname of the custom field', VALUE_OPTIONAL),
                                )
                            ), 'User custom fields (also known as user profil fields)', VALUE_OPTIONAL),
                        'enrolledcourses' => new external_multiple_structure(
                            new external_single_structure(
                                array(
                                    'id'  => new external_value(PARAM_INT, 'Id of the course'),
                                    'fullname'  => new external_value(PARAM_TEXT, 'Fullname of the course'),
                                    'code'  => new external_value(PARAM_TEXT, 'course code'),
                                    'shortname' => new external_value(PARAM_TEXT, 'Shortname of the course')
                                )
                            ), 'Courses where the user is enrolled - limited by which courses the user is able to see', VALUE_OPTIONAL),
                        'skills' => new external_multiple_structure(
                            new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'skill list', VALUE_OPTIONAL
                        ),
                        'tags' => new external_multiple_structure(
                            new external_single_structure(array(
                                'id'  => new external_value(PARAM_INT, 'Tag id'),
                                'relid'  => new external_value(PARAM_INT, 'Relation id'),
                                'name'  => new external_value(PARAM_TEXT, 'Tag name')
                            ))
                        )
                    )
                ), 'Cohort Ext users details', VALUE_OPTIONAL)
            )
        );
    }


    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function get_cohort_users_parameters() {
        return new external_function_parameters(
            array(
                'cohortid' => new external_value(PARAM_INT, 'cohort ID') 
            )
        );
    }

    /**
     * @param array $userids  array of user ids
     * @return array An array of arrays describing users
     * @since Moodle 2.2
     */
    public static function get_cohort_users($cohortid) {
        global $CFG, $USER, $DB;

        $params = self::validate_parameters(self::get_cohort_users_parameters(),
            array('cohortid'=>$cohortid));

        $returndata = array();
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            return $returndata;

            /* Everything is much more simple now...
             *
            $admin = false;
            //Not an admin user of the cohort
            // Check if allowed to view users for other reasons, like teacher in a cohort course
            $sauser = $DB->get_record('monorail_cohort_susers', array('userid'=>$USER->id, 'cohortid'=>$cohortid));
            if ($sauser) {
                return $returndata;
            }
            require_once($CFG->dirroot . "/course/lib.php");
            require_once($CFG->dirroot . "/lib/accesslib.php");
            // OK there really should be a simpler way than this horrible loop ...
            $adminrecs = $DB->get_records('monorail_cohort_courseadmins', array('cohortid' => $cohortid));
            $teacher = false;
            $mroleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
            foreach ($adminrecs as $adminrec) {
                $course = $DB->get_record('course', array('id'=>$adminrec->courseid));
                 if(!$course) {
                    //course may have been deleted,  
                    continue;
                 }

                $context = context_course::instance($course->id);
                try {
                    self::validate_context($context);
                    // Check user is enrolled as teacher
                    if(user_has_role_assignment($USER->id, $mroleid, $context->id)){
                        $teacher = true;
                        break;
                    }
                } catch (Exception $ex) {
                    // nada
                }
            }
            if (! $teacher) {
            }
            */
        }

        //User ids of cohort members

        /*
        $userids = array();
        $userData = array();

        $userList = $DB->get_recordset_sql("SELECT id, userid, usertype FROM {monorail_cohort_admin_users} WHERE cohortid=?", array($cohortid));

        foreach ($userList as $usr) {
            $userids[] = $usr->userid;
            $userData[$usr->userid] = $usr;
        }

        $suserids = $DB->get_fieldset_select('monorail_cohort_susers', 'userid', 'cohortid = ?', array($cohortid)); 
        $cuserids = array_merge($userids, $suserids);

        list($uselect, $ujoin) = context_instance_preload_sql('u.id', CONTEXT_USER, 'ctx');
        list($sqluserids, $params) = $DB->get_in_or_equal($cuserids);
        $usersql = "SELECT u.* $uselect
        FROM {user} u $ujoin
        WHERE u.id $sqluserids ORDER BY u.lastname,u.firstname ASC";
        */

        $users = $DB->get_recordset_sql("SELECT u.id AS id, u.deleted AS deleted, u.firstname AS firstname, u.lastname AS lastname, " .
                "mcau.usertype AS usertype, u.lastaccess AS lastaccess, mcau.id AS mcid " .
            "FROM {cohort_members} AS cm " .
                "INNER JOIN {user} AS u ON cm.userid=u.id " .
                "LEFT JOIN {monorail_cohort_admin_users} AS mcau ON mcau.cohortid=cm.cohortid AND mcau.userid=cm.userid " .
                    "WHERE cm.cohortid=?", array($cohortid));
   
        $result = array();

        //require_once($CFG->dirroot . '/enrol/externallib.php');
        //require_once($CFG->dirroot . "/user/lib.php");

        foreach ($users as $user) {
            if ($user->deleted) {
                continue;
            }

            /*
            $uinfo = $DB->get_record('user', array('id' => $user->id));

            $fullname = fullname($user->id);

            //Custom fields
            $fields = $DB->get_recordset_sql("SELECT f.*
                                            FROM {user_info_field} f
                                            JOIN {user_info_category} c
                                                 ON f.categoryid=c.id");
            $customfields = array();
            foreach ($fields as $field) {
                require_once($CFG->dirroot.'/user/profile/lib.php');
                require_once($CFG->dirroot.'/user/profile/field/'.$field->datatype.'/field.class.php');
                $newfield = 'profile_field_'.$field->datatype;
                $formfield = new $newfield($field->id, $uinfo->id);
                if ($formfield->is_visible() and !$formfield->is_empty()) {
                    $customfields[] =
                        array('name' => $formfield->field->name, 'value' => $formfield->data,
                        '    type' => $field->datatype, 'shortname' => $formfield->field->shortname);
                }
            }
            $fields->close();

            $euser = array('id' => $uinfo->id, 'firstname' => $uinfo->firstname,'lastname' => $uinfo->lastname,
                           'username' => $uinfo->username,'fullname' => $fullname,'email' => $uinfo->email, 
                           'profilepichash' => $profilepichash,'maildisplay' => $uinfo->maildisplay, 
                           'enrolledcourses' => array(), 'lastaccess'=>$uinfo->lastaccess, 'customfields'=> $customfields,
                           'icq'=>$uinfo->icq, 'skype'=>$uinfo->skype, 'yahoo'=>$uinfo->yahoo, 'city'=>$uinfo->city, 'country'=>$uinfo->country,
                           'aim'=>$uinfo->aim, 'msn'=>$uinfo->msn, 'department'=>$uinfo->department, 'institution'=>$uinfo->institution,
                           'description'=>$uinfo->description, 'lastaccessdate' => date("d.m.Y, H:i", $uinfo->lastaccess));
            if(in_array($user->id, $suserids)){
                $euser['suspended'] = 1;
            } else {
                $euser['suspended'] = 0; 
            }

            $euser["usertype"] = $userData[$uinfo->id]->usertype;

            // tags
            $usertags = array();
            $tags = $DB->get_records_sql("SELECT t.id AS id, t.name AS name, ut.id AS relid FROM {monorail_usertag} AS t " .
                    "INNER JOIN {monorail_usertag_user} AS ut ON ut.tagid=t.id WHERE ut.userid=?", array($user->id));

            foreach ($tags as $tag)
            {
                $usertags[] = array("id" => $tag->id, "name" => $tag->name, "relid" => $tag->relid);
            }

            $euser["tags"] = $usertags;
            */

            $result[] = array(
                "id" => $user->id,
                "firstname" => $user->firstname,
                "lastname" => $user->lastname,
                "usertype" => ($user->mcid && $user->usertype ? $user->usertype : 4),
                "profilepichash" => monorail_get_userpic_hash($user->id),
                "lastaccess" => $user->lastaccess
            );
        }

        $users->close();

        $returndata = array();
        $returndata['users'] = $result;

        $invite = monorail_get_or_create_invitation('cohort', $cohortid);

        $ivUsers = array();
        $invitedusers = $DB->get_recordset_sql("SELECT id, email, timemodified, status FROM {monorail_invite_users} WHERE monorailinvitesid=?",
            array($invite->id));

        foreach ($invitedusers as $iu) {
            $ivUsers[] = array(
                "email" => $iu->email,
                "status" => $iu->status,
                "date" => $iu->timemodified
            );
        }

        $returndata['invitedusers'] = $ivUsers;

        return $returndata;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function get_cohort_users_returns() {
        return new external_function_parameters(
            array('users' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id'    => new external_value(PARAM_NUMBER, 'ID of the user'),
                        'firstname'   => new external_value(PARAM_NOTAGS, 'The first name(s) of the user', VALUE_OPTIONAL),
                        'lastname'    => new external_value(PARAM_NOTAGS, 'The family name of the user', VALUE_OPTIONAL),
                        'usertype' => new external_value(PARAM_INT, 'user role in cohort (1 - owner, 2 - admin, 3 - observer, 4 - org. teacher)', VALUE_OPTIONAL),
                        'profilepichash' => new external_value(PARAM_TEXT, 'Profile pic hash', VALUE_OPTIONAL),
                        'lastaccess'  => new external_value(PARAM_INT, 'last access to the site (0 if never)', VALUE_OPTIONAL)
                        /*
                        'email'       => new external_value(PARAM_TEXT, 'An email address - allow email as root@localhost', VALUE_OPTIONAL),
                        'icq'         => new external_value(PARAM_NOTAGS, 'icq number', VALUE_OPTIONAL),
                        'skype'       => new external_value(PARAM_NOTAGS, 'skype id', VALUE_OPTIONAL),
                        'yahoo'       => new external_value(PARAM_NOTAGS, 'yahoo id', VALUE_OPTIONAL),
                        'aim'         => new external_value(PARAM_NOTAGS, 'aim id', VALUE_OPTIONAL),
                        'msn'         => new external_value(PARAM_NOTAGS, 'msn number', VALUE_OPTIONAL),
                        'department'  => new external_value(PARAM_TEXT, 'department', VALUE_OPTIONAL),
                        'institution' => new external_value(PARAM_TEXT, 'institution', VALUE_OPTIONAL),
                        'interests'   => new external_value(PARAM_TEXT, 'user interests (separated by commas)', VALUE_OPTIONAL),
                        'firstaccess' => new external_value(PARAM_INT, 'first access to the site (0 if never)', VALUE_OPTIONAL),
                        'lastaccessdate'  => new external_value(PARAM_TEXT, 'last access to the site datestring', VALUE_OPTIONAL),
                        'city'        => new external_value(PARAM_NOTAGS, 'Home city of the user', VALUE_OPTIONAL),
                        'country'     => new external_value(PARAM_ALPHA, 'Home country code of the user, such as AU or CZ', VALUE_OPTIONAL),
                        'maildisplay'     => new external_value(PARAM_INT, 'Display mail of he user', VALUE_OPTIONAL),
                        'suspended' => new external_value(PARAM_INT, '1 if user suspended else 0'),
                        'customfields' => new external_multiple_structure(
                            new external_single_structure(
                                array(
                                    'type'  => new external_value(PARAM_ALPHANUMEXT, 'The type of the custom field - text field, checkbox...', VALUE_OPTIONAL),
                                    'value' => new external_value(PARAM_TEXT, 'The value of the custom field', VALUE_OPTIONAL),
                                    'name' => new external_value(PARAM_TEXT, 'The name of the custom field', VALUE_OPTIONAL),
                                    'shortname' => new external_value(PARAM_TEXT, 'The shortname of the custom field', VALUE_OPTIONAL),
                                )
                            ), 'User custom fields (also known as user profil fields)', VALUE_OPTIONAL),
                        'enrolledcourses' => new external_multiple_structure(
                            new external_single_structure(
                                array(
                                    'id'  => new external_value(PARAM_INT, 'Id of the course'),
                                    'fullname'  => new external_value(PARAM_TEXT, 'Fullname of the course'),
                                    'code'  => new external_value(PARAM_TEXT, 'course code'),
                                    'shortname' => new external_value(PARAM_TEXT, 'Shortname of the course')
                                )
                            ), 'Courses where the user is enrolled - limited by which courses the user is able to see', VALUE_OPTIONAL),
                        'skills' => new external_multiple_structure(
                            new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'skill list', VALUE_OPTIONAL
                        ),
                        'tags' => new external_multiple_structure(
                            new external_single_structure(array(
                                'id'  => new external_value(PARAM_INT, 'Tag id'),
                                'relid'  => new external_value(PARAM_INT, 'Relation id'),
                                'name'  => new external_value(PARAM_TEXT, 'Tag name')
                            ))
                        )
                        */
                    )
                ), 'Cohort users details', VALUE_OPTIONAL),
                'invitedusers' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'email'  => new external_value(PARAM_TEXT, 'Email'),
                            'status'  => new external_value(PARAM_INT, 'Invitation status'),
                            'date' => new external_value(PARAM_INT, 'Invitation date')
                        )
                    ), 'Users invited to the cohort.', VALUE_OPTIONAL)
            )
        );
    }



   /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function cohort_courses_parameters() {
        return new external_function_parameters(
            array(
                'cohortid' => new external_value(PARAM_INT, 'cohort ID') 
            )
        );
    }

    /**
     * @param array $cohortid id of the cohort
     * @return array An array of arrays describing users
     * @since Moodle 2.2
     */
    public static function cohort_courses($cohortid) {
        global $CFG, $USER, $DB;

        $params = self::validate_parameters(self::cohort_courses_parameters(),
            array('cohortid'=>$cohortid));

        $returndata = array();
        $result = array();

        //$usercourseids = array();

        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));

        if (!$auser) {
            //Not an admin user of the cohort
            //$usercourses = enrol_get_users_courses($USER->id, false); 
            //foreach ($usercourses as $id => $course) {
            //    array_push($usercourseids, $id);
            //}

            // Only admins can see this list now...
            $returndata['courses'] = $result;
            return $returndata;
        }

        $cadmins = $DB->get_records('monorail_cohort_courseadmins', array('cohortid' => $cohortid));

        //$mroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'), MUST_EXIST);
        //require_once($CFG->libdir . '/enrollib.php');

        foreach($cadmins as $cadmin)
        {
           $course = $DB->get_record('course', array('id'=>$cadmin->courseid),'id,fullname,shortname,category');
           if(!$course) {
               continue;
           }
           $courseinvites = monorail_get_or_create_invitation('course',$cadmin->courseid);
            $ci = monorail_get_course_details($course->id);

           /*
           if (($ci->course_access == 1) && !$auser && !in_array($course->id,$usercourseids)) {
               continue;
           }
           */

           /*
           if($auser) {
               //This user should have manager role
               $context = context_course::instance($course->id); 
               if(is_enrolled($context, $USER->id)) {
                   if(!user_has_role_assignment($USER->id, $mroleid, $context->id)) {
                       role_assign($mroleid, $USER->id, $context->id);
                   }
               } else {
                   $enrol = enrol_get_plugin('manual');
                   $enrolinstances = enrol_get_instances($course->id, true);
                   foreach ($enrolinstances as $courseenrolinstance) {
                       if ($courseenrolinstance->enrol == "manual") {
                            $instance = $courseenrolinstance;
                            break;
                        }
                   }
                   $enrol->enrol_user($instance, $USER->id, $mroleid, time(), 0, ENROL_USER_ACTIVE);
                   wscache_reset_by_dependency("course_info_" . $cadmin->courseid);
               }
           }
           */

            $carray = array(
                'id' => $cadmin->courseid,
                'fullname' => $ci->fullname,
                'shortname' => $ci->shortname,
                'code' => $ci->code, 
                'invitecode' => $courseinvites->code, 
                'invitesopen'=> $courseinvites->active, 
                'course_privacy' => $ci->course_privacy,
                'course_access' => $ci->course_access,
                'teacher_name' => $ci->teachername,
                'teacher_title' => $ci->teachertitle,
                'teacher_avatar' => $ci->teacherlogo,
                'course_background' => $ci->coursebackground,
                'course_backgroundtn' => $ci->coursebackgroundtn,
                'completed' => ($ci->coursestatus) ? 0 : 1,
                'course_logo' => $ci->courselogo,
                'participants' => $ci->students,
                'price' => $ci->price,
                'category' => $course->category,
                "cert_enabled" => $ci->cert_enabled
            );

            $result[] = $carray;
        }

        $returndata['courses'] = $result;
        return $returndata;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function cohort_courses_returns() {
        return new external_single_structure(
            array('courses' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id'        => new external_value(PARAM_INT, 'course id'),
                        'fullname'  => new external_value(PARAM_TEXT, 'course full name'),
                        'shortname' => new external_value(PARAM_TEXT, 'course short name'),
                        'intro' => new external_value(PARAM_RAW, 'intro'),
                        'code' => new external_value(PARAM_TEXT, 'course code'),
                        'invitecode' => new external_value(PARAM_TEXT, 'course invite code'),
                        'invitesopen' => new external_value(PARAM_INT, 'course invitation status', VALUE_OPTIONAL),
                        'course_privacy' => new external_value(PARAM_INT, 'course public=1, private=2', VALUE_OPTIONAL),
                        'course_access' => new external_value(PARAM_INT, 'course access draft=1, invite=2, open=3', VALUE_OPTIONAL),
                        'course_review' => new external_value(PARAM_INT, 'course review status', VALUE_OPTIONAL),
                        'teacher_name' => new external_value(PARAM_TEXT, 'teacher name', VALUE_OPTIONAL),
                        'teacher_title' => new external_value(PARAM_TEXT, 'teacher title', VALUE_OPTIONAL),
                        'teacher_avatar' => new external_value(PARAM_TEXT, 'teacher avatar', VALUE_OPTIONAL),
                        'course_background' => new external_value(PARAM_TEXT, 'course background', VALUE_OPTIONAL),
                        'course_backgroundtn' => new external_value(PARAM_TEXT, 'course background', VALUE_OPTIONAL),
                        'course_logo' => new external_value(PARAM_TEXT, 'course logo', VALUE_OPTIONAL),
                        'participants' => new external_value(PARAM_INT, 'participant number', VALUE_OPTIONAL),
                        'price' => new external_value(PARAM_FLOAT, 'Course price', VALUE_OPTIONAL),
                        'completed' => new external_value(PARAM_INT, 'completed'),
                        'skills'    => new external_multiple_structure(
                            new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'list of skills', VALUE_DEFAULT, array()),
                        'category' => new external_value(PARAM_INT, 'course category', VALUE_OPTIONAL),
                        'cert_enabled' => new external_value(PARAM_INT, 'certificates enabled', VALUE_OPTIONAL)
                    )), 'Courses belonging to the cohort', VALUE_OPTIONAL)
            ));
    }

   /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function cohort_course_users_parameters() {
        return new external_function_parameters(
            array(
                'cohortid' => new external_value(PARAM_INT, 'cohort ID'), 
                'courseid' => new external_value(PARAM_INT, 'course ID') 
            )
        );
    }

    /**
     * @param array $cohortid id of the cohort
     * @return array An array of arrays describing users
     * @since Moodle 2.2
     */
    public static function cohort_course_users($cohortid, $courseid) {
        global $CFG, $USER, $DB;

        $params = self::validate_parameters(self::cohort_course_users_parameters(),
            array('cohortid'=>$cohortid, 'courseid'=>$courseid));

        $result = array();
        $context = context_course::instance($courseid);


       //check if user is an admin for this cohort
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            $admin = false;
            $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
            $context = context_course::instance($courseid);
            try {
                self::validate_context($context);
                // We cannot check for course:delete since no one has that except admins
                require_capability('moodle/course:create', $context);
                // Check user is enrolled as teacher
                $users = get_enrolled_users($context);
                $teacher = false;
                foreach ($users as $user) {
                    if ($user->id === $USER->id) {
                        // get user role
                        $roles = get_user_roles($context , $user->id);
                        foreach ($roles as $role) {
                            if ($role->name === 'Teacher') {
                                $teacher = true;
                                break;
                            }
                        }
                        break;
                    }
                }
                if (! $teacher) {
                    return $result;
                }
            } catch (Exception $e) {
                return $result;
            }
        } else {
            $admin = true;
        }

       //COHORT admin user should have access to the below user details

        //===========================MOODLE - get_enrolled_users/user_details modified=============================================
        list($enrolledsql, $enrolledparams) = get_enrolled_sql($context);
        $sqlparams['courseid'] = $courseid;
        $sql = "SELECT u.* 
                  FROM {user} u 
                 WHERE u.id IN ($enrolledsql)
                 ORDER BY u.id ASC";
        $enrolledusers = $DB->get_recordset_sql($sql, $enrolledparams);

        $eusers = array();
        foreach ($enrolledusers as $user) {
           $uinfo = $DB->get_record('user', array('id' => $user->id),
                 'firstname, lastname, username, email, id, maildisplay');

           $euser = array('id' => $uinfo->id, 'firstname' => $uinfo->firstname,'lastname' => $uinfo->lastname,
                          'username' => $uinfo->username,'email' => $uinfo->email, 'maildisplay' => $uinfo->maildisplay);

           $roles = get_user_roles($context, $user->id, false);
           $euser['roles'] = array();
           foreach ($roles as $role) {
                $euser['roles'][] = array(
                'roleid'       => $role->roleid,
                'name'         => $role->name,
                'shortname'    => $role->shortname,
                'sortorder'    => $role->sortorder
              );
           }

           $eusers[] = $euser;
        }
        $enrolledusers->close();
        //===================================================================================================
        $neusers = array();
        $invitedusers = array();
        if ($admin) {
            //Invited users to the course; who have not yet enrolled to eliademy
            $invite = monorail_get_or_create_invitation('course', $courseid);
            $invitedusers = $DB->get_fieldset_select('monorail_invite_users', 'email', 'monorailinvitesid = ?', array($invite->id)); 
            //Get users who are not enrolled
            $cohortmembers = $DB->get_fieldset_select('cohort_members', 'userid', 'cohortid = ?', array($cohortid));
            foreach ($eusers as $euser) {
                foreach (array_keys($cohortmembers, $euser['id']) as $key) {
                   unset($cohortmembers[$key]);
                }  
            }
            foreach ($cohortmembers as $cmember) {
               $uinfo = $DB->get_record('user', array('id' => $cmember),
                     'firstname, lastname, username, email, id, maildisplay');
               $neuser = array('id' => $uinfo->id, 'firstname' => $uinfo->firstname,'lastname' => $uinfo->lastname,
                              'username' => $uinfo->username,'email' => $uinfo->email, 'maildisplay' => $uinfo->maildisplay);
               $neusers[] = $neuser;
            }
        }
        $returndata = array();
        $returndata['eusers'] = $eusers;
        $returndata['neusers'] = $neusers;
        $returndata['invitedusers'] = implode(',',$invitedusers);
        return $returndata;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function cohort_course_users_returns() {
        return new external_function_parameters(
            array(
               'eusers' => new external_multiple_structure (
                  new external_single_structure(
                    array(
                    'id'    => new external_value(PARAM_NUMBER, 'ID of the user'),
                    'username'    => new external_value(PARAM_RAW, 'Username policy is defined in Moodle security config', VALUE_OPTIONAL),
                    'firstname'    => new external_value(PARAM_NOTAGS, 'The firstname of the user',VALUE_OPTIONAL),
                    'lastname'    => new external_value(PARAM_NOTAGS, 'The lastname of the user',VALUE_OPTIONAL),
                    'email'       => new external_value(PARAM_TEXT, 'An email address - allow email as root@localhost', VALUE_OPTIONAL),
                    'maildisplay'       => new external_value(PARAM_INT, 'Display mail for user', VALUE_OPTIONAL),
                    'roles' => new external_multiple_structure(new external_single_structure(array(
                        'roleid' => new external_value(PARAM_INT, 'role id'),
                        'name' => new external_value(PARAM_TEXT, 'name'),
                        'shortname' => new external_value(PARAM_TEXT, 'shortname'),
                        'sortorder' => new external_value(PARAM_INT, 'sortorder')), "role"), VALUE_OPTIONAL),
                    )), 'Cohort enrolled users details',VALUE_OPTIONAL),
                'neusers' => new external_multiple_structure (
                  new external_single_structure(
                    array(
                    'id'    => new external_value(PARAM_NUMBER, 'ID of the user'),
                    'username'    => new external_value(PARAM_RAW, 'Username policy is defined in Moodle security config', VALUE_OPTIONAL),
                    'firstname'    => new external_value(PARAM_NOTAGS, 'The firstname of the user'),
                    'lastname'    => new external_value(PARAM_NOTAGS, 'The lastname of the user'),
                    'email'       => new external_value(PARAM_TEXT, 'An email address - allow email as root@localhost', VALUE_OPTIONAL),
                    'maildisplay'       => new external_value(PARAM_INT, 'Display mail for user', VALUE_OPTIONAL),
                    )), 'Cohort not enrolled users details',VALUE_OPTIONAL),
                'invitedusers' => new external_value(PARAM_TEXT, 'Email ids of users invited to course',VALUE_OPTIONAL)
            ));
    }

   /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function revoke_inviteduser_parameters() {
        return new external_function_parameters(
            array(
                'useremail' => new external_value(PARAM_EMAIL, 'Email id to which invite was sent'),
                'cohortid' => new external_value(PARAM_INT, 'Id of the cohort'),
                'type' => new external_value(PARAM_ALPHANUM, 'Type of invite',VALUE_DEFAULT, 'cohort') 
            )
        );
    }

    /**
     * @param array $email of the user
     * @param array $cohortid id of the cohort
     * @param array $type course/cohort 
     * @return array An array of arrays describing users
     * @since Moodle 2.2
     */
    public static function revoke_inviteduser($email, $cohortid, $type) {
        global $CFG, $USER, $DB;

        $params = self::validate_parameters(self::revoke_inviteduser_parameters(),
            array('useremail'=>$email,'cohortid' => $cohortid, 'type' => $type));

        $result = array();
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            //Not an admin user of the cohort
            $warning = array();
            $warning['item'] = 'revoke_inviteduser';
            $warning['itemid'] = $cohortid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Only admin user can access this API';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        } 
        $warnings = array();

        $invite = monorail_get_or_create_invitation($type, $cohortid);
        if($invite) {
            $DB->delete_records('monorail_invite_users', array('email'=>$email, 'monorailinvitesid'=>$invite->id));

            /*
            if(!$invite->active) {
                $cinfo = $DB->get_record('monorail_cohort_info', array('cohortid' => $cohortid));
                $cohortusers = $DB->count_records("cohort_members", array("cohortid" => $cohortid));
                $openinvites = $cinfo->userscount - $cohortusers; 
                if($openinvites > 0) {
                    $DB->set_field('monorail_invites', 'active', 1, array('code'=>$invite->code));
                }
            }
            */
        }

        $result['warnings'] = $warnings;
        return $result;
    }

    public static function get_timezones_parameters() {
        return new external_function_parameters(array());
    }

    /**
     * Get timezones (XXX: not used anymore?)
     */
    public static function get_timezones() {
        global $DB;

        $results = array();
        $timezones = get_list_of_timezones();

        foreach ($timezones as $timezone)
        {
            $timezone = str_replace('_', ' ', $timezone);

            $results[] = array(
                'value' => $timezone,
                'tokens' => array_merge(array($timezone), explode('/', $timezone))
            );
        }

        return $results;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function revoke_inviteduser_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function cohort_course_enrol_parameters() {
        return new external_function_parameters(
            array(
                'enrolments' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'rolename'    => new external_value(PARAM_TEXT, 'Role to assign to the user - coursecreator,editingteacher,frontpage,
                                guest,manager,student,teacher,teachercopy,user,wuser'),
                            'inviteid' => new external_value(PARAM_TEXT, 'Invitation id sent to user', VALUE_OPTIONAL),
                            'courseid' => new external_value(PARAM_INT, 'Course id'),
                            'cohortid' => new external_value(PARAM_INT, 'Cohort id', VALUE_OPTIONAL),
                            'userid' => new external_value(PARAM_INT, 'The user that is going to be enrolled'),
                            'timestart' => new external_value(PARAM_INT, 'Timestamp when the enrolment start', VALUE_OPTIONAL),
                            'timeend' => new external_value(PARAM_INT, 'Timestamp when the enrolment end', VALUE_OPTIONAL),
                            'suspend' => new external_value(PARAM_INT, 'set to 1 to suspend the enrolment', VALUE_OPTIONAL)
                        )),'Enrollment data for user',VALUE_DEFAULT,array()),
                'invitedusers' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'rolename'    => new external_value(PARAM_TEXT, 'Role to assign to the user - coursecreator,editingteacher,frontpage,
                                guest,manager,student,teacher,teachercopy,user,wuser'),
                            'email' => new external_value(PARAM_EMAIL, 'Email of user'),
                            'inviteid' => new external_value(PARAM_TEXT, 'Invitation id sent to user'),
                            'courseid' => new external_value(PARAM_INT, 'Course id'),
                            'timestart' => new external_value(PARAM_INT, 'Timestamp when the enrolment start', VALUE_OPTIONAL),
                            'timeend' => new external_value(PARAM_INT, 'Timestamp when the enrolment end', VALUE_DEFAULT, 0),
                            'suspend' => new external_value(PARAM_INT, 'set to 1 to suspend the enrolment', VALUE_DEFAULT, 0)
                        )),'Enrollment data for user who has been invited to cohort/course',VALUE_DEFAULT,array())
            )
        );
    }
 
    /**
     * Enrolment of users
     *
     * Function throw an exception at the first error encountered.
     * @param array $enrolments  An array of user enrolment
     * @since Moodle 2.2
     */
    public static function cohort_course_enrol($enrolments, $invitedusers) {
        global $DB, $CFG, $USER;

        $params = self::validate_parameters(self::cohort_course_enrol_parameters(),
            array('enrolments'=>$enrolments,'invitedusers'=>$invitedusers));

        $warnings = array();
        $result = array();
        //Normal enrol for users with accounts
        if(!empty($enrolments)) {
            $response = self::enrol_users($enrolments, 1);
            //Need to send emails to these users!!
            //FIXME: NOT CHECKED WHETHER ENROLMENT WAS SUCCESFULL OR NOT - should do that
            foreach ($enrolments as $enrolment) {
                    //User is enrolled to the course - confirmed!
                    $coursecode = $DB->get_field('monorail_course_data', 'code', array('courseid'=>$enrolment['courseid']),MUST_EXIST);
                    $coursename = $DB->get_field('course', 'fullname', array('id'=>$enrolment['courseid']),MUST_EXIST);
                    // XXX: companyname never used, disabling. If it's
                    // needed, several palces where this method was used
                    // must be updated to provide cohortid.
                    //$companyname = $DB->get_field('cohort', 'name', array('id'=>$enrolment['cohortid']),MUST_EXIST);

                    $subject = monorail_template_compile(get_string('email_subject_enroll','theme_monorail'), array(
                                              'courseName' => $coursename, 'cohortadminName' => fullname($USER)));
                    $link = $CFG->magic_ui_root.'/courses/'.$coursecode;
                    $pargs = array('cohortadminName' => fullname($USER), 'courseName' => $coursename, 'link' => $link);
                    try {
                        monorail_send_info_mail('mail/cohortcourseenroll', $subject, $pargs, $enrolment['userid']); 
                    } catch (Exception $ex) {
                        add_to_log($enrolment['courseid'], 'monorailservices', 'externallib', 'cohort_course_enrol', 'Error from monorail_send_info_mail: '.$ex->getMessage());
                    }

                wscache_reset_by_dependency("course_info_" . $enrolment['courseid']);
                wscache_reset_by_user($enrolment['userid']);
            }
        }
        require_once($CFG->dirroot . '/enrol/externallib.php');
        //For users without eliademy account!
        foreach($invitedusers as $inviteduser) {
            $record = new stdClass;
            $record->inviteid = $inviteduser['inviteid'];
            $record->rolename = $inviteduser['rolename'];
            $record->email = $inviteduser['email'];
            $record->courseid = $inviteduser['courseid'];
            $record->timeend = $inviteduser['timeend'];
            $record->timestart = isset($inviteduser['timestart']) ? $inviteduser['timestart'] : time();
            (isset($inviteduser['suspend']) && !empty($inviteduser['suspend'])) ?
            ENROL_USER_SUSPENDED : ENROL_USER_ACTIVE;
            $DB->insert_record('monorail_cohort_crsinvitee', $record);
        }     

        $result['warnings'] = $warnings;
        return $result;
    }
 
   /**
     * @return null
     * @since Moodle 2.2
     */
    public static function cohort_course_enrol_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }
//=============================================

   /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function course_set_status_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID'),
                'status' => new external_value(PARAM_INT, 'Course status')
            )
        );
    }

   /**
     * Set course status
     */
    public static function course_set_status($courseid, $status) {
        global $DB, $CFG;

        $params = self::validate_parameters(self::course_set_status_parameters(),
            array('courseid' => $courseid, 'status' => $status));

        $warnings = array();
        $result = array();

        // check access to course
        $context = context_course::instance($courseid);
        try {
            self::validate_context($context);
            require_capability('moodle/course:update', $context);
        } catch (Exception $e) {
            $warning = array();
            $warning['item'] = 'course';
            $warning['itemid'] = $courseid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'No access rights in course context to set status';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }

        try {
            if ($status === 1) {
                // complete
                monorail_process_course_end($courseid);
            } else if ($status === 0) {
                // activate
                $coursedata = $DB->get_record('monorail_course_data', array('courseid'=>$courseid), '*', MUST_EXIST);
                $coursedata->enddate = 0;
                $coursedata->status = 1;
                $coursedata->timemodified = time();
                $DB->update_record('monorail_course_data', $coursedata);
            } else {
                $warning = array();
                $warning['item'] = 'course';
                $warning['itemid'] = $courseid;
                $warning['warningcode'] = '1';
                $warning['message'] = 'Unknown status for course';
                $warnings[] = $warning;
                $result['warnings'] = $warnings;
                return $result;
            }

            addBackgroundCall(function () use ($courseid)
            {
                $api = magento_get_instance();

                if ($api)
                {
                    $api->begin();
                    $api->updateCourseStatus($courseid);
                }
            });

            wscache_reset_by_dependency("course_info_" . $courseid);

        } catch (Exception $e) {
            $warning = array();
            $warning['item'] = 'course';
            $warning['itemid'] = $courseid;
            $warning['warningcode'] = '1';
            $warning['message'] = 'Error when setting course status';
            $warnings[] = $warning;
            $result['warnings'] = $warnings;
            return $result;
        }
        $result['warnings'] = $warnings;
        return $result;
    }

   /**
     * @return null
     * @since Moodle 2.2
     */
    public static function course_set_status_returns() {
        return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    /* @return $categories
     *
     */
    public static function get_timezones_returns() {
        return new external_multiple_structure (
            new external_single_structure(
                array(
                    'value' => new external_value(PARAM_TEXT, 'value'),
                    'tokens' => new external_multiple_structure(
                        new external_value(PARAM_TEXT, 'token')
                    )
                )
            )
        );
    }    

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function cohort_gapp_users_parameters() {
        return new external_function_parameters(
            array(
                'cohortid' => new external_value(PARAM_INT, 'Cohort ID')
            )
        );
    }

   /**
     * Set course status
     */
    public static function cohort_gapp_users($cohortid) {
        global $DB, $USER, $SESSION, $CFG;

        $params = self::validate_parameters(self::cohort_gapp_users_parameters(),
            array('cohortid' => $cohortid));

        $result = array();
        $auser = $DB->get_record('monorail_cohort_admin_users', array('userid'=>$USER->id, 'cohortid' => $cohortid));
        if(!$auser) {
            //Not an admin user of the cohort
            return $result;
        } 
        $userrec = $DB->get_record('user', array('id' => $USER->id),'department'); 
        //TODO: Use once we move to oauth 2.0 
        // XXX: gfunc_get_gapp_useremails DOES NOT EXIST!
        //$credsrec = $DB->get_record('monorail_gapp_creds', array('userid' => $USER->id, 'domain' => $userrec->department), '*', MUST_EXIST);
        //require_once($CFG->dirroot . '/auth/googleapps/lib/gfunc.php');
        //$response = gfunc_get_gapp_useremails($credsrec->domain,$credsrec->token,$credsrec->secret); 
        //$result['emailids'] = $response;
        $response = reset(monorail_data('TEXT', $USER->id, $userrec->department.'_domainusers'));
        $result['emailids'] = (!empty($response)) ? $response->value : '';
        return $result;
    }

    /* @return $email addresses
     *
     */
    public static function cohort_gapp_users_returns() {
        return new external_single_structure(
            array(
                'emailids' => new external_value(PARAM_TEXT, 'Emailids of users', VALUE_OPTIONAL)
            )
        );
    }    

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function get_cal_url_parameters() {
        return new external_function_parameters(array());
    }

   /**
     * Get user calendar url 
     */
    public static function get_cal_url() {
        global $DB, $USER, $SESSION, $CFG;

        $result = array();
        $authtoken = sha1($USER->id . $USER->password . $CFG->calendar_exportsalt);
        $link = new moodle_url('/calendar/export_execute.php', array('userid'=>$USER->id, 'authtoken'=>$authtoken, 'preset_what'=>'all', 'preset_time'=>'recentupcoming'));
        $result['calurl'] = $link->out();
        return $result;
    }

    /* @return $email addresses
     *
     */
    public static function get_cal_url_returns() {
        return new external_single_structure(
            array(
                'calurl' => new external_value(PARAM_URL, 'Url of the calendar')
            )
        );
    }    

   /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     */
    public static function get_enrolled_users_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'course id'),
            )
        );
    }

    /**
     * Get course participants details
     *
     * @param int $courseid  course id
     * @return array An array of users
     */
    public static function get_enrolled_users($courseid) {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . "/user/lib.php");

        $params = self::validate_parameters(
            self::get_enrolled_users_parameters(),
            array(
                'courseid'=>$courseid,
            )
        );
        $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
        $context = get_context_instance(CONTEXT_COURSE, $courseid);
        try {
            self::validate_context($context);
        } catch (Exception $e) {
            $exceptionparam = new stdClass();
            $exceptionparam->message = $e->getMessage();
            $exceptionparam->courseid = $params['courseid'];
            throw new moodle_exception('errorcoursecontextnotvalid' , 'webservice', '', $exceptionparam);
        }
        require_capability('moodle/course:viewparticipants', $context);

        $canEditCourse = (has_capability('moodle/course:manageactivities', $context) || has_capability('moodle/course:update', $context)) ? 1 : 0;

        // TODO: don't give this list to students if teacher disabled participants page
        // (but this would break so many things... using UI-side "permissions" for now)

        list($enrolledsql, $enrolledparams) = get_enrolled_sql($context);
        list($ctxselect, $ctxjoin) = context_instance_preload_sql('u.id', CONTEXT_USER, 'ctx');
        $sqlparams['courseid'] = $courseid;
        $sql = "SELECT u.* $ctxselect
                  FROM {user} u $ctxjoin
                 WHERE u.id IN ($enrolledsql)
                 ORDER BY u.id ASC";
        $enrolledusers = $DB->get_recordset_sql($sql, $enrolledparams);
        $users = array();
        $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);
        foreach ($enrolledusers as $user) {
            context_instance_preload($user);
            if ($userdetails = user_get_user_details($user, $course, array('id', 'fullname', 'roles', 'profileimageurlsmall', 'city', 'institution')))
            {
                $userdetails['profilepichash'] = monorail_get_userpic_hash($userdetails['id']);
                $userInfo = $DB->get_record_sql("SELECT id, email, maildisplay FROM {user} WHERE id=?", array($userdetails["id"]));

                if (!$canEditCourse && !$userInfo->maildisplay && $userdetails["id"] != $USER->id)
                {
                   $userdetails['email'] = "*******";
                }
                else
                {
                    $userdetails['email'] = $userInfo->email;
                }

                if($fieldid) {
                 $title = $DB->get_field('user_info_data', 'data', array('userid'=>$userdetails['id'], 'fieldid' => $fieldid), IGNORE_MULTIPLE);
                 if($title) {
                     $userdetails['title'] = $title;
                 }
             }

                $auser = $DB->get_record_sql("SELECT id, usertype FROM {monorail_cohort_admin_users} WHERE userid=?", array($userdetails["id"]));

                if ($auser) {
                    $userdetails["cohortadminrole"] = $auser->usertype;
                }

                wscache_add_cache_dependency("user_info_" . $userdetails["id"]);
                $users[] = $userdetails;
            }
        }
        $enrolledusers->close();

        wscache_add_cache_dependency("course_info_" . $courseid);

        return $users;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     */
    public static function get_enrolled_users_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'    => new external_value(PARAM_NUMBER, 'ID of the user'),
                    'fullname'    => new external_value(PARAM_NOTAGS, 'The fullname of the user'),
                    'profilepichash'    => new external_value(PARAM_TEXT, 'Profile pic hash'),
                    'email' => new external_value(PARAM_RAW, 'User email'),
                    'title' => new external_value(PARAM_TEXT, 'User title',VALUE_OPTIONAL),
                    'city' => new external_value(PARAM_TEXT, 'User location',VALUE_OPTIONAL),
                    'institution' => new external_value(PARAM_TEXT, 'User organization',VALUE_OPTIONAL),
                    'profileimageurlsmall' => new external_value(PARAM_RAW, 'Profile picture'),
                    'cohortadminrole' => new external_value(PARAM_INT, 'Is this user an admin in cohort?', VALUE_DEFAULT, 0),
                    'roles' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'roleid'       => new external_value(PARAM_INT, 'role id'),
                                'name'         => new external_value(PARAM_TEXT, 'role name'),
                                'shortname'    => new external_value(PARAM_ALPHANUMEXT, 'role shortname'),
                                'sortorder'    => new external_value(PARAM_INT, 'role sortorder')
                            )
                    ), 'user roles', VALUE_OPTIONAL),
                )
            )
        );
    }


 public static function get_ltitools_parameters()
 {
  return new external_function_parameters(
    array(
      'ltitoolids' => new external_multiple_structure(
        new external_value(PARAM_INT, 'LTI tools id'),
        'LTI tools id',
        VALUE_DEFAULT, array())
    )
  );
 }

 /**
  * LTI tools info
  * @param array of ltitool  $ids
  * @return an array of warnings if any
  * @since  Moodle 2.4
  */
 public static function get_ltitools($ltitoolids) {
  global $CFG, $USER, $DB;


  $params = self::validate_parameters(
    self::get_ltitools_parameters(),
    array(
      'ltitoolids'=>$ltitoolids,
    )
  );
  require_once($CFG->dirroot . '/mod/lti/locallib.php');
  $warnings = array();
  $ltitoolsinfo = array();
  $ltitools = $DB->get_records('lti_types', array('state' => 1, 'course' => 1, 'coursevisible' =>1, 'createdby' => 2));
  foreach ($ltitools as $ltitool) {
   $ltiinf = array();
   $ltiinf['id'] = $ltitool->id;
   $ltiinf['name'] = $ltitool->name;
   $ltiinf['url'] = $ltitool->baseurl;
   $orginfo = $DB->get_record('lti_types_config', array('typeid' => $ltitool->id, 'name' => 'organizationurl'));
   $ltiinf['iconurl'] = $orginfo->value;
   $ltiinf['custom'] = 0;
   $ltitoolsinfo[] = $ltiinf;
  }

  $ltitools = $DB->get_records('lti_types', array('state' => 1, 'course' => 1, 'coursevisible' =>1, 'createdby' => $USER->id));
  foreach ($ltitools as $ltitool) {
   $ltiinf = array();
   $ltiinf['id'] = $ltitool->id;
   $ltiinf['name'] = $ltitool->name;
   $ltiinf['url'] = $ltitool->baseurl;
   $orginfo = $DB->get_record('lti_types_config', array('typeid' => $ltitool->id, 'name' => 'organizationurl'));
   $ltiinf['iconurl'] = $orginfo->value;
   $ltiinf['custom'] = 1;
   $config = array();
   $records = $DB->get_records('lti_types_config', array('typeid' => $ltitool->id));
   $keys = array('organizationurl','organizationid','resourcekey','password','sendname' , 'sendemailaddr', 'acceptgrades', 'forcessl', 'launchcontainer', 'coursevisible', 'customparameters');
   foreach ($records as $record) {
       if(in_array($record->name, $keys)) {
           $config[$record->name] = $record->value; 
       }
   }    
   $ltiinf['config'] = $config;
   $ltitoolsinfo[] = $ltiinf;
  }
  $result = array();
  $result['ltitools'] = $ltitoolsinfo;
  $result['warnings'] = $warnings;
  return $result;
 }

 /**
  * Describes the return value for get_ltitools
  * @return external_single_structure
  */
 public static function get_ltitools_returns() {
  return new external_single_structure(
    array(
      'ltitools' => new external_multiple_structure (
        new external_single_structure(
          array(
            'id' => new external_value(PARAM_INT, 'ID of the tool'),
            'name' => new external_value(PARAM_TEXT, 'Name of the tool'),
            'custom' => new external_value(PARAM_INT, 'Custom user tool or generic'),
            'url' => new external_value(PARAM_URL, 'Launch url for the tool'),
            'iconurl' => new external_value(PARAM_URL, 'Icon url for the tool'),
            'config'        => new external_single_structure(
                            array(
                                  'organizationurl' => new external_value(PARAM_URL, "Icon url of the tool", VALUE_OPTIONAL),
                                  'organizationid' => new external_value(PARAM_TEXT, "Org Id", VALUE_OPTIONAL),
                                  'resourcekey' => new external_value(PARAM_TEXT, "Consumer key", VALUE_OPTIONAL),
                                  'password' => new external_value(PARAM_TEXT, "Consumer Password", VALUE_OPTIONAL),
                                  'sendname' => new external_value(PARAM_INT, "Share username", VALUE_OPTIONAL),
                                  'sendemailaddr' => new external_value(PARAM_INT, "Share email", VALUE_OPTIONAL),
                                  'acceptgrades' => new external_value(PARAM_INT, "Accept grades ", VALUE_OPTIONAL),
                                  'forcessl' => new external_value(PARAM_INT, "Force ssl", VALUE_OPTIONAL),
                                  'launchcontainer' => new external_value(PARAM_INT, "Launch(embed/new window)", VALUE_OPTIONAL),
                                  'coursevisible' => new external_value(PARAM_INT, "Visible in course", VALUE_OPTIONAL),
                                  'customparameters' => new external_value(PARAM_TEXT, "Custom parameters", VALUE_OPTIONAL)
                            ), 'config parameters for tool , provided if user configured tool', VALUE_DEFAULT, array())

          )),'LTI tool info'),
      'warnings'  => new external_warnings()
    )
  );
 }

 public static function add_ltitool_parameters() {
  return new external_function_parameters(
    array('ltitool' => new external_multiple_structure (
      new external_single_structure(
        array(
          'course' => new external_value(PARAM_INT, 'course id'),
          'toolname'  => new external_value(PARAM_TEXT, 'name'),
          'toolurl'  => new external_value(PARAM_URL, 'LTI tool url', VALUE_OPTIONAL),
          'securetoolurl' => new external_value(PARAM_URL, 'LTI tool secure url',VALUE_OPTIONAL),
          'instructorchoicesendname' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 1),
          'instructorchoicesendemailaddr' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 0),
          'instructorchoiceallowroster' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT,0),
          'instructorchoiceallowsetting' => new external_value(PARAM_INT, 'LTI tool ',VALUE_OPTIONAL),
          'instructorcustomparameters' => new external_value(PARAM_TEXT, 'LTI tool ',VALUE_DEFAULT,''),
          'instructorchoiceacceptgrades' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 0),
          'grade' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 100),
          'launchcontainer' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT,0),
          'resourcekey' => new external_value(PARAM_TEXT, 'LTI tool ',VALUE_DEFAULT,''),
          'password' => new external_value(PARAM_TEXT, 'LTI tool ',VALUE_DEFAULT,''),
          'forcessl' => new external_value(PARAM_INT, 'LTI tool force ssl',VALUE_DEFAULT,0),
          'debuglaunch' => new external_value(PARAM_INT, 'LTI tool ', VALUE_DEFAULT,0),
          'showtitlelaunch' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 0),
          'showdescriptionlaunch' => new external_value(PARAM_INT, 'LTI tool ', VALUE_DEFAULT, 0),
          'icon' => new external_value(PARAM_URL, 'LTI tool ',VALUE_OPTIONAL),
          'secureicon' => new external_value(PARAM_URL, 'LTI tool ', VALUE_OPTIONAL)
        )), 'list of LTI tool information')
    )
  );
 }

 /**
  * LTI tools info
  * @param array of ltitool  $ids
  * @return an array of warnings if any
  * @since  Moodle 2.4
  */
 public static function add_ltitool($ltitool) {
  global $CFG, $USER, $DB;

  $params = self::validate_parameters(
    self::add_ltitool_parameters(),
    array(
      'ltitool'=>$ltitool,
    )
  );
  require_once($CFG->dirroot . '/mod/lti/lib.php');
  require_once($CFG->dirroot . '/mod/lti/locallib.php');
  require_once($CFG->dirroot . '/course/lib.php');
  $warnings = array();
  $ltitoolinfo = array();
  foreach ($ltitool as $tool) {
   $context = context_course::instance($tool['course']);
   try {
    self::validate_context($context);
    require_capability('moodle/course:manageactivities', $context);
   } catch (Exception $e) {
    $warning = array();
    $warning['item'] = 'course';
    $warning['itemid'] = $tool['course'];
    $warning['warningcode'] = '1';
    $warning['message'] = 'No access rights in course context';
    $warnings[] = $warning;
    continue;
   }

   // We need to add the new tool for this user
   $data = new stdClass();
   $data->lti_organizationurl         = $tool['icon'];
   $data->lti_organizationid         = $tool['organizationid'];
   $data->lti_resourcekey         = $tool['resourcekey'];
   $data->lti_password         = $tool['password'];
   $data->lti_sendname         = $tool['instructorchoicesendname'];
   $data->lti_sendemailaddr         = $tool['instructorchoicesendemailaddr'];
   $data->lti_acceptgrades         = $tool['instructorchoiceacceptgrades'];
   $data->lti_forcessl         = $tool['forcessl'];
   $data->lti_launchcontainer         = 4;
   $data->lti_coursevisible         = 1;
   $data->lti_customparameters         = $tool['instructorcustomparameters'];
   $data->lti_typename         = $tool['toolname'];
   $data->lti_toolurl         = $tool['toolurl'];
   $newtool = new stdClass();
   $newtool->state = LTI_TOOL_STATE_CONFIGURED;

   $ltiinf = array();
   $ltiinf['id'] = lti_add_type($newtool,$data);
   $ltiinf['name'] = $tool['toolname'];
   $ltiinf['launchurl'] = $tool['toolurl'];
   $ltiinf['iconurl'] = $lticonfig['organizationurl'];
   $ltitoolinfo[] = $ltiinf;
  }
  $result = array();
  $result['ltitool'] = $ltitoolinfo;
  $result['warnings'] = $warnings;
  return $result;
 }


 /**
  * Describes the return value for get_ltitools
  * @return external_single_structure
  */
 public static function add_ltitool_returns() {
  return new external_single_structure(
    array(
      'ltitool' => new external_multiple_structure (
        new external_single_structure(
          array(
            'id' => new external_value(PARAM_INT, 'ID of the tool'),
            'name' => new external_value(PARAM_TEXT, 'Name of the tool'),
            'launchurl' => new external_value(PARAM_URL, 'Launch url for the tool'),
            'iconurl' => new external_value(PARAM_URL, 'Icon url for the tool')
          )),'LTI tool info'),
      'warnings'  => new external_warnings()
    )
  );
 }

 /**
  * Returns description of method parameters
  *
  * @return external_function_parameters
  */
 public static function remove_ltitool_parameters() {
  return new external_function_parameters(
    array(
      'toolid' => new external_value(PARAM_INT, 'Tool id to delete')
    )
  );
 }

 public static function  del_course_module($moduleid) {
  global $CFG, $DB;
  require_once($CFG->dirroot . '/course/lib.php');
  // delete all modules if they exist
  $rval = false;
  if ($cm = $DB->get_record("course_modules", array("id" => $moduleid))) {
   $context = context_course::instance($cm->course);
   try {
    self::validate_context($context);
    require_capability('moodle/course:manageactivities', $context);
   } catch (Exception $e) {
    $warning = array();
    $warning['item'] = 'course';
    $warning['itemid'] = $tool['course'];
    $warning['warningcode'] = '1';
    $warning['message'] = 'No access rights in course context';
    $warnings[] = $warning;
    return $rval;
   }
   if ($modulename = $DB->get_field('modules', 'name', array('id' => $cm->module))) {
    $modlib = "{$CFG->dirroot}/mod/{$modulename}/lib.php";
    if (file_exists($modlib)) {
     include_once($modlib);
     $deleteinstancefunction = $modulename."_delete_instance";
     $deleteinstancefunction($cm->instance);
    }
   }
   delete_mod_from_section($moduleid, $cm->section);
   $rval = delete_course_module($moduleid);
   rebuild_course_cache($cm->course);
  }
   return $rval;
  }

 /**
  * Remove LTI tool from course
  *
  * @param int $courseid  course id
  * @return array An array of users
  */
 public static function remove_ltitool($toolid) {
  global $CFG, $DB, $USER;

  $params = self::validate_parameters(
    self::remove_ltitool_parameters(),
    array(
      'toolid'=>$toolid
    )
  );

  $warnings = array();
  if($toolid != 0) {
      //Check if user has rightrs to delete tool
      $ltitool = $DB->get_record('lti_types', array('id' => $toolid,'createdby' => $USER->id)); 
      if($ltitool) {
          require_once($CFG->dirroot . '/mod/lti/locallib.php');
          require_once($CFG->dirroot . '/mod/lti/lib.php');
          //User has acceess to delete tool
          lti_delete_type($toolid);
          $obsltis = $DB->get_records('lti', array('typeid' => $toolid));
          foreach($obsltis as $obslti) {
              // FXIME: instance and course is not enough to get unique cm - add module id
              $cm = $DB->get_record("course_modules", array("instance" => $obslti->id, "course" => $obslti->course));
              if($cm) {
                  $result = self::del_course_module($cm->id);
                  if(!$result) {
                      $warning = array();
                      $warning['item'] = 'lticoursemodule';
                      $warning['itemid'] = $moduleid;
                      $warning['warningcode'] = '1';
                      $warning['message'] = 'Module cannot be deleted';
                      $warnings[] = $warning;
                  }else {
                      lti_delete_instance($obslti->id);
                  } 
              }               
          }
      }
  }
  $result = array();
  $result['warnings'] = $warnings;
  return $result;
 }

 /**
  * Describes the return value for remove_ltitool
  * @return external_single_structure
  * @since  Moodle 2.4
  */
 public static function remove_ltitool_returns() {
  return new external_single_structure(
    array(
      'warnings'  => new external_warnings()
    )
  );
} 

public static function update_ltitool_parameters() {
  return new external_function_parameters(
    array('ltitool' => new external_multiple_structure (
      new external_single_structure(
        array(
          'id'  => new external_value(PARAM_INT, 'Id'),
          'toolname'  => new external_value(PARAM_TEXT, 'name'),
          'toolurl'  => new external_value(PARAM_URL, 'LTI tool url', VALUE_OPTIONAL),
          'securetoolurl' => new external_value(PARAM_URL, 'LTI tool secure url',VALUE_OPTIONAL),
          'instructorchoicesendname' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 1),
          'instructorchoicesendemailaddr' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 0),
          'instructorchoiceallowroster' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT,0),
          'instructorchoiceallowsetting' => new external_value(PARAM_INT, 'LTI tool ',VALUE_OPTIONAL),
          'instructorcustomparameters' => new external_value(PARAM_TEXT, 'LTI tool ',VALUE_DEFAULT,''),
          'instructorchoiceacceptgrades' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 0),
          'grade' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 100),
          'launchcontainer' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT,0),
          'resourcekey' => new external_value(PARAM_TEXT, 'LTI tool ',VALUE_DEFAULT,''),
          'password' => new external_value(PARAM_TEXT, 'LTI tool ',VALUE_DEFAULT,''),
          'forcessl' => new external_value(PARAM_INT, 'LTI tool force ssl',VALUE_DEFAULT,1),
          'debuglaunch' => new external_value(PARAM_INT, 'LTI tool ', VALUE_DEFAULT,0),
          'showtitlelaunch' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 0),
          'showdescriptionlaunch' => new external_value(PARAM_INT, 'LTI tool ', VALUE_DEFAULT, 0),
          'icon' => new external_value(PARAM_URL, 'LTI tool ',VALUE_OPTIONAL),
          'secureicon' => new external_value(PARAM_URL, 'LTI tool ', VALUE_OPTIONAL)
        )), 'list of LTI tool information')
    )
  );
 }

 /**
  * LTI tools info
  * @param array of ltitool  $ids
  * @return an array of warnings if any
  * @since  Moodle 2.4
  */
 public static function update_ltitool($ltitool) {
  global $CFG, $USER, $DB;

  $params = self::validate_parameters(
    self::update_ltitool_parameters(),
    array(
      'ltitool'=>$ltitool,
    )
  );
  require_once($CFG->dirroot . '/mod/lti/lib.php');
  require_once($CFG->dirroot . '/mod/lti/locallib.php');
  $warnings = array();
  foreach ($ltitool as $tool) {
       //Find if user owns the tool
       $ltirec = $DB->get_record('lti_types', array('createdby' => $USER->id, 'id' => $tool['id']));
       if(!$ltirec) {
           //User does not own the tool
           continue;
       }
       $data = new stdClass();
       $data->lti_organizationurl         = $tool['icon'];
       $data->lti_organizationid         = $tool['organizationid'];
       $data->lti_resourcekey         = $tool['resourcekey'];
       $data->lti_password         = $tool['password'];
       $data->lti_sendname         = $tool['instructorchoicesendname'];
       $data->lti_sendemailaddr         = $tool['instructorchoicesendemailaddr'];
       $data->lti_acceptgrades         = $tool['instructorchoiceacceptgrades'];
       $data->lti_forcessl         = $tool['forcessl'];
       $data->lti_launchcontainer         = 4;
       $data->lti_coursevisible         = 1;
       $data->lti_customparameters         = $tool['instructorcustomparameters'];
       $data->lti_typename         = $tool['toolname'];
       $data->lti_toolurl         = $tool['toolurl'];
       $newtool = new stdClass();
       $newtool->state = LTI_TOOL_STATE_CONFIGURED;
       $newtool->id = $tool['id'];
       lti_update_type($newtool,$data);

  }
  $result = array();
  $result['warnings'] = $warnings;
  return $result;
 }

/**
  * Describes the return value for update_ltitool
  * @return external_single_structure
  * @since  Moodle 2.4
  */
 public static function update_ltitool_returns() {
  return new external_single_structure(
    array(
      'warnings'  => new external_warnings()
    )
  );
 } 


public static function add_ltimod_parameters() {
 return new external_function_parameters(
   array('ltimod' => new external_single_structure(
     array(
       'sectionid' => new external_value(PARAM_INT, 'section id'),
       'course' => new external_value(PARAM_INT, 'course id'),
       'name'  => new external_value(PARAM_TEXT, 'name'),
       'intro' => new external_value(PARAM_TEXT, 'Description', VALUE_DEFAULT, 'External tool', VALUE_DEFAULT, 'Ext Tool'),
       'introformat' => new external_value(PARAM_INT, 'description format ', VALUE_DEFAULT, 1),
       'typeid' => new external_value(PARAM_INT, 'Type id'),
       'grade' => new external_value(PARAM_INT, 'LTI tool ',VALUE_DEFAULT, 100),
     ), 'list of LTI module information')
   )
 );
}

/**
 * LTI add module
 * @param array of ltitool  $$ltimod info
 * @return an array of warnings if any
 * @since  Moodle 2.4
 */
public static function add_ltimod($ltimod) {
 global $CFG, $USER, $DB;

 $params = self::validate_parameters(
   self::add_ltimod_parameters(),
   array(
     'ltimod'=>$ltimod,
   )
 );
 require_once($CFG->dirroot . '/mod/lti/lib.php');
 require_once($CFG->dirroot . '/mod/lti/locallib.php');
 require_once($CFG->dirroot . '/course/lib.php');
 $warnings = array();
 $ltitoolinfo = array();
 $context = context_course::instance($ltimod['course']);
 try {
  self::validate_context($context);
  require_capability('moodle/course:manageactivities', $context);
 } catch (Exception $e) {
  $warning = array();
  $warning['item'] = 'course';
  $warning['itemid'] = $ltimod['course'];
  $warning['warningcode'] = '1';
  $warning['message'] = 'No access rights in course context';
  $warnings[] = $warning;
  $result = array();
  $result['warnings'] = $warnings;
  return $result;
 }

 //Here we have to create the LTI module , then associate course module
 //with the LTI tool and then return the config url to the tool back for user to config
 $ltirec = $DB->get_record("lti_types", array("id"=>$ltimod['typeid']));
 if(!$ltirec) {
  $result = array();
  $result['warnings'] = $warnings;
  return $result;
 }
 $lticonfig = lti_get_type_config($ltimod['typeid']);
 $lti = array(
   'course' => $ltimod['course'],
   'name'  => $ltimod['name'],
   'intro' => $ltimod['intro'],
   'introformat' => $ltimod['introformat'],
   'typeid'  => $ltimod['typeid'],
   'toolurl'  => $ltirec->baseurl,
   'securetoolurl' => $ltirec->baseurl,
   'instructorchoicesendname' => $lticonfig['sendname'],
   'instructorchoicesendemailaddr' => $lticonfig['sendemailaddr'],
   'instructorchoiceallowroster' => $lticonfig['acceptgrades'],
   'instructorchoiceallowsetting' => $lticonfig['accept'],
   'instructorcustomparameters' => $lticonfig['customparameters'],
   'instructorchoiceacceptgrades' => $lticonfig['acceptgrades'],
   'grade' => $ltimod['grade'],
   'launchcontainer' => $lticonfig['launchcontainer'],
   'resourcekey' => $lticonfig['resourcekey'],
   'password' => $lticonfig['password'],
   'debuglaunch' => 0,
   'showtitlelaunch' => 0,
   'showdescriptionlaunch' => 0,
   'icon' => $lticonfig['organizationurl'],
   'secureicon' => $lticonfig['organizationurl']
 );


 $ltiid = lti_add_instance((object)$lti, null);

 $course = $DB->get_record('course', array('id' => $ltimod['course']), '*', MUST_EXIST);
 $lti = $DB->get_record('lti', array('id' => $ltiid), '*', MUST_EXIST);
 $ltitype = $DB->get_record('lti_types', array('id' => $lti->typeid));
 if($ltitype && $ltitype->createdby != 2){ //If user created
  $exurl = lti_view_embed($lti, $course);
  if(empty($exurl)) {
   $warning = array();
   $warning['item'] = 'course';
   $warning['itemid'] = $ltimod['course'];
   $warning['warningcode'] = '1';
   $warning['message'] = 'Edu app ext url is wrong';
   $warnings[] = $warning;
   $result = array();
   $result['warnings'] = $warnings;
  }
 }

 $moduleid = $DB->get_field('modules', 'id', array('name'=> 'lti'));
 $newcm = new stdClass();
 $newcm->course   = $ltimod['course'];
 $newcm->module   = $moduleid;
 $newcm->instance   = $ltiid;
 $newcm->visible  = 1;
 $newcm->section  = $ltimod['sectionid'];
 $newcm->added    = time();
 $newcm->coursemodule  = 0;
 $newcmid = $DB->insert_record('course_modules', $newcm);

 //section info update
 $newcm->coursemodule  = $newcmid;

 $section = $DB->get_record("course_sections", array("id"=>$ltimod['sectionid']));
 $newcm->section = $section->section;
 add_mod_to_section($newcm);
 rebuild_course_cache($newcm->course);

 $ltiinf = array();
 $ltiinf['id'] = $ltiid;
 $ltiinf['cmid'] = $newcmid;
 $ltiinf['name'] = $ltimod['name'];
 $link = new moodle_url('/mod/lti/launch.php', array('id'=>$newcmid));
 $ltiinf['launchurl'] = $link->out();
 $ltiinf['iconurl'] = $lticonfig['organizationurl'];
 $ltitoolinfo[] = $ltiinf;
 $result['ltitool'] = $ltitoolinfo;
 $result['warnings'] = $warnings;
 return $result;
}

/**
 * Describes the return value for get_ltitools
 * @return external_single_structure
 */
public static function add_ltimod_returns() {
 return new external_single_structure(
   array(
     'ltitool' => new external_multiple_structure (
       new external_single_structure(
         array(
           'id' => new external_value(PARAM_INT, 'ID of the ltimod'),
           'cmid' => new external_value(PARAM_INT, 'Course module ID of the tool'),
           'name' => new external_value(PARAM_TEXT, 'Name of the tool'),
           'launchurl' => new external_value(PARAM_URL, 'Launch url for the tool'),
           'iconurl' => new external_value(PARAM_URL, 'Icon url for the tool')
         )),'LTI tool info', VALUE_OPTIONAL),
     'warnings'  => new external_warnings()
   )
 );
}

/**
  * Returns description of method parameters
  *
  * @return external_function_parameters
  */
 public static function remove_ltimod_parameters() {
  return new external_function_parameters(
    array(
      'moduleid' => new external_value(PARAM_INT, 'course module id', VALUE_DEFAULT, 0),
    )
  );
 }

 
 /**
  * Remove LTI mod from course
  *
  * @param int $moduleid  module id
  * @return array $warnings
  */
 public static function remove_ltimod($moduleid) {
  global $CFG, $DB, $USER;

  $params = self::validate_parameters(
    self::remove_ltimod_parameters(),
    array(
      'moduleid'=>$moduleid,
    )
  );

  $warnings = array();
  
  $rval = self::del_course_module($moduleid);
  if(!$rval) {
      $warning = array();
      $warning['item'] = 'lticoursemodule';
      $warning['itemid'] = $moduleid;
      $warning['warningcode'] = '1';
      $warning['message'] = 'Module cannot be deleted';
      $warnings[] = $warning;
  }
  $result = array();
  $result['warnings'] = $warnings;
  return $result;
 }

 /**
  * Describes the return value for remove_ltimod
  * @return external_single_structure
  * @since  Moodle 2.4
  */
 public static function remove_ltimod_returns() {
  return new external_single_structure(
    array(
      'warnings'  => new external_warnings()
    )
  );
 } 

 public static function update_ltimod_parameters() {
  return new external_function_parameters(
    array('ltimods' => new external_multiple_structure (
      new external_single_structure(
        array(
          'moduleid'  => new external_value(PARAM_INT, 'LTI module Id'),
          'name'  => new external_value(PARAM_TEXT, 'name'),
        )), 'list of LTI mod information')
    )
  );
 }

 /**
  * LTI tools info
  * @param array of ltitool  $ids
  * @return an array of warnings if any
  * @since  Moodle 2.4
  */
 public static function update_ltimod($ltimods) {
  global $CFG, $USER, $DB;

  $params = self::validate_parameters(
    self::update_ltimod_parameters(),
    array(
      'ltimods'=>$ltimods,
    )
  );
  require_once($CFG->dirroot . '/mod/lti/lib.php');
  $warnings = array();
  foreach ($ltimods as $ltimod) {
       $cm = $DB->get_record('course_modules', array('id' => $ltimod['moduleid']));
       if(!$cm) {
          continue;
       }
       $context = context_course::instance($cm->course);
       try {
         self::validate_context($context);
         require_capability('moodle/course:manageactivities', $context);
       } catch (Exception $e) {
           $warning = array();
           $warning['item'] = 'course';
           $warning['itemid'] = $ltimod['course'];
           $warning['warningcode'] = '1';
           $warning['message'] = 'No access rights in course context';
           $warnings[] = $warning;
           continue;
       }
       $newtool = new stdClass();
       $newtool->instance = $cm->instance;
       $newtool->name = $ltimod['name'];
       lti_update_instance($newtool,null);
  }
  $result = array();
  $result['warnings'] = $warnings;
  return $result;
 }

/**
  * Describes the return value for update_ltimod
  * @return external_single_structure
  * @since  Moodle 2.4
  */
 public static function update_ltimod_returns() {
  return new external_single_structure(
    array(
      'warnings'  => new external_warnings()
    )
  );
 } 

 /**
  * Send message parameters
  */
 public static function send_message_parameters() {
  return new external_function_parameters(
    array(
      'courseid' => new external_value(PARAM_INT, 'Course ID'),
      'summary' => new external_value(PARAM_RAW, 'Summary text', VALUE_DEFAULT,''),
      'userids' => new external_multiple_structure(
        new external_value(PARAM_INT, 'user ID'),'list of user ids',VALUE_DEFAULT,array())
    )
  );
 }
 
 /**
  * Send an invitation to an email
  */
 public static function send_message($courseid, $summary, $userids) {
  global $DB, $USER, $CFG;
 
  $params = self::validate_parameters(self::send_message_parameters(),
    array('courseid' => $courseid, 'summary' => $summary, 'userids' => $userids));

  $sendMessage = false;
  $roleteacher = false;

  $eteacherid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
  $teacherid = $DB->get_field('role', 'id', array('shortname'=>'teacher'), MUST_EXIST);
  $context = context_course::instance($courseid);
  if(user_has_role_assignment($USER->id, $teacherid, $context->id) || user_has_role_assignment($USER->id, $eteacherid, $context->id)) {
     $roleteacher = true; 
  }
  //If course belongs to cohort and user is admin
  if($DB->count_records('monorail_cohort_courseadmins', array('courseid' => $courseid, 'adminid' => $USER->id)) > 0) {
      $sendMessage = true;
  } else {
      $cohortid = $DB->get_field('monorail_cohort_courseadmins', 'cohortid', array('courseid'=>$courseid), IGNORE_MULTIPLE);
      if($cohortid) {
          if(($DB->count_records("cohort_members", array("cohortid" => $cohortid, "userid" => $USER->id)) > 0) && $roleteacher) {
              $sendMessage = true;
          }
      }
  } 

  if(!$sendMessage && $roleteacher) {
   //maybe a mooc course
   $cperm = monorail_get_course_perms($courseid);
   $sendMessage = (($cperm["course_privacy"] == 1) && ($cperm["course_access"] == 3));
  }

  $warnings = array();
  $result = array();
  $coursename = $DB->get_field('course', 'fullname', array('id'=>$courseid));
  if(!$coursename || !$sendMessage) {
   //Unable to find course
   $warning = array();
   $warning['item'] = 'send_message';
   $warning['itemid'] = $courseid;
   $warning['warningcode'] = '1';
   $warning['message'] = 'Cannot find course to send message';
   $warnings[] = $warning;
   $result['warnings'] = $warnings;
   return $result;
  }
 
  if(empty($userids)) {
   //Send email to all course participants
   $participants = self::get_enrolled_users($courseid);
   foreach ($participants as $participant) {
    if($USER->id ==  $participant['id']) {
        continue;
    }
    $userids[] = $participant['id'];
   }
  }
 
  if(!empty($userids)) {
    $subject = monorail_template_compile(get_string('email_course_message_subject','theme_monorail'), array(
      'courseName' => $coursename));

    $summary = fix_utf8($summary);
    $summary = clean_text($summary, FORMAT_HTML); 
   require_once($CFG->libdir . '/enrollib.php');
   foreach ($userids as $userid) {
    $user = $DB->get_record('user', array('id'=>$userid));
    if($user && is_enrolled($context, $userid)) {

     $template_content = monorail_template_compile(monorail_get_template('mail/base'), array(
        'body' => monorail_template_compile(monorail_get_template('mail/coursemessage'), array(
                  'course_message' => nl2br($summary), 'courseName' => $coursename,
                  'teacherName' => fullname($USER,true), 'username' => $user->firstname,
                  )),
        'title' => $subject,
        'footer_additional_link' => monorail_template_compile(monorail_get_template('mail/change_settings_link'), array(
                'change_settings_url' => $CFG->magic_ui_root.'/settings'
        )),
        'block_header' => ''
     ));
     if (!monorail_send_email('course_messages', $subject, $template_content, null, null, $user->email, true, fullname($USER, true))) {
      $warning = array();
      $warning['item'] = 'invite_send';
      $warning['itemid'] = $id;
      $warning['warningcode'] = '1';
      $warning['message'] = 'Failed to send invitation';
      $warnings[] = $warning;
     }
    }
   }
  }
  $result['warnings'] = $warnings;
  return $result;
 }

 /**
  * Returns description of method result value
  *
  * @return null
  * @since Moodle 2.2
  */
 public static function send_message_returns() {
  return new external_single_structure(
    array(
      'warnings'  => new external_warnings()
    )
  );
 }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     */
    public static function get_feed_parameters() {
        return new external_function_parameters(
            array(
                'count' => new external_value(PARAM_INT, 'count of items to return', VALUE_DEFAULT, 10),
                'mode' => new external_value(PARAM_INT, 'mode, all=0, unread only=1, read only=2', VALUE_DEFAULT, 0),
            )
        );
    }
    
    public static function get_read_notification_count_to_fetch($count = 0) {
        global $CFG;
        if($count) {
            return $count;
        }
        if(isset($CFG->UNREAD_NOTI_COUNT)) {
            return $CFG->UNREAD_NOTI_COUNT;
        }
        return 100;
    }
    
    public static function get_unread_notification_count_to_fetch($count = 0) {
        global $CFG;
        if($count) {
            return $count;
        }
        if(isset($CFG->READ_NOTI_COUNT)) {
            return $CFG->READ_NOTI_COUNT;
        }
        return 100;
    }

    /**
     * Get feed items
     */
    public static function get_feed($notifications, $count, $mode) {
        global $CFG, $DB, $USER;
        require_once($CFG->dirroot . "/local/monorailfeed//lib.php");

        $params = self::validate_parameters(
            self::get_feed_parameters(),
            array(
                'count'=>$count,
                'mode'=>$mode,
            )
        );
        $return = array();
        
        // get any up to count
        $usernotifications = monorailfeed_get_user($USER->id, $mode,self::get_read_notification_count_to_fetch($count));
        // get all unread and merge
        $userunreadnotifications = monorailfeed_get_user($USER->id, 1,self::get_unread_notification_count_to_fetch());
        if (count($userunreadnotifications["notifications"])) {
            $notifications = array_merge($usernotifications["notifications"], $userunreadnotifications["notifications"]);
            ksort($notifications);
        } else {
            $notifications = $usernotifications["notifications"];
        }
        $coursecache = array();
        $coursedatacache = array();
        $usercache = array(array('Moodle', ''));
        
        foreach ($notifications as $notif) {
            try {
                if ($notif->type == 'faded_del' || $notif->type == 'task_del') {
                    continue;
                }
                if (! isset($coursecache[$notif->courseid])) {
                    $course = $DB->get_record('course', array('id'=>$notif->courseid));
                    $coursecache[$notif->courseid] = $course;
                    $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$notif->courseid));
                    $coursedatacache[$notif->courseid] = $courseData;
                }
                $course = $coursecache[$notif->courseid];
                $courseData = $coursedatacache[$notif->courseid];
                $record = array(
                    "id" => $notif->id,
                    "userid" => $notif->createdby,
                    "read" => ($notif->timeread ? 1 : 0),
                    "coursename" => $course->fullname,
                    "timestamp" => $notif->timemodified,
                );
                if (! isset($record["userid"])) {
                    $record["userid"] = 0;
                }
                if ($record["userid"] == $USER->id)
                {
                    // Don't show any notifications about actions by current user.
                    continue;
                }
                switch ($notif->type) {
                    case 'calendar':
                        $record["text"] = get_string($notif->component, 'theme_monorail');
                        try {
                            require_once($CFG->dirroot.'/calendar/lib.php');
                            $event = calendar_event::load($notif->instance);
                        } catch (Exception $ex) {
                            $event = monorailfeed_get_revision('event', $notif->instance);
                        }
                        $date  = new DateTime();
                        $date->setTimestamp($event->timestart);
                        $d = $date->format('d');
                        $m = $date->format('m');
                        $y = $date->format('Y');
                        //$record["actionurl"] = "../calendar/view.php?view=day&course={$notif->courseid}&cal_d=$d&cal_m=$m&cal_y=$y";

                        $record["actionurl"] = "/calendar/{$y}{$m}";
                        break;

                    case 'forum':
                        $record["text"] = get_string($notif->component, 'theme_monorail');
                        require_once($CFG->dirroot.'/mod/forum/lib.php');
                        $post = forum_get_post_full($notif->instance);
                        //$record["actionurl"] = '../theme/monorail/forum.php?d='.$post->discussion.'#post'.$notif->instance;
                        $record["actionurl"] = "/courses/" . $courseData->code . "/discussions/" . $post->forum . "/" . $post->discussion;
                        break;

                    case 'section':
                        $record["text"] = get_string($notif->component, 'theme_monorail');
                        if (isset($notif->revtableid)) {
                            try {
                                $revision = $DB->get_record('monorailfeed_rev_sections', array('id'=>$notif->revtableid), '*', MUST_EXIST);
                                $section = $DB->get_record('course_sections', array('id'=>$revision->coursesectionsid), '*', MUST_EXIST);
                                $sectionnum = $section->section;

                                if (!$section->visible)
                                {
                                    // Don't show notifications about
                                    // hidden topics.
                                    continue 2;
                                }
                            } catch (Exception $ex) {
                                $sectionnum = null;
                            }
                        }
                        if ($sectionnum > 0)
                        {
                            $sectionnum -= $DB->get_field_sql("SELECT COUNT(*) FROM {course_sections} WHERE course=? AND NOT visible AND section<?",
                                array($notif->courseid, $sectionnum));

                            $record["actionurl"] = '/courses/' .$courseData->code . "/" . $sectionnum;
                            $record["text"] .= " " . $section->name;
                        }
                        else
                        {
                            $record["actionurl"] = '/courses/' .$courseData->code;
                            $record["text"] .= " " . get_string('overview_topic', 'theme_monorail');
                        }
                        $record["sectionid"] = $sectionnum;
                        break;

                    case 'task_new':
                    case 'task_upd':
                        if (!$DB->get_field('course_modules', 'visible', array('id'=>$notif->instance))) {
                            // Don't show notifications about hidden tasks.
                            continue 2;
                        }

                        $record["text"] = get_string($notif->type, 'theme_monorail');
                        $record["actionurl"] = '/tasks/' .$notif->instance;
                        break;

                    case 'material':
                        require_once($CFG->dirroot.'/theme/monorail/lib.php');
                        $record["text"] = get_string($notif->type, 'theme_monorail');

                        $cm = get_coursemodule_from_id('resource', $notif->instance, $notif->courseid, true, MUST_EXIST);
                        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
                        $fs = get_file_storage();
                        $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0);
                        foreach ($files as $file) {
                            if ($file->get_filename() != '.') {
                                $record["actionurl"] = monorail_get_file_url($file);
                                break;
                            }
                        }
                        break;

                    case 'teacher':
                        switch ($notif->component) {
                            case 'user_enrolled':
                                $record["text"] = get_string($notif->component, 'theme_monorail');
                                $record["actionurl"] = '/courses/' .$courseData->code. '/participants';
                                break;
                            case 'user_submitted':
                                $cm = get_coursemodule_from_id(null, $notif->instance, $notif->courseid, false, MUST_EXIST);
                                $record["text"] = get_string('user_submitted_'.$cm->modname, 'theme_monorail');
                                $record["actionurl"] = '/tasks/' .$notif->instance;
                                break;
                            case "user_completed":
                                $record["text"] = get_string("user_completed_course", 'theme_monorail');
                                $record["actionurl"] = '/courses/' .$courseData->code. '/progress';
                                break;
                        }
                        break;
                        
                    case 'grade':
                    case 'comment':
                        $record["text"] = get_string($notif->type, 'theme_monorail');
                        $record["actionurl"] = '/tasks/' .$notif->instance;
                        break;

                    case 'cert':
                        $record["text"] = get_string("certificate", 'theme_monorail');
                        $record["actionurl"] = '/courses/' . $courseData->code . "/certificate";
                        break;

                    case "bbb_new":
                        $record["text"] = get_string("bbb_new", 'theme_monorail');
                        $record["actionurl"] = '/courses/' . $courseData->code . "/liveevent";
                        break;

                    case "bbb_upd":
                        $record["text"] = get_string("bbb_upd", 'theme_monorail');
                        $record["actionurl"] = '/courses/' . $courseData->code . "/liveevent";
                        break;

                    case "bbb_del":
                        $record["text"] = get_string("bbb_del", 'theme_monorail');
                        $record["actionurl"] = '/courses/' . $courseData->code . "/liveevent";
                        break;
                }

                if (! isset($usercache[$record["userid"]])) {
                    $user = $DB->get_record("user", array("id"=>$record["userid"]));
                    $usercache[$record["userid"]] = array(
                        "fullname" => fullname($user, true),
                        "pichash" => monorail_get_userpic_hash($record["userid"]),
                    );
                }
                $record["fullname"] = $usercache[$record["userid"]]["fullname"];
                $record["profilepichash"] = $usercache[$record["userid"]]["pichash"];
                
                $return[] = $record;
            } catch (Exception $ex) {
                add_to_log(1, 'monorailservices', 'externallib', 'get_feed', 'Error, notification '.$notif->id.', skipping: '.$ex->getMessage());
            }
        }
        return array(
            "notifications" => $return,
            "moreavailable" => ($usernotifications["moreavailable"]) ? 1 : 0,
        );
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     */
    public static function get_feed_returns() {
        return new external_single_structure(
            array(
                "notifications" => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'notification id'),
                            'userid' => new external_value(PARAM_INT, 'user id'),
                            'profilepichash' => new external_value(PARAM_RAW, 'profile image hash',VALUE_OPTIONAL),
                            'fullname' => new external_value(PARAM_RAW, 'user name'),
                            'read' => new external_value(PARAM_INT, 'is notification read, 1=yes, 0=no'),
                            'actionurl' => new external_value(PARAM_RAW, 'url to action', VALUE_OPTIONAL),
                            'coursename' => new external_value(PARAM_RAW, 'course name'),
                            'text' => new external_value(PARAM_RAW, 'text of item'),
                            'timestamp' => new external_value(PARAM_INT, 'timestamp'),
                            'sectionid' => new external_value(PARAM_INT, 'section id', VALUE_OPTIONAL),
                        )
                    )
                ),
                "moreavailable" => new external_value(PARAM_INT, '1=yes, 0=no'),
            )
        );
    }
    
    public static function get_skills_parameters() {
        return new external_function_parameters(array());
    }

    /**
     * Search for skills
     */
    public static function get_skills() {
        global $DB, $CFG;

        $results = array();
        
        $skills = $DB->get_records('monorail_skills', array(), 'name', 'name');
        
        foreach ($skills as $skill) {
            $results[] = $skill->name;
        }
        return $results;
    }
    
    public static function get_skills_returns() {
        return new external_multiple_structure (
            new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL)
        );
    }    

    public static function validate_skill_parameters() {
        return new external_function_parameters(
            array(
                'skillname' => new external_value(PARAM_TEXT, 'skill name'),
            )
        );
    }

    /**
     * Validate skill
     */
    public static function validate_skill($skillname) {
        global $DB, $CFG;

        $params = self::validate_parameters(
            self::validate_skill_parameters(),
            array(
                'skillname'=>$skillname,
            )
        );
        $results = array();
        require_once("$CFG->dirroot/theme/monorail/skills.php");
        if ($DB->record_exists('monorail_skills', array('name'=>$skillname))) {
            // exact match
            $results['result'] = 2;
        } else if (! $DB->record_exists('monorail_skills', array('cleanedname'=>monorail_cleaned_skill_name($skillname)))) {
            $results['result'] = 0;
        } else {
            $results['result'] = 1;
            $skills = $DB->get_fieldset_select('monorail_skills', 'name', "cleanedname = ?", array(monorail_cleaned_skill_name($skillname)));
            if (count($skills)) {
                $results['suggestions'] = $skills;
            }
        }
        
        return $results;
    }
    
    public static function validate_skill_returns() {
        return new external_single_structure(
            array(
                'result' => new external_value(PARAM_INT, 'result, 0 success (available), 1 failed (fuzzy match), 2 exact match'),
                'suggestions' => new external_multiple_structure(
                    new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL),
                    'list of suggestions, if any', VALUE_OPTIONAL
                )
            )
        );
    }
    
    public static function save_skills_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'course id'),
                'skills' => new external_multiple_structure(
                    new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL),
                    'list of skills, if any', VALUE_DEFAULT,array()
                )
            )
        );
    }

    /**
     * Save course skills
     */
    public static function save_skills($courseid, $skills) {
        global $DB, $CFG;

        $params = self::validate_parameters(
            self::save_skills_parameters(),
            array(
                'courseid'=>$courseid,
                'skills'=>$skills,
            )
        );
        // access check
        $context = get_context_instance(CONTEXT_COURSE, $courseid);
        try {
            self::validate_context($context);
            require_capability('moodle/course:update', $context);
        } catch (Exception $e) {
            $exceptionparam = new stdClass();
            $exceptionparam->message = $e->getMessage();
            $exceptionparam->catid = $courseid;
            throw new moodle_exception('errorcontextnotvalid', 'webservice', '', $exceptionparam);
        }
        $result = array();
        require_once("$CFG->dirroot/theme/monorail/skills.php");
        $courseskills = $DB->get_fieldset_sql('select ms.name from {monorail_skills} ms, {monorail_course_skills} mcs where ms.id = mcs.skillid and mcs.courseid = ?', array($courseid));
        $errors = array(
            'adds'=>array(),
            'rems'=>array()
        );
        $result['result'] = 1;
        // add new skills
        foreach ($skills as $skill) {
            if (! in_array($skill, $courseskills)) {
                // add
                if (! $DB->record_exists('monorail_skills', array('name'=>$skill))) {
                    monorail_add_skill($skill);
                }
                if ($skillrec = monorail_get_skill(null, $skill)) {
                    if (! monorail_add_course_skill($skillrec->id, $courseid)) {
                        unset($skills[$skill]);
                        $errors['adds'][] = $skill;
                    } else {
                        $result['result'] = 0;
                    }
                } else {
                    // failed
                    add_to_log($courseid, 'monorailservices', 'externallib', 'save_skills', "Failed to save skill $skill to course ".$courseid);
                    unset($skills[$skill]);
                    $errors['adds'][] = $skill;
                }
            }
        }
        // remove old skills
        foreach (array_diff($courseskills, $skills) as $skill) {
            if ($skillrec = monorail_get_skill(null, $skill)) {
                if (! monorail_remove_course_skill($skillrec->id, $courseid)) {
                    $errors['rems'][] = $skill;
                } else {
                    $result['result'] = 0;
                }
            } else {
                // failed
                add_to_log($courseid, 'monorailservices', 'externallib', 'save_skills', "Failed to remove skill $skill from course ".$courseid);
                $errors['rems'][] = $skill;
            }
        }
        // return real skill list to client
        $skills = monorail_get_course_skills($courseid);
        $result['skills'] = array();
        foreach ($skills as $skill) {
            $skillrec = monorail_get_skill($skill->skillid);
            $result['skills'][] = $skillrec->name;
        }
        $result['errors'] = $errors;
        return $result;
    }
    
    public static function save_skills_returns() {
        return new external_single_structure(
            array(
                'result' => new external_value(PARAM_INT, 'result, 0 success, 1 some error'),
                'errors' => new external_single_structure(
                    array(
                        'adds' => new external_multiple_structure(
                            new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'failed adds', VALUE_OPTIONAL
                        ),
                        'rems' => new external_multiple_structure(
                            new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'failed removes', VALUE_OPTIONAL
                        )
                    )
                ),
                'skills' => new external_multiple_structure(
                    new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'final list of skills', VALUE_OPTIONAL
                ),
            )
        );
    }    



    public static function clone_course_parameters() {
      return new external_function_parameters(
          array(
              'course' => new external_single_structure( array ('fullname'  => new external_value(PARAM_TEXT, 'course full name'),
                  'shortname' => new external_value(PARAM_TEXT, 'course short name',VALUE_OPTIONAL),
                  'coursecode' => new external_value(PARAM_TEXT, 'course code'),
                  'background' => new external_value(PARAM_TEXT, 'course background'),
                  'course_logo' => new external_value(PARAM_TEXT, 'course logo'),
                  'category' => new external_value(PARAM_INT, 'course category'),
                  'startdate' => new external_value(PARAM_INT, 'course start date', VALUE_OPTIONAL),
                  'enddate' => new external_value(PARAM_INT, 'course category', VALUE_OPTIONAL),
                  'cert_enabled' => new external_value(PARAM_INT, 'certificates enabled', VALUE_OPTIONAL),
                  'language' => new external_value(PARAM_TEXT, 'course language', VALUE_OPTIONAL),
                  'course_country' => new external_value(PARAM_TEXT, 'course country', VALUE_OPTIONAL),
                  'keywords' => new external_value(PARAM_TEXT, 'course keywords', VALUE_OPTIONAL)
              ), 'clone course information object',VALUE_REQUIRED)));
    }


    public static  function utility_clone_files($file, $data, $type='resource', $inline=false)
    {
      global $USER,$CFG;
      if (!file_exists($CFG->dataroot.'/temp/files/'.$USER->id.'/')) {
        mkdir($CFG->dataroot.'/temp/files/'.$USER->id.'/');
      }
      $filename = time()."-".$file->get_id()."-". $file->get_filename();
      $file->copy_content_to($CFG->dataroot.'/temp/files/'.$USER->id.'/' . $filename);
      $resources = array();
      $resource = array();
      $resource['name'] = $file->get_filename();
      $resource['filename'] = $filename;
      $contextualdata = self::monorailservices_get_file_contextdata($file->get_pathnamehash(), $file->get_id());
      if($contextualdata) {
        $resource['contextualdata'] = $contextualdata;
        $jscdata = json_decode($contextualdata);
        if(property_exists($jscdata,'visible'))
        {
            if(!$jscdata->visible && !$inline) {
                return;//If not visible and img not inline do not copy - deleted file 
            } 
        }
      }
    
      $fileitem = array();
      $fnames = monorail_data('assignfile', $file->get_id(), 'name');
      if($fnames) {
         foreach ($fnames as $fname) {
            $filedisplayname = $fname->value;
            break;
         }
         $fileitem['filename'] = $filedisplayname;
      } else {
        $fileitem['filename'] = urldecode($file->get_filename()); 
      }
      if($type == 'assign') {
        $resource['assignmentid'] = $data['assignmentid'];
        $resources[] = $resource;
        $retval =  array_shift(self::add_assignfiles($resources));
        $fileitem['fileid'] = $retval[0]['fileid']; 
      } else {
        $resource['course'] = (int)$data['course'];
        $resource['sectionid'] = $data['section'];
        $resources[] = $resource;
        $retval = array_shift(self::create_filerscs($resources));
        $fileitem['moduleid'] = $retval[0]['resourceid']; 
      }
      $fileitems = array();
      $fileitems[] = $fileitem;
      try {
        self::update_filename($fileitems);
      } catch (Exception $ex) {
      }
      return $retval;
    }

    public static  function utility_clone_inline_attachments($data, $type = 'resource')
    {
      global $CFG,$USER,$DB;

      $dom = new DOMDocument();
      $fs = get_file_storage();
      $dom->loadHTML(mb_convert_encoding($data['html'], 'html-entities', 'utf-8'));
      $moduleids = array();
      $fileids = array();
      $vrscremove = array(); 
      foreach ($dom->getElementsByTagName("img") as $img)
      {
        if ($img->hasAttribute("data-attachment"))
        {
          if($img->hasAttribute("data-attachment-type") && (strcmp($img->getAttribute("data-attachment-type"), "eliademyvideo") == 0)) {
              //TODO: Media library - Bug 1859 Temp workaround uploaded videos should not be copied, when course is duplicated  
              $img->removeAttribute("data-attachment");
              $img->removeAttribute("data-entry-id");
              $img->removeAttribute("data-attachment-type");
              $img->setAttribute("src", '');
              $vrscremove[] = $img;
              continue;
          }
          if($type == 'assign') {
              $file = $fs->get_file_by_id($img->getAttribute("data-attachment"));
              if($file) {
                array_push($fileids, $img->getAttribute("data-attachment"));
                $response = self::utility_clone_files($file, $data, $type, true);
                $img->setAttribute("data-attachment", $response[0]['fileid']);
              }
          } else {
            $modcontext = get_context_instance(CONTEXT_MODULE, $img->getAttribute("data-attachment"));
            $files = $fs->get_area_files($modcontext->id, 'mod_'.$type , 'content');
            foreach ($files as $file)
            {
              if ($file->get_filename() != ".")
              {
                $response = self::utility_clone_files($file, $data, $type, true);
                array_push($moduleids, $img->getAttribute("data-attachment"));
                $img->setAttribute("data-attachment", $response[0]['resourceid']);
                $img->setAttribute("src", $response[0]['fileurl']);
              }
            }
          }
        }
      }

     try {
      foreach($vrscremove as $delvrsc ){ 
         $delvrsc->parentNode->removeChild($delvrsc); 
      } 
     } catch (Exception $ex) { 
        //Handle error
     }
      $html2 = "";
      foreach ($dom->getElementsByTagName("body")->item(0)->childNodes as $subNode)
      {
        $html2 .= $subNode->C14N();
      }
      $result = array();
      $result['moduleids'] = $moduleids;
      $result['fileids'] = $fileids;
      $result['html'] = $html2;
      return $result;
    }

    /**
     * Clone a course
     *
     */
    public static function clone_course($course) {
      global $CFG, $USER, $DB;

      $params = self::validate_parameters(self::clone_course_parameters(), array('course'=>$course));


      $warnings = array();
      $return = array();
      $coursecode = 0;
      $clonecourseid = 0;
      try {
        $courseid = $DB->get_field('monorail_course_data', 'courseid', array('code'=>$course['coursecode']),MUST_EXIST);
        if ($courseid == 0) {
          throw new Exception();
        }
        // access check

        $newCourseData = array();
        $licence = $DB->get_field("monorail_course_data", "licence", array("courseid" => $courseid));

        if ($licence != 2) {
            try {
                $context = get_context_instance(CONTEXT_COURSE, $courseid);
                require_capability('moodle/course:manageactivities', $context);
            } catch (Exception $e) {
                $exceptionparam = new stdClass();
                $exceptionparam->message = $e->getMessage();
                $exceptionparam->catid = $courseid;
                throw new moodle_exception('errorcontextnotvalid', 'webservice', '', $exceptionparam);
            }
        } else {
            // Cloning a CC course.
            $newCourseData["licence"] = 2;
            $newCourseData["derivedfrom"] = $courseid;
        }

        $courses = array();
        $ccourse = array();
        $ccourse['fullname'] = $course['fullname'];
        $ccourse['categoryid'] = $course['category'];
        if (array_key_exists('shortname', $course)) {
          $ccourse['shortname'] = $course['shortname'];
        }
        if (array_key_exists('startdate', $course) && ($course['startdate'] > 0)) {
          $ccourse['startdate'] = $course['startdate'];
        } else {
          $ccourse['startdate'] = 0;
        }
        if (array_key_exists('enddate', $course) && ($course['enddate'] > 0)) {
          $ccourse['enddate'] = $course['enddate'];
        }
        $ccourse['course_logo'] = $course['course_logo'];
        $ccourse['mainteacher'] = $USER->id;
        $usercohort = $DB->get_record('cohort_members', array('userid'=>$USER->id));
        if($usercohort) {
          $ccourse['cohortid'] = $usercohort->cohortid;
        }
        $ccourse['coursebackground'] = $course['background'];
        $courses['course'] = $ccourse;
        $crsresp = self::create_courses($courses);
        // Ok we have the basic clone course now
        $clonecrs = array_shift($crsresp);
        $clonecrsid = $clonecrs['id'];

        if (isset($course["cert_enabled"]))
        {
            monorail_data('BOOLEAN', $clonecrsid, 'cert_enabled', $course['cert_enabled'], true);
        }

        if (isset($course["language"]))
        {
            monorail_data('TEXT', $clonecrsid, 'language', $course['language'], true);
        }

        if (isset($course["course_country"])) {
            monorail_data('TEXT', $clonecrsid, 'course_country', $course['course_country'], true);
        }

        if (isset($course["keywords"]))
        {
            monorail_data('TEXT', $clonecrsid, 'keywords', $course['keywords'], true);
        }

        require_once("$CFG->dirroot/theme/monorail/skills.php");
        $skills = monorail_get_course_skills($courseid);
        $sarray = array();
        foreach ($skills as $skill) {
          $skillrec = monorail_get_skill($skill->skillid);
          $sarray[] = $skillrec->name;
        }
        self::save_skills($clonecrsid, $sarray); // save skills of tyhe course
        $clonecourseid = $clonecrsid;
        // Now iterate thru the course sections and copy the same
        $origcourse = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $courseObj = (object) array('id' => $courseid, 'modinfo' => null, 'sectioncache' => null);
        $origmodinfo = get_fast_modinfo($courseObj);
        $origcrssections = get_all_sections($origcourse->id);
        $unsupportedmodules = array();
        $assignmodules = array();
        foreach($origcrssections as $origsection) {
          //If summary has some embedded resource
          $ccsection =array();
          $csections = array();
          $ccsection['id'] = 0;
          $ccsection['summary'] = $origsection->summary;
          $ccsection['name'] = $origsection->name;
          $ccsection['summaryformat'] = $origsection->summaryformat;
          $ccsection['course'] = (int)$clonecrsid;
          $ccsection['visible'] = $origsection->visible;
          $ccsection['availablefrom'] = $origsection->availablefrom;
          $ccsection['availableuntil'] = $origsection->availableuntil;
          $ccsection['showavailability'] = $origsection->showavailability;
          $ccsection['groupingid'] = $origsection->groupingid;
          $csections[] = $ccsection;
          $clonecrssec = self::update_course_sections($csections, array(), true);
          $newsection = array_shift($clonecrssec['section']);
          $newsectionid = $newsection['sectionid'];
          //Update summary now
          $cncsection['id'] = $newsectionid;
          $helperutil = self::utility_clone_inline_attachments(array('html' =>  $origsection->summary,
              'course' => $clonecrsid, 'section' => $newsectionid));
          $clonedmodules = $helperutil['moduleids'];
          $cncsection['summary'] =$helperutil['html'] ;
          $cnsections[] = $cncsection;
          self::update_course_sections($cnsections, array(), true);
          //Course section has been created , now clone all modules of this section
          //for each module of the section
          if (isset($origmodinfo->sections[$origsection->section])) {
            foreach ($origmodinfo->sections[$origsection->section] as $cmid) { //matching /course/lib.php:print_section() logic
              $cm = $origmodinfo->cms[$cmid];
              //now clone the course modules
              $modulename = $cm->modname;
              switch ($modulename)
              {
                case "assign":
                case "quiz":
                  $assignmodules[] = $cm->id;
                  break;
                case "forum" :
                  //No need to do anything here , forum will be created for section 0 and old forum posts are
                  //not required to be cloned
                  break;
                case "resource" :
                  if(!in_array($cm->id, $clonedmodules)) {
                    //Clone the resource
                    $fs = get_file_storage();
                    $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
                    $files = $fs->get_area_files($modcontext->id, 'mod_resource', 'content');
                    foreach ($files as $file)
                    {
                      if ($file->get_filename() != ".")
                      {
                        $response = self::utility_clone_files($file, array('course'=> (int)$clonecrsid, 'section' => $newsectionid));
                        break;
                      }
                    }
                  }
                  break;
                case "url" :
                  $moduleinfo = $DB->get_record('course_modules', array('id' => $cm->id));
                  $origurl = $DB->get_record('url', array('id' => $moduleinfo->instance));
                  if($origurl) {
                    //Add new course url resource
                    $urls = array();
                    $uresource = array();
                    $uresources = array();
                    $uresource['course'] = $clonecrsid;
                    $uresource['sectionid'] = $newsectionid;
                    $uresource['name'] = $origurl->name;
                    $uresource['externalurl'] = $origurl->externalurl;
                    $uresources[] = $uresource;
                    $response = array_shift(self::add_course_rurls($uresources));
                  }
                  break;
                case "lti" :
                  //Configured modules , cannot be cloned
                  array_push($unsupportedmodules, "lti");
                  break;
                case "kalvidres" :
                  //Configured modules , cannot be cloned
                  array_push($unsupportedmodules, "kalvidres");
                  break;
              }
            }
          }
        }
        //All the course sections have been copied
        //update the cache for thie new course
        rebuild_course_cache($clonecourseid);
        foreach($assignmodules as $amod) {
          $taskdetails = self::util_get_task_details($amod, false);
          if($taskdetails) {
            switch($taskdetails['modulename']) {
              case 'assign' :
                $assignments = array();
                $assignment = array();
                $assignment['course'] = $clonecrsid;
                //$assignment['sectionid'] = $newsectionid;
                if(array_key_exists('duedate', $taskdetails) && ($taskdetails['duedate'] > 0)) {
                  $assignment['duedate'] = strftime("%Y-%m-%dT%T %z", 
                    (($course['startdate']) + (strtotime($taskdetails['duedate']) - $origcourse->startdate)));
                }
                $assignment['nosubmissions'] = $taskdetails['nosubmissions'];
                $assignment['visible'] = 1 ;
                $assignment['name'] = $taskdetails['name'];
                $assignment['modulename'] = 'assign';
                $assignment['attempt'] = 0;
                $assignment['quiet'] = 1;
                $assignments[] = $assignment;
                $response = array_shift(self::util_update_assignments($assignments, false)); //Not validating context;
                $assign_mid = $response[0]['assignid'];
                $assignid = $response[0]['id'];
                //update course assignment files
                $helperutil = self::utility_clone_inline_attachments(array('html' => $taskdetails['intro'],
                    'assignmentid' => $assign_mid), 'assign');
                $fileids = $helperutil['fileids'];
                $assignment['id'] = $assignid;
                $assignment['modulename'] = 'assign';
                $assignment['instanceid'] = $assign_mid;
                $assignment['intro'] = $helperutil['html'];
                $assignments = array();
                $assignments[] = $assignment;
                $response = array_shift(self::util_update_assignments($assignments, false));
                //now copy file resourses
                $files = $taskdetails['fileinfo'];
                if($files) {
                  $fs = get_file_storage();
                  //Create a new resource for this course and section
                  foreach($files as $taskfile) {
                    if(!in_array($taskfile['id'], $fileids)) {
                      $file = $fs->get_file_by_id($taskfile['id']);
                      if($file) {
                        $response = self::utility_clone_files($file, array('assignmentid' => $assign_mid),'assign');
                      }
                    }
                  }
                }
                break;
              case 'quiz':
                $assignments = array();
                $assignment = array();
                $assignment['course'] = $clonecrsid;
                $assignment['intro'] = $taskdetails['intro'];
                $assignment['name'] = $taskdetails['name'];
                
                if(array_key_exists('duedate', $taskdetails) && ($taskdetails['duedate'] > 0)) {
                  $assignment['duedate'] = strftime("%Y-%m-%dT%T %z", (($course['startdate']) + (strtotime($taskdetails['duedate']) - $origcourse->startdate)));
                }
                $assignment['nosubmissions'] = $taskdetails['nosubmissions'];
                $assignment['visible'] = 1 ;
                $assignment['modulename'] = 'quiz';
                $assignment['attempt'] = 0;
                $assignment['attempts'] = $taskdetails['attempts'];
                $assignment['quiet'] = 1;
                $assignments[] = $assignment;
                $response = array_shift(self::util_update_assignments($assignments, false));
                $quiz_mid = $response[0]['assignid'];
                //now copy file resourses
                $questions = $taskdetails['questions'];
                $qfileids = array();
                if($questions) {
                  $clonequestions = array();
                  foreach($questions as $question) {
                    $question['id'] = 0;
                    $question['instanceid'] = 0;
                    $helperutil = self::utility_clone_inline_attachments(array('html' => $question['text'],
                      'assignmentid' => $quiz_mid), 'assign');
                    $question['text'] = $helperutil['html'];
                    $qfileids= array_merge($qfileids,$helperutil['fileids']);
                    $clonequestions[] = $question;
                  }
                  $assignments = array();
                  $assignment = array();
                  $helperutil = self::utility_clone_inline_attachments(array('html' => $taskdetails['intro'],
                      'assignmentid' => $quiz_mid), 'assign');
                  $qfileids= array_merge($qfileids,$helperutil['fileids']);
                  $assignment['intro'] = $helperutil['html'];
                  $assignment['name'] = $taskdetails['name'];
                  if(array_key_exists('duedate', $taskdetails)) {
                    $assignment['duedate'] = strftime("%Y-%m-%dT%T %z", 
                               (($course['startdate']) + (strtotime($taskdetails['duedate']) - $origcourse->startdate)));
                  }
                  $assignment['nosubmissions'] = $taskdetails['nosubmissions'];
                  $assignment['visible'] = 1 ;
                  $assignment['course'] = $clonecrsid;
                  $assignment['id'] = $response[0]['id'];
                  $assignment['instanceid'] = $quiz_mid;
                  $assignment['questions'] = $clonequestions;
                  $assignment['modulename'] = 'quiz';
                  $assignment['attempt'] = 0;
                  $assignment['quiet'] = 1;
                  $assignments[] = $assignment;
                  $response = self::util_update_assignments($assignments, false);
                  //now copy file resourses
                  $files = $taskdetails['fileinfo'];
                  if($files) {
                    $fs = get_file_storage();
                    //Create a new resource for this course and section
                    foreach($files as $taskfile) {
                      if(!in_array($taskfile['id'], $qfileids)) {
                        $file = $fs->get_file_by_id($taskfile['id']);
                        if($file) {
                          $response = self::utility_clone_files($file, array('assignmentid' => $quiz_mid),'assign');
                          $optionData = monorail_data('assignfile', $file->get_id(), 'options');
                          $optionData = reset($optionData);
                          if (($optionData !== FALSE) && $response)
                          {
                             monorail_data('assignfile', $response[0]['fileid'], 'options', $optionData->value);
                          }
                        }
                      }
                    }
                  }
                }
                break;
              case 'quiz':
                break;
            }
          }
        }
        $courseData = $DB->get_record("monorail_course_data", array("courseid" => $clonecourseid));
        if ($courseData) {
          $DB->set_field('course', 'shortname', $courseData->code, array('id'=>$clonecourseid));
          $return['coursecode'] = $courseData->code;

            if (!empty($newCourseData)) {
                foreach ($newCourseData as $ndKey => $ndVal) {
                    $courseData->$ndKey = $ndVal;
                }

                $DB->update_record("monorail_course_data", $courseData);
            }
        }
      } catch (Exception $ex) {

        $course = $DB->get_record('course', array('id'=>$clonecourseid));
        if($course) {
          self::delete_course($clonecourseid, false);
        }
        $warning = array();
        $warning['item'] = 'clone_course';
        $warning['itemid'] = 1;
        $warning['warningcode'] = '1';
        $warning['message'] = 'Error when cloning course :'.$ex->getMessage();
        $warnings[] = $warning;
        add_to_log(1, 'monorailservices', 'externallib.clone_course', '', 'ERROR: Failed to clone course: '.$USER->id.'/'.' - '.$ex->getMessage());
      }

      if(count($unsupportedmodules) > 0) {
        $return['errormodules'] = implode(",", $unsupportedmodules);
      }
      $return['warnings'] = $warnings;
      return $return;
    }


    public static function clone_course_returns() {
      return new external_single_structure(
          array(
              'coursecode' => new external_value(PARAM_TEXT, 'course code that was created', VALUE_OPTIONAL),
              'errormodules' => new external_value(PARAM_TEXT, 'Modules that failed to clone', VALUE_OPTIONAL),
              'warnings'  => new external_warnings()
          )
      );
    }

    public static function get_usertags_parameters()
    {
        return new external_function_parameters(array());
    }

    public static function get_usertags()
    {
        global $DB, $USER;

        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        if (!$cohort)
        {
            $cohort = $DB->get_field('cohort_members', 'cohortid', array('userid' => $USER->id), IGNORE_MULTIPLE);
        }

        if ($cohort)
        {
            $tags = $DB->get_records('monorail_usertag', array('cohortid' => $cohort), 'id,name');

            foreach ($tags as $tag)
            {
                $results[] = array("id" => $tag->id, "name" => $tag->name);
            }
        }

        return $results;
    }

    public static function get_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'ID of tag'),
                'name' => new external_value(PARAM_TEXT, 'tag name')
            ))
        );
    }

    public static function create_usertags_parameters()
    {
        return new external_function_parameters(
            array('tags' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'name'  => new external_value(PARAM_TEXT, 'Tag name')
                    )), 'list of tag/group information')
            )
        );
    }

    public static function create_usertags($tags)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::create_usertags_parameters(), array('tags' => $tags));
        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort admins are allowed to create tags (groups).

        if ($cohort)
        {
            foreach ($tags as $tag)
            {
                $rec = new stdClass();

                $rec->name = $tag["name"];
                $rec->cohortid = $cohort;

                $id = $DB->insert_record("monorail_usertag", $rec);

                $results[] = array("id" => $id, "name" => $tag["name"]);
            }
        }

        return $results;
    }

    public static function create_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'ID of new tag'),
                'name' => new external_value(PARAM_TEXT, 'Tag name')
            ))
        );
    }

    public static function update_usertags_parameters()
    {
        return new external_function_parameters(
            array('tags' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'Tag id'),
                        'name'  => new external_value(PARAM_TEXT, 'New tag name')
                    )), 'list of tag/group information')
            )
        );
    }

    public static function update_usertags($tags)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::update_usertags_parameters(), array('tags' => $tags));
        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort admins are allowed to edit tags (groups).

        if ($cohort)
        {
            foreach ($tags as $tag)
            {
                $tagcohort = $DB->get_field('monorail_usertag', 'cohortid', array('id' => $tag["id"]), IGNORE_MULTIPLE);

                // Only updating tags that belong to the cohort current
                // user is admin of.

                if ($cohort == $tagcohort)
                {
                    $rec = new stdClass();

                    $rec->id = $tag["id"];
                    $rec->name = $tag["name"];

                    $DB->update_record("monorail_usertag", $rec);

                    $results[] = $tag;
                }
            }
        }

        return $results;
    }

    public static function update_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'ID of updated tag'),
                'name' => new external_value(PARAM_TEXT, 'Tag name')
            ))
        );
    }

    public static function delete_usertags_parameters()
    {
        return new external_function_parameters(
            array('tags' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'id'  => new external_value(PARAM_INT, 'tag id')
                    )), 'list of tag/group information')
            )
        );
    }

    public static function delete_usertags($tags)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::delete_usertags_parameters(), array('tags' => $tags));
        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort admins are allowed to delete tags (groups).

        if ($cohort)
        {
            foreach ($tags as $tag)
            {
                $tagcohort = $DB->get_field('monorail_usertag', 'cohortid', array('id' => $tag["id"]), IGNORE_MULTIPLE);

                // Only updating tags that belong to the cohort current
                // user is admin of.

                if ($cohort == $tagcohort)
                {
                    $enrols = $DB->get_records("monorail_usertag_enrolment", array("tagid" => $tag["id"]));
                    $unenrolments = array();

                    foreach ($enrols as $enrol)
                    {
                        $unenrolments[] = array(
                            "courseid" => $enrol->courseid,
                            "userid" => $enrol->userid
                        );
                    }

                    if (!empty($unenrolments))
                    {
                        self::unenrol_users($unenrolments);
                    }

                    $DB->delete_records("monorail_usertag_enrolment", array("tagid" => $tag["id"]));
                    $DB->delete_records("monorail_usertag_user", array("tagid" => $tag["id"]));
                    $DB->delete_records("monorail_usertag_course", array("tagid" => $tag["id"]));
                    $DB->delete_records("monorail_usertag", array("id" => $tag["id"]));

                    $results[] = array("id" => $tag["id"]);
                }
            }
        }

        return $results;
    }

    public static function delete_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'ID of deleted tag')
            ))
        );
    }

    public static function assign_usertags_parameters()
    {
        return new external_function_parameters(
            array('tags' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'tagid' => new external_value(PARAM_INT, 'Tag id'),
                        'userid'  => new external_value(PARAM_INT, 'User id')
                    )), 'list of tag/user pairs')
            )
        );
    }

    public static function assign_usertags($tags)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::assign_usertags_parameters(), array('tags' => $tags));
        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort admins are allowed to assign tags.

        if ($cohort)
        {
            foreach ($tags as $tag)
            {
                if (!$DB->record_exists("monorail_usertag_user", array("tagid" => $tag["tagid"], "userid" => $tag["userid"])))
                {
                    $tagcohort = $DB->get_field('monorail_usertag', 'cohortid', array('id' => $tag["tagid"]), IGNORE_MULTIPLE);

                    // Only updating tags that belong to the cohort current
                    // user is admin of.

                    if ($cohort == $tagcohort)
                    {
                        $rec = new stdClass();

                        $rec->tagid = $tag["tagid"];
                        $rec->userid = $tag["userid"];

                        $tag["id"] = $DB->insert_record("monorail_usertag_user", $rec);

                        // Automatically enrol to courses where this tag is
                        // enrolled.
                        $tagCourses = $DB->get_records_sql("SELECT uc.courseid AS courseid, role.shortname AS role" .
                                " FROM {monorail_usertag_course} AS uc INNER JOIN {role} AS role ON role.id=uc.roleid" .
                                " WHERE uc.tagid=?", array($tag["tagid"]));

                        $enrolments = array();

                        foreach ($tagCourses as $tc)
                        {
                            $enrolments[] = array(
                                "rolename" => $tc->role,
                                "courseid" => $tc->courseid,
                                "userid" => $tag["userid"]
                            );

                            // TODO: check if user already enroled to this
                            // course.
                            $enrolRec = new stdClass();

                            $enrolRec->tagid = $tag["tagid"];
                            $enrolRec->courseid = $tc->courseid;
                            $enrolRec->userid = $tag["userid"];

                            $DB->insert_record("monorail_usertag_enrolment", $enrolRec);
                        }

                        if (!empty($enrolments))
                        {
                            self::cohort_course_enrol($enrolments, array());
                        }

                        $results[] = $tag;
                    }
                }
            }
        }

        return $results;
    }

    public static function assign_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'Relation id'),
                'tagid' => new external_value(PARAM_INT, 'Tag id'),
                'userid' => new external_value(PARAM_TEXT, 'User id')
            ))
        );
    }

    public static function unassign_usertags_parameters()
    {
        return new external_function_parameters(
            array('tags' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'tagid' => new external_value(PARAM_INT, 'Tag id'),
                        'userid'  => new external_value(PARAM_INT, 'User id')
                    )), 'list of tag/user pairs')
            )
        );
    }

    public static function unassign_usertags($tags)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::unassign_usertags_parameters(), array('tags' => $tags));
        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort admins are allowed to assign tags.

        if ($cohort)
        {
            foreach ($tags as $tag)
            {
                $tagcohort = $DB->get_field('monorail_usertag', 'cohortid', array('id' => $tag["tagid"]), IGNORE_MULTIPLE);

                // Only updating tags that belong to the cohort current
                // user is admin of.

                if ($cohort == $tagcohort)
                {
                    $DB->delete_records("monorail_usertag_user", array("userid" => $tag["userid"], "tagid" => $tag["tagid"]));

                    // TODO: if this tag is enroled to course and there are
                    // users that were enroled to this course because they
                    // were members of this tag, unenrol the users (TODO:
                    // must keep track of users who are enroled as tag
                    // members to separate from manually enroled ones...)

                    $results[] = $tag;
                }
            }
        }

        return $results;
    }

    public static function unassign_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'tagid' => new external_value(PARAM_INT, 'Tag id'),
                'userid' => new external_value(PARAM_INT, 'User id')
            ))
        );
    }

   /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function cohort_course_usertags_parameters()
    {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'course ID') 
            )
        );
    }

    /**
     * @param array $cohortid id of the cohort
     * @return array An array of arrays describing users
     * @since Moodle 2.2
     */
    public static function cohort_course_usertags($courseid)
    {
        global $USER, $DB;

        $params = self::validate_parameters(self::cohort_course_usertags_parameters(),
            array('courseid'=>$courseid));

        $result = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort user can get tag list of a course.

        if ($cohort)
        {
            $tags = $DB->get_records_sql("SELECT t.id AS id, t.name AS name, r.shortname AS rolename, uc.id AS relid FROM {monorail_usertag} AS t" .
                " INNER JOIN {monorail_usertag_course} AS uc ON uc.tagid=t.id" .
                " INNER JOIN {role} AS r ON r.id=uc.roleid WHERE t.cohortid=? AND uc.courseid=?",
                    array($cohort, $courseid));

            foreach ($tags as $tag)
            {
                $result[] = array(
                    "id" => $tag->relid,
                    "name" => $tag->name,
                    "tagid" => $tag->id,
                    "courseid" => $courseid,
                    "rolename" => $tag->rolename);
            }
        }

        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function cohort_course_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'Relation id'),
                'name' => new external_value(PARAM_TEXT, 'Tag name'),
                'tagid' => new external_value(PARAM_INT, 'Tag id'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'rolename' => new external_value(PARAM_TEXT, 'Role name')
            ))
        );
    }

    public static function enrol_usertags_parameters()
    {
        return new external_function_parameters(
            array('tags' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'tagid' => new external_value(PARAM_INT, 'Tag id'),
                        'courseid'  => new external_value(PARAM_INT, 'Course id'),
                        'rolename' => new external_value(PARAM_TEXT, 'Role name'),
                    )), 'list of tag/course pairs')
            )
        );
    }

    public static function enrol_usertags($tags)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::enrol_usertags_parameters(), array('tags' => $tags));
        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort admins are allowed to assign tags.

        if ($cohort)
        {
            foreach ($tags as $tag)
            {
                if (!$DB->record_exists("monorail_usertag_course", array("tagid" => $tag["tagid"], "courseid" => $tag["courseid"])))
                {
                    $tagcohort = $DB->get_field('monorail_usertag', 'cohortid', array('id' => $tag["tagid"]), IGNORE_MULTIPLE);

                    // Only updating tags that belong to the cohort current
                    // user is admin of.

                    if ($cohort == $tagcohort)
                    {
                        $roleid = $DB->get_field("role", "id", array("shortname" => $tag["rolename"]));

                        $rec = new stdClass();

                        $rec->tagid = $tag["tagid"];
                        $rec->courseid = $tag["courseid"];
                        $rec->roleid = $roleid;

                        $tag["id"] = $DB->insert_record("monorail_usertag_course", $rec);

                        $results[] = $tag;

                        // Enrol existing members of this tag
                        $tagUsers = $DB->get_records_sql("SELECT userid FROM {monorail_usertag_user} WHERE tagid=?", array($tag["tagid"]));
                        $enrolments = array();

                        foreach ($tagUsers as $usr)
                        {
                            $enrolments[] = array(
                                "rolename" => $tag["rolename"],
                                "courseid" => $tag["courseid"],
                                "userid" => $usr->userid
                            );

                            // TODO: check if user already enroled to this
                            // course.
                            $enrolRec = new stdClass();

                            $enrolRec->tagid = $tag["tagid"];
                            $enrolRec->courseid = $tag["courseid"];
                            $enrolRec->userid = $usr->userid;

                            $DB->insert_record("monorail_usertag_enrolment", $enrolRec);
                        }

                        if (!empty($enrolments))
                        {
                            self::cohort_course_enrol($enrolments, array());
                        }
                    }
                }
            }
        }

        return $results;
    }

    public static function enrol_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'id' => new external_value(PARAM_INT, 'Relation id'),
                'tagid' => new external_value(PARAM_INT, 'Tag id'),
                'courseid' => new external_value(PARAM_INT, 'Course id'),
                'rolename' => new external_value(PARAM_TEXT, 'Role name')
            ))
        );
    }

    public static function unenrol_usertags_parameters()
    {
        return new external_function_parameters(
            array('tags' => new external_multiple_structure (
                new external_single_structure(
                    array(
                        'tagid' => new external_value(PARAM_INT, 'Tag id'),
                        'courseid'  => new external_value(PARAM_INT, 'Course id'),
                    )), 'list of tag/course pairs')
            )
        );
    }

    public static function unenrol_usertags($tags)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::unenrol_usertags_parameters(), array('tags' => $tags));
        $results = array();

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        // Only cohort admins are allowed to assign tags.

        if ($cohort)
        {
            foreach ($tags as $tag)
            {
                $tagcohort = $DB->get_field('monorail_usertag', 'cohortid', array('id' => $tag["tagid"]), IGNORE_MULTIPLE);

                // Only updating tags that belong to the cohort current
                // user is admin of.

                if ($cohort == $tagcohort)
                {
                    $enrols = $DB->get_records("monorail_usertag_enrolment", array("tagid" => $tag["tagid"], "courseid" => $tag["courseid"]));
                    $unenrolments = array();

                    foreach ($enrols as $enrol)
                    {
                        $unenrolments[] = array(
                            "courseid" => $tag["courseid"],
                            "userid" => $enrol->userid
                        );
                    }

                    if (!empty($unenrolments))
                    {
                        self::unenrol_users($unenrolments);
                    }

                    $DB->delete_records("monorail_usertag_enrolment", array("tagid" => $tag["tagid"], "courseid" => $tag["courseid"]));
                    $DB->delete_records("monorail_usertag_course", array("tagid" => $tag["tagid"], "courseid" => $tag["courseid"]));

                    $results[] = $tag;
                }
            }
        }

        return $results;
    }

    public static function unenrol_usertags_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(array(
                'tagid' => new external_value(PARAM_INT, 'Tag id'),
                'courseid' => new external_value(PARAM_INT, 'Course id')
            ))
        );
    }

    public static function get_course_grades_parameters()
    {
        return new external_function_parameters(array(
            'courseid' => new external_value(PARAM_INT, 'Course id')
        ));
    }

    public static function get_course_grades($courseid)
    {
        global $DB, $USER, $CFG;

        $params = self::validate_parameters(self::get_course_grades_parameters(), array('courseid' => $courseid));
        $users = array();

        $context = context_course::instance($courseid);

        require_capability('moodle/course:manageactivities', $context);
        require_capability('moodle/course:viewparticipants', $context);

        require_once($CFG->libdir . '/gradelib.php');

        $tasks = array();

        $extrafields='m.id as assignmentid, m.course, m.nosubmissions, m.submissiondrafts, m.sendnotifications, '.
                     'm.sendlatenotifications, m.duedate, m.allowsubmissionsfromdate, m.grade, m.timemodified,
                      m.intro, m.introformat ';
        $modules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, $extrafields
                               FROM {course_modules} cm, {modules} md, {assign} m
                               WHERE cm.course = $courseid AND
                                    cm.instance = m.id AND
                                    md.name = 'assign' AND
                                    (NOT m.nosubmissions) AND
                                    md.id = cm.module ORDER BY m.duedate ASC", array());

        foreach ($modules as $module)
        {
            $tasks[] = array(
                "id" => $module->id,
                "assignmentid" => $module->assignmentid,
                "name" => $module->name,
                "mod" => "assign");
        }

        $extrafields='m.id as assignmentid, m.course, m.timemodified, m.timeclose AS duedate ';
        $qmodules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, $extrafields
                               FROM {course_modules} cm, {modules} md, {quiz} m
                               WHERE cm.course = $courseid AND
                                    cm.instance = m.id AND
                                    md.name = 'quiz' AND
                                    md.id = cm.module ORDER BY m.timeclose ASC", array());

        foreach ($qmodules as $module)
        {
            $grade = "";

            $tasks[] = array(
                "id" => $module->id,
                "assignmentid" => $module->assignmentid,
                "name" => $module->name,
                "mod" => "quiz",
                "usergrades" => array());
        }

        list($enrolledsql, $enrolledparams) = get_enrolled_sql($context);
        list($ctxselect, $ctxjoin) = context_instance_preload_sql('u.id', CONTEXT_USER, 'ctx');
        $sqlparams['courseid'] = $courseid;
        $sql = "SELECT u.* $ctxselect
                  FROM {user} u $ctxjoin
                 WHERE u.id IN ($enrolledsql)
                 ORDER BY u.firstname ASC";

        $enrolledusers = $DB->get_recordset_sql($sql, $enrolledparams);

        $courseCohortId = $DB->get_field_sql("SELECT cohortid FROM {monorail_cohort_courseadmins} WHERE courseid=?",
            array($courseid));

        foreach ($enrolledusers as $user)
        {
            $roles = get_user_roles($context, $user->id, false);
            $roleok = true;

            foreach ($roles as $role)
            {
                if ($role->shortname == "editingteacher" || $role->shortname == "manager")
                {
                    $roleok = false;
                    break;
                }
            }

            if (!$roleok)
            {
                continue;
            }

            $grades = array();
            $gradePercent = array();
            $allSubmitted = true;

            foreach ($tasks as &$task)
            {
                $grade = "-";
                $feedback = "";
                $submitted = false;

                if ($task["mod"] == "assign")
                {
                    $grading_info = grade_get_grades($courseid, 'mod', "assign", $task["assignmentid"], $user->id);

                    foreach ($grading_info->items as $gi)
                    {
                        foreach ($gi->grades as $g)
                        {
                            if ($g->grade !== null)
                            {
                                $grade = round($g->grade);
                            }

                            if ($g->feedback)
                            {
                                $feedback = $g->feedback;
                            }

                            if ($g->grade !== null && $gi->grademax)
                            {
                                $gradePercent[] = $task["usergrades"][] = round(($g->grade / $gi->grademax) * 100);
                                $submitted = true;
                            }
                            else
                            {
                                //$gradePercent[] = $task["usergrades"][] = 0;
                                //$gradePercent[] = 0;
                            }
                            break;
                        }
                    }

                    // http://cdn.meme.am/instances/500x/66004438.jpg
                    if ($grade === "-")
                    {
                        // No grade - check for submission.
                        $subStatus = $DB->get_field_sql("SELECT status FROM {assign_submission} WHERE assignment=? AND userid=?",
                            array($task["assignmentid"], $user->id));

                        if ($subStatus == "submitted") {
                            $grade = "*";
                        }
                    }
                }
                else if ($task["mod"] == "quiz")
                {
                    $attemptData = null;
                    $stateData = monorail_data('quiz_end', $task["assignmentid"], "user" . $user->id);

                    if (!empty($stateData)) {
                        $stateData = reset($stateData);

                        if ((int) $stateData->value) {
                            $attemptData = monorail_data('quiz_attempt_best', $task["assignmentid"], "user" . $user->id);

                            if (empty($attemptData))
                            {
                                $attemptData = monorail_data('quiz_attempt', $task["assignmentid"], "user" . $user->id);
                            }
                        }
                    }

                    if (!empty($attemptData))
                    {
                        $attemptData = reset($attemptData);
                        $attemptData = unserialize($attemptData->value);

                        $quizTotal = 0;
                        $quizCorrect = 0;

                        foreach ($attemptData as $question)
                        {
                            $allOk = true;

                            if (isset($question["correct"])) {
                                $allOk = $question["correct"];
                            } else {
                                foreach ($question["answers"] as $answer)
                                {
                                    if ($answer["correct"] != $answer["checked"])
                                    {
                                        $allOk = false;
                                    }
                                }
                            }

                            $quizTotal++;

                            if ($allOk)
                            {
                                $quizCorrect++;
                            }
                        }

                        //$grade = $quizCorrect . '/' . $quizTotal;
                        $grade = round(($quizCorrect / $quizTotal) * 100);

                        if ($quizTotal)
                        {
                            $gradePercent[] = $task["usergrades"][] = round(($quizCorrect / $quizTotal) * 100);
                            $submitted = true;
                        }
                        else
                        {
                            //$gradePercent[] = $task["usergrades"][] = 0;
                            //$gradePercent[] = 0;
                        }
                    }
                    else
                    {
                        //$gradePercent[] = $task["usergrades"][] = 0;
                        //$gradePercent[] = 0;
                    }
                }

                $grades[] = array(
                    "taskid" => $task["id"],
                    "taskname" => $task["name"],
                    "grade" => $grade,
                    "feedback" => $feedback
                );

                if (!$submitted)
                {
                    $allSubmitted = false;
                }
            }

            if ($courseCohortId) {
                $userGroups = $DB->get_fieldset_sql("SELECT ut.name FROM {monorail_usertag_user} AS utu " .
                    "INNER JOIN {monorail_usertag} AS ut ON utu.tagid=ut.id " .
                        "WHERE utu.userid=? AND ut.cohortid=?", array($user->id, $courseCohortId));
            } else {
                $userGroups = array();
            }

            $users[] = array(
                "userid" => $user->id,
                "firstname" => $user->firstname,
                "lastname" => $user->lastname,
                "email" => $user->email,
                "groups" => implode(", ", $userGroups),
                "picturehash" => monorail_get_userpic_hash($user->id),
                "grades" => $grades,
                "average" => (empty($gradePercent) ? "-" : round(array_sum($gradePercent) / count($gradePercent), 2)),
                "all_submitted" => $allSubmitted ? 1 : 0
            );
        }

        $enrolledusers->close();

        // Calculate average per task.

        foreach ($tasks as &$task)
        {
            if (!empty($task["usergrades"]))
            {
                $task["average"] = round(array_sum($task["usergrades"]) / count($task["usergrades"]), 2);
            }
            else
            {
                $task["average"] = "-";
            }

            unset($task["usergrades"]);
        }

        $certs = array();

        foreach ($DB->get_recordset_sql("SELECT id, courseid, userid, state, hashurl FROM {monorail_certificate} WHERE courseid=?", array($courseid)) as $c)
        {
            $certs[] = array(
                "id" => $c->id,
                "courseid" => $c->courseid,
                "userid" => $c->userid,
                "state" => $c->state,
                "hashurl" => $c->hashurl);
        }

        return array(
            "courseid" => $courseid,
            "tasks" => $tasks,
            "users" => $users,
            "certificates" => $certs
        );
    }

    public static function get_course_grades_returns()
    {
        return new external_single_structure(array(
            'courseid' => new external_value(PARAM_INT, 'Course id'),
            "tasks" => new external_multiple_structure(new external_single_structure(array(
                "id" => new external_value(PARAM_INT, "Instance id"),
                "assignmentid" => new external_value(PARAM_INT, "Assignment id"),
                "name" => new external_value(PARAM_TEXT, "Task name"),
                "mod" => new external_value(PARAM_TEXT, "Task module"),
                "average" => new external_value(PARAM_TEXT, "Average grade for comleted tasks (in percents)")
            ))),
            "users" => new external_multiple_structure(new external_single_structure(array(
                "userid" => new external_value(PARAM_INT, "User id"),
                "firstname" => new external_value(PARAM_TEXT, "First name"),
                "lastname" => new external_value(PARAM_TEXT, "Last name"),
                "email" => new external_value(PARAM_TEXT, "User email"),
                "groups" => new external_value(PARAM_TEXT, "User groups"),
                "picturehash" => new external_value(PARAM_TEXT, "User picture"),
                "grades" => new external_multiple_structure(new external_single_structure(array(
                    "taskid" => new external_value(PARAM_INT, "Task id"),
                    "grade" => new external_value(PARAM_TEXT, "Grade string"),
                    "feedback" => new external_value(PARAM_RAW, "Grade feedback", VALUE_OPTIONAL)
                ))),
                "average" => new external_value(PARAM_TEXT, "Average grade for the user (in percents)"),
                "all_submitted" => new external_value(PARAM_INT, "All tasks submitted?", VALUE_DEFAULT, 0)
            ))),
            "certificates" => new external_multiple_structure(new external_single_structure(array(
                "id" => new external_value(PARAM_INT, "Certificate id"),
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "userid" => new external_value(PARAM_INT, "User id"),
                "state" => new external_value(PARAM_INT, "State of certificate (0 - revoked, 1 - issued)"),
                "hashurl" => new external_value(PARAM_TEXT, "Certificate URL hash")
            )))
        ));
    }

    public static function update_certificate_parameters()
    {
        return new external_function_parameters(array(
            "cert" => new external_single_structure(array(
                'id'  => new external_value(PARAM_INT, 'Certificate id', VALUE_OPTIONAL),
                'courseid'  => new external_value(PARAM_INT, 'Course id', VALUE_OPTIONAL),
                'userid'  => new external_value(PARAM_INT, 'User id', VALUE_OPTIONAL),
                'state'  => new external_value(PARAM_INT, 'Certificate state', VALUE_OPTIONAL),
                'hashurl'  => new external_value(PARAM_TEXT, 'URL hash', VALUE_OPTIONAL)
            ))
        ));
    }

    public static function update_certificate($cert)
    {
        global $DB, $USER;

        $params = self::validate_parameters(self::update_certificate_parameters(), array('cert' => $cert));

        $info = array(
            "id" => (int) (@$params["cert"]["id"]),
            "courseid" => (int) (@$params["cert"]["courseid"]),
            "userid" => (int) (@$params["cert"]["userid"]),
            "state" => (int) (@$params["cert"]["state"]),
            "hashurl" => (@$params["cert"]["hashurl"])
        );

        // Must know the course id before proceeding
        if (!$info["courseid"])
        {
            if ($info["id"])
            {
                $info["courseid"] = (int) $DB->get_field("monorail_certificate", "courseid", array("id" => $info["id"]));
            }
            else
            {
                // No course/certificate id - can't do anything!
                return array();
            }
        }

        // Checking user permissions...
        $context = context_course::instance($info["courseid"]);
        $roles = get_user_roles($context, $USER->id, false);
        $roleok = false;

        foreach ($roles as $role)
        {
            if ($role->shortname == "editingteacher" || $role->shortname == "manager")
            {
                $roleok = true;
                break;
            }
        }

        if (!$roleok)
        {
            // Not a teacher - can't issue anything!
            return array();
        }

        wscache_reset_by_user($info["userid"]);

        if ($info["state"])
        {
            // Issuing certificate

            if (!$info["id"])
            {
                // Issue new certificate.

                $oldId = $DB->get_records_sql("SELECT id, state, hashurl FROM {monorail_certificate} WHERE userid=? AND courseid=?",
                    array($info["userid"], $info["courseid"]));

                if (empty($oldId))
                {
                    // Must create new certificate record.
                    $rec = new stdClass();

                    $rec->courseid = $info["courseid"];
                    $rec->userid = $info["userid"];
                    $rec->state = $info["state"];
                    $rec->issuedby = $USER->id;
                    $rec->issuedat = time();
                    $rec->hashurl = $info["hashurl"] = self::create_certificate($info["userid"], $info["courseid"]);

                    if (!$rec->hashurl)
                    {
                        return array();
                    }

                    $info["id"] = $DB->insert_record("monorail_certificate", $rec);
                    social_share_certificate($info["courseid"], $info["userid"]);
                }
                else
                {
                    // Certificate record already exists
                    $oldId = reset($oldId);

                    if ($oldId->state)
                    {
                        // And certificate is issued - nothing to do.
                        $info["id"] = $oldId->id;
                        $info["hashurl"] = $oldId->hashurl;
                    }
                    else
                    {
                        // Re-issuing.
                        $info["id"] = $oldId->id;
                        $info["hashurl"] = $oldId->hashurl = self::create_certificate($info["userid"], $info["courseid"]);

                        if (!$oldId->hashurl)
                        {
                            return array();
                        }

                        $oldId->state = $info["state"];
                        $oldId->issuedby = $USER->id;
                        $oldId->issuedat = time();

                        $DB->update_record("monorail_certificate", $oldId);
                        social_share_certificate($info["courseid"], $info["userid"]);
                    }
                }
            }
            else
            {
                // Re-issue old certificate...
                $oldRec = $DB->get_records_sql("SELECT id, state, hashurl, userid, courseid FROM {monorail_certificate} WHERE id=?", array($info["id"]));

                if (empty($oldRec))
                {
                    // Invalid certificate id - bailing out
                    return array();
                }

                $oldRec = reset($oldRec);

                if ($oldRec->state)
                {
                    // Old certificate still issued.
                    $info["hashurl"] = $oldRec->hashurl;
                }
                else
                {
                    $oldRec->state = $info["state"];
                    $oldRec->hashurl = $info["hashurl"] = self::create_certificate($oldRec->userid, $oldRec->courseid);

                    if (!$oldRec->hashurl)
                    {
                        return array();
                    }

                    $oldRec->issuedby = $USER->id;
                    $oldRec->issuedat = time();

                    $DB->update_record("monorail_certificate", $oldRec);
                    social_share_certificate($oldRec->courseid, $oldRec->userid);
                }
            }
        }
        else
        {
            // Revoking certificate
            $oldRec = $DB->get_records_sql("SELECT id, state, hashurl FROM {monorail_certificate} WHERE id=?", array($info["id"]));

            if (empty($oldRec))
            {
                // Invalid certificate id - bailing out
                return array();
            }

            $oldRec = reset($oldRec);

            if (!$oldRec->state)
            {
                // Certificate already revoked
            }
            else
            {
                // Revoking certificate
                self::destroy_certificate($oldRec->hashurl);

                $oldRec->state = $info["state"];
                $oldRec->issuedby = $USER->id;
                $oldRec->issuedat = time();

                $DB->update_record("monorail_certificate", $oldRec);
            }
        }

        return $info;
    }

    public static function update_certificate_returns()
    {
        return new external_single_structure(array(
            'id'  => new external_value(PARAM_INT, 'Certificate id', VALUE_OPTIONAL),
            'courseid'  => new external_value(PARAM_INT, 'Course id', VALUE_OPTIONAL),
            'userid'  => new external_value(PARAM_INT, 'User id', VALUE_OPTIONAL),
            'state'  => new external_value(PARAM_INT, 'Certificate state', VALUE_OPTIONAL),
            "hashurl" => new external_value(PARAM_TEXT, "Certificate URL hash", VALUE_OPTIONAL)
        ));
    }

    private static function uniord($u)
    {
        // i just copied this function fron the php.net comments, but it should work fine!
        $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
        $k1 = ord(substr($k, 0, 1));
        $k2 = ord(substr($k, 1, 1));
        return $k2 * 256 + $k1;
    }

    private static function center_text($pdf, $text, $x, $y, $w, $size)
    {
        $text2 = iconv("UTF-8", "windows-1252", $text);

        if (mb_strlen($text, "UTF-8") != mb_strlen($text2, "UTF-8"))
        {
            if (self::uniord($text) < 4352)
            {
                $pdf->SetFont('OpenSans');
            }
            else
            {
                $pdf->SetFont('Fallback');
            }
        }

        $pdf->SetFontSize($size);
        $width = $pdf->GetStringWidth($text);

        while ($width > $w)
        {
            $size -= 1;
            $pdf->SetFontSize($size);
            $width = $pdf->GetStringWidth($text);
        }

        $pdf->setXY((($w - $width) / 2 + $x), $y);

        $pdf->Cell(0, 0, $text);
    }

    private static function center_image($pdf, $image, $x, $y, $w, $h, $type = "")
    {
        $size = getimagesize($image);

        $ax = $size[0] / $w;
        $ay = $size[1] / $h;

        $iw = $size[0] / $ax;
        $ih = $size[1] / $ax;

        if ($iw > $w || $ih > $h)
        {
            $iw = $size[0] / $ay;
            $ih = $size[1] / $ay;
        }

        if (!strcasecmp($type, "png")) {
            $pdf->ImagePngWithAlpha($image, ($w - $iw) / 2 + $x, ($h - $ih) / 2 + $y, $iw, $ih);
        } else {
            $pdf->Image($image, ($w - $iw) / 2 + $x, ($h - $ih) / 2 + $y, $iw, $ih, $type);
        }
    }

    private static function create_certificate($userid, $courseid)
    {
        global $CFG, $DB, $USER;

        // Always give the same hash for user/course combination.
        $hash = md5("--" . $userid . "--" . $courseid . "--");

        // Pull data about course/user
        $userData = $DB->get_records_sql("SELECT id, firstname, lastname FROM {user} WHERE id=?", array($userid));

        if (empty($userData))
        {
            // Invalid user
            return null;
        }

        $userData = reset($userData);

        $courseData = $DB->get_records_sql("SELECT id, fullname FROM {course} WHERE id=?", array($courseid));

        if (empty($courseData))
        {
            // Invalid course
            return null;
        }

        $courseData = reset($courseData);
        $teacherId = null;
        $courseCode = null;

        $courseData2 = $DB->get_records_sql("SELECT id, mainteacher, code FROM {monorail_course_data} WHERE courseid=?", array($courseid));

        if (!empty($courseData2))
        {
            $courseData2 = reset($courseData2);

            $courseCode = $courseData2->code;

            if ($courseData2->mainteacher)
            {
                $teacherId = $courseData2->mainteacher;
            }
        }

        if (!$teacherId)
        {
            $context = context_course::instance($courseid);
            $users = get_enrolled_users($context);

            foreach ($users as $user)
            {
                $roles = get_user_roles($context , $user->id);

                foreach ($roles as $role)
                {
                    if ($role->shortname == 'editingteacher')
                    {
                        $teacherId = $user->id;
                        break 2;
                    }
                }
            }
        }

        $teacherData = $DB->get_records_sql("SELECT id, firstname, lastname, institution FROM {user} WHERE id=?", array($teacherId));

        if (empty($teacherData))
        {
            // No teacher found!
            return null;
        }

        $teacherData = reset($teacherData);

        $teacherTitle = get_string("label_teacher", "theme_monorail");

        $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);
        if ($fieldid)
        {
            $title = $DB->get_field('user_info_data', 'data', array('userid'=>$teacherId, 'fieldid' => $fieldid), IGNORE_MULTIPLE);
            if ($title)
            {
                $teacherTitle = $title;
            }
        }

        $monorailData = array();
        $monorailDataRecs = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($courseid));

        foreach ($monorailDataRecs as $md)
        {
            $monorailData[$md->datakey] = $md->value;
        }

        // Make certificate PDF.

        require_once __DIR__ . "/fpdf_alpha.php";
        $fs = get_file_storage();

        if ((int) @$monorailData["cert_custom"]) { // custom certificate...
            $info = json_decode($monorailData["cert_custom_info"]);

            if (@$info->template_id) {
                $file = $fs->get_file_by_id($info->template_id);
                $realfile = self::get_real_filename($file);
                $imagesize = getimagesize($realfile);
                $ext = self::get_file_type($realfile);

                if (substr($ext, 0, 6) == "image/") {
                    $ext = substr($ext, 6);
                } else {
                    // Fallback to file name (not safe).
                    $fname = $file->get_filename();
                    $ext = substr($fname, strrpos($fname, '.') + 1);
                }

                if ($imagesize[0] > $imagesize[1]) {
                    $pdf = new PDF_ImageAlpha('L', 'mm', array(287, 203));

                    $pdf->AddPage();

                    if (!strcasecmp($ext, "png")) {
                        $pdf->ImagePngWithAlpha($realfile, 0, 0, 287, 203);
                    } else {
                        $pdf->Image($realfile, 0, 0, 287, 203, $ext);
                    }

                    $cw = 287;
                    $ch = 203;
                } else {
                    $pdf = new PDF_ImageAlpha('P', 'mm', array(203, 287));

                    $pdf->AddPage();
                    if (!strcasecmp($ext, "png")) {
                        $pdf->ImagePngWithAlpha($realfile, 0, 0, 203, 287);
                    } else {
                        $pdf->Image($realfile, 0, 0, 203, 287, $ext);
                    }

                    $cw = 203;
                    $ch = 287;
                }
            } else {
                $pdf = new PDF_ImageAlpha('L', 'mm', array(287, 203));
                $pdf->AddPage();

                $cw = 287;
                $ch = 203;
            }

            $pdf->AddFont("Cinzel", "", "cinzel.ttf", true);
            $pdf->AddFont("Tangerine", "", "tangerine.ttf", true);
            $pdf->AddFont("OpenSans", "", "opensans.ttf", true);
            $pdf->AddFont("Fallback", "", "DroidSansFallbackFull.ttf", true);

            $pdf->SetTextColor(0, 1, 2);
            $pdf->SetFont('Cinzel');

            if (@$info->student_x && @$info->student_y) {
                $pdf->setXY($info->student_x / 100 * $cw - 1, $info->student_y / 100 * $ch - 1);
                $pdf->SetFontSize(20);
                $pdf->Cell(0, 0, $userData->firstname . " " . $userData->lastname);
            }

            if (@$info->date_x && @$info->date_y) {
                $pdf->setXY($info->date_x / 100 * $cw - 1, $info->date_y / 100 * $ch - 1);
                $pdf->SetFontSize(14);
                $pdf->Cell(0, 0, date("d F, Y", time()));
            }

            if (file_put_contents($CFG->external_data_root . "/../../cert/$hash.pdf", $pdf->Output("", "S")) === FALSE) {
                return null;
            }
        } else { // custom/default certificate...
            $pdf = new PDF_ImageAlpha('L', 'mm', array(287, 203));

            $pdf->AddPage();
            $pdf->AddFont("Cinzel", "", "cinzel.ttf", true);
            $pdf->AddFont("Tangerine", "", "tangerine.ttf", true);
            $pdf->AddFont("OpenSans", "", "opensans.ttf", true);
            $pdf->AddFont("Fallback", "", "DroidSansFallbackFull.ttf", true);

            $pdf->ImagePngWithAlpha(__DIR__ . "/cert/background.png", 0, 0, 287, 203);

            $pdf->SetTextColor(0, 1, 2);

            $pdf->SetFont('Cinzel');
            self::center_text($pdf, get_string('certificate_title', 'theme_monorail'), 0, 41, 287, 32);

            $pdf->SetFont('Cinzel');
            self::center_text($pdf, get_string('is_awarded', 'theme_monorail'), 0, 57, 287, 24);

            $pdf->SetFont('Tangerine');
            self::center_text($pdf, $userData->firstname . " " . $userData->lastname, 20, 82, 247, 42);

            $pdf->SetFont('Cinzel');
            self::center_text($pdf, get_string('for_completing', 'theme_monorail'), 0, 102, 287, 22);

            $pdf->SetFont('Cinzel');
            self::center_text($pdf, $courseData->fullname, 20, 115, 247, 22);

            // XXX: userdate from moodle removes leading zeroes to work around
            // some windows libraries bugs, so not usable here...
            $pdf->SetFont('Cinzel');
            self::center_text($pdf, date("d F, Y", time()), 23, 170, 60, 10);

            $pdf->SetFont('Cinzel');
            self::center_text($pdf, strtoupper($hash), 23, 175, 60, 10);

            $pdf->SetFont('Cinzel');
            self::center_text($pdf, $teacherData->firstname . " " . $teacherData->lastname, 203, 165, 60, 10);
            $pdf->SetFont('Cinzel');
            self::center_text($pdf, $teacherTitle, 203, 170, 60, 10);
            $pdf->SetFont('Cinzel');
            self::center_text($pdf, $teacherData->institution, 203, 175, 60, 10);

            if (@$monorailData["cert_orglogo_fileid"]) {
                $file = $fs->get_file_by_id($monorailData["cert_orglogo_fileid"]);
                $realfile = self::get_real_filename($file);
                $ext = self::get_file_type($realfile);

                if (substr($ext, 0, 6) == "image/") {
                    $ext = substr($ext, 6);
                } else {
                    // Fallback to file name (not safe).
                    $fname = $file->get_filename();
                    $ext = substr($fname, strrpos($fname, '.') + 1);
                }

                self::center_image($pdf, $realfile, 30, 127, 48, 32, $ext);
            }

            if (@$monorailData["cert_sigpic_fileid"]) {
                $file = $fs->get_file_by_id($monorailData["cert_sigpic_fileid"]);
                $realfile = self::get_real_filename($file);
                $ext = self::get_file_type($realfile);

                if (substr($ext, 0, 6) == "image/") {
                    $ext = substr($ext, 6);
                } else {
                    // Fallback to file name (not safe).
                    $fname = $file->get_filename();
                    $ext = substr($fname, strrpos($fname, '.') + 1);
                }

                self::center_image($pdf, $realfile, 209, 127, 48, 32, $ext);
            }

            if (file_put_contents($CFG->external_data_root . "/../../cert/$hash.pdf", $pdf->Output("", "S")) === FALSE) {
                return null;
            }
        }   // Default certificate.

        // End making certificate PDF.

        try {
            $im = new Imagick();
            $im->setResolution(60, 60);
            $im->readImage($CFG->external_data_root . "/../../cert/$hash.pdf[0]");
            $im->setImageFormat("jpg");
            $im->setCompression(Imagick::COMPRESSION_JPEG);
            $im->setCompressionQuality(70);
            $im->writeImage($CFG->external_data_root . "/../../cert/$hash.jpg");
        }
        catch (Exception $err)
        {
            error_log("Imagick PHP module not installed - cannot create certificate screenshot.");
        }

        $certImgUrl = $CFG->landing_page . "/cert/$hash.jpg";
        $certPageUrl = $CFG->landing_page . "/cert/$hash.html";
        $description = str_replace("{%coursename%}", $courseData->fullname, get_string("email_notif_student_cert_share", "theme_monorail"));

        $mailData = array(
            "fb_url" => "https://www.facebook.com/dialog/feed?app_id=" . $CFG->FBAppID . "&display=page" .
                "&name=" .  urlencode($courseData->fullname) .
                "&description=" .  urlencode($description) .
                "&link=" .  urlencode($certPageUrl) .
                "&picture=" .  urlencode($certImgUrl) .
                "&redirect_uri=" . urlencode("https://facebook.com"),

            "tw_url" => "https://twitter.com/intent/tweet?url=" . urlencode($certPageUrl) . "&text=" . urlencode($courseData->fullname),
            "li_url" => "http://www.linkedin.com/shareArticle?mini=true" .
                "&title=" . urlencode($courseData->fullname) .
                "&url=" . urlencode($certPageUrl) .
                "&summary=" . urlencode($description),
            "certimgurl" => $certImgUrl,
            "certurl" => $certPageUrl,
            "username" => $userData->firstname . " " . $userData->lastname,
            "coursename" => $courseData->fullname,
            "courselink" => $CFG->magic_ui_root . "/courses/" . $courseCode
        );

        monorailfeed_create_notification($courseid, 'certificate', 'cert', null, $USER->id, $userid);
        monorail_send_info_mail('mail/certificatenotification',
            str_replace("{%coursename%}", $courseData->fullname, get_string('email_notif_student_cert_subj', 'theme_monorail')),
            $mailData, $userid);

        return $hash;
    }

    private static function destroy_certificate($hashurl)
    {
        global $CFG;

        @unlink($CFG->external_data_root . "/../../cert/$hashurl.pdf");
        @unlink($CFG->external_data_root . "/../../cert/$hashurl.jpg");
    }

    public static function get_cohort_stats_parameters()
    {
        return new external_function_parameters(array());
    }

    public static function get_cohort_stats()
    {
        global $DB, $USER, $CFG;

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);
        $courseData = array();
        $userData = array();

        if ($cohort)
        {
            // Users of cohort
            $cohortMembers = array();
            $members = $DB->get_records_sql("SELECT cm.userid AS id, CONCAT(u.firstname, ' ', u.lastname) AS fullname FROM {cohort_members} AS cm" .
                " INNER JOIN {user} AS u ON cm.userid=u.id WHERE cm.cohortid=?", array($cohort));

            foreach ($members as $member)
            {
                $cohortMembers[] = $member->id;

                $userData[] = array("userid" => $member->id, "fullname" => $member->fullname);
            }

            // Courses of cohort
            $cohortCourses = array();
            $courses = $DB->get_records_sql("SELECT DISTINCT courseid AS id FROM {monorail_cohort_courseadmins} WHERE cohortid=?", array($cohort));

            foreach ($courses as $course)
            {
                $cohortCourses[] = $course->id;
            }

            // Collect user task grades.
            if (!empty($cohortCourses) && !empty($cohortMembers))
            {
                foreach ($cohortCourses as $courseId)
                {
                    $courseInfo = $DB->get_records_sql("SELECT id, status FROM {monorail_course_data} WHERE courseid=?", array($courseId));
                    $courseInfo = reset($courseInfo);

                    $courseInfo2 = $DB->get_records_sql("SELECT id, fullname FROM {course} WHERE id=?", array($courseId));
                    $courseInfo2 = reset($courseInfo2);

                    $crs = array(
                        "courseid" => $courseId,
                        "fullname" => $courseInfo2->fullname,
                        "completed" => $courseInfo->status ? 0 : 1,
                        "tasks" => array(),
                        "sections" => array()
                    );

                    $courseUsers = array();
                    $users = $DB->get_records_sql("SELECT u.id AS id FROM {role_assignments} AS ra" .
                        " INNER JOIN {user} AS u ON u.id = ra.userid" .
                        " INNER JOIN {role} AS r ON r.id = ra.roleid" .
                        " INNER JOIN {context} AS cxt ON cxt.id = ra.contextid" .
                        " INNER JOIN {course} AS c ON c.id = cxt.instanceid" .
                            " WHERE c.id=? AND r.shortname='student' AND u.id IN (" . implode(", ", $cohortMembers) . ")", array($courseId));

                    foreach ($users as $usr)
                    {
                        $courseUsers[] = $usr->id;
                    }

                    $assigns = $DB->get_records_sql("SELECT id, grade, name FROM {assign} WHERE course=? AND NOT nosubmissions", array($courseId));

                    foreach ($assigns as $assign)
                    {
                        $task = array("taskid" => $assign->id, "tasktype" => "assign", "users" => array(), "name" => $assign->name);

                        foreach ($courseUsers as $usrId)
                        {
                            $grPercent = -1;
                            $aSubmitted = 1;

                            $grades = $DB->get_recordset_sql("SELECT grade FROM {assign_grades} WHERE assignment=? AND userid=?",
                                        array($assign->id, $usrId));

                            foreach ($grades as $grade)
                            {
                                $grPercent = ($grade->grade / ($assign->grade ? $assign->grade : 1)) * 100;
                                break;
                            }

                            if ($grPercent == -1)
                            {
                                if (!$DB->record_exists("assign_submission", array("assignment" => $assign->id, "userid" => $usrId, "status" => "submitted")))
                                {
                                    $aSubmitted = 0;
                                }
                            }

                            $task["users"][] = array("userid" => $usrId, "gradepercent" => $grPercent, "submitted" => $aSubmitted);
                        }

                        $crs["tasks"][] = $task;
                    }

                    $quizes = $DB->get_records_sql("SELECT id, name FROM {quiz} WHERE course=?", array($courseId));

                    foreach ($quizes as $quiz)
                    {
                        $task = array("taskid" => $quiz->id, "tasktype" => "quiz", "users" => array(), "name" => $quiz->name);

                        foreach ($courseUsers as $usrId)
                        {
                            $grPercent = -1;

                            $attempts = $DB->get_records_sql("SELECT id, value FROM {monorail_data} WHERE (type='quiz_attempt' OR type='quiz_attempt_best')" .
                                " AND datakey=? AND itemid=? ORDER BY type DESC LIMIT 1", array("user" . $usrId, $quiz->id));

                            if (!empty($attempts))
                            {
                                $attempts = reset($attempts);
                                $attemptData = unserialize($attempts->value);

                                $quizTotal = 0;
                                $quizCorrect = 0;

                                foreach ($attemptData as $question)
                                {
                                    $allOk = true;

                                    if (isset($question["correct"])) {
                                        $allOk = $question["correct"];
                                    } else {
                                        foreach ($question["answers"] as $answer)
                                        {
                                            if ($answer["correct"] != $answer["checked"])
                                            {
                                                $allOk = false;
                                            }
                                        }
                                    }

                                    $quizTotal++;

                                    if ($allOk)
                                    {
                                        $quizCorrect++;
                                    }
                                }

                                $grade = $quizCorrect . '/' . $quizTotal;

                                if ($quizTotal)
                                {
                                    $grPercent = round(($quizCorrect / $quizTotal) * 100);
                                }
                            }

                            $task["users"][] = array("userid" => $usrId, "gradepercent" => $grPercent, "submitted" => $grPercent == -1 ? 0 : 1);
                        }

                        $crs["tasks"][] = $task;
                    }

                    // Collect total course section visits...
                    $visits = array();
                    $userVisits = array();

                    $sectionVisits = $DB->get_recordset_sql("SELECT COALESCE(section, 0) AS section, userid, COUNT(*) AS visits FROM {monorail_user_activity_log}" .
                        " WHERE courseid=? AND action='courseview' GROUP BY section, userid", array($courseId));

                    foreach ($sectionVisits as $sv)
                    {
                        if (!isset($visits[$sv->section]))
                        {
                            $visits[$sv->section] = $sv->visits;
                            $userVisits[$sv->section] = array(array("userid" => $sv->userid, "visits" => $sv->visits));
                        }
                        else
                        {
                            $visits[$sv->section] += $sv->visits;
                            $userVisits[$sv->section][] = array("userid" => $sv->userid, "visits" => $sv->visits);
                        }
                    }

                    // Make section entries...
                    $sections = $DB->get_records_sql("SELECT id, name, section FROM {course_sections} WHERE course=? ORDER BY section ASC", array($courseId));

                    foreach ($sections as $sect)
                    {
                        $crs["sections"][] = array(
                            "index" => $sect->section,
                            "name" => $sect->name,
                            "visits" => isset($visits[$sect->section]) ? $visits[$sect->section] : 0,
                            "users" => isset($userVisits[$sect->section]) ? $userVisits[$sect->section] : array());
                    }

                    $courseData[] = $crs;
                }
            }
        }

        return array("courses" => $courseData, "users" => $userData);
    }

    public static function get_cohort_stats_returns()
    {
        return new external_single_structure(array(
            "courses" => new external_multiple_structure(new external_single_structure(array(
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "fullname" => new external_value(PARAM_TEXT, "Course name"),
                "completed" => new external_value(PARAM_INT, "Is course completed"),
                "tasks" => new external_multiple_structure(new external_single_structure(array(
                    "taskid" => new external_value(PARAM_INT, "Task id"),
                    "tasktype" => new external_value(PARAM_TEXT, "Type of task (assign/quiz/...)"),
                    "name" => new external_value(PARAM_TEXT, "Task name"),
                    "users" => new external_multiple_structure(new external_single_structure(array(
                        "userid" => new external_value(PARAM_INT, "User id"),
                        "gradepercent" => new external_value(PARAM_INT, "Grade percent (-1 if not graded)"),
                        "submitted" => new external_value(PARAM_INT, "1 for submitted, 0 for not submitted")
                    )))
                ))),
                "sections" => new external_multiple_structure(new external_single_structure(array(
                    "index" => new external_value(PARAM_INT, "Section index"),
                    "name" => new external_value(PARAM_TEXT, "Section name"),
                    "visits" => new external_value(PARAM_INT, "Total number of views"),
                    "users" => new external_multiple_structure(new external_single_structure(array(
                        "userid" => new external_value(PARAM_INT, "User id"),
                        "visits" => new external_value(PARAM_INT, "Views per user")
                    )))
                )))
            ))),
            "users" => new external_multiple_structure(new external_single_structure(array(
                "userid" => new external_value(PARAM_INT, "User id"),
                "fullname" => new external_value(PARAM_TEXT, "Course name")
            )))
        ));
    }

    public static function get_cohort_activity_parameters()
    {
        return new external_function_parameters(array(
            'period_start'  => new external_value(PARAM_INT, 'Period start'),
            'period_end'  => new external_value(PARAM_INT, 'Period end')
        ));
    }

    public static function get_cohort_activity($start, $end)
    {
        global $DB, $USER, $CFG;

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);
        $courseData = array();

        if ($cohort)
        {
            // Courses of cohort
            $courses = $DB->get_records_sql("SELECT DISTINCT courseid AS id FROM {monorail_cohort_courseadmins} WHERE cohortid=?", array($cohort));

            foreach ($courses as $course)
            {
                // Collect section visits...
                $visits = array();

                $sectionVisits = $DB->get_recordset_sql("SELECT COUNT(*) AS visits, COALESCE(section, 0) AS section, FLOOR(timestamp / 86400) AS day" .
                    " FROM {monorail_user_activity_log} WHERE courseid=? AND action='courseview' AND timestamp>=? AND timestamp<=? GROUP BY day, section",
                        array($course->id, $start, $end));

                foreach ($sectionVisits as $vis)
                {
                    $visits[] = array(
                        "section" => $vis->section,
                        "date_from" => $vis->day * 86400,
                        "date_to" => ($vis->day + 1) * 86400,
                        "count" => $vis->visits
                    );
                }

                if (!empty($visits))
                {
                    $courseData[] = array("courseid" => $course->id, "visits" => $visits);
                }
            }
        }

        return array("courses" => $courseData);
    }

    public static function get_cohort_activity_returns()
    {
        return new external_single_structure(array(
            "courses" => new external_multiple_structure(new external_single_structure(array(
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "visits" => new external_multiple_structure(new external_single_structure(array(
                    "section" => new external_value(PARAM_INT, "Section number"),
                    "date_from" => new external_value(PARAM_INT, "Start of time interval"),
                    "date_to" => new external_value(PARAM_INT, "End of time interval"),
                    "count" => new external_value(PARAM_INT, "Number of views")
                )))
            )))
        ));
    }

    public static function get_cohort_enrollments_parameters()
    {
        return new external_function_parameters(array(
            'period_start'  => new external_value(PARAM_INT, 'Period start'),
            'period_end'  => new external_value(PARAM_INT, 'Period end')
        ));
    }

    public static function get_cohort_enrollments($start, $end)
    {
        global $DB, $USER, $CFG;

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);
        $courseData = array();

        if ($cohort)
        {
            // Courses of cohort
            $courses = $DB->get_records_sql("SELECT DISTINCT courseid AS id FROM {monorail_cohort_courseadmins} WHERE cohortid=?", array($cohort));

            foreach ($courses as $course)
            {
                // Collect enrollments...
                $enrollments = array();

                $courseEnrollments = $DB->get_recordset_sql("SELECT COUNT(*) AS enrolls, FLOOR(ue.timestart / 86400) AS day FROM {enrol} AS e" .
                    " INNER JOIN {user_enrolments} AS ue ON ue.enrolid=e.id WHERE e.courseid=? GROUP BY day", array($course->id));

                foreach ($courseEnrollments as $enr)
                {
                    $enrollments[] = array(
                        "date_from" => $enr->day * 86400,
                        "date_to" => ($enr->day + 1) * 86400,
                        "count" => $enr->enrolls
                    );
                }

                if (!empty($enrollments))
                {
                    $courseData[] = array("courseid" => $course->id, "enrollments" => $enrollments);
                }
            }
        }

        return array("courses" => $courseData);
    }

    public static function get_cohort_enrollments_returns()
    {
        return new external_single_structure(array(
            "courses" => new external_multiple_structure(new external_single_structure(array(
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "enrollments" => new external_multiple_structure(new external_single_structure(array(
                    "date_from" => new external_value(PARAM_INT, "Start of time interval"),
                    "date_to" => new external_value(PARAM_INT, "End of time interval"),
                    "count" => new external_value(PARAM_INT, "Number of views")
                )))
            )))
        ));
    }

public static function add_vfilerscs_parameters() {
 return new external_function_parameters(
   array('vfilerscs' => new external_multiple_structure (
     new external_single_structure(
       array(
         'course' => new external_value(PARAM_INT, 'course id', VALUE_OPTIONAL),
         'options' => new external_value(PARAM_TEXT, 'extra information about attachment', VALUE_OPTIONAL),
         'taskid' => new external_value(PARAM_INT, 'Task id', VALUE_DEFAULT, 0),
         'sectionid' => new external_value(PARAM_INT, 'section id', VALUE_DEFAULT,0),
         'taskid' => new external_value(PARAM_INT, 'taskid id', VALUE_OPTIONAL),
         'height' => new external_value(PARAM_INT, 'height', VALUE_DEFAULT, 360),
         'width' => new external_value(PARAM_INT, 'Width', VALUE_DEFAULT, 480),
         'filename' => new external_value(PARAM_NOTAGS, 'filename'),
         'filesize' => new external_value(PARAM_INT, 'filesize in MB'),
         'contextualdata' => new external_value(PARAM_RAW, 'contextual data', VALUE_DEFAULT, '{"links":null,"visible":false}'),
         'vresid' => new external_value(PARAM_TEXT, 'Resource id'),
       )), 'list of video file resource information')
   )
 );
}

/**
 * add_vfilerscs
 * @param array of $vfilerscs
 * @return an array of warnings if any
 * @since  Moodle 2.4
 */
public static function add_vfilerscs($vfilerscs) {
 global $CFG, $USER, $DB;

 $params = self::validate_parameters(
   self::add_vfilerscs_parameters(),
   array(
     'vfilerscs'=>$vfilerscs,
   )
 );
 require_once($CFG->dirroot . '/mod/kalvidres/lib.php');
 require_once($CFG->dirroot . '/course/lib.php');

 $warnings = array();
 $vfilerscinfo = array();

 // Available space currently
 $availablespace = 1024; //default limit 
 $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'videolimit'), IGNORE_MULTIPLE);
 if($fieldid) {
    $vlimit = $DB->get_field('user_info_data', 'data', array('userid'=>$USER->id, 'fieldid' => $fieldid), IGNORE_MULTIPLE);
    if($vlimit) {
      $availablespace = $vlimit * 1024; //MB
    } 
 }
 $vLimit = $availablespace;
 $usedSpace = 0;
 $vrecords = $DB->get_records("monorail_course_videorsc", array("userid"=>$USER->id)); 
 foreach($vrecords as $vrecord) {
    $usedSpace = $usedSpace + $vrecord->filesize; 
 }
 $usedSpace = round($usedSpace/(1024*1024)); //MB
 $availablespace = $availablespace - $usedSpace;
 foreach ($vfilerscs as $vfilersc) {
  $courseid = 0;
  $entryid = null;
  $kvresinf = array();
  if($vfilersc['taskid'] == 0) {
   $context = context_course::instance($vfilersc['course']);
   try {
    self::validate_context($context);
    require_capability('moodle/course:manageactivities', $context);
   } catch (Exception $e) {
    $warning = array();
    $warning['item'] = 'course';
    $warning['itemid'] = $vfilersc['course'];
    $warning['warningcode'] = '1';
    $warning['message'] = 'No access rights in course context';
    $warnings[] = $warning;
    continue;
   }
   if($availablespace <= 0) {
    $warning = array();
    $warning['item'] = 'course';
    $warning['itemid'] = $vfilersc['course'];
    $warning['warningcode'] = '1';
    $warning['message'] = 'No space left, user';
    $warnings[] = $warning;
    continue;
   }
   //Get the course section info
   $section = $DB->get_record("course_sections", array("id"=>$vfilersc['sectionid']));
   if(!$section) {
    //User cannot create assignment for these courses
    $warning = array();
    $warning['item'] = 'course';
    $warning['itemid'] = $vfilersc['course'];
    $warning['warningcode'] = '1';
    $warning['message'] = 'Unable to find course section to add resource';
    $warnings[] = $warning;
    continue;//continue with next module error getting section
   }

   $vresult = self::add_video_resource($vfilersc['vresid'], $vfilersc['filename']);
   if ($vresult) {
      $vfilersc['vresid'] = $vresult->id;
      $kvresinf['thumbnail'] = $vresult->thumbnailUrl;
   } else {
      $warning = array();
      $warning['item'] = 'course';
      $warning['itemid'] = $vfilersc['course'];
      $warning['warningcode'] = '1';
      $warning['message'] = 'Unable to add media!';
      $warnings[] = $warning;
      continue;
   }

   // Add new module
   $moduleid = $DB->get_field('modules', 'id', array('name'=>'kalvidres'));
   // create a new course module for url
   $newcm = new stdClass();
   $newcm->course   = $vfilersc['course'];
   $newcm->module   = $moduleid;
   $newcm->instance = 0;
   $newcm->visible  = 1;
   $newcm->section  = $section->id;;
   $newcm->coursemodule  = 0;
   $newcm->added    = time();
   $newcmid = $DB->insert_record('course_modules', $newcm);

   $data = new stdClass();
   $data->course         = $vfilersc['course'];
   $data->entry_id       = $vfilersc['vresid'];
   $data->height         = $vfilersc['height'];
   $data->width          = $vfilersc['width'];
   $data->uiconf_id      = 11601128;//$CFG->kalPlayerId;
   $data->video_title    = $vfilersc['filename'];;
   $data->name         = $vfilersc['filename'];;
   $data->intro        = $vfilersc['filename'];;

   $kvresinf['id'] = kalvidres_add_instance($data);
   $DB->set_field('course_modules', 'instance', $kvresinf['id'], array('id'=>$newcmid));


   $kvresinf['mid'] = $newcmid;
   $kvresinf['modname'] = 'kalvidres';
   $kvresinf['vresid'] = $vfilersc['vresid'];

   //section info update
   $newcm->coursemodule = $newcmid;
   $newcm->section = $section->section;
   add_mod_to_section($newcm);

   $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcmid));
   set_coursemodule_visible($newcm->coursemodule, 1);
   rebuild_course_cache($data->course);
   $courseid = $data->course;
   $entryid = $data->entry_id;
   //send add events
   $eventdata = new stdClass();
   $eventdata->modulename = "kalvidres";
   $eventdata->name       = 'mod_created';
   $eventdata->cmid       = $newcm->coursemodule;
   $eventdata->courseid   = $data->course;
   $eventdata->userid     = $USER->id;
   events_trigger("mod_created", $eventdata);

  } else {
   $instanceInfo = $DB->get_record_sql("SELECT cm.instance AS id, m.name AS module".
     " FROM {course_modules} AS cm, {modules} AS m WHERE m.id=cm.module AND cm.id=?",
     array($vfilersc["taskid"]), MUST_EXIST);

   //Check if user has permissions
   $assignment = $DB->get_record($instanceInfo->module, array('id' => $instanceInfo->id), "*", MUST_EXIST);
   $mid = $DB->get_field('modules', 'id', array('name'=>$instanceInfo->module));
   $cm = $DB->get_record('course_modules', array('course' => $assignment->course, 'instance' => $assignment->id, 'module'=>$mid), "*", MUST_EXIST);
   $context = context_module::instance($cm->id);
   try {
    self::validate_context($context);
    require_capability('moodle/course:manageactivities', $context);
   } catch (Exception $e) {
    $warning = array();
    $warning['item'] = 'assignment';
    $warning['itemid'] = $assignment->id;
    $warning['warningcode'] = '1';
    $warning['message'] = 'No access rights in course context to add file resource for assignment';
    $warnings[] = $warning;
    continue;
   }
   if($availablespace <= 0) {
    $warning = array();
    $warning['item'] = 'course';
    $warning['itemid'] = $vfilersc['course'];
    $warning['warningcode'] = '1';
    $warning['message'] = 'No space left, user';
    $warnings[] = $warning;
    continue;
   }
   $vresult = self::add_video_resource($vfilersc['vresid'], $vfilersc['filename']);
   if ($vresult) {
      $vfilersc['vresid'] = $vresult->id;
      $kvresinf['thumbnail'] = $vresult->thumbnailUrl;
   } else {
      $warning = array();
      $warning['item'] = 'course';
      $warning['itemid'] = $vfilersc['course'];
      $warning['warningcode'] = '1';
      $warning['message'] = 'Unable to add media!';
      $warnings[] = $warning;
      continue;
   }
   $data = new stdClass();
   $data->courseid         = $assignment->course;
   $data->entry_id       = $vfilersc['vresid'];
   $data->taskid         = $cm->id;
   $data->options        = $vfilersc['options'];
   $data->contextualdata = $vfilersc['contextualdata'];
   $kvresinf['id'] = $DB->insert_record('monorail_assign_kalvidres', $data);
   $kvresinf['vresid'] = $data->entry_id;
   $courseid = $data->courseid;
   $entryid = $data->entry_id;
  }
  //Update the filesize in db
  $vfdata = new stdClass();
  $vfdata->courseid = $courseid;
  $vfdata->userid = $USER->id;
  $vfdata->entryid  = $entryid;
  $vfdata->filesize  = $vfilersc['filesize'];
  $DB->insert_record('monorail_course_videorsc', $vfdata);
  $availablespace = $availablespace - round($vfilersc['filesize']/(1024*1024));
  $vfilerscinfo[] = $kvresinf;
 }
 $result = array();
 $result['vfilersc'] = $vfilerscinfo;
 if(($vLimit - $availablespace) <= 0) {
   $result['vspace'] = 0;
 } else {
   $result['vspace'] = round($vLimit - $availablespace);
 }
 $result['warnings'] = $warnings;
 return $result;
}


/**
 * Describes the return value for add_vfilerscs
 * @return external_single_structure
 */
public static function add_vfilerscs_returns() {
 return new external_single_structure(
   array(
     'vfilersc' => new external_multiple_structure (
       new external_single_structure(
         array(
           'id' => new external_value(PARAM_INT, 'ID of the resource'),
           'mid' => new external_value(PARAM_INT, 'Module id of the resource', VALUE_OPTIONAL),
           'vresid' => new external_value(PARAM_TEXT, 'Resource Id'),
           'thumbnail' => new external_value(PARAM_URL, 'Thumbnail for the resource'),
           'modname' => new external_value(PARAM_TEXT, 'Module name of the resource', VALUE_OPTIONAL),
         )),'Video file resource info',VALUE_OPTIONAL),
     'vspace' => new external_value(PARAM_INT, 'space available'),
     'warnings'  => new external_warnings()
   )
 );
}

/**
 * Describes the params for del_vfilerscs
 * @return external_single_structure
 * @since  Moodle 2.4
 */

public static function del_vfilerscs_parameters() {
 return new external_function_parameters(
   array('vfilerscs' => new external_multiple_structure (
     new external_single_structure(
       array(
         'vresid' => new external_value(PARAM_TEXT, 'Kal video resource id', VALUE_DEFAULT, 0),
         'moduleid' => new external_value(PARAM_INT, 'moduleid id', VALUE_DEFAULT, 0),
         'type' => new external_value(PARAM_TEXT, 'Video resource type'),
       )), 'list of video file resource information')
   )
 );
}

/**
 * remove vfilerscs
 * @param array of $vfilerscs info
 *
 * @return an array of warnings if any
 * @since  Moodle 2.4
 */
public static function del_vfilerscs($vfilerscs) {
 global $CFG, $DB, $USER;

 $params = self::validate_parameters(self::del_vfilerscs_parameters(),
   array('vfilerscs' => $vfilerscs));

 $warnings = array();
 $result = array();

 foreach ($vfilerscs as $vfilersc) {
  if($vfilersc['type'] == 'course_modules') {
   $cm = $DB->get_record('course_modules', array('id' => $vfilersc['moduleid']), "*", MUST_EXIST);
   $resid = $cm->instance;
   $resinfo = $DB->get_record('kalvidres', array('id' => $resid), "*", MUST_EXIST);
   $vfilersc['vresid'] = $resinfo->entry_id;
   $context = context_course::instance($resinfo->course);
   try {
    self::validate_context($context);
    require_capability('moodle/course:manageactivities', $context);
   } catch (Exception $e) {
    $warning = array();
    $warning['item'] = 'course';
    $warning['itemid'] = $resinfo->course;
    $warning['warningcode'] = '1';
    $warning['message'] = 'No access rights in course context to remove url';
    $warnings[] = $warning;
    continue;
   }
   require_once($CFG->dirroot . '/mod/kalvidres/lib.php');
   //Ok remove the course module and update cache
   $cm = get_coursemodule_from_instance('kalvidres', $resid, 0, false, MUST_EXIST);
   $DB->delete_records('course_modules', array('id'=>$cm->id));
   kalvidres_delete_instance($resid);
   rebuild_course_cache($cm->course);

   //send delete events
   $eventdata = new stdClass();
   $eventdata->modulename = "url";
   $eventdata->name       = 'mod_deleted';
   $eventdata->cmid       = $cm->id;
   $eventdata->courseid   = $cm->course;
   $eventdata->userid     = $USER->id;
   events_trigger("mod_deleted", $eventdata);
  } else if ($vfilersc['type'] == 'task_vfiles') {
   $arec = $DB->get_record('monorail_assign_kalvidres', array('entry_id' => $vfilersc['vresid']));
   if($arec) {
    $DB->delete_records('monorail_assign_kalvidres', array('id'=>$arec->id));
   }
  }
  $rec = $DB->get_record('monorail_course_videorsc', array('entryid' => $vfilersc['vresid']));
  if($rec) {
     //delete entry from kaltura here, if fail keep a record
     self::delete_video_resource($rec->entryid); 
  }
 }

 // Available space currently
 $availablespace = 1024; //default limit 
 $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'videolimit'), IGNORE_MULTIPLE);
 if($fieldid) {
    $vlimit = $DB->get_field('user_info_data', 'data', array('userid'=>$USER->id, 'fieldid' => $fieldid), IGNORE_MULTIPLE);
    if($vlimit) {
      $availablespace = $vlimit * 1024; //MB
    } 
 }
 $usedSpace = 0;
 $vrecords = $DB->get_records("monorail_course_videorsc", array("userid"=>$USER->id)); 
 foreach($vrecords as $vrecord) {
    $usedSpace = $usedSpace + $vrecord->filesize; 
 }
 $usedSpace = round($usedSpace/(1024*1024)); //MB
 $availablespace = $availablespace - $usedSpace;
 if($usedSpace <= 0) {
   $result['vspace'] = 0;
 } else {
   $result['vspace'] = $usedSpace;
 }
 $result['warnings'] = $warnings;
 return $result;

}

/**
 * Describes the return value for del_vfilerscs
 * @return external_single_structure
 * @since  Moodle 2.4
 */
public static function del_vfilerscs_returns() {
 return new external_single_structure(
   array(
     'vspace' => new external_value(PARAM_INT, 'space used'),
     'warnings'  => new external_warnings()
   )
 );
}

/**
 * Returns description of method parameters
 *
 * @return external_function_parameters
 * @since Moodle 2.2
*/
public static function send_support_mail_parameters() {
 return new external_function_parameters(
   array('mailinfos' => new external_multiple_structure (
     new external_single_structure(
       array(
         'subject' => new external_value(PARAM_TEXT, 'Subject of the mail'),
         'type' => new external_value(PARAM_TEXT, 'Type of message'),
       )), 'list of video file resource information')
   )
 );
}

/**
 * Send support mail 
 */
public static function send_support_mail($mailinfos) {
    global $DB, $USER, $SESSION, $CFG;
 $params = self::validate_parameters(self::send_support_mail_parameters(),
   array('mailinfos' => $mailinfos));

 $warnings = array();
 $result = array();
 foreach ($mailinfos as $mailinfo) {
     mail("support@eliademy.com", $mailinfo['subject'],
            "Customer mail to support. Source ->". $CFG->wwwroot ." \n" .
            "Name: " . $USER->firstname . " " . $USER->lastname . "\n" .
            "Username: " . $USER->username . " (" . $USER->email . " User id: " . $USER->id . ")\n",
            "From: $USER->email\r\n" .
            "Reply-To: $USER->email\r\n"); 
 }

 $result['warnings'] = $warnings;
 return $result;
}

/* @return 
 *
 */
public static function send_support_mail_returns() {
  return new external_single_structure(
    array(
     'warnings'  => new external_warnings()
    ));
}

    public static function get_course_stats_parameters()
    {
        return new external_function_parameters(array());
    }

    public static function get_course_stats()
    {
        global $DB, $USER, $CFG;

        $courseData = array();
        $userData = array();

        $ownedCourses = $DB->get_records_sql("SELECT c.id AS id, c.fullname AS fullname" .
            " FROM {role_assignments} AS ra" .
            " INNER JOIN {user} AS u ON u.id = ra.userid" .
            " INNER JOIN {role} AS r ON r.id = ra.roleid" .
            " INNER JOIN {context} AS cxt ON cxt.id = ra.contextid" .
            " INNER JOIN {course} AS c ON c.id = cxt.instanceid" .
                " WHERE u.id=? AND r.shortname IN ('editingteacher', 'manager')", array($USER->id));

        foreach ($ownedCourses as $course)
        {
            $courseInfo = $DB->get_records_sql("SELECT id, status FROM {monorail_course_data} WHERE courseid=?", array($course->id));
            $courseInfo = reset($courseInfo);

            $crs = array(
                "courseid" => $course->id,
                "fullname" => $course->fullname,
                "completed" => $courseInfo->status ? 0 : 1,
                "tasks" => array(),
                "sections" => array()
            );

            $courseUsers = array();

            $users = $DB->get_records_sql("SELECT u.id AS id, CONCAT(u.firstname, ' ', u.lastname) AS fullname" .
                " FROM {role_assignments} AS ra" .
                " INNER JOIN {user} AS u ON u.id = ra.userid" .
                " INNER JOIN {role} AS r ON r.id = ra.roleid" .
                " INNER JOIN {context} AS cxt ON cxt.id = ra.contextid" .
                " INNER JOIN {course} AS c ON c.id = cxt.instanceid" .
                    " WHERE c.id=? AND r.shortname='student'", array($course->id));

            foreach ($users as $usr)
            {
                $courseUsers[] = $usr->id;

                if (!array_key_exists($usr->id, $userData))
                {
                    $userData[$usr->id] = array("userid" => $usr->id, "fullname" => $usr->fullname);
                }
            }

            $assigns = $DB->get_records_sql("SELECT id, grade, name FROM {assign} WHERE course=? AND NOT nosubmissions", array($course->id));

            foreach ($assigns as $assign)
            {
                $task = array("taskid" => $assign->id, "tasktype" => "assign", "users" => array(), "name" => $assign->name);

                foreach ($courseUsers as $usrId)
                {
                    $grPercent = -1;
                    $aSubmitted = 1;

                    $grades = $DB->get_recordset_sql("SELECT grade FROM {assign_grades} WHERE assignment=? AND userid=?",
                                array($assign->id, $usrId));

                    foreach ($grades as $grade)
                    {
                        $grPercent = ($grade->grade / ($assign->grade ? $assign->grade : 1)) * 100;
                        break;
                    }

                    if ($grPercent == -1)
                    {
                        if (!$DB->record_exists("assign_submission", array("assignment" => $assign->id, "userid" => $usrId, "status" => "submitted")))
                        {
                            $aSubmitted = 0;
                        }
                    }

                    $task["users"][] = array("userid" => $usrId, "gradepercent" => $grPercent, "submitted" => $aSubmitted);
                }

                $crs["tasks"][] = $task;
            }

            $quizes = $DB->get_records_sql("SELECT id, name FROM {quiz} WHERE course=?", array($course->id));

            foreach ($quizes as $quiz)
            {
                $task = array("taskid" => $quiz->id, "tasktype" => "quiz", "users" => array(), "name" => $quiz->name);

                foreach ($courseUsers as $usrId)
                {
                    $grPercent = -1;

                    $attempts = $DB->get_records_sql("SELECT id, value FROM {monorail_data} WHERE (type='quiz_attempt' OR type='quiz_attempt_best')" .
                        " AND datakey=? AND itemid=? ORDER BY type DESC LIMIT 1", array("user" . $usrId, $quiz->id));

                    if (!empty($attempts))
                    {
                        $attempts = reset($attempts);
                        $attemptData = unserialize($attempts->value);

                        $quizTotal = 0;
                        $quizCorrect = 0;

                        foreach ($attemptData as $question)
                        {
                            $allOk = true;

                            if (isset($question["correct"])) {
                                $allOk = $question["correct"];
                            } else {
                                foreach ($question["answers"] as $answer)
                                {
                                    if ($answer["correct"] != $answer["checked"])
                                    {
                                        $allOk = false;
                                    }
                                }
                            }

                            $quizTotal++;

                            if ($allOk)
                            {
                                $quizCorrect++;
                            }
                        }

                        $grade = $quizCorrect . '/' . $quizTotal;

                        if ($quizTotal)
                        {
                            $grPercent = round(($quizCorrect / $quizTotal) * 100);
                        }
                    }

                    $task["users"][] = array("userid" => $usrId, "gradepercent" => $grPercent, "submitted" => $grPercent == -1 ? 0 : 1);
                }

                $crs["tasks"][] = $task;
            }

            // Collect total course section visits...
            $visits = array();
            $userVisits = array();

            $sectionVisits = $DB->get_recordset_sql("SELECT COALESCE(section, 0) AS section, userid, COUNT(*) AS visits FROM {monorail_user_activity_log}" .
                " WHERE courseid=? AND action='courseview' GROUP BY section, userid", array($course->id));

            foreach ($sectionVisits as $sv)
            {
                if (!isset($visits[$sv->section]))
                {
                    $visits[$sv->section] = $sv->visits;
                    $userVisits[$sv->section] = array(array("userid" => $sv->userid, "visits" => $sv->visits));
                }
                else
                {
                    $visits[$sv->section] += $sv->visits;
                    $userVisits[$sv->section][] = array("userid" => $sv->userid, "visits" => $sv->visits);
                }
            }

            // Make section entries...
            $sections = $DB->get_records_sql("SELECT id, name, section FROM {course_sections} WHERE course=? ORDER BY section ASC", array($course->id));

            foreach ($sections as $sect)
            {
                $crs["sections"][] = array(
                    "index" => $sect->section,
                    "name" => $sect->name,
                    "visits" => isset($visits[$sect->section]) ? $visits[$sect->section] : 0,
                    "users" => isset($userVisits[$sect->section]) ? $userVisits[$sect->section] : array());
            }

            $courseData[] = $crs;
        }

        return array("courses" => $courseData, "users" => array_values($userData));
    }

    public static function get_course_stats_returns()
    {
        return new external_single_structure(array(
            "courses" => new external_multiple_structure(new external_single_structure(array(
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "fullname" => new external_value(PARAM_TEXT, "Course name"),
                "completed" => new external_value(PARAM_INT, "Is course completed"),
                "tasks" => new external_multiple_structure(new external_single_structure(array(
                    "taskid" => new external_value(PARAM_INT, "Task id"),
                    "tasktype" => new external_value(PARAM_TEXT, "Type of task (assign/quiz/...)"),
                    "name" => new external_value(PARAM_TEXT, "Task name"),
                    "users" => new external_multiple_structure(new external_single_structure(array(
                        "userid" => new external_value(PARAM_INT, "User id"),
                        "gradepercent" => new external_value(PARAM_INT, "Grade percent (-1 if not graded)"),
                        "submitted" => new external_value(PARAM_INT, "1 for submitted, 0 for not submitted")
                    )))
                ))),
                "sections" => new external_multiple_structure(new external_single_structure(array(
                    "index" => new external_value(PARAM_INT, "Section index"),
                    "name" => new external_value(PARAM_TEXT, "Section name"),
                    "visits" => new external_value(PARAM_INT, "Total number of views"),
                    "users" => new external_multiple_structure(new external_single_structure(array(
                        "userid" => new external_value(PARAM_INT, "User id"),
                        "visits" => new external_value(PARAM_INT, "Views per user")
                    )))
                )))
            ))),
            "users" => new external_multiple_structure(new external_single_structure(array(
                "userid" => new external_value(PARAM_INT, "User id"),
                "fullname" => new external_value(PARAM_TEXT, "Course name")
            )))
        ));
    }

    public static function get_course_activity_parameters()
    {
        return new external_function_parameters(array(
            'period_start'  => new external_value(PARAM_INT, 'Period start'),
            'period_end'  => new external_value(PARAM_INT, 'Period end')
        ));
    }

    public static function get_course_activity($start, $end)
    {
        global $DB, $USER, $CFG;

        $courseData = array();

        $courses = $DB->get_records_sql("SELECT c.id AS id" .
            " FROM {role_assignments} AS ra" .
            " INNER JOIN {user} AS u ON u.id = ra.userid" .
            " INNER JOIN {role} AS r ON r.id = ra.roleid" .
            " INNER JOIN {context} AS cxt ON cxt.id = ra.contextid" .
            " INNER JOIN {course} AS c ON c.id = cxt.instanceid" .
                " WHERE u.id=? AND r.shortname IN ('editingteacher', 'manager')", array($USER->id));

        foreach ($courses as $course)
        {
            // Collect section visits...
            $visits = array();

            $sectionVisits = $DB->get_recordset_sql("SELECT COUNT(*) AS visits, COALESCE(section, 0) AS section, FLOOR(timestamp / 86400) AS day" .
                " FROM {monorail_user_activity_log} WHERE courseid=? AND action='courseview' AND timestamp>=? AND timestamp<=? GROUP BY day, section",
                    array($course->id, $start, $end));

            foreach ($sectionVisits as $vis)
            {
                $visits[] = array(
                    "section" => $vis->section,
                    "date_from" => $vis->day * 86400,
                    "date_to" => ($vis->day + 1) * 86400,
                    "count" => $vis->visits
                );
            }

            if (!empty($visits))
            {
                $courseData[] = array("courseid" => $course->id, "visits" => $visits);
            }
        }

        return array("courses" => $courseData);
    }

    public static function get_course_activity_returns()
    {
        return new external_single_structure(array(
            "courses" => new external_multiple_structure(new external_single_structure(array(
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "visits" => new external_multiple_structure(new external_single_structure(array(
                    "section" => new external_value(PARAM_INT, "Section number"),
                    "date_from" => new external_value(PARAM_INT, "Start of time interval"),
                    "date_to" => new external_value(PARAM_INT, "End of time interval"),
                    "count" => new external_value(PARAM_INT, "Number of views")
                )))
            )))
        ));
    }

    public static function get_course_enrollments_parameters()
    {
        return new external_function_parameters(array(
            'period_start'  => new external_value(PARAM_INT, 'Period start'),
            'period_end'  => new external_value(PARAM_INT, 'Period end')
        ));
    }

    public static function get_course_enrollments($start, $end)
    {
        global $DB, $USER, $CFG;

        $courseData = array();

        $courses = $DB->get_records_sql("SELECT c.id AS id" .
            " FROM {role_assignments} AS ra" .
            " INNER JOIN {user} AS u ON u.id = ra.userid" .
            " INNER JOIN {role} AS r ON r.id = ra.roleid" .
            " INNER JOIN {context} AS cxt ON cxt.id = ra.contextid" .
            " INNER JOIN {course} AS c ON c.id = cxt.instanceid" .
                " WHERE u.id=? AND r.shortname IN ('editingteacher', 'manager')", array($USER->id));

        foreach ($courses as $course)
        {
            // Collect enrollments...
            $enrollments = array();

            $courseEnrollments = $DB->get_recordset_sql("SELECT COUNT(*) AS enrolls, FLOOR(ue.timestart / 86400) AS day FROM {enrol} AS e" .
                " INNER JOIN {user_enrolments} AS ue ON ue.enrolid=e.id WHERE e.courseid=? GROUP BY day", array($course->id));

            foreach ($courseEnrollments as $enr)
            {
                $enrollments[] = array(
                    "date_from" => $enr->day * 86400,
                    "date_to" => ($enr->day + 1) * 86400,
                    "count" => $enr->enrolls
                );
            }

            if (!empty($enrollments))
            {
                $courseData[] = array("courseid" => $course->id, "enrollments" => $enrollments);
            }
        }

        return array("courses" => $courseData);
    }

    public static function get_course_enrollments_returns()
    {
        return new external_single_structure(array(
            "courses" => new external_multiple_structure(new external_single_structure(array(
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "enrollments" => new external_multiple_structure(new external_single_structure(array(
                    "date_from" => new external_value(PARAM_INT, "Start of time interval"),
                    "date_to" => new external_value(PARAM_INT, "End of time interval"),
                    "count" => new external_value(PARAM_INT, "Number of views")
                )))
            )))
        ));
    }

    public static function get_cohort_userstat_parameters()
    {
        return new external_function_parameters(array(
            'userid'  => new external_value(PARAM_INT, 'User id'),
        ));
    }

    public static function get_cohort_userstat($userid)
    {
        global $DB, $USER, $CFG;

        $data = array("userid" => $userid, "activity" => 0, "usertags" => array(), "courses" => array());

        $cohort = $DB->get_field('monorail_cohort_admin_users', 'cohortid', array('userid'=>$USER->id), IGNORE_MULTIPLE);

        if ($cohort)
        {
            require_once($CFG->libdir . '/gradelib.php');

            $groups = $DB->get_records_sql("SELECT mu.id AS id, mu.name AS name FROM {monorail_usertag} AS mu" .
                " INNER JOIN {monorail_usertag_user} AS muu ON muu.tagid=mu.id" .
                " WHERE muu.userid=? AND mu.cohortid=?", array($userid, $cohort));

            foreach ($groups as $group)
            {
                $data["usertags"][] = $group->name;
            }

            $courses = $DB->get_records_sql("SELECT c.id AS id, c.fullname AS fullname, mcd.code AS code, mcd.status AS status" .
                " FROM {role_assignments} AS ra" .
                " INNER JOIN {role} AS r ON r.id = ra.roleid" .
                " INNER JOIN {context} AS cxt ON cxt.id = ra.contextid" .
                " INNER JOIN {course} AS c ON c.id = cxt.instanceid" .
                " INNER JOIN {monorail_cohort_courseadmins} AS mcc ON mcc.courseid=c.id" .
                " INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=c.id" .
                " WHERE ra.userid=? AND mcc.cohortid=? AND r.shortname='student'", array($userid, $cohort));

            $courseIds = array();

            foreach ($courses as $course)
            {
                $tasks = 0;
                $submitted = 0;
                $gradePercentSum = 0;

                $assigns = $DB->get_records_sql("SELECT id FROM {assign} WHERE course=? AND NOT nosubmissions", array($course->id));

                foreach ($assigns as $assign)
                {
                    $tasks++;

                    $taskSub = false;
                    $grading_info = grade_get_grades($course->id, 'mod', "assign", $assign->id, $userid);

                    foreach ($grading_info->items as $gi)
                    {
                        foreach ($gi->grades as $g)
                        {
                            if ($g->grade !== null)
                            {
                                if ($gi->grademax)
                                {
                                    $gradePercentSum += round(($g->grade / $gi->grademax) * 100);
                                }

                                $taskSub = true;
                                $submitted++;
                            }

                            break;
                        }

                        break;
                    }

                    if (!$taskSub)
                    {
                        if ($DB->record_exists("assign_submission", array("assignment" => $assign->id, "userid" => $userid, "status" => "submitted")))
                        {
                            $submitted++;
                        }
                    }
                }

                $quizes = $DB->get_records_sql("SELECT id FROM {quiz} WHERE course=?", array($course->id));

                foreach ($quizes as $quiz)
                {
                    $tasks++;

	                $attemptData = monorail_data('quiz_attempt_best', $quiz->id, "user" . $userid);

                    if (empty($attemptData))
                    {
	                    $attemptData = monorail_data('quiz_attempt', $quiz->id, "user" . $userid);
                    }

                    if (!empty($attemptData))
                    {
                        $submitted++;

                        $attemptData = reset($attemptData);
                        $attemptData = unserialize($attemptData->value);

                        $quizTotal = 0;
                        $quizCorrect = 0;

                        foreach ($attemptData as $question)
                        {
                            $allOk = true;

                            if (isset($question["correct"])) {
                                $allOk = $question["correct"];
                            } else {
                                foreach ($question["answers"] as $answer)
                                {
                                    if ($answer["correct"] != $answer["checked"])
                                    {
                                        $allOk = false;
                                    }
                                }
                            }

                            $quizTotal++;

                            if ($allOk)
                            {
                                $quizCorrect++;
                            }
                        }

                        if ($quizTotal)
                        {
                            $gradePercentSum += round(($quizCorrect / $quizTotal) * 100);
                        }
                    }
                }

                $data["courses"][] = array(
                    "id" => $course->id,
                    "code" => $course->code,
                    "fullname" => $course->fullname,
                    "completed" => $course->status ? 0 : 1,
                    "lastaccess" => (int) $DB->get_field_sql("SELECT MAX(timestamp) FROM {monorail_user_activity_log}" .
                        " WHERE courseid=? AND userid=? AND action='courseview'", array($course->id, $userid)),
                    "tasks" => $tasks,
                    "submitted" => $submitted,
                    "average_grade" => $tasks ? round($gradePercentSum / $tasks) : 0);

                $courseIds[] = $course->id;
            }

            if (!empty($courseIds))
            {
                $data["activity"] = (int) $DB->get_field_sql("SELECT COUNT(*) FROM {monorail_user_activity_log}" .
                    " WHERE userid=? AND courseid IN (" . implode(", ", $courseIds) . ") AND action='courseview'", array($userid));
            }
            else
            {
                $data["activity"] = 0;
            }
        }

        return $data;
    }

    public static function get_cohort_userstat_returns()
    {
        return new external_single_structure(array(
            "userid" => new external_value(PARAM_INT, "User id"),
            "usertags" => new external_multiple_structure(new external_value(PARAM_TEXT, "Tag name")),
            "activity" => new external_value(PARAM_INT, "Total activity"),
            "courses" => new external_multiple_structure(new external_single_structure(array(
                "id" => new external_value(PARAM_INT, "Course id"),
                "code" => new external_value(PARAM_TEXT, "Course code"),
                "fullname" => new external_value(PARAM_TEXT, "Course name"),
                "completed" => new external_value(PARAM_INT, "Course completed"),
                "lastaccess" => new external_value(PARAM_INT, "Last access"),
                "tasks" => new external_value(PARAM_INT, "Total numer of tasks"),
                "submitted" => new external_value(PARAM_INT, "Numer of submitted tasks"),
                "average_grade" => new external_value(PARAM_INT, "Average grade")
            )))
        ));
    }

    public static function get_social_settings_parameters() {
        return new external_function_parameters(
            array(
              "userid" => new external_value(PARAM_INT, "User id"),
            )
        );
    }

    public static function get_social_settings($userid)
    {
        global $DB, $USER, $CFG;
        $params = self::validate_parameters(self::get_social_settings_parameters(),
                                                array('userid' => $userid));
  
       $socialaccounts = array();
       $accounts = $DB->get_records('monorail_social_keys', array('userid' => $userid)); 
       $myself = ($USER->id == $userid) ? true : false;
       $details = array('accounturl');

       foreach($accounts as $account) {
           $valid = $account->timemodified + $account->expires - time(); 
           if($valid <= 0 && ($account->expires > 0)) { // $account->expires == 0, non expire token
             //Account Expired still posts; delete existing account records
             $DB->delete_records('monorail_social_settings', array('userid' => $userid, 'social' => $account->social));
             $DB->delete_records('monorail_social_keys', array('userid' => $userid, 'social' => $account->social)); 
             continue;
           }
           $settings = $DB->get_records('monorail_social_settings', array('userid' => $userid, 'social' => $account->social));
           if(count($settings) > 0) {
             $socialaccount = array(); 
             $socialaccount['type'] = $account->social;
             foreach($settings as $setting) {
               if($myself || in_array($setting->datakey, $details)) {
                 $socialaccount[$setting->datakey] = $setting->value;
               }
             }
             $socialaccounts[] = $socialaccount;
          }
       }
       return $socialaccounts;
    }

    public static function get_social_settings_returns()
    {
        return new external_multiple_structure(
        new external_single_structure(
          array(
            "type" => new external_value(PARAM_TEXT, "Account type",VALUE_OPTIONAL),
            "account" => new external_value(PARAM_TEXT, "Account username",VALUE_OPTIONAL),
            "accounturl" => new external_value(PARAM_TEXT, "Account username",VALUE_OPTIONAL),
            "share_enroll" => new external_value(PARAM_INT, "Share if enrolll to public course",VALUE_DEFAULT,0),
            "share_complete" => new external_value(PARAM_INT, "Share if course completed",VALUE_DEFAULT,0),
            "share_certificate" => new external_value(PARAM_INT, "Share course certificate",VALUE_DEFAULT,0),
          )),'Settings for social accounts',VALUE_OPTIONAL);
    }

    public static function update_social_settings_parameters() {
        return new external_function_parameters(
            array('settings' => new external_multiple_structure (
                new external_single_structure(
                    array(
                      "type" => new external_value(PARAM_TEXT, "Account type"),
                      "share_enroll" => new external_value(PARAM_INT, "Share if enrolll to public course",VALUE_OPTIONAL),
                      "share_complete" => new external_value(PARAM_INT, "Share if course completed",VALUE_OPTIONAL),
                      "share_certificate" => new external_value(PARAM_INT, "Share course certificate",VALUE_OPTIONAL)
                    )))
            )
        );
    }

    public static function update_social_settings($settings)
    {
        global $DB, $USER, $CFG;
        $params = self::validate_parameters(self::update_social_settings_parameters(),
                                                array('settings' => $settings));

        $basesettings = array('share_enroll','share_complete','share_certificate'); 
        foreach($settings as $setting) { 
            foreach($basesettings as $basesetting) {
              if(array_key_exists($basesetting, $setting)) {
                $record = $DB->get_record('monorail_social_settings', array('userid' => $USER->id, 'social' => $setting['type'], 'datakey' => $basesetting));
                if($record) {
                  $record->value = $setting[$basesetting];
                  $DB->update_record('monorail_social_settings',$record);
                } else {
                  $record = new stdClass();
                  $record->value = $setting[$basesetting];
                  $record->datakey = $basesetting;
                  $record->userid = $USER->id;
                  $record->social = $setting['type'];
                  $record->timemodified = time();
                  $DB->insert_record('monorail_social_settings',$record);
                } 
              }
            }
       }
       $result = array();
       $result['warnings'] = array();
       return $result;
    }

    public static function update_social_settings_returns()
    {
      return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
    }

    public static function delete_social_account_parameters() {
        return new external_function_parameters(
            array('account'   => new external_value(PARAM_TEXT, 'Account name')
            )
        );
    }

    public static function delete_social_account($account)
    {
       global $DB, $USER;
       $params = self::validate_parameters(self::delete_social_account_parameters(),
                                                array('account' => $account));
 
       //Delete all user records
       $DB->delete_records('monorail_social_settings', array('userid' => $USER->id, 'social' => $account)); 
       $DB->delete_records('monorail_social_keys', array('userid' => $USER->id, 'social' => $account)); 

       $result = array();
       $result['warnings'] = array();
       return $result;
    }

    public static function delete_social_account_returns()
    {
      return new external_single_structure(
            array(
                'warnings'  => new external_warnings()
            )
        );
  
   }
 
    public static function rate_course_parameters()
    {
        return new external_function_parameters(array(
            'courseid' => new external_value(PARAM_INT, 'Course id'),
            'stars' => new external_value(PARAM_INT, 'Star count'),
            'review' => new external_value(PARAM_RAW, 'Review text'),
            'shareon' => new external_value(PARAM_RAW, 'Social accounts where to share (comma separated)'),
            'enable_autoshare' => new external_value(PARAM_INT, 'Re-enable enrollment auto sharing settings', VALUE_DEFAULT, 0),
            'image' => new external_value(PARAM_RAW, 'Image to share (leave empty to share course background)', VALUE_DEFAULT, ''),
            'share_message' => new external_value(PARAM_RAW, 'Text to share on social networks', VALUE_DEFAULT, '')
        ));
    }

    public static function rate_course($courseid, $stars, $review, $shareon, $enable_autoshare, $image, $share_message)
    {
        global $DB, $USER, $CFG;

        social_share_course_review($courseid, $stars, $share_message, $shareon . "-", $image);

        $rec = new stdClass();

        $rec->userid = $USER->id;
        $rec->entryid = $courseid;
        $rec->entrytype = "course";
        $rec->timestamp = time();
        $rec->action = "S";

        $DB->insert_record("monorail_share_reminders", $rec);

        // User has the option (not) to set this setting while writing
        // review/rating...
        if ($enable_autoshare)
        {
            $DB->execute("UPDATE {monorail_social_settings} SET value=1 WHERE userid=" . $USER->id . " AND datakey='share_enroll'");
        }

        if ($stars > 0 || $review)
        {
            $user = $DB->get_record_sql("SELECT firstname, lastname FROM {user} WHERE id=?", array($USER->id));
            $courseData = $DB->get_record("monorail_course_data", array("courseid" => $courseid));

            if (isset($CFG->mage_api_url))
            {
                addBackgroundCall(function () use ($courseData, $stars, $review, $user, $USER)
                {
                    $api = magento_get_instance();

                    if ($api)
                    {
                        $api->rateProduct($courseData->code, $stars, $review, $user->firstname . " " . $user->lastname, $USER->id);
                    }
                });
            }

            wscache_reset_by_dependency("course_info_" . $courseid);
        }

        return array();
    }

    public static function rate_course_returns()
    {
        return new external_single_structure(array());
    }

    public static function get_share_reminder_parameters()
    {
        return new external_function_parameters(array(
            'entryid' => new external_value(PARAM_INT, 'Entry id'),
            'entrytype' => new external_value(PARAM_RAW, 'Entry type')
        ));
    }

    public static function get_share_reminder($entryid, $entrytype)
    {
        global $DB, $USER;

        $data = array("entryid" => $entryid, "entrytype" => $entrytype);

        // Reminders about certificates are more intrusive (bug 1920)
        if ($entrytype != "certificate")
        {
            // User registered less than 24 hours ago - do not disturb.
            $firstAccessTime = $DB->get_field_sql("SELECT firstaccess FROM {user} WHERE id=?", array($USER->id));

            if (time() - $firstAccessTime < 86400)
            {
                return $data;
            }

            if ($entrytype == "course")
            {
                // User enrolled to the course less than 24 hours ago - don't
                // notify...

                $enrollTime = $DB->get_field_sql("SELECT ue.timecreated FROM {user_enrolments} AS ue" .
                        " INNER JOIN {enrol} AS e ON ue.enrolid=e.id WHERE ue.userid=? AND e.courseid=?",
                    array($USER->id, $entryid));

                if (time() - $enrollTime < 86400)
                {
                    return $data;
                }
            }

            // Last reminder shown less than 1 hour ago - don't show new ones...
            $lastReminderTime = $DB->get_field_sql("SELECT MAX(timestamp) FROM {monorail_share_reminders} WHERE userid=?", array($USER->id));

            if (time() - $lastReminderTime < 3600)
            {
                return $data;
            }
        }

        $reminderCount = $DB->get_field_sql("SELECT COUNT(*) FROM {monorail_share_reminders}" .
                " WHERE userid=? AND entrytype=? AND entryid=?",
                array($USER->id, $entrytype, $entryid));

        // Reminder(s) for this item already shown...
        if ($reminderCount > 0)
        {
            $lastReminder = $DB->get_record_sql("SELECT id, timestamp, action FROM {monorail_share_reminders} " .
                    "WHERE (userid=? AND entrytype=? AND entryid=?) ORDER BY timestamp DESC LIMIT 1",
                    array($USER->id, $entrytype, $entryid));

            // User already shared, or doesn't want to share...
            if ($lastReminder->action == "S" || $lastReminder->action == "N")
            {
                return $data;
            }

            // Don't remind too often...
            $lastReminderDaysPassed = (time() - $lastReminder->timestamp) / 86400;

            switch ($reminderCount)
            {
                case 1:
                    // 1 day after first reminder...
                    if ($lastReminderDaysPassed < 1)
                    {
                        return $data;
                    }

                case 2:
                    // 7 days after second reminder...
                    if ($lastReminderDaysPassed < 7)
                    {
                        return $data;
                    }

                default:
                    // then remind every 30 days until user shares or tells
                    // to never remind.
                    if ($lastReminderDaysPassed < 30)
                    {
                        return $data;
                    }
            }
        }

        $rec = new stdClass();

        $rec->userid = $USER->id;
        $rec->entryid = $entryid;
        $rec->entrytype = $entrytype;
        $rec->action = 'O';
        $rec->timestamp = time();

        $data["reminderid"] = $DB->insert_record("monorail_share_reminders", $rec);
        $data["show"] = 1;

        return $data;
    }

    public static function get_share_reminder_returns()
    {
        return new external_single_structure(array(
            'entryid' => new external_value(PARAM_INT, 'Entry id'),
            'entrytype' => new external_value(PARAM_RAW, 'Entry type'),
            'reminderid' => new external_value(PARAM_INT, 'Reminder id', VALUE_OPTIONAL),
            'show' => new external_value(PARAM_INT, 'Show the reminder?', VALUE_DEFAULT, 0)
        ));
    }

    public static function save_share_reminder_parameters()
    {
        return new external_function_parameters(array(
            'reminderid' => new external_value(PARAM_INT, 'Reminder id'),
            'action' => new external_value(PARAM_RAW, 'Action')
        ));
    }

    public static function save_share_reminder($reminderid, $action)
    {
        global $DB, $USER;

        $rec = new stdClass();

        $rec->id = $reminderid;
        $rec->action = $action;

        $DB->update_record("monorail_share_reminders", $rec);

        return array();
    }

    public static function save_share_reminder_returns()
    {
        return new external_single_structure(array());
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_course_info_parameters() {
      return new external_function_parameters(
          array(
              'coursecode' => new external_value(PARAM_TEXT, 'course code'),
              'courseid' => new external_value(PARAM_INT, 'course id')
          )
      );
    }

    /**
     * Return course info in which the user is enrolled
     * @param int $courseid specify course ids to be returned
     * @return array of course info and warnings
     * @since  Moodle 2.4
     */
    public static function get_course_info($coursecode, $courseid) {
      global $USER, $DB, $CFG;

      require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');

      $params = self::validate_parameters(self::get_course_info_parameters(), array('coursecode' => $coursecode, "courseid" => $courseid));

      $warnings = array();
      try
      {
        if ($courseid)
        {
            $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$courseid));

            $coursecode = $courseData->code;
        }
        else
        {
            $courseData = $DB->get_record('monorail_course_data', array('code'=>$coursecode));

            $courseid = $courseData->courseid;
        }

        $coursecontext = context_course::instance($courseid);

        if($coursecontext && is_enrolled($coursecontext, $USER->id))
        {
          $cert_hashurl = "";
          $cert_issuetime = "";

          $certificates = $DB->get_records_sql("SELECT id, hashurl, issuedat FROM {monorail_certificate} WHERE userid=? AND courseid=? AND state",
            array($USER->id, $courseid));

          foreach ($certificates as $cert)
          {
            $cert_hashurl = $cert->hashurl;
            $cert_issuetime = $cert->issuedat;
          }

          // Data from course table
          $courseRec = $DB->get_record("course", array("id" => $courseid));

          // Monorail data
          $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($courseid));
          $monorailData = array();
    
          foreach ($moreData as $md)
          {
            $monorailData[$md->datakey] = $md->value;
          }

            // Load background picture
            $backgroundimage = @$monorailData["coursebackground"];
            if (!$backgroundimage)
            {
                $backgroundimage = $CFG->magic_ui_root . "/app/img/stock/other.jpg";
            }  
            $backgroundimagetn = monorail_get_course_background_tn($backgroundimage, $courseid);
            if(!$backgroundimagetn) {
                $backgroundimagetn = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg"; 
            }
            // Load course logo
            $course_logo = @$monorailData["course_logo"];

            if (!$course_logo)
            {
                $course_logo = "/";
            }

            // invitations
            $courseinvites = monorail_get_or_create_invitation('course', $courseid);
            $courseinviteurl = monorail_format_invite_link($courseinvites->code);

            $canedit = (has_capability('moodle/course:update', $coursecontext)) ? 1 : 0;
            $cantwrite = 0;

            $cohortid = 0;//init
            $cohort = $DB->get_field('monorail_cohort_courseadmins', 'cohortid', array('courseid'=>$courseid), IGNORE_MULTIPLE);
            if ($cohort) {
              $cohortid=$cohort;
              //Ok cohort course can have privacy settings set by admin which teacher cannot change
              $csetting = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohortid, 'name' => 'cohort_course_privacy'));
              $ccprivate = (!empty($csetting)) ? $csetting->value: 0;
              $csetting = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohortid, 'name' => 'cohort_course_privacy_ext'));
              $ccprivateext = (!empty($csetting)) ? $csetting->value: 0;

                // If i am only a manager in this course, and not the owner of
                // this cohort, can't write then:

                $myroles = $DB->get_fieldset_sql("SELECT r.shortname AS role FROM {role_assignments} AS ra " .
                    "INNER JOIN {role} AS r ON ra.roleid=r.id " .
                    "INNER JOIN {context} AS ctx ON ctx.id=ra.contextid AND ctx.contextlevel=50 " .
                        "WHERE ra.userid=? AND ctx.instanceid=?", array($USER->id, $courseid));

                $nonManager = false;

                foreach ($myroles as $myrole) {
                    if ($myrole != "manager") {
                        $nonManager = true;
                        break;
                    }
                }

                if (!$nonManager) {
                    $userCohortRole = $DB->get_field_sql("SELECT usertype FROM {monorail_cohort_admin_users} WHERE cohortid=? AND userid=?",
                        array($cohortid, $USER->id));

                    if ($userCohortRole != 1 && $userCohortRole != 2) {
                        $cantwrite = 1;
                    }
                }
            }

        if ($courseRec->startdate)
        {
            $datestring = userdate($courseRec->startdate, "%d %b %Y")." - ";

            if($courseData->enddate) {
              $datestring .= userdate($courseData->enddate, "%d %b %Y");
            }
        }
        else
        {
            $datestring = get_string("self_paced", "theme_monorail");
        }
          $mid = $DB->get_field('modules', 'id', array('name'=>'assign'));
          $acount = $DB->count_records('course_modules', array('course'=>$courseid, 'visible'=>1, 'module' => $mid));
          $mid = $DB->get_field('modules', 'id', array('name'=>'quiz'));
          $qcount = $DB->count_records('course_modules', array('course'=>$courseid, 'visible'=>1, 'module' => $mid));
          $tcount = $qcount + $acount;
          $extcourse = $DB->record_exists('monorail_external_courses', array('intcode'=>$coursecode)) ? 1 : 0;
          $cperm = monorail_get_course_perms($courseid);
          $crsarray = array(
            "courseid" => $courseid,
            "code" => $coursecode,
            "canedit" => $canedit,
            "cantwrite" => $cantwrite,
            "shortname" => $courseRec->shortname,
            "fullname" => $courseRec->fullname,
            "timemodified" => (int) $courseRec->timemodified,
            "startdate" => (int) $courseRec->startdate,
            "completed" => ($courseData->status) ? 0 : 1,
            "coursebackground" => $backgroundimage,
            "coursebackgroundtn" => $backgroundimagetn,
            "course_logo" => $course_logo,
            "cert_hashurl" => $cert_hashurl,
            "cert_issuetime" => $cert_issuetime,
            'inviteurl' => $courseinviteurl,
            'invitesopen' => 1,
            'invitecode' => $courseinvites->code,
            'extcourse' => $extcourse, 
            'categoryid' => $courseRec->category,
            'course_privacy' => $cperm['course_privacy'],
            'course_access' => $cperm['course_access'],
            'course_review' => $cperm['review'],
            'course_price' => (double) @$monorailData["course_price"],
            'course_paypal_account' => @$monorailData["course_paypal_account"],
            'review_submit_time' => @$monorailData["review_submit_time"],
            'course_rating' => (int) @$monorailData["course_rating"],
            'cohortid' => (!empty($cohortid)) ? $cohortid : 0,
            'ccprivate' => (!empty($ccprivate)) ? $ccprivate: 0,
            'ccprivateext' => (!empty($ccprivateext)) ? $ccprivateext: 0,
            'enddate' => $courseData->enddate,
            'mainteacher' => $courseData->mainteacher,
            "cert_enabled" => (int) (@$monorailData["cert_enabled"]),
            "cert_custom" => (int) (@$monorailData["cert_custom"]),
            "cert_custom_info" => (@$monorailData["cert_custom_info"]),
            "cert_orglogo" => (@$monorailData["cert_orglogo"]),
            "cert_sigpic" => (@$monorailData["cert_sigpic"]),
            "language" => (@$monorailData["language"]),
            "course_country" => (@$monorailData["course_country"]),
            "keywords" => (@$monorailData["keywords"]),
            "licence" => (int) $courseData->licence,
            "participants_hidden" => (int) (@$monorailData["participants_hidden"]),
            "durationstr" => $datestring,
            "taskcount" => $tcount,
            "progress" => (float) self::get_user_course_progress($courseid, $USER->id)
          );
          if($extcourse) {
              $crsarray['extprovider'] = $DB->get_field('monorail_external_courses', 'provider', array('intcode'=> $coursecode));
          }

            if ($courseData->derivedfrom) {
                $crsarray["derivedfrom"] = (int) $courseData->derivedfrom;
                $crsarray["derivedfrom_name"] = $DB->get_field_sql("SELECT fullname FROM {course} WHERE id=?", array($courseData->derivedfrom));
                $crsarray["derivedfrom_code"] = $DB->get_field_sql("SELECT code FROM {monorail_course_data} WHERE courseid=?", array($courseData->derivedfrom));
            }

          // Teacher
          if (isset($courseData->mainteacher) && $courseData->mainteacher)
          {
            $teacher = $DB->get_record_sql("SELECT firstname, lastname FROM {user} WHERE id=?", array($courseData->mainteacher));
            $teacherData = array();
            foreach ($DB->get_recordset_sql("SELECT datakey, value FROM {monorail_data} WHERE itemid=?" .
              " AND datakey IN ('pichash')", array($courseData->mainteacher)) as $item)
            {
              $teacherData[$item->datakey] = $item->value;
            }
            $teacherName = $teacher->firstname . " " . $teacher->lastname;
            $teacherTitle = get_string('label_teacher','theme_monorail');
            $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);
            if($fieldid) {
              $title = $DB->get_field('user_info_data', 'data', array('userid'=>$courseData->mainteacher, 'fieldid' => $fieldid), IGNORE_MULTIPLE);
              if($title) {
                $teacherTitle = $title;
              }
            }
            // Teacher logo.
            if (array_key_exists("pichash", $teacherData))
            {
              $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/" . $teacherData["pichash"] . "_90.jpg";
            }
            else
            {
              $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/default_90.jpg";
            }
            $crsarray['teacher_name'] = $teacherName;
            $crsarray['teacher_title'] = $teacherTitle;
            $crsarray['teacher_avatar'] = $teacherLogo;
          }

          // skills
          /*
          try {
            require_once("$CFG->dirroot/theme/monorail/skills.php");
            $skills = monorail_get_course_skills($courseid);
            $crsarray['skills'] = array();
            foreach ($skills as $skill) {
               $skillrec = monorail_get_skill($skill->skillid);
               $crsarray['skills'][] = $skillrec->name;
            }
          } catch (Exception $ex) {
            add_to_log($courseid, 'monorailservices', 'externallib', 'get_course_info', 'Exception when trying to get skills: '.$ex->getMessage());
          }
          */

          $studentCount = count(get_students_in_cource($courseid));

          $crsarray['_students'] = $studentCount;
          if ($canedit)
          {
              $crsarray['students'] = $studentCount;
          }
          else
          {
              // XXX: not sure why student count cannot be given to
              // non-teachers (the same number can be calculated from
              // participant list, which is available to all students any
              // way), so just adding _students for all to lessen client
              // complexity.
              $crsarray['students'] = 0;
          }

          wscache_add_cache_dependency("course_info_" . $courseid);
          wscache_add_cache_dependency("course_tasks_" . $courseid);

        } else {
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $courseid;
          $warning['warningcode'] = '1';
          $warning['message'] = 'No access rights in course context';
          $warnings[] = $warning;

          // This may happen if user tries to open course he doesn't belong to.
          wscache_cancel_cached_function();
        }
      } catch(Exception $e) {
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $courseid;
          $warning['warningcode'] = '1';
          $warning['message'] = 'Error in course context, no course with id';
          $warnings[] = $warning;
      }

      $crsarray['warnings'] = $warnings;
      return $crsarray;
    }

    /**
     * Describes the return value for get_course_info
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_course_info_returns() {
        return new external_single_structure(
            array(
                "courseid" => new external_value(PARAM_INT, "course id", VALUE_OPTIONAL),
                "code" => new external_value(PARAM_TEXT, "course code", VALUE_OPTIONAL),

                "shortname" => new external_value(PARAM_TEXT, "course shortname", VALUE_OPTIONAL),
                "fullname" => new external_value(PARAM_TEXT, "course fullname", VALUE_OPTIONAL),
                "timemodified" => new external_value(PARAM_INT, "course timemodified", VALUE_OPTIONAL),
                "startdate" => new external_value(PARAM_INT, "course startdate", VALUE_OPTIONAL),
                "canedit" => new external_value(PARAM_INT, "course can edit", VALUE_OPTIONAL),
                "cantwrite" => new external_value(PARAM_INT, "this admin user cannot write to this course", VALUE_OPTIONAL),
                "progress" => new external_value(PARAM_FLOAT, "course progress", VALUE_OPTIONAL),

                "completed" => new external_value(PARAM_INT, "course completed", VALUE_OPTIONAL),
                "coursebackground" => new external_value(PARAM_TEXT, "course background", VALUE_OPTIONAL),
                "coursebackgroundtn" => new external_value(PARAM_TEXT, "course background thumbnail", VALUE_OPTIONAL),
                "course_logo" => new external_value(PARAM_TEXT, "course logo", VALUE_OPTIONAL),

                'skills'    => new external_multiple_structure(new external_value(PARAM_TEXT, 'skill name', VALUE_OPTIONAL), 'list of skills', VALUE_DEFAULT, array()),

                'mainteacher' => new external_value(PARAM_INT, 'main teacher', VALUE_OPTIONAL),
                'teacher_name' => new external_value(PARAM_TEXT, 'teacher name', VALUE_OPTIONAL),
                'teacher_title' => new external_value(PARAM_TEXT, 'teacher title', VALUE_OPTIONAL),
                'teacher_avatar' => new external_value(PARAM_TEXT, 'teacher avatar', VALUE_OPTIONAL),
                'language' => new external_value(PARAM_TEXT, 'Course language', VALUE_OPTIONAL),
                'course_country' => new external_value(PARAM_TEXT, 'Course country', VALUE_OPTIONAL),
                'keywords' => new external_value(PARAM_TEXT, 'Course keywords', VALUE_OPTIONAL),
                'licence' => new external_value(PARAM_INT, 'Course licence', VALUE_OPTIONAL),
                'derivedfrom' => new external_value(PARAM_INT, 'Course derivedfrom', VALUE_OPTIONAL),
                'derivedfrom_name' => new external_value(PARAM_TEXT, 'Course derivedfrom name', VALUE_OPTIONAL),
                'derivedfrom_code' => new external_value(PARAM_TEXT, 'Course derivedfrom code', VALUE_OPTIONAL),
                'participants_hidden' => new external_value(PARAM_INT, 'Participants hidden', VALUE_OPTIONAL),

                "cert_hashurl" => new external_value(PARAM_TEXT, "Certificate URL hash (if issued)", VALUE_OPTIONAL),
                "cert_issuetime" => new external_value(PARAM_TEXT, "Certificate issue time", VALUE_OPTIONAL),

                'enddate' => new external_value(PARAM_INT, 'end date timestamp', VALUE_OPTIONAL),
                'taskcount' => new external_value(PARAM_INT, 'No of visible tasks in course', VALUE_OPTIONAL),
                'inviteurl' => new external_value(PARAM_URL, 'public invitation url for self enrollment', VALUE_OPTIONAL),
                'invitesopen' => new external_value(PARAM_INT, 'public invitation url status', VALUE_OPTIONAL),
                'invitecode' => new external_value(PARAM_TEXT, 'public invitation code', VALUE_OPTIONAL),
                'categoryid' => new external_value(PARAM_INT, 'category id', VALUE_OPTIONAL),
                'course_privacy' => new external_value(PARAM_INT, 'course public=1, private=2', VALUE_DEFAULT, 2),
                'course_access' => new external_value(PARAM_INT, 'course access draft=1, invite=2, open=3', VALUE_DEFAULT, 1),
                'course_review' => new external_value(PARAM_INT, 'course review', VALUE_DEFAULT, 0),
                'course_rating' => new external_value(PARAM_INT, 'course rating', VALUE_OPTIONAL),
                'course_price' => new external_value(PARAM_NUMBER, 'Price for course enrollment', VALUE_OPTIONAL),
                'course_paypal_account' => new external_value(PARAM_TEXT, 'PayPal account that receives payments for course', VALUE_OPTIONAL),
                'review_submit_time' => new external_value(PARAM_TEXT, 'Time when course was submitted for review (for pub. catalog)', VALUE_OPTIONAL),
                'cohortid'        => new external_value(PARAM_INT, 'Cohort id if course belongs to cohort',VALUE_OPTIONAL),
                'ccprivate'       => new external_value(PARAM_INT, 'If teacher can edit public/private access restrictions',VALUE_OPTIONAL),
                'ccprivateext'       => new external_value(PARAM_INT, 'If teacher can edit public/private access restrictions',VALUE_OPTIONAL),
                'cert_enabled' => new external_value(PARAM_INT, 'Certificates enabled for course', VALUE_OPTIONAL),
                "cert_orglogo" => new external_value(PARAM_TEXT, "Organization logo for certificate (if set)", VALUE_OPTIONAL),
                "cert_sigpic" => new external_value(PARAM_TEXT, "Teacher signature for certificate (if set)", VALUE_OPTIONAL),
                'cert_custom' => new external_value(PARAM_INT, 'Custom certificates', VALUE_OPTIONAL),
                'cert_custom_info' => new external_value(PARAM_TEXT, 'Custom certificate info', VALUE_OPTIONAL),

                "durationstr" => new external_value(PARAM_TEXT, "durationstr", VALUE_OPTIONAL),
                "students" => new external_value(PARAM_INT, "student count", VALUE_OPTIONAL),
                "_students" => new external_value(PARAM_INT, "student count", VALUE_OPTIONAL),
                "extcourse" => new external_value(PARAM_INT, "External course", VALUE_DEFAULT, 0),
                "extprovider" => new external_value(PARAM_TEXT, "External course provider", VALUE_OPTIONAL),

                'warnings'  => new external_warnings()
          )
      );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_user_courses_parameters() {
      return new external_function_parameters(
          array(
              'courseids' => new external_multiple_structure(
                  new external_value(PARAM_INT, 'course id'),
                  '0 or more course ids',
                  VALUE_DEFAULT, array()),
              'isteacher' => new external_value(PARAM_INT, 'Course teacher', VALUE_DEFAULT, 0),
              'isactive' => new external_value(PARAM_INT, 'Completed courses', VALUE_DEFAULT, 1),
              'capabilities'  => new external_multiple_structure(
                  new external_value(PARAM_CAPABILITY, 'capability'),
                  'ANDed list of capabilities used to filter courses',
                  VALUE_DEFAULT, array()),
          )
      );
    }

    private static function get_user_course_progress($courseid, $userid)
    {
        global $DB, $CFG;

        require_once($CFG->libdir . '/gradelib.php'); 

          $cprogress = monorail_data("courseprogress", $courseid, 'progress-'.$userid);

          if(empty($cprogress))
          {
            // We need to collect all data about progress here !!
            $qmodules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, m.id as assignmentid, m.course 
                                   FROM {course_modules} cm, {modules} md, {quiz} m
                                   WHERE cm.course = $courseid AND
                                        cm.instance = m.id AND
                                        md.name = 'quiz' AND
                                        md.id = cm.module", array());
            $totalquiz = count($qmodules);
            $cquiz = 0;
            foreach($qmodules as $module) {
                $haveSubmissions = $DB->count_records("monorail_data", array("type" => "quiz_attempt", "itemid" => $module->assignmentid, "datakey" => "user" . $userid));
                if($haveSubmissions) {
                  $cquiz = $cquiz + 1; 
                }
            } 
            $amodules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, m.id as assignmentid, m.course, m.nosubmissions, m.duedate 
                                   FROM {course_modules} cm, {modules} md, {assign} m
                                   WHERE cm.course = $courseid AND
                                        cm.instance = m.id AND
                                        md.name = 'assign' AND
                                        md.id = cm.module", array()); 
            $totalassign = count($amodules); 
            $cassign = 0;
            foreach($amodules as $module) {
              if(!$module->nosubmissions) {
                //Find if graded submissions
                $grading_info = grade_get_grades($module->course, 'mod', 'assign', $module->assignmentid, $userid);
                $gradingitem = $grading_info->items[0];
                $gradebookgrade = $gradingitem->grades[$userid];                 
                if($gradebookgrade->dategraded) {
                  $cassign = $cassign + 1; 
                }
              } else if($module->duedate < time()){
                  $cassign = $cassign + 1;
              } 
            } 
            $totalTasks = $totalquiz + $totalassign;
            $ctotalTasks = $cquiz + $cassign; 
            $courseprogress = ($totalTasks) ? ($ctotalTasks/$totalTasks)*100 : 0;
            // XXX: replaced $id with $courseid (who makes stupid errors like this?)
            monorail_data("courseprogress", $courseid, 'progress-'.$userid, $courseprogress);

            return $courseprogress;
          } else {
            $cprogress = reset($cprogress);

            return $cprogress->value;
          }
    }

    /**
     * Return courses in which the user is enrolled, optionally filtered
     * by course id and/or capability
     * @param array of ints $courseids specify course ids to be returned
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return array of courses and warnings
     * @since  Moodle 2.4
     */
    public static function get_user_courses($courseids = array(), $isteacher = 0, $isactive = 1, $capabilities = array()) {
      global $USER, $DB, $CFG;
    
      require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');
    
      $params = self::validate_parameters(self::get_user_courses_parameters(),
          array('courseids' => $courseids, 'capabilities' => $capabilities, 'isteacher' => $isteacher, 'isactive' => $isactive));

      //$fields = 'sortorder,shortname,fullname,timemodified,startdate';
      //$courses = array();
      //$courses = enrol_get_users_courses($USER->id, $iscompleted, $fields);
      //$courses = enrol_get_users_courses($USER->id, false, $fields);

        if ($isteacher) {
            $interestingRoles = "'coursecreator', 'editingteacher', 'teacher'";

            $auser = $DB->get_record_sql("SELECT id, usertype FROM {monorail_cohort_admin_users} WHERE userid=?", array($USER->id));

            if (!$auser || $auser->usertype == 3) {
                $interestingRoles .= ", 'manager'";
            }
        } else {
            $interestingRoles = "'student'";
        }

        $interestingRoleIds = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN (" . $interestingRoles . ")");

        if (empty($interestingRoleIds)) {
            // No interesting roles???
            $interestingRoleIds = array(0);
        }

        $courses = $DB->get_records_sql("SELECT DISTINCT c.id AS id, c.sortorder AS sortorder " .
            "FROM {course} AS c " .
                "INNER JOIN {context} AS ctx ON c.id=ctx.instanceid AND ctx.contextlevel=50 " .
                "INNER JOIN {role_assignments} AS ra ON ra.contextid=ctx.id " .
                "INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=c.id ".
                    "WHERE ra.roleid IN (" . implode(", ", $interestingRoleIds) . ") AND ra.userid=? AND mcd.status=?",
                        array($USER->id, (int) $isactive));

      $unavailablecourseids = array();
      $warnings = array();

      $coursearray = array();
      require_once($CFG->libdir . '/gradelib.php');
      foreach ($courses as $id => $course) {
        if((count($params['courseids'])>0) && !in_array($courses[$id]->id, $params['courseids'])) {
          array_push($unavailablecourseids,$id);
          continue;
        }
        $coursecontext = context_course::instance($courses[$id]->id);
        try {
          self::validate_context($coursecontext);
          require_capability('moodle/course:viewparticipants', $coursecontext);
        } catch (Exception $e) {
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $id;
          $warning['warningcode'] = '1';
          $warning['message'] = 'No access rights in course context';
          $warnings[] = $warning;
          continue;
        }
        foreach ($params['capabilities'] as $cap) {
          if (!has_capability($cap, $coursecontext)) {
            continue;
          }
        }

        $canedit = (has_capability('moodle/course:manageactivities', $coursecontext) || has_capability('moodle/course:update', $coursecontext)) ? 1 : 0;
        $uc = monorail_get_course_details($courses[$id]->id);

        /*
        if(($isteacher && !$canedit) || (!$isteacher && $canedit)) {
          continue;
        }  
        */

        /*
        $cactive = intval($courseData->status);
        if(($cactive && !$isactive) || (!$cactive && $isactive)) {
          continue;
        }
        */

        if ($isteacher) {
            $courseprogress = 0;
        } else {
            $courseprogress = self::get_user_course_progress($courses[$id]->id, $USER->id);
        }

        $crsarray = array('id'=>$uc->id,
            'code' => $uc->code,
            'fullname'=>$uc->fullname,
            'coursebackground' => $uc->coursebackground,
            'coursebackgroundtn' => $uc->coursebackgroundtn,
            'course_logo' => $uc->courselogo,
            'durationstr' => $uc->durationstr,
            'students' => $uc->students,
            'progress' => $courseprogress,
            'stars' => $uc->stars,
            'price' => $uc->price,
            'course_access' => $uc->course_access,
            'course_privacy' => $uc->course_privacy,
            'can_edit' => $canedit,
            "cert_enabled" => $uc->cert_enabled
        );

        wscache_add_cache_dependency("course_info_" . $courses[$id]->id);

        $coursearray[] = $crsarray;
      }

      wscache_add_cache_dependency("user_courses_" . $USER->id);
    
      $result = array();
      $result['courses'] = $coursearray;
    
      if (count($unavailablecourseids)>0) {
        foreach ($unavailablecourseids as $unavailablecourseid) {
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $unavailablecourseid;
          $warning['warningcode'] = '2';
          $warning['message'] = 'User is not enrolled or does not have requested capability';
          $warnings[] = $warning;
        }
      }
      $result['warnings'] = $warnings;
      return $result;
    }
    
    /**
     * Describes the return value for get_user_courses
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_user_courses_returns() {
      return new external_single_structure(
          array(
              'courses'   => new external_multiple_structure(self::course_basic(), 'list of courses with basic info'),
              'warnings'  => new external_warnings()
          )
      );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_user_tasks_parameters() {
      return new external_function_parameters(
          array(
              'courseids' => new external_multiple_structure(
                  new external_value(PARAM_INT, 'course id'),
                  '0 or more course ids',
                  VALUE_DEFAULT, array()),
              'capabilities'  => new external_multiple_structure(
                  new external_value(PARAM_CAPABILITY, 'capability'),
                  'ANDed list of capabilities used to filter courses',
                  VALUE_DEFAULT, array())
          )
      );
    }
    
    /**
     * Return user tasks for given courses, optionally filtered
     * by course id and/or capability
     * @param array of ints $courseids specify course ids to be returned
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return array of courses and warnings
     * @since  Moodle 2.4
     */
    public static function get_user_tasks($courseids = array(), $capabilities = array()) {
      global $USER, $DB, $CFG;

      require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');

      $params = self::validate_parameters(self::get_user_tasks_parameters(),
          array('courseids' => $courseids, 'capabilities' => $capabilities));

      $warnings = array();

      $tasksarray = array();
      $assign_module_info = $DB->get_record("modules", array("name" => "assign"));
      $quiz_module_info = $DB->get_record("modules", array("name" => "quiz"));
      require_once($CFG->libdir . '/gradelib.php'); 
      foreach ($courseids as $id) {

        $canViewCourse = self::can_view_course($id, $USER->id);
        $context = context_course::instance($id);

        /*
        try {
          self::validate_context($context);
          require_capability('moodle/course:viewparticipants', $context);
        } catch (Exception $e)
        */

        if (!$canViewCourse) {
          unset($courses[$id]);
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $id;
          $warning['warningcode'] = '1';
          $warning['message'] = 'No access rights in course context';
          $warnings[] = $warning;
          continue;
        }

        /*
        $process = true;
        foreach ($params['capabilities'] as $cap) {
          if (!has_capability($cap, $context)) {
            $process = false;
          }
        }
        if(!$process) {
            continue;
        } 
        */

        $totaltasks = 0;
        $completedtasks = 0;
        $assignmentarray = array();
        $quizarray = array();

        $canEditCourse = (has_capability('moodle/course:manageactivities', $context) || has_capability('moodle/course:update', $context)) ? 1 : 0;

        // Get a list of assignments for the course.
    
        $extrafields='m.id as assignmentid, m.course, m.nosubmissions, m.submissiondrafts, m.sendnotifications, '.
            'm.sendlatenotifications, m.duedate, m.allowsubmissionsfromdate, m.grade, m.timemodified,
                m.intro, m.introformat ';
    
        $modules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, $extrafields
            FROM {course_modules} cm, {modules} md, {assign} m
            WHERE cm.course = $id AND
            cm.instance = m.id AND
            md.name = 'assign' AND
            md.id = cm.module ORDER BY m.duedate ASC", array());

        $canedit = $canEditCourse; //(has_capability('moodle/course:update', $context)) ? 1 : 0;
        $canview = $canViewCourse;

        if ($modules) {
          foreach ($modules as $module) {
            if (!$canEditCourse && $module->allowsubmissionsfromdate && $module->allowsubmissionsfromdate > time()) {
                continue;
            }

            //$context = context_module::instance($module->id);
            $totaltasks = $totaltasks +1;
            /*
            try {
              self::validate_context($context);
              require_capability('mod/assign:view', $context);
            } catch (Exception $e)
            */

            if (!$canview) {
              $warning = array();
              $warning['item'] = 'module';
              $warning['itemid'] = $module->id;
              $warning['warningcode'] = '1';
              $warning['message'] = 'No access rights in module context';
              $warnings[] = $warning;
              continue;
            }
            $configarray = array();
    
            $instance_rec = @reset($DB->get_records("course_modules", array("course" => $module->course, "instance" => $module->assignmentid, "module" => $assign_module_info->id)));
            $section_rec = @reset($DB->get_records("course_sections", array("id"=>$instance_rec->section)));
    
            try {
              $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$module->assignmentid, 'userid'=> $USER->id), '*', MUST_EXIST);
              if ($submitinfo->status === 'draft') {
                throw new Exception("");
              }
              //ok get the num of files submitted
              $submitfile = $DB->get_record('assignsubmission_file', array('assignment'=>$module->assignmentid, 'submission'=> $submitinfo->id));
              $submittext = $DB->get_record('assignsubmission_onlinetext', array('assignment'=>$module->assignmentid, 'submission'=> $submitinfo->id));
              if (isset($submittext->onlinetext) && strlen($submittext->onlinetext) > 0) {
                if (! isset($submitfile) || ! $submitfile) {
                  $submitfile = new stdClass();
                  $submitfile->numfiles = 0;
                }
                $submitfile->numfiles += 1;
              }
              $haveSubmissions = ($submitfile->numfiles > 0 ? 1 : 0);
            } catch (Exception $e) {
              //No submissions
              $haveSubmissions = 0;
            }
            if ($canedit) {
              $gradedCount = $DB->count_records('assign_grades', array('assignment'=>$module->assignmentid));
              $submitcount = 0;
              $submitusers = $DB->get_records('assign_submission', array('assignment'=>$module->assignmentid),'userid');
              $course_context = context_course::instance($module->course);
              $mroleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST);
              foreach ($submitusers as $submituser) {
                if ($submituser->status === 'submitted') {
                  if (is_enrolled($course_context, $submituser->userid) && user_has_role_assignment($submituser->userid, $mroleid, $course_context->id)) {
    
                    $submitcount = $submitcount + 1;
                  }
                }
              }
            }
    
            $gradearray = array();
            $grading_info = grade_get_grades($module->course, 'mod', 'assign', $module->assignmentid, $USER->id);
            $gradingitem = $grading_info->items[0];
            $gradebookgrade = $gradingitem->grades[$USER->id];
            $gradearray[] = array(
                'grade'=> (($gradebookgrade->grade)?intval($gradebookgrade->grade):null),
                'grademax'=> intval($gradingitem->grademax),
                'gradestring'=> str_replace(' ','',get_string('gradelong', 'grades', array('grade'=>intval($gradebookgrade->grade),
                    'max'=>intval($gradingitem->grademax)))),
                'feedback' => $gradebookgrade->str_feedback,
                'dategraded' => $gradebookgrade->dategraded
            );
    
            $assignarray= array('id'=>$module->assignmentid,
                'cmid'=>$module->id,
                'instanceid'=>$instance_rec->id,
                'course'=>$module->course,
                'visible' => $module->visible,
                'name'=>$module->name,
                'modulename' => 'assign',
                'nosubmissions'=>$module->nosubmissions,
                'submissiondrafts'=>$module->submissiondrafts,
                'sendnotifications'=>$module->sendnotifications,
                'sendlatenotifications'=>$module->sendlatenotifications,
                'duedate'=>$module->duedate,
                'allowsubmissionsfromdate'=>$module->allowsubmissionsfromdate,
                'grade'=>$module->grade,
                'timemodified'=>$module->timemodified,
                'intro' => $module->intro,
                'introformat' => $module->introformat,
                'grades' => $gradearray,
                'gradedcount' => ($canedit) ? $gradedCount : 0,
                'submitcount' => ($canedit) ? @$submitCount : 0,
                'fileinfo' => array(),
                'canedit'=>$canedit,
                'haveSubmissions'=>$haveSubmissions,
                'section_visible'=>$section_rec->visible
            );
            if($canedit){
              $assignarray['submitcount'] = $submitcount;
            }
            $assignmentarray[]= $assignarray;
            // count completion
            if (! $canedit) {
              if (! $module->nosubmissions) {
                if ($gradearray[0]['dategraded']) {
                  $completedtasks = $completedtasks +1;
                }
              } else if ($module->duedate < time()) {
                $completedtasks = $completedtasks +1;
              }
            }
          }
        }
    
        $extrafields='m.id as assignmentid, m.course, m.timemodified, m.timeclose AS duedate, m.timeopen AS timeopen ';
        $qmodules =  $DB->get_records_sql("SELECT cm.*, m.name, md.name as modname, $extrafields
            FROM {course_modules} cm, {modules} md, {quiz} m
            WHERE cm.course = $id AND
            cm.instance = m.id AND
            md.name = 'quiz' AND
            md.id = cm.module ORDER BY m.timeclose ASC", array());
    
        // Get a list of quizes for the course.
        if ($modules = array_merge($qmodules)) {
          foreach ($modules as $module) {
            if (!$canEditCourse && $module->timeopen && $module->timeopen > time()) {
                continue;
            }
            //$context = context_module::instance($module->id);
            $totaltasks = $totaltasks +1;
            /*
            try {
              self::validate_context($context);
              require_capability('mod/assign:view', $context);
            } catch (Exception $e)
            */

            if (!$canview) {
              $warning = array();
              $warning['item'] = 'module';
              $warning['itemid'] = $module->id;
              $warning['warningcode'] = '1';
              $warning['message'] = 'No access rights in module context';
              $warnings[] = $warning;
              continue;
            }
            $configarray = array();
    
            $instance_rec = @reset($DB->get_records("course_modules", array("course" => $module->course, "instance" => $module->assignmentid, "module" => $quiz_module_info->id)));
    
            // quizzes have no grades, just push out an empty record since our UI might need this record to exist..
            $gradearray = array(
                array(
                    'grade'=> 0,
                    'grademax'=> 0,
                    'gradestring'=> str_replace(' ','',get_string('gradelong', 'grades', array('grade'=>0, 'max'=>0))),
                    'feedback' => null,
                    'dategraded' => null
                )
            );
    
            $submitCount = 0;
            $gradedCount = 0;
            $haveSubmissions = 0;

            if ($canedit) {
                // Teacher
                $gradedCount = $DB->count_records('quiz_grades', array('quiz'=>$module->assignmentid));

                $quiztakers = $DB->get_records('monorail_data',  array("itemid" => $module->assignmentid, "type" => 'quiz_attempt'));
                $mroleid = $DB->get_field('role', 'id', array('shortname'=>'student'), MUST_EXIST);
                $course_context = context_course::instance($module->course);

                foreach ($quiztakers as $quiztaker) {
                    $qtid = substr($quiztaker->datakey, 4);

                    if (!is_enrolled($course_context, $qtid) || !user_has_role_assignment($qtid, $mroleid, $course_context->id)) {
                        continue;
                    }

                    $stateData = monorail_data('quiz_end', $module->assignmentid, "user" . $qtid);
                    if ($stateData) {
                        $stateData = reset($stateData);

                        if ((int) $stateData->value) {
                            $submitCount++;
                        }
                    }
                }
            } else {
                // Student
                $stateData = monorail_data('quiz_end', $module->assignmentid, "user" . $USER->id);

                if ($stateData) {
                    $stateData = reset($stateData);
                    if ((int) $stateData->value) {
                        $haveSubmissions = 1;
                    }
                }
            }
    
            if ($haveSubmissions)
            {
              $attemptData = monorail_data('quiz_attempt', $module->assignmentid, "user" . $USER->id);
              $attemptData = reset($attemptData);
              $attemptData = unserialize($attemptData->value);
    
              $quizTotal = 0;
              $quizCorrect = 0;
    
              foreach ($attemptData as $question)
              {
                $allOk = true;
    
                if (isset($question["correct"])) {
                    $allOk = $question["correct"];
                } else {
                    foreach ($question["answers"] as $answer)
                    {
                      if ($answer["correct"] != $answer["checked"])
                      {
                        $allOk = false;
                      }
                    }
                }
    
                $quizTotal++;
    
                if ($allOk)
                {
                  $quizCorrect++;
                }
              }
    
              $lastscore = $quizCorrect . '/' . $quizTotal;
              $bestAttemptData = monorail_data('quiz_attempt_best', $module->assignmentid, "user" . $USER->id);
              //Score is of best attempt if available
              if($bestAttemptData) {
                $bestAttemptData = reset($bestAttemptData);
                $bestAttemptData = unserialize($bestAttemptData->value);
                $quizTotal = 0;
                $quizCorrect = 0;
    
                foreach ($bestAttemptData as $question)
                {
                  $allOk = true;
    
                  foreach ($question["answers"] as $answer)
                  {
                    if ($answer["correct"] != $answer["checked"])
                    {
                      $allOk = false;
                    }
                  }
    
                  $quizTotal++;
    
                  if ($allOk)
                  {
                    $quizCorrect++;
                  }
                }
              }
              $score = round($quizCorrect / $quizTotal * 100) . "% (" . $quizCorrect . '/' . $quizTotal . ")";
            }
            else
            {
              $score = "";
              $lastscore = "";
            }

            $attempts = $DB->get_field('quiz', 'attempts' , array('id'=>$module->assignmentid));
            $attemptscount  = reset(monorail_data('quiz_attempts_count', $module->assignmentid, "user" . $USER->id));
            if(!$attemptscount) {
              $attemptscount = 0;
            } else {
              $attemptscount = $attemptscount->value;
            }
            $endquiz  = reset(monorail_data('quiz_end', $module->assignmentid, "user" . $USER->id));
            if(!$endquiz) {
              $endquiz = 0;
            } else {
              $endquiz = $endquiz->value;
            }
            $quizarray[]= array('id'=>$module->assignmentid,
                'cmid'=>$module->id,
                'instanceid'=>$instance_rec->id,
                'course'=>$module->course,
                'name'=>$module->name,
                'modulename'=> 'quiz',
                'visible' => $module->visible,
                'nosubmissions'=>0,
                'submissiondrafts'=>(isset($module->submissiondrafts)) ? $module->submissiondrafts : null,
                'sendnotifications'=>(isset($module->sendnotifications)) ? $module->sendnotifications : null,
                'sendlatenotifications'=>(isset($module->sendlatenotifications)) ? $module->sendlatenotifications : null,
                'duedate'=>$module->duedate,
                'allowsubmissionsfromdate'=>(isset($module->allowsubmissionsfromdate)) ? $module->allowsubmissionsfromdate : null,
                'grade'=>(isset($module->grade)) ? $module->grade : null,
                'grades' => $gradearray,
                'timemodified'=>$module->timemodified,
                'gradedcount' => ($canedit) ? $gradedCount : 0,
                'submitcount' => ($canedit) ? $submitCount : 0,
                'haveSubmissions' => $haveSubmissions,
                'canedit' => $canedit,
                'attempts' =>  $attempts,
                'attemptsleft' => max(($attempts - $attemptscount), 0),
                'endquiz' => $endquiz,
                'lastscore' => $lastscore,
                'score' => $score
            );
              // count completion
              if (! $canedit) {
                 if ($haveSubmissions) {
                    $completedtasks = $completedtasks +1;
                 }
              }
            }
          }
        $cprogress = ($totaltasks) ? ($completedtasks/$totaltasks)*100 : 0;
        if($cprogress > 0) {
            monorail_data("courseprogress", $id, 'progress-'.$USER->id, $cprogress);
        } 
        $tasksarray[] = array('id' => $id,
                              'progress' => $cprogress, 
                              'quizes' => $quizarray,
                              'assignments' => $assignmentarray);
      }
    
      $result = array();
      $result['tasks'] = $tasksarray;
      $result['warnings'] = $warnings;
      return $result;
    }
    
    /**
     * Describes the return value for get_user_tasks 
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_user_tasks_returns() {
      return new external_single_structure(
          array(
              'tasks'   => new external_multiple_structure(self::course_tasks(), 'list of courses with basic info'),
              'warnings'  => new external_warnings()
          )
      );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_course_settings_parameters() {
      return new external_function_parameters(
          array(
              'courseids' => new external_multiple_structure(
                  new external_value(PARAM_INT, 'course id'),
                  '0 or more course ids',
                  VALUE_DEFAULT, array()),
              'capabilities'  => new external_multiple_structure(
                  new external_value(PARAM_CAPABILITY, 'capability'),
                  'ANDed list of capabilities used to filter courses',
                  VALUE_DEFAULT, array())
          )
      );
    }
    
    /**
     * Return courses in which the user is enrolled, optionally filtered
     * by course id and/or capability
     * @param array of ints $courseids specify course ids to be returned
     * @param array of strings $capabilities specify required capabilities for the courses returned
     * @return array of courses and warnings
     * @since  Moodle 2.4
     */
    public static function get_course_settings($courseids = array(), $capabilities = array()) {
      global $USER, $DB, $CFG;
    
      require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');
    
      $params = self::validate_parameters(self::get_course_settings_parameters(),
          array('courseids' => $courseids, 'capabilities' => $capabilities));
    
      $courses = array();
      $warnings = array();
    
      $settingarray = array();
    
      foreach ($courseids as $id) {
        $coursecontext = context_course::instance($id);
        try {
          self::validate_context($coursecontext);
          require_capability('moodle/course:manageactivities', $coursecontext);
        } catch (Exception $e) {
          unset($courseInfo);
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $id;
          $warning['warningcode'] = '1';
          $warning['message'] = 'No access rights in course context';
          $warnings[] = $warning;
          continue;
        }

        $process = true;
        foreach ($params['capabilities'] as $cap) {
          if (!has_capability($cap, $coursecontext)) {
            $process = false;
          }
        }
        if(!$process) {
          continue;
        }
        $courseInfo  = $DB->get_record('course', array('id' => $id), "*", MUST_EXIST);
        $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($courseInfo->id));
        $monorailData = array();
    
        foreach ($moreData as $md)
        {
          $monorailData[$md->datakey] = $md->value;
        }
        // invitations
        $courseinvites = monorail_get_or_create_invitation('course', $courseInfo->id);
        $courseinviteurl = monorail_format_invite_link($courseinvites->code);
    
        $canedit = (has_capability('moodle/course:update', $coursecontext)) ? 1 : 0;
    
        $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$courseInfo->id));
        $cohortid = 0;//init
        $cohort = $DB->get_field('monorail_cohort_courseadmins', 'cohortid', array('courseid'=>$courseInfo->id), IGNORE_MULTIPLE);
        if($cohort) {
          $cohortid=$cohort;
          //Ok cohort course can have privacy settings set by admin which teacher cannot change
          $csetting = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohortid, 'name' => 'cohort_course_privacy'));
          $ccprivate = (!empty($csetting)) ? $csetting->value: 0;
          $csetting = $DB->get_record('monorail_cohort_settings', array('cohortid'=>$cohortid, 'name' => 'cohort_course_privacy_ext'));
          $ccprivateext = (!empty($csetting)) ? $csetting->value: 0;
        }
        $cperm = monorail_get_course_perms($courseInfo->id);
        $settingarray = array('id'=>$courseInfo->id,
            'inviteurl' => $courseinviteurl,
            'invitesopen' => $courseinvites->active,
            'invitecode' => $courseinvites->code,
            'categoryid' => $courseInfo->category,
            'course_privacy' => $cperm['course_privacy'],
            'course_access' => $cperm['course_access'],
            'course_price' => (double) @$monorailData["course_price"],
            'course_paypal_account' => @$monorailData["course_paypal_account"],
            'review_submit_time' => @$monorailData["review_submit_time"],
            'course_rating' => (int) @$monorailData["course_rating"],
            'cohortid' => (!empty($cohortid)) ? $cohortid : 0,
            'ccprivate' => (!empty($ccprivate)) ? $ccprivate: 0,
            'ccprivateext' => (!empty($ccprivateext)) ? $ccprivateext: 0,
            'startdate' => $courseInfo->startdate,
            'enddate' => $courseData->enddate,
            'code' => $courseData->code,
            'mainteacher' => $courseData->mainteacher,
            "cert_enabled" => (int) (@$monorailData["cert_enabled"]),
            "cert_orglogo" => (@$monorailData["cert_orglogo"]),
            "cert_sigpic" => (@$monorailData["cert_sigpic"])
        );
        $settingssarray[] = $settingarray;
      }
    
      $result = array();
      $result['settings'] = $settingssarray;
      $result['warnings'] = $warnings;
      return $result;
    }
    
    /**
     * Describes the return value for get_assignments
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_course_settings_returns() {
      return new external_single_structure(
          array(
              'settings'   => new external_multiple_structure(self::course_settings(), 'list of courses with settings info',VALUE_DEFAULT,array()),
              'warnings'  => new external_warnings()
          )
      );
    }
    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function reset_tasks_parameters() {
      return new external_function_parameters(
          array(
              'courseid' => new external_value(PARAM_INT, 'Course id'),
              'userid' => new external_value(PARAM_INT, 'User id'),
              "quizid" => new external_value(PARAM_INT, "Quiz id (reset only results for given quiz)", VALUE_DEFAULT, 0)
          )
      );
    }

   /**
     * Return course info in which the user is enrolled
     * @param int $courseid specify course ids to be returned
     * @return array of course info and warnings
     * @since  Moodle 2.4
     */
    public static function reset_tasks($courseid, $userid, $quizid) {
        global $USER, $DB, $CFG;

        $result = array();
        $warnings = array();
        $context = context_course::instance($courseid);
        try {
          self::validate_context($context);
          require_capability('mod/assign:view', $context);
        } catch (Exception $e) {
           $warning = array();
           $warning['item'] = 'course';
           $warning['itemid'] = $id;
           $warning['warningcode'] = '1';
           $warning['message'] = 'No access rights in course context';
           $warnings[] = $warning;
           $result['warnings'] = $warnings;
           return $result;
        }

        if(!is_enrolled($context, $userid)) {
           $warning = array();
           $warning['item'] = 'course';
           $warning['itemid'] = $courseid;
           $warning['warningcode'] = '1';
           $warning['message'] = 'User not enrolled in course';
           $warnings[] = $warning;
           $result['warnings'] = $warnings;
           return $result;
        }

      try {
        $tasks = self::get_user_tasks(array($courseid));
        $utasks = reset($tasks["tasks"]);
        $quizes = $utasks["quizes"];
        $assignments = $utasks["assignments"];
        $DB->delete_records('monorail_data', array('datakey'=>"progress-".$userid, 'itemid'=>$courseid, 'type'=>'courseprogress'));
        foreach($quizes as $quiz) {
            if ($quizid && $quiz["id"] != $quizid) {
                continue;
            }

          //Quizes data stored in monorail data
          $DB->delete_records('monorail_data', array('datakey'=>"user" . $userid, 'itemid'=>$quiz["id"], 'type'=>'quiz_end'));
          $DB->delete_records('monorail_data', array('datakey'=>"user" . $userid, 'itemid'=>$quiz["id"], 'type'=>'quiz_attempt'));
          $DB->delete_records('monorail_data', array('datakey'=>"user" . $userid, 'itemid'=>$quiz["id"], 'type'=>'quiz_attempt_best'));
          $DB->delete_records('monorail_data', array('datakey'=>"user" . $userid, 'itemid'=>$quiz["id"], 'type'=>'quiz_attempts_count'));
        }

        if (!$quizid) {
            //get the context id / file info
            $mid = $DB->get_field('modules', 'id', array('name'=>'assign'));
            $fs = get_file_storage();
            foreach($assignments as $assign) {
              $cm = $DB->get_record('course_modules', array('course' => $courseid, 'instance' => $assign["id"], 'module'=>$mid), "*", MUST_EXIST);
              $submitinfo = $DB->get_record('assign_submission', array('assignment'=>$assign["id"], 'userid'=> $userid));

              if($submitinfo) {
                $DB->delete_records('assignsubmission_onlinetext', array('assignment'=>$assign["id"], 'submission'=> $submitinfo->id));
                $cm = $DB->get_record('course_modules', array('course' => $courseid, 'instance' => $assign["id"], 'module'=>$mid), "*", MUST_EXIST);
                $acontext = context_module::instance($cm->id, $userid);
                $files = $fs->get_area_files($acontext->id, 'assignsubmission_file', 'submission_files', $submitinfo->id, "timemodified", false);
                foreach ($files as $file) {
                  $cfile = monorail_converted_fileurl($file, true);
                  @unlink($cfile);
                  $file->delete();
                }
                $DB->delete_records('assignsubmission_file', array('assignment'=>$assign["id"], 'submission'=> $submitinfo->id));
                $DB->delete_records('assign_submission', array('assignment'=>$assign["id"], 'userid'=> $userid));
              }
                $DB->delete_records('assign_grades', array('assignment'=>$assign["id"], 'userid'=>$userid));
            }
        }
      } catch (Exception $ex) {
         $warning = array();
         $warning['item'] = 'course';
         $warning['itemid'] = $courseid;
         $warning['warningcode'] = '1';
         $warning['message'] = 'Exception :'.$ex->getMessage();
         $warnings[] = $warning;
      }

      if (!$quizid) {
          try {
            //Unset all grades
            require_once("$CFG->libdir/gradelib.php");
            grade_user_unenrol($courseid, $userid);
          } catch (Exception $ex) {
             $warning = array();
             $warning['item'] = 'course';
             $warning['itemid'] = $courseid;
             $warning['warningcode'] = '1';
             $warning['message'] = 'Exception :'.$ex->getMessage();
             $warnings[] = $warning;
          }
      }

      $result['warnings'] = $warnings;
      return $result;
    }

    public static function reset_tasks_returns() {
      return new external_single_structure(
          array(
              'warnings'  => new external_warnings()
          )
      );
    }

//Taken from moodle 2.5 calendar module, Some minor modifications for CBTEC custom changes
//To be replaced with original API's once moodle upgrade is done
    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.5
     */
    public static function delete_calendar_events_parameters() {
      return new external_function_parameters(
          array('events' => new external_multiple_structure(
              new external_single_structure(
                  array(
                      'eventid' => new external_value(PARAM_INT, 'Event ID', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
                      'repeat'  => new external_value(PARAM_BOOL, 'Delete comeplete series if repeated event')
                  ), 'List of events to delete'
              )
          )
          )
      );
    }
    
    /**
     * Delete Calendar events
     *
     * @param array $eventids A list of event ids with repeat flag to delete
     * @return null
     * @since Moodle 2.5
     */
    public static function delete_calendar_events($events) {
      global $CFG, $DB;
      require_once($CFG->dirroot."/calendar/lib.php");
    
      // Parameter validation.
      $params = self::validate_parameters(self:: delete_calendar_events_parameters(), array('events' => $events));
      try { 
        $transaction = $DB->start_delegated_transaction();
    
        foreach ($params['events'] as $event) {
          $eventobj = calendar_event::load($event['eventid']);
    
          // Let's check if the user is allowed to delete an event.
          if (!calendar_edit_event_allowed($eventobj)) {
            throw new moodle_exception("nopermissions");
          }
          // Time to do the magic.
          $eventobj->delete($event['repeat']);
        }
    
        // Everything done smoothly, let's commit.
        $transaction->allow_commit();
      } catch (Exception $ex) {
         add_to_log(1, 'monorailservices', 'externallib.delete_calendar_events', '', 'delete calendar events failed :'.$ex->getMessage());
      } 
      return null;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.5
     */
    public static function  delete_calendar_events_returns() {
      return null;
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters.
     * @since Moodle 2.5
     */
    public static function create_calendar_events_parameters() {
      // Userid is always current user, so no need to get it from client.
      // Module based calendar events are not allowed here. Hence no need of instance and modulename.
      // subscription id and uuid is not allowed as this is not an ical api.
      return new external_function_parameters(
          array('events' => new external_multiple_structure(
              new external_single_structure(
                  array(
                      'name' => new external_value(PARAM_TEXT, 'event name', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
                      'description' => new external_value(PARAM_RAW, 'Description', VALUE_DEFAULT, null, NULL_ALLOWED),
                      'location' => new external_value(PARAM_RAW, 'Location', VALUE_DEFAULT, null, NULL_ALLOWED),
                      'format' => new external_format_value('description', VALUE_DEFAULT),
                      'courseid' => new external_value(PARAM_INT, 'course id', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'groupid' => new external_value(PARAM_INT, 'group id', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'repeats' => new external_value(PARAM_INT, 'number of repeats', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'eventtype' => new external_value(PARAM_TEXT, 'Event type', VALUE_DEFAULT, 'user', NULL_NOT_ALLOWED),
                      'timestart' => new external_value(PARAM_TEXT, 'timestart', VALUE_DEFAULT, date(DATE_ISO8601, time()), NULL_NOT_ALLOWED),
                      'timeduration' => new external_value(PARAM_INT, 'time duration', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'modulename' => new external_value(PARAM_TEXT, 'module name', VALUE_OPTIONAL),
                      'instance' => new external_value(PARAM_INT, 'instance id',VALUE_OPTIONAL),
                      'visible' => new external_value(PARAM_INT, 'visible', VALUE_DEFAULT, 1, NULL_NOT_ALLOWED),
                      'sequence' => new external_value(PARAM_INT, 'sequence', VALUE_DEFAULT, 1, NULL_NOT_ALLOWED),
                      'cdata' => new external_multiple_structure(
                        new external_single_structure(
                          array(
                           "type" => new external_value(PARAM_TEXT, 'Data type',VALUE_OPTIONAL),
                           "datakey" => new external_value(PARAM_TEXT, 'Data Key',VALUE_OPTIONAL),
                           "value" => new external_value(PARAM_RAW, 'Data value',VALUE_OPTIONAL),
                           VALUE_OPTIONAL),"Custom monorail_data for the module if any" ,VALUE_OPTIONAL),VALUE_DEFAULT, array())
          ), 'event')
          )
          )
      );
    }

    /**
     * Create Calendar events.
     *
     * @param array $events A list of events to create.
     * @return array array of events created.
     * @since Moodle 2.5
     * @throws moodle_exception if user doesnt have the permission to create events.
     */
    public static function create_calendar_events($events) {
      global $CFG, $DB, $USER;
      require_once($CFG->dirroot."/calendar/lib.php");

      // Parameter validation.
      $params = self::validate_parameters(self::create_calendar_events_parameters(), array('events' => $events));

      $transaction = $DB->start_delegated_transaction();
      $return = array();
      $warnings = array();

      try {
        foreach ($params['events'] as $event) {

          // Let us set some defaults.
          $event['userid'] = $USER->id;
          if(!in_array('modulename',$event)) {
            $event['modulename'] = '';
          }
          if(!in_array('instance',$event)) {
            $event['instance'] = 0;
          }
          $event['subscriptionid'] = null;
          $event['uuid']= '';
          $event['format'] = external_validate_format($event['format']);
          if ($event['repeats'] > 0) {
            $event['repeat'] = 1;
          } else {
            $event['repeat'] = 0;
          }

          $event["timestart"] = strtotime($event["timestart"]);

          $eventobj = new calendar_event($event);

          // Let's check if the user is allowed to delete an event.
          if (!calendar_add_event_allowed($eventobj)) {
            $warnings [] = array('item' => $event['name'], 'warningcode' => 'nopermissions', 'message' => 'you do not have permissions to create this event');
            continue;
          }
          // Let's create the event.
          $var = $eventobj->create($event);
          $var = (array)$var->properties();
          if(in_array('cdata',$event) && ($event['instance'] > 0)) {
            foreach($event['cdata'] as $cdat) {
              $mdat = array();
              $mdat['type'] =  $cdat->type;
              $mdat['itemid'] =  $event['instance'];
              $mdat['datakey'] =  $cdat->datakey;
              $mdat['value'] =  $cdat->value;
              $DB->insert_record('monorail_data',$mdat);
            }
          }
          if ($event['repeat']) {
            $children = $DB->get_records('event', array('repeatid' => $var['id']));
            foreach ($children as $child) {
              $return[] = (array) $child;
            }
          } else {
            $return[] = $var;
          }

          if ($event["location"])
          {
            monorail_data("TEXT", $var["id"], "event_location", $event["location"]);
          }
        }
        // Everything done smoothly, let's commit.
        $transaction->allow_commit();
      } catch (Exception $ex) {
        add_to_log(1, 'monorailservices', 'externallib.create_calendar_events', '', 'ERROR: Failed to create calendar event: '.$ex->getMessage());
      }
      return array('events' => $return, 'warnings' => $warnings);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description.
     * @since Moodle 2.5
     */
    public static function  create_calendar_events_returns() {
      return new external_single_structure(
          array(
              'events' => new external_multiple_structure( new external_single_structure(
                  array(
                      'id' => new external_value(PARAM_INT, 'event id'),
                      'name' => new external_value(PARAM_TEXT, 'event name'),
                      'description' => new external_value(PARAM_RAW, 'Description', VALUE_OPTIONAL),
                      'format' => new external_format_value('description'),
                      'courseid' => new external_value(PARAM_INT, 'course id'),
                      'groupid' => new external_value(PARAM_INT, 'group id'),
                      'userid' => new external_value(PARAM_INT, 'user id'),
                      'repeatid' => new external_value(PARAM_INT, 'repeat id', VALUE_OPTIONAL),
                      'modulename' => new external_value(PARAM_TEXT, 'module name', VALUE_OPTIONAL),
                      'eventtype' => new external_value(PARAM_TEXT, 'Event type'),
                      'timestart' => new external_value(PARAM_INT, 'timestart'),
                      'timeduration' => new external_value(PARAM_INT, 'time duration'),
                      'visible' => new external_value(PARAM_INT, 'visible'),
                      'uuid' => new external_value(PARAM_TEXT, 'unique id of ical events', VALUE_OPTIONAL, '', NULL_NOT_ALLOWED),
                      'sequence' => new external_value(PARAM_INT, 'sequence'),
                      'timemodified' => new external_value(PARAM_INT, 'time modified'),
                      'subscriptionid' => new external_value(PARAM_INT, 'Subscription id', VALUE_OPTIONAL),
                  ), 'event')
              ),
              'warnings' => new external_warnings()
          )
      );
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters.
     * @since Moodle 2.5
     */
    public static function update_calendar_events_parameters() {
      // Userid is always current user, so no need to get it from client.
      // Module based calendar events are not allowed here. Hence no need of instance and modulename.
      // subscription id and uuid is not allowed as this is not an ical api.
      return new external_function_parameters(
          array('events' => new external_multiple_structure(
              new external_single_structure(
                  array(
                      'id' => new external_value(PARAM_INT, 'Event id'),
                      'name' => new external_value(PARAM_TEXT, 'event name', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
                      'description' => new external_value(PARAM_RAW, 'Description', VALUE_DEFAULT, null, NULL_ALLOWED),
                      'location' => new external_value(PARAM_RAW, 'Location', VALUE_DEFAULT, null, NULL_ALLOWED),
                      'format' => new external_format_value('description', VALUE_DEFAULT),
                      'courseid' => new external_value(PARAM_INT, 'course id', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'groupid' => new external_value(PARAM_INT, 'group id', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'repeats' => new external_value(PARAM_INT, 'number of repeats', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'eventtype' => new external_value(PARAM_TEXT, 'Event type', VALUE_DEFAULT, 'user', NULL_NOT_ALLOWED),
                      'timestart' => new external_value(PARAM_TEXT, 'timestart', VALUE_DEFAULT, date(DATE_ISO8601, time()), NULL_NOT_ALLOWED),
                      'timeduration' => new external_value(PARAM_INT, 'time duration', VALUE_DEFAULT, 0, NULL_NOT_ALLOWED),
                      'visible' => new external_value(PARAM_INT, 'visible', VALUE_DEFAULT, 1, NULL_NOT_ALLOWED),
                      'sequence' => new external_value(PARAM_INT, 'sequence', VALUE_DEFAULT, 1, NULL_NOT_ALLOWED),
          ), 'event')
          )
          )
      );
    }

    /**
     * Update Calendar events.
     *
     * @param array $events A list of events to update.
     * @return array array of events created.
     * @since Moodle 2.5
     * @throws moodle_exception if user doesnt have the permission to create events.
     */
    public static function update_calendar_events($events) {
      global $CFG, $DB, $USER;
      require_once($CFG->dirroot."/calendar/lib.php");

      // Parameter validation.
      $params = self::validate_parameters(self::update_calendar_events_parameters(), array('events' => $events));

      $transaction = $DB->start_delegated_transaction();
      $return = array();
      $warnings = array();

      foreach ($params['events'] as $event) {
        if(!$DB->record_exists('event',array('id' => $event['id']))) {
            continue;
        }
        $event['subscriptionid'] = null;
        $event['uuid']= '';
        $event['format'] = external_validate_format($event['format']);
        if ($event['repeats'] > 0) {
          $event['repeat'] = 1;
        } else {
          $event['repeat'] = 0;
        }

        $event["timestart"] = strtotime($event["timestart"]);

        $eventobj = new calendar_event($event);

        // Let's check if the user is allowed to update an event.
        if (!calendar_add_event_allowed($eventobj)) {
          $warnings [] = array('item' => $event['name'], 'warningcode' => 'nopermissions', 'message' => 'you do not have permissions to update this event');
          continue;
        }
        // Let's update the event.
        if(!$eventobj->update($event)) {
          add_to_log($event['id'], 'monorailservices', 'externallib', 'update_event', 'Error when trying update');
        }

        monorail_data("TEXT", $event["id"], "event_location", $event["location"]);
      }

      // Everything done smoothly, let's commit.
      $transaction->allow_commit();
      return array('warnings' => $warnings);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description.
     * @since Moodle 2.5
     */
    public static function  update_calendar_events_returns() {
      return new external_single_structure(
          array(
              'warnings' => new external_warnings()
          )
      );
    }
 //===============Calendar 2.5 Webservice API End ============================================//

//==== BIG BLUE BUTTON ===========================================================================//
   /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function delete_bigblue_events_parameters() {
     return new external_function_parameters(
        array(
          'eventid' => new external_value(PARAM_INT, 'Tool id to delete')
        )
      );
    }
    
    /**
     * Delete bigblue events
     *
     * @param array $eventids A list of event ids with repeat flag to delete
     * @return null
     * @since Moodle 2.3
     */
    public static function delete_bigblue_events($eventid) {
      global $DB, $CFG, $USER;

      // Parameter validation.
      $params = self::validate_parameters(self::delete_bigblue_events_parameters(), array('eventid' => $eventid));
      $warnings = array();
      require_once($CFG->dirroot . '/mod/bigbluebuttonbn/lib.php');
      require_once($CFG->dirroot . '/course/lib.php');
        try {
          $bbbinstance = $DB->get_record('bigbluebuttonbn', array('id' => $eventid), '*', MUST_EXIST); 
          $cm = $DB->get_record("course_modules", array("course" => $bbbinstance->course, "instance" => $eventid), '*', MUST_EXIST); 
          bigbluebuttonbn_delete_instance($cm->instance);
          $rval = delete_course_module($cm->id);
          delete_mod_from_section($cm->id, $cm->section);
          rebuild_course_cache($cm->course);

            //send update events
            $eventdata = new stdClass();
            $eventdata->modulename = "bigbluebuttonbn";
            $eventdata->name       = 'mod_deleted';
            $eventdata->cmid       = $cm->id;
            $eventdata->instance   = $cm->instance;
            $eventdata->courseid   = $bbbinstance->course;
            $eventdata->userid     = $USER->id;
            events_trigger("mod_deleted", $eventdata);

        } catch(Exception $e) { 
           $warning = array();
           $warning['item'] = 'course';
           $warning['itemid'] = $event["id"];
           $warning['warningcode'] = '1';
           $warning['message'] = 'Error '.$e->getMessage();
           $warnings[] = $warning;
        }
      return array('warnings' => $warnings);
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.5
     */
    public static function  delete_bigblue_events_returns() {
      return new external_single_structure(
          array(
              'warnings' => new external_warnings()
          )
      );
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters.
     * @since Moodle 2.3
     */
    public static function create_bigblue_events_parameters() {
      return new external_function_parameters(
          array('events' => new external_multiple_structure(
              new external_single_structure(
                  array(
                      'name' => new external_value(PARAM_TEXT, 'event name', VALUE_DEFAULT, "Virtual Class"),
                      'wait' => new external_value(PARAM_INT, 'Value if the user has to wait, either 1 or 0', VALUE_DEFAULT, 0),
                      'newwindow' => new external_value(PARAM_INT, 'In session needs to be opened in a new window', VALUE_DEFAULT, 1),
                      'allmoderators' => new external_value(PARAM_INT, 'IF all participants are moderators', VALUE_DEFAULT, 0),
                      'record' => new external_value(PARAM_INT, 'In session needs to be recorded', VALUE_DEFAULT, 0),
                      'description' => new external_value(PARAM_TEXT, 'Description', VALUE_DEFAULT, "Welcome to Live Session"),
                      'welcome' => new external_value(PARAM_TEXT, 'Description', VALUE_DEFAULT, "Welcome to Live Session"),
                      'course' => new external_value(PARAM_INT, 'course id'),
                      'timedue' => new external_value(PARAM_TEXT, 'End time'),
                      'timeavailable' => new external_value(PARAM_TEXT, 'time duration/Time start')
          ), 'event')
          )
          )
      );
    }

    /**
     * Create bigblue events.
     *
     * @param array $events A list of events to create.
     * @return array array of events created.
     * @since Moodle 2.3
     * @throws moodle_exception if user doesnt have the permission to create events.
     */
    public static function create_bigblue_events($events) {
      global $CFG, $DB, $USER;
     
      // Parameter validation.
      $params = self::validate_parameters(self::create_bigblue_events_parameters(), array('events' => $events));
      $return = array();
      $warnings = array();
      require_once($CFG->dirroot . '/mod/bigbluebuttonbn/locallib.php');
      require_once($CFG->dirroot . '/mod/bigbluebuttonbn/lib.php');
      require_once($CFG->dirroot . '/course/lib.php'); 
      $moduleid = $DB->get_field('modules', 'id', array('name'=> "bigbluebuttonbn")); 
      foreach($events as $event) { 
        try {
          $course = $DB->get_record('course', array('id' => $event["course"]), '*', MUST_EXIST);
          $context = context_course::instance($event["course"]);
          self::validate_context($context);
          require_capability('moodle/course:manageactivities', $context);

          //Add it to first section!
           $section = $DB->get_record_sql("SELECT id,section FROM {course_sections} WHERE course=?"
                        . " ORDER BY section LIMIT 1", array($event["course"]));
           $newcm = new stdClass();
           $newcm->course   = $event["course"];
           $newcm->module   = $moduleid;
           $newcm->instance = 0; // not known yet, will be updated later
           $newcm->visible  = 1;
           $newcm->section  = $section->id;
           $newcm->coursemodule  = 0;
           $newcm->added    = time();
           $newcmid = $DB->insert_record('course_modules', $newcm);

           $bbbinstance = new stdClass();
           $bbbinstance->name = $event["name"];
           $bbbinstance->welcome = $event["welcome"];
           $bbbinstance->timeavailable = strtotime($event["timeavailable"]);
           $bbbinstance->timedue = strtotime($event["timedue"]);
           $bbbinstance->wait = $event["wait"];
           $bbbinstance->course = $event["course"];
           $bbbinstance->description = $event["description"];
           $bbbinstance->record = 0;
           $bbbinstance->allmoderators = 0;
           $bbbinstance->newwindow = 1;

           $bbbid = bigbluebuttonbn_add_instance((object)$bbbinstance);

           $newcm->coursemodule = $bbbid;
           //Update the instance field in course_modules
           $DB->set_field('course_modules', 'instance', $newcm->coursemodule, array('id'=>$newcmid));
           //section info update
           $newcm->coursemodule  = $newcmid;
           $newcm->section = $section->section;
           add_mod_to_section($newcm);
           $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcm->coursemodule));
           set_coursemodule_visible($newcm->coursemodule, 1);
           rebuild_course_cache($event["course"]);
           $retval = array();
           $retval["id"] = $bbbid; 
           $retval["cmid"] = $newcmid; 
           $return[] = $retval;

            //send add events
            $eventdata = new stdClass();
            $eventdata->modulename = "bigbluebuttonbn";
            $eventdata->name       = 'mod_created';
            $eventdata->cmid       = $newcm->coursemodule;
            $eventdata->courseid   = $event["course"];
            $eventdata->userid     = $USER->id;
            events_trigger("mod_created", $eventdata);

        } catch(Exception $e) { 
           $warning = array();
           $warning['item'] = 'course';
           $warning['itemid'] = $event["course"];
           $warning['warningcode'] = '1';
           $warning['message'] = 'Error validating '.$e->getMessage();
           $warnings[] = $warning;
           continue;
        }  
      } 
      return array('events' => $return, 'warnings' => $warnings);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description.
     * @since Moodle 2.5
     */
    public static function  create_bigblue_events_returns() {
      return new external_single_structure(
          array(
              'events' => new external_multiple_structure( new external_single_structure(
                  array(
                      'id' => new external_value(PARAM_INT, 'BBB Instance id'),
                      'cmid' => new external_value(PARAM_INT, 'Course Module id'),
                  ), 'event')
              ),
              'warnings' => new external_warnings()
          )
      );
    }


    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters.
     * @since Moodle 2.5
     */
    public static function update_bigblue_events_parameters() {
      return new external_function_parameters(
          array('events' => new external_multiple_structure(
              new external_single_structure(
                  array(
                      'id' => new external_value(PARAM_INT, 'BBB Instance id'),
                      'name' => new external_value(PARAM_TEXT, 'event name', VALUE_REQUIRED, '', NULL_NOT_ALLOWED),
                      'wait' => new external_value(PARAM_INT, 'Value if the user has to wait, either 1 or 0', VALUE_DEFAULT, 1),
                      'newwindow' => new external_value(PARAM_INT, 'In session needs to be opened in a new window', VALUE_DEFAULT, 1),
                      'allmoderators' => new external_value(PARAM_INT, 'IF all participants are moderators', VALUE_DEFAULT, 0),
                      'record' => new external_value(PARAM_INT, 'In session needs to be recorded', VALUE_DEFAULT, 0),
                      'description' => new external_value(PARAM_TEXT, 'Description', VALUE_DEFAULT, "Welcome to Big Blue Live Session"),
                      'welcome' => new external_value(PARAM_TEXT, 'Description', VALUE_DEFAULT, "Welcome to Big Blue Live Session"),
                      'timedue' => new external_value(PARAM_TEXT, 'timestart'),
                      'timeavailable' => new external_value(PARAM_TEXT, 'time duration')
          ), 'event')
          )
          )
      );
    }

    /**
     * Update bigblue events.
     *
     * @param array $events A list of events to update.
     * @since Moodle 2.3
     * @throws moodle_exception if user doesnt have the permission to create events.
     */
    public static function update_bigblue_events($events) {
      global $DB, $CFG, $USER;
     
      // Parameter validation.
      $params = self::validate_parameters(self::update_bigblue_events_parameters(), array('events' => $events));
      $warnings = array();
      require_once($CFG->dirroot . '/mod/bigbluebuttonbn/lib.php');
      foreach($events as $event) { 
        try {
          $bbbins = $DB->get_record('bigbluebuttonbn', array('id' => $event["id"]), '*', MUST_EXIST); 
          $course = $DB->get_record('course', array('id' => $bbbins->course), '*', MUST_EXIST);
          $context = context_course::instance($course->id);
          self::validate_context($context);
          require_capability('moodle/course:manageactivities', $context);
          //Add it to first section!
          $bbbinstance = new stdClass();
          $bbbinstance->name = $event["name"];
          $bbbinstance->welcome = $event["welcome"];
          $bbbinstance->timeavailable = strtotime($event["timeavailable"]);
          $bbbinstance->timedue = strtotime($event["timedue"]);
          $bbbinstance->instance = $event["id"];
          $bbbinstance->wait = $event["wait"];
          $bbbinstance->course = $bbbins->course;
          $bbbinstance->description = $event["description"];
          $bbb = bigbluebuttonbn_update_instance((object)$bbbinstance);

            //send update events
            $eventdata = new stdClass();
            $eventdata->modulename = "bigbluebuttonbn";
            $eventdata->name       = 'mod_updated';
            $eventdata->cmid       = $event["id"];
            $eventdata->courseid   = $bbbins->course;
            $eventdata->userid     = $USER->id;
            events_trigger("mod_updated", $eventdata);

        } catch(Exception $e) { 
           $warning = array();
           $warning['item'] = 'course';
           $warning['itemid'] = $event["id"];
           $warning['warningcode'] = '1';
           $warning['message'] = 'Error validating '.$e->getMessage();
           $warnings[] = $warning;
        }
      }
      return array('warnings' => $warnings);
    }

    /**
     * Returns description of method result value.
     *
     * @return external_description.
     * @since Moodle 2.3
     */
    public static function  update_bigblue_events_returns() {
      return new external_single_structure(
          array(
              'warnings' => new external_warnings()
          )
      );
    }
    
     /**
    * Returns description of method parameters
    * @return external_function_parameters
    */
    public static function get_bigblue_events_parameters() {
     return new external_function_parameters(
       array(
         'courseid' => new external_value(PARAM_INT, 'Course Id')
       )
     );
    }

    /**
     * Return BBB events for the user
     * @param courseid Id of the course 
     * @return array of user BBB events
     */
    public static function get_bigblue_events($courseid) {
      global $USER, $DB, $CFG;
        
        $events = array();
        $warnings = array();
        try {
          $moduleid = $DB->get_field('modules', 'id', array('name'=> "bigbluebuttonbn"));
          $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
          $course_context = context_course::instance($courseid);
          if (is_enrolled($course_context, $USER->id)) {
            $modules = $DB->get_records('course_modules', array('course'=>$courseid, 'module'=>$moduleid));
            foreach($modules as $module) {
            try {
              $bbbrec = $DB->get_record('bigbluebuttonbn', array('id' => $module->instance), '*', MUST_EXIST);
              $event = array();
              $event["id"] = $module->instance;
              $event["name"] = $bbbrec->name;
              $event["welcome"] = $bbbrec->welcome;
              $event["description"] = $bbbrec->description;
              $event["timedue"] = date(DATE_ISO8601, $bbbrec->timedue);
              $event["timeavailable"] = date(DATE_ISO8601, $bbbrec->timeavailable);
              $events[] = $event;
           }  catch (Exception $ex) {
             $warning = array();
             $warning['item'] = 'course';
             $warning['itemid'] = $courseid;
             $warning['warningcode'] = '1';
             $warning['message'] = 'No access rights '.$ex->getMessage();
             $warnings[] = $warning;
             continue;
           }
            }
          }
        } catch (Exception $ex) { 
          $warning = array();
          $warning['item'] = 'course';
          $warning['itemid'] = $courseid;
          $warning['warningcode'] = '1';
          $warning['message'] = 'No access rights '.$ex->getMessage();
          $warnings[] = $warning;
        }
        return(array('events' => $events, 'warnings' => $warnings));
    }

    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters.
     * @since Moodle 2.5
     */
    public static function get_bigblue_events_returns() {
      return new external_function_parameters(
          array('events' => new external_multiple_structure(
              new external_single_structure(
                  array(
                      'id' => new external_value(PARAM_INT, 'BBB Instance id'),
                      'name' => new external_value(PARAM_TEXT, 'event name'),
                      'description' => new external_value(PARAM_TEXT, 'Description'),
                      'welcome' => new external_value(PARAM_TEXT, 'Description'),
                      'timedue' => new external_value(PARAM_TEXT, 'End Time'),
                      'timeavailable' => new external_value(PARAM_TEXT, 'Start time')
          ), 'event')),
              'warnings' => new external_warnings()
          )
      );
    }

    public static function get_invoices_parameters()
    {
        return new external_function_parameters(array(
            "cohortid" => new external_value(PARAM_INT, "Cohort id (if not 0, add cohort payment invoices)", VALUE_DEFAULT, 0)
        ));
    }

    public static function get_invoices($cohortid)
    {
        global $DB, $USER;

        $result = array();

        if ($cohortid) {
            // Show invoices only to cohort admins
            if ($DB->record_exists("monorail_cohort_admin_users", array("userid" => $USER->id, "cohortid" => $cohortid))) {
                $invoices = $DB->get_records_sql("SELECT * FROM {monorail_cohort_invoice} WHERE cohortid=? ORDER BY timeissued DESC", array($cohortid));

                foreach ($invoices as $inv) {
                    $invData = (array) $inv;
                    $invData["entries"] = array();

                    $entries = $DB->get_recordset_sql("SELECT * FROM {monorail_cohort_inv_entry} WHERE invoiceid=? ORDER BY id", array($inv->id));

                    foreach ($entries as $ent) {
                        $invData["entries"][] = (array) $ent;
                    }

                    $result[] = $invData;
                }
            }
        }

        return array("invoices" => $result);
    }

    public static function get_invoices_returns()
    {
        return new external_single_structure(array(
            "invoices" => new external_multiple_structure(new external_single_structure(array(
                "id" => new external_value(PARAM_INT, "Invoice id"),
                "invoicenum" => new external_value(PARAM_INT, "Invoice number"),
                "timeissued" => new external_value(PARAM_INT, "Invoice number"),
                "timepaid" => new external_value(PARAM_INT, "Invoice number"),
                "paymentstatus" => new external_value(PARAM_INT, "Invoice number"),
                "invoicetype" => new external_value(PARAM_INT, "Invoice number"),
                "quantity" => new external_value(PARAM_INT, "Invoice number"),
                "unitprice" => new external_value(PARAM_FLOAT, "Invoice number"),
                "totaltax" => new external_value(PARAM_FLOAT, "Invoice number"),
                "totalprice" => new external_value(PARAM_FLOAT, "Invoice number"),

                "entries" => new external_multiple_structure(new external_single_structure(array(
                    "usertype" => new external_value(PARAM_INT, "Item type (1 - Owner, 2 - admin, 3 - student)"),
                    "datefrom" => new external_value(PARAM_INT, "Date from"),
                    "dateto" => new external_value(PARAM_INT, "Date to"),
                    "quantity" => new external_value(PARAM_INT, "Quantity"),
                    "unitprice" => new external_value(PARAM_FLOAT, "Unit price"),
                    "totalprice" => new external_value(PARAM_FLOAT, "Total price")
                )))
            )))
        ));
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_cc_courses_parameters() {
    	return new external_function_parameters(array());
    }

    /**
     * Return CC courses
     */
    public static function get_cc_courses()
    {
        global $DB, $CFG;

        require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');

        $studentRoleIds = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN ('student')");

        if (empty($studentRoleIds)) {
            // No student roles?! A disaster!
            $studentRoleIds = array(1);
            error_log("No student role records found in role table!");
        }

        $publicCourses = $DB->get_records_sql("SELECT c.id AS id, mcd.code AS code, mcd.mainteacher AS mainteacher, c.fullname AS fullname, ".
                "c.startdate AS startdate, mcd.enddate AS enddate, COUNT(ra.roleid) AS students, c.category AS category, mcd.derivedfrom AS derivedfrom " .
            "FROM {monorail_course_data} AS mcd " .
                "INNER JOIN {course} AS c ON c.id=mcd.courseid " .
                "INNER JOIN {context} AS ctx ON ctx.instanceid=c.id AND ctx.contextlevel=50 " .
                "LEFT JOIN {role_assignments} AS ra ON ra.contextid=ctx.id AND ra.roleid IN (" . implode(", ", $studentRoleIds) . ") " .
                    "WHERE mcd.licence=2 GROUP BY id ORDER BY students DESC");

        $titleFieldId = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);
        $courses = array();

        foreach ($publicCourses as $course)
        {
            $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($course->id));
            $monorailData = array();

            foreach ($moreData as $md)
            {
                $monorailData[$md->datakey] = $md->value;
            }

            // Load background picture
            $backgroundimage = @$monorailData["coursebackground"];

            if (!$backgroundimage || $backgroundimage == "/") {
                $backgroundimage = $CFG->magic_ui_root . "/app/img/stock/other.jpg";
                $backgroundimagetn = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
            } else {
                try {
                    $backgroundimagetn = monorail_get_course_background_tn($backgroundimage, $courses[$id]->id);
                } catch (Exception $ex) {
                    // Don't fail completely if thumbnail doesn't exist.
                    $backgroundimagetn = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
                }
            }

            // Load course logo
            $course_logo = @$monorailData["course_logo"];
        
            if (!$course_logo || $course_logo == "/") {
                $course_logo = $CFG->magic_ui_root . "/app/img/logo-course-title.png";
            }

            if ($course->startdate) {
                $datestring = userdate($course->startdate, "%d %b %Y")." - ";
                if($course->enddate) {
                  $datestring .= userdate($course->enddate, "%d %b %Y");
                }
            } else {
                $datestring = get_string("self_paced", "theme_monorail");
            }

            $teacherData = array();

            foreach ($DB->get_recordset_sql("SELECT id, datakey, value FROM {monorail_data} WHERE itemid=?" .
                        " AND datakey IN ('pichash')", array($course->mainteacher)) as $item) {
                $teacherData[$item->datakey] = $item->value;
            }

            $teacherName = $DB->get_field_sql("SELECT CONCAT(firstname, ' ', lastname) FROM {user} WHERE id=?", array($course->mainteacher));
            $teacherTitle = get_string('label_teacher','theme_monorail');
            $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/default_32.jpg"; 

            // Teacher title
            if ($titleFieldId) {
                $title = $DB->get_field('user_info_data', 'data', array('userid' => $course->mainteacher, 'fieldid' => $titleFieldId), IGNORE_MULTIPLE);
                if ($title) {
                    $teacherTitle = $title;
                }
            }

            // Teacher logo.
            if (array_key_exists("pichash", $teacherData)) {
                $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/" . $teacherData["pichash"] . "_32.jpg";
            }

            $data = array(
                "id" => $course->id,
                "code" => $course->code,
                "category" => $course->category,
                "fullname" => $course->fullname,
                "students" => $course->students,
                'coursebackground' => $backgroundimage,
                'coursebackgroundtn' => $backgroundimagetn,
                'course_logo' => $course_logo,
                'durationstr' => $datestring,
                "stars" => (int) $monorailData["course_rating"],
                'teacher_name' => $teacherName,
                'teacher_title' => $teacherTitle,
                'teacher_avatar' => $teacherLogo,
                "licence" => 2,
                "cert_enabled" => (int) @$monorailData["cert_enabled"]
            );

            if ($course->derivedfrom) {
                $data["derivedfrom"] = $course->derivedfrom;

                $otherCourse = $DB->get_record_sql("SELECT c.id AS id, c.fullname AS fullname, mcd.code AS code " .
                    "FROM {course} AS c INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=c.id " .
                        "WHERE c.id=?", array($course->derivedfrom));

                $data["derivedfrom_name"] = $otherCourse->fullname;
                $data["derivedfrom_code"] = $otherCourse->code;
            }

            $courses[] = $data;
        }

        return array("courses" => $courses);
    }

    /**
     * Describes the return value for get_user_courses
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_cc_courses_returns() {
      return new external_single_structure(
          array(
              'courses'   => new external_multiple_structure(self::course_basic(), 'list of courses with basic info')
          )
      );
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_course_material_parameters() {
    	return new external_function_parameters(array(
            "courseid" => new external_value(PARAM_INT, "Course id", VALUE_DEFAULT, 0),
            "code" => new external_value(PARAM_TEXT, "Course code", VALUE_DEFAULT, "")
        ));
    }

    /**
     * Return course material.
     */
    public static function get_course_material($courseid, $code)
    {
        global $DB, $CFG, $USER;

        require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');

        $studentRoleIds = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN ('student')");

        if (empty($studentRoleIds)) {
            // No student roles?! A disaster!
            $studentRoleIds = array(1);
            error_log("No student role records found in role table!");
        }

        if ($courseid) {
            $whereSql = "c.id=?";
            $whereData = $courseid;
        } else if ($code) {
            $whereSql = "mcd.code=?";
            $whereData = $code;
        } else {
            throw new Exception("No way to identify course!");
        }

        $course = $DB->get_record_sql("SELECT c.id AS id, mcd.code AS code, mcd.mainteacher AS mainteacher, c.fullname AS fullname, ".
                "c.startdate AS startdate, mcd.enddate AS enddate, COUNT(ra.roleid) AS students, c.category AS category, mcd.licence AS licence, mcd.derivedfrom AS derivedfrom " .
            "FROM {monorail_course_data} AS mcd " .
                "INNER JOIN {course} AS c ON c.id=mcd.courseid " .
                "INNER JOIN {context} AS ctx ON ctx.instanceid=c.id AND ctx.contextlevel=50 " .
                "LEFT JOIN {role_assignments} AS ra ON ra.contextid=ctx.id AND ra.roleid IN (" . implode(", ", $studentRoleIds) . ") " .
                    "WHERE " . $whereSql, array($whereData));

        if (!$course || !$course->id) {
            throw new Exception("Course not found!");
        }

        if (!self::can_view_course($course->id, $USER->id)) {
            throw new Exception("Access denied!");
        }

        $titleFieldId = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);
        $moreData = $DB->get_records_sql("SELECT id, value, datakey FROM {monorail_data} WHERE itemid=?", array($course->id));
        $monorailData = array();

        foreach ($moreData as $md)
        {
            $monorailData[$md->datakey] = $md->value;
        }

        // Load background picture
        $backgroundimage = @$monorailData["coursebackground"];

        if (!$backgroundimage || $backgroundimage == "/") {
            $backgroundimage = $CFG->magic_ui_root . "/app/img/stock/other.jpg";
            $backgroundimagetn = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
        } else {
            try {
                $backgroundimagetn = monorail_get_course_background_tn($backgroundimage, $courses[$id]->id);
            } catch (Exception $ex) {
                // Don't fail completely if thumbnail doesn't exist.
                $backgroundimagetn = $CFG->magic_ui_root . "/app/img/stock/other-tn.jpg";
            }
        }

        // Load course logo
        $course_logo = @$monorailData["course_logo"];
    
        if (!$course_logo || $course_logo == "/") {
            $course_logo = $CFG->magic_ui_root . "/app/img/logo-course-title.png";
        }

        if ($course->startdate) {
            $datestring = userdate($course->startdate, "%d %b %Y")." - ";
            if($course->enddate) {
              $datestring .= userdate($course->enddate, "%d %b %Y");
            }
        } else {
            $datestring = get_string("self_paced", "theme_monorail");
        }

        $teacherData = array();

        foreach ($DB->get_recordset_sql("SELECT id, datakey, value FROM {monorail_data} WHERE itemid=?" .
                    " AND datakey IN ('pichash')", array($course->mainteacher)) as $item) {
            $teacherData[$item->datakey] = $item->value;
        }

        $teacherName = $DB->get_field_sql("SELECT CONCAT(firstname, ' ', lastname) FROM {user} WHERE id=?", array($course->mainteacher));
        $teacherTitle = get_string('label_teacher','theme_monorail');
        $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/default_32.jpg"; 

        // Teacher title
        if ($titleFieldId) {
            $title = $DB->get_field('user_info_data', 'data', array('userid' => $course->mainteacher, 'fieldid' => $titleFieldId), IGNORE_MULTIPLE);
            if ($title) {
                $teacherTitle = $title;
            }
        }

        // Teacher logo.
        if (array_key_exists("pichash", $teacherData)) {
            $teacherLogo = $CFG->wwwroot . "/public_images/user_picture/" . $teacherData["pichash"] . "_32.jpg";
        }

        $courseData = array(
            "id" => $course->id,
            "code" => $course->code,
            "category" => $course->category,
            "fullname" => $course->fullname,
            "students" => $course->students,
            'coursebackground' => $backgroundimage,
            'coursebackgroundtn' => $backgroundimagetn,
            'course_logo' => $course_logo,
            'durationstr' => $datestring,
            'startdate' => $course->startdate,
            'enddate' => $course->enddate,
            "stars" => (int) @$monorailData["course_rating"],
            'teacher_name' => $teacherName,
            'teacher_title' => $teacherTitle,
            'teacher_avatar' => $teacherLogo,
            "licence" => $course->licence,
            "keywords" => @$monorailData["keywords"],
            "language" => @$monorailData["language"],
            "course_country" => @$monorailData["course_country"],
            "cert_enabled" => (int)@$monorailData["cert_enabled"]
        );

        if ($course->derivedfrom) {
            $courseData["derivedfrom"] = $course->derivedfrom;

            $otherCourse = $DB->get_record_sql("SELECT c.id AS id, c.fullname AS fullname, mcd.code AS code " .
                "FROM {course} AS c INNER JOIN {monorail_course_data} AS mcd ON mcd.courseid=c.id " .
                    "WHERE c.id=?", array($course->derivedfrom));

            $courseData["derivedfrom_name"] = $otherCourse->fullname;
            $courseData["derivedfrom_code"] = $otherCourse->code;
        }

        return array("course" => $courseData);
    }

    /**
     * Describes the return value for get_user_courses
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_course_material_returns() {
        return new external_single_structure(
            array(
                'course'  => new external_single_structure( array(
                    'id'        => new external_value(PARAM_INT, 'course id'),
                    'category'        => new external_value(PARAM_INT, 'course category'),
                    'code' => new external_value(PARAM_TEXT, 'course code'),
                    'fullname'  => new external_value(PARAM_TEXT, 'course full name'),
                    'coursebackground' => new external_value(PARAM_TEXT, 'course background picture'),
                    'coursebackgroundtn' => new external_value(PARAM_TEXT, 'course background picture'),
                    'course_logo' => new external_value(PARAM_TEXT, 'course logo image', VALUE_OPTIONAL),
                    'durationstr' => new external_value(PARAM_TEXT, 'course duration string', VALUE_OPTIONAL),
                    'progress' => new external_value(PARAM_FLOAT, 'course progress for logged in student', VALUE_DEFAULT, 0),
                    'students' => new external_value(PARAM_INT, 'students count in course', VALUE_OPTIONAL),
                    'stars' => new external_value(PARAM_INT, 'stars given to course (-1 if not applicable)', VALUE_OPTIONAL),
                    'price' => new external_value(PARAM_FLOAT, 'course price (-1 if not applicable)', VALUE_OPTIONAL),
                    'course_access' => new external_value(PARAM_INT, 'course access', VALUE_OPTIONAL),
                    'course_privacy' => new external_value(PARAM_INT, 'course privacy', VALUE_OPTIONAL),
                    'can_edit' => new external_value(PARAM_INT, 'can edit?', VALUE_OPTIONAL),
                    'teacher_name' => new external_value(PARAM_TEXT, 'teacher name', VALUE_OPTIONAL),
                    'teacher_title' => new external_value(PARAM_TEXT, 'teacher title', VALUE_OPTIONAL),
                    'teacher_avatar' => new external_value(PARAM_TEXT, 'teacher avatar', VALUE_OPTIONAL),
                    'licence' => new external_value(PARAM_INT, 'course licence', VALUE_OPTIONAL),
                    'keywords' => new external_value(PARAM_TEXT, 'course keywords', VALUE_OPTIONAL),
                    'language' => new external_value(PARAM_TEXT, 'course language', VALUE_OPTIONAL),
                    'course_country' => new external_value(PARAM_TEXT, 'course country', VALUE_OPTIONAL),
                    'derivedfrom' => new external_value(PARAM_INT, 'Course derivedfrom', VALUE_OPTIONAL),
                    'derivedfrom_name' => new external_value(PARAM_TEXT, 'Course derivedfrom name', VALUE_OPTIONAL),
                    'derivedfrom_code' => new external_value(PARAM_TEXT, 'Course derivedfrom code', VALUE_OPTIONAL),
                    'cert_enabled' => new external_value(PARAM_INT, 'Certificates enabled for course', VALUE_OPTIONAL),
                    'startdate' => new external_value(PARAM_INT, 'start date timestamp', VALUE_OPTIONAL),
                    'enddate' => new external_value(PARAM_INT, 'end date timestamp', VALUE_OPTIONAL),
            ), 'Course basic information object' )));
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function delete_course_invite_parameters() {
    	return new external_function_parameters(array(
            "id" => new external_value(PARAM_INT, "Invitation id"),
        ));
    }

    /**
     * Return course material.
     */
    public static function delete_course_invite($id)
    {
        global $DB, $CFG, $USER;

        $invite = $DB->get_record_sql("SELECT iu.id AS id, i.targetid AS courseid " .
            "FROM {monorail_invite_users} AS iu " .
                "INNER JOIN {monorail_invites} AS i ON iu.monorailinvitesid=i.id " .
                    "WHERE iu.id=? AND i.type='course'", array($id));

        if (!$invite) {
            // No invite - no problem.
            return array();
        }

        $context = get_context_instance(CONTEXT_COURSE, $invite->courseid);

        try {
            self::validate_context($context);
        } catch (Exception $e) {
            $exceptionparam = new stdClass();
            $exceptionparam->message = $e->getMessage();
            $exceptionparam->courseid = $invite->courseid;
            throw new moodle_exception('errorcoursecontextnotvalid' , 'webservice', '', $exceptionparam);
        }

        $canEditCourse = (has_capability('moodle/course:manageactivities', $context)
                            || has_capability('moodle/course:update', $context)) ? 1 : 0;

        if (!$canEditCourse) {
            // No access rights.
            return array();
        }

        $DB->execute("DELETE FROM {monorail_invite_users} WHERE id=?", array($id));

        return array();
    }

    /**
     * Describes the return value for get_user_courses
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function delete_course_invite_returns() {
        return new external_single_structure(
            array(
                'ok' => new external_value(PARAM_INT, "Status", VALUE_DEFAULT, 1)
             ));
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function get_cert_orders_parameters() {
    	return new external_function_parameters(array());
    }

    /**
     * Return course material.
     */
    public static function get_cert_orders()
    {
        global $DB, $USER;

        $orders = array();

        $orderRecords = $DB->get_records_sql("SELECT id, userid, courseid, status, address, created FROM {monorail_cert_order} " .
            "WHERE userid=? ORDER BY created DESC", array($USER->id));

        foreach ($orderRecords as $o) {
            $ord = (array) $o;

            // TODO: get this data from somewhere...
            $ord["expected_delivery"] = strtotime("+3 weeks", $ord["created"]);

            $orders[] = $ord;
        }

        return array("orders" => $orders);
    }

    /**
     * Describes the return value for get_user_courses
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function get_cert_orders_returns() {
        return new external_single_structure(array(
            "orders" => new external_multiple_structure(new external_single_structure(array(
                "id" => new external_value(PARAM_INT, "Order id"),
                "userid" => new external_value(PARAM_INT, "User id"),
                "courseid" => new external_value(PARAM_INT, "Course id"),
                "status" => new external_value(PARAM_INT, "Order status (0 - created, 1 - paid, 2 - placed, 3 - processing, 4 - shipped, 5 - completed)"),
                "address" => new external_value(PARAM_TEXT, "Address"),
                "created" => new external_value(PARAM_INT, "Time create"),
                "expected_delivery" => new external_value(PARAM_INT, "Time of expected delivery")
            )))
        ));
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     * @since  Moodle 2.4
     */
    public static function add_cert_order_parameters() {
    	return new external_function_parameters(array(
            "courseid" => new external_value(PARAM_INT, "Course id"),
            "status" => new external_value(PARAM_INT, "Order status"),
            "address" => new external_value(PARAM_TEXT, "Address")
        ));
    }

    /**
     * Return course material.
     */
    public static function add_cert_order($courseid, $status, $address)
    {
        global $USER, $DB;

        $rec = new stdClass();

        $rec->userid = $USER->id;
        $rec->courseid = $courseid;
        $rec->status = $status;
        $rec->address = $address;
        $rec->created = time();

        $id = $DB->insert_record("monorail_cert_order", $rec);

        return array("id" => $id);
    }

    /**
     * Describes the return value for get_user_courses
     * @return external_single_structure
     * @since  Moodle 2.4
     */
    public static function add_cert_order_returns() {
        return new external_single_structure(
            array(
                "id" => new external_value(PARAM_INT, "Order id")
             ));
    }

    private static function update_public_profile_page($userId = null)
    {
        global $CFG, $USER, $DB;

        if (!$userId) {
            $USERID = $USER->id;
        } else {
            $USERID = $userId;
        }

        ob_start();

        require $CFG->dirroot . '/theme/monorail/templates/profile.phtml';

        $text = ob_get_contents();

        ob_end_clean();

        if (empty($text))
        {
            @unlink($CFG->external_data_root . "/../../users/" . $USERID . ".html");
        }
        else
        {
            file_put_contents($CFG->external_data_root . "/../../users/" . $USERID . ".html", $text);
        }
    }

    /* Get the real file name.
     *
     * NOTE: moodle doesn't want us to know it. Use with care (i.e. do not modify).
     * */
    private static function get_real_filename($file)
    {
        global $CFG;

        $contenthash = $file->get_contenthash();
        $l1 = $contenthash[0].$contenthash[1];
        $l2 = $contenthash[2].$contenthash[3];

        return $CFG->dataroot . "/filedir/$l1/$l2/$contenthash";
    }

    private static function can_view_course($courseid, $userid)
    {
        global $DB;

        if ($DB->get_field_sql("SELECT ra.id FROM {role_assignments} AS ra INNER JOIN {context} AS ctx ON ctx.id=ra.contextid AND ctx.contextlevel=50 " .
            "WHERE ra.userid=? AND ctx.instanceid=?", array($userid, $courseid))) {

            // User is enrolled to course.
            return true;
        }

        if ($DB->get_field_sql("SELECT licence FROM {monorail_course_data} WHERE courseid=?", array($courseid)) == 2) {
            // Course is free.
            return true;
        }

        return false;
    }

    private static function is_course_public($courseid)
    {
        global $DB;

        $cperm = monorail_get_course_perms($courseid);

        if (isset($cperm["course_privacy"]) && isset($cperm["course_access"]))
        {
            return $cperm["course_privacy"] == 1 && $cperm["course_access"] > 1;
        }
        else if ((int) $DB->get_field_sql("SELECT value FROM {monorail_data} WHERE itemid=? and datakey='public'", array($courseid)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static function get_file_type($filename)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $filename);
        finfo_close($finfo);

        return $type;
    }
}
