<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * A script to serve files from web service client
 *
 * @package    core_webservice
 * @copyright  2011 Dongsheng Cai <dongsheng@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * AJAX_SCRIPT - exception will be converted into JSON
 */
define('AJAX_SCRIPT', true);

/**
 * NO_MOODLE_COOKIES - we don't want any cookie
 */
define('NO_MOODLE_COOKIES', true);


require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot . '/webservice/lib.php');


//authenticate the user
$token = required_param('token', PARAM_ALPHANUM);
$webservicelib = new webservice();
$authenticationinfo = $webservicelib->authenticate_user($token);

//check the service allows file download
$enabledfiledownload = (int) ($authenticationinfo['service']->downloadfiles);
if (empty($enabledfiledownload)) {
    throw new moodle_exception('File downloading must be enabled in external service settings', 'monorailservice');
}

//finally we can serve the file :)
$fileparam = get_file_argument();
$fargs = explode("?", $fileparam);
$params=array();
foreach($fargs as $farg) {
    $keyval = explode("=", $farg);
    if(count($keyval) < 2) {
        throw new moodle_exception('missingargs', 'monorailservice');
    }
    $params[$keyval[0]] = $keyval[1];    
}

if((!array_key_exists('id', $params)) && (!array_key_exists('path', $params))) {
    throw new moodle_exception('missingargs', 'monorailservice path/id required');
}

$fs = get_file_storage();

if(array_key_exists('id', $params)) {
    if (!$file = $fs->get_file_by_id($params['id'])) {
        send_header_404();
        print_error('filenotfound', 'error', $CFG->wwwroot.'/my');
        return;
    }  
} else if(array_key_exists('path', $params)) {
    if (!$file = $fs->get_file_by_hash($params['path'])) {
        send_header_404();
        print_error('filenotfound', 'error', $CFG->wwwroot.'/my');
        return;
    }  
}

$forceDownload = true;
if(array_key_exists('forcedownload', $params)){
    $forceDownload = (bool)$params['forcedownload'];
}
send_stored_file($file, 0, 0, $forceDownload); 

