<?php
/**
 * Eliademy.com web services
 *
 * @package   monorail_services
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

    session_cache_limiter(false);

    require_once __DIR__ . '/../../../config.php';
    require_once __DIR__ . '/../cachelib.php';

    global $DB, $USER;

    try
    {
        require_login(null, false, null, false, true);
    }
    catch (Exception $ex)
    {
        echo json_encode(array("status" => "nouser"));
        exit(0);
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $fun = @$_POST["wsfunction"];
        $cancache = wscache_can_cache($fun);
    }
    else if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        $fun = @$_GET["wsfunction"];
        $cancache = wscache_can_cache($fun);

        // 0. Check etag.
        if (@$_SERVER["HTTP_IF_NONE_MATCH"] && $cancache)
        {
            $etag = explode(".", $_SERVER["HTTP_IF_NONE_MATCH"]);

            if (count($etag) == 2 && $etag[0] == $USER->id && wscache_cache_entry_valid($etag[1], $etag[0]))
            {
                // Client already has the same data.
                header("HTTP/1.1 304 Not Modified");
                header("Cache-Control: private, must-revalidate");
                header("ETag: " . $_SERVER["HTTP_IF_NONE_MATCH"]);
                exit(0);
            }
        }
    }
    else
    {
        // Invalid request method.
        exit(0);
    }

    // 1. Check cache.
    if ($cancache)
    {
        $argstring = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST")
        {
            foreach ($_POST as $key => $val)
            {
                if ($key != "wsfunction" && $key != "moodlewsrestformat")
                {
                    $argstring .= $key . serialize($val);
                }
            }
        }
        else
        {
            foreach ($_GET as $key => $val)
            {
                if ($key != "wsfunction" && $key != "moodlewsrestformat")
                {
                    $argstring .= $key . serialize($val);
                }
            }
        }

        $data = wscache_get_cached($fun, $USER->id, $argstring);

        // Client has no etag/invalid etag - serve data and new etag.

        if ($data !== FALSE && $data !== NULL)
        {
            header("Cache-Control: private, must-revalidate");
            header("ETag: " . $USER->id . "." . $data->id);
            header("Content-Length: " . mb_strlen($data->data, '8bit'));

            echo $data->data;
            exit(0);
        }
    }

    // 2. Check for magic backend implementation.
    // (!! if function has magic implementation, this file shouldn't even be called !!)

    // 3. Fall back to old monorail service.
    require_once __DIR__ . "/../db/services.php";
    require_once __DIR__ . "/../externallib.php";

    $MAGE_INST = null;

    function magento_get_instance()
    {
        global $MAGE_INST, $CFG;

        if (!$MAGE_INST)
        {
            try {
                require_once __DIR__ . "/../mageapi.php";

                $MAGE_INST = new MageApi($CFG->mage_api_url);

                if (!$MAGE_INST->connect($CFG->mage_api_user, $CFG->mage_api_key))
                {
                    $MAGE_INST = null;
                }
            } catch (Exception $e) {
                error_log("Unable to connect to magento: " . $e->getMessage());
                $MAGE_INST = null;
            }
        }

        return $MAGE_INST;
    }

    function magento_close_instance()
    {
        global $MAGE_INST;

        if ($MAGE_INST) {
            $MAGE_INST->commit();
            $MAGE_INST->disconnect();
        }

        $MAGE_INST = null;
    }

    class Undefined { }

    // http://stackoverflow.com/questions/16496554/can-php-detect-4-byte-encoded-utf8-chars
    function replace4byte($string)
    {
        return preg_replace('%(?:
              \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
            | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
            | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
        )%xs', '', $string);
    }

    function convertParams($param, &$paramInfo, &$data)
    {
        if ($param === null || array_key_exists($param, $data))
        {
            $data_ = ($param === null) ? $data : $data[$param];

            switch (get_class($paramInfo))
            {
                case "external_multiple_structure":
                    $res = [];

                    foreach (array_keys($data_) as $k1)
                    {
                        $res[] = convertParams($k1, $paramInfo->content, $data_);
                    }

                    return $res;

                case "external_single_structure":
                case "external_function_parameters":
                    $res = [];

                    foreach ($paramInfo->keys as $p1 => $i1)
                    {
                        $dat = convertParams($p1, $i1, $data_);

                        if (@get_class($dat) != "Undefined")
                        {
                            $res[$p1] = $dat;
                        }
                    }

                    return $res;

                case "external_value":
                    switch ($paramInfo->type)
                    {
                        case "int":
                            return (int) $data_;

                        case "float":
                            return (float) $data_;

                        case "text":
                        case "raw":
                            $oldEnc = mb_detect_encoding($data_, mb_detect_order(), true);
                            $newText = iconv($oldEnc, "UTF-8//TRANSLIT", $data_);

                            if ($newText !== FALSE) {
                                // Mysql doesn't support 4-byte unicode.
                                // No emoji, sorry...
                                return replace4byte($newText);
                            } else {
                                return iconv($oldEnc, "ASCII//IGNORE", $data_);
                            }

                        // XXX: other types?
                    }

                default:
                    return $data_;
            }
        }
        else
        {
            // 2 - optional, 0 - use default, 1 - not specified (still
            // using default - not catching all errors?).

            switch (get_class($paramInfo))
            {
                // default/required fields are confusing in
                // external_multiple_structure
                case "external_multiple_structure":
                    return is_array($paramInfo->default) ? $paramInfo->default : new Undefined();

                default:
                    switch ($paramInfo->required)
                    {
                        case 0:
                        case 1:
                            return $paramInfo->default;

                        default:
                            return new Undefined();
                    }
            }
        }
    }

    $meth = $functions[$fun]["methodname"];
    $paramMeth = $meth . "_parameters";
    $returnMeth = $meth . "_returns";

    $methParams = local_monorailservices_external::$paramMeth();
    $methReturns = local_monorailservices_external::$returnMeth();

    $args = array();

    foreach ($methParams->keys as $param => $paramInfo)
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST")
        {
            $arg = convertParams($param, $paramInfo, $_POST);
        }
        else
        {
            $arg = convertParams($param, $paramInfo, $_GET);
        }

        if (@get_class($arg) != "Undefined")
        {
            $args[] = $arg;
        }
        else
        {
            break;
        }
    }

    $backgroundCalls = array();

    /* Add a function to be called after the result has been sent to user.
     *
     * Do not try to access session here - it's already closed.
     * */
    function addBackgroundCall($call)
    {
        global $backgroundCalls;

        $backgroundCalls[] = $call;
    }

    try
    {
        $startTime = microtime(true);

        if ($cancache)
        {
            wscache_start_cached_function();
        }

        $ret = call_user_func_array(array("local_monorailservices_external", $meth), $args);

        $result = array("status" => "ok",
                        "result" => convertParams(null, $methReturns, $ret));

        $data = json_encode($result);

        header("Content-Length: " . mb_strlen($data, '8bit'));

        if ($cancache)
        {
            $cacheId = wscache_end_cached_function($fun, $USER->id, $argstring, $data);

            if ($cacheId)
            {
                header("Cache-Control: private, must-revalidate");
                header("ETag: " . $USER->id . "." . $cacheId);
            }
            else
            {
                header("Cache-Control: no-cache");
            }
        }
        else
        {
            header("Cache-Control: no-cache");
        }

        $totalTime = microtime(true) - $startTime;

        if ($totalTime > 3)
        {
            error_log("SLOW FUNCTION: " . $meth . " (" . $totalTime . "s. BOOO!)");
        }

        echo $data;
    }
    catch (Exception $ex)
    {
        if ($cancache)
        {
            wscache_cancel_cached_function();
        }

        $result = array("status" => "failed",
                        "message" => $ex->getMessage());

        error_log("WEB SERVICE CALL FAILED: " . $fun . ": " . $ex->getMessage() . " (user: " . $USER->username . ")");

        // Just in case...
        wscache_reset_by_user($USER->id);

        mail("aurelijus@cloudberrytec.com",
             "WEB SERVICE CALL FAILED: " . $fun . ": " . $ex->getMessage() . " (user: " . $USER->username . ")",
                "ARGS:\n" . var_export($args, true) .
                "\n\nSERVER: \n" . var_export($_SERVER, true) .
                "\n\n----------------------------------------------\nERROR:\n" . var_export($ex, true));

        header("Cache-Control: no-cache");

        echo json_encode($result);
    }

    if (!empty($backgroundCalls))
    {
        session_write_close();
        fastcgi_finish_request();
        try
        {
            foreach ($backgroundCalls as $call)
            {
                $call();
            }
        }
        catch (Exception $ex)
        {
            error_log("WEB SERVICE POST-CALL FAILED: " . $fun . ": " . $ex->getMessage() . " (user: " . $USER->username . ")");

            // Just in case...
            wscache_reset_by_user($USER->id);

            mail("aurelijus@cloudberrytec.com",
                 "WEB SERVICE POST-CALL FAILED: " . $fun . ": " . $ex->getMessage() . " (user: " . $USER->username . ")",
                    "ARGS:\n" . var_export($args, true) .
                    "\n\nSERVER: \n" . var_export($_SERVER, true) .
                    "\n\n----------------------------------------------\nERROR:\n" . var_export($ex, true));
        }
    }

    magento_close_instance();
