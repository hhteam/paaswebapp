<?php

function magento_page_must_not_exist($code)
{
    $url = 'https://eliademy.com/catalog/catalog/product/view/sku/' . $code;
    $file_headers = @get_headers($url);

    if ($file_headers[0] != 'HTTP/1.1 404 Not Found') {
        mail("aurelijus@eliademy.com,info@cloudberrytec.com", "MAGENTO ERROR", "Unable to update magento page: " . $url . " (delete or update directly in magento admin panel)");
    }
}

function magento_update_course($coursedata, $monorailData, $courseinfo)
{
    global $CFG;
    // Update the external course catalog database

    if (isset($CFG->mage_api_url))
    {
        $api = magento_get_instance();

        if ($api)
        {
            $catalogItem = $api->getProductBySku($coursedata->code);

            $privacy = (isset($monorailData["course_privacy"])) ? $monorailData["course_privacy"] : 0;
            $access = (isset($monorailData["course_access"])) ? $monorailData["course_access"] : 0;

            $api->begin();

            if (($privacy == 1) && ($access > 1))
            {
                $api->preloadCatalogData();
                $prod = $api->prepareCourseData($monorailData, $coursedata, $courseinfo);

                if (empty($catalogItem))
                {
                    magento_page_must_not_exist($coursedata->code);

                    // Adding new product to catalog.
                    $api->createProduct("virtual", $api->getAttributeSetId(), $coursedata->code, $prod["product"]);
                }
                else
                {
                    // Not changing these fields:
                    unset($prod["product"]["visibility"]);
                    unset($prod["product"]["websites"]);

                    // Updating categories.
                    if (empty($prod["product"]["categories"]))
                    {
                        // Categories not set - don't change anything.
                        unset($prod["product"]["categories"]);
                    }
                    else
                    {
                        foreach ($catalogItem["categories"] as $oldCat)
                        {
                            // Preserve special categories.
                            if (!in_array($oldCat, $CFG->mage_catmap))
                            {
                                // TODO: is special category is CC, and
                                // this course is not CC, then remove the
                                // special category CC...
                                $prod["product"]["categories"][] = $oldCat;
                            }
                        }
                    }

                    // Updating existing product.
                    $api->updateProduct($coursedata->code, $prod["product"]);

                    /*
                    $oldMedia = $api->getProductMediaList($catalogItem["product_id"]);

                    if (!empty($oldMedia))
                    {
                        foreach ($oldMedia as $media)
                        {
                            $api->deleteProductMedia($catalogItem["product_id"], $media["file"]);
                        }
                    }
                    */
                }

                try
                {
                    $api->createProductMedia($coursedata->code, $prod["course_logo"], "logo", "course_logo", 100);
                    $api->createProductMedia($coursedata->code, $prod["course_background"], "background", "course_background", 100);
                    $api->createProductMedia($coursedata->code, $prod["course_backgroundtn"], "backgroundtn", "course_backgroundtn", 100);

                    if (@$prod["teacher_avatar"])
                    {
                        $api->createProductMedia($coursedata->code, $prod["teacher_avatar"], "teacher", "teacher_avatar", 100);
                    }
                }
                catch (Exception $e)
                {
                    add_to_log(1, 'monorail', 'externallib.update_course', '', "Unable to set catalog image: " . $e->getMessage());
                }
            }
            else
            {
                if (!empty($catalogItem))
                {
                    // Removing item from catalog.
                    $api->deleteProduct($catalogItem["product_id"]);
                } else {
                    magento_page_must_not_exist($coursedata->code);
                }
            }
        }
    }
}

class MageApi
{
    private $client;
    private $session;
    private $valid;
    private $transMode;
    private $transCmd;

    private $attrSet = null;
    private $stores = array();

    public function __construct($url)
    {
        $this->transMode = false;

        try
        {
            $this->client = new SoapClient($url . 'soap/?wsdl');
            $this->valid = true;
        }
        catch (SoapFault $e)
        {
            $this->valid = false;
        }
    }

    // Connect to magento API
    public function connect($user, $key)
    {
        if (!$this->valid)
        {
            return false;
        }

        try
        {
            $this->session = $this->client->login($user, $key);
        }
        catch (SoapFault $e)
        {
            return false;
        }

        return true;
    }

    // Disconnect from magento API
    public function disconnect()
    {
        if (!$this->valid)
        {
            return;
        }

        $this->client->endSession($this->session);
    }

    // Preload data required for catalog operations.
    public function preloadCatalogData()
    {
        if (!$this->valid)
        {
            return;
        }

        // TODO: store data from magento as monorail data to speed up
        // operations.

        foreach ($this->client->call($this->session, "catalog_product_attribute_set.list") as $attr)
        {
            if ($attr["name"] == "COURSE")
            {
                $this->attrSet = $attr["set_id"];
                break;
            }
        }

        if ($this->attrSet === null)
        {
            throw new Exception("No attribute set COURSE.");
        }

        foreach ($this->client->call($this->session, "store.list") as $store)
        {
            $this->stores[$store["code"]] = $store;
        }

        if (!array_key_exists("default", $this->stores))
        {
            throw new Exception("default store not found.");
        }
    }

    // Return id of course product attribute set.
    public function getAttributeSetId()
    {
        return $this->attrSet;
    }

    public function prepareCourseData($monorailData, $courseData, $course)
    {
        global $CFG, $DB;

        // Course background
        $background = @$monorailData["coursebackground"];
        $pos = strrpos($background, "/");

        if ($pos !== FALSE)
        {
            $background = substr($background, $pos + 1);
        }

        $pos = strpos($background, "?");

        if ($pos !== FALSE)
        {
            $background = substr($background, 0, $pos);
        }

        if (empty($background))
        {
            $background = "other.jpg";
        }

        if (file_exists($CFG->magic_ui_dir . "/app/img/stock/" . $background))
        {
            $background = $CFG->magic_ui_dir . "/app/img/stock/" . $background;
        }
        else if (file_exists($CFG->dirroot . "/public_images/course_logo/" . $background))
        {
            $background = $CFG->dirroot . "/public_images/course_logo/" . $background;
        }
        else
        {
            $background = $CFG->magic_ui_dir . "/app/img/stock/other.jpg";
        }

        $backgroundtn = monorail_get_course_background_tn($background,$course->id, true);
        if(!file_exists($backgroundtn)) {
          $backgroundtn = $background;
        }
        // Course logo
        $course_logo = @$monorailData["course_logo"];

        $pos = strrpos($course_logo, "/");

        if ($pos !== FALSE)
        {
            $course_logo = substr($course_logo, $pos + 1);
        }

        $pos = strpos($course_logo, "?");

        if ($pos !== FALSE)
        {
            $course_logo = substr($course_logo, 0, $pos);
        }

        $cohort = $DB->get_records('monorail_cohort_courseadmins', array('courseid'=>$course->id), null, 'cohortid');
        if (count($cohort)) {
            $cohort = reset($cohort);
        } else {
            $cohort = null;
        }

        if (!empty($course_logo) && file_exists($CFG->dirroot . "/public_images/course_logo/" . $course_logo))
        {
            $course_logo = $CFG->dirroot . "/public_images/course_logo/" . $course_logo;
        }
        else if (empty($course_logo) && ! empty($cohort) && file_exists($CFG->dirroot . "/public_images/course_logo/coh" . $cohort->cohortid. ".png"))
        {
            // use cohort default
            $course_logo = $CFG->dirroot . "/public_images/course_logo/coh" . $cohort->cohortid . ".png";
        }
        else
        {
            $course_logo = $CFG->magic_ui_dir . "/app/img/logo-course-title.png";
        }

        $cats = array();

        if (array_key_exists($course->category, $CFG->mage_catmap))
        {
            $cats[] = $CFG->mage_catmap[$course->category];
        }

        if (@$courseData->licence == 2 && $CFG->mage_cc_category) {
            $cats[] = $CFG->mage_cc_category;
        }

        $attribs = array();

        // Main teacher
        if (isset($courseData->mainteacher) && $courseData->mainteacher)
        {
            $teacher = $DB->get_record_sql("SELECT firstname, lastname, description FROM {user} WHERE id=?", array($courseData->mainteacher));
            $teacherData = array();

            foreach ($DB->get_recordset_sql("SELECT datakey, value FROM {monorail_data} WHERE itemid=?" .
                " AND datakey IN ('pichash')", array($courseData->mainteacher)) as $item)
            {
                $teacherData[$item->datakey] = $item->value;
            }

            $attribs["teacher_id"] = $courseData->mainteacher;
            $attribs["teacher"] = $teacher->firstname . " " . $teacher->lastname;
            $attribs["teacher_title"] = "";
            $attribs["teacher_bio"] = $teacher->description;

            $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);

            if($fieldid) {
                $title = $DB->get_field('user_info_data', 'data', array('userid'=>$courseData->mainteacher, 'fieldid' => $fieldid), IGNORE_MULTIPLE);
                if($title) {
                    $attribs["teacher_title"] = $title;
                }
            }

            // Teacher logo (XXX: pichash should be enough - update all
            // places in magneto).
            if (array_key_exists("pichash", $teacherData))
            {
                $teacher_logo = $CFG->dirroot . "/public_images/user_picture/" . $teacherData["pichash"] . "_90.jpg";
                $attribs["teacher_pichash"] = $teacherData["pichash"];
            }
            else
            {
                $teacher_logo = null;
                $attribs["teacher_pichash"] = "default";
            }

            $soclinks = array();
            $slinks = $DB->get_records_sql("SELECT id, social, value FROM {monorail_social_settings} WHERE datakey='accounturl' AND userid=?", array($courseData->mainteacher));

            foreach ($slinks as $sl)
            {
                $soclinks[] = array("type" => $sl->social, "link" => $sl->value);
            }

            $attribs["teacher_social_links"] = json_encode($soclinks);
        }
        else
        {
            $teacher_logo = null;
        }

        $userCount = $this->courseUserCount($course->id);
        $summary = $DB->get_record_sql("SELECT summary FROM {course_sections} WHERE section=0 AND course=?", array($course->id));

        if (isset($summary->summary) && !empty($summary->summary))
        {
            $summary_html = monorail_export_attachments($summary->summary);
        }
        else
        {
            $summary_html = "";
        }

        $topics = array();
        $sections = $DB->get_records_sql("SELECT id, name FROM {course_sections} WHERE course=? AND visible AND section>0 ORDER BY section ASC",
            array($course->id));

        foreach ($sections as &$sect)
        {
            $topics[] = $sect->name;
        }

        $courseinvites = monorail_get_or_create_invitation('course', $course->id);

        $attribs["participants"] = $userCount;
        $attribs["meta_keyword"] = @$monorailData["keywords"];
        $attribs["meta_title"] = $course->fullname;
        $attribs["enrollment_active"] = $courseinvites->active;
        $attribs["topic_list"] = json_encode($topics);
        $attribs["course_language"] = @$monorailData["language"];
        $attribs["course_completed"] = $courseData->status ? 0 : 1;
        $attribs["course_cert"] = (int)@$monorailData["cert_enabled"];

        if (@$courseData->licence == 2) {
            $attribs["course_licence"] = "2";

            if (@$courseData->derivedfrom) {
                $otherCourse = $DB->get_record_sql("SELECT c.id AS id, c.fullname AS fullname, mcd.code AS code " .
                    "FROM {course} AS c INNER JOIN {monorail_course_data} AS mcd ON c.id=mcd.courseid " .
                        "WHERE c.id=?", array($courseData->derivedfrom));

                $attribs["course_derivedfrom_name"] = $otherCourse->fullname;
                $attribs["course_derivedfrom_code"] = $otherCourse->code;
            } else {
                $attribs["course_derivedfrom_name"] = "";
                $attribs["course_derivedfrom_code"] = "";
            }
        } else {
            $attribs["course_licence"] = "";
            $attribs["course_derivedfrom_name"] = "";
            $attribs["course_derivedfrom_code"] = "";
        }

        // Is course invitations only?
        $access = (isset($monorailData["course_access"])) ? $monorailData["course_access"] : 0;
        $privacy = (isset($monorailData["course_privacy"])) ? $monorailData["course_privacy"] : 0;
        $attribs["invitations_only"] = (($privacy == 1) && ($access == 2)) ? 1:0 ;

        return array (
            "product" => array (
                "name" => $course->fullname,
                "meta_title" => $course->fullname,
                "websites" => array($this->stores["default"]["website_id"]),
                "description" => $summary_html,
                "short_description" => $summary_html,
                "meta_description" => $summary_html,
                "categories" => $cats,
                "status" => "1",
                "visibility" => "4",
                "tax_class_id" => "1",
                "price" => (float) @$monorailData["course_price"],
                "additional_attributes" => array("single_data" => $attribs),
                "stock_data" => array("is_in_stock" => "1", "qty" => "1000", "manage_stock" => "1")
            ),
            "course_background" => $background,
            "course_backgroundtn" => $backgroundtn,
            "course_logo" => $course_logo,
            "teacher_avatar" => $teacher_logo
        );
    }

    public function getProductList()
    {
        if (!$this->valid)
        {
            return;
        }

        return $this->client->call($this->session, "catalog_product.list");
    }

    public function getProductBySku($sku)
    {
        if (!$this->valid)
        {
            return null;
        }

        try
        {
            $info = $this->client->call($this->session, "catalog_product.info", $sku);

            return $info;
        }
        catch (Exception $e)
        {
            return null;
        }
    }

    public function getProductMediaList($id)
    {
        if (!$this->valid)
        {
            return;
        }

        return $this->client->call($this->session, "catalog_product_attribute_media.list", $id);
    }

    // Update count of course participants.
    public function updateParticipants($courseId)
    {
        if (!$this->valid)
        {
            return;
        }

        global $DB;

        $courseCode = $DB->get_field("monorail_course_data", "code", array("courseid" => $courseId));

        try
        {
            $info = $this->client->call($this->session, "catalog_product.info", $courseCode);

            if (empty($info))
            {
                // Course not in catalog.
                return;
            }
        }
        catch (Exception $e)
        {
            return;
        }

        $userCount = $this->courseUserCount($courseId);
        $data = array("additional_attributes" => array("single_data" => array("participants" => $userCount)));

        if ($this->transMode)
        {
            $this->transCmd[] = array("catalog_product.update", array($courseCode, $data));
        }
        else
        {
            $this->client->call($this->session, "catalog_product.update", array($courseCode, $data));
        }
    }

    /* For all courses where user is main teacher, update teacher
     * name/bio/picture/... (better run this inside transaction)
     *
     * Returns: true if at least 1 course was updated, otherwise false.
     * */
    public function updateCourseTeacherInfo($userId)
    {
        global $DB;

        $changesMade = false;

        $courses = $DB->get_records_sql("SELECT mcd.courseid AS id, mcd.code AS code FROM {monorail_course_data} AS mcd " .
            "INNER JOIN {monorail_course_perms} AS mcp ON mcp.course=mcd.courseid " .
                "WHERE mcp.privacy=1 AND mcp.caccess>1 AND mcd.mainteacher=?", array($userId));

        if (!empty($courses))
        {
            // Have at least 1 course to update - collect user data.
            $attribs = array();

            $user = $DB->get_record_sql("SELECT firstname, lastname, description FROM {user} WHERE id=?", array($userId));

            $attribs["teacher_id"] = $userId;
            $attribs["teacher"] = $user->firstname . " " . $user->lastname;
            $attribs["teacher_title"] = "";
            $attribs["teacher_bio"] = $user->description;

            $fieldid = $DB->get_field('user_info_field', 'id', array('shortname'=>'usertitle'), IGNORE_MULTIPLE);

            if ($fieldid)
            {
                $title = $DB->get_field('user_info_data', 'data', array('userid'=>$userId, 'fieldid' => $fieldid), IGNORE_MULTIPLE);

                if ($title)
                {
                    $attribs["teacher_title"] = $title;
                }
            }

            $attribs["teacher_pichash"] = $DB->get_field_sql("SELECT value FROM {monorail_data} WHERE datakey='pichash' AND itemid=?", array($userId));

            $soclinks = array();
            $slinks = $DB->get_records_sql("SELECT id, social, value FROM {monorail_social_settings} WHERE datakey='accounturl' AND userid=?", array($userId));

            foreach ($slinks as $sl)
            {
                $soclinks[] = array("type" => $sl->social, "link" => $sl->value);
            }

            $attribs["teacher_social_links"] = json_encode($soclinks);

            // Update all courses.
            foreach ($courses as $course)
            {
                try
                {
                    $info = $this->client->call($this->session, "catalog_product.info", $course->code);

                    if (empty($info))
                    {
                        // Course not in catalog.
                        continue;
                    }
                }
                catch (Exception $e)
                {
                    continue;
                }

                $data = array("additional_attributes" => array("single_data" => $attribs));

                if ($this->transMode)
                {
                    $this->transCmd[] = array("catalog_product.update", array($course->code, $data));
                }
                else
                {
                    $this->client->call($this->session, "catalog_product.update", array($course->code, $data));
                }

                $changesMade = true;
            }
        }

        return $changesMade;
    }

    public function updateCourseStatus($courseId)
    {
        if (!$this->valid)
        {
            return;
        }

        global $DB;

        $courseData = $DB->get_record_sql("SELECT id, code, status FROM {monorail_course_data} WHERE courseid=?", array($courseId));

        try
        {
            $info = $this->client->call($this->session, "catalog_product.info", $courseData->code);

            if (empty($info))
            {
                // Course not in catalog.
                return;
            }
        }
        catch (Exception $e)
        {
            return;
        }

        $data = array("additional_attributes" => array("single_data" => array("course_completed" => $courseData->status ? 0 : 1)));

        if ($this->transMode)
        {
            $this->transCmd[] = array("catalog_product.update", array($courseData->code, $data));
        }
        else
        {
            $this->client->call($this->session, "catalog_product.update", array($courseData->code, $data));
        }
    }

    // Starts transaction mode (consequent state altering calls are
    // executed only if commit is called - for speed and consistency).
    public function begin()
    {
        if (!$this->transMode) {
            $this->transMode = true;
            $this->transCmd = array();
        }
    }

    public function commit()
    {
        if ($this->transMode)
        {
            if (!empty($this->transCmd) && $this->valid)
            {
                try
                {
                    $this->client->multiCall($this->session, $this->transCmd);
                }
                catch (Exception $e)
                {
                    error_log("magento commit error: " . $e->getMessage());
                }
            }

            $this->transMode = false;
            $this->transCmd = array();
        }
    }

    public function rollback()
    {
        if ($this->transMode) {
            $this->transMode = false;
            $this->transCmd = array();
        }
    }

    // http://www.magentocommerce.com/api/soap/catalog/catalogProduct/catalog_product.create.html
    public function createProduct($type, $set, $sku, $data)
    {
        if (!$this->valid)
        {
            return;
        }

        if ($this->transMode)
        {
            $this->transCmd[] = array("catalog_product.create", array($type, $set, $sku, $data));
        }
        else
        {
            $this->client->call($this->session, "catalog_product.create", array($type, $set, $sku, $data));
        }
    }

    // http://www.magentocommerce.com/api/soap/catalog/catalogProduct/catalog_product.update.html
    public function updateProduct($sku, $data)
    {
        if (!$this->valid)
        {
            return;
        }

        if ($this->transMode)
        {
            $this->transCmd[] = array("catalog_product.update", array($sku, $data));
        }
        else
        {
            $this->client->call($this->session, "catalog_product.update", array($sku, $data));
        }
    }

    // http://www.magentocommerce.com/api/soap/catalog/catalogProduct/catalog_product.delete.html
    public function deleteProduct($id)
    {
        if (!$this->valid)
        {
            return;
        }

        // TODO: delete media

        if ($this->transMode)
        {
            $this->transCmd[] = array("catalog_product.delete", $id);
        }
        else
        {
            $this->client->call($this->session, "catalog_product.delete", $id);
        }
    }

    public function rateProduct($sku, $stars, $review, $username, $userid)
    {
        if (!$this->valid)
        {
            return;
        }

        if ($this->transMode)
        {
            $this->transCmd[] = array("rating.info", array($sku, $stars, $review, $username, $userid));
        }
        else
        {
            $this->client->call($this->session, "rating.info", array($sku, $stars, $review, $username, $userid));
        }
    }

    // http://www.magentocommerce.com/api/soap/catalog/catalogProductAttributeMedia/catalog_product_attribute_media.create.html
    public function createProductMedia($sku, $fileName, $label, $type, $position)
    {
        global $CFG;

        if (!$this->valid)
        {
            return;
        }

        copy($fileName, $CFG->external_data_root . "/../../extimg/" . $sku . "_course_" . $label . ".jpg");
    }

    public function deleteProductMedia($id, $file)
    {
        if (!$this->valid)
        {
            return;
        }

        // TODO: delete files.

        if ($this->transMode)
        {
            $this->transCmd[] = array("catalog_product_attribute_media.remove", array("product" => $id, "file" => $file));
        }
        else
        {
            $this->client->call($this->session, "catalog_product_attribute_media.remove", array("product" => $id, "file" => $file));
        }
    }

    private function courseUserCount($courseid)
    {
        global $DB;

        $interestingRoleIds = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN " .
            "('student', 'coursecreator', 'editingteacher', 'teacher')");

        if (empty($interestingRoleIds)) {
            // No interesting roles???
            $interestingRoleIds = array(0);
        }

        $boringRoleIds = $DB->get_fieldset_sql("SELECT id FROM {role} WHERE shortname IN ('manager')");

        if (empty($boringRoleIds)) {
            // No roles???
            $boringRoleIds = array(0);
        }

        $userCount = $DB->get_fieldset_sql("SELECT ra.userid AS id " .
            "FROM {role_assignments} AS ra " .
                "INNER JOIN {context} AS ctx ON ra.contextid=ctx.id AND ctx.contextlevel=50 " .
                    "WHERE ra.roleid IN (" . implode(", ", $interestingRoleIds) . ") AND ctx.instanceid=?",
                        array($courseid));

        $userDontCount = $DB->get_fieldset_sql("SELECT ra.userid AS id " .
            "FROM {role_assignments} AS ra " .
                "INNER JOIN {context} AS ctx ON ra.contextid=ctx.id AND ctx.contextlevel=50 " .
                    "WHERE ra.roleid IN (" . implode(", ", $boringRoleIds) . ") AND ctx.instanceid=?",
                        array($courseid));

        $userCount = array_diff($userCount, $userDontCount);
        $userCount = array_unique($userCount);

        return count($userCount);
    }
}
