<?php
/**
 * Eliademy.com web services
 * 
 * @package   monorail_services
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


defined('MOODLE_INTERNAL') || die;

global $CFG;

require_once($CFG->dirroot . '/theme/monorail/lib.php');
require_once($CFG->dirroot . '/local/monorailfeed/lib.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot . '/lib/filestorage/zip_packer.php');


// Thanks to Chris Lewis in this thread: https://moodle.org/mod/forum/discuss.php?d=196596
function monorailservices_restore_from_file($filepath) {
    global $CFG, $USER; 
    
    //Unzip the backup
    $rand = 2;
    while(strlen($rand)<10) { $rand = '0'.$rand; }
    $rand .= rand();
    check_dir_exists($CFG->dataroot . '/temp/backup');
    $nmuext = new zip_packer;
    $nmuext->extract_to_pathname($filepath, $CFG->dataroot . '/temp/backup/' . $rand);
    if (file_exists($CFG->dataroot . '/temp/backup/' . $rand . '/course/course.xml')) {
        // Moodle 2.0+
		$xml = simplexml_load_file($CFG->dataroot . '/temp/backup/' . $rand . '/course/course.xml');
		$shortname = (string)$xml->shortname;
		$fullname = (string)$xml->fullname;
    } else if (file_exists($CFG->dataroot . '/temp/backup/' . $rand . '/moodle.xml')) {
        // Moodle 1.9
        $xml = simplexml_load_file($CFG->dataroot . '/temp/backup/' . $rand . '/moodle.xml');
        $shortname = (string)$xml->COURSE->HEADER->SHORTNAME;
        $fullname = (string)$xml->COURSE->HEADER->FULLNAME;
    }
    if (isset($fullname)) {
		$categoryid = 15;
		// Create new course
        add_to_log(1, 'monorailservices', 'monorailservices_restore_from_file', '', 'Fullname: '.$fullname.' / Shortname: '.$shortname);
		$courseid = restore_dbops::create_new_course($fullname, $shortname, $categoryid);
        add_to_log(1, 'monorailservices', 'monorailservices_restore_from_file', '', 'Created courseid '.$courseid);
		// Restore backup into course
		$controller = new restore_controller($rand, $courseid,
				backup::INTERACTIVE_NO, backup::MODE_IMPORT, 2,
				backup::TARGET_NEW_COURSE);
        $loggercolumns = array('userid'=>$USER->id, 'course'=>$courseid, 'module'=>'monorailservices', 'ip'=>getremoteaddr(), 'cmid'=>0, 'url'=>'');
		$controller->get_logger()->set_next(new database_logger(backup::LOG_INFO, 'time', 'action', 'info', 'log', $loggercolumns));
        if ($controller->get_status() == backup::STATUS_REQUIRE_CONV) {
            $controller->convert();
        }
		$controller->execute_precheck();
		$controller->execute_plan();
        return $courseid;
	} else {
		//~ exit('Failed to open course.xml.');
        return 0;
	}
}

function monorailservices_finalize_course($courseid) {
    global $DB, $CFG, $USER;
    
    $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
    // TODO put in monorailservices lib
    // Create a unique code for the course
    $codeData = new stdClass();
    $codeData->courseid = $courseid;
    do
    {
        $codeData->code = substr(md5($codeData->courseid . rand()), 0, 10);
    }
    while ($DB->record_exists("monorail_course_data", array("code" => $codeData->code)));
    $codeData->enddate = 0;
    $codeData->mainteacher = $USER->id;
    $codeData->timemodified = time();
    $codeData->modifiedby = $USER->id;
    $DB->insert_record('monorail_course_data', $codeData);
    
    // Add an enrolment plugin if nexessary
    
    // TODO put in monorailservices lib
    // automatically enrol current user to course as teacher
    require_once($CFG->libdir . '/enrollib.php');
    $enrolinstances = enrol_get_instances($courseid, true);
    foreach ($enrolinstances as $courseenrolinstance) {
        if ($courseenrolinstance->enrol == "manual") {
            $instance = $courseenrolinstance;
            break;
        }
    }
    if (! isset($instance)) {
        $enrol = enrol_get_plugin('manual');
        $enrol->add_default_instance($course);
    }
    $enrolinstances = enrol_get_instances($courseid, true);
    foreach ($enrolinstances as $courseenrolinstance) {
        if ($courseenrolinstance->enrol == "manual") {
            $instance = $courseenrolinstance;
            break;
        }
    }
    $roleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), MUST_EXIST);
    //finally proceed the enrolment
    $enrol->enrol_user($instance, $USER->id, $roleid, time(), 0, ENROL_USER_ACTIVE);
    
    // convert imported assignments (2.2) to assign format
    require_once($CFG->dirroot . '/mod/assign/upgradelib.php');
    require_once($CFG->dirroot . '/mod/assign/locallib.php');
    $version = $DB->get_field('modules', 'version', array('name'=>'assignment'), MUST_EXIST);
    $assignments = $DB->get_records('assignment', array('course'=>$courseid));
    foreach ($assignments as $assignment) {
        if (assign::can_upgrade_assignment($assignment->assignmenttype, $version)) {
            $assignment_upgrader = new assign_upgrade_manager();
            $log = '';
            $success = $assignment_upgrader->upgrade_assignment($assignment->id, $log);
            if (!$success) {
                // failed, log
                add_to_log($courseid, 'monorailservices', 'importlib.monorailservices_finalize_cour', '', 'ERROR: Failed to convert assignment '.$assignment->id.' to new 2.3 format');
            } else {
                add_to_log($courseid, 'monorailservices', 'importlib.monorailservices_finalize_cour', '', 'Converted assignment '.$assignment->id.' to new 2.3 format');
            }
        }
    }
    
    // Clean content
    monorailservices_clean_imported_content($courseid);
    
    // If no forums, create
    try {
        $forums = $DB->get_record('forum', array('course'=>$courseid), '*', MUST_EXIST);
    } catch (Exception $ex) {
        try {
            $section = $DB->get_record('course_sections', array('course'=>$courseid, 'section'=>0), '*', MUST_EXIST);
            $moduleid = $DB->get_field('modules', 'id', array('name'=>'forum'));
            $newcm = new stdClass();
            $newcm->course   = $courseid;
            $newcm->module   = $moduleid;
            $newcm->instance = 0;
            $newcm->visible  = 1;
            $newcm->section  = $section->id;
            $newcm->coursemodule  = 0;
            $newcm->added    = time();
            $newcmid = $DB->insert_record('course_modules', $newcm);
            $data = array(
                'sectionid' => $section->id,
                'course' => $courseid,
                'type'  => 'general',
                'name'  => get_string('forum_general',"theme_monorail"),
                'intro' => '<p>A general forum</p>',
                'introformat' => 1,
                'assessed'  => 0,
                'assesstimestart'  => 0,
                'assesstimefinish'  => 0,
                'scale'  => 0,
                'maxbytes'  => 51200,
                'maxattachments'  => 2,
                'forcesubscribe'  => 0,
                'trackingtype'  => 1,
                'rsstype'  => 0,
                'rssarticles'  => 0,
                'warnafter'  => 0,
                'blockafter'  => 0,
                'blockperiod'  => 0,
                'completiondiscussions'  => 0,
                'completionreplies' => 0,
                'completionposts' => 0,
                'coursemodule' => $newcmid,
                'cmidnumber' => $newcmid);
            $data = (object) $data;
            require_once($CFG->dirroot . '/mod/forum/lib.php');
            require_once($CFG->dirroot . '/course/lib.php');
            $forumid = forum_add_instance($data);
            $newcm->coursemodule = $forumid;
            $DB->set_field('course_modules', 'instance', $newcm->coursemodule, array('id'=>$newcmid));
            $newcm->coursemodule  = $newcmid;
            $newcm->section = $section->section;
            add_mod_to_section($newcm);
            $DB->set_field('course_modules', 'section', $section->id, array('id'=>$newcm->coursemodule));
            set_coursemodule_visible($newcm->coursemodule, 1);
            rebuild_course_cache($data->course);
        } catch (Exception $exf) {
            // Unable to add forum
            add_to_log($courseid, 'monorailservices', 'importlib.monorailservices_finalize_cour', '', 'ERROR: Failed to add a default forum to course: '.$exf->getMessage());
            monorail_data('log', $courseid, 'error', $exf->getTraceAsString(), false);
        }
    }

    monorail_update_course_perms($courseid, 1, 1);

    return $codeData->code;
}

// Thanks to http://stackoverflow.com/a/7582804/1489738
function monorailservices_strip_html($data_str) {
    $allowable_tags = '<br><p><a><b><i><u><ul><li><ol><div><span>';
    $allowable_atts = array('href');
    
    $strip_arr = array();
    $data_sxml = simplexml_load_string('<root>'. $data_str .'</root>', 'SimpleXMLElement', LIBXML_NOERROR | LIBXML_NOXMLDECL);
    if ($data_sxml ) {
        // loop all elements with an attribute
        foreach ($data_sxml->xpath('descendant::*[@*]') as $tag) {
            // loop attributes
            foreach ($tag->attributes() as $name=>$value) {
                // check for allowable attributes
                if (!in_array($name, $allowable_atts)) {
                    // set attribute value to empty string
                    $tag->attributes()->$name = '';
                    // collect attribute patterns to be stripped
                    $strip_arr[$name] = '/ '. $name .'=""/';
                }
            }
        }
        // strip unallowed attributes and root tag
        $data_str = strip_tags(preg_replace($strip_arr, array(''),$data_sxml->asXML()), $allowable_tags);
    }

    return $data_str;
}

function monorailservices_delete_mod($cm) {
    global $CFG, $DB;
    
    require_once($CFG->dirroot . "/course/lib.php");
    $modlib = "$CFG->dirroot/mod/$cm->modname/lib.php";
    if (file_exists($modlib)) {
        require_once($modlib);
    } else {
        return 1;
    }
    $deleteinstancefunction = $cm->modname."_delete_instance";
    if (!$deleteinstancefunction($cm->instance)) {
        return 1;
    }
    // remove all module files in case modules forget to do that
    $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
    $fs = get_file_storage();
    $fs->delete_area_files($modcontext->id);
    if (!delete_course_module($cm->id)) {
        return 1;
    }
    if (!delete_mod_from_section($cm->id, $cm->section)) {
        return 1;
    }
    rebuild_course_cache($cm->course);
    return 0;
}

function monorailservices_clean_imported_content($courseid) {
    global $DB, $CFG;
    require_once($CFG->dirroot . "/course/lib.php");
    
    $allowed_mods = array('assign', 'quiz', 'forum', 'resource', 'url');
    
    // Course summary - though not visible we will still clean it just in case
    $course = $DB->get_record('course', array('id'=>$courseid));
    $course->summary = monorailservices_strip_html($course->summary);
    $DB->update_record('course', $course);
    
    // Section summaries
    $sections = $DB->get_records('course_sections', array('course'=>$courseid));
    foreach ($sections as $section) {
        $section->summary = monorailservices_strip_html($section->summary);
        $DB->update_record('course_sections', $section);
    }

    // Modules
    $modules = $DB->get_records('course_modules', array('course'=>$courseid));
    foreach ($modules as $module) {
        $module->visible = 1;
        $module->groupmembersonly = 0;
        $module->completion = 0;
        $module->availablefrom = 0;
        $module->availableuntil = 0;
        $module->showavailability = 0;
        $module->showdescription = 0;
        $DB->update_record('course_modules', $module);
        $cm = get_coursemodule_from_id('', $module->id, 0, true);
        $mod = $DB->get_record('modules', array('id'=>$module->module));
        if (! in_array($mod->name, $allowed_mods)) {
            // Not allowed, delete
            monorailservices_delete_mod($cm);
        } else {
            // Clean and fix
            $modrecord = $DB->get_record($mod->name, array('id'=>$module->instance));
            $modrecord->intro = monorailservices_strip_html($modrecord->intro);
            $modrecord->introformat = 1;
            switch ($mod->name) {
                case "assign":
                    $modrecord->alwaysshowdescription = 1;
                    $modrecord->sendnotifications = 1;
                    $modrecord->sendlatenotifications = 1;
                    $modrecord->grade = 100;    // FIXME: when we allow more grading options, add logic here
                    break;
                case "quiz":
                    //TODO: any special fixes for quiz data?
                    break;
                case "forum":
                    $modrecord->type = 'general';
                    if (strtolower($modrecord->name = 'news forum')) {
                        $modrecord->name = 'General forum';
                    }
                    $modrecord->forcesubscribe = 0;
                    break;
                case "resource":
                    // Make sure only one file per resource
                    $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
                    $fs = get_file_storage();
                    $files = $fs->get_area_files($modcontext->id, 'mod_resource', 'content');
                    $first = true;
                    foreach ($files as $file) {
                        if ($file->get_filename() !== '.') {
                            if (! $first) {
                                // Split in to new module
                                $newcm = new stdClass();
                                $newcm->course   = $courseid;
                                $newcm->module   = $module->module;
                                $newcm->instance = 0; // not known yet, will be updated later
                                $newcm->visible  = 1;
                                $newcm->section  = $module->section;
                                $newcm->coursemodule  = 0;
                                $newcm->added    = time();
                                $newcmid = $DB->insert_record('course_modules', $newcm);
                                $data = array(
                                    'course' => $modrecord->course,
                                    'name' => $modrecord->name,
                                    'intro' => $modrecord->intro,
                                    'introformat' => $modrecord->introformat,
                                    'tobemigrated' => $modrecord->tobemigrated,
                                    'legacyfiles' => $modrecord->legacyfiles,
                                    'legacyfileslast' => $modrecord->legacyfileslast,
                                    'display' => $modrecord->display,
                                    'filterfiles' => $modrecord->filterfiles,
                                    'revision' => $modrecord->revision,
                                );
                                $data = (object) $data;
                                require_once($CFG->dirroot . '/mod/resource/lib.php');
                                require_once($CFG->libdir . '/resourcelib.php');
                                resource_set_display_options($data);
                                $resourceid = $DB->insert_record('resource', $data);
                                $DB->set_field('course_modules', 'instance', $resourceid, array('id'=>$newcmid));
                                //section info update
                                $newcm->coursemodule  = $newcmid;
                                $newcm->section = $module->section;
                                add_mod_to_section($newcm);
                                $DB->set_field('course_modules', 'section', $module->section, array('id'=>$newcmid));
                                set_coursemodule_visible($newcm->coursemodule, 1);
                                $context = context_module::instance($newcmid);
                                $fsnew = get_file_storage();
                                // Prepare file record object
                                $fileinfo = array(
                                    'contextid' => $context->id,
                                    'component' => 'mod_resource',
                                    'filearea' => 'content',
                                    'itemid' => 0,
                                    'filepath' => '/',
                                    'filename' => $file->get_filename()
                                );
                                $filenew = $fsnew->create_file_from_storedfile($fileinfo, $file->get_id());
                                rebuild_course_cache($courseid);
                            } else {
                                $first = false;
                            }
                        }
                    }
                    break;
                case "url":
                    if (strpos($modrecord->externalurl, 'youtube.com') === false &&
                            strpos($modrecord->externalurl, 'youtu.be') === false &&
                            strpos($modrecord->externalurl, 'slideshare.net') === false &&
                            strpos($modrecord->externalurl, 'vimeo.com') === false) {
                        // delete
                        monorailservices_delete_mod($cm);
                        rebuild_course_cache($cm->course);
                        unset($modrecord);
                    }
                    break;
                if (isset($modrecord)) {
                    $DB->update_record($mod->name, $modrecord);
                    rebuild_course_cache($cm->course);
                }
            }
        }
    }
    // Trigger any documents to conversion
    // We don't do this earlier because of the possible splitting of files in resource mods
    $resources = $DB->get_records('resource', array('course'=>$courseid));
    foreach ($resources as $resource) {
        $cm = get_coursemodule_from_instance('resource', $resource->id, 0);
        $modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
        $fs = get_file_storage();
        $files = $fs->get_area_files($modcontext->id, 'mod_resource', 'content');
        foreach ($files as $file) {
            if ($file->get_filename() !== '.' && $file->get_mimetype() !== 'document/unknown') {
                $tempfile = $file->copy_content_to_temp(); //Does not have an extn to filename;
                if($tempfile) {
                    //monorail_trigger_document_conversion($file, $tempfile);
                } else {
                    add_to_log(1, 'importlib', 'monorailservices_clean_imported_comntent', '', 'Document conversion Failed copy error: '.$file->get_filename());
                }
            } else {
                    add_to_log(1, 'importlib', 'monorailservices_clean_imported_comntent', '', 'Failed to trigger doc conversion: '.$file->get_filename().'  '.$file->get_mimetype());
            }
        }
    }
    
}
