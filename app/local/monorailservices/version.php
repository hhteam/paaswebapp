<?php
/**
 * Eliademy.com web services
 * 
 * @package   monorail_services
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_monorailservices';
$plugin->version  = 2015040900;
$plugin->requires = 2012061700;
$plugin->dependencies 	= array(
        'theme_monorail' => ANY_VERSION
		);
