<?php
/**
 * Eliademy.com web services
 *
 * @package   monorail_services
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$WSCACHE_CURRENT = false;
$WSCACHE_CURRENT_DEPS = array();

/* DEPENDENCIES:
 *
 * course_info_<id> - course name, etc., enrolled user info.
 * course_content_<id> - course sections.
 * course_tasks_<id> - course tasks (when tasks are added or deleted - because we have 1 almost useless field in course_info...).
 * user_info_<id> - user name, picture, etc.
 * user_courses_<id> - user courses.
 * user_language_<id> - user language.
 *
 * TODO:
 * course_fourms_<id> - course forums and recent posts.
 * forum_posts_<id> - posts in forum.
 *
 * TODO (changes too often?):
 * task_<id> - task details.
 * user_tasks_<id> - task list of user.
 *
 * TODO:
 * user_events_<time>_<id> ???
 * */

/* Check if function output is cacheable. Cacheable functions must provide
 * a way of invalidating their cache (with dependencies, or else...).
 * */
function wscache_can_cache($name)
{
    return in_array($name, array(
        "local_monorailservices_course_get_cont",
        "local_monorailservices_enrolled_users",
        "local_monorailservices_get_course_info",
        "local_monorailservices_get_user_courses",
        "local_monorailservices_get_users_by_id",
        "local_monorailservices_supported_langs",
        "local_monorailservices_getcrs_categories",
        "local_monorailservices_get_countries"
      ));
}

/* Get cached data by user/function/arguments
 *
 * Returns object with id and data.
 * */
function wscache_get_cached($name, $userid, $args)
{
    global $DB;

    return $DB->get_record_sql("SELECT id, data FROM {monorail_webservice_cache} " .
        "WHERE userid=? AND servicename=? AND arguments=?", array($userid, $name, $args));
}

/* Check if cache entry exists.
 * */
function wscache_cache_entry_valid($id, $userid)
{
    global $DB;

    return $DB->record_exists('monorail_webservice_cache', array('id' => $id, 'userid' => $userid));
}

/* NOTE: Nesting not supported!
 * */
function wscache_start_cached_function()
{
    global $WSCACHE_CURRENT;
    global $WSCACHE_CURRENT_DEPS;

    $WSCACHE_CURRENT = true;
    $WSCACHE_CURRENT_DEPS = array();
}

function wscache_add_cache_dependency($name)
{
    global $WSCACHE_CURRENT;
    global $WSCACHE_CURRENT_DEPS;

    if ($WSCACHE_CURRENT)
    {
        $WSCACHE_CURRENT_DEPS[] = $name;
    }
    else
    {
        error_log("Adding cache dependency for uncached function!");
    }
}

function wscache_end_cached_function($name, $userid, $args, $data)
{
    global $DB;
    global $WSCACHE_CURRENT;
    global $WSCACHE_CURRENT_DEPS;

    if (!$WSCACHE_CURRENT)
    {
        return null;
    }

    $trans = $DB->start_delegated_transaction();

    // REPLACE INTO would be better, but it doesn't return id of the record...
    $cacheId = $DB->get_field_sql("SELECT id FROM {monorail_webservice_cache} " .
        "WHERE userid=? AND servicename=? AND arguments=?", array($userid, $name, $args));

    if (!$cacheId)
    {
        $obj = new stdClass();

        $obj->userid = $userid;
        $obj->servicename = $name;
        $obj->arguments = $args;
        $obj->timecached = time();
        $obj->data = $data;

        $cacheId = $DB->insert_record("monorail_webservice_cache", $obj, true);
    }
    else
    {
        $DB->execute("UPDATE {monorail_webservice_cache} SET data=? WHERE id=?", array($data, $cacheId));
    }

    if (!empty($WSCACHE_CURRENT_DEPS))
    {
        foreach ($WSCACHE_CURRENT_DEPS as $dep)
        {
            $obj = new stdClass();

            $obj->cacheid = $cacheId;
            $obj->cachetoken = $dep;

            $DB->insert_record("monorail_webservice_cache_d", $obj, false);
        }
    }

    $DB->commit_delegated_transaction($trans);

    $WSCACHE_CURRENT = false;
    $WSCACHE_CURRENT_DEPS = array();

    return $cacheId;
}

function wscache_cancel_cached_function()
{
    global $WSCACHE_CURRENT;
    global $WSCACHE_CURRENT_DEPS;

    $WSCACHE_CURRENT = false;
    $WSCACHE_CURRENT_DEPS = array();
}

function wscache_reset_by_dependency($dep)
{
    global $DB, $CFG;

    $caches = $DB->get_fieldset_sql("SELECT cacheid FROM {monorail_webservice_cache_d} WHERE cachetoken=?", array($dep));

    if (!empty($caches))
    {
        $trans = $DB->start_delegated_transaction();

        $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $caches) . ")");
        $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $caches) . ")");

        $DB->commit_delegated_transaction($trans);
    }

    try {
        require_once("$CFG->dirroot/theme/monorail/vendor/autoload.php");

        $redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host'   => $CFG->relay_redis_host,
            'port'   => $CFG->relay_redis_port,
        ));

        $redis->del($dep);

        if (substr($dep, 0, 12) == "course_info_") {
            // Cache clean is actually cache refresh here.
            require_once __DIR__ . '/../../theme/monorail/lib.php';
            monorail_get_course_details(substr($dep, 12));
        }
    } catch (Exception $err) { }
}

function wscache_reset_by_user($userid)
{
    global $DB;

    $caches = $DB->get_fieldset_sql("SELECT id FROM {monorail_webservice_cache} WHERE userid=?",
        array($userid));

    if (!empty($caches))
    {
        $trans = $DB->start_delegated_transaction();

        $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $caches) . ")");
        $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $caches) . ")");

        $DB->commit_delegated_transaction($trans);
    }
}

function wscache_reset($name, $userid)
{
    global $DB;

    $caches = $DB->get_fieldset_sql("SELECT id FROM {monorail_webservice_cache} WHERE servicename=? AND userid=?",
        array($name, $userid));

    if (!empty($caches))
    {
        $trans = $DB->start_delegated_transaction();

        $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $caches) . ")");
        $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $caches) . ")");

        $DB->commit_delegated_transaction($trans);
    }
}

function wscache_reset_for_all($name)
{
    global $DB;

    $caches = $DB->get_fieldset_sql("SELECT id FROM {monorail_webservice_cache} WHERE servicename=?", array($name));

    if (!empty($caches))
    {
        $trans = $DB->start_delegated_transaction();

        $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $caches) . ")");
        $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $caches) . ")");

        $DB->commit_delegated_transaction($trans);
    }
}

/* Remove old cache. Run this every now and then (from cron).
 * */
function wscache_cleanup()
{
    global $DB;

    // 100 oldest caches (but not younger than 1 month).
    $caches = $DB->get_fieldset_sql("SELECT id FROM {monorail_webservice_cache} WHERE timecached<? ORDER BY timecached LIMIT 100",
        array(strtotime("-1 month")));

    // 100 garbage dependencies.
    $deps = $DB->get_fieldset_sql("SELECT cd.id AS cdid, COUNT(c.id) AS cid FROM " .
        "{monorail_webservice_cache_d} AS cd LEFT JOIN {monorail_webservice_cache} AS c ON cd.cacheid=c.id ".
            "GROUP BY cdid HAVING cid=0 LIMIT 100");

    if (!empty($caches) || !empty($deps))
    {
        $trans = $DB->start_delegated_transaction();

        if (!empty($caches)) {
            $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE cacheid IN (" . implode(", ", $caches) . ")");
            $DB->execute("DELETE FROM {monorail_webservice_cache} WHERE id IN (" . implode(", ", $caches) . ")");
        }

        if (!empty($deps)) {
            $DB->execute("DELETE FROM {monorail_webservice_cache_d} WHERE id IN (" . implode(", ", $deps) . ")");
        }

        $DB->commit_delegated_transaction($trans);
    }
}

/* Cahe separate pieces of data in redis.
 * */
function wscache_get_data($name, $call)
{
    global $CFG;

    try {
        require_once("$CFG->dirroot/theme/monorail/vendor/autoload.php");

        $redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host'   => $CFG->relay_redis_host,
            'port'   => $CFG->relay_redis_port,
        ));
    } catch (Exception $err) {
        $redis = null;
    }

    if (!$redis) {
        return call_user_func($call);
    }

    try {
        $data = $redis->get($name);

        if ($data) {
            return unserialize($data);
        }
    } catch (Exception $err) { }

    $data = call_user_func($call);

    try {
        $redis->set($name, serialize($data));
    } catch (Exception $err) { }

    return $data;
}
