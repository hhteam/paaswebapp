<?php
/**
 * Eliademy.com web services
 * 
 * @package   monorail_services
 * @copyright CBTec Oy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */ 


defined('MOODLE_INTERNAL') || die;

// Checks whether a person is a member of a course
function monorailservices_is_member_of_course($userid, $courseid) {    
    global $DB;
    
    $roles = array(1,3,5);    //manager, teacher, student
    
    $context = get_context_instance(CONTEXT_COURSE, $courseid);
    $users = array();
    foreach ($roles as $role) {
        $list = get_role_users($role , $context);
        $idsonly = array();
        foreach ($list as $user) {
            $idsonly[] = $user->id;
        }
        $users = array_merge($users, $idsonly);
    }
    
    return in_array($userid, $users);
}

// Checks whether some member is a member in own courses, eg fellow student or teacher
function monorailservices_is_member_own_courses($userid, $otheruserid) {
    $courses = enrol_get_all_users_courses($userid, true, "id");
    
    foreach ($courses as $course) {
        if (monorailservices_is_member_of_course($otheruserid, $course->id)) {
            return true;
        }
    }
    return false;
}

// Check whether course has any members
function monorailservices_course_has_members($courseid, $students=true, $teachers=false, $exclude=array()) {
    global $DB;
    
    $roles = array();
    if ($students)
        $roles[] = 3;
    if ($teachers) 
        $roles[] = 5;
    if (! $students && ! $teachers) 
        return false;
    
    $context = get_context_instance(CONTEXT_COURSE, $courseid);
    $users = array();
    foreach ($roles as $role) {
        $list = get_role_users($role , $context);
        
        $idsonly = array();
        foreach ($list as $user) {
            if (in_array($user->id, $exclude))
                continue;
            $idsonly[] = $user->id;
        }
        $users = array_merge($users, $idsonly);
    }
    if (count($users))
        return true;
    return false;
}

// Checks if course can be deleted with current data
// Check that the following is not true;
// course is public and course is in public catalog and course has participants
function monorailservices_is_course_deletable($courseid) {
    global $CFG;
    require_once("$CFG->dirroot/theme/monorail/lib.php");
    $cperm = monorail_get_course_perms($courseid);
    if (($cperm['course_privacy'] == 1) && ($cperm['course_access'] == 3) &&
        monorailservices_course_has_members($courseid)) {
        return false;
    }
    return true;
}       


// Clean course data from monorail tables
// HOX WARNING TODO: we _really_ should have proper cleanup method calls, this
// will unfortunately leave lots of orphaned data lying around. But still Moodle
// is insane enough to in rare cases reuse course id's, so we _have to_ clean up
// data at least for the courseid columns, otherwise course creation crashes internally
// inside moodle
function monorailservices_delete_course_data($courseid) {
    global $DB, $CFG;
    require_once("$CFG->dirroot/theme/monorail/lib.php");
    // delete cohort course info if any
    $DB->delete_records('monorail_cohort_courseadmins', array('courseid' => $courseid)); 
    $DB->delete_records('monorail_cohort_crsinvitee', array('courseid' => $courseid)); 
    $DB->delete_records('monorail_course_data', array('courseid' => $courseid));
    $DB->delete_records('monorail_course_skills', array('courseid' => $courseid));
    monorail_data('TEXT', $courseid, 'coursebackground', $delete=true);
    monorail_data('TEXT', $courseid, 'courselogo', $delete=true);
    monorail_data('BOOLEAN', $courseid, 'public', $delete=true);
    monorail_data('BOOLEAN', $courseid, 'business', $delete=true);
    monorail_data('INTEGER', $courseid, 'review_submit_time', $delete=true);
    monorail_data('FLOAT', $courseid, 'course_price', $delete=true);
    monorail_data('TEXT', $courseid, 'course_paypal_account', $delete=true);
    $DB->delete_records('monorail_data', array('type'=>'EXT_DATA', 'itemid'=>$courseid));
    monorail_data('INTEGER', $courseid, 'course_rating', $delete=true);
    $DB->delete_records('monorailfeed_notifications', array('courseid' => $courseid));
    $DB->delete_records('monorailfeed_rev_course', array('courseid' => $courseid));
    $DB->delete_records('monorailfeed_rev_event', array('courseid' => $courseid));
}

?>
