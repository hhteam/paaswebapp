<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


function monorail_update_event($event, $repeatids) {
    
    require_once(dirname(__FILE__) . '/../../config.php');
    global $CFG;
    
    try {
        require_once($CFG->dirroot .'/local/monorailfeed/lib.php');
        
        // if course event created by teachers
        if ($event->eventtype == 'course') {
            // get last revision of event
            try {
                $rev = monorailfeed_get_revision('event', $event->id);
                if ($event->timestart != $rev->timestart || $event->timeduration != $event->timeduration) {
                    // time changed
                    $revid = monorailfeed_save_revision('event', $event->id);
                    monorailfeed_create_notification($event->courseid, 'event_upd', 'calendar', $event->id, $event->userid, null, $revid);
                }
            } catch (Exception $ex) {
                // an old event with no revision history
                $revid = monorailfeed_save_revision('event', $event->id);
                monorailfeed_create_notification($event->courseid, 'event_upd', 'calendar', $event->id, $event->userid, null, $revid);
            }
        }
    } catch (Exception $ex) {
        add_to_log(1, 'monorail', 'calendar/lib.monorail_update_event', '', 'Failed to process update calendar event hook for event '.$event->id);
    }
}

function monorail_add_event($event, $repeatids) {
    
    require_once(dirname(__FILE__) . '/../../config.php');
    global $CFG;

    try {
        require_once($CFG->dirroot .'/local/monorailfeed/lib.php');
        
        // if course event created by teachers
        if ($event->eventtype == 'course') {
            $revid = monorailfeed_save_revision('event', $event->id);
            monorailfeed_create_notification($event->courseid, 'event_new', 'calendar', $event->id, $event->userid, null, $revid);
        }
    } catch (Exception $ex) {
        add_to_log(1, 'monorail', 'calendar/lib.monorail_add_event', '', 'Failed to process add calendar event hook for event '.$event->id);
    }    
}

function monorail_delete_event($eventid, $repeatids) {
    
    require_once(dirname(__FILE__) . '/../../config.php');
    global $DB, $CFG;

    try {
        require_once($CFG->dirroot .'/local/monorailfeed/lib.php');
        
        // get last revision of event
        $event = monorailfeed_get_revision('event', $eventid);
        
        // remove any other notifications about this
        $notifications = monorailfeed_get_unread_instance('calendar', $eventid);
        foreach ($notifications as $notification) {
            monorailfeed_remove_notification($notification->id);
        }
        
        // if course event, only supported atm
        if (isset($event->courseid) && $event->courseid > 0) {
            monorailfeed_create_notification($event->courseid, 'event_del', 'calendar', $event->eventid, $event->editedby, null, $event->id);
        }
    } catch (Exception $ex) {
        add_to_log(1, 'monorail', 'calendar/lib.monorail_delete_event', '', 'Failed to process delete calendar event hook for event '.$eventid);
    }       
}

?>
