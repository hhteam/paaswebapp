<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

 
defined('MOODLE_INTERNAL') || die();
 
$plugin->version   = 2014031700;
$plugin->requires  = 2012062500; // See http://docs.moodle.org/dev/Moodle_Versions
$plugin->component = 'local_monorailfeed';
$plugin->cron     = 10;
