<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


function xmldb_local_monorailfeed_install() {
    
    require_once('upgradelib.php');
    
    //~ global $DB;
    //~ $dbman = $DB->get_manager();

    $result = monorailfeed_upgrade_calendar_lib();

    return $result;
    
    
}

?>
