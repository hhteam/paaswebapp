<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


function xmldb_local_monorailfeed_upgrade($oldversion = 0) {
    
    require_once('upgradelib.php');
    
    global $DB;
    $dbman = $DB->get_manager();

    $result = true;
    
    if ($result && $oldversion < 2012070902) {
        // Define table monorailfeed_notifications to be dropped
        $table = new xmldb_table('monorailfeed_notifications');

        // Conditionally launch drop table for monorailfeed_notifications
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }
        
        // Define table monorailfeed_notifications to be created
        $table = new xmldb_table('monorailfeed_notifications');

        // Adding fields to table monorailfeed_notifications
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('instance', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('component', XMLDB_TYPE_CHAR, '30', null, null, null, null);
        $table->add_field('revtableid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table monorailfeed_notifications
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorailfeed_notifications
        $table->add_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));

        // Conditionally launch create table for monorailfeed_notifications
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table monorailfeed_rev_course to be created
        $table = new xmldb_table('monorailfeed_rev_course');

        // Adding fields to table monorailfeed_rev_course
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('fullname', XMLDB_TYPE_CHAR, '254', null, null, null, null);
        $table->add_field('shortname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('idnumber', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('summary', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table monorailfeed_rev_course
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorailfeed_rev_course
        $table->add_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));

        // Conditionally launch create table for monorailfeed_rev_course
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

         // Define table monorailfeed_rev_sections to be created
        $table = new xmldb_table('monorailfeed_rev_sections');

        // Adding fields to table monorailfeed_rev_sections
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('coursesectionsid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('summary', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table monorailfeed_rev_sections
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorailfeed_rev_sections
        $table->add_index('coursesectionsid', XMLDB_INDEX_NOTUNIQUE, array('coursesectionsid'));

        // Conditionally launch create table for monorailfeed_rev_sections
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Define table monorailfeed_rev_assign to be created
        $table = new xmldb_table('monorailfeed_rev_assign');

        // Adding fields to table monorailfeed_rev_assign
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('assignid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('intro', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('duedate', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('allowsubmissionsfromdate', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('grade', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table monorailfeed_rev_assign
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorailfeed_rev_assign
        $table->add_index('assignid', XMLDB_INDEX_NOTUNIQUE, array('assignid'));

        // Conditionally launch create table for monorailfeed_rev_assign
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Define table monorailfeed_rev_quiz to be created
        $table = new xmldb_table('monorailfeed_rev_quiz');

        // Adding fields to table monorailfeed_rev_quiz
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('quizid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('intro', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timeopen', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timeclose', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('timelimit', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('grademethod', XMLDB_TYPE_INTEGER, '4', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table monorailfeed_rev_quiz
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorailfeed_rev_quiz
        $table->add_index('quizid', XMLDB_INDEX_NOTUNIQUE, array('quizid'));

        // Conditionally launch create table for monorailfeed_rev_quiz
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
         // Define table monorailfeed_rev_event to be created
        $table = new xmldb_table('monorailfeed_rev_event');

        // Adding fields to table monorailfeed_rev_event
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('eventid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('description', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('timestart', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timeduration', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table monorailfeed_rev_event
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table monorailfeed_rev_event
        $table->add_index('eventid', XMLDB_INDEX_NOTUNIQUE, array('eventid'));

        // Conditionally launch create table for monorailfeed_rev_event
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2012070902, 'local', 'monorailfeed');
    }

    if ($oldversion < 2012071001) {

        // Define field coursemodulesid to be added to monorailfeed_rev_assign
        $table = new xmldb_table('monorailfeed_rev_assign');
        $field = new xmldb_field('coursemodulesid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'assignid');

        // Conditionally launch add field coursemodulesid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define index coursemodulesid (not unique) to be added to monorailfeed_rev_assign
        $table = new xmldb_table('monorailfeed_rev_assign');
        $index = new xmldb_index('coursemodulesid', XMLDB_INDEX_NOTUNIQUE, array('coursemodulesid'));

        // Conditionally launch add index coursemodulesid
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
        
        // Define field coursemodulesid to be added to monorailfeed_rev_quiz
        $table = new xmldb_table('monorailfeed_rev_quiz');
        $field = new xmldb_field('coursemodulesid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'quizid');

        // Conditionally launch add field coursemodulesid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define index coursemodulesid (not unique) to be added to monorailfeed_rev_quiz
        $table = new xmldb_table('monorailfeed_rev_quiz');
        $index = new xmldb_index('coursemodulesid', XMLDB_INDEX_NOTUNIQUE, array('coursemodulesid'));

        // Conditionally launch add index coursemodulesid
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2012071001, 'local', 'monorailfeed');
    }

      if ($oldversion < 2012071804) {

        // Define field coursesectionsid to be added to monorailfeed_rev_assign
        $table = new xmldb_table('monorailfeed_rev_assign');
        $field = new xmldb_field('coursesectionsid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'coursemodulesid');

        // Conditionally launch add field coursesectionsid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field coursesectionsid to be added to monorailfeed_rev_quiz
        $table = new xmldb_table('monorailfeed_rev_quiz');
        $field = new xmldb_field('coursesectionsid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'coursemodulesid');

        // Conditionally launch add field coursesectionsid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2012071804, 'local', 'monorailfeed');
    }
    
    if ($oldversion < 2012073102) {

        // Define table monorailfeed_user_access to be created
        $table = new xmldb_table('monorailfeed_user_access');

        // Adding fields to table monorailfeed_user_access
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('lastaccess', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table monorailfeed_user_access
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_UNIQUE, array('userid'));

        // Conditionally launch create table for monorailfeed_user_access
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // loop through all users and create an initial last access. Otherwise current
        // users will be swamped with notifications
        $users = $DB->get_records('user');
        foreach ($users as $user) {
            try {
                $lastaccess = $DB->get_record('monorailfeed_user_access', array('userid'=>$user->id), 'lastaccess', MUST_EXIST);
            } catch (Exception $ex) {
                // insert
                $record = new stdClass();
                $record->userid = $user->id;
                $record->lastaccess = $user->lastaccess;
                $DB->insert_record('monorailfeed_user_access', $record);
            }
        }

        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2012073102, 'local', 'monorailfeed');
    }
    
    if ($oldversion < 2012080302) {

        // Define field courseid to be added to monorailfeed_rev_event
        $table = new xmldb_table('monorailfeed_rev_event');
        $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'eventid');

        // Conditionally launch add field courseid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2012080302, 'local', 'monorailfeed');
    }

    if ($oldversion < 2012081900) {

        // Define field editedby to be added to monorailfeed_rev_course
        $table = new xmldb_table('monorailfeed_rev_course');
        $field = new xmldb_field('editedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'summary');

        // Conditionally launch add field editedby
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define field editedby to be added to monorailfeed_rev_sections
        $table = new xmldb_table('monorailfeed_rev_sections');
        $field = new xmldb_field('editedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'summary');

        // Conditionally launch add field editedby
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define field editedby to be added to monorailfeed_rev_assign
        $table = new xmldb_table('monorailfeed_rev_assign');
        $field = new xmldb_field('editedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'grade');

        // Conditionally launch add field editedby
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define field editedby to be added to monorailfeed_rev_quiz
        $table = new xmldb_table('monorailfeed_rev_quiz');
        $field = new xmldb_field('editedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'grademethod');

        // Conditionally launch add field editedby
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define field editedby to be added to monorailfeed_rev_event
        $table = new xmldb_table('monorailfeed_rev_event');
        $field = new xmldb_field('editedby', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'timeduration');

        // Conditionally launch add field editedby
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2012081900, 'local', 'monorailfeed');
    }

    if ($oldversion < 2013071100) {

        // Define table monorailfeed_user_settings to be created
        $table = new xmldb_table('monorailfeed_user_settings');

        // Adding fields to table monorailfeed_user_settings
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '20', null, null, null, null);
        $table->add_field('value', XMLDB_TYPE_INTEGER, '1', null, null, null, '2');

        // Adding keys to table monorailfeed_user_settings
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Adding indexes to table monorailfeed_user_settings
        $table->add_index('userid-type', XMLDB_INDEX_UNIQUE, array('userid', 'type'));

        // Conditionally launch create table for monorailfeed_user_settings
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define field createdby to be added to monorailfeed_notifications
        $table = new xmldb_table('monorailfeed_notifications');
        $field = new xmldb_field('createdby', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'timemodified');

        // Conditionally launch add field createdby
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define key createdby (foreign) to be added to monorailfeed_notifications
        $table = new xmldb_table('monorailfeed_notifications');
        $key = new xmldb_key('createdby', XMLDB_KEY_FOREIGN, array('createdby'), 'user', array('id'));

        // Launch add key createdby
        $dbman->add_key($table, $key);
        
        // Do some data migration, fill createdby in all rows
        $notifications = $DB->get_records('monorailfeed_notifications');
        foreach ($notifications as $notif) {
            switch ($notif->type) {
                case 'calendar':
                    if (isset($notif->revtableid)) {
                        try {
                            $revision = $DB->get_record('monorailfeed_rev_event', array('id'=>$notif->revtableid), 'editedby', MUST_EXIST);
                            $createdby = $revision->editedby;
                        } catch (Exception $ex) {
                            $createdby = 1;
                        }
                    } else {
                        $createdby = 1;
                    }
                    break;
                case 'forum':
                    try {
                        $post = $DB->get_record('forum_posts', array('id'=>$notif->instance), 'userid', MUST_EXIST);
                        $createdby = $post->userid;
                    } catch (Exception $ex) {
                        $createdby = 1;
                    }
                    break;
                case 'section':
                    if (isset($notif->revtableid)) {
                        try {
                            $revision = $DB->get_record('monorailfeed_rev_sections', array('id'=>$notif->revtableid), '*', MUST_EXIST);
                            $createdby = $revision->editedby;
                        } catch (Exception $ex) {
                            $createdby = 1;
                        }
                    } else {
                        $createdby = 1;
                    }
                    break;
                case 'task_new':
                case 'task_upd':
                    if (isset($notif->revtableid)) {
                        try {
                            $revision = $DB->get_record('monorailfeed_rev_'.$notif->component, array('id'=>$notif->revtableid), '*', MUST_EXIST);
                            $createdby = $revision->editedby;
                        } catch (Exception $ex) {
                            $createdby = 1;
                        }
                    } else {
                        $createdby = 1;
                    }
                    break;
                case 'material':
                case 'url':
                    $createdby = $notif->component;
                    break;
                case 'grade':
                    try {
                        $cm = get_coursemodule_from_id('assign', $notif->instance, $notif->courseid, false);
                        $notifuser = $DB->get_record('monorailfeed_users', array('monorailfeednotificationsid'=>$notif->id), 'id', MUST_EXIST);
                        $grade = $DB->get_record('assign_grades', array('assignment'=>$cm->instance, 'userid'=>$notifuser->id), 'grader', MUST_EXIST);
                        $createdby = $grade->grader;
                    } catch (Exception $ex) {
                        $createdby = 1;
                    }
                    break;
            }
            $notif->createdby = $createdby;
            if ($notif->type === 'material') {
                $notif->component = 'file';
            }
            try {
                $DB->update_record('monorailfeed_notifications', $notif);
            } catch (Exception $ex) {
                // don't die
            }
        }

        // Define field timesent to be added to monorailfeed_users
        $table = new xmldb_table('monorailfeed_users');
        $field = new xmldb_field('timesent', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0', 'timemodified');

        // Conditionally launch add field timesent
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Define index userid-timesent (not unique) to be added to monorailfeed_users
        $table = new xmldb_table('monorailfeed_users');
        $index = new xmldb_index('userid-timesent', XMLDB_INDEX_NOTUNIQUE, array('userid', 'timesent'));

        // Conditionally launch add index userid-timesent
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }
        
        // Add some new notification settings
        $users = $DB->get_records_sql('select distinct userid from {monorailfeed_user_settings}');
        foreach ($users as $user) {
            $record = new stdClass();
            $record->userid = $user->userid;
            $record->type = 'user_enrolled';
            $record->value = 1;
            try {
                $DB->insert_record('monorailfeed_user_settings', $record);
            } catch (Exception $ex) {
                // already inserted
            }
            $record = new stdClass();
            $record->userid = $user->userid;
            $record->type = 'user_submitted';
            $record->value = 1;
            try {
                $DB->insert_record('monorailfeed_user_settings', $record);
            } catch (Exception $ex) {
                // already inserted
            }
        }
        
        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2013071700, 'local', 'monorailfeed');
    }

    if ($oldversion < 2014031700) {
        $users = $DB->get_records_sql('select distinct userid from {monorailfeed_user_settings}');
        foreach ($users as $user) {
            $record = new stdClass();
            $record->userid = $user->userid;
            $record->type = 'user_completed';
            $record->value = 1;
            try {
                $DB->insert_record('monorailfeed_user_settings', $record);
            } catch (Exception $ex) {
                // already inserted
            }
        }

        // monorailfeed savepoint reached
        upgrade_plugin_savepoint(true, 2014031700, 'local', 'monorailfeed');
    }

    // always update calendar lib
    monorailfeed_upgrade_calendar_lib();
    
    return $result;
    
    
}

?>
