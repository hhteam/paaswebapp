<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


/* List of handlers */
$handlers = array (
     'mod_created' => array (
         'handlerfile'      => '/local/monorailfeed/handlers.php',
         'handlerfunction'  => 'mod_created_handler',
         'schedule'         => 'instant'
     ),
     'mod_updated' => array (
         'handlerfile'      => '/local/monorailfeed/handlers.php',
         'handlerfunction'  => 'mod_updated_handler',
         'schedule'         => 'instant'
     ),
     'mod_deleted' => array (
         'handlerfile'      => '/local/monorailfeed/handlers.php',
         'handlerfunction'  => 'mod_deleted_handler',
         'schedule'         => 'instant'
     ),
     'user_enrolled' => array (
         'handlerfile'      => '/local/monorailfeed/handlers.php',
         'handlerfunction'  => 'user_enrolled_handler',
         'schedule'         => 'instant'
     )
);

?>
