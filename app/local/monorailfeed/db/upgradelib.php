<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


function monorailfeed_upgrade_calendar_lib() {
    
    global $CFG;
    
    // make sure calendar/monorail dir exists
    if (!file_exists($CFG->dirroot .'/calendar/monorail')) {
        mkdir($CFG->dirroot .'/calendar/monorail', 0755);
    }

    // copy new version of calendar/lib.php
    $result = copy($CFG->dirroot .'/local/monorailfeed/calendar/lib.php', $CFG->dirroot .'/calendar/monorail/lib.php');
    
    return $result;

}

function monorailfeed_remove_calendar_lib() {
    
    global $CFG;
    
    // Remove file
    unlink($CFG->dirroot .'/calendar/monorail/lib.php');
    
    // Remove dir
    $result = rmdir($CFG->dirroot .'/calendar/monorail');
    
    return $result;

}


?>
