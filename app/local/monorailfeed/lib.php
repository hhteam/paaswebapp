<?php
/**
 * Monorail notifications engine
 *
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */


defined('MOODLE_INTERNAL') || die();

/**
 * Cron job launched by Moodle main cron
 *
 */
function local_monorailfeed_cron() {
    global $DB, $CFG;
    $time = getdate();
    if ($time['hours'] === 7) {
        require_once($CFG->dirroot. '/theme/monorail/lib.php');
        $lastrun = monorail_data('monorailfeed', 1, 'cronlastrun');
        if (date('zY', current($lastrun)->value) != date('zY', time())) {
            // hasn't run today
            monorail_data('monorailfeed', 1, 'cronlastrun', time());

            // Get distinct users who have unread notifs
            $query = <<<QUERY
                select distinct mu.userid from
                    {monorailfeed_users} mu,
                    {monorailfeed_notifications} mn
                where
                    mu.timesent = 0 and
                    mu.timeread = 0 and
                    mn.id = mu.monorailfeednotificationsid and
                    mn.timemodified > ?
QUERY;
            $users = $DB->get_records_sql($query, array(time()-(60*60*24*7)));
            foreach ($users as $user) {
                $usersettings = monorailfeed_user_email_settings($user->userid, null, true);
                if (in_array(1, $usersettings)) {
                    // User wants something as daily notif, let's check
                    $notifications = monorailfeed_get_unsent_user($user->userid, time()-(60*60*24*7));
                    $notifstosend = array();
                    if (count($notifications) > 0) {
                        $userrec = $DB->get_record('user', array('id'=>$user->userid), 'lang, timezone');
                        foreach ($notifications as $notification) {
                            if ($usersettings[monorailfeed_type_to_setting($notification->type, $notification->component)] == 1) {
                                try {
                                    $notiftosend = monorailfeed_getnotification($notification->id, $userrec->lang);
                                } catch (Exception $ex) {
                                    continue;
                                }
                                $notifstosend[] = $notiftosend;
                            }
                        }
                        if (count($notifstosend) > 0) {
                            // We have something to send
                            add_to_log(1, 'monorailfeed', 'lib', 'monorailfeed_cron', 'User '.$user->userid.' daily summary has notifications: '.count($notifstosend));
                            try {
                                monorail_send_notifications(
                                    monorailfeed_format_mail_subject($notifstosend, $userrec->lang),
                                    monorailfeed_format_mail_notifs($notifstosend, $userrec->timezone),
                                    $user->userid,
                                    $userrec->lang
                                );
                            } catch (Exception $ex) {
                                add_to_log(1, 'monorailfeed', 'lib', 'monorailfeed_cron', 'Exception from mailer: '.$ex->getMessage());
                            }
                            monorailfeed_mark_all_sent($user->userid);
                        }
                    }
                }
            }
        }
    }
}

/**
 * MonoRailFeed Get component data item revision
 */
function monorailfeed_get_revision($component, $cid) {
	global $DB;

    // constants
    //TODO: should really make the revision system in to a class
    $REV_TABLES = array(
                "assign" => "monorailfeed_rev_assign",
                "course" => "monorailfeed_rev_course",
                "section" => "monorailfeed_rev_sections",
                "quiz" => "monorailfeed_rev_quiz",
                "event" => "monorailfeed_rev_event"
                );
    $ORIG_IDCOLS = array(
                "assign" => "assignid",
                "course" => "courseid",
                "section" => "coursesectionsid",
                "quiz" => "quizid",
                "event" => "eventid"
                );

    $query = <<<EOD
        select * from {{$REV_TABLES[$component]}} where
            id = (select max(id) from {{$REV_TABLES[$component]}} where {$ORIG_IDCOLS[$component]} = ?)
EOD;
    $revision = $DB->get_record_sql($query, array($cid), MUST_EXIST);

    return $revision;
}

/**
 * MonoRailFeed Compare item with revision
 */
function monorailfeed_compare_with_rev($component, $event, $rev) {
    // constants
    //TODO: should really make the revision system in to a class
    $REV_COLS = array(
        "assign" => array("name", "intro", "duedate", "allowsubmissionsfromdate", "grade"),
        "course" => array("fullname", "shortname", "idnumber", "summary"),
        "section" => array("name", "summary"),
        // TODO: do these columns cover all the needed quiz columns, except questions?
        "quiz" => array("name", "intro", "timeopen", "timeclose", "timelimit", "grademethod"),
        "event" => array("courseid", "name", "description", "timestart", "timeduration")
        );

    $changed = false;

    foreach ($REV_COLS[$component] as $col) {
        if ($event->$col !== $rev->$col) {
            $changed = true;
        }
    }

    return $changed;
}

/**
 * MonoRailFeed Save component data item revision
 */
function monorailfeed_save_revision($component, $cid, $cmid = null, $editedby = null) {
    require_once(dirname(__FILE__) . '/../../config.php');
	global $DB;

    // constants
    //TODO: should really make the revision system in to a class
    $REV_TABLES = array(
                "assign" => "monorailfeed_rev_assign",
                "course" => "monorailfeed_rev_course",
                "section" => "monorailfeed_rev_sections",
                "quiz" => "monorailfeed_rev_quiz",
                "event" => "monorailfeed_rev_event"
                );
    $ORIG_TABLES = array(
                "assign" => "assign",
                "course" => "course",
                "section" => "course_sections",
                "quiz" => "quiz",
                "event" => "event",
                "bigbluebuttonbn" => "bigbluebuttonbn"
                );
    $REV_COLS = array(
                "assign" => array("name", "intro", "duedate", "allowsubmissionsfromdate", "grade"),
                "course" => array("fullname", "shortname", "idnumber", "summary"),
                "section" => array("name", "summary"),
                "quiz" => array("name", "intro", "timeopen", "timeclose", "timelimit", "grademethod"),
                "event" => array("courseid", "name", "description", "timestart", "timeduration"),
                "bigbluebuttonbn" => array("name", "description")
                );

    // get new row
    $data = $DB->get_record($ORIG_TABLES[$component], array('id'=>$cid), '*', MUST_EXIST);
    // create revision row in correct table
    $record = new stdClass();
    $record->{str_replace("_", "", $ORIG_TABLES[$component])."id"} = $cid;
    if (isset($cmid)) {
        $record->coursemodulesid = $cmid;
        $cm = $DB->get_record('course_modules', array('id'=>$cmid), 'section', MUST_EXIST);
        $record->coursesectionsid = $cm->section;
    }
    for ($i=0; $i<count($REV_COLS[$component]); $i++) {
        $record->{$REV_COLS[$component][$i]} = $data->{$REV_COLS[$component][$i]};
    }
    // edited by
    if (! isset($editedby)) {
        if ($component == 'event') {
            $record->editedby = $data->userid;
        } else if ($component == 'course') {
            $query = <<<EOD
                select userid from {log} where
                    course = ? and time >= ? and time <= ? and module = 'course' and action = 'update'
EOD;
            try {
                $results = $DB->get_records_sql($query, array($cid, $data->timemodified-2, $data->timemodified+2), MUST_EXIST);
                foreach ($results as $result) {
                    $record->editedby = $result->userid;
                    break;
                }
            } catch (Exception $ex) {
                $record->editedby = 0;
            }
        }
    } else {
        $record->editedby = $editedby;
    }
    $record->timemodified = time();
    try {
        $revid = $DB->insert_record($REV_TABLES[$component], $record);
    } catch (Exception $ex) {
        $revid = 0;
    }
    return $revid;
}

/**
 * MonoRailFeed Create notification
 */
function monorailfeed_create_notification($courseid, $component, $typename, $instance, $createdby=null, $userid=null, $revid=null, $timestamp=null, $roles=array(3,5)) {
    global $CFG, $DB, $USER;
    if (!empty($CFG->debug) && $CFG->debug >= DEBUG_DEVELOPER) {
        $trace = debug_backtrace();
        add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', '', 'Called from '.$trace[0]['function']);
    }

    // check if the section (if any) is hidden and skip if it is
    if ($typename != 'forum' && $typename != 'calendar' && $typename != 'teacher' && $typename != "cert") {
        try {
            if ($component == 'assign') {
                $cm = get_coursemodule_from_id('assign', $instance, $courseid, true, MUST_EXIST);
                $sectioninfo = $DB->get_record('course_modules', array('id'=>$instance), 'id,visible', MUST_EXIST);
            } else if ($component == 'quiz') {
                $cm = get_coursemodule_from_id('quiz', $instance, $courseid, true, MUST_EXIST);
                $sectioninfo = $DB->get_record('course_modules', array('id'=>$instance), 'id,visible', MUST_EXIST);
            } else if ($component == 'bigbluebuttonbn') {
                /*
                $cm = get_coursemodule_from_id('bigbluebuttonbn', $instance, $courseid, true, MUST_EXIST);
                $sectioninfo = $DB->get_record('course_sections', array('course'=>$courseid, 'section'=>$cm->sectionnum), '*', MUST_EXIST);
                */

                // FIXME: This whole place is outdated. All items are
                // attached to section 0, which is never hidden.

                $sectioninfo = new stdClass();
                $sectioninfo->visible = true;

            } else if ($typename == 'material') {
                $cm = get_coursemodule_from_id('resource', $instance, $courseid, true, MUST_EXIST);
                $sectioninfo = $DB->get_record('course_sections', array('course'=>$courseid, 'section'=>$cm->sectionnum), '*', MUST_EXIST);
            } else if ($typename == 'section') {
                $sectioninfo = $DB->get_record('course_sections', array('id'=>$instance), '*', MUST_EXIST);
            } else {
                add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'Skipped creating notification! Unknown type when trying to find section: type='.$typename.' component='.$component);
                return;
            }
        } catch (Exception $ex) {
            add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'Error finding section to determine if hidden! '.get_class($ex).': '.$ex->getMessage());
            return;
        }
        if ($sectioninfo->visible != 1) {
            // hidden section, skip
            return;
        }
    }
    // check first if there already is an existing notification relating to this revision
    if (isset($revid) and isset($component)) {
        $existing = $DB->get_record('monorailfeed_notifications', array('courseid'=>$courseid, 'type'=>$typename, 'component'=>$component, 'instance'=>$instance, 'revtableid'=>$revid), 'id');
    } else {
        $revid = 0;
    }
    // set time if not set
    if (! isset($timestamp)) {
        $timestamp = time();
    }
    // Set createdby if null
    if (! isset($createdby)) {
        $createdby = $USER->id;
    }
    if (isset($existing) && isset($existing->id)) {
        // existing found, let's just link user if supplied
        $notification_id = $existing->id;
    } else {
        // create notification in db
        $record = new stdClass();
        $record->courseid = $courseid;
        $record->instance = $instance;
        $record->type = $typename;
        $record->component = $component;
        $record->revtableid = $revid;
        $record->timemodified = $timestamp;
        $record->createdby = $createdby;
        try {
            $notification_id = $DB->insert_record('monorailfeed_notifications', $record);
        } catch (Exception $ex) {
            // something wrong when inserting notification to DB, return true so that process doesn't die
            add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'Error creating notification! '.get_class($ex).': '.$ex->getMessage());
            return true;
        }
    }
    // check for any old notifications relating to this new one and mark them read
    $notifications = $DB->get_records_select('monorailfeed_notifications', "courseid = ? and instance = ? and type = ? and component = ? and id != ?", array($courseid, $instance, $typename, $component, $notification_id));
    foreach ($notifications as $notification) {
        if (isset($userid)) {
            $isread = $DB->get_field('monorailfeed_users', 'timeread', array('monorailfeednotificationsid'=>$notification->id, 'userid'=>$userid, 'timeread'=>0));
            if (isset($isread)) {
                monorailfeed_mark_read($notification->id, $userid);
            }
        } else {
            $usernotifs = $DB->get_records('monorailfeed_users', array('monorailfeednotificationsid'=>$notification->id, 'timeread'=>0));
            foreach ($usernotifs as $notif) {
                monorailfeed_mark_read($notification->id, $notif->userid);
            }
        }
    }
    require_once($CFG->dirroot. '/theme/monorail/vendor/autoload.php');
    $redis = new Predis\Client(array(
        'scheme' => 'tcp',
        'host'   => $CFG->relay_redis_host,
        'port'   => $CFG->relay_redis_port,
    ));
    // create user records
	if (isset($userid)) {
		$record = new stdClass();
		$record->monorailfeednotificationsid = $notification_id;
		$record->userid = $userid;
		$record->timeread = 0;
		$record->timemodified = $timestamp;
		try {
			$DB->insert_record('monorailfeed_users', $record, false);
            // trigger push to relay
            $sessions = $DB->get_records('monorail_sessions', array('userid'=>$userid));
            foreach ($sessions as $session) {
                $redis->sadd('notifications', $session->sesskey);
            }
            // send via email
            monorailfeed_send_instant_notif($userid, $courseid, $typename, $notification_id, $component);
		} catch (Exception $ex) {
			 //what can we do, let's not let the process die here
             add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'Error creating notification! '.get_class($ex).': '.$ex->getMessage());
		}
	} else {
		// loop through all students enrolled and create
		try {
			$context = get_context_instance(CONTEXT_COURSE, $courseid);
            $users = array();
            foreach ($roles as $role) {
                $list = get_role_users($role , $context);
                $users = array_merge($users, $list);
            }
		} catch (Exception $ex) {
			// something wrong when getting users, return true so that process doesn't die
            add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'Error creating notification! '.get_class($ex).': '.$ex->getMessage());
			return true;
		}
		foreach ($users as $user) {
            if ($user->id == $createdby) {
                continue;
            }
			$record = new stdClass();
			$record->monorailfeednotificationsid = $notification_id;
			$record->userid = $user->id;
			$record->timeread = 0;
			$record->timemodified = $timestamp;
            $failed = false;
			try {
				$DB->insert_record('monorailfeed_users', $record, false);
			} catch (Exception $ex) {
                // update
                try {
                    $usernotification = $DB->get_record('monorailfeed_users', array('monorailfeednotificationsid'=>$notification_id, 'userid'=>$user->id), '*', MUST_EXIST);
                    $usernotification->timeread = 0;
                    $usernotification->timemodified = $timestamp;
                    $DB->update_record('monorailfeed_users', $usernotification);
                } catch (Exception $exx) {
                    //what can we do, let's not let the process die here
                    add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'Error creating notification! '.get_class($ex).': '.$ex->getMessage());
                    $failed = true;
                }
			}
            if (! $failed) {
                // trigger push to relay
                $sessions = $DB->get_records('monorail_sessions', array('userid'=>$user->id));
                foreach ($sessions as $session) {
                    $redis->sadd('notifications', $session->sesskey);
                }
                // send via email
                monorailfeed_send_instant_notif($user->id, $courseid, $typename, $notification_id, $component);
            }
		}
	}
    try {
        require_once(dirname(__FILE__) . '/../pushnotifications/pushnotifications.php');
        $notiftosend = monorailfeed_getnotification($notification_id);
        send_pushnotifications_to_users($notiftosend);
    } catch (Exception $ex) {
        add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'EXCEPTION '.get_class($ex).': '.$ex->getMessage());
    }
	return true;
}

function monorailfeed_format_mail_notifs($notifications, $tz=99) {
    global $CFG, $DB;
    $txt = "";
    $template_notification = monorail_get_template('mail/notification');
    $template_notification_course = monorail_get_template('mail/notification_course');
    $coursebundles = array();
    foreach ($notifications as $notification) {
        if (! isset($coursebundles[$notification->courseid])) {
            $coursebundles[$notification->courseid] = array();
        }
        $coursebundles[$notification->courseid][] = $notification;
    }
    foreach ($coursebundles as $course) {
        $notificationstxt = "";
        foreach ($course as $notification) {
            if (isset($notification->externallink)) {
                $link = $CFG->magic_ui_root.'/'.ltrim($notification->externallink, '/');
            } else {
                $link = $CFG->magic_ui_root.'/'.ltrim($notification->link, '/');
            }

            if ($notification->authorid && !$notification->author) {
                $author = $DB->get_field_sql("SELECT CONCAT(firstname, ' ', lastname) FROM {user} WHERE id=?", array($notification->authorid));
            } else {
                $author = $notification->author;
            }

            $notificationstxt .= monorail_template_compile($template_notification, array(
                'author' => $author,
                'url' => $link,
                'action' => $notification->content,
                'date' => userdate($notification->time, "%d.%m.%Y, %H:%M", $tz)
            ));
        }
        $txt .= monorail_template_compile($template_notification_course, array(
            'coursename' => $notification->coursename,
            'notifications' => $notificationstxt
        ));
    }
    return $txt;
}

function monorailfeed_format_mail_subject($notifications, $lang='en') {
    if (count($notifications) === 1) {
        $subject = str_replace('{%coursename%}', $notifications[0]->coursename, get_string_manager()->get_string('email_subject', 'theme_monorail', null, $lang));
    } else {
        $subject = get_string_manager()->get_string('email_subject_daily', 'theme_monorail', null, $lang);
    }
    return $subject;
}

function monorailfeed_send_instant_notif($userid, $courseid, $typename, $notification_id, $component) {
    global $DB;
    try {
        $usersettings = monorailfeed_user_email_settings($userid, null, true);
        if ($usersettings[monorailfeed_type_to_setting($typename, $component)] == 2) {
            require_once(dirname(__FILE__) . '/../../theme/monorail/lib.php');
            $user = $DB->get_record('user', array('id'=>$userid), 'lang, timezone');
            $notifstosend = array(
                monorailfeed_getnotification($notification_id, $user->lang)
            );
            monorail_send_notifications(
                monorailfeed_format_mail_subject($notifstosend, $user->lang),
                monorailfeed_format_mail_notifs($notifstosend, $user->timezone),
                $userid,
                $user->lang
            );
            monorailfeed_mark_sent($notification_id, $userid);
        }
    } catch (Exception $ex) {
        add_to_log($courseid, 'monorailfeed', 'lib.monorailfeed_create_notification', 'EXCEPTION '.get_class($ex).': '.$ex->getMessage());
    }
}

function monorailfeed_remove_notification($id) {

	require_once(dirname(__FILE__) . '/../../config.php');
	global $DB;

    if (isset($id) && $id>0) {
        try {
            // first remove the users rows
            try {
                $DB->delete_records('monorailfeed_users', array('monorailfeednotificationsid'=>$id));
            } catch (Exception $ex) {
                // no user rows
            }
            // then remove actual notification
            $DB->delete_records('monorailfeed_notifications', array('id'=>$id));
            add_to_log(1, 'monorailfeed', 'remove_notification', '', 'Removed notification->id '.$id);
        } catch (Exception $ex) {
            add_to_log(1, 'monorailfeed', 'remove_notification', '', 'Error! Failed to remove notification->id '.$id.': '.$ex->getMessage());
        }
    }
}

/* mode 0 = all, 1 = unread only, 2 = read only */
function monorailfeed_get_user($userid, $mode=1, $count=0) {
	global $DB;

    // Get ids of courses user is enroled to.
    $courses = enrol_get_all_users_courses($userid, true, "id");
    if (count($courses)) {
        $userCourses = implode(", ", array_keys($courses));
        $params = array();
        $returncolumns = "mn.id, mn.courseid, mn.type, mn.instance, mn.component, mn.revtableid, mn.createdby, mu.timeread, mn.timemodified";
        $query = "select count(1) from {monorailfeed_notifications} mn, {monorailfeed_users} mu where ";
        if ($mode == 1) {
            $query .= "mu.timeread = 0 AND ";
        } else if ($mode == 2) {
            $query .= "mu.timeread != 0 AND ";
        }
        $query .= "mn.courseid IN ($userCourses) AND ";
        $query .= "mu.userid = ? AND ";
        $params[] = $userid;
        $query .= "mu.monorailfeednotificationsid = mn.id ";
        $maxcount = $DB->count_records_sql($query, $params);
        $query .= "order by mn.id desc ";
        if ($count) {
            $query .= "limit $count ";
        }
        $query =  str_replace("count(1)", $returncolumns, $query);
        $notifications = $DB->get_records_sql($query, $params);
    	return array(
            "notifications" => $notifications,
            "moreavailable" => ($maxcount > count($notifications)) ? true : false,
        );
    } else {
        return array(
            "notifications" => array(),
            "moreavailable" => false,
        );
    }
}

function monorailfeed_get_unsent_user($userid, $fromtime) {
	global $DB;

    // Get ids of courses user is enroled to.
    $userCourses = implode(", ", array_keys(enrol_get_all_users_courses($userid, true, "id")));
    if (strlen($userCourses) > 0) {
        $query = <<<EOD
            select mn.id, mn.courseid, mn.type, mn.instance, mn.component, mn.revtableid, mn.createdby from {monorailfeed_notifications} mn, {monorailfeed_users} mu where
                mu.timesent = 0 AND
                mu.timeread = 0 AND
                mn.timemodified > ? AND
                mn.courseid IN ($userCourses) AND
                mu.userid = ? AND
                mu.monorailfeednotificationsid = mn.id
                    order by mn.id desc
EOD;
        $notifications = $DB->get_records_sql($query, array($fromtime, $userid));
    } else {
        $notifications = array();
    }
	return $notifications;
}

function monorailfeed_get_unread_instance($type, $instance) {
	global $DB;
	$query = <<<'EOD'
		select mn.id, mn.courseid, mu.userid, mn.component, mn.createdby from {monorailfeed_notifications} mn, {monorailfeed_users} mu where
            mn.type = ? AND
			mu.timeread = 0 AND
            mn.instance = ? AND
			mu.monorailfeednotificationsid = mn.id
				order by mn.id desc
EOD;
	$notifications = $DB->get_records_sql($query, array($type, $instance));
	return $notifications;
}

function monorailfeed_get_all_instance($type, $instance) {
	global $DB;
	$query = <<<'EOD'
		select mn.id, mn.courseid, mu.userid, mn.component, mn.createdby from {monorailfeed_notifications} mn, {monorailfeed_users} mu where
            mn.type = ? AND
            mn.instance = ? AND
			mu.monorailfeednotificationsid = mn.id
				order by mn.id desc
EOD;
	$notifications = $DB->get_records_sql($query, array($type, $instance));
	return $notifications;
}

function monorailfeed_json_user($userid) {
    global $DB;
    // build json
    $notifications = monorailfeed_get_user($userid, 1);
    $notifications = $notifications["notifications"];
    // if no notifications, just empty result will do
    if (count($notifications) > 0) {
        $json_str = '';
        $courses= array();
        $course_total = 0;
        $tasks_total = 0;
        $material_total = 0;
        $total = 0;
        $sections_total = 0;
        $calendar_total = 0;
        $teacher_total = 0;

        // total
        $json = array('c'=>
                    array('a' => 0,
                        'c'=>0,
                        't'=>0,
                        's'=>0,
                        'm'=>0,
                        'e'=>0,
                        'u'=>0)
                    );
        // loop notifications and place
        foreach ($notifications as $notification) {
            if (@$notification->createdby == $userid) {
                continue;
            }
            try {
                $json[$notification->id] = array(
                    't'=>    $notification->type,
                    'o'=> $notification->component,
                    'c'=> $notification->courseid,
                    'i'=>      $notification->id,
                    'd'=> $notification->instance,
                    'b'=> $notification->createdby);
                if (strpos($notification->type, 'task') !== false) {
                    // task, we need to fill info about it as UI might not know about it
                    // information might be deleted from moodle core db, let's use our revision tables
                    $query = <<<EOD
                        select name,coursesectionsid from {monorailfeed_rev_{$notification->component}} where
                            id = (select max(id) from {monorailfeed_rev_{$notification->component}} where coursemodulesid = ?)
EOD;
                    try {
                        $revision = $DB->get_record_sql($query, array($notification->instance), MUST_EXIST);
                    } catch (Exception $ex) {
                        continue;
                    }
                    if (isset($revision->coursesectionsid)) {
                        try {
                            $section = $DB->get_record('course_sections', array('id'=>$revision->coursesectionsid), 'section', MUST_EXIST);
                        } catch (Exception $ex) {}
                        $json[$notification->id]['s'] = $section->section;
                    } else {
                        $json[$notification->id]['s'] = 0;
                    }
                } else if ($notification->type == 'grade' || $notification->type == 'comment') {
                    // attach grade info to notification
                    $query = <<<EOD
                        SELECT gg.finalgrade, gi.grademax, gi.gradepass
                           FROM {grade_grades} gg,
                                {grade_items} gi
                          WHERE gg.userid = ?
                                AND gi.idnumber = ?
                                AND gg.itemid = gi.id
EOD;
                    try {
                        $grade = $DB->get_record_sql($query, array($userid, $notification->instance), MUST_EXIST);
                    } catch (Exception $ex) {
                        continue;
                    }
                    $json[$notification->id]['gm'] = $grade->grademax;
                    $json[$notification->id]['gf'] = $grade->finalgrade;
                    if ($grade->gradepass != null) {
                        $json[$notification->id]['gp'] = $grade->gradepass;
                    } else {
                        $json[$notification->id]['gp'] = '-';
                    }
                } else if ($notification->type == 'section') {
                    // add section number to notification
                    try {
                        $section = $DB->get_record('course_sections', array('id'=>$notification->instance), 'name, summary, section', MUST_EXIST);
                    } catch (Exception $ex) {
                        continue;
                    }
                    $json[$notification->id]['s'] = $section->section;
                    $json[$notification->id]['sn'] = $section->name;
                    $json[$notification->id]['ss'] = $section->summary;
                } else if ($notification->type == 'forum') {
                    // add discussion and forum ids
                    $query = <<<EOD
                        select fp.discussion, fd.forum, fp.userid, fp.message, fp.created
                            from {forum_posts} fp join {forum_discussions} fd on fp.discussion = fd.id
                            where
                                fp.id = ?
EOD;
                    try {
                        $postdata = $DB->get_record_sql($query, array($notification->instance), MUST_EXIST);
                    } catch (Exception $ex) {
                        continue;
                    }
                    $json[$notification->id]['f'] = $postdata->forum;
                    $json[$notification->id]['di'] = $postdata->discussion;
                    $json[$notification->id]['ui'] = $postdata->userid;
                    $json[$notification->id]['r'] = $postdata->created;
                } else if ($notification->type == 'calendar') {
                    // add some event data
                    if ($notification->component != 'event_del') {
                        try {
                            $event = $DB->get_record('event', array('id'=>$notification->instance), 'name, description, timestart, timeduration', MUST_EXIST);
                        } catch (Exception $ex) {}
                    } else {
                        $event = monorailfeed_get_revision('event', $notification->instance);
                    }
                    $json[$notification->id]['n'] = $event->name;
                    $json[$notification->id]['e'] = $event->description;
                    $json[$notification->id]['s'] = $event->timestart;
                    $json[$notification->id]['u'] = $event->timeduration;
                    $json[$notification->id]['ed'] = date('j', $event->timestart);
                    $json[$notification->id]['em'] = date('n', $event->timestart);
                    $json[$notification->id]['ey'] = date('Y', $event->timestart);
                } else if ($notification->type == 'material') {
                    try {
                        $cm = get_coursemodule_from_id('resource', $notification->instance, $notification->courseid, false, MUST_EXIST);
                        // get section number
                        $section = $DB->get_record('course_sections', array('id'=>$cm->section), 'section', MUST_EXIST);
                    } catch (Exception $ex) {
                        continue;
                    }
                    $json[$notification->id]['s'] = $section->section;
                }
                // do counting here so any errors will skip this part
                if (!isset($courses[$notification->courseid])) {
                    $courses[$notification->courseid] = array('c'=>0, 's'=>0, 'a'=>0, 't'=>0, 'f'=>0, 'm'=>0, 'u'=>0);
                }
                // course counts
                if (strpos($notification->type, 'task') !== false || $notification->type == 'grade' || $notification->type == 'comment') {
                    $courses[$notification->courseid]['t']++;
                    $tasks_total++;
                } else if ($notification->type == 'forum') {
                    $courses[$notification->courseid]['f']++;
                } else if ($notification->type == 'course') {
                    $courses[$notification->courseid]['c']++;
                } else if ($notification->type == 'section') {
                    $courses[$notification->courseid]['s']++;
                    $sections_total++;
                } else if ($notification->type == 'material') {
                    $courses[$notification->courseid]['m']++;
                    $material_total++;
                } else if ($notification->type == 'teacher') {
                    $courses[$notification->courseid]['u']++;
                    $teacher_total++;
                }
                // totals
                if ($notification->type == 'calendar') {
                    $calendar_total++;
                    $total++;
                } else {
                    $courses[$notification->courseid]['a']++;
                    $course_total++;
                    $total++;
                }
            } catch (dml_missing_record_exception $ex) {
                // remove
                add_to_log(1, 'monorailfeed', 'monorailfeed_json_user', '', 'Error getting data for user '.$userid.', notification '.$notification->id.', will remove the notification');
                monorailfeed_remove_notification($notification->id);
                unset($json[$notification->id]);
                continue;
            } catch (Exception $ex) {
                // let's not fail because one notif crashes, log instead
                add_to_log(1, 'monorailfeed', 'monorailfeed_json_user', '', 'Error for user '.$userid.'!: '.get_class($ex).' - '.$ex->getMessage());
            }
        }
        $json['c']['o'] = $courses;
        $json['c']['c'] = $course_total;
        $json['c']['t'] = $tasks_total;
        $json['c']['s'] = $sections_total;
        $json['c']['e'] = $calendar_total;
        $json['c']['m'] = $material_total;
        $json['c']['u'] = $teacher_total;
        $json['c']['a'] = $total;
        $jsonStr = json_encode($json);
    } else {
        return "[]";;
    }
	return $jsonStr;
}

function monorailfeed_get_notifications($userid) {
    global $DB;
    // get last access
    try {
        $useraccess = $DB->get_record('monorailfeed_user_access', array('userid'=>$userid), '*', MUST_EXIST);
        $since = $useraccess->lastaccess;
    } catch (Exception $ex) {
        // first connect! get user created date, create record in user access table
        $since = $DB->get_field('user', 'timecreated', array('id'=>$userid), MUST_EXIST);
        if ($since == 0) {
            // no valid time created, set timemodified instead
            $since = $DB->get_field('user', 'timemodified', array('id'=>$userid), MUST_EXIST);
        }
        $record = new stdClass();
        $record->userid = $userid;
        $record->lastaccess = $since;
        $DB->insert_record('monorailfeed_user_access', $record, false);
        $useraccess = $DB->get_record('monorailfeed_user_access', array('userid'=>$userid), '*', MUST_EXIST);
    }

    // get new lastaccess for timestamp upto
    $upto = time();

    $output = "[]";

    // get unread notifications json
    try {
        $output = monorailfeed_json_user($userid);
    } catch (Exception $ex) {
        add_to_log(1, 'monorailfeed', 'lib.monorailfeed_get_notifications', '', 'Error! Failed to create notification json for user '.$userid.': '.$ex->getMessage());
    }

    try{
        // update last access for user
        $useraccess->lastaccess = $upto+1;
        $DB->update_record('monorailfeed_user_access', $useraccess);
    } catch (Exception $ex) {
        add_to_log(1, 'monorailfeed', 'lib.monorailfeed_get_notifications', '', 'Error! Failed to update last access timestamp for user '.$userid.': '.$ex->getMessage());
    }

    return $output;
}

function monorailfeed_mark_read($notificationid, $userid) {
	global $DB;
	try {
		$DB->set_field('monorailfeed_users', 'timeread', time(), array('userid'=>$userid, 'monorailfeednotificationsid'=>$notificationid));
	} catch (Exception $ex) {
		add_to_log(1, 'monorailfeed', 'lib.mark_read', '', 'Error! Failed to mark notification as read user '.$userid.', notificationid '.$notificationid.': '.$ex->getMessage());
	}
}

function monorailfeed_mark_sent($notificationid, $userid) {
	global $DB;
	try {
		$DB->set_field('monorailfeed_users', 'timesent', time(), array('userid'=>$userid, 'monorailfeednotificationsid'=>$notificationid));
	} catch (Exception $ex) {
		add_to_log(1, 'monorailfeed', 'lib', 'mark_sent', 'Error! Failed to mark notification as sent, user '.$userid.', notificationid '.$notificationid.': '.$ex->getMessage());
	}
}

function monorailfeed_mark_all_read($userid) {
	global $DB;
	try {
		$DB->set_field('monorailfeed_users', 'timeread', time(), array('userid'=>$userid, 'timeread'=>0));
	} catch (Exception $ex) {
		add_to_log(1, 'monorailfeed', 'lib.mark_all_read', '', 'Error! Failed to mark all notifications as read user '.$userid.': '.$ex->getMessage());
	}
}

function monorailfeed_mark_all_sent($userid) {
	global $DB;
	try {
		$DB->set_field('monorailfeed_users', 'timesent', time(), array('userid'=>$userid, 'timesent'=>0, 'timeread'=>0));
	} catch (Exception $ex) {
		add_to_log(1, 'monorailfeed', 'lib', 'mark_all_sent', 'Error! Failed to mark all notifications as sent, user '.$userid.': '.$ex->getMessage());
	}
}

/**
 * Fetch feed
 *
 * @param int $count number of feed item
 * @param string|null $beforedate all feed item must be created before this time
 * @return null|array
 */
/* TODO: Used only by huxley webservices - refactoring needed of those */
function monorailfeed_compilefeed($count, $beforedate = null, $afterdate = null) {
	global $CFG, $PAGE, $OUTPUT, $DB, $USER;

    // Get ids of courses user is enroled to.
    $userCourses = implode(", ", array_keys(enrol_get_all_users_courses($USER->id, true, "id")));

	$datecondition = '';
	if ($beforedate) {
		$datecondition = "AND mu.timemodified < $beforedate";
	}
	if ($afterdate) {
		$datecondition .= " AND mu.timemodified > $afterdate";
	}

	$query = <<<EOD
		SELECT mn.id, mn.courseid, mn.type, mn.instance, mn.component, mu.timemodified, mu.timeread, mn.revtableid, mn.createdby
		FROM {monorailfeed_notifications} mn, {monorailfeed_users} mu
		WHERE
			mu.userid = ? AND
            mn.courseid IN ($userCourses) AND
			mu.monorailfeednotificationsid = mn.id $datecondition AND
            mn.type != 'event_del' AND
            mn.type != 'faded_del'
		ORDER BY mn.id desc
EOD;
	$notifications = $DB->get_records_sql($query, array($USER->id), 0, $count);

	$feed = array();
    $usercache = array(array('Moodle', ''));
    $PAGE->set_context(null);
	foreach ($notifications as $notification) {
        try {
            $course = $DB->get_record('course', array('id'=>$notification->courseid));
            $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$notification->courseid));

            if ($notification->type == 'faded_del' || $notification->type == 'task_del') {
                continue;
            }

            $item = monorailfeed_notification_content($notification, $courseData);

            $item->coursename = $course->fullname;

            // author
            if (! isset($item->authorid))
                $item->authorid = 0;

            if ($item->authorid == $USER->id)
            {
                // Don't show any notifications about actions by current user.
                continue;
            }

            if (isset($usercache[$item->authorid])) {
                $item->author = $usercache[$item->authorid][0];
                $item->userpic = $usercache[$item->authorid][1];
                if (isset($usercache[$item->authorid][2])) {
                    $item->picurl = $usercache[$item->authorid][2];
                }
                if (isset($usercache[$item->authorid][3])) {
                    $item->authroles = $usercache[$item->authorid][3];
                }
            } else {
                try {
                    $user = $DB->get_record('user', array('id' => $item->authorid));
                    require_once($CFG->dirroot.'/user/lib.php');
                    $userdata = user_get_user_details($user, $course, array('id', 'fullname', 'profileimageurl', 'roles'));
                    $item->picurl = $userdata['profileimageurl'];
                    $item->author = $userdata['fullname'];
                    $item->authroles = $userdata['roles'];
                    $avatar = new user_picture($user);
                    $avatar->size = 40;
                    $item->userpic = $OUTPUT->render($avatar);
                    $usercache[$item->authorid] = array($item->author, $item->userpic, $item->picurl, $item->authroles);
                } catch (Exception $ex) {
                    $item->author = $usercache[$item->authorid][0];
                    $item->userpic = $usercache[$item->authorid][1];
                    $item->picurl = $usercache[$item->authorid][2];
                }
            }

            $feed[] = $item;
        } catch (Exception $ex) {
            // log but don't die
            add_to_log(1, 'monorailfeed', 'lib.monorailfeed_compilefeed', '', 'Error! User '.$USER->id.': Notification '.$notification->id.' data fetching failed: '.$ex->getMessage());
        }
	}
	return $feed;
}

function monorailfeed_sort_news_by_time(stdClass $a, stdClass $b) {
	if (!$a->time) return -1;
	if (!$b->time) return 1;
	if ( $a->time < $b->time ) return 1;
	if ( $a->time > $b->time ) return -1;
	return 0; // equality
}

/**
 * Map notification type to notification setting
 *
 * @param $type Notification type (string)
 * @param $component Notification component (string)
 * @returns string Setting type string
 */
function monorailfeed_type_to_setting($type, $component) {
    switch ($type) {
        case "task_upd":
        case "task_new":
        case "task_del":
        case "bbb_upd":
        case "bbb_new":
        case "bbb_del":
            return 'task';
        case "calendar":
            return 'event';
        case "teacher":
            return $component;
        default:
            return $type;
    }
}

/**
 * Get or save user notification email settings
 *
 * @param $userid ID of user
 * @param $newsettings Array, optional, if changing settings
 * @return array of object (always)
 */
function monorailfeed_user_email_settings($userid, array $newsettings=null, $wantarray=false) {
    global $DB;

    $NOTIFICATION_EMAIL_SETTINGS = array(
        'task' => 1,
        'grade' => 1,
        'comment' => 1,
        'event' => 1,
        'forum' => 1,
        'section' => 1,
        'material' => 1,
        'user_enrolled' => 1,
        'user_submitted' => 1,
        'user_completed' => 1,
        'platform_updates' => 1,
    );

    try {
        if (isset($newsettings)) {
            throw new Exception();  // we want to update
        }
        $settings = $DB->get_records('monorailfeed_user_settings', array('userid'=>$userid), null, '*');
        if (count($settings) === 0) {
            throw new Exception();  // init
        }
    } catch (Exception $ex) {
        // defaults
        if (isset($newsettings)) {
            // use newsettings while adding any missing ones from defaults
            $settings = array_merge($NOTIFICATION_EMAIL_SETTINGS, $newsettings);
        } else {
            $settings = $NOTIFICATION_EMAIL_SETTINGS;
        }
        // save
        foreach ($settings as $type => $value) {
            try {
                $record = new stdClass();
                $record->userid = $userid;
                $record->type = $type;
                $record->value = $value;
                $DB->insert_record('monorailfeed_user_settings', $record);
            } catch (Exception $exs) {
                // try update instead
                try {
                    $record = $DB->get_record('monorailfeed_user_settings', array('userid'=>$userid, 'type'=>$type), '*', MUST_EXIST);
                    $record->value = $value;
                    $DB->update_record('monorailfeed_user_settings', $record);
                } catch (Exception $exu) {
                    add_to_log(1, 'monorailfeed', 'lib.user_email_settings', '', 'Failed inserting or updating notification setting: user '.$userid.', type '.$type.' value '.$value.': '.$exs->getMessage());
                }
            }
        }
        // get as objects
        $settings = $DB->get_records('monorailfeed_user_settings', array('userid'=>$userid));
    }
    if ($wantarray) {
        // return array instead
        $arr = array();
        foreach ($settings as $setting) {
            $arr[$setting->type] = $setting->value;
        }
        $settings = $arr;
    }
    return $settings;
}

/**
 * Add notification content for feed or sending.
 *
 * @param $notification Notification record (object)
 * @param $courseData course code data record
 * @param $lang language code or null for current
 * @returns object
 */
function monorailfeed_notification_content($notification, $courseData, $lang=null) {
	global $CFG, $DB;
    $item 			= new stdClass();
    $item->id       = $notification->id;
    $item->time		= $notification->timemodified ;
    $item->type		= 'notifications';
    if (isset($notification->timeread)) {
        $item->read     = $notification->timeread ? true : false;
    }
    $item->content  = '';
    $item->sectionid = null;

    switch ($notification->type) {
        case 'calendar':
            $item->content .= get_string_manager()->get_string($notification->component, 'theme_monorail', null, $lang);

            try {
                require_once($CFG->dirroot.'/calendar/lib.php');
                $event = calendar_event::load($notification->instance);
            } catch (Exception $ex) {
                $event = monorailfeed_get_revision('event', $notification->instance);
            }
            $date  = new DateTime();
            $date->setTimestamp($event->timestart);

            $d = $date->format('d');
            $m = $date->format('m');
            $y = $date->format('Y');

            //$item->link = "../calendar/view.php?view=day&course={$notification->courseid}&cal_d=$d&cal_m=$m&cal_y=$y";
            $item->link = "/calendar/{$y}{$m}";

            $item->authorid = $notification->createdby;
            break;

        case 'forum':
            $item->content .= get_string_manager()->get_string($notification->component, 'theme_monorail', null, $lang);

            require_once($CFG->dirroot.'/mod/forum/lib.php');
            $post = forum_get_post_full($notification->instance);

            //$item->link = '../theme/monorail/forum.php?d='.$post->discussion.'#post'.$notification->instance;
            $item->link = "/courses/" . $courseData->code . "/discussions/" . $post->forum . "/" . $post->discussion;

            $item->authorid = $notification->createdby;
            break;

        case 'section':
            $item->content .= get_string_manager()->get_string($notification->component, 'theme_monorail', null, $lang);

            if (isset($notification->revtableid)) {
                try {
                    $revision = $DB->get_record('monorailfeed_rev_sections', array('id'=>$notification->revtableid), '*', MUST_EXIST);
                    $section = $DB->get_record('course_sections', array('id'=>$revision->coursesectionsid), '*', MUST_EXIST);
                    $sectionnum = $section->section;
                } catch (Exception $ex) {
                    $sectionnum = null;
                }
            }
            $item->authorid = $notification->createdby;
            if ($sectionnum > 0)
            {
                $item->link = '/courses/' .$courseData->code . "/" . $sectionnum;
                $item->content .= " " . $section->name;
            }
            else
            {
                $item->link = '/courses/' .$courseData->code;
                $item->content .= " " . get_string_manager()->get_string("overview_topic", "theme_monorail", null, $lang);
            }
            $item->sectionid = $sectionnum;
            break;

        case 'task_new':
        case 'task_upd':
            $item->content .= get_string_manager()->get_string($notification->type, 'theme_monorail', null, $lang);
            $item->link = '/tasks/' .$notification->instance;
            $item->authorid = $notification->createdby;
            break;

        case 'material':
            require_once($CFG->dirroot.'/theme/monorail/lib.php');
            $item->content .= get_string_manager()->get_string($notification->type, 'theme_monorail', null, $lang);

            $cm = get_coursemodule_from_id('resource', $notification->instance, $notification->courseid, true, MUST_EXIST);
            $context = get_context_instance(CONTEXT_MODULE, $cm->id);
            $fs = get_file_storage();
            $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0);
            foreach ($files as $file) {
                if ($file->get_filename() != '.') {
                    $item->link = monorail_get_file_url($file);
                    break;
                }
            }
            // externally (from notif email) we want an external working link
            // to course page, not to material directly
            $item->externallink = '/courses/' .$courseData->code;
            $item->authorid = $notification->createdby;
            break;

        case 'teacher':
            switch ($notification->component) {
                case 'user_enrolled':
                    $item->content .= get_string_manager()->get_string($notification->component, 'theme_monorail', null, $lang);
                    $item->link = '/courses/' .$courseData->code. '/participants';
                    $item->authorid = $notification->createdby;
                    break;
                case 'user_submitted':
                    $cm = get_coursemodule_from_id(null, $notification->instance, $notification->courseid, false, MUST_EXIST);
                    $item->content .= get_string_manager()->get_string('user_submitted_'.$cm->modname, 'theme_monorail', null, $lang);
                    $item->link = '/tasks/' .$notification->instance;
                    $item->authorid = $notification->createdby;
                    break;
                case 'user_completed':
                    $item->content .= get_string_manager()->get_string('user_completed_course', 'theme_monorail', null, $lang);
                    $item->link = '/courses/' . $courseData->code;
                    $item->authorid = $notification->createdby;
                    break;
            }
            break;

        case 'grade':
            $item->content .= get_string_manager()->get_string($notification->type, 'theme_monorail', null, $lang);
            $item->link = '/tasks/' .$notification->instance;
            $item->authorid = $notification->createdby;
            break;

        case 'comment':
            $item->content .= get_string_manager()->get_string($notification->type, 'theme_monorail', null, $lang);
            $item->link = '/tasks/' .$notification->instance;
            $item->authorid = $notification->createdby;
            break;

        case "certificate":
            $item->content .= get_string_manager()->get_string($notification->type, 'theme_monorail', null, $lang);
            $item->link = '/courses/' . $courseData->code;
            $item->authorid = $notification->createdby;
            break;

        case 'bbb_new':
        case 'bbb_upd':
        case 'bbb_del':
            $item->content .= get_string_manager()->get_string($notification->type, 'theme_monorail', null, $lang);
            $item->authorid = $notification->createdby;
            $item->link = '/courses/' .$courseData->code . "/liveevent";
            break;
    }
    return $item;
}

/**
 * Fetch notification
 *
 * @param notificationid - notification id
 * @param lang - language or null for current
 * @return null|array
 */
function monorailfeed_getnotification($notificationid, $lang=null) {
	global $CFG, $PAGE, $OUTPUT, $DB, $USER;
        $notification = $DB->get_record('monorailfeed_notifications', array('id'=>$notificationid));
        if($notification) {

            $course = $DB->get_record('course', array('id'=>$notification->courseid));
            try {
                $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$notification->courseid), '*', MUST_EXIST);
            } catch (Exception $ex) {
                // missing monorail_course_data means invalid data, skip this to suppress some warnings and invalid notifications pushed out
                return null;
            }

            $item = monorailfeed_notification_content($notification, $courseData, $lang);

            $item->coursename = $course->fullname;
            $item->courseid = $course->id;

            // author
            if (! isset($item->authorid))
                $item->authorid = 0;

                try {
                    $user = $DB->get_record('user', array('id' => $item->authorid));
                    require_once($CFG->dirroot.'/user/lib.php');
                    $userdata = user_get_user_details($user, context_module::instance(null), array('id', 'fullname', 'profileimageurl'));
                    $item->picurl = $userdata['profileimageurl'];
                    $item->author = $userdata['fullname'];
                } catch (Exception $ex) {
                }
            return $item;
        }
	return null;
}

?>
