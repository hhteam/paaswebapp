<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 



defined('MOODLE_INTERNAL') || die();

function mod_handler($eventdata, $action) {
    try {
        require_once(dirname(__FILE__) . '/../../config.php');
        global $DB, $CFG, $USER;
        
        // assignments and quizzes
        if ($eventdata->modulename == 'assign' || $eventdata->modulename == 'quiz')
        {
            require_once(dirname(__FILE__) . '/lib.php');
            
            // don't create revisions when mod_deleted
            if ($action != 'del') {
                $coursemodule = $DB->get_record('course_modules', array('id'=>$eventdata->cmid), 'id, instance', MUST_EXIST);
                
                // save a new revision
                $revid = monorailfeed_save_revision($eventdata->modulename, $coursemodule->instance, $coursemodule->id, $eventdata->userid);
            } else {
                $rev = monorailfeed_get_revision($eventdata->modulename, $eventdata->instance);
                $revid = $rev->id;
                // when mod_deleted, mark all other notifications as read for this instance
                $notifications = monorailfeed_get_unread_instance($eventdata->modulename, $eventdata->cmid);
                foreach ($notifications as $notification) {
                    monorailfeed_remove_notification($notification->id);
                }
                $notifications = monorailfeed_get_unread_instance('grade', $eventdata->cmid);
                foreach ($notifications as $notification) {
                    if ($notification->component == $eventdata->modulename) {
                        monorailfeed_remove_notification($notification->id);
                    }
                }
            }

            // call API to create notification
            return monorailfeed_create_notification($eventdata->courseid, $eventdata->modulename, 'task_'.$action, $eventdata->cmid, $eventdata->userid, null, $revid);
        }
        else if ($eventdata->modulename == "bigbluebuttonbn")
        {
            // call API to create notification
            return monorailfeed_create_notification($eventdata->courseid, $eventdata->modulename, 'bbb_'.$action, $eventdata->cmid, $eventdata->userid, null, null);
        }
        else if ($eventdata->modulename == 'resource' && $action == 'new')
        {
            // create a notification about file upload
            require_once(dirname(__FILE__) . '/lib.php');
            
            // call API to create notification
            try {
                // file uploads go as material notifications, picture uploads trigger a section notification
                $cm = get_coursemodule_from_id('resource', $eventdata->cmid, $eventdata->courseid, true, MUST_EXIST);
                $context = get_context_instance(CONTEXT_MODULE, $cm->id);
                $fs = get_file_storage();
                $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0);
                foreach ($files as $file) {
                    if ($file->get_filename() != '.') {
                        if (file_mimetype_in_typegroup($file->get_mimetype(), 'web_image')) {
                            // trigger section notification
                            $sectioninfo = $DB->get_record('course_sections', array('course'=>$eventdata->courseid, 'section'=>$cm->sectionnum), '*', MUST_EXIST);
                            $rev = monorailfeed_get_revision('section', $sectioninfo->id);
                            $result = monorailfeed_create_notification($eventdata->courseid, 'section', 'section', $sectioninfo->id, $eventdata->userid, null, $rev->id);
                        } else {
                            // trigger material notification
                            $result = monorailfeed_create_notification($eventdata->courseid, 'file', 'material', $eventdata->cmid, $eventdata->userid);
                        }
                        break;  // one file only
                    }
                }
            } catch (Exception $ex) {
                add_to_log(1, 'monorailfeed', 'handlers.mod_handler', '', 'Error! Failed to create a notification: '.get_class($ex).': '.$ex->getMessage());
                $result = false;
            }
            return true;
        } else if ($eventdata->modulename == 'resource' && $action == 'del') { 
            // remove notifications
            require_once(dirname(__FILE__) . '/lib.php');
            $notifications = $DB->get_records('monorailfeed_notifications', array('instance'=>$eventdata->cmid, 'type'=>'material'), 'id');
            foreach ($notifications as $notif) {
                monorailfeed_remove_notification($notif->id);
            }
            
            // remove converted files
            //TODO
            
            return true;
        } else {
            // return true for everything else
            return true;
        }
    } catch (Exception $ex) {
        add_to_log(1, 'monorailfeed', 'handlers.mod_handler', '', 'Error! Failed to process mod_handler: '.get_class($ex).': '.$ex->getMessage());
    }
}

function mod_created_handler($eventdata) {
	
    $result = mod_handler($eventdata, 'new');
    return $result;
}

function mod_updated_handler($eventdata) {

    return mod_handler($eventdata, 'upd');
}

function mod_deleted_handler($eventdata) {
	
    return mod_handler($eventdata, 'del');
}

function user_enrolled_handler($eventdata) {
    try {
        require_once(dirname(__FILE__) . '/lib.php');

        if (defined("NEW_COHORT_COURSE") && NEW_COHORT_COURSE == $eventdata->courseid)
        {
            // Not notifying about enrollments to new courses...
        }
        else
        {
            monorailfeed_create_notification($eventdata->courseid, 'user_enrolled', 'teacher', $eventdata->userid, $eventdata->userid, null, null, null, array(3));
        }
    } catch (Exception $ex) {
        add_to_log(1, 'monorailfeed', 'handlers.php', 'user_enrolled_handler', 'Error! Failed to process: '.get_class($ex).': '.$ex->getMessage());
    }
    return true;
}


?>
