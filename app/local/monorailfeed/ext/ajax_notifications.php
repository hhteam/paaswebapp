
<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


define("NO_DEBUG_DISPLAY", true);

require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just return empty set and die
	echo "[]";
	die();
}

require_once(dirname(__FILE__) .'/../lib.php');

$output = monorailfeed_get_notifications($USER->id);

echo $output;

?>
