
<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

try {
    $i = required_param('i', PARAM_INT);
} catch (Exception $ex) {
    die();
}

// get data
try {
    $discussion = $DB->get_record('forum_discussions', array('id' => $i), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $discussion->course), '*', MUST_EXIST);
    $forum = $DB->get_record('forum', array('id' => $discussion->forum), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('forum', $forum->id, $course->id, false, MUST_EXIST);
} catch (Exception $ex) {
    die();
}

// check access
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}
$modcontext = get_context_instance(CONTEXT_MODULE, $cm->id);
try {
    require_capability('mod/forum:viewdiscussion', $modcontext, NULL, true, 'noviewdiscussionspermission', 'forum');
} catch (Exception $ex) {
    die();
}

// collect some data
try {
    $reply_count = $DB->get_field_sql('select count(1) as c from {forum_posts} where discussion = ? and parent != 0', array('id'=>$i), 'c', MUST_EXIST);
    $user = $DB->get_record('user', array('id' => $discussion->userid));
    $name = fullname($user);

    $return = array('i'=>$i, 'n'=>$name, 'c'=>$reply_count, 'd'=>$discussion->name);

    echo json_encode($return);
} catch (Exception $ex) {
    die();
}

?>
