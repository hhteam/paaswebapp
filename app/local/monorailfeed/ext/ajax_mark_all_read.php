
<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $SESSION;

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}
require_once(dirname(__FILE__) .'/../lib.php');

// mark as read
// note, we do not need to worry if someone is trying to mark read someone elses
// notification since we are passing logged in user ID
monorailfeed_mark_all_read($USER->id);

// let client know we're ready
echo '[]';

?>
