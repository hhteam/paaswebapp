
<?php
/**
 * Monorail notifications engine
 * 
 * @package   monorailfeed
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


require_once(dirname(__FILE__) . '/../../../config.php');

global $USER, $DB, $SESSION;

try {
    $notificationid = required_param('i', PARAM_INT);
} catch (Exception $ex) {
    die();
}

// verify that user is logged in
try {
	require_login(null, false, null, false, true);
} catch (Exception $ex) {
	// not logged in, just die
	die();
}
require_once(dirname(__FILE__) .'/../lib.php');

// mark as read
// note, we do not need to worry if someone is trying to mark read someone elses
// notification since we are passing logged in user ID
monorailfeed_mark_read($notificationid, $USER->id);

?>
