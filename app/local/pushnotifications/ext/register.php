<?php

/**
 * A script to register device for push notification 
 *
 */
/**
 * AJAX_SCRIPT - exception will be converted into JSON
 */
define('AJAX_SCRIPT', true);

/**
 * NO_MOODLE_COOKIES - we don't want any cookie
 */
define('NO_MOODLE_COOKIES', true);

require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');
require_once(dirname(dirname(__FILE__)) . '/pushnotifications.php');
require_once($CFG->dirroot . '/webservice/lib.php');


//authenticate the user
$token = required_param('token', PARAM_ALPHANUM);
$registerid = required_param('registerid', PARAM_RAW);
$deviceid = required_param('deviceid', PARAM_ALPHANUM);
$userid = required_param('userid', PARAM_INT);

$webservicelib = new webservice();
$authenticationinfo = $webservicelib->authenticate_user($token);

if($userid == $authenticationinfo['user']->id){
    $retval = pushnotifications_register_user($authenticationinfo['user'], $registerid, $deviceid);
}
echo json_encode(array("registered"=>$retval));
