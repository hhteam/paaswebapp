<?php
/**
 * Eliademy.com push notifications 
 * 
 * @package   push notifications 
 * @copyright CBTec Oy
 */ 


defined('MOODLE_INTERNAL') || die;

global $CFG;
require_once($CFG->dirroot . '/lib/moodlelib.php');


function pushnotifications_register_user($user, $registerid, $deviceid) {
    global $CFG, $DB; 
    $record= new stdClass();
    $record->userid = $user->id;
    $record->username = $user->username;
    $record->registerid = $registerid;
    $record->deviceid = $deviceid;
    $euser = $DB->get_record("pushnotifications_users", array("userid"=>$user->id, "deviceid"=>$deviceid)); 
    if($euser) {
        $euser->registerid = $registerid;
        return $DB->update_record('pushnotifications_users', $euser);
    } else {
        if($DB->insert_record('pushnotifications_users', $record) > 0){
            return true;
        } else { 
            return false;
        }
    }
}

function pushnotifications_unregister_user($user, $registerid, $deviceid) {
    global $DB, $CFG;
    $euser = $DB->get_record("pushnotifications_users", array("userid"=>$user->id, "deviceid"=>$deviceid)); 
    if($euser) {
        return $DB->delete_records('pushnotifications_users', array('id'=>$euser->id, "deviceid"=>$deviceid));
    } 
}

function send_pushnotifications_to_users($notification) {
    global $DB, $CFG;
    if ($notification) {
        $notifications = array(); 
        try {
            $notifications['notifications'] = $notification;
            $notificationid = $notification->id;
            $registrationids = array();
            $nusers = $DB->get_records("monorailfeed_users", array("monorailfeednotificationsid" => $notificationid)); 
            foreach ($nusers as $nuser) {
                $pnusers = $DB->get_records("pushnotifications_users", array("userid" => $nuser->userid));
                if (count($pnusers) > 0) {
                    require_once(dirname(__FILE__) .'/../monorailfeed/lib.php');
                    $usersettings = monorailfeed_user_email_settings($nuser->userid, null, true);
                    if ($usersettings[monorailfeed_type_to_setting($notification->type, $notification->component)] == 2) {
                        foreach ($pnusers as $pnuser) {
                            $registrationids[] = $pnuser->registerid;
                        } 
                    }
                }
            }
            if(count($registrationids) > 0){ 
                pushnotifications_send($registrationids, $notifications); 
            }
        } catch (Exception $ex) {
            add_to_log("", 'pushnotifications', 'lib.send_pushnotifications_to_users', 'EXCEPTION '.get_class($ex).': '.$ex->getMessage());
        }   
    }
}

function pushnotifications_send($registerids, $ns) {
    global $DB, $CFG, $USER;
        $headers = array(
            'Authorization: key=' . $CFG->push_gcmapikey,
            'Content-Type: application/json'
        );
        $params = array(
            'registration_ids' => $registerids,
            'data' => $ns,
        );
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $CFG->push_gcmserviceurl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
 
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
}
