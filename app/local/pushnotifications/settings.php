<?php

/**
 * Eliademy.com push notifications
 *
 * @package   push notifications
 * @copyright CBTec Oy
 */

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $settings = new admin_settingpage('local_pushnotifications', 'Push Notifications');
    $ADMIN->add('localplugins', $settings);
    $settings->add(new admin_setting_configtext('push_gcmserviceurl', 'GCM Service url',
                                                'Service url GCM', 'https://android.googleapis.com/gcm/send',
                                                 PARAM_RAW));
    $settings->add(new admin_setting_configtext('push_gcmapikey', 'GCM Api Key',
                                                'API key for GCM service', '',
                                                 PARAM_RAW));
    $settings->add(new admin_setting_configtext('push_gcmsenderkey', 'GCM Sender Key',
                                                'Sender key for GCM service', '',
                                                 PARAM_RAW));
}
