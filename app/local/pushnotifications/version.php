<?php
/**
 * Eliademy.com push notifications plugin 
 * 
 * @package   local_pushnotification 
 * @copyright CBTec Oy
 */ 

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_pushnotifications';
$plugin->version  = 2013070900;
$plugin->requires = 2012061700;
$plugin->dependencies 	= array(
		);
