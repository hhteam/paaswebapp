<?php  // Moodle configuration file

setlocale(LC_ALL, "en_US.UTF8");

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'moodle';
$CFG->dbpass    = 'S1B2@moodle';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbsocket' => false,
);

$CFG->wwwroot   = 'https://mrdi.eliademy.com/app';
$CFG->dataroot  = '/opt/moodledata';
$CFG->admin     = 'admin';
//$CFG->adminMessage     = 'Note, Eliademy has a scheduled maintenance break on Sunday 18.06.2017 at 18:00 EET (UTC+02:00) and the duration will be 6 hours. Until 24:00';


$CFG->directorypermissions = 02777;

$CFG->passwordsaltmain = '123123123';

require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
$CFG->calendar = 'monorail';
$CFG->monorail_video_upload_limit = 524288000; // ~500 mb
$CFG->monorail_upload_limit = 10485760; // ~10 mb
$CFG->magic_ui_root = "https://mrdi.eliademy.com/app/a";
$CFG->magic_ui_dir = "/var/www/app/a";
$CFG->magic_login_url = "https://mrdi.eliademy.com/login";
$CFG->external_data_root = "/var/www/inc/data";
$CFG->landing_page = "https://mrdi.eliademy.com";
$CFG->catalog_url = "https://mrdi.eliademy.com/catalog";

$CFG->emailer_redis_host = "127.0.0.1";
$CFG->emailer_redis_port = 6379;
$CFG->relay_redis_host = "127.0.0.1";
$CFG->relay_redis_port = 6379;

$CFG->redis_host = "127.0.0.1";
$CFG->redis_port = 6379;

$CFG->mage_api_url = "https://mrdi.eliademy.com/catalog/index.php/api/";
$CFG->mage_api_user = "catalog_admin";
$CFG->mage_api_key = "bie6au4Eidaim8CeeMei2";
$CFG->mage_ips = array("10.2.10.97", "2a04:3541:1000:500:d837:3dff:fe46:16b3","94.237.54.41","127.0.0.1");

$CFG->mage_catmap = array(
        1 => 38,    // Social science
        2 => 39,    // Arts'n design
        3 => 35,    // Business'n law
        4 => 37,    // Technology
        7 => 41,    // Education
        9 => 42,    // Life science
        10 => 43,   // Physical sciences
        11 => 44,   // Philosophy
        12 => 45,   // Humanities
        13 => 46,   // Languages
        15 => 47);  // Other

$CFG->mage_cc_category = 54;

$CFG->twitterappid = "fItH7YhBJ8iHpk4MLWH3mPl2E";
$CFG->twitterappsecret = "CLmXT34E1asfGnpwQApBLb1sG1g72r5kbzs7o3HyY28QJN3zvS";
$CFG->FBAppID = "109786629688133";

$CFG->tpp_papagei_secret = "1b26ce59a73806dda6a52cc1d6c7e6290823c395";
$CFG->tpp_papagei_key = "e550fdaeffd8684dcc24d316436f7736";

//Demo account (rajeev@eliademy.com)

$CFG->braintree_environment = "sandbox";
$CFG->braintree_merchantId = "tds3kw8ry8bcqm4c";
$CFG->braintree_publicKey = "tstgmwb4w3m453ys";
$CFG->braintree_privateKey = "5435b5b7e0d99ad7cd50aab2343cbc79";




/* Demo account (aurelijus.b@gmail.com)

$CFG->braintree_environment = "sandbox";
$CFG->braintree_merchantId = "x3gjc7ptc33wjvy3";
$CFG->braintree_publicKey = "qr9cwdk7bmmzj6br";
$CFG->braintree_privateKey = "79fc83357f55311d713a33ebbca07b63";
*/

// Demo account
/*$CFG->braintree_environment = "sandbox";
$CFG->braintree_merchantId = "wznn6zmj3czn6xbn";
$CFG->braintree_publicKey = "dpdq88ctj72sz5km";
$CFG->braintree_privateKey = "536414458635c0b5bdce87e1279f3b4a";
*/
// Real account
/*$CFG->braintree_environment = "production";
$CFG->braintree_merchantId = "4ckkbsddn3kzxqxz";
$CFG->braintree_publicKey = "m8jw7s2cgm59tgrp";
$CFG->braintree_privateKey = "a860ae5abed5c4cee45a2e9172764936";
*/

// VAT for EU countries
$CFG->vat_percent = array(
    "BE" => 21,
    "BG" => 20,
    "CZ" => 21,
    "DK" => 25,
    "DE" => 19,
    "EE" => 20,
    "EL" => 23,
    "GR" => 23,	// XXX: EL = GR
    "ES" => 21,
    "FR" => 20,
    "HR" => 25,
    "IE" => 23,
    "IT" => 22,
    "CY" => 19,
    "LV" => 21,
    "LT" => 21,
    "LU" => 17,
    "HU" => 27,
    "MT" => 18,
    "NL" => 21,
    "AT" => 20,
    "PL" => 23,
    "PT" => 23,
    "RO" => 24,
    "SI" => 22,
    "SK" => 20,
    "FI" => 24,
    "SE" => 25,
    "UK" => 20,
    "GB" => 20  // XXX: UK = GB
);

// Price of 1 user/month of Pro.
$CFG->pro_licence_price = 1.0;
$CFG->my_host = "mrdi.eliademy.com";

// Production account
$CFG->mprint_host = "orders.metidaprint.com";
$CFG->mprint_user = "info@cloudberrytec.com";
$CFG->mprint_password = "RedeltIted6";

$CFG->paypal_userid = "AR4oNhCCKZNnNQ3yHlVJ6BsPfwKBQ1I4hFR0oYTRgHxf5IL_ptvxVlDrQL3v";
$CFG->paypal_secret = "EOHy9RDpvjdrupsgFwXq01NE3hS09rv-n8QejqWiD42VDI_Vd81wyX0eY7IC";

$CFG->cert_price = 30.0;

# Capptcha stuff
$CFG->solvemedia_challenge_key = "SzrqBRYFSWbFw28Jox99v-y911SY3O8l"; //public
$CFG->solvemedia_verification_key = "QwkFNhmNq9YdiO5XLTG7bKTisJgVYHUY"; //private
$CFG->solvemedia_auth_hash_key = "1NrNvcrnGxCI962pvM-11qPV3hGc8Ovn";

