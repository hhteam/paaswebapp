<?php

/*
 * Get facebook code and call the normal login page
 * Needed to add the parameter authprovider in order to identify the authentication provider
 */
require('../../config.php');
require_once($CFG->libdir . '/filelib.php');
require_once("$CFG->dirroot/theme/monorail/sociallib.php");

global $USER, $DB, $SESSION, $CFG, $PAGE;

try {
    require_login(null, false, null, false, true);
} catch (Exception $ex) {
    // not logged in, just die
    die();
}

$code = optional_param('code', '', PARAM_TEXT); 
$error = optional_param('error', '', PARAM_TEXT); //FB can return an error
$state = optional_param('state', '', PARAM_TEXT); //FB can return an error

if(!empty($error)) {
    $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script>
 self.close();
</script>
<body>
</body>
</html>
HTML;
echo $text;
die();
}

$curl = new curl();
$params = array();
$picurl = '';
$spic = false;
if (!empty($code)) {
    $params['client_id'] = get_config('auth/googleoauth2', 'facebookclientid');
    $params['client_secret'] = get_config('auth/googleoauth2', 'facebookclientsecret');
    $requestaccesstokenurl = 'https://graph.facebook.com/oauth/access_token';
    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/facebook.php';
    $params['code'] = $code;    
    $postreturnvalues = $curl->post($requestaccesstokenurl, $params);
    parse_str($postreturnvalues, $returnvalues);
    $accesstoken = $returnvalues['access_token'];
    $expires = $returnvalues['expires'];
    $postallowed = social_facebook_allowed_permission($accesstoken,'publish_actions');
    unset($postreturnvalues);
    $postreturnvalues = null;
   if(!empty($accesstoken) && !empty($state)) {
       if(strcmp(substr($state, 0, 2),'p_') == 0) {
          $params = array();
          $params['access_token'] = $accesstoken;
          $postreturnvalues = $curl->get('https://graph.facebook.com/me', $params);
          $postreturnvalues = json_decode($postreturnvalues);
           $spic = true;
           $picurl = "https://graph.facebook.com/". $postreturnvalues->username . "/picture?type=large";
           if (!file_exists($CFG->dataroot.'/temp/files/'.$USER->id.'/')) {
               mkdir($CFG->dataroot.'/temp/files/'.$USER->id.'/');
           }
           // save picture to temp files
           $result = copy($picurl, $CFG->dataroot.'/temp/files/'.$USER->id. '/' . $USER->id.'socialprofilepicture');
       } 
   }

   if (!empty($accesstoken) && $postallowed) {
       if($DB->record_exists('monorail_social_keys', array('userid'=>$USER->id, 'social' => 'facebook'))){
          $vrec = $DB->get_record('monorail_social_keys', array('userid'=>$USER->id, 'social' => 'facebook'), '*', MUST_EXIST);
          $vrec->accesstoken  = $accesstoken;
          $vrec->expires  = $expires;
          $vrec->timemodified  = time();
          $DB->update_record('monorail_social_keys', $vrec);
       } else {
           $vfdata = new stdClass();
           $vfdata->social  = 'facebook';
           $vfdata->userid  = $USER->id;
           $vfdata->accesstoken  = $accesstoken;
           $vfdata->expires  = $expires;
           $vfdata->timemodified  = time();
           $DB->insert_record('monorail_social_keys', $vfdata);
       }
       if(empty($postreturnvalues)) {
          $params = array();
          $params['access_token'] = $accesstoken;
          $postreturnvalues = $curl->get('https://graph.facebook.com/me', $params);
          $postreturnvalues = json_decode($postreturnvalues);
       }
       if (array_key_exists ('username', $postreturnvalues)) {
         $vrec = $DB->get_record('monorail_social_settings', array('userid'=>$USER->id, 'social' => 'facebook', 'datakey' => 'account'));
         if(!$vrec) {
           $vrec = new stdClass();
           $vrec->social  = 'facebook';
           $vrec->userid  = $USER->id;
           $vrec->timemodified  = time();
           $vrec->datakey = 'account';
           $vrec->value = $postreturnvalues->username;
           $DB->insert_record('monorail_social_settings',$vrec);
         }
       } 
       if (array_key_exists ('link', $postreturnvalues)) {
         $vrec = $DB->get_record('monorail_social_settings', array('userid'=>$USER->id, 'social' => 'facebook', 'datakey' => 'accounturl'));
         if(!$vrec) {
           $vrec = new stdClass();
           $vrec->social  = 'facebook';
           $vrec->userid  = $USER->id;
           $vrec->timemodified  = time();
           $vrec->datakey = 'accounturl';
           $vrec->value = $postreturnvalues->link;
           $DB->insert_record('monorail_social_settings',$vrec);
         }
       } 
       if (array_key_exists ('email', $postreturnvalues)) {
         $vrec = $DB->get_record('user', array('id'=>$USER->id));
         if($vrec && (strcmp($postreturnvalues->email, $vrec->username) == 0) && (strcmp($vrec->auth, 'googleoauth2') != 0)) {
           $vrec->auth = 'googleoauth2';
           $DB->update_record('user',$vrec);
         }
       } 
       $settings = array('share_enroll','share_complete','share_certificate','share_review');
       foreach($settings as $setting) {
           $record = $DB->get_record('monorail_social_settings', array('userid' => $USER->id, 'social' => 'facebook', 'datakey' => $setting));
            if(!$record) {
               $record = new stdClass();
               $record->value = 1;
               $record->datakey = $setting;
               $record->userid = $USER->id;
               $record->social = 'facebook';
               $record->timemodified = time();
               $DB->insert_record('monorail_social_settings',$record);
           }
       }
       
    } 
$text = '';
if(!$spic) {
$text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script>
 window.opener.socialAccountsUpdate();
 self.close();
</script>
<body>
</body>
</html>
HTML;
} else {
$picurl = "'".$picurl."'";
$text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
var userpicture = window.opener.jQuery("img#user-profile-image");
var src = userpicture.attr('src');
src = src.substring(0, src.indexOf('?v=') + 3);
userpicture.attr('src', {$picurl});
self.close();
</script>
<body>
</body>
</html>
HTML;
}
echo $text;
} 
?>
