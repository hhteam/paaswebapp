<?php

/*
 * Get linkedin code and call the normal login page
 * Needed to add the parameter authprovider in order to identify the authentication provider
 */
require('../../config.php');
$code = optional_param('code', '', PARAM_TEXT); //
$error = optional_param('error', '', PARAM_TEXT); //Linkedin can return an error

if(!empty($error)) {
     $errmsg = "LinkedIn".":".get_string("accessdenied","admin");
     $hostname = parse_url($CFG->wwwroot);
     $urltogo = $hostname['scheme']."://".$hostname['host']."/?".'&error='.rawurlencode($errmsg).'#login';
     redirect($urltogo);
}

if (empty($code)) {
    throw new moodle_exception('linkedin_failure');
}

$loginurl = '/login/index.php';
if (!empty($CFG->alternateloginurl)) {
    $loginurl = $CFG->alternateloginurl;
}
$url = new moodle_url($loginurl, array('code' => $code, 'authprovider' => 'linkedin'));
redirect($url);
?>
