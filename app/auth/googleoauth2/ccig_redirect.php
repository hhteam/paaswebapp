<?php

/*
 * Get facebook code and call the normal login page
 * Needed to add the parameter authprovider in order to identify the authentication provider
 */
require('../../config.php');
$data = $_REQUEST['data'];
if (!isset($_REQUEST['data']) && !isset($data['access_token']) && !isset($data['key'])) {
     $errmsg = "CCIG".":".get_string("accessdenied","admin");
     $hostname = parse_url($CFG->wwwroot);
     $urltogo = $hostname['scheme']."://".$hostname['host']."/login-ccig?".'&error='.rawurlencode($errmsg).'#login';
     //redirect($urltogo);
     die();
}

$loginurl = '/login/index.php';
if (!empty($CFG->alternateloginurl)) {
    $loginurl = $CFG->alternateloginurl;
}
$url = new moodle_url($loginurl, array('code' => $data['access_token'].'##'.$data['key'], 'authprovider' => 'ccig'));
redirect($url);

