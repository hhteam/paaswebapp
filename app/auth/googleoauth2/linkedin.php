<?php

/*
 * Get linkedin code and call the normal login page
 * Needed to add the parameter authprovider in order to identify the authentication provider
 */
require('../../config.php');
require_once($CFG->libdir . '/filelib.php');

global $USER, $DB, $SESSION, $CFG, $PAGE;

try {
    require_login(null, false, null, false, true);
} catch (Exception $ex) {
    // not logged in, just die
    die();
}

$code = optional_param('code', '', PARAM_TEXT); 
$error = optional_param('error', '', PARAM_TEXT); //FB can return an error
$state = optional_param('state', '', PARAM_TEXT); 
if(!empty($error)) {
        $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script>
 self.close();
</script>
<body>
</body>
</html>
HTML;
echo $text;
die();
}

$curl = new curl();
$params = array();
$picurl = '';
$spic = false;
if (!empty($code)) {
    $params['client_id'] = get_config('auth/googleoauth2', 'linkedinclientid');
    $params['client_secret'] = get_config('auth/googleoauth2', 'linkedinclientsecret');
    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/linkedin.php';
    $params['code'] = $code;    
    $postreturnvalues = $curl->get('https://www.linkedin.com/uas/oauth2/accessToken?client_id=' . urlencode($params['client_id']) . '&redirect_uri=' . urlencode($params['redirect_uri'] ). '&client_secret=' . urlencode($params['client_secret']) . '&code=' .urlencode( $params['code']) . '&grant_type=authorization_code'); 
    $returnvalues = json_decode($postreturnvalues); 
    $accesstoken = $returnvalues->access_token;
    $expires = $returnvalues->expires_in;
    
   if (!empty($accesstoken)) {
       if($DB->record_exists('monorail_social_keys', array('userid'=>$USER->id, 'social' => 'linkedin'))){
          $vrec = $DB->get_record('monorail_social_keys', array('userid'=>$USER->id, 'social' => 'linkedin'), '*', MUST_EXIST);
          $vrec->accesstoken  = $accesstoken;
          $vrec->expires  = $expires;
          $vrec->timemodified  = time();
          $DB->update_record('monorail_social_keys', $vrec);
       } else {
           $vfdata = new stdClass();
           $vfdata->social  = 'linkedin';
           $vfdata->userid  = $USER->id;
           $vfdata->accesstoken  = $accesstoken;
           $vfdata->expires  = $expires;
           $vfdata->timemodified  = time();
           $DB->insert_record('monorail_social_keys', $vfdata);
       }

       $params = array();
       $params['oauth2_access_token'] = $accesstoken;
       $params['format'] = 'json';
       $postreturnvalues = json_decode($curl->get('https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,site-standard-profile-request,main-address,email-address,public-profile-url,picture-url)', $params));
       if(!empty($state)) {
         if(strcmp(substr($state, 0, 2),'p_') == 0) {
           $spic = true;
           if(!empty($postreturnvalues->pictureUrl)) {
             $picurl = $postreturnvalues->pictureUrl;
             if (!file_exists($CFG->dataroot.'/temp/files/'.$USER->id.'/')) {
               mkdir($CFG->dataroot.'/temp/files/'.$USER->id.'/');
             }
             // save picture to temp files
             $result = copy($picurl, $CFG->dataroot.'/temp/files/'.$USER->id. '/' . $USER->id.'socialprofilepicture');
           }
         }
       }
       if (!empty($postreturnvalues->emailAddress)) {
         $vrec = $DB->get_record('monorail_social_settings', array('userid'=>$USER->id, 'social' => 'linkedin', 'datakey' => 'account'));
         if(!$vrec) {
           $vrec = new stdClass();
           $vrec->social  = 'linkedin';
           $vrec->userid  = $USER->id;
           $vrec->timemodified  = time();
           $vrec->datakey = 'account';
           $vrec->value = $postreturnvalues->emailAddress;
           $DB->insert_record('monorail_social_settings',$vrec);
         }
       } 
       if (!empty($postreturnvalues->publicProfileUrl)) {
         $vrec = $DB->get_record('monorail_social_settings', array('userid'=>$USER->id, 'social' => 'linkedin', 'datakey' => 'accounturl'));
         if(!$vrec) {
           $vrec = new stdClass();
           $vrec->social  = 'linkedin';
           $vrec->userid  = $USER->id;
           $vrec->timemodified  = time();
           $vrec->datakey = 'accounturl';
           $vrec->value = $postreturnvalues->publicProfileUrl;
           $DB->insert_record('monorail_social_settings',$vrec);
         }
       } 
       if (!empty($postreturnvalues->emailAddress)) {
         $vrec = $DB->get_record('user', array('id'=>$USER->id));
         if($vrec && (strcmp($postreturnvalues->emailAddress, $vrec->username) == 0) && (strcmp($vrec->auth, 'googleoauth2') != 0)) {
           $vrec->auth = 'googleoauth2';
           $DB->update_record('user',$vrec);
         }
       }

       $settings = array('share_enroll','share_complete','share_certificate','share_review');
       foreach($settings as $setting) {
           $record = $DB->get_record('monorail_social_settings', array('userid' => $USER->id, 'social' => 'linkedin', 'datakey' => $setting));
            if(!$record) {
               $record = new stdClass();
               $record->value = 1;
               $record->datakey = $setting;
               $record->userid = $USER->id;
               $record->social = 'linkedin';
               $record->timemodified = time();
               $DB->insert_record('monorail_social_settings',$record);
           }
       }
    } 

if(!$spic) {
    $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script>
 window.opener.socialAccountsUpdate();
 self.close();
</script>
<body>
</body>
</html>
HTML;
} else {
$picurl = "'".$picurl."'";
  $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
var userpicture = window.opener.jQuery("img#user-profile-image");
var src = userpicture.attr('src');
src = src.substring(0, src.indexOf('?v=') + 3);
userpicture.attr('src', {$picurl});
self.close();
</script>
<body>
</body>
</html>
HTML;
}
echo $text;
} 
?>
