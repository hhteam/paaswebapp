<?php

/*
 */
require('../../config.php');
global $USER, $DB, $SESSION, $CFG;

require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot.'/theme/monorail/twitter/twitteroauth.php');

try {
    require_login(null, false, null, false, true);
} catch (Exception $ex) {
    // not logged in, just die
    die();
}

if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
  unset($_SESSION['oauth_token']);
  unset($_SESSION['oauth_token']);
  $text = <<<HTML
  <html>
    <head>
      <title></title>
      <meta http-equiv="Content-Type" content="text/html;
      charset=iso-8859-1">
    </head>
    <script>
      self.close();
    </script>
    <body>
    </body>
  </html>
HTML;
  echo $text;
  die();
}

$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
$accesstoken = $connection->getAccessToken($_REQUEST['oauth_verifier']);
$picurl = '';
unset($_SESSION['oauth_token']);
unset($_SESSION['oauth_token_secret']);
$spic = false;
if (!empty($accesstoken)) {
       $token = $accesstoken['oauth_token']; 
       $secret = $accesstoken['oauth_token_secret']; 
       if(isset($_SESSION['sgetpic'])) {
         unset($_SESSION['sgetpic']);
         $account = $connection->get('account/verify_credentials');
         $spic = true;
         if($account) {
           $picurl = str_replace("_normal", "", $account->profile_image_url_https);
           if (!file_exists($CFG->dataroot.'/temp/files/'.$USER->id.'/')) {
               mkdir($CFG->dataroot.'/temp/files/'.$USER->id.'/');
           }
           // save picture to temp files
           $result = copy($picurl, $CFG->dataroot.'/temp/files/'.$USER->id. '/' . $USER->id.'socialprofilepicture');
         } 
       }
       if(!empty($token) && !empty($secret)) {
        if($DB->record_exists('monorail_social_keys', array('userid'=>$USER->id, 'social' => 'twitter'))){
          $vrec = $DB->get_record('monorail_social_keys', array('userid'=>$USER->id, 'social' => 'twitter'), '*', MUST_EXIST);
          $vrec->accesstoken  = $token.':TWITTER:'.$secret;
          $vrec->expires  = 0;
          $vrec->timemodified  = time();
          $DB->update_record('monorail_social_keys', $vrec);
        } else {
           $vfdata = new stdClass();
           $vfdata->social  = 'twitter';
           $vfdata->userid  = $USER->id;
           $vfdata->accesstoken  = $token.':TWITTER:'.$secret;
           $vfdata->expires  = 0;
           $vfdata->timemodified  = time();
           $DB->insert_record('monorail_social_keys', $vfdata);
        }
        if(!empty($accesstoken['screen_name'])) {
         $vrec = $DB->get_record('monorail_social_settings', array('userid'=>$USER->id, 'social' => 'twitter', 'datakey' => 'account'));
         if(!$vrec) {
           $vrec = new stdClass();
           $vrec->social  = 'twitter';
           $vrec->userid  = $USER->id;
           $vrec->timemodified  = time();
           $vrec->datakey = 'account';
           $vrec->value = $accesstoken['screen_name'];
           $DB->insert_record('monorail_social_settings',$vrec);
         }
         $vrec = $DB->get_record('monorail_social_settings', array('userid'=>$USER->id, 'social' => 'twitter', 'datakey' => 'accounturl'));
         if(!$vrec) {
           $vrec = new stdClass();
           $vrec->social  = 'twitter';
           $vrec->userid  = $USER->id;
           $vrec->timemodified  = time();
           $vrec->datakey = 'accounturl';
           $vrec->value = 'http://twitter.com/'.$accesstoken['screen_name'];
           $DB->insert_record('monorail_social_settings',$vrec);
         }

        }  
        $settings = array('share_enroll','share_complete','share_certificate','share_review');
          foreach($settings as $setting) {
           $record = $DB->get_record('monorail_social_settings', array('userid' => $USER->id, 'social' => 'twitter', 'datakey' => $setting));
            if(!$record) {
               $record = new stdClass();
               $record->value = 1;
               $record->datakey = $setting;
               $record->userid = $USER->id;
               $record->social = 'twitter';
               $record->timemodified = time();
               $DB->insert_record('monorail_social_settings',$record);
           }
        }
       }
}
if(empty($picurl)) {
 $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script>
 window.opener.socialAccountsUpdate();
 self.close();
</script>
<body>
</body>
</html>
HTML;
} else {
 $picurl = "'".$picurl."'";
  $text = <<<HTML
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html;
charset=iso-8859-1">
</head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
var userpicture = window.opener.jQuery("img#user-profile-image");
var src = userpicture.attr('src');
src = src.substring(0, src.indexOf('?v=') + 3);
userpicture.attr('src', {$picurl});
self.close();
</script>
<body>
</body>
</html>
HTML;
}
echo $text;
?>
