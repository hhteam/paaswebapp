<?php

/*
 * Get google code and call the normal login page
 * Needed to add the parameter authprovider in order to identify the authentication provider
 */
require('../../config.php');
$code = optional_param('code', '', PARAM_TEXT); //Google can return an error
$state = optional_param('state', '', PARAM_TEXT); //Invite code
$error = optional_param('error', '', PARAM_TEXT); //Google can return an error

if(!empty($error)) {
     $errmsg = "Google".":".get_string("accessdenied","admin");
     $hostname = parse_url($CFG->wwwroot);
     $urltogo = $hostname['scheme']."://".$hostname['host']."/?".'&error='.rawurlencode($errmsg).'#login';
     redirect($urltogo);
}

if (empty($code)) {
    throw new moodle_exception('google_failure');
}

if (!empty($state)) {
    foreach (explode('&', $state) as $params) {
    $param = explode("=", $params);
    if ($param[0] == 'invcode') {
        global $SESSION;
        $SESSION->wantsurl = $CFG->magic_ui_root.'/invitation/'.$param[1];
      }
    }
}

$loginurl = '/login/index.php';
if (!empty($CFG->alternateloginurl)) {
    $loginurl = $CFG->alternateloginurl;
}
$url = new moodle_url($loginurl, array('code' => $code, 'authprovider' => 'googleapps'));
redirect($url);
?>
