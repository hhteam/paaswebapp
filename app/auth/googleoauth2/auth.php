<?php

/**
 * @author Jerome Mouneyrac
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package moodle multiauth
 *
 * Authentication Plugin: Google/Facebook/Messenger Authentication
 * If the email doesn't exist, then the auth plugin creates the user.
 * If the email exist (and the user has for auth plugin this current one),
 * then the plugin login the user related to this email.
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->libdir.'/moodlelib.php');

/**
 * Google/Facebook/Messenger Oauth2 authentication plugin.
 */
class auth_plugin_googleoauth2 extends auth_plugin_base {

    /**
     * Constructor.
     */
    function auth_plugin_googleoauth2() {
        $this->authtype = 'googleoauth2';
        $this->roleauth = 'auth_googleoauth2';
        $this->errorlogtag = '[AUTH GOOGLEOAUTH2] ';
        $this->config = get_config('auth/googleoauth2');
    }

    /**
     * Prevent authenticate_user_login() to update the password in the DB
     * @return boolean
     */
    function prevent_local_passwords() {
        //Allow to set a default password else auth logic breaks if username known !
        return false;
    }


    /**
     * Authenticates user against the selected authentication provide (Google, Facebook...)
     *
     * @param string $username The username (with system magic quotes)
     * @param string $password The password (with system magic quotes)
     * @return bool Authentication success or failure.
     */
    function user_login($username, $password) {
        global $DB, $CFG, $SESSION;

        //retrieve the user matching username
        $user = $DB->get_record('user', array('username' => $username,
            'mnethostid' => $CFG->mnet_localhost_id));
    
        //username must exist and have the right authentication method
        if (!empty($user) && ($user->auth == 'googleoauth2') && !empty($password)) {
           //check if auth request coming from this module oauth verified call, in that case
           //password verification does not really make any sense
           if (isset($SESSION->goauthv) && ($user->password == $password)) { 
               return true;
           }
           //If password request is coming from outside the social auth plugin callback
           //say with user supplied username and password go via normalassword mechanism
           return validate_internal_user_password($user, $password);
        }

        return false;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return false;
    }

   // Extending the plugin functionality to allow user to login with username and password
   //====================================================================================
   /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object  (with system magic quotes)
     * @param  string  $newpassword Plaintext password (with system magic quotes)
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        $user = get_complete_user_data('id', $user->id);
        return update_internal_user_password($user, $newpassword);
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return true;
    }

     /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return true;
    }

     /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null; // use default internal method
    }

//Copy of function from moodlelig authenticate_user_login only change being that it does not  
//update_internal_user_password . Reason is that to extend the plugin functionality to also
//allow user to login with username/password and also by social login and we do not want to reset
//user password if user is logging in via social auth. 
/**
 * Authenticates a user against the chosen authentication mechanism
 *
 * Given a username and password, this function looks them
 * up using the currently selected authentication mechanism,
 * and if the authentication is successful, it returns a
 * valid $user object from the 'user' table.
 *
 * Uses auth_ functions from the currently active auth module
 *
 * After authenticate_user_login() returns success, you will need to
 * log that the user has logged in, and call complete_user_login() to set
 * the session up.
 *
 * Note: this function works only with non-mnet accounts!
 *
 * @param string $username  User's username
 * @param string $password  User's password
 * @return user|flase A {@link $USER} object or false if error
 */
private function goauth_authenticate_user_login($username, $password) {
    global $CFG, $DB;

    $authsenabled = get_enabled_auth_plugins();

    if ($user = get_complete_user_data('username', $username, $CFG->mnet_localhost_id)) {
        $auth = empty($user->auth) ? 'manual' : $user->auth;  // use manual if auth not set
        if (!empty($user->suspended)) {
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Suspended Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }
        if ($auth=='nologin' or !is_enabled_auth($auth)) {
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Disabled Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }
        $auths = array($auth);

    } else {
        // check if there's a deleted record (cheaply)
        if ($DB->get_field('user', 'id', array('username'=>$username, 'deleted'=>1))) {
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Deleted Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }

        // User does not exist
        $auths = $authsenabled;
        $user = new stdClass();
        $user->id = 0;
    }

    foreach ($auths as $auth) {
        $authplugin = get_auth_plugin($auth);

        // on auth fail fall through to the next plugin
        if (!$authplugin->user_login($username, $password)) {
            continue;
        }

        // successful authentication
        if ($user->id) {                          // User already exists in database
            if (empty($user->auth)) {             // For some reason auth isn't set yet
                $DB->set_field('user', 'auth', $auth, array('username'=>$username));
                $user->auth = $auth;
            }

            //update_internal_user_password($user, $password); // just in case salt or encoding were changed (magic quotes too one day)

            if ($authplugin->is_synchronised_with_external()) { // update user record from external DB
                $user = update_user_record($username);
            }
        } else {
            add_to_log(SITEID, 'auth_googleoauth2', '', '', 'Not creating account after authentication, user not exist in DB' . $username); 
            continue;
            /* 
            // if user not found and user creation is not disabled, create it
            if (empty($CFG->authpreventaccountcreation)) {
                $user = create_user_record($username, $password, $auth);
            } else {
                continue;
            }
            */
        }

        $authplugin->sync_roles($user);

        foreach ($authsenabled as $hau) {
            $hauth = get_auth_plugin($hau);
            $hauth->user_authenticated_hook($user, $username, $password);
        }

        if (empty($user->id)) {
            return false;
        }

        if (!empty($user->suspended)) {
            // just in case some auth plugin suspended account
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Suspended Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }

        return $user;
    }

    // failed if all the plugins have failed
    add_to_log(SITEID, 'login', 'error', 'index.php', $username);
    if (debugging('', DEBUG_ALL)) {
        error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Failed Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
    }
    return false;
}

    /**
     *  sends a post request with  $param array
     *     returns the response
     */
    function http_post_url_encoded_data($url, $params) {
        foreach ($params as $key => $value) {
            $data .= $key . '=' . $value . '&';
        }
        rtrim($data, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

//==============================Customization end======================================================


    /**
     * Authentication hook - is called every time user hit the login page
     * The code is run only if the param code is mentionned.
     */
    function loginpage_hook() {
        global $USER, $SESSION, $CFG, $DB;

        //check the Google authorization code
        $authorizationcode = optional_param('code', '', PARAM_TEXT);
        if (!empty($authorizationcode)) {

            $authprovider = required_param('authprovider', PARAM_ALPHANUMEXT);

            //set the params specific to the authentication provider
            $params = array();

            switch ($authprovider) {
                case 'google':
                    $params['client_id'] = get_config('auth/googleoauth2', 'googleclientid');
                    $params['client_secret'] = get_config('auth/googleoauth2', 'googleclientsecret');
                    $requestaccesstokenurl = 'https://accounts.google.com/o/oauth2/token';
                    $params['grant_type'] = 'authorization_code';
                    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/google_redirect.php';
                    $params['code'] = $authorizationcode;
                    break;
                case 'googleapps':
                    $params['client_id'] = get_config('auth/googleapps', 'googleclientid');
                    $params['client_secret'] = get_config('auth/googleapps', 'googleclientsecret');
                    $requestaccesstokenurl = 'https://accounts.google.com/o/oauth2/token';
                    $params['grant_type'] = 'authorization_code';
                    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/googleapps_redirect.php';
                    $params['code'] = $authorizationcode;
                    break;
                case 'facebook':
                    $params['client_id'] = get_config('auth/googleoauth2', 'facebookclientid');
                    $params['client_secret'] = get_config('auth/googleoauth2', 'facebookclientsecret');
                    $requestaccesstokenurl = 'https://graph.facebook.com/oauth/access_token';
                    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/facebook_redirect.php';
                    $params['code'] = $authorizationcode;
                    break;
                case 'messenger':
                    $params['client_id'] = get_config('auth/googleoauth2', 'messengerclientid');
                    $params['client_secret'] = get_config('auth/googleoauth2', 'messengerclientsecret');
                    $requestaccesstokenurl = 'https://oauth.live.com/token';
                    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/messenger_redirect.php';
                    $params['code'] = $authorizationcode;
                    $params['grant_type'] = 'authorization_code';
                    break;
                case 'mslive':
                    $params['client_id'] = get_config('auth/googleoauth2', 'msliveclientid');
                    $params['client_secret'] = get_config('auth/googleoauth2', 'msliveclientsecret');
                    $requestaccesstokenurl = 'https://login.live.com/oauth20_token.srf';
                    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/live_redirect.php';
                    $params['code'] = $authorizationcode;
                    $params['grant_type'] = 'authorization_code';
                    break;
                case 'linkedin':
                    $requestaccesstokenurl = 'https://www.linkedin.com/uas/oauth2/accessToken?';
                    $params['client_id'] = get_config('auth/googleoauth2', 'linkedinclientid');
                    $params['client_secret'] = get_config('auth/googleoauth2', 'linkedinclientsecret');
                    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleoauth2/linkedin_redirect.php';
                    $params['code'] = $authorizationcode;
                    $params['grant_type'] = 'authorizationcode';
                    break;
                case 'ccig':
                    $params['code'] = $authorizationcode;
                    break;
                default:
                    throw new moodle_exception('unknown_oauth2_provider');
                    break;
            }

            //request by curl an access token and refresh token
            require_once($CFG->libdir . '/filelib.php');
            if ($authprovider == 'messenger') { //Windows Live returns an "Object moved" error with curl->post() encoding
                $curl = new curl();
                $postreturnvalues = $curl->get('https://oauth.live.com/token?client_id=' . urlencode($params['client_id']) . '&redirect_uri=' . urlencode($params['redirect_uri'] ). '&client_secret=' . urlencode($params['client_secret']) . '&code=' .urlencode( $params['code']) . '&grant_type=authorization_code');
           } else if ($authprovider == 'linkedin') {
                $curl = new curl();
                $postreturnvalues = $curl->get('https://www.linkedin.com/uas/oauth2/accessToken?client_id=' . urlencode($params['client_id']) . '&redirect_uri=' . urlencode($params['redirect_uri'] ). '&client_secret=' . urlencode($params['client_secret']) . '&code=' .urlencode( $params['code']) . '&grant_type=authorization_code');
           } else if ($authprovider == 'mslive') {
                $postreturnvalues = $this->http_post_url_encoded_data($requestaccesstokenurl, $params);
           } else if ($authprovider == 'ccig'){
                $curl = new curl();
                //do nothing
           } else {
                $curl = new curl();
                $postreturnvalues = $curl->post($requestaccesstokenurl, $params);
            }


            switch ($authprovider) {
                case 'google':
                case 'googleapps':
                    $postreturnvalues = json_decode($postreturnvalues);
                    $accesstoken = $postreturnvalues->access_token;
                    //$refreshtoken = $postreturnvalues->refresh_token;
                    //$expiresin = $postreturnvalues->expires_in;
                    //$tokentype = $postreturnvalues->token_type;
                    break;
                case 'facebook':
                    $postreturnvalues = json_decode($postreturnvalues);
                    $accesstoken = $postreturnvalues->access_token;
                    break;
                case 'messenger':
                    $accesstoken = json_decode($postreturnvalues)->access_token;
                    break;
                case 'mslive':
                    $accesstoken = json_decode($postreturnvalues)->access_token;
                    break;
                case 'linkedin':
                    $accesstoken = json_decode($postreturnvalues)->access_token;
                    break;
                case 'ccig':
                    $accesstoken = $params['code']; 
                    break;


                default:
                    break;
            }

            //with access token request by curl the email address
            if (!empty($accesstoken)) {
                //get the username matching the email
                switch ($authprovider) {
                    case 'google':
                    case 'googleapps':
                        $params = array();
                        $params['access_token'] = $accesstoken;
                        $params['alt'] = 'json';
                        $postreturnvalues = $curl->get('https://www.googleapis.com/oauth2/v1/userinfo', $params);
                        $postreturnvalues = json_decode($postreturnvalues);//email, id, name, verified_email, given_name, family_name, link, gender, locale;
                        $useremail = $postreturnvalues->email;
                        $verified = $postreturnvalues->verified_email;
                        break;

                    case 'facebook':
                        $params = array();
                        $params['access_token'] = $accesstoken;
                        $params['fields'] = 'email,first_name,last_name';
                        $postreturnvalues = $curl->get('https://graph.facebook.com/me', $params);
                        $postreturnvalues = json_decode($postreturnvalues);
                        $useremail = $postreturnvalues->email;
                        //~ $verified = $postreturnvalues->verified;
                        $verified = 1; //see bug https://bugs.cloudberrytec.com/show_bug.cgi?id=195
                        break;

                    case 'linkedin':
                        $params = array();
                        $params['oauth2_access_token'] = $accesstoken;
                        $params['format'] = 'json';
                        $postreturnvalues = $curl->get('https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,site-standard-profile-request,main-address,email-address,public-profile-url,picture-url)', $params);
                        $postreturnvalues = json_decode($postreturnvalues);
                        $useremail = $postreturnvalues->emailAddress;
                        //~ $verified = $postreturnvalues->verified;
                        $verified = 1; //see bug https://bugs.cloudberrytec.com/show_bug.cgi?id=195
                        break;

                    case 'mslive':
                        $params = array();
                        $params['access_token'] = $accesstoken;
                        $curl = new curl();
                        $postreturnvalues = $curl->get('https://apis.live.net/v5.0/me', $params);
                        $postreturnvalues = json_decode($postreturnvalues);
                        $useremail = $postreturnvalues->emails->preferred;
                        $verified = 1;
                        break;

                    case 'messenger':
                        $params = array();
                        $params['access_token'] = $accesstoken;
                        $postreturnvalues = $curl->get('https://apis.live.net/v5.0/me', $params);
                        $postreturnvalues = json_decode($postreturnvalues);
                        $useremail = $postreturnvalues->emails->preferred;
                        $verified = 1; //not super good but there are no way to check it yet:
                                       //http://social.msdn.microsoft.com/Forums/en-US/messengerconnect/thread/515d546d-1155-4775-95d8-89dadc5ee929
                        break;
                    case 'ccig':
                        $tokkey = $accesstoken;
                        $parts = explode("##", $tokkey);
                        if (strpos($CFG->landing_page, 'eliademy') === FALSE) { //only for non production
                          $curl->setopt( array(
                           'CURLOPT_SSL_VERIFYHOST' => 0,
                           'CURLOPT_SSL_VERIFYPEER,' => 0));
                        }
                        $curl->setHeader('Authorization: Bearer '.$parts[0]);
                        $postreturnvalues = $curl->get('https://rest.ccig.pl/employees/'.$parts[1].'/');
                        $postreturnvalues = json_decode($postreturnvalues)[0];

                        $useremail = iconv('utf-8', 'us-ascii//TRANSLIT', $postreturnvalues->name).'.'.iconv('utf-8', 'us-ascii//TRANSLIT',$postreturnvalues->surname).'-'.$postreturnvalues->id.'@eliademy.com';

                        // ccig users may change their emails, but id should
                        // stay the same...

                        $ccigusers = $DB->get_records_sql("SELECT id, email FROM {user} WHERE email LIKE '%-" . $postreturnvalues->id . "@eliademy.com'");

                        if (!empty($ccigusers)) {
                            $cciguserexists = false;

                            foreach ($ccigusers as $cciguser) {
                                if ($cciguser->email == $useremail) {
                                    $cciguserexists = true;
                                    break;
                                }
                            }

                            if (!$cciguserexists) {
                                // ccig user has a new email, but id is the same
                                $cciguser = reset($ccigusers);
                                $useremail = $cciguser->email;

                            } // else if count $ccigusers > 1 - we have a problem (more than one account for the same user...)
                        }

                        $ccigdomains = array('eliademy.com');
                        $userdomain = array_pop(explode('@', $useremail));
                        if(in_array($userdomain, $ccigdomains)) {
                            $verified = 1; //not super good but there are no way to check it yet:
                        }
                        break;

                    default:
                        break;
                }

                
                //throw an error if the email address is not verified
                if (!$verified) {
                    throw new moodle_exception('emailaddressmustbeverified', 'auth_googleoauth2');
                }

                // Prohibit login if email belongs to the prohibited domain
                if ($err = email_is_not_allowed($useremail)) {
                   throw new moodle_exception($err, 'auth_googleoauth2');
                }

                //if email not existing in user database then create a new username (userX).
                if (empty($useremail) or $useremail != clean_param($useremail, PARAM_EMAIL)) {
                    throw new moodle_exception('couldnotgetuseremail');
                    //TODO: display a link for people to retry
                }
                //get the user - don't bother with auth = googleoauth2 because
                //authenticate_user_login() will fail it if it's not 'googleoauth2'
                $user = $DB->get_record('user', array('email' => $useremail, 'deleted' => 0, 'mnethostid' => $CFG->mnet_localhost_id));
                //create the user if it doesn't exist
                if (empty($user)) {
                    if(get_config('auth/googleoauth2', 'googleuserprefix') == "@email"){
                        //If @email is set use email as username
                        $username = $useremail;
                    } else {
                        //get following incremented username
                        $lastusernumber = get_config('auth/googleoauth2', 'lastusernumber');
                        $lastusernumber = empty($lastusernumber)?1:$lastusernumber++;
                        //check the user doesn't exist
                        $nextuser = $DB->get_record('user',
                                array('username' => get_config('auth/googleoauth2', 'googleuserprefix').$lastusernumber));
                        while (!empty($nextuser)) {
                            $lastusernumber = $lastusernumber +1;
                            $nextuser = $DB->get_record('user',
                                array('username' => get_config('auth/googleoauth2', 'googleuserprefix').$lastusernumber));
                        }
                        set_config('lastusernumber', $lastusernumber, 'auth/googleoauth2');
                        $username = get_config('auth/googleoauth2', 'googleuserprefix') . $lastusernumber;
                    } 
                    //retrieve more information from the provider
                    $picurl = ''; 
                    $newuser = new stdClass();
                    $newuser->email = $useremail;
                    switch ($authprovider) {
                        case 'google':
                        case 'googleapps':
                            $newuser->auth = 'googleoauth2';
                            if (array_key_exists ('given_name', $postreturnvalues)) {
                                $newuser->firstname = $postreturnvalues->given_name;
                            }
                            if (array_key_exists ('family_name', $postreturnvalues)) {
                                $newuser->lastname = $postreturnvalues->family_name;
                            }
                            if (array_key_exists ('locale', $postreturnvalues)) {
                                //$newuser->lang = $postreturnvalues->locale;
                                //TODO: convert the locale into correct Moodle language code
                            }
                            if (array_key_exists ('picture', $postreturnvalues)) {
                                $picurl = $postreturnvalues->picture;
                            }
                            break;

                        case 'facebook':
                            $newuser->firstname =  $postreturnvalues->first_name;
                            $newuser->lastname =  $postreturnvalues->last_name;

                            if(array_key_exists ('location', $postreturnvalues)) {
                                $newuser->location = $postreturnvalues->location->name;
                            }
                            if(array_key_exists ('education', $postreturnvalues)) {
                                foreach($postreturnvalues->education as $edu) 
                                    $newuser->institution = $edu->school->name;
                                if(!empty($newuser->institution) && (strlen($newuser->institution) > 40)){
                                    $newuser->institution = '';//DB schema size restriction to 40;
                                }  
                            }
                            $picurl = "https://graph.facebook.com/". $postreturnvalues->id . "/picture?type=large";
                            break;

                        case 'messenger':
                            $newuser->firstname =  $postreturnvalues->first_name;
                            $newuser->lastname =  $postreturnvalues->last_name;
                            break;

                        case 'linkedin':
                            $newuser->firstname =  $postreturnvalues->firstName;
                            $newuser->lastname =  $postreturnvalues->lastName;
                            $picurl = $postreturnvalues->pictureUrl;
                            break;

                        case 'mslive':
                            $newuser->firstname =  $postreturnvalues->first_name;
                            $newuser->lastname =  $postreturnvalues->last_name;
                            break;

                        case 'ccig':
                            $newuser->firstname =  $postreturnvalues->name;
                            $newuser->lastname =  $postreturnvalues->surname;
                            break;

                        default:
                            break;
                    }

                    //retrieve country and city if the provider failed to give it
                    if (!isset($newuser->country) or !isset($newuser->city)) {
                        $googleipinfodbkey = get_config('auth/googleoauth2', 'googleipinfodbkey');
                        if (!empty($googleipinfodbkey)) {
                            $locationdata = $curl->get('http://api.ipinfodb.com/v3/ip-city/?key=' .
                                $googleipinfodbkey . '&ip='. getremoteaddr() . '&format=json' );
                            $locationdata = json_decode($locationdata);
                        }
                        if (!empty($locationdata)) {
                            //TODO: check that countryCode does match the Moodle country code
                            $newuser->country = isset($newuser->country)?isset($newuser->country):$locationdata->countryCode;
                            $newuser->city = isset($newuser->city)?isset($newuser->city):$locationdata->cityName;
                        }
                    }
                    if (!empty($newuser->email) && !empty($newuser->firstname) && !empty($newuser->lastname)) {
                        $userpassword = hash_hmac("md5", substr(sha1(rand()), 0, 30), get_config('auth/googleoauth2', 'googleauthsecret')); 
                        $cuser = create_user_record($username, $userpassword, 'googleoauth2');
                        add_to_log(SITEID, 'auth_googleoauth2', '', '', $newuser->firstname . '/' . $newuser->lastname . '/' . $cuser->id . '/' . $authprovider);
                        add_to_log(SITEID, 'auth_googleoauth2', '', '', $newuser->location . '/' . $newuser->institution . '/' . $newuser->country . '/' . $newuser->city);
                        //Updated basic information email/firstname/lastname
                        $newuser->id = $cuser->id;
                        $newuser->maildisplay = 0;
                        $newuser->password = $userpassword;
                        try {
			    $DB->update_record('user', $newuser);	
                            //Add member to cohort if request  from CCIG
                            if(strcmp($authprovider,'ccig') == 0) {
                              require_once($CFG->dirroot . '/cohort/lib.php');
                              $cohortid = 0;
                              if($postreturnvalues->company == 1) { //CCIG
                                $cohortid = 208; //Production value;
                              } else if ($postreturnvalues->company == 2) {
                                $cohortid = 443; // Production value;
                              }
                              if($cohortid > 0) {
                                // Cohort members are cohort teachers now:
                                //cohort_add_member($cohortid, $newuser->id);
                                // This is for non-teacher members:
                                try {
                                    $DB->execute("INSERT INTO {monorail_cohort_user_link} (cohortid, userid) VALUES (?, ?)",
                                        array($cohortid, $newuser->id));
                                } catch (Exception $ex) {}
                              }
                            }
			} catch (Exception $ex) {
                            add_to_log(SITEID, 'auth_googleoauth2', '', '', "Exception : ".get_class($ex).': '.$ex->getMessage());
                            $errmsg = get_string("servererror");
                            $hostname = parse_url($CFG->wwwroot);
                            if(strcmp($authprovider,'ccig') == 0) {
                              $urltogo = $hostname['scheme']."://".$hostname['host'].'/login-ccig?error='.rawurlencode($errmsg);
                            } else {
                              $urltogo = $hostname['scheme']."://".$hostname['host'].'/?error='.rawurlencode($errmsg).'#login';
                            }
                            redirect($urltogo);
                            exit;
                        }
                    } else {
                        add_to_log(SITEID, 'auth_googleoauth2', '', '', 'Not creating account and exiting, no fullname ' . $authprovider);
                        $errmsg = ucfirst($authprovider).":".get_string("servererror");
                        $hostname = parse_url($CFG->wwwroot);
                        if(strcmp($authprovider,'ccig') == 0) {
                          $urltogo = $hostname['scheme']."://".$hostname['host'].'/login-ccig?error='.rawurlencode($errmsg);
                        } else {
                          $urltogo = $hostname['scheme']."://".$hostname['host'].'/?error='.rawurlencode($errmsg).'#login';
                        }
                        redirect($urltogo);
                        exit;
                    }
                } else {
                    $username = $user->username;

                    if (strcmp($authprovider, 'ccig') == 0) {
                        /* Making sure that returning CCIG users don't get
                         * lost... */

                        require_once($CFG->dirroot . '/cohort/lib.php');
                        $cohortid = 0;

                        if ($postreturnvalues->company == 1) { //CCIG
                            $cohortid = 208; //Production value;
                        } else if ($postreturnvalues->company == 2) {
                            $cohortid = 443; // Production value;
                        }

                        if ($cohortid > 0) {
                            if (!$DB->get_field_sql("SELECT id FROM {monorail_cohort_user_link} WHERE cohortid=? AND userid=?", array($cohortid, $user->id))) {
                                try {
                                    $DB->execute("INSERT INTO {monorail_cohort_user_link} (cohortid, userid) VALUES (?, ?)",
                                        array($cohortid, $user->id));
                                } catch (Exception $ex) {}
                            }
                        }
                    }
                }

                //authenticate the user
                //TODO: delete this log later
                $userid = empty($user)?'new user':$user->id;
                $userpassword = empty($user)?$newuser->password:$user->password;
                add_to_log(SITEID, 'auth_googleoauth2', '', '', $username . '/' . $useremail . '/' . $userid);
                $SESSION->goauthv = true;//Request from current oauth verified session;
                $user = $this->goauth_authenticate_user_login($username, $userpassword);
                //$user = authenticate_user_login($username, $userpassword);
                unset($SESSION->goauthv);
                if ($user) {

                    //set a cookie to remember what auth provider was selected
                    setcookie('MOODLEGOOGLEOAUTH2_'.$CFG->sessioncookie, $authprovider,
                            time()+(DAYSECS*60), $CFG->sessioncookiepath,
                            $CFG->sessioncookiedomain, $CFG->cookiesecure,
                            $CFG->cookiehttponly);

                    $socialplugins = array("facebook", "linkedin", "google");
                    $socialprovider = $authprovider;
                    if(strcmp($socialprovider, 'google') == 0) {
                        $socialprovider = 'googleplus'; 
                    }
                    $sociallink = $DB->get_record('user_info_field', array('shortname'=>$socialprovider));
                    if($sociallink && in_array($authprovider, $socialplugins)) {
                        $updatesocial = false;
                        $uidata = $DB->get_record('user_info_data', array('userid' => $user->id, 'fieldid' => $sociallink->id));
                        if(!$uidata) {
                           $updatesocial = true;         
                        } else {
                           if(empty($datafield->data)) {
                               $updatesocial = true;         
                           }
                        }
                        if($updatesocial) {
                            $publicurl = '';
                            if(array_key_exists ('link', $postreturnvalues) && !empty($postreturnvalues->link)) {
                                $publicurl = $postreturnvalues->link;
                            } else if (array_key_exists ('publicProfileUrl', $postreturnvalues) && !empty($postreturnvalues->publicProfileUrl)) { 
                                $publicurl = $postreturnvalues->publicProfileUrl;
                            }
                            if(!empty($publicurl) && $uidata) {
                               $uidata->data = $publicurl;
                               $DB->update_record('user_info_data', $uidata);
                            } else if (!empty($publicurl) && !$uidata) {
                               $customdata = new  stdClass();
                               $customdata->userid = $user->id;
                               $customdata->fieldid = $sociallink->id;
                               $customdata->dataformat = 1;
                               $customdata->data = $publicurl;
                               $DB->insert_record('user_info_data', $customdata);
                            }
                        }
                    }
                    //prefill more user information if new user
                    if (!empty($newuser)) {
                        $newuser->id = $user->id;
                        //$DB->update_record('user', $newuser);
                        //If picurl is set get user profile image here
                        if(!empty($picurl)){
                            //We have the provider url , create and set the moodle icon url 
                            $context = get_context_instance(CONTEXT_USER, $newuser->id, MUST_EXIST);
                            $fs = get_file_storage();
                            $fileinfo = array(
                                'contextid' => $context->id,
                                'component' => 'user',
                                'filearea' => 'draft',
                                'itemid' => 0,
                                'filepath' => '/',
                                );
                            $file = $fs->create_file_from_url($fileinfo, $picurl);
                            $iconfile = $file->copy_content_to_temp();
                            require_once("$CFG->dirroot/theme/monorail/lib.php");
                            require_once($CFG->libdir . '/gdlib.php');
                            monorail_process_avatar($newuser->id, $iconfile);
                            $newpicture = (int)process_new_icon($context, 'user', 'icon', 0, $iconfile);
                            @unlink($iconfile);
                            $DB->set_field('user', 'picture', $newpicture, array('id' => $newuser->id));
                        }
                        //Use the updated record       
                        $user = $DB->get_record('user', array('email' => $useremail, 'deleted' => 0, 'mnethostid' => $CFG->mnet_localhost_id));
                    }

                    complete_user_login($user);
                    if (!empty($newuser)) {
                        set_user_preference("email_send_count", 10);
                        set_user_preference("email_bounce_count", 1); 
                        require_once("$CFG->dirroot/theme/monorail/engagements.php");
                        engagements_trigger('welcome-email', $user->id);
                    }
                    // Redirection
                    if (user_not_fully_set_up($USER)) {
                        $urltogo = $CFG->wwwroot.'/user/edit.php';
                        // We don't delete $SESSION->wantsurl yet, so we get there later
                    } else if (isset($_COOKIE["loginRedirect"]) && !empty($_COOKIE["loginRedirect"])) {
                        $urltogo = $_COOKIE["loginRedirect"];
                        setcookie("loginRedirect", "", time() - 3600, "/");
                    } else if (isset($SESSION->wantsurl) and (strpos($SESSION->wantsurl, $CFG->wwwroot) === 0)) {
                        $urltogo = $SESSION->wantsurl;    // Because it's an address in this site
                        unset($SESSION->wantsurl);
                    } else {
                        // No wantsurl stored or external - go to homepage
                        $urltogo = $CFG->wwwroot.'/';
                        unset($SESSION->wantsurl);
                    }
                    redirect($urltogo);
                } else {
                    $errmsg = get_string("emailexists");
                    if($userid != 'new user')
                        $errmsg = ucfirst($authprovider).":".get_string("authentication")." ".get_string("error");
                    $hostname = parse_url($CFG->wwwroot);
                    if(strcmp($authprovider,'ccig') == 0) {
                      $urltogo = $hostname['scheme']."://".$hostname['host']."/login-ccig?username=".urlencode($username).'&error='.rawurlencode($errmsg);
                    } else {
                      $urltogo = $hostname['scheme']."://".$hostname['host']."/?username=".urlencode($username).'&error='.rawurlencode($errmsg).'#login';
                    }
                    redirect($urltogo);
                }
            } else {
                throw new moodle_exception('couldnotgetgoogleaccesstoken', 'auth_googleoauth2');
            }
        }
    }


    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * TODO: as print_auth_lock_options() core function displays an old-fashion HTML table, I didn't bother writing
     * some proper Moodle code. This code is similar to other auth plugins (04/09/11)
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        global $OUTPUT;

        // set to defaults if undefined
        if (!isset($config->googleclientid)) {
            $config->googleclientid = '';
        }
        if (!isset($config->googleclientsecret)) {
            $config->googleclientsecret = '';
        }
        if (!isset ($config->facebookclientid)) {
            $config->facebookclientid = '';
        }
        if (!isset ($config->facebookclientsecret)) {
            $config->facebookclientsecret = '';
        }
        if (!isset ($config->messengerclientid)) {
            $config->messengerclientid = '';
        }
        if (!isset ($config->messengerclientsecret)) {
            $config->messengerclientsecret = '';
        }
        if (!isset ($config->msliveclientid)) {
            $config->msliveclientid = '';
        }
        if (!isset ($config->msliveclientsecret)) {
            $config->msliveclientsecret = '';
        }
        if (!isset($config->googleipinfodbkey)) {
            $config->googleipinfodbkey = '';
        }
        if (!isset($config->googleuserprefix)) {
            $config->googleuserprefix = 'social_user_';
        }
        if (!isset ($config->linkedinclientid)) {
            $config->linkedinclientid = '';
        }
        if (!isset ($config->linkedinclientsecret)) {
            $config->linkedinclientsecret = '';
        }
        if (!isset ($config->googleauthsecret)) {
            $config->googleauthsecret = substr(sha1(rand()), 0, 40);
        }

        echo '<table cellspacing="0" cellpadding="5" border="0">
            <tr>
               <td colspan="3">
                    <h2 class="main">';

        print_string('auth_googlesettings', 'auth_googleoauth2');
        // Google auth secret 

        echo '</h2>
               </td>
            </tr>
            <tr>
                <td align="right"><label for="googleauthsecret">';

        print_string('auth_googleauthsecret_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'googleauthsecret', 'name' => 'googleauthsecret',
                    'class' => 'googleauthsecret', 'value' => $config->googleauthsecret));

        if (isset($err["googleauthsecret"])) {
            echo $OUTPUT->error_text($err["googleauthsecret"]);
        }

        echo '</td><td>';

        print_string('auth_googleauthsecret', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Google client id

        echo '</h2>
               </td>
            </tr>
            <tr>
                <td align="right"><label for="googleclientid">';

        print_string('auth_googleclientid_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'googleclientid', 'name' => 'googleclientid',
                    'class' => 'googleclientid', 'value' => $config->googleclientid));

        if (isset($err["googleclientid"])) {
            echo $OUTPUT->error_text($err["googleclientid"]);
        }

        echo '</td><td>';

        print_string('auth_googleclientid', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Google client secret

        echo '<tr>
                <td align="right"><label for="googleclientsecret">';

        print_string('auth_googleclientsecret_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'googleclientsecret', 'name' => 'googleclientsecret',
                    'class' => 'googleclientsecret', 'value' => $config->googleclientsecret));

        if (isset($err["googleclientsecret"])) {
            echo $OUTPUT->error_text($err["googleclientsecret"]);
        }

        echo '</td><td>';

        print_string('auth_googleclientsecret', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Facebook client id

        echo '<tr>
                <td align="right"><label for="facebookclientid">';

        print_string('auth_facebookclientid_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'facebookclientid', 'name' => 'facebookclientid',
                    'class' => 'facebookclientid', 'value' => $config->facebookclientid));

        if (isset($err["facebookclientid"])) {
            echo $OUTPUT->error_text($err["facebookclientid"]);
        }

        echo '</td><td>';

        print_string('auth_facebookclientid', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Facebook client secret

        echo '<tr>
                <td align="right"><label for="facebookclientsecret">';

        print_string('auth_facebookclientsecret_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'facebookclientsecret', 'name' => 'facebookclientsecret',
                    'class' => 'facebookclientsecret', 'value' => $config->facebookclientsecret));

        if (isset($err["facebookclientsecret"])) {
            echo $OUTPUT->error_text($err["facebookclientsecret"]);
        }

        echo '</td><td>';

        print_string('auth_facebookclientsecret', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Messenger client id

        echo '<tr>
                <td align="right"><label for="messengerclientid">';
        print_string('auth_messengerclientid_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'messengerclientid', 'name' => 'messengerclientid',
                    'class' => 'messengerclientid', 'value' => $config->messengerclientid));

        if (isset($err["messengerclientid"])) {
            echo $OUTPUT->error_text($err["messengerclientid"]);
        }

        echo '</td><td>';

        print_string('auth_messengerclientid', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Messenger client secret

        echo '<tr>
                <td align="right"><label for="messengerclientsecret">';

        print_string('auth_messengerclientsecret_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'messengerclientsecret', 'name' => 'messengerclientsecret',
                    'class' => 'messengerclientsecret', 'value' => $config->messengerclientsecret));

        if (isset($err["messengerclientsecret"])) {
            echo $OUTPUT->error_text($err["messengerclientsecret"]);
        }

        echo '</td><td>';

        print_string('auth_messengerclientsecret', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Linkedin client id

        echo '<tr>
                <td align="right"><label for="linkedinclientid">';

        print_string('auth_linkedinclientid_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'linkedinclientid', 'name' => 'linkedinclientid',
                    'class' => 'linkedinclientid', 'value' => $config->linkedinclientid));

        if (isset($err["linkedinclientid"])) {
            echo $OUTPUT->error_text($err["linkedinclientid"]);
        }

        echo '</td><td>';

        print_string('auth_linkedinclientid', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // linkedin client secret

        echo '<tr>
                <td align="right"><label for="linkedinclientsecret">';

        print_string('auth_linkedinclientsecret_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'linkedinclientsecret', 'name' => 'linkedinclientsecret',
                    'class' => 'linkedinclientsecret', 'value' => $config->linkedinclientsecret));

        if (isset($err["linkedinclientsecret"])) {
            echo $OUTPUT->error_text($err["linkedinclientsecret"]);
        }

        echo '</td><td>';

        print_string('auth_linkedinclientsecret', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Live client id

        echo '<tr>
                <td align="right"><label for="msliveclientid">';
        print_string('auth_msliveclientid_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'msliveclientid', 'name' => 'msliveclientid',
                    'class' => 'msliveclientid', 'value' => $config->msliveclientid));

        if (isset($err["msliveclientid"])) {
            echo $OUTPUT->error_text($err["msliveclientid"]);
        }

        echo '</td><td>';

        print_string('auth_msliveclientid', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // Live client secret

        echo '<tr>
                <td align="right"><label for="msliveclientsecret">';

        print_string('auth_msliveclientsecret_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'msliveclientsecret', 'name' => 'msliveclientsecret',
                    'class' => 'msliveclientsecret', 'value' => $config->msliveclientsecret));

        if (isset($err["msliveclientsecret"])) {
            echo $OUTPUT->error_text($err["msliveclientsecret"]);
        }

        echo '</td><td>';
        print_string('auth_msliveclientsecret', 'auth_googleoauth2') ;
        echo '</td></tr>';

        // IPinfoDB

        echo '<tr>
                <td align="right"><label for="googleipinfodbkey">';

        print_string('auth_googleipinfodbkey_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'googleipinfodbkey', 'name' => 'googleipinfodbkey',
                    'class' => 'googleipinfodbkey', 'value' => $config->googleipinfodbkey));

        if (isset($err["googleipinfodbkey"])) {
            echo $OUTPUT->error_text($err["googleipinfodbkey"]);
        }

        echo '</td><td>';

        print_string('auth_googleipinfodbkey', 'auth_googleoauth2') ;

        echo '</td></tr>';

        // User prefix

        echo '<tr>
                <td align="right"><label for="googleuserprefix">';

        print_string('auth_googleuserprefix_key', 'auth_googleoauth2');

        echo '</label></td><td>';


        echo html_writer::empty_tag('input',
                array('type' => 'text', 'id' => 'googleuserprefix', 'name' => 'googleuserprefix',
                    'class' => 'googleuserprefix', 'value' => $config->googleuserprefix));

        if (isset($err["googleuserprefix"])) {
            echo $OUTPUT->error_text($err["googleuserprefix"]);
        }

        echo '</td><td>';

        print_string('auth_googleuserprefix', 'auth_googleoauth2') ;

        echo '</td></tr>';


        /// Block field options
        // Hidden email options - email must be set to: locked
        echo html_writer::empty_tag('input', array('type' => 'hidden', 'value' => 'locked',
                    'name' => 'lockconfig_field_lock_email'));

        //display other field options
        foreach ($user_fields as $key => $user_field) {
            if ($user_field == 'email') {
                unset($user_fields[$key]);
            }
        }
        print_auth_lock_options('googleoauth2', $user_fields, get_string('auth_fieldlocks_help', 'auth'), false, false);



        echo '</table>';
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config) {
        // set to defaults if undefined
        if (!isset ($config->googleclientid)) {
            $config->googleclientid = '';
        }
        if (!isset ($config->googleclientsecret)) {
            $config->googleclientsecret = '';
        }
        if (!isset ($config->facebookclientid)) {
            $config->facebookclientid = '';
        }
        if (!isset ($config->facebookclientsecret)) {
            $config->facebookclientsecret = '';
        }
        if (!isset ($config->linkedinclientid)) {
            $config->linkedinclientid = '';
        }
        if (!isset ($config->linkedinclientsecret)) {
            $config->linkedinclientsecret = '';
        }
        if (!isset ($config->messengerclientid)) {
            $config->messengerclientid = '';
        }
        if (!isset ($config->messengerclientsecret)) {
            $config->messengerclientsecret = '';
        }
        if (!isset ($config->msliveclientid)) {
            $config->msliveclientid = '';
        }
        if (!isset ($config->msliveclientsecret)) {
            $config->msliveclientsecret = '';
        }
        if (!isset ($config->googleipinfodbkey)) {
            $config->googleipinfodbkey = '';
        }
        if (!isset ($config->googleuserprefix)) {
            $config->googleuserprefix = 'social_user_';
        }
        if (!isset ($config->googleauthsecret)) {
            $config->googleauthsecret = substr(sha1(rand()), 0, 40);
        }

        // save settings
        set_config('googleclientid', $config->googleclientid, 'auth/googleoauth2');
        set_config('googleclientsecret', $config->googleclientsecret, 'auth/googleoauth2');
        set_config('facebookclientid', $config->facebookclientid, 'auth/googleoauth2');
        set_config('facebookclientsecret', $config->facebookclientsecret, 'auth/googleoauth2');
        set_config('messengerclientid', $config->messengerclientid, 'auth/googleoauth2');
        set_config('messengerclientsecret', $config->messengerclientsecret, 'auth/googleoauth2');
        set_config('googleipinfodbkey', $config->googleipinfodbkey, 'auth/googleoauth2');
        set_config('googleuserprefix', $config->googleuserprefix, 'auth/googleoauth2');
        set_config('linkedinclientid', $config->linkedinclientid, 'auth/googleoauth2');
        set_config('linkedinclientsecret', $config->linkedinclientsecret, 'auth/googleoauth2');
        set_config('googleauthsecret', $config->googleauthsecret, 'auth/googleoauth2');
        set_config('msliveclientid', $config->msliveclientid, 'auth/googleoauth2');
        set_config('msliveclientsecret', $config->msliveclientsecret, 'auth/googleoauth2');

        return true;
    }

    /**
     * Called when the user record is updated.
     *
     * We check there is no hack-attempt by a user to change his/her email address
     *
     * @param mixed $olduser     Userobject before modifications    (without system magic quotes)
     * @param mixed $newuser     Userobject new modified userobject (without system magic quotes)
     * @return boolean result
     *
     */
    function user_update($olduser, $newuser) {
        if ($olduser->email != $newuser->email) {
            return false;
        } else {
            return true;
        }
    }

}
