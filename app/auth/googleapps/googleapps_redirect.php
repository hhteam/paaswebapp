<?php

/*
 * Get google code and call the normal login page
 * Needed to add the parameter authprovider in order to identify the authentication provider
 */
require('../../config.php');
$code = optional_param('code', '', PARAM_TEXT); 
$error = optional_param('error', '', PARAM_TEXT);
$state = optional_param('state', '', PARAM_TEXT);

if(!empty($error)) {
     $errmsg = "Google".":".get_string("accessdenied","admin");
     $hostname = parse_url($CFG->wwwroot);
     $urltogo = $hostname['scheme']."://".$hostname['host']."/?".'&error='.rawurlencode($errmsg).'#login';
     redirect($urltogo);
}

if (empty($code)) {
    throw new moodle_exception('google_failure');
}

$loginurl = '/login/index.php';
if (!empty($CFG->alternateloginurl)) {
    $loginurl = $CFG->alternateloginurl;
}
$url = new moodle_url($loginurl, array('gcode' => $code, 'gauthprovider' => 'googleapps'));
redirect($url);
?>
