<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_google', language 'en'
 *
 * @package   auth_google
 * @author Jerome Mouneyrac
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Google Apps';
$string['auth_googleappsdescription'] = 'Settings for the google apps authentication plugin';
$string['auth_googlesettings'] = 'Google Market Application settings';
$string['auth_googleclientid'] = 'Your client ID - it can be found in the Google Apps.';
$string['auth_googleclientid_key'] = 'Google Apps Client ID';
$string['auth_googleclientsecret'] = 'Your client secret - it can be found in the Google Apps.';
$string['auth_googleclientsecret_key'] = 'Google Apps Client secret';
$string['auth_googleclientdomains_key'] = 'Google Apps permitted domain names';
$string['auth_googleclientdomains'] = 'Google Apps domains separated by comma';
