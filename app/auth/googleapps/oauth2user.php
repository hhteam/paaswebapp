<?php

require('../../config.php');

$loginurl = '/login/index.php';
if (!empty($CFG->alternateloginurl)) {
    $loginurl = $CFG->alternateloginurl;
}
$hostname = parse_url($CFG->wwwroot);
$gurl = "https://accounts.google.com/o/oauth2/auth";
$url = new moodle_url($gurl, array('client_id' => get_config('auth/googleapps', 'googleclientid'), 
                                   'redirect_uri' => $hostname['scheme']."://".$hostname['host']."/app/auth/googleoauth2/googleapps_redirect.php",
                                   'state' => substr(sha1(rand()), 0, 10),
                                   'response_type' => 'code',
                                   'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'));
redirect($url);
?>
