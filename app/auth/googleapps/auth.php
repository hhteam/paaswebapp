<?php

/**
 * @author Jerome Mouneyrac
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package moodle multiauth
 *
 * Authentication Plugin: Google/Facebook/Messenger Authentication
 * If the email doesn't exist, then the auth plugin creates the user.
 * If the email exist (and the user has for auth plugin this current one),
 * then the plugin login the user related to this email.
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->libdir.'/moodlelib.php');

/**
 * Google/Facebook/Messenger Oauth2 authentication plugin.
 */
class auth_plugin_googleapps extends auth_plugin_base {

    /**
     * Constructor.
     */
    function auth_plugin_googleapps() {
        $this->authtype = 'googleapps';
        $this->roleauth = 'auth_googleapps';
        $this->errorlogtag = '[AUTH GOOGLEAPPS] ';
        $this->config = get_config('auth/googleapps');
    }

    /**
     * Prevent authenticate_user_login() to update the password in the DB
     * @return boolean
     */
    function prevent_local_passwords() {
        //Allow to set a default password else auth logic breaks if username known !
        return false;
    }


    /**
     * Authenticates user against the selected authentication provide (Google, Facebook...)
     *
     * @param string $username The username (with system magic quotes)
     * @param string $password The password (with system magic quotes)
     * @return bool Authentication success or failure.
     */
    function user_login($username, $password) {
        global $DB, $CFG, $SESSION;

        //retrieve the user matching username
        $user = $DB->get_record('user', array('username' => $username,
            'mnethostid' => $CFG->mnet_localhost_id));
    
        //username must exist and have the right authentication method
        if (!empty($user) && ($user->auth == 'googleapps') && !empty($password)) {
           //check if auth request coming from this module oauth verified call, in that case
           //password verification does not really make any sense
           if (isset($SESSION->gappauthv) && ($user->password == $password)) { 
               return true;
           }
           //If password request is coming from outside the social auth plugin callback
           //say with user supplied username and password go via normalassword mechanism
           return validate_internal_user_password($user, $password);
        }

        return false;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return false;
    }

   // Extending the plugin functionality to allow user to login with username and password
   //====================================================================================
   /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object  (with system magic quotes)
     * @param  string  $newpassword Plaintext password (with system magic quotes)
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        $user = get_complete_user_data('id', $user->id);
        return update_internal_user_password($user, $newpassword);
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return true;
    }

     /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return true;
    }

     /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null; // use default internal method
    }

//Copy of function from moodlelig authenticate_user_login only change being that it does not  
//update_internal_user_password . Reason is that to extend the plugin functionality to also
//allow user to login with username/password and also by social login and we do not want to reset
//user password if user is logging in via social auth. 
/**
 * Authenticates a user against the chosen authentication mechanism
 *
 * Given a username and password, this function looks them
 * up using the currently selected authentication mechanism,
 * and if the authentication is successful, it returns a
 * valid $user object from the 'user' table.
 *
 * Uses auth_ functions from the currently active auth module
 *
 * After authenticate_user_login() returns success, you will need to
 * log that the user has logged in, and call complete_user_login() to set
 * the session up.
 *
 * Note: this function works only with non-mnet accounts!
 *
 * @param string $username  User's username
 * @param string $password  User's password
 * @return user|flase A {@link $USER} object or false if error
 */
private function gappauth_authenticate_user_login($username, $password) {
    global $CFG, $DB;

    $authsenabled = get_enabled_auth_plugins();

    if ($user = get_complete_user_data('username', $username, $CFG->mnet_localhost_id)) {
        $auth = empty($user->auth) ? 'manual' : $user->auth;  // use manual if auth not set
        if (!empty($user->suspended)) {
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Suspended Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }
        if ($auth=='nologin' or !is_enabled_auth($auth)) {
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Disabled Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }
        $auths = array($auth);

    } else {
        // check if there's a deleted record (cheaply)
        if ($DB->get_field('user', 'id', array('username'=>$username, 'deleted'=>1))) {
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Deleted Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }

        // User does not exist
        $auths = $authsenabled;
        $user = new stdClass();
        $user->id = 0;
    }

    foreach ($auths as $auth) {
        $authplugin = get_auth_plugin($auth);

        // on auth fail fall through to the next plugin
        if (!$authplugin->user_login($username, $password)) {
            continue;
        }

        // successful authentication
        if ($user->id) {                          // User already exists in database
            if (empty($user->auth)) {             // For some reason auth isn't set yet
                $DB->set_field('user', 'auth', $auth, array('username'=>$username));
                $user->auth = $auth;
            }

            //update_internal_user_password($user, $password); // just in case salt or encoding were changed (magic quotes too one day)

            if ($authplugin->is_synchronised_with_external()) { // update user record from external DB
                $user = update_user_record($username);
            }
        } else {
            add_to_log(SITEID, 'auth_googleapps', '', '', 'Not creating account after authentication, user not exist in DB' . $username); 
            continue;
            /* 
            // if user not found and user creation is not disabled, create it
            if (empty($CFG->authpreventaccountcreation)) {
                $user = create_user_record($username, $password, $auth);
            } else {
                continue;
            }
            */
        }

        $authplugin->sync_roles($user);

        foreach ($authsenabled as $hau) {
            $hauth = get_auth_plugin($hau);
            $hauth->user_authenticated_hook($user, $username, $password);
        }

        if (empty($user->id)) {
            return false;
        }

        if (!empty($user->suspended)) {
            // just in case some auth plugin suspended account
            add_to_log(SITEID, 'login', 'error', 'index.php', $username);
            error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Suspended Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
            return false;
        }

        return $user;
    }

    // failed if all the plugins have failed
    add_to_log(SITEID, 'login', 'error', 'index.php', $username);
    if (debugging('', DEBUG_ALL)) {
        error_log('[client '.getremoteaddr()."]  $CFG->wwwroot  Failed Login:  $username  ".$_SERVER['HTTP_USER_AGENT']);
    }
    return false;
}

//==============================Customization end======================================================


    /**
     * Authentication hook - is called every time user hit the login page
     * The code is run only if the param code is mentionned.
     */
    function loginpage_hook() {
        global $USER, $SESSION, $CFG, $DB;

        //check the Google authorization code
        $authorizationcode = optional_param('gcode', '', PARAM_TEXT);
        $isAdmin = false;
        $domain = $SESSION->domain;
        unset($SESSION->domain);
        if (!empty($authorizationcode)) {

            $authprovider = required_param('gauthprovider', PARAM_ALPHANUMEXT);

            //set the params specific to the authentication provider
            $params = array();

            switch ($authprovider) {
                case 'googleapps':
                    $params['client_id'] = get_config('auth/googleapps', 'googleclientid');
                    $params['client_secret'] = get_config('auth/googleapps', 'googleclientsecret');
                    $requestaccesstokenurl = 'https://accounts.google.com/o/oauth2/token';
                    $params['grant_type'] = 'authorization_code';
                    $params['redirect_uri'] = $CFG->wwwroot . '/auth/googleapps/googleapps_redirect.php';
                    $params['code'] = $authorizationcode;
                    break;
                default:
                    throw new moodle_exception('unknown_oauth2_provider');
                    break;
            }

            //request by curl an access token and refresh token
            require_once($CFG->libdir . '/filelib.php');
            $curl = new curl();
            $postreturnvalues = $curl->post($requestaccesstokenurl, $params);


            switch ($authprovider) {
                case 'googleapps':
                    $postreturnvalues = json_decode($postreturnvalues);
                    $accesstoken = $postreturnvalues->access_token;
                    //$refreshtoken = $postreturnvalues->refresh_token;
                    //$expiresin = $postreturnvalues->expires_in;
                    //$tokentype = $postreturnvalues->token_type;
                    break;
                default:
                    break;
            }

            //with access token request by curl the email address
            if (!empty($accesstoken)) {
                //get the username matching the email
                switch ($authprovider) {
                    case 'googleapps':
                        $params = array();
                        $params['access_token'] = $accesstoken;
                        $params['alt'] = 'json';
                        $postreturnvalues = $curl->get('https://www.googleapis.com/oauth2/v1/userinfo', $params);
                        if($curl->get_errno() == 0) {
                          $postreturnvalues = json_decode($postreturnvalues);//email, id, name, verified_email, given_name, family_name, link, gender, locale;
                          $useremail = $postreturnvalues->email;
                          $verified = $postreturnvalues->verified_email;
                          //Get admin details of this user this call may fail

                          $maxTry = 3;
                          do {
                            $adminval = $curl->get('https://www.googleapis.com/admin/directory/v1/users/'.$postreturnvalues->email,$params);
                          } while ($curl->get_errno() != 0 && (--$maxTry) > 0);

                          if($curl->get_errno() == 0) {
                           //This user may be admin, call success
                            $adminval = json_decode($adminval);
                            $isAdmin = $adminval->isAdmin;
                          } else {
                            // TODO: it doesn't mean this user is not
                            // admin. It means we have network error!
                          }
                        }
                        break;
                    default:
                        break;
                }
                // Prohibit if not admin 
                if (!$isAdmin) {
                   throw new moodle_exception("Google domain admin access only", 'auth_googleapps');
                }
                
                //throw an error if the email address is not verified
                if (!$verified) {
                    throw new moodle_exception('emailaddressmustbeverified', 'auth_googleapps');
                }

                // Prohibit login if email belongs to the prohibited domain
                if ($err = email_is_not_allowed($useremail)) {
                   throw new moodle_exception($err, 'auth_googleapps');
                }

                //if email not existing in user database then create a new username (userX).
                if (empty($useremail) or $useremail != clean_param($useremail, PARAM_EMAIL)) {
                    throw new moodle_exception('couldnotgetuseremail');
                    //TODO: display a link for people to retry
                }
                //get the user - don't bother with auth = googleapps because
                //authenticate_user_login() will fail it if it's not 'googleapps'
                $user = $DB->get_record('user', array('email' => $useremail, 'deleted' => 0, 'mnethostid' => $CFG->mnet_localhost_id));
                //create the user if it doesn't exist
                if (empty($user)) {
                    //If @email is set use email as username
                    $username = $useremail;
                    //retrieve more information from the provider
                    $picurl = ''; 
                    $newuser = new stdClass();
                    $newuser->email = $useremail;
                    switch ($authprovider) {
                        case 'googleapps':
                            $newuser->auth = 'googleapps';
                            if (array_key_exists ('given_name', $postreturnvalues)) {
                                $newuser->firstname = $postreturnvalues->given_name;
                            }
                            if (array_key_exists ('family_name', $postreturnvalues)) {
                                $newuser->lastname = $postreturnvalues->family_name;
                            }
                            if (array_key_exists ('locale', $postreturnvalues)) {
                                //$newuser->lang = $postreturnvalues->locale;
                                //TODO: convert the locale into correct Moodle language code
                            }
                            if (array_key_exists ('picture', $postreturnvalues)) {
                                $picurl = $postreturnvalues->picture;
                            }
                            break;
                        default:
                            break;
                    }

                    //retrieve country and city if the provider failed to give it
                    if (!isset($newuser->country) or !isset($newuser->city)) {
                        $googleipinfodbkey = get_config('auth/googleapps', 'googleipinfodbkey');
                        if (!empty($googleipinfodbkey)) {
                            $locationdata = $curl->get('http://api.ipinfodb.com/v3/ip-city/?key=' .
                                $googleipinfodbkey . '&ip='. getremoteaddr() . '&format=json' );
                            $locationdata = json_decode($locationdata);
                        }
                        if (!empty($locationdata)) {
                            //TODO: check that countryCode does match the Moodle country code
                            $newuser->country = isset($newuser->country)?isset($newuser->country):$locationdata->countryCode;
                            $newuser->city = isset($newuser->city)?isset($newuser->city):$locationdata->cityName;
                        }
                    }
                    if (!empty($newuser->email) && !empty($newuser->firstname) && !empty($newuser->lastname)) {
                        $userpassword = hash_hmac("md5", substr(sha1(rand()), 0, 30), get_config('auth/googleapps', 'googleauthsecret')); 
                        $cuser = create_user_record($username, $userpassword, 'googleapps');
                        add_to_log(SITEID, 'auth_googleapps', '', '', $newuser->firstname . '/' . $newuser->lastname . '/' . $cuser->id . '/' . $authprovider);
                        add_to_log(SITEID, 'auth_googleapps', '', '', $newuser->location . '/' . $newuser->institution . '/' . $newuser->country . '/' . $newuser->city);
                        //Updated basic information email/firstname/lastname
                        $newuser->id = $cuser->id;
                        $newuser->maildisplay = 0;
                        $newuser->password = $userpassword;
                        if($isAdmin) {
                          $newuser->department = $domain;
                        }
                        try {
			    $DB->update_record('user', $newuser);	
                            // A new user is created we need to add cohort and add this person as member if he is an admin!!
                            if($isAdmin) {
                              //New cohort needs to be created or existing
                              if(/*!$DB->record_exists('monorail_cohort_info', array('domain' => $domain)) && (is 'domain' supposed to be unique? there's plenty gmail.com domains...) */
                                !$DB->record_exists('monorail_cohort_admin_users', array('userid' => $newuser->id))) {  
                                //No record with the domain name , new cohort can be created
                                //Create cohort now and assign this user as admin
                                $record = new stdClass;
                                $dname = explode(".", $domain);
                                $record->name = ucfirst($dname[0]);
			        $record->description = 'Eliademy@'.$domain;
                                $record->descriptionformat = 1;
                                $record->timecreated = time();
                                $record->timemodified = time();
                                $record->contextid = context_system::instance()->id;

                                require_once($CFG->dirroot . '/cohort/lib.php');
                                $cohortid = cohort_add_cohort($record);

                                //Add admin record - default user who created the cohort is the admin
                                $adminrecord = new stdClass;
                                $adminrecord->cohortid = $cohortid;
                                $adminrecord->userid = $newuser->id;
                                $DB->insert_record('monorail_cohort_admin_users', $adminrecord);

                                //Add admin user as cohort member
                                cohort_add_member($cohortid, $newuser->id);

      				//Cohort info
			        $inforecord = new stdClass;
			        $inforecord->cohortid = $cohortid;
			        $inforecord->userid = $cuser->id;
			        $inforecord->userscount = 0;
			        $inforecord->domain = $domain;
			        $inforecord->domainrestrict = 1;
                    $inforecord->company_status = 1;
                    $inforecord->demo_period_end = strtotime("+30 days");
                                $DB->insert_record('monorail_cohort_info', $inforecord);
                            
                               if(!$DB->record_exists('monorail_gapp_creds', array('userid' => $cuser->id, 'domain' => $domain))) {
                                 //User is new add gapp oauth creds to db
                                 $gcreds = new stdClass();
                                 $gcreds->userid = $cuser->id;
                                 $gcreds->domain = $domain;
                                 $gcreds->token = "somerandomtoken";// Will be used once we move to oauth 2.0;
                                 $gcreds->secret = "somerandomsecret"; // Will be used once we move to oauth 2.0;
                                 $DB->insert_record('monorail_gapp_creds', $gcreds);
                               }
                               // send welcome email
                               require_once("$CFG->dirroot/theme/monorail/engagements.php");
                               engagements_trigger('welcome-email-cohort', $newuser->id);
                               //TODO: Auto enroll this user to custom courses
                              }
                            }     
                           //cohort_add_member($cohortid, $newuser->id);
			} catch (Exception $ex) {
                            add_to_log(SITEID, 'auth_googleapps', '', '', "Exception : ".get_class($ex).': '.$ex->getMessage());
                            $errmsg = get_string("servererror");
                            $hostname = parse_url($CFG->wwwroot);
                            $urltogo = $hostname['scheme']."://".$hostname['host'].'/?error='.rawurlencode($errmsg).'#login';
                            redirect($urltogo);
                            exit;
                        }
                    } else {
                        add_to_log(SITEID, 'auth_googleapps', '', '', 'Not creating account and exiting, no fullname ' . $authprovider);
                        $errmsg = ucfirst($authprovider).":".get_string("servererror");
                        $hostname = parse_url($CFG->wwwroot);
                        $urltogo = $hostname['scheme']."://".$hostname['host'].'/?error='.rawurlencode($errmsg).'#login';
                        redirect($urltogo);
                        exit;
                    }
                } else {
                    $username = $user->username;
                }

                //authenticate the user
                //TODO: delete this log later
                $userid = empty($user)?'new user':$user->id;
                $userpassword = empty($user)?$newuser->password:$user->password;
                add_to_log(SITEID, 'auth_googleapps', '', '', $username . '/' . $useremail . '/' . $userid);
                $SESSION->gappauthv = true;//Request from current oauth verified session;
                $user = $this->gappauth_authenticate_user_login($username, $userpassword);
                //$user = authenticate_user_login($username, $userpassword);
                unset($SESSION->gappauthv);
                if ($user) {

                    //set a cookie to remember what auth provider was selected
                    setcookie('MOODLEGOOGLEAPPS_'.$CFG->sessioncookie, $authprovider,
                            time()+(DAYSECS*60), $CFG->sessioncookiepath,
                            $CFG->sessioncookiedomain, $CFG->cookiesecure,
                            $CFG->cookiehttponly);

                    $socialplugins = array("facebook", "linkedin", "google");
                    $socialprovider = $authprovider;
                    if(strcmp($socialprovider, 'googleapps') == 0) {
                        $socialprovider = 'googleplus'; 
                    }
                    $sociallink = $DB->get_record('user_info_field', array('shortname'=>$socialprovider));
                    if($sociallink && in_array($authprovider, $socialplugins)) {
                        $updatesocial = false;
                        $uidata = $DB->get_record('user_info_data', array('userid' => $user->id, 'fieldid' => $sociallink->id));
                        if(!$uidata) {
                           $updatesocial = true;         
                        } else {
                           if(empty($datafield->data)) {
                               $updatesocial = true;         
                           }
                        }
                        if($updatesocial) {
                            $publicurl = '';
                            if(array_key_exists ('link', $postreturnvalues) && !empty($postreturnvalues->link)) {
                                $publicurl = $postreturnvalues->link;
                            } else if (array_key_exists ('publicProfileUrl', $postreturnvalues) && !empty($postreturnvalues->publicProfileUrl)) { 
                                $publicurl = $postreturnvalues->publicProfileUrl;
                            }
                            if(!empty($publicurl) && $uidata) {
                               $uidata->data = $publicurl;
                               $DB->update_record('user_info_data', $uidata);
                            } else if (!empty($publicurl) && !$uidata) {
                               $customdata = new  stdClass();
                               $customdata->userid = $user->id;
                               $customdata->fieldid = $sociallink->id;
                               $customdata->dataformat = 1;
                               $customdata->data = $publicurl;
                               $DB->insert_record('user_info_data', $customdata);
                            }
                        }
                    }
                    //prefill more user information if new user
                    if (!empty($newuser)) {
                        $newuser->id = $user->id;
                        //$DB->update_record('user', $newuser);
                        //If picurl is set get user profile image here
                        if(!empty($picurl)){
                            //We have the provider url , create and set the moodle icon url 
                            $context = get_context_instance(CONTEXT_USER, $newuser->id, MUST_EXIST);
                            $fs = get_file_storage();
                            $fileinfo = array(
                                'contextid' => $context->id,
                                'component' => 'user',
                                'filearea' => 'draft',
                                'itemid' => 0,
                                'filepath' => '/',
                                );
                            $file = $fs->create_file_from_url($fileinfo, $picurl);
                            $iconfile = $file->copy_content_to_temp();
                            require_once("$CFG->dirroot/theme/monorail/lib.php");
                            require_once($CFG->libdir . '/gdlib.php');
                            monorail_process_avatar($newuser->id, $iconfile);
                            $newpicture = (int)process_new_icon($context, 'user', 'icon', 0, $iconfile);
                            @unlink($iconfile);
                            $DB->set_field('user', 'picture', $newpicture, array('id' => $newuser->id));
                        }
                        //Use the updated record       
                        $user = $DB->get_record('user', array('email' => $useremail, 'deleted' => 0, 'mnethostid' => $CFG->mnet_localhost_id));
                    }

                    complete_user_login($user);
                    if (!empty($newuser)) {
                        set_user_preference("email_send_count", 10);
                        set_user_preference("email_bounce_count", 1); 
                        if(!$isAdmin) {
                          require_once("$CFG->dirroot/theme/monorail/engagements.php");
                          engagements_trigger('welcome-email', $user->id);
                        }
                    }
                    // Redirection
                    if (user_not_fully_set_up($USER)) {
                        $urltogo = $CFG->wwwroot.'/user/edit.php';
                        // We don't delete $SESSION->wantsurl yet, so we get there later
                    } else if (isset($_COOKIE["loginRedirect"]) && !empty($_COOKIE["loginRedirect"])) {
                        $urltogo = $_COOKIE["loginRedirect"];
                        setcookie("loginRedirect", "", time() - 3600, "/");
                    } else if (isset($SESSION->wantsurl) and (strpos($SESSION->wantsurl, $CFG->wwwroot) === 0)) {
                        $urltogo = $SESSION->wantsurl;    // Because it's an address in this site
                        unset($SESSION->wantsurl);
                    } else {
                        // No wantsurl stored or external - go to homepage
                        $urltogo = $urltogo = $CFG->wwwroot.'/a/organization-details';
                        unset($SESSION->wantsurl);
                    }
                    redirect($urltogo);
                } else {
                    $errmsg = get_string("emailexists");
                    if($userid != 'new user')
                        $errmsg = ucfirst($authprovider).":".get_string("authentication")." ".get_string("error");
                    $hostname = parse_url($CFG->wwwroot);
                    $urltogo = $hostname['scheme']."://".$hostname['host']."/?username=".urlencode($username).'&error='.rawurlencode($errmsg).'#login';
                    redirect($urltogo);
                }
            } else {
                throw new moodle_exception('couldnotgetgoogleaccesstoken', 'auth_googleapps');
            }
        }
    }

/**
   * Prints a form for configuring this authentication plugin.
   *
   * This function is called from admin/auth.php, and outputs a full page with
   * a form for configuring this plugin.
   *
   * TODO: as print_auth_lock_options() core function displays an old-fashion HTML table, I didn't bother writing
   * some proper Moodle code. This code is similar to other auth plugins (04/09/11)
   *
   * @param array $page An object containing all the data for this page.
   */
  function config_form($config, $err, $user_fields) {
   global $OUTPUT;

   // set to defaults if undefined
   if (!isset($config->googleclientid)) {
    $config->googleclientid = '';
   }
   if (!isset($config->googleclientsecret)) {
    $config->googleclientsecret = '';
   }
   if (!isset($config->googleclientdomains)) {
    $config->googleclientsecret = '';
   }
   echo '<table cellspacing="0" cellpadding="5" border="0">
     <tr>
     <td colspan="3">
     <h2 class="main">';

   print_string('auth_googlesettings', 'auth_googleapps');

   // Google client id

   echo '</h2>
     </td>
     </tr>
     <tr>
     <td align="right"><label for="googleclientid">';

   print_string('auth_googleclientid_key', 'auth_googleapps');

   echo '</label></td><td>';


   echo html_writer::empty_tag('input',
     array('type' => 'text', 'id' => 'googleclientid', 'name' => 'googleclientid',
       'class' => 'googleclientid', 'value' => $config->googleclientid));

   if (isset($err["googleclientid"])) {
    echo $OUTPUT->error_text($err["googleclientid"]);
   }

   echo '</td><td>';

   print_string('auth_googleclientid', 'auth_googleapps') ;

   echo '</td></tr>';

   // Google client secret

   echo '<tr>
     <td align="right"><label for="googleclientsecret">';

   print_string('auth_googleclientsecret_key', 'auth_googleapps');

   echo '</label></td><td>';


   echo html_writer::empty_tag('input',
     array('type' => 'text', 'id' => 'googleclientsecret', 'name' => 'googleclientsecret',
       'class' => 'googleclientsecret', 'value' => $config->googleclientsecret));

   if (isset($err["googleclientsecret"])) {
    echo $OUTPUT->error_text($err["googleclientsecret"]);
   }

   echo '</td><td>';

   print_string('auth_googleclientsecret', 'auth_googleapps') ;

   echo '</td></tr>';

   // Google client domains

   echo '<tr>
     <td align="right"><label for="googleclientdomains">';

   print_string('auth_googleclientdomains_key', 'auth_googleapps');

   echo '</label></td><td>';


   echo html_writer::empty_tag('input',
     array('type' => 'text', 'id' => 'googleclientdomains', 'name' => 'googleclientdomains',
       'class' => 'googleclientdomains', 'value' => $config->googleclientdomains));

   if (isset($err["googleclientdomains"])) {
    echo $OUTPUT->error_text($err["googleclientdomains"]);
   }

   echo '</td><td>';

   print_string('auth_googleclientdomains', 'auth_googleapps') ;


   print_auth_lock_options('googleapps', $user_fields, get_string('auth_fieldlocks_help', 'auth'), false, false);

   echo '</table>';
  }

  /**
   * Processes and stores configuration data for this authentication plugin.
   */
  function process_config($config) {
   // set to defaults if undefined
   if (!isset ($config->googleclientid)) {
    $config->googleclientid = '';
   }
   if (!isset ($config->googleclientsecret)) {
    $config->googleclientsecret = '';
   }
   if (!isset ($config->googleclientdomains)) {
    $config->googleclientdomains = '';
   }

   // save settings
   set_config('googleclientid', $config->googleclientid, 'auth/googleapps');
   set_config('googleclientsecret', $config->googleclientsecret, 'auth/googleapps');
   set_config('googleclientdomains', $config->googleclientdomains, 'auth/googleapps');

   return true;
  }

    /**
     * Called when the user record is updated.
     *
     * We check there is no hack-attempt by a user to change his/her email address
     *
     * @param mixed $olduser     Userobject before modifications    (without system magic quotes)
     * @param mixed $newuser     Userobject new modified userobject (without system magic quotes)
     * @return boolean result
     *
     */
    function user_update($olduser, $newuser) {
        if ($olduser->email != $newuser->email) {
            return false;
        } else {
            return true;
        }
    }

}
