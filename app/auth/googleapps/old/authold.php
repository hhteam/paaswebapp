<?php

/**
 * Part of the code is from the googleoauth2 plugin by
 * @author Jerome Mouneyrac
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 *
 * @package googleapps auth
 *
 * Authentication Plugin: Google Apps Authentication
 *
 */

if (!defined('MOODLE_INTERNAL')) {
 die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/authlib.php');

/**
 * Google Apps Oauth2 authentication plugin.
*/
class auth_plugin_googleapps extends auth_plugin_base {

 /**
  * Constructor.
  */
 function auth_plugin_googleapps() {
  $this->authtype = 'googleapps';
  $this->roleauth = 'auth_googleapps';
  $this->errorlogtag = '[AUTH GOOGLEAPPS] ';
  $this->config = get_config('auth/googleapps');
 }

 /**
  * Prevent authenticate_user_login() to update the password in the DB
  * @return boolean
  */
 function prevent_local_passwords() {
  //Allow to set a default password else auth logic breaks if username known !
  return false;
 }

 /**
  * Authenticates user against the selected authentication provide (Google, Facebook...)
  *
  * @param string $username The username (with system magic quotes)
  * @param string $password The password (with system magic quotes)
  * @return bool Authentication success or failure.
  */
 function user_login($username, $password) {
  global $DB, $CFG;

  //retrieve the user matching username
  $user = $DB->get_record('user', array('username' => $username,
    'mnethostid' => $CFG->mnet_localhost_id));

  //username must exist and have the right authentication method
  if (!empty($user) && ($user->auth == 'googleapps') && !empty($password) && ($password == $user->password)) {
   return true;
  }

  return false;
 }

 /**
  * Returns true if this authentication plugin is 'internal'.
  *
  * @return bool
  */
 function is_internal() {
  return false;
 }

 /**
  * Returns true if this authentication plugin can change the user's
  * password.
  *
  * @return bool
  */
 function can_change_password() {
  return false;
 }

 /**
  * Authentication hook - is called every time user hit the login page
  * The code is run only if the param code is mentionned.
  */
 function loginpage_hook() {
  global $USER, $SESSION, $CFG, $DB;

  $email = optional_param('email', '', PARAM_TEXT);
  if (preg_match("/(.*)@(.*)/", $email, $matches)) {
   $domain = $matches[2];
   $username = $matches[1];
  }
  //check the provider
  $provider = optional_param('authprovider', '', PARAM_TEXT);
  if(!empty($email) && !empty($provider) && (strcmp($provider, "googleapps") == 0) && $_SESSION['OPENID_AUTH']){
   require_once('lib/gfunc.php');
   try {
       $gresponse = gfunc_get_domain_details($domain,$username);
   } catch (Exception $ex) {
       add_to_log(SITEID, 'auth_googleappps', '', '', "Exception : ".get_class($ex).': '.$ex->getMessage());
       $errmsg = get_string("servererror");
       $hostname = parse_url($CFG->wwwroot);
       $urltogo = $hostname['scheme']."://".$hostname['host'].'/?error='.rawurlencode($errmsg).'#login';
       redirect($urltogo);
       exit;
   } 
   $summary = $gresponse['user'];
   //If user is admin then proceed to create user and cohort else exit!
   if($summary && $summary->login->admin) {
    $semail = $summary->login->userName.'@'.$domain;
    $sfname = $summary->name->givenName;
    $slname = $summary->name->familyName;
    $user = $DB->get_record('user', array('email' => $semail, 'deleted' => 0, 'suspended' => 0,'mnethostid' => $CFG->mnet_localhost_id,
      'username' => $semail));
    if (empty($user)) {
     //Create account
     if (!empty($semail) && !empty($sfname) && !empty($slname)) {
      $userpassword = hash_hmac("md5", substr(sha1(rand()), 0, 30), get_config('auth/googleapps', 'googleclientsecret'));
      $cuser = create_user_record($semail, $userpassword, 'googleapps');
      add_to_log(SITEID, 'auth_googleapps', '', '', $sfname . '/' . $slname . '/' . $cuser->id);
      //Updated basic information email/firstname/lastname
      $newuser = new stdClass();
      $newuser->email = $semail;
      $newuser->firstname = $sfname;
      $newuser->username = $semail;
      $newuser->lastname = $slname;
      $newuser->id = $cuser->id;
      $newuser->department = $domain;
      $newuser->password = $userpassword;
      $DB->update_record('user', $newuser);

       //User is new add gapp oauth creds to db
       $gcreds = new stdClass();
       $gcreds->userid = $cuser->id;
       $gcreds->domain = $domain;
       $gcreds->token = "somerandomtoken";// Will be used once we move to oauth 2.0;
       $gcreds->secret = "somerandomsecret"; // Will be used once we move to oauth 2.0;
       $DB->insert_record('monorail_gapp_creds', $gcreds);  
     } else {
      //Cannot create account!
      add_to_log(SITEID, 'auth_googleapps', '', '', 'Not creating account and exiting');
      $hostname = parse_url($CFG->wwwroot);
      $urltogo = $hostname['scheme']."://".$hostname['host'].'/?error='.rawurlencode("Google Apps: Server Error").'#login';
      redirect($urltogo);
      exit;
     }

    } else {
       $gcreds = new stdClass();
       $gcreds->userid = $user->id;
       $gcreds->domain = $domain;
       $gcreds->token = "somerandomtoken";// Will be used once we move to oauth 2.0;;
       $gcreds->secret = "somerandomsecret"; // Will be used once we move to oauth 2.0; 
       $credsrec = $DB->get_record('monorail_gapp_creds', array('userid' => $user->id, 'domain' => $domain));
       if(empty($credsrec)){
          $DB->insert_record('monorail_gapp_creds', $gcreds); 
       } else { 
           $gcreds->id = $credsrec->id;
           $DB->update_record('monorail_gapp_creds', $gcreds);  
       }
    }
    $deluserid =  empty($user)? $newuser->id : $user->id;
    $DB->delete_records('monorail_data', array('itemid' => $deluserid, 'datakey' => $domain.'_domainusers', 'type' => 'TEXT'));
    //Domain admin , domain users list
    $data = new stdClass();
    $data->type             = 'TEXT';
    $data->itemid           = empty($user)? $newuser->id : $user->id;
    $data->datakey          = $domain.'_domainusers';
    $data->value            = $gresponse['domainusers'];
    $data->timemodified = time();
    $result = $DB->insert_record('monorail_data', $data);

    $userpassword = empty($user)?$newuser->password:$user->password;
    $username = empty($user)?$newuser->username:$user->username;
    $auser = authenticate_user_login($username, $userpassword);
    if(!empty($auser)){
     if (!empty($newuser)) {
      set_user_preference("email_send_count", 10);
      set_user_preference("email_bounce_count", 1);
      //Create cohort now and assign this user as admin
      $record = new stdClass;
      $dname = explode(".", $domain);
      $record->name = ucfirst($dname[0]);
      $record->description = 'Eliademy@'.$domain;
      $record->descriptionformat = 1;
      $record->timecreated = time();
      $record->timemodified = time();
      $record->contextid = context_system::instance()->id;

      require_once($CFG->dirroot . '/cohort/lib.php');
      $cohortid = cohort_add_cohort($record);

      //Add admin record - default user who created the cohort is the admin
      $adminrecord = new stdClass;
      $adminrecord->cohortid = $cohortid;
      $adminrecord->userid = $newuser->id;
      $DB->insert_record('monorail_cohort_admin_users', $adminrecord);

      //Add admin user as cohort member
      cohort_add_member($cohortid, $newuser->id);

      //Cohort info
      $inforecord = new stdClass;
      $inforecord->cohortid = $cohortid;
      $inforecord->userid = $cuser->id;
      $inforecord->userscount = 5;
      $inforecord->domain = $domain;
      $inforecord->domainrestrict = 1;
      $DB->insert_record('monorail_cohort_info', $inforecord);

      // send welcome email
      require_once("$CFG->dirroot/theme/monorail/engagements.php");
      engagements_trigger('welcome-email-cohort', $newuser->id);
      
      //TODO: Auto enroll this user to custom courses
     }
     complete_user_login($auser);
     
     // Redirection
     if (user_not_fully_set_up($USER)) {
      $urltogo = $CFG->wwwroot.'/user/edit.php';
      // We don't delete $SESSION->wantsurl yet, so we get there later
     } else if (isset($_COOKIE["loginRedirect"]) && !empty($_COOKIE["loginRedirect"])) {
      $urltogo = $_COOKIE["loginRedirect"];
      setcookie("loginRedirect", "", time() - 3600, "/");
     } else if (isset($SESSION->wantsurl) and (strpos($SESSION->wantsurl, $CFG->wwwroot) === 0)) {
      $urltogo = $SESSION->wantsurl;    // Because it's an address in this site
      unset($SESSION->wantsurl);
     } else {
      // No wantsurl stored or external - go to homepage
      $urltogo = $CFG->wwwroot.'/a/organization-details';
      unset($SESSION->wantsurl);
     }
     redirect($urltogo);
    } else {
     $hostname = parse_url($CFG->wwwroot);
     $urltogo = $hostname['scheme']."://".$hostname['host']."/?username=".urlencode($username).'&error='.rawurlencode("GoogleApps: Error").'#login';
     redirect($urltogo);
    }
   }
   } else {
      // $errmsg = get_string("servererror");
      // $hostname = parse_url($CFG->wwwroot);
      // $urltogo = $hostname['scheme']."://".$hostname['host'].'/?error='.rawurlencode($errmsg).'#login';
      // redirect($urltogo);
      // exit;
   }
  }


  /**
   * Prints a form for configuring this authentication plugin.
   *
   * This function is called from admin/auth.php, and outputs a full page with
   * a form for configuring this plugin.
   *
   * TODO: as print_auth_lock_options() core function displays an old-fashion HTML table, I didn't bother writing
   * some proper Moodle code. This code is similar to other auth plugins (04/09/11)
   *
   * @param array $page An object containing all the data for this page.
   */
  function config_form($config, $err, $user_fields) {
   global $OUTPUT;

   // set to defaults if undefined
   if (!isset($config->googleclientid)) {
    $config->googleclientid = '';
   }
   if (!isset($config->googleclientsecret)) {
    $config->googleclientsecret = '';
   }
   if (!isset($config->googleclientdomains)) {
    $config->googleclientsecret = '';
   }
   echo '<table cellspacing="0" cellpadding="5" border="0">
     <tr>
     <td colspan="3">
     <h2 class="main">';

   print_string('auth_googlesettings', 'auth_googleapps');

   // Google client id

   echo '</h2>
     </td>
     </tr>
     <tr>
     <td align="right"><label for="googleclientid">';

   print_string('auth_googleclientid_key', 'auth_googleapps');

   echo '</label></td><td>';


   echo html_writer::empty_tag('input',
     array('type' => 'text', 'id' => 'googleclientid', 'name' => 'googleclientid',
       'class' => 'googleclientid', 'value' => $config->googleclientid));

   if (isset($err["googleclientid"])) {
    echo $OUTPUT->error_text($err["googleclientid"]);
   }

   echo '</td><td>';

   print_string('auth_googleclientid', 'auth_googleapps') ;

   echo '</td></tr>';

   // Google client secret

   echo '<tr>
     <td align="right"><label for="googleclientsecret">';

   print_string('auth_googleclientsecret_key', 'auth_googleapps');

   echo '</label></td><td>';


   echo html_writer::empty_tag('input',
     array('type' => 'text', 'id' => 'googleclientsecret', 'name' => 'googleclientsecret',
       'class' => 'googleclientsecret', 'value' => $config->googleclientsecret));

   if (isset($err["googleclientsecret"])) {
    echo $OUTPUT->error_text($err["googleclientsecret"]);
   }

   echo '</td><td>';

   print_string('auth_googleclientsecret', 'auth_googleapps') ;

   echo '</td></tr>';

   // Google client domains

   echo '<tr>
     <td align="right"><label for="googleclientdomains">';

   print_string('auth_googleclientdomains_key', 'auth_googleapps');

   echo '</label></td><td>';


   echo html_writer::empty_tag('input',
     array('type' => 'text', 'id' => 'googleclientdomains', 'name' => 'googleclientdomains',
       'class' => 'googleclientdomains', 'value' => $config->googleclientdomains));

   if (isset($err["googleclientdomains"])) {
    echo $OUTPUT->error_text($err["googleclientdomains"]);
   }

   echo '</td><td>';

   print_string('auth_googleclientdomains', 'auth_googleapps') ;


   print_auth_lock_options('googleapps', $user_fields, get_string('auth_fieldlocks_help', 'auth'), false, false);

   echo '</table>';
  }

  /**
   * Processes and stores configuration data for this authentication plugin.
   */
  function process_config($config) {
   // set to defaults if undefined
   if (!isset ($config->googleclientid)) {
    $config->googleclientid = '';
   }
   if (!isset ($config->googleclientsecret)) {
    $config->googleclientsecret = '';
   }
   if (!isset ($config->googleclientdomains)) {
    $config->googleclientdomains = '';
   }

   // save settings
   set_config('googleclientid', $config->googleclientid, 'auth/googleapps');
   set_config('googleclientsecret', $config->googleclientsecret, 'auth/googleapps');
   set_config('googleclientdomains', $config->googleclientdomains, 'auth/googleapps');

   return true;
  }

  /**
   * Called when the user record is updated.
   *
   * We check there is no hack-attempt by a user to change his/her email address
   *
   * @param mixed $olduser     Userobject before modifications    (without system magic quotes)
   * @param mixed $newuser     Userobject new modified userobject (without system magic quotes)
   * @return boolean result
   *
   */
  function user_update($olduser, $newuser) {
   if ($olduser->email != $newuser->email) {
    return false;
   } else {
    return true;
   }
  }

 }
