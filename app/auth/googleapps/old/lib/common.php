<?php

set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));
require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
global $CFG;


$CONSUMER_KEY = get_config('auth/googleapps', 'googleclientid');;
$CONSUMER_SECRET = get_config('auth/googleapps', 'googleclientsecret');;

$BASE_URL = $CFG->wwwroot . '/auth/googleapps/lib';
$STORE_PATH = "/tmp/_php_consumer_test";

/**
 * Include the OpenID libraries, including the special
 * Google Apps discovery code required for authenticating
 * Google Apps users via OpenID
 */
require_once "Auth/OpenID/google_discovery.php";
require_once "Auth/OpenID/AX.php";
require_once "Auth/OpenID/Consumer.php";
require_once "Auth/OpenID/FileStore.php";
require_once "Auth/OpenID/PAPE.php";

//if(!(session_status() == PHP_SESSION_ACTIVE)) {
 session_start();
//}

if (!file_exists($STORE_PATH) &&
    !mkdir($STORE_PATH)) {
    print "Could not create the FileStore directory '$STORE_PATH'. ".
        " Please check the effective permissions.";
    exit(0);
}

$store = new Auth_OpenID_FileStore($STORE_PATH);
$consumer = new Auth_OpenID_Consumer($store);
new GApps_OpenID_Discovery($consumer);
?>
