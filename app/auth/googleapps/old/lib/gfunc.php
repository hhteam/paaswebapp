<?php

function gfunc_get_domain_details($domain,$username) {
 require_once 'common.php';
 require_once 'Zend/Gdata/Gapps.php';
 require_once 'Zend/Oauth/Consumer.php';
 $options = array(
    'requestScheme' => Zend_Oauth::REQUEST_SCHEME_HEADER,
    'version' => '1.0',
    'signatureMethod' => 'HMAC-SHA1',
    'consumerKey' => $CONSUMER_KEY,
    'consumerSecret' => $CONSUMER_SECRET
 );
 $consumer = new Zend_Oauth_Consumer($options);
 $token = new Zend_Oauth_Token_Access();
 $httpClient = $token->getHttpClient($options);
 $gdata = new Zend_Gdata_Gapps($httpClient,$domain);
 $summary = $gdata->retrieveUser($username);
 $emails = '';
 if($summary && $summary->login->admin) {
     $users = $gdata->retrieveAllUsers();
     foreach ($users as $user) {
         $emails .= $user->login->username.'@'.$domain.',';
     }
 }
 $response = array();
 $response['user'] = $summary;
 $response['domainusers'] = $emails;
 return $response;
}

?>
