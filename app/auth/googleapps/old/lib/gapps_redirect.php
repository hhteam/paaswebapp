<?php
/*
* Needed to add the parameter authprovider in order to identify the authentication provider
*/
require('../../../config.php');
require_once "common.php";

$response = $consumer->complete($BASE_URL . '/gapps_redirect.php');

// set session variable depending on authentication result
if ($response->status == Auth_OpenID_SUCCESS) {
  $_SESSION['OPENID_AUTH'] = true;
  $ax = new Auth_OpenID_AX_FetchResponse();
  $data = $ax->fromSuccessResponse($response)->data;
  $oid = $response->endpoint->claimed_id;
  $_SESSION['firstName'] = $data['http://axschema.org/namePerson/first'][0];
  $_SESSION['lastName'] = $data['http://axschema.org/namePerson/last'][0];
  $_SESSION['email'] = $data['http://axschema.org/contact/email'][0];

  if(isset( $_SESSION['email'])) {
      $loginurl = '/login/index.php';
      if (!empty($CFG->alternateloginurl)) {
      $loginurl = $CFG->alternateloginurl;
      }
      $url = new moodle_url($loginurl, array('email' => $_SESSION['email'], 'authprovider' => 'googleapps'));
      redirect($url);
  } else {
      die('Unable to get the user info!');
  }   
} else {
  $_SESSION['OPENID_AUTH'] = false;
  throw new moodle_exception('google_failure');
  die('Unable to get the user info!');
}
?>
