<?php

require('../../config.php');

$domain = required_param('domain', PARAM_TEXT); //Google domain 
$loginurl = '/login/index.php';
if (!empty($CFG->alternateloginurl)) {
    $loginurl = $CFG->alternateloginurl;
}
global $SESSION;
$SESSION->domain = $domain;
$hostname = parse_url($CFG->wwwroot);
$gurl = "https://accounts.google.com/o/oauth2/auth";
$url = new moodle_url($gurl, array('client_id' => get_config('auth/googleapps', 'googleclientid'), 
                                   'redirect_uri' => $hostname['scheme']."://".$hostname['host']."/app/auth/googleapps/googleapps_redirect.php",
                                   'state' => substr(sha1(rand()), 0, 10),
                                   'response_type' => 'code',
                                   'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/admin.directory.user.readonly'));
redirect($url);
?>
