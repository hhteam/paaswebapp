<?php
// This file is not a part of Moodle - http://moodle.org/
// This is a none core contributed module.
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// The GNU General Public License
// can be see at <http://www.gnu.org/licenses/>.

/**
 * Google Apps authentication plugin version specification.
 *
 * @package    auth
 * @subpackage googleapps
 * @copyright  2013 CBTec
 * @license
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version  = 2014121100;
$plugin->requires = 2011070100;
$plugin->release = '1.0 (Build: 2012010900)';
$plugin->maturity = MATURITY_ALPHA;
