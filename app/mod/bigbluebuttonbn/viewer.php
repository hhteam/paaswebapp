<?php
/**
 * Join a BigBlueButton room
 *
 * @package   mod_bigbluebuttonbn
 * @author    Fred Dixon  (ffdixon [at] blindsidenetworks [dt] com)
 * @author    Jesus Federico  (jesus [at] blindsidenetworks [dt] com)
 * @copyright 2010-2014 Blindside Networks Inc.
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v2 or later
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/locallib.php');
require_once($CFG->libdir . '/completionlib.php');


function bigbluebuttonbn_view_joining( $bbbsession, $context, $bigbluebuttonbn ) {
    global $CFG, $DB;


    $joining = false;

    if( $bbbsession['flag']['moderator'] || !$bbbsession['flag']['wait'] ) {  // If is a moderator or if is a viewer and no waiting is required
        //
        // Join directly
        //
        $metadata = array("meta_origin" => $bbbsession['origin'],
                "meta_originVersion" => $bbbsession['originVersion'],
                "meta_originServerName" => $bbbsession['originServerName'],
                "meta_originServerCommonName" => $bbbsession['originServerCommonName'],
                "meta_originTag" => $bbbsession['originTag'],
                "meta_context" => $bbbsession['context'],
                "meta_contextActivity" => $bbbsession['contextActivity'],
                "meta_contextActivityDescription" => $bbbsession['contextActivityDescription'],
                "meta_recording" => $bbbsession['textflag']['record']);
        $response = bigbluebuttonbn_getCreateMeetingArray( $bbbsession['meetingname'], $bbbsession['meetingid'], $bbbsession['welcome'], $bbbsession['modPW'], $bbbsession['viewerPW'], $bbbsession['salt'], $bbbsession['url'], $bbbsession['logoutURL'], $bbbsession['textflag']['record'], $bbbsession['timeduration'], $bbbsession['voicebridge'], $metadata );

        if (!$response) {
            if ( $bbbsession['flag']['administrator'] )
               return array('error' => get_string('view_error_unable_join', 'bigbluebuttonbn')); 
            else if ( $bbbsession['flag']['moderator'] )
               return array('error' => get_string('view_error_unable_join_teacher', 'bigbluebuttonbn'));
            else
               return array('error' => get_string('view_error_unable_join_student', 'bigbluebuttonbn'));
        } else if( $response['returncode'] == "FAILED" ) {
            return array('error' => get_string( 'index_error_checksum', 'bigbluebuttonbn' ));
        } else if ($response['hasBeenForciblyEnded'] == "true"){
            return array('error' => get_string( 'index_error_forciblyended', 'bigbluebuttonbn' ));
        } else { ///////////////Everything is ok /////////////////////
            $joining = true;
        }
    } else {
       $joining = true;
    }
    return $joining;
}

function get_bigblue_url($id) {
  global $CFG, $DB, $USER;
  if ($id) {
    $cm = get_coursemodule_from_id('bigbluebuttonbn', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $bigbluebuttonbn = $DB->get_record('bigbluebuttonbn', array('id' => $cm->instance), '*', MUST_EXIST);
  } else {
    return array("error" => 'You must specify a course_module ID or an instance ID');
  }

  if ( $CFG->version < '2013111800' ) {
    //This is valid before v2.6

    $module = $DB->get_record('modules', array('name' => 'bigbluebuttonbn'));

    $module_version = $module->version;
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

  }

  if ( $CFG->version < '2014051200' ) {

    //This is valid before v2.7
    add_to_log($course->id, 'bigbluebuttonbn', 'view', 'view.php?id=$cm->id', $bigbluebuttonbn->name, $cm->id);

  }
  $moderator = has_capability('mod/bigbluebuttonbn:moderate', $context);
  $administrator = has_capability('moodle/category:manage', $context);

  //Validates if the BigBlueButton server is running 
  //BigBlueButton server data
  $bbbsession['salt'] = trim($CFG->BigBlueButtonBNSecuritySalt);
  $bbbsession['url'] = trim(trim($CFG->BigBlueButtonBNServerURL),'/').'/';

  $serverVersion = bigbluebuttonbn_getServerVersion($bbbsession['url']); 
  if ( !isset($serverVersion) ) { //Server is not working
    if ( $administrator )
        return array('error' => get_string('view_error_unable_join', 'bigbluebuttonbn'));
    else if ( $moderator )
        return array('error' => get_string('view_error_unable_join_teacher', 'bigbluebuttonbn'));
    else
        return array('error' => get_string('view_error_unable_join_student', 'bigbluebuttonbn'));
  } else {
    $xml = bigbluebuttonbn_wrap_simplexml_load_file( bigbluebuttonbn_getMeetingsURL( $bbbsession['url'], $bbbsession['salt'] ) );
    if ( !isset($xml) || $xml->returncode == 'FAILED' ){ // The salt is wrong
        if ( $administrator ) 
            return array('error' => get_string('view_error_unable_join', 'bigbluebuttonbn'));
        else if ( $moderator )
            return array('error' => get_string('view_error_unable_join_teacher', 'bigbluebuttonbn'));
        else
            return array('error' => get_string('view_error_unable_join_student', 'bigbluebuttonbn'));
    }
  }

  //// BigBlueButton Setup Starts

  //Server data
  $bbbsession['modPW'] = $bigbluebuttonbn->moderatorpass;
  $bbbsession['viewerPW'] = $bigbluebuttonbn->viewerpass;
  //User data
  $bbbsession['username'] = $USER->firstname.' '.$USER->lastname;
  $bbbsession['userID'] = $USER->id;
  $bbbsession['flag']['moderator'] = $moderator;
  $bbbsession['textflag']['moderator'] = $moderator? 'true': 'false';
  $bbbsession['flag']['administrator'] = $administrator;
  $bbbsession['textflag']['administrator'] = $administrator? 'true': 'false';

  //Database info related to the activity
  $bbbsession['meetingname'] = "Eliademy:".$course->fullname.":".$bigbluebuttonbn->name;
  $bbbsession['welcome'] = $bigbluebuttonbn->welcome;
  if( !isset($bbbsession['welcome']) || $bbbsession['welcome'] == '') {
    $bbbsession['welcome'] = get_string('mod_form_field_welcome_default', 'bigbluebuttonbn'); 
  }

  $bbbsession['voicebridge'] = $bigbluebuttonbn->voicebridge;
  $bbbsession['description'] = $bigbluebuttonbn->description;
  $bbbsession['flag']['newwindow'] = $bigbluebuttonbn->newwindow;
  $bbbsession['flag']['wait'] = $bigbluebuttonbn->wait;
  $bbbsession['flag']['allmoderators'] = $bigbluebuttonbn->allmoderators;
  $bbbsession['flag']['record'] = $bigbluebuttonbn->record;
  $bbbsession['textflag']['newwindow'] = $bigbluebuttonbn->newwindow? 'true':'false';
  $bbbsession['textflag']['wait'] = $bigbluebuttonbn->wait? 'true': 'false';
  $bbbsession['textflag']['record'] = $bigbluebuttonbn->record? 'true': 'false';
  $bbbsession['textflag']['allmoderators'] = $bigbluebuttonbn->allmoderators? 'true': 'false';
  if( $bigbluebuttonbn->record )
    $bbbsession['welcome'] .= '<br><br>'.get_string('bbbrecordwarning', 'bigbluebuttonbn');

  $bbbsession['timeavailable'] = $bigbluebuttonbn->timeavailable;
  $bbbsession['timedue'] = $bigbluebuttonbn->timedue;
  $bbbsession['timeduration'] = intval($bigbluebuttonbn->timeduration / 60);
  if( $bbbsession['timeduration'] > 0 )
    $bbbsession['welcome'] .= '<br><br>'.str_replace("%duration%", ''.$bbbsession['timeduration'], get_string('bbbdurationwarning', 'bigbluebuttonbn'));

  //Additional info related to the course
  $bbbsession['coursename'] = $course->fullname;
  $bbbsession['courseid'] = $course->id;
  $bbbsession['cm'] = $cm;

  //Operation URLs
  $courseData = $DB->get_record('monorail_course_data', array('courseid'=>$course->id));
  $bbbsession['courseURL'] = $CFG->wwwroot.'/a/courses/'.$courseData->code.'/liveevent';
  $bbbsession['logoutURL'] = $CFG->wwwroot.'/theme/monorail/ext/ajax_live_logout.php';
  //$bbbsession['courseURL'] = $CFG->wwwroot.'/course/view.php?id='.$bigbluebuttonbn->course;
  //$bbbsession['logoutURL'] = $CFG->wwwroot.'/mod/bigbluebuttonbn/view_end.php?id='.$id;

  //Metadata
  $bbbsession['origin'] = "Moodle";
  $bbbsession['originVersion'] = $CFG->release;
  $parsedUrl = parse_url($CFG->wwwroot);
  $bbbsession['originServerName'] = $parsedUrl['host'];
  $bbbsession['originServerUrl'] = $CFG->wwwroot;
  $bbbsession['originServerCommonName'] = '';
  $bbbsession['originTag'] = 'moodle-mod_bigbluebuttonbn ('.$module_version.')';
  $bbbsession['context'] = $course->fullname;
  $bbbsession['contextActivity'] = $bigbluebuttonbn->name;
  $bbbsession['contextActivityDescription'] = $bigbluebuttonbn->description;

  //// BigBlueButton Setup Ends

  $bbbsession['bigbluebuttonbnid'] = $bigbluebuttonbn->id;
  /// find out current groups mode
  if (groups_get_activity_groupmode($cm) == 0) {  //No groups mode
    $bbbsession['meetingid'] = $bigbluebuttonbn->meetingid.'-'.$bbbsession['courseid'].'-'.$bbbsession['bigbluebuttonbnid'];
  } else {                                        // Separate groups mode
    //If doesnt have group
    $bbbsession['group'] = (!$group)?groups_get_activity_group($cm): $group;
    $bbbsession['meetingid'] = $bigbluebuttonbn->meetingid.'-'.$bbbsession['courseid'].'-'.$bbbsession['bigbluebuttonbnid'].'['.$bbbsession['group'].']';
  }

  if( $bbbsession['flag']['administrator'] || $bbbsession['flag']['moderator'] || $bbbsession['flag']['allmoderators'] )
    $bbbsession['joinURL'] = bigbluebuttonbn_getJoinURL($bbbsession['meetingid'], $bbbsession['username'], $bbbsession['modPW'], $bbbsession['salt'], $bbbsession['url'], $bbbsession['userID']);
else
    $bbbsession['joinURL'] = bigbluebuttonbn_getJoinURL($bbbsession['meetingid'], $bbbsession['username'], $bbbsession['viewerPW'], $bbbsession['salt'], $bbbsession['url'], $bbbsession['userID']);


  $joining = false;
  $bigbluebuttonbn_view = '';
  if (!$bigbluebuttonbn->timeavailable ) {
    if (!$bigbluebuttonbn->timedue || time() <= $bigbluebuttonbn->timedue){
        //GO JOINING
        $bigbluebuttonbn_view = 'join';
        $joining = bigbluebuttonbn_view_joining( $bbbsession, $context, $bigbluebuttonbn );
    } else {
        return get_string('view_error_unable_join_student', 'bigbluebuttonbn');
    }
  } else if ( time() < $bigbluebuttonbn->timeavailable ){
    //CALLING BEFORE
    $bigbluebuttonbn_view = 'join';
    $joining = bigbluebuttonbn_view_joining( $bbbsession, $context, $bigbluebuttonbn );
  } else if (!$bigbluebuttonbn->timedue || time() <= $bigbluebuttonbn->timedue ) {
    //GO JOINING
    $bigbluebuttonbn_view = 'join';
    $joining = bigbluebuttonbn_view_joining( $bbbsession, $context, $bigbluebuttonbn );
  } else {
    //CALLING AFTER
    return array('error' => get_string('view_error_unable_join_student', 'bigbluebuttonbn'));
  }
  if($joining) {
      return array('url' => $bbbsession['joinURL']);
  } else {
      return array('error' => get_string('view_error_unable_join_student', 'bigbluebuttonbn'));
  }
}

?>
