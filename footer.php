<?php

header("Access-Control-Allow-Origin: https://blog.eliademy.com");

require_once __DIR__ . "/inc/config.php";

$lang = preg_replace("/[^a-z_]/", "",@$_GET["lang"]);

if (!$lang) {
    $lang = preg_replace("/[^a-z_]/", "", substr(@$_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
}

if ($lang == "en") {
    $lang = "default";
}

require_once __DIR__ . "/inc/i18n/default.php";

$default_str = $str;

if ($lang != "default") {
    if (array_key_exists($lang, $LANGS) && file_exists(__DIR__ . "/inc/i18n/" . $lang . ".php")) {
        require_once __DIR__ . "/inc/i18n/" . $lang . ".php";
    } else {
        $lang = "default";
    }
}

function get_string($word)
{
    global $str, $default_str;

    if (isset($str[$word]))
    {
        return $str[$word];
    }
    else
    {
        return $default_str[$word];
    }
}
?>
<div style="text-align: center; background-color: #F5F5F5; border-top: 1px solid #EEE; padding: 20px 0px;" dir="<?php if ($lang == "ar") { echo "rtl"; } else { echo "ltr"; } ?>">
 <div style="margin-bottom: 10px;">
	<img src="<?php echo $CFG->landing_page; ?>/img/pace_logo.png" width="100">
 </div>
 <div style="margin-bottom: 10px;">
	<a href="http://www.facebook.com/eliademy" target="_blank"><img src="https://eliademy.com/img/logo-facebook.png" alt="Facebook" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
	<a href="https://www.twitter.com/eliademy" target="_blank"><img src="https://eliademy.com/img/logo-twitter.png" alt="Twitter" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
    <a href="http://www.linkedin.com/company/eliademy" target="_blank"><img src="https://eliademy.com/img/logo-linkedin.png" alt="LinkedIn" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
	<a href="https://plus.google.com/u/0/105453567066036560669/posts" target="_blank"><img src="https://eliademy.com/img/logo-gplus.png" alt="Google+" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
	<a href="http://pinterest.com/eliademy/" target="_blank"><img src="https://eliademy.com/img/logo-pinterest.png" alt="Pinterest" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
	<a href="https://www.youtube.com/c/Eliademy" target="_blank"><img src="https://eliademy.com/img/logo-youtube.png" alt="YouTube" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
    <a href="https://eliademy.com/blog" target="_blank"><img src="https://eliademy.com/img/logo-blog.png" alt="Blog" style="border-radius: 10px; width: 20px; height: 20px;"/></a>
 </div>
 <div style="font-weight: bold; color: #9A9A9A; margin-bottom: 10px;">
	<a href="https://eliademy.com/about" style="color: #9A9A9A;"><?php echo get_string("title_about_us"); ?></a> &bull;
	<a href="https://eliademy.com/opensource" style="color: #9A9A9A;"><?php echo get_string("title_open_source"); ?></a> &bull;
	<a href="https://eliademy.com/terms" style="color: #9A9A9A;"><?php echo get_string("title_terms_of_use"); ?></a> &bull;
	<a href="https://eliademy.com/privacy" style="color: #9A9A9A;"><?php echo get_string("title_privacy_policy"); ?></a> &bull;
<!-- 	<a href="https://eliademy.com/love" style="color: #9A9A9A;"><?php echo get_string("title_love"); ?></a> -->
    <a href="http://helpdesk.eliademy.com/knowledgebase" style="color: #9A9A9A;"><?php echo get_string("title_support"); ?></a>
 </div>
 <div style="color: #9A9A9A;">
	<a href="http://cbtec.fi">© CBTec 2016 | <?php echo get_string("text_madeinfinland"); ?></a> | <a href="https://eliademy.com/en/premium" target="_blank"><?php echo get_string("footer_powered_by"); ?></a>
 </div>
</div>
