<?php
    header("Content-Type: application/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    require_once __DIR__ . "/inc/config.php";

    try
    {
        $db = new PDO("mysql:dbname=" . DB_NAME . ";host=" . DB_HOST, DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

        $q = $db->prepare("SELECT mci.public_page_path FROM " . DB_PREFIX . "monorail_cohort_info AS mci WHERE mci.public_page=1");

        if ($q->execute())
        {
            $paths = $q->fetchAll(PDO::FETCH_COLUMN, 0);
        }
    }
    catch (Exception $ex)
    {
        error_log(var_export($ex, true));
        $paths = array();
    }

    $lastModified = date("c", strtotime("first day of this month midnight"));
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach ($paths as $path) { ?>
   <url>
      <loc>https://eliademy.com/<?php echo $path ?></loc>
      <lastmod><?php echo $lastModified ?></lastmod>
      <changefreq>monthly</changefreq>
      <priority>1.0</priority>
   </url>
<?php } ?>
</urlset>
