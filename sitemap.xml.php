<?php
    header("Content-Type: application/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    $lastModified = date("c", filemtime(__DIR__ . "/inc/i18n/default.php"));
    $lastFriday = date("c", strtotime("last friday"));
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <url>
      <loc>https://eliademy.com/</loc>
      <lastmod><?php echo $lastModified ?></lastmod>
      <changefreq>monthly</changefreq>
      <priority>1.0</priority>
   </url>
	<url>
		<loc>https://eliademy.com/courses/</loc>
        <lastmod><?php echo $lastFriday ?></lastmod>
		<changefreq>weekly</changefreq>
		<priority>1.0</priority>
	</url>
	<url>
		<loc>https://eliademy.com/blog/</loc>
		<lastmod><?php echo $lastFriday ?></lastmod>
		<changefreq>weekly</changefreq>
		<priority>0.9</priority>
	</url>
	<url>
		<loc>https://eliademy.com/login</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/signup</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/business</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/mobile</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/lti</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/about</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/terms</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.5</priority>
	</url>
	<url>
		<loc>https://eliademy.com/privacy</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.5</priority>
	</url>
	<url>
		<loc>https://eliademy.com/opensource</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.5</priority>
	</url>
	<url>
		<loc>https://eliademy.com/love</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.5</priority>
	</url>
<?php foreach (scandir(__DIR__ . "/inc/i18n") as $file)
        if ($file != "default.php" && substr($file, 0, 1) != ".")
{
    $langCode = substr($file, 0, -4);
?>
    <url>
        <loc>https://eliademy.com/<?php echo $langCode; ?>/</loc>
        <lastmod><?php echo date("c", filemtime(__DIR__ . "/inc/i18n/" . $file)) ?></lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>

	<url>
		<loc>https://eliademy.com/<?php echo $langCode ?>/login</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/<?php echo $langCode; ?>/signup</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/<?php echo $langCode; ?>/business</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/<?php echo $langCode; ?>/mobile</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/<?php echo $langCode; ?>/lti</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
	<url>
		<loc>https://eliademy.com/<?php echo $langCode; ?>/about</loc>
        <lastmod><?php echo $lastModified ?></lastmod>
		<changefreq>yearly</changefreq>
		<priority>0.8</priority>
	</url>
<?php } ?>
</urlset>
