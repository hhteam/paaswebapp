<?php $str = array (
  'button_cancel' => 'Avbryt',
  'button_close' => 'Lukk',
  'button_continue' => 'Fortsett',
  'button_create_account' => 'Opprett en konto',
  'button_enroll' => 'Meld inn',
  'button_get_started' => 'Kom i gang',
  'button_learn_more' => 'Lær mer',
  'button_send' => 'Send',
  'button_signin_with_google' => 'poprzez Google',
  'button_signin_with_live' => 'poprzez Live',
  'button_submit' => 'Send inn',
  'dlg_forgotten_password' => 'Glemt passordet?',
  'dlg_log_in' => 'Logg inn',
  'feature_free_body_unlimcourses' => 'Det er ingen begrensninger på hvor mang kurs du kan opprette eller antall filer du kan laste opp.',
  'feature_pro_header_gaps' => 'Integrasjon med Google Apps',
  'label_country' => 'Køntri',
  'label_customers' => 'Kunder',
  'or' => 'Lub',
  'placeholder_email' => 'E-post-adresse',
  'placeholder_firstname' => 'Fornavn',
  'placeholder_lastname' => 'Etternavn',
  'placeholder_organization' => 'Organisasjon',
  'placeholder_password' => 'Passord',
  'placeholder_username' => 'Brukernavn',
  'pro_feature7' => 'Ingen kredittkort kreves for 30-dagers prøveversjon',
  'service_name' => 'Eliademy',
  'teacher' => 'Instruktor',
  'testimonial_badanov' => 'Alexander Badanov (Volga State University of Technology, Rosja)',
  'testimonial_inge' => 'Inge de Waard (Institute of Tropical Medicine, Belgia)',
  'testimonial_kamp' => 'Johannes Kamp (AOC Oost, Holandia)',
  'testimonial_pavel' => 'Pavel Janicek (Scola Humanitas Litvínov, Republika Czeska)',
  'text_business_part2' => 'Juan J. Arroyo (Founder and CEO, Apisec)',
  'text_business_part3' => 'Franco Farnedi (CEO, Farnedi ICT)',
  'text_business_part4' => 'George Lew (Marketing Director, Innochannel)',
  'text_featured_id' => 'Utvalgte',
  'title_blog' => 'Blog',
  'title_lti' => 'LTI',
  'title_privacy_policy' => 'Retningslinjer angående personvern',
  'title_support' => 'Støtte',
  'title_terms_of_use' => 'Vilkår for Bruk',
);