<?php
/**
 * mrdi.com
 *
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */


$str = array(
    "service_name" => "mrdi",
    "and" => "and",
    "or" => "or",
    "teacher" => "Instructor",
    "label_instructors" => "Instructors",
    "label_courses" => "Courses",
    "label_customers" => "Customers",
    "label_pricing" => "Pricing",
    "label_all" => "All",

    "placeholder_signup_form_email" => "Enter your email",

    //"text_private_course" => "This course is available by invitations only, please ask your teacher to invite you by email.",
    //"text_enrollment_closed" => "Enrolment to this course is closed.",

	/* Buttons  */
    "button_start_today" => "Start today",
    "button_signup_now" => "Sign up now",
    "button_submit" => "Submit",
    /* disable with buttons after circular login buttons are out  */
    "button_signin_with_fb" => "with Facebook",
    "button_signin_with_lin" => "with LinkedIn",
    "button_signin_with_google" => "with Google",
    "button_signin_with_live" => "with Live",
    "button_create_account" => "Create an account",
    "button_get_started" => "Get Started",
    "button_continue" => "Continue",
    "button_go_to_courses" => "Go to courses",
    "button_enroll" => "Enrol",
    //"button_request_info" => "Contact a Sales Rep",
    "button_send" => "Send",
    "button_cancel" => "Cancel",
    // "button_help_translate" => "Help us translate",
    "button_browse_courses" => "Browse all courses",
    "button_learn_more" => "Learn more",
    "button_read_more" => "Read more",
    "button_close" => "Close",

    //"title_home" => "Home",
    "title_terms_of_use" => "Terms of Use",
    "title_privacy_policy" => "Privacy Policy",
    "title_support" => "Support",
    "title_about_us" => "About Us",
    "title_lti" => "LTI",
    "title_blog" => "Blog",
    "title_premium" => "Premium",
    "label_features" => "Features",
    "title_testimonials" => "Testimonials",
    "title_certificate" => "Certificate of Completion",
    //"title_mobile" => "Mobile",
    //"title_business" => "Business",
    "title_open_source" => "Open Source",
	"footer_powered_by" => "Powered by Eliademy",
    "title_signup_business" => "Sign Up A New Organization",
    //"title_catalog" => "Catalog",
    //"title_my_courses" => "My Courses",
    //"title_calendar" => "Calendar",
    "title_love" => "Love",
    "title_faq" => "FAQ",

    /* Page titles, descriptions */
    //"home_title" => "mrdi | Create your own online course",
    //"login_title" => "mrdi | Login",
    //"lti_title" => "mrdi | LTI",
    //"mobile_title" => "mrdi | Mobile",
    //"opensource_title" => "mrdi | Open Source",
    //"privacy_title" => "mrdi | Privacy Policy",
    //"terms_title" => "mrdi | Terms Of Use",
    //"signup_title" => "mrdi | Sign Up",
    //"business_title" => "mrdi | Premium",
    //"about_title" => "mrdi | About Us",
    //"love_title" => "mrdi | Love",
    //"certificate_title" => "mrdi | Certificate of Completion",
    //"features_title" => "mrdi | Features",
    //"testimonials_title" => "mrdi | Testimonials",

    "home_description" => "mrdi is a free e-learning platform (LMS) that allows educators and students to create, share and manage online courses. Course content belongs to educators.",
    "business_description" => "mrdi Premium is an e-learning platform (LMS) for you to train your employees, customers or students. It includes all free features plus special functionality for team learning management and sharing confidential courses.",

	 /* Sign up and sign in dialogs  */
    "dlg_log_in" => "Log in",
    "dlg_login_with_email" => "Or with your email",
    "dlg_password_incorrect" => "Username and/or password incorrect.",
    "dlg_forgot_password" => "Forgot your password?",
    "dlg_dont_have_account" => "Don't have an account?",
	"dlg_contact_manager" => "Please contact your manager.",
    "dlg_sign_up" => "Sign up",
    "dlg_have_account" => "Already have an account?",
    "dlg_accept_terms" => "By signing up you agree to <a href=\"" . CFG_ROOT_PATH . "terms\">Terms Of Use</a> and <a href=\"" . CFG_ROOT_PATH . "privacy\">Privacy Policy</a>.",
    "dlg_forgotten_password" => "Forgotten password",
    "dlg_to_reset_password" => "To reset your password, submit your username or your email address below. If we can find you in the database, an email will be sent to your email address, with instructions how to get access again.",
    "dlg_password_reset" => "If you supplied a correct username or email address then an email should have been sent to you.<br><br>It contains easy instructions to confirm and complete this password change. If you continue to have difficulty, please contact the site administrator.",
    //"dlg_request_info" => "Please fill the contact form below and our specialists will get across to you on pricing and how to get started",
    //"text_business_signup_free" => "You are signing up for an unlimited trial for up to 5 users.",

    "dlg_social_signin_error" => "User with this email already exists. Please choose another email or login with your existing account. If you already signed up with a social account, please use social login button instead.",
    "dlg_login_unsupported_browser" => "Your browser is not supported, please upgrade to the most recent version.",
    //"dlg_login_unsupported_browser_learn_more" => "Learn more.",

	/* Sign up and sign in placeholders  */
    "placeholder_email" => "Email address",
    "placeholder_password" => "Password",
    "placeholder_firstname" => "First name",
    "placeholder_lastname" => "Last name",
    "placeholder_username" => "User name",
    "placeholder_name" => "Name",
    "placeholder_organization" => "Organization",

    "error_wrong_auth" => "You are signed up to mrdi with your social account. To log in, please click on social log in button.",
    "error_user_unknown" => "Cannot find user with the given username.",
    "error_login_retry" => "Your login attempt has failed 10 times, due to security reasons please relaunch your browser and try again",
    "text_footer_slogan" => "Democratizing education with technology",
    "text_follow_us" => "Follow us on",
    "text_featured_id" => "Featured in",
    "text_madeinfinland" => "Made in Finland",
    /* "text_address" => "Hiilikatu 3, Helsinki, 00180",*/

    /* Home page. */
    "text_home_header" => "Create your own online course",
    "text_home_subheader" => "Simple, secure & free forever",

    "text_our_partners" => "Our partners",
    "text_our_address" => "Our Address",
    "text_front_featured_courses" => "Featured courses",

    //"text_front1" => "Simplicity, easy to use",
    //"text_subfront1" => "The simple and attractive interface of mrdi engages the 21st century teacher and student by providing them with new ways of creating courses, viewing course materials and receiving notifications.",
    //"text_front2" => "Available anywhere anytime",
    //"text_subfront2" => "In 3 steps, own your online classroom by creating a course, providing relevant information and inviting students. Also receive updates and manage courses all from one place.",
    //"text_front3" => "Set up a class outside of school. Corporate trainings, classes outside of campus",
    //"text_subfront3" => "Save time and energy through only one course editing button. Sync calendar, notes and access content on the web, on mobile phone and on your tablet.",

    "text_feature1" => "Beautiful course in 3 steps",
    "text_subfeature1" => "Instantly create and edit online courses, forums and quizzes, share documents and embed any multimedia content straight into your course. With our state of the art visual editor your course will look like an expensive study book with just a few clicks.",

    "text_feature2" => "Available anytime, anywhere",
    "text_subfeature2" => "Our truly mobile service allows access from any modern online device - Mac, PC, tablet or smartphone. You can create an entire course on your tablet, preview any course file (including MS Office) in a browser, and complete a quiz on your smartphone.",

    "text_feature3" => "Stay on top of your learning",
    "text_subfeature3" => "mrdi offers an educational calendar where each student will find all assigned courses, quizzes and deadlines. And to ensure that your class will never miss an important update, the platform offers a news feed and e-mail notification system that everybody can personalize according to their own learning schedule.",

    "text_feature4" => "Own your LMS",
    "text_subfeature4" => "mrdi is built for teachers. If you are limited by your IT department when you are giving visiting lectures or corporate training and need a simple and easy way to share course content with your students - try mrdi. Just upload a list of emails and all your students will join you in an LMS controlled by you.",

    /* testimonilas */
    "text_what_people_say" => "What people say about mrdi",
    "text_over_15K_teachers" => "Over 15.000 teachers already trust mrdi",

    "testimonial_text_badanov" => "mrdi is very simple to use for both teachers and students. Creating course is very easy and editing on the fly saves precious time for Teachers.",
    "testimonial_badanov" => "Alexander Badanov (Volga State University of Technology, Russia)",

    "testimonial_text_inge" => "Really easy, simple, ubiquitous course platform that can be enriched with content in just a few moments. Nice.",
    "testimonial_inge" => "Inge de Waard (Institute of Tropical Medicine, Belgium)",

    "testimonial_text_pavel" => "mrdi is amazing and totally wowed me! So simple compared to Moodle.",
    "testimonial_pavel" => "Pavel Janicek (Scola Humanitas Litvínov, Czech Republic)",

    "testimonial_text_mevlut" => "I believe students should be focusing on the topic which they are learning rather than dealing with technical issues of most LMS's. My foresight is that the future of e-learning lies in mrdi.",
    "testimonial_mevlut" => "Mevlüt Develi (Ankara University, Turkey)",
    
    "testimonial_text_mccall" => "mrdi is super easy to use, has wonderful design aspects unexpected of an LMS, and is social and free. What a relief to find an LMS that meets my needs as a teacher. And of course students. Simple, elegant and functional.",
    "testimonial_mccall" => "Robert McCall (Online English Systems Ltd, Brazil)",

    "testimonial_text_francis" => "Complex and expensive education systems are something of the past with mrdi. The ultimate disruptive education solution.",
    "testimonial_francis" => "Francis Ortiz (Francisortiz, Spain)",

    "testimonial_text_kamp" => "With mrdi I see more, a fresh look. Keep your openness. Your angle is right. Online learning tool should be simple to use.",
    "testimonial_kamp" => "Johannes Kamp (AOC Oost, Netherlands)",

    "testimonial_text_ketil" => "mrdi is a perfect tool for sharing documents and lessons with students and keeping track of their assignments. The style and the ease of use makes up for few missing features of classic LMS.",
    "testimonial_ketil" => "Ketil Thorgersen (Stockholm Musikpedagogiska Institut, Sweden)",

    "testimonial_text_jules" => "Because mrdi is the bomb when it comes to appearance, customer support, ease of use and function. I feel like we landed on a hidden gold mine.",
    "testimonial_jules" => "Jules Gourley (Full Circle Maternity, USA)",

    "testimonial_text_priscilla" => "I've been using mrdi since July 2014 and I love this platform. Very easy to create courses and track the enrollments and completions. Prompt support provided by the team when I needed it.",
    "testimonial_priscilla" => "Priscilla Almeida (Appnomic, USA)",

    "testimonial_text_tiina" => "mrdi is so easy to use and my students have been happy with this platform.",
    "testimonial_tiina" => "Tiina Huhtala (KSL, Finland)",

    "testimonial_text_susanna" => "I have been mrdi now for three years in my online course. I like especially the visual layout of the course structure. I have got good feedback of that from students as well. The program is also easy to use both from teachers and students point of view. Videos and links are directly visible and it is easy to see the course structure.",
    "testimonial_susanna" => "Susanna Fabricius (Arcada University, Finland)",

    "testimonial_text_harris" => "I am a middle school teacher in Arizona. I teach 7th and 8th grade. I have been looking for an online tool like mrdi for a long time. Nothing ever seemed to do just what I wanted. mrdi does just what I want. I share links and videos, we share discussion questions, and it helps me to take my kids out of the classroom where real learning happens. My students love the site because it's easy to navigate and they can access it from their homes or cell phones. I have recommended it to all of my colleagues.",
    "testimonial_harris" => "John Harris (Esterlla Middle School, USA)",

    "testimonial_text_belskaya" => "A very handy tool for a busy teacher! A tremendous help to keep students progress on the course.",
    "testimonial_belskaya" => "Tatyana Belskaya (RANEPA, Russia)",

    "testimonial_text_hoffman" => "I love the interface, it is very user friendly, the fact that it is free is amazing! I am very grateful for mrdi and would highly recommend it to friends and colleagues.",
    "testimonial_hoffman" => "Tuesday Hoffman (USA)",

    "testimonial_text_radomske" => "User-friendly. Simple. Flexible. I'm not a programmer, I'm an instructor. Thank you for letting me do my job by doing so well at yours!",
    "testimonial_radomske" => "Kim Radomske (Canada)",

    "testimonial_text_piccinini" => "mrdi helps to work collaboratively wherever we are, whenever we can. This is a great platform. Not only can we do tasks together, but we can also participate in discussions what is a way we feel more united as a group. Besides, as teachers, we can receive tasks submissions and grade them easily. There is also an option upload other course materials from different resources - online presentation, videos, files and even take part in webinars. Thank you to all the staff that makes mrdi available.",
    "testimonial_piccinini" => "Maria Isabel Piccinini (Centro de Capacitación, Argentina)",

    "testimonial_text_aikathiele" => "mrdi is the best, most user-friendly LMS that I've used so far. Great user interface. Clean dashboard. Would highly recommend",
    "testimonial_aikathiele" => "Aika Thiele (George Mason University, US)",

    "testimonial_text_syedhashmi" => "It is not that the moment I saw mrdi, I fell in love with it, no, no, no, absolutely not. My deep respect and sincere love for mrdi are carefully assessed based on an accurate evaluation through continuous research on the internet plus my constant interaction with mrdi team over a long period of 4 years.",
    "testimonial_syedhashmi" => "Syed Hashmi (Illustrated Math Series, US)",
    "testimonial_text_jorgebarrero" => "Only in Finland, this could happen. It gave us Linux, now this great concept. Open Operating Systems can be compared to Open Educational Platforms, not like Coursera or EdX, but one really open to any teacher who wants to share knowledge. In an easy and simple way.",
    "testimonial_jorgebarrero" => "Jorge Barrero (Formación de Expertos Digitales, Venezuela)",

    "text_for_organizations0" => "Looking for an LMS to teach confidential courses?",
    "text_for_organizations1" => "With mrdi Premium, you can train your team, customers and partners.",

    //"text_about_us0" => "Our mission",
    //"text_about_us1" => "Democratizing education with technology",

    "text_support0" => "Built together with the community",
    "text_support1" => "We make sure every customer request, suggestion or idea counts.",
    "button_support" => "Visit our community portal and vote",

    "text_follow0" => "Stay up to date",
    "text_follow1" => "A new feature is released every week",
    "button_blog" => "Learn more in our blog",

    "text_interested" => "Interested? Start your first course right now.",
    "text_interested_pro" => "Interested? Learn more about mrdi Premium.",
    //"text_interested_testimonials" => "Interested? Read more what users say about mrdi.",

    /* About us */
    "text_aboutus_para1" => "Our mission is to democratize education with technology. This mission comes from our belief that education should be available to all and everywhere. Going back to history, the great philosopher Plato started the first Academy in the world in an olive tree grove, also known as <i>Elia</i> in Greek. It was a remarkable event which laid the foundation for human education. However, Plato's Academy was an exclusive academy only free to the people of Athens. Applying the same philosophy and taking it further, we think education should be available to everyone all over the world, thus we created mrdi.",
    "text_aboutus_para2" => "mrdi (əlɪaˈdəmi) is an e-learning platform that allows educators and students to create, share and manage online courses.",
    "text_aboutus_para21" => "It is a free alternative to Moodle, Blackboard and other commercial learning management systems.",
    "text_aboutus_para3" => "mrdi is developed by CBTec Oy. mrdi is built using Open Source and in cooperation with educators and students from all over the world.",
    "text_aboutus_para4" => "You can visit us at Hiilikatu 3 in Helsinki or reach us by email (support@mrdi.com).",

    /* Mobile */
    //"text_mobile_header" => "Heading out? Take the whole classroom with you.",
    //"text_mobile_subheader" => "The mrdi mobile application is an extension of mrdi, a free online classroom for instructors to create, share and manage educational content with students.",
    //"text_mobile_features" => "Key features:",
    //"text_mobile_features_1" => "Access course content anytime, anywhere",
    //"text_mobile_features_2" => "Receive notifications about activity of teachers and other students",
    //"text_mobile_features_3" => "Catch up with upcoming, due and completed tasks and material",
    //"text_mobile_features_4" => "Collaborate with co-students and instructors through an interactive forum",
    //"text_mobile_features_5" => "See what you should do right now or later for your course activities",
    //"text_mobile_features_footer" => "mrdi is also compatible with Moodle LMS. Existing Moodle users can purchase the compatibility update which will allow sign in to the LMS just by providing their university name and credentials. Also, the application can be used for free if the user knows the Moodle site URL.",

    /* Business */
    "text_business_title" => "Organize and share knowledge of your team",
    "text_business_subtitle" => "Elegant cloud solution to train your employees, customers or students",

    "text_business_trial_signup2" => "Try mrdi Premium for free",

    "text_business_feature1" => "Confidential courses",
    "text_business_subfeature1" => "Each instructor with a premium subscription can create confidential courses. Such courses are not visible on the Internet and available exclusively to users you have invited by email. It is a great tool to share know how confidentially with your partners, customers or students.",

    //"tutorial_video_premium" => "Watch a quick video about private learning space",

    "text_business_feature2" => "Unlimited webinars",
    "text_business_subfeature2" => "You can train your team or customers through a webinar with instant messaging, desktop and presentation sharing. You can also annotate any presented document in real time. You can choose to make webinars, conferences for up to 50 participants or private on-to-one tutoring.",

    "text_business_feature3" => "Advanced user management",
    "text_business_subfeature3" => "With our premium subscription, you can allocate roles to your team members (administrator, observer or org. instructor), assign students to groups and enroll students to any course taught in your organization without the need of sending a course invitation by email.",

    "text_business_features_header" => "Make stunning courses in minutes",
    "text_business_features_subheader" => "mrdi is really simple to use. It allows instructors to create and share online courses with online multimedia, private videos, LTI edu apps, discussions, tasks, quizzes and customizable completion certificates. It is packed with user-friendly features and you will fall in love with it instantly.",
    "text_business_features_subheader_link" => "Learn more about all features",

    "text_business_start_now" => "Start your learning portal right now",
    "text_business_start_now_subtext" => "Free 30-day unlimited trial. No installation or credit card required. If you are using Google Apps for Business, try from {Marketplace} instead.",
    "text_business_start_now_subtext_no_apps" => "Free 30-day unlimited trial. No installation or credit card required.",

    "text_business_testimonials" => "Trusted by 600+ Smart Companies",
    "text_business_testimonials_subheader" => "Many organizations are already using mrdi, see what they say:",

    //"text_business_quote1" => "mrdi is a great addition to our high quality services by enabling us to provide our customers holistic solutions. mrdi meets the criteria of a an ideal service which we're glad to provide.",
    //"text_business_part1" => "Mika Lausamo (Manager of Service, HS-Works Oy)",

    //APISEC quote is disabled for now
    "text_business_quote2" => "mrdi provides practical solutions and can help us in this crucial time for the development of Mexico.",
    "text_business_part2" => "Juan J. Arroyo (Founder and CEO, Apisec)",

    "text_business_quote3" => "mrdi will revolutionize Italian world of business and private education; its easy and user-friendly technology paves the way to the implementation of a continuing education model.",
    "text_business_part3" => "Franco Farnedi (CEO, Farnedi ICT)",

    "text_business_quote4" => "Corporate training is the key to keep business competitive and mrdi is a very user-friendly and affordable learning platform that can enable this.",
    "text_business_part4" => "George Lew (Marketing Director, Innochannel)",

    //"text_business_quote5" => "mrdi helps many institutions and corporates impart training to their students and employees with just a click of mouse.",
    //"text_business_part5" => "Arvind Kanaujia (Director, Alpinesoft IT)",

    //"text_business_quote6" => "There is a huge demand for simple to use and easy to apply tools in internal and external education processes within companies and mrdi targets just that need.",
    //"text_business_part6" => "Sami Haahtinen (CEO, Bad Wolf Oy)",

    "text_business_quote7" => "For our Finnish language courses for large companies we experience a trend towards Blended Learning. mrdi's solution supplements perfectly our classroom courses.",
    "text_business_part7" => "Katja Jääskeläinen (ScanLang GmbH, Austria)",

    "text_business_plans_pricing" => "Pricing made simple",
    //"text_business_plans_pricing_subheader" => "All plans include:",
    "text_business_plans_custom" => "Need enterprise integration or different payment method? {Contact our sales team} to create a custom offer for you.",

    "text_business_faq" => "Questions",
    "text_business_faq_q1" => "What is mrdi premium?",
    "text_business_faq_a1" => "mrdi Premium is a solution for you to train your employees, customers or students. It includes all free features plus special functionality for team learning management and sharing confidential courses.",

    "text_business_faq_q2" => "How does mrdi Premium pricing work?",
    "text_business_faq_a2" => "mrdi Premium costs 5&euro;/month. If you teach confidential courses, we will charge additional 1&euro;/month for each unique participant on your confidential courses. We don't charge anything for participants on open, paid or invitation only courses.",

    "text_business_faq_q3" => "How monthly bill is calculated?",
    "text_business_faq_a3" => "We bill only for the actual days students and instructors had access to your confidential courses. For example - if 10 students were enrolled in 5 confidential courses, we will bill only for 10 students. Likewise, if students were enrolled for 2 weeks - only half of monthly fee will be billed. Billing happens each calendar month on the same date you started using mrdi Premium.",

    //"text_business_faq_q3" => "Can I change my plan at a later time?",
    //"text_business_faq_a3" => "Yes, you can upgrade or downgrade your plan at any time. You can also cancel any paid plan and use only free functionality - there are no lock-ins with mrdi.",

    "text_business_faq_q4" => "Can I charge for course enrollments?",
    "text_business_faq_a4" => "It's totally free to sell course via mrdi. We take care of processing payments from major credit cards and PayPal, and you earn 90% of net sales. You will be paid on the 1st working day of the month following sales month. Students who purchased your course online are not counted towards premium billing.",

    "text_business_faq_q5" => "Does your price include sales tax?",
    "text_business_faq_a5" => "Price is VAT exclusive. VAT will be added to EU customers based on the Member State VAT rate where the customer is located unless a valid VAT number is provided. Customers from outside of EU don't have to pay VAT.",

    "text_business_faq_q6" => "What forms of payment do you accept?",
    "text_business_faq_a6" => "You can purchase mrdi with any major credit card. For annual subscriptions over 50 members, we can issue an invoice payable by bank transfer. Please contact us to arrange an invoice purchase. Monthly purchases must be paid for by credit card.",

    "text_business_faq_q7" => "Do you offer academic pricing?",
    "text_business_faq_a7" => "Nonprofits and public academic institutions may receive annual Premium subscription for 50&euro;.",
    "text_business_faq_a7_more" => "Please start 30-day trial first and apply for an academic license.",

    "text_business_faq_q8" => "Can I switch to a free version after trial ends",
    "text_business_faq_a8" => "Yes. If you don't want to continue with mrdi Premium, your account will be automatically switched to a free plan. You will lose access to webinars, admin interface and will not be able to create confidential courses.",

    "text_business_faq_q9" => "Is there an on-premises version of mrdi?",
    "text_business_faq_a9" => "We don't plan to offer an internally hosted version of mrdi. We are committed to providing a robust and secure service from the cloud. If you are looking for an integration with your user directory, please contact us.",

    "text_business_faq_q_last" => "Have more questions?",
    "text_business_faq_a_last" => "Contact us and we will do our best to help.",

    "text_business_form_company_name" => "Enter your company name",
    "text_business_form_start_free" => "Start 30-day trial",

    //"text_pricing_vat" => "Price is VAT exclusive. VAT will be added to EU customers based on the Member State VAT rate where the customer is located unless a valid VAT number is provided.",
    //"text_pricing_free" => "Free",
    "text_pricing_per_mo" => "mo",
    "text_pricing_per_month" => "month",
    //"text_pricing_per_year" => "/year",
    //"text_pricing_users" => "USERS",

    //"text_licences" => "Licences",
    //"text_cost_year_licence" => "Per user per year",
    //"text_cost_month_licence" => "Per user per month",
    "text_cost_month_per_user" => "montly per unique participant on confidential courses",
    //"text_cost_month_per_admin" => "+ 5€ monthly for the account owner",

    //"text_person_see_achive" => "See what you can achieve with mrdi today!",

    /* this block is not used anymore, need to delete later */
    //"text_no_account_limits" => "No document upload limit",
    //"text_no_hidden_fees" => "No hidden fees",
    //"text_no_charge_support" => "No charge for email support",
    //"text_no_credit_card" => "No credit card required for 5 user trial",

    /* features next to pricing */
    "pro_feature0" => "No advertisements",
    "pro_feature1" => "Confidential courses",
    "pro_feature4" => "Unlimited webinars",
    "pro_feature5" => "Admin controls and analytics",
    "pro_feature2" => "Customizable completion certificates",
    "pro_feature3" => "128-bit SSL security",
    "pro_feature6" => "Priority customer support",
    "pro_feature10" => "10% commission rate for course sales",
    "pro_feature11" => "Landing page for your courses",
    //"pro_feature9" => "Integration with Google Apps",
    "pro_feature7" => "No credit card required for 30-day trial",

    /* Support us page */
    "text_support_hero_title" => "Support mrdi",
    "text_support_hero_subtitle" => "Join us on our BIG mission to democratize education with technology",
    "text_support_hero_action" => "Help Now",
    "text_support_principles_header" => "Our service to over 15000 teachers and 200 NGOs is based on 3 principles:",
    "text_support_principles_1" => "Free to use",
    "text_support_principles_1_description" => "For an individual instructor, mrdi is completely free to use, forever.",
    "text_support_principles_2" => "No tricks",
    "text_support_principles_2_description" => "We do not sell any data about you to third parties or monetize on your content. Ever.",
    "text_support_principles_3" => "Security",
    "text_support_principles_3_description" => "Content always belongs to the instructor and is stored in secure EU data centres.",
    "text_support_principles_paragraph" => "We intend to keep this promise and hope you can contribute to help grow the platform, and keep it open, stable and accessible to all. There are a few ways.",
    "text_support_share_header" => "Spread the word about mrdi",
    "text_support_share_paragraph" => "If you genuinely like our service, even though it is far from perfect, please let your colleagues and friends know about it. An email, a blog entry or a simple share on social media can bring lots of new people to mrdi. Which is vital to develop service further. And who knows, maybe your friends and colleagues will discover just the tool they have been looking for.",
    "text_support_localize_header" => "Become localization volunteer",
    "text_support_localize_paragraph" => "We want to make online learning accessible in all languages of the world. For this, we need your help. One string, sentence or a whole language, any help is much appreciated. Join our translation community who already makes mrdi available in over 36 languages. It really means a lot to our team.",
    "text_support_localize_action" => "Make mrdi Speak Your Language",
    "text_support_donate_header" => "Support by donation",
    "text_support_donate_paragraph" => "As a business residing in Finland, we cannot accept donations. However, by choosing to buy any of those courses you support our work and help us make mrdi better and better.",
    "text_support_donate_footer" => "And if you would like to donate your course as well, publish a story about our mission or share a testimonial, feel free to {get in touch with us} anytime.",
    //"text_support_donate_button" => "Donate your course",
    "text_support_footer" => "Thank you for helping us to democratize education with technology",
    "text_support_footer_regards" => "With best regards from Helsinki",
    "text_support_footer_signature" => "mrdi Team",

    /* Certificate */
    "label_cert_not_exist" => "Certificate of Completion not found",
    "label_premium_cert_available" => "Premium printed certificate is waiting for you",
    "label_premium_cert_available_2" => "Purchase this certificate if you are %NAME",
    "label_order_now" => "Order now",
    "label_download" => "Download",
    "label_share" => "Share",
    "label_recipient" => "Recipient",
    "label_full_profile" => "View full profile",

    /* Org page */
    "label_no_public_courses_yet" => "No courses to showcase yet",
    "label_search_courses" => "Search your courses",
    "label_announcements" => "Latest News",
    "label_ads" => "Advertisements",
    "label_all_courses" => "All courses",
    "label_country" => "Country",
    "label_no_courses_found" => "No courses matching your search criteria",

    /* Features */
    "feature_free_label" => "Available to all instructors",

    "feature_free_header_editor" => "Course editor",
    "feature_free_body_editor" => "Create a course with all the editing tools you need. What you see is what you get. mrdi will render all the text, pictures, videos, audios and interactive elements for you.",
    
    "feature_free_header_lms" => "Your own LMS",
    "feature_free_body_lms" => "mrdi is built for teachers. You don't need to be limited by your IT department. You control who gets access to your courses and how they are delivered.",
    
    "feature_free_header_forum" => "Interactive discussions",
    "feature_free_body_forum" => "Online classroom has never been this social. Now you can organize chats into different themes for better engagement on specific topics. Anyone on your course could directly respond to messages by others.",
    
    "feature_free_header_qizzes" => "Tasks and quizzes",
    "feature_free_body_qizzes" => "You can create tasks and quizzes in your courses and control when they become visible, what should happen after a deadline or how many tries an answer can be submitted.",

    "feature_free_header_grading" => "Grading tools",
    "feature_free_body_grading" => "Understand better how your students are learning using appropriate assignments and quizzes. You can easily see which tasks have been graded, who have completed the tasks and the get instant feedbacks on the class performance.",

    "feature_free_header_multimedia" => "Online multimedia",
    "feature_free_body_multimedia" => "If you have ready have course materials somewhere else, all you need is to paste a link. mrdi support YouTube, Slideshare, Prezi and over 160 other sources seamlessly.",

    "feature_free_header_certificate" => "Course completion certificates",
    "feature_free_body_certificate" => "mrdi gives you the ability to reward your students with a certificate of completion. Your students can get a free online certificate or order a high quality printed copy (sales revenue will be shared with you).",
    
    "feature_free_header_linkedin" => "Integration with LinkedIn",
    "feature_free_body_linkedin" => "All online certificates received on mrdi can be added to a LinkedIn profile, or shared in social media.",
    
    "feature_free_header_marketplace" => "Online course marketplace",
    "feature_free_body_marketplace" => "mrdi is already the home to tens of thousands of courses in over thirty languages. You can teach your courses for free or set an enrollment price. We will share 70% of all revenue with you.",

    "feature_free_header_notifications" => "Robust notifications system",
    "feature_free_body_notifications" => "Receive instant notifications on all important class activities, just like in a social network.",

    "feature_free_header_calendar" => "Personal study calendar",
    "feature_free_body_calendar" => "See all your tasks and deadlines at a glance. You can synchronize your mrdi calendar with iCal, Google and Outlook.",

    //"feature_free_header_office" => "View MS Office documents",
    //"feature_free_body_office" => "Students can preview any course document in Microsoft Office format instantly on any computer. They don’t even need to own Microsoft Office, mrdi cloud conversion will make any document available for free.",

    "feature_free_header_analytics" => "Learner's analytics",
    "feature_free_body_analytics" => "See enrollment history, engagement with course content, task submission progress and grade distribution for any of your courses.",

    "feature_free_header_localization" => "In your language",
    "feature_free_body_localization" => "Our incredible team of volunteers localized mrdi to over 30 languages. Chances are your students will have our user interface in their native language.",

    "feature_free_header_unlimcourses" => "Unlimited courses",
    "feature_free_body_unlimcourses" => "There are no limits on the amount of courses you can create or the number of file attachments you can upload.",

    "feature_pro_label" => "Premium features",

    "feature_pro_header_private" => "Confidential courses",
    "feature_pro_body_private" => "You can create and teach completely confidential courses which will not be visible for anyone except invited students.",

    "feature_pro_header_userdirectory" => "User management",
    "feature_pro_body_userdirectory" => "See all users on all your courses at a glance, assign them to groups and manage access.",
    
    "feature_pro_header_enrollments" => "Course management",
    "feature_pro_body_enrollments" => "Enroll individual students or group to any course taught in your organization without sending invitations.",

    "feature_pro_header_webinar" => "Online tutoring and webinars",
    "feature_pro_body_webinar" => "You can schedule an online tutoring session with live video and desktop sharing. You don't need to invite your students to some other platform, webinars are built into your course.",
    
    "feature_pro_header_videos" => "Private video storage",
    "feature_pro_body_videos" => "All your instructional videos can be uploaded directly to your course. No need to use any 3rd party service and be concerned about access.",

    "feature_pro_header_roles" => "Administration roles",
    "feature_pro_body_roles" => "You can assign roles of an administrator (with full access to all courses), an observer (only view access) or an organization instructor.",
    
    "feature_pro_header_organal" => "Organization analytics",
    "feature_pro_body_organal" => "An extended version of analytics that covers all courses taught in your organization.",

    "feature_pro_header_orgcatalog" => "Landing page",
    "feature_pro_body_orgcatalog" => "Enhance your brand with a customizable landing page about your organization and courses you teach. No need to set up another website to promote your courses.",

    //this one is not visible
    "feature_pro_header_gaps" => "Integration with Google Apps",
    "feature_pro_body_gaps" => "Your organization users can sign in with existing Google Apps for Business accounts, without a need to sign up for a new one.",

    "feature_pro_header_support" => "Priority support",
    "feature_pro_body_support" => "We will do our best to provide a solution within 24 hours.",

    //"e4b_user_error" => "Only organization admininstrator can purchase licenses."
);
