<!-- header -->
<div style="background: #202D8A; background: -moz-linear-gradient(top, #202D8A 0%, #553973 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1f2a44), color-stop(100%,#4189dd)); background: -webkit-linear-gradient(top, #1f2a44 0%,#4189dd 100%); background: -o-linear-gradient(top, #1f2a44 0%,#4189dd 100%); background: -ms-linear-gradient(top, #1f2a44 0%,#4189dd 100%); background: linear-gradient(to bottom, #1f2a44 0%,#4189dd 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f2a44', endColorstr='#4189dd',GradientType=0 ); text-align: center; min-height: 360px;">
    <img src="<?php echo CFG_ROOT_PATH; ?>img/logo-eliademy-tree-white.png" style="height: 100px; margin-top: 30px;">
    <h1 style="margin: 0px; padding: 20px 0px; color: #FFFFFF;"><?php echo(get_string("text_business_title")); ?></h1>
    <p class="subheader" style="color: #FFFFFF;"><?php echo(get_string("text_business_subtitle")); ?></p>
    <div style="text-align: center; padding-bottom: 30px;">
        <form method="get" action="<?php echo CFG_ROOT_PATH . $lang; ?>/premium-signup" style="margin-bottom: 0px;">
            <input type="text" name="company" placeholder="<?php echo get_string("text_business_form_company_name"); ?>" style="vertical-align: top; height: 33px;">
            <input type="submit" class="btn btn-primary btn-large" value="<?php echo get_string("text_business_form_start_free"); ?>">
        </form>
    </div>
</div>
<div style="text-align: center; background: #FAFAFA; border-bottom: 1px solid #EEE; height: 50px;">
    <ul class="nav nav-pills" style="display: inline-block;">
        <li><a href="#features" data-scroll><?php echo get_string("label_features"); ?></a></li>
        <li><a href="#customers" data-scroll><?php echo get_string("label_customers"); ?></a></li>
        <li><a href="#pricing" data-scroll><?php echo get_string("label_pricing"); ?></a></li>
    </ul>
</div>
<div class="container"><div class="panel-wrapper">
    <div class="row-fluid" style="margin-top: 2em;">
        <div class="span5">
            <div style="text-align: center;">
                <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
                <i class="fa fa-users" style="font-size: 25px; color: #4189DD; line-height: 50px;"></i>
                </span>
                <h2 style="color:#3A3A3A"><?php echo get_string("text_business_feature1"); ?></h2>
            </div>
            <p><?php echo get_string("text_business_subfeature1"); ?></p>
            <!-- video tutorial <i class="fa fa-youtube-play"></i> <a class="ignored_link" data-magnific-iframe="true" href="https://www.youtube.com/watch?v=pxXV0xmxxV0"></a> -->
        </div>
        <div class="span7 image right">
            <img src="<?php echo(CFG_ROOT_PATH); ?>img/shots/business-courses.png">
        </div>
    </div>
    <div class="row-fluid" style="margin-top: 2em;">
        <div class="span7 image left">
            <img src="<?php echo(CFG_ROOT_PATH); ?>img/shots/business-webinar.png">
        </div>
        <div class="span5">
            <div style="text-align: center;">
                <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
                <i class="fa fa-calendar" style="font-size: 25px; color: #4189DD; line-height: 50px;"></i>
                </span>
                <h2 style="color:#3A3A3A"><?php echo get_string("text_business_feature2"); ?></h2>
            </div>
            <p><?php echo get_string("text_business_subfeature2"); ?></p>
        </div>
    </div>
    <div class="row-fluid" style="margin-top: 2em;">
        <div class="span5">
            <div style="text-align: center;">
                <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
                <i class="fa fa-cog" style="font-size: 25px; color: #4189DD; line-height: 50px;"></i>
                </span>
                <h2 style="color:#3A3A3A"><?php echo get_string("text_business_feature3"); ?></h2>
            </div>
            <p><?php echo get_string("text_business_subfeature3"); ?></p>
        </div>
        <div class="span7 image right">
            <img src="<?php echo(CFG_ROOT_PATH); ?>img/shots/business-analytics.png">
        </div>
    </div>
    <h1 id="features"><?php echo get_string("text_business_features_header"); ?></h1>
    <p style="text-align: center;"><?php echo get_string("text_business_features_subheader"); ?> <a href="<?php echo CFG_ROOT_PATH . $lang; ?>/features"><?php echo get_string("text_business_features_subheader_link"); ?></a>.</p>
    </div></div> <!-- /container -->
    <div class="braker-block">
        <h2><?php echo get_string("text_business_start_now"); ?></h2>
        <a class="btn btn-large btn-primary" href="<?php echo CFG_ROOT_PATH . $lang; ?>/premium-signup"><?php echo get_string("text_business_trial_signup2"); ?></a>
        <p style="margin-top: 20px;"><?php echo str_replace(array("{", "}"), array("<a href=\"" . CFG_GOOGLE_APPS_LINK . "\" target=\"_blank\">", "</a>"), get_string("text_business_start_now_subtext")); ?></p>
    </div>
    <div class="container"><div class="panel-wrapper">
        <h1 id="customers"><?php echo get_string("text_business_testimonials"); ?></h1>
        <h2><?php echo get_string("text_business_testimonials_subheader"); ?></h2>
        <div class="row-fluid logos-l">
            <div class="span4">
                <a href="http://scanlang.at/index.php/home-english/sprachkurs/" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/scanlang.png"></a>
                <p><em>&laquo;<?php echo get_string("text_business_quote7"); ?>&raquo;</em></p>
                <p>– <?php echo get_string("text_business_part7"); ?></p>
            </div>
            <div class="span4">
                <a href="http://www.farnedi.it" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/farnedi.png"></a>
                <p><em>&laquo;<?php echo get_string("text_business_quote3"); ?>&raquo;</em></p>
                <p>– <?php echo get_string("text_business_part3"); ?></p>
            </div>
            <div class="span4">
                <a href="http://www.innochannel.com.my" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/innochannel.png"></a>
                <p><em>&laquo;<?php echo get_string("text_business_quote4"); ?>&raquo;</em></p>
                <p>– <?php echo get_string("text_business_part4"); ?></p>
            </div>
        </div>
        </div></div> <!-- /container -->
        <div class="braker-block">
            <h2><?php echo get_string("text_business_start_now"); ?></h2>
            <a class="btn btn-large btn-primary" href="<?php echo CFG_ROOT_PATH . $lang; ?>/premium-signup"><?php echo get_string("text_business_trial_signup2"); ?></a>
            <p style="margin-top: 20px;"><?php echo str_replace(array("{", "}"), array("<a href=\"" . CFG_GOOGLE_APPS_LINK . "\" target=\"_blank\">", "</a>"), get_string("text_business_start_now_subtext")); ?></p>
        </div>
        <div class="container"><div class="panel-wrapper">
            <h1 style="margin-bottom: 1em;" id="pricing"><?php echo get_string("text_business_plans_pricing"); ?></h1>
            <div class="row-fluid">
                <div class="span6" style="text-align: center;">
                    <div style="font-size: 92px; line-height: normal;"><big>5</big>&euro;<sub>/<?php echo get_string("text_pricing_per_mo"); ?></sub></div>
                    <div style="color: #666;">+1&euro; <?php echo get_string("text_cost_month_per_user"); ?></div>
                    <a style="margin: 20px 0px;" class="btn btn-large btn-success" href="<?php echo CFG_ROOT_PATH . $lang; ?>/premium-signup"><?php echo get_string("text_business_form_start_free"); ?></a>
                </div>
                <div class="span6" style="line-height: 30px;">
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature0"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature1"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature4"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature5"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature2"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature10"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature11"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature3"); ?></div>
                    <div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature6"); ?></div>
                </div>
            </div>
            <p style="text-align: center; margin: 20px 0px;"><?php echo str_replace(array("{", "}"), array("<a href=\"mailto:support@eliademy.com\">", "</a>"), get_string("text_business_plans_custom")); ?></p>
            <div class="line-separator"></div>
            <div class="row-fluid"><div class="span6">
                <strong><?php echo get_string("text_business_faq_q1"); ?></strong>
                <p><?php echo get_string("text_business_faq_a1"); ?></p>
                <strong><?php echo get_string("text_business_faq_q2"); ?></strong>
                <p><?php echo get_string("text_business_faq_a2"); ?></p>
                <strong><?php echo get_string("text_business_faq_q3"); ?></strong>
                <p><?php echo get_string("text_business_faq_a3"); ?></p>
                <strong><?php echo get_string("text_business_faq_q4"); ?></strong>
                <p><?php echo get_string("text_business_faq_a4"); ?></p>
                <strong><?php echo get_string("text_business_faq_q5"); ?></strong>
                <p><?php echo get_string("text_business_faq_a5"); ?></p>
            </div><div class="span6">
                <strong><?php echo get_string("text_business_faq_q6"); ?></strong>
                <p><?php echo get_string("text_business_faq_a6"); ?></p>
                <strong><?php echo get_string("text_business_faq_q7"); ?></strong>
                <p><?php echo get_string("text_business_faq_a7"); ?> <?php echo get_string("text_business_faq_a7_more"); ?></p>
                <strong><?php echo get_string("text_business_faq_q8"); ?></strong>
                <p><?php echo get_string("text_business_faq_a8"); ?></p>
                <strong><?php echo get_string("text_business_faq_q9"); ?></strong>
                <p><?php echo get_string("text_business_faq_a9"); ?></p>
                <strong><?php echo get_string("text_business_faq_q_last"); ?></strong>
                <p><?php echo get_string("text_business_faq_a_last"); ?></p>
            </div></div>
        <div class="line-separator"></div>
        
        <h1><?php echo get_string("text_business_start_now"); ?></h1>
        <div style="text-align: center;">
            <form method="get" action="<?php echo CFG_ROOT_PATH . $lang; ?>/premium-signup">
                <input type="text" name="company" placeholder="<?php echo get_string("text_business_form_company_name"); ?>" style="vertical-align: top; height: 33px;">
                <input type="submit" class="btn btn-primary btn-large" value="<?php echo get_string("text_business_form_start_free"); ?>">
            </form>
            <p><?php echo str_replace(array("{", "}"), array("<a href=\"" . CFG_GOOGLE_APPS_LINK . "\" target=\"_blank\">", "</a>"), get_string("text_business_start_now_subtext")); ?></p>
        </div>
</div></div> <!-- /container -->

<script type="text/javascript">
    mixpanel.track("Premium page loaded", {"Page name" : "Premium page"});
</script>
<?php /*
<!-- Pricing per year -->
<div style="text-align: center;">
<h2><?php echo get_string("text_business_plans_pricing_subheader"); ?></h2>
<div class="row-fluid" style="line-height: 30px; display: inline-block; text-align: left; width: 80%;">
<div class="span6">
<div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature1"); ?></div>
<div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature2"); ?></div>
<div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature3"); ?></div>
</div><div class="span6">
<div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature4"); ?></div>
<div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature5"); ?></div>
<div><i class="fa fa-check" style="color: green;"></i> <?php echo get_string("pro_feature6"); ?></div>
</div>
</div>
<div class="pricing-boxes">
<div>
<div><strong>25</strong> <?php echo get_string("text_pricing_users"); ?></div>
<div><sup>&euro;</sup><big>250</big><sub><?php echo get_string("text_pricing_per_year"); ?></sub></div>
<a class="btn btn-primary" href="<?php echo CFG_ROOT_PATH . $lang; ?>/business-signup"><?php echo get_string("dlg_sign_up"); ?></a>
</div><div class="active">
<div><strong>50</strong> <?php echo get_string("text_pricing_users"); ?></div>
<div><sup>&euro;</sup><big>500</big><sub><?php echo get_string("text_pricing_per_year"); ?></sub></div>
<a class="btn" href="<?php echo CFG_ROOT_PATH . $lang; ?>/business-signup"><?php echo get_string("dlg_sign_up"); ?></a>
</div><div>
<div><strong>100</strong> <?php echo get_string("text_pricing_users"); ?></div>
<div><sup>&euro;</sup><big>1000</big><sub><?php echo get_string("text_pricing_per_year"); ?></sub></div>
<a class="btn" href="<?php echo CFG_ROOT_PATH . $lang; ?>/business-signup"><?php echo get_string("dlg_sign_up"); ?></a>
</div><div>
<div><strong>200</strong> <?php echo get_string("text_pricing_users"); ?></div>
<div><sup>&euro;</sup><big>2000</big><sub><?php echo get_string("text_pricing_per_year"); ?></sub></div>
<a class="btn" href="<?php echo CFG_ROOT_PATH . $lang; ?>/business-signup"><?php echo get_string("dlg_sign_up"); ?></a>
</div><div>
<div><strong>500</strong> <?php echo get_string("text_pricing_users"); ?></div>
<div><sup>&euro;</sup><big>5000</big><sub><?php echo get_string("text_pricing_per_year"); ?></sub></div>
<a class="btn" href="<?php echo CFG_ROOT_PATH . $lang; ?>/business-signup"><?php echo get_string("dlg_sign_up"); ?></a>
</div>
</div>
<!-- Pricing per year -->
*/ ?>
