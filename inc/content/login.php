<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div style="text-align: center;">
    <h2 style="color:#3A3A3A; margin-top: 30px; margin-bottom: 10px;">
    <?php echo(get_string("dlg_dont_have_account")); ?> <?php echo(get_string("dlg_contact_manager")); ?>
    </h2>
</div>
<div class="content formpage" dir="auto"><div class="container panel"><div class="form-box" style="position: relative;">
    <div id="hideLogin" class="browser_warning" style="position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; background: #FFF;">
        <?php echo get_string("dlg_login_unsupported_browser") ?>
        <a href="http://helpdesk.eliademy.com/knowledgebase/articles/236003"><?php echo get_string("button_learn_more") ?></a>
    </div>
    <script><!--
        if (document.addEventListener && window.navigator.userAgent.indexOf("MSIE ") == -1<?php if (!defined("IE11") || !IE11) { ?> && window.navigator.userAgent.indexOf("Trident/") == -1<?php } ?>) {
            document.getElementById("hideLogin").style.display = "none";
        }
    --></script>
    <div class="topblock" style="display:none;">
        <h3><?php echo get_string("dlg_log_in"); ?></h3>
        <div class="row-fluid">
            <div class="span3" style="text-align: center;">
                <a href="https://www.facebook.com/dialog/oauth/?client_id=<?php echo(CFG_FB_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/facebook_redirect.php&amp;scope=email,user_birthday,user_location&amp;response_type=code" class="btn btn-facebook social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-facebook" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://www.linkedin.com/uas/oauth2/authorization?client_id=<?php echo(CFG_LIN_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/linkedin_redirect.php&amp;scope=r_emailaddress&amp;state=<?php echo(substr(sha1(rand()), 0, 30)); ?>&amp;response_type=code" class="btn btn-linkedin social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-linkedin" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://login.live.com/oauth20_authorize.srf?client_id=<?php echo(CFG_LIVE_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/live_redirect.php&amp;scope=wl.signin%20wl.basic%20wl.emails&amp;response_type=code" class="btn btn-githug social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-windows" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo(CFG_GOOGLE_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/google_redirect.php&amp;scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&amp;state=<?php echo(substr(sha1(rand()), 0, 30)); ?>&amp;response_type=code" class="btn btn-google-plus social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-google-plus" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
        </div>
        <?php if (isset($_GET["error"]) && !empty($_GET["error"])): ?>
        <div class="alert alert-error" style="margin-top: 15px;"><?php echo get_string("dlg_social_signin_error"); ?></div>
        <?php endif; ?>
    </div>
    <div style="text-align: center; margin-bottom: 15px;"><?php //echo(get_string("dlg_login_with_email")); ?></div>
    <form method="post" action="#" id="login_form" class="form-horizontal" style="padding-bottom: 5px;padding-top:5px;">
        <div class="control-group" style="margin-top:10px;">
            <label class="control-label" for="username"><?php echo(get_string("placeholder_email")); ?></label>
            <div class="controls">
                <?php if (isset($_GET["username"]) && !empty($_GET["username"])): ?>
                <input type="text" id="username" name="username" value="<?php echo($_GET["username"]); ?>">
                <?php else: ?>
                <input type="text" id="username" name="username">
                <?php endif; ?>
            </div>
        </div>
        <div class="control-group" id="login_controls">
            <label class="control-label" for="password"><?php echo(get_string("placeholder_password")); ?></label>
            <div class="controls">
                <input type="password" id="password" name="password">
                <span id="signinerror" class="help-inline hide"><?php echo(get_string("dlg_password_incorrect")); ?></span>
            </div>
        </div>
        <div style="float: left; padding: 5px 0px 0px 20px;">
            <a href="javascript:void(0)" onclick="$('#passwd_reset_dialog').modal('show')"><?php echo(get_string("dlg_forgot_password")); ?></a>
        </div>
        <div class="control-group">
            <div class="controls submit">
                <input type="submit" class="btn btn-primary" value="<?php echo(get_string("dlg_log_in")); ?>">
            </div>
        </div>
    </form>
</div>
</div>
</div>
<!--
<div style="text-align: center;">
<a class="twitter-timeline"
data-dnt="true"
href="https://twitter.com/eliademy"
data-widget-id="599182356897996800"
width="420"
data-tweet-limit="2"
data-chrome="nofooter noborders noscrollbar transparent">
Tweets by @eliademy</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>

-->
<div class="modal hide fade" id="passwd_reset_dialog">
<form style="margin: 0px;" method="post" action="#" id="passwd_reset_form">
<div class="modal-header">
    <h3><?php echo(get_string("dlg_forgotten_password")); ?></h3>
</div>
<div class="modal-body" style="padding: 0px;">
    <div style="background-color: #E7F3FF; color: #428EE2; padding: 15px;"><?php echo(get_string("dlg_to_reset_password")); ?></div>
    <div class="control-group" id="reset_controls" style="margin: 15px;">
        <input type="text" style="width: 96%;" name="email" placeholder="<?php echo(get_string("placeholder_email")); ?>">
    </div>
</div>
<div class="modal-footer">
    <input type="submit" class="btn btn-primary" value="<?php echo(get_string("button_submit")); ?>">
</div>
</form>
</div>
<div class="modal hide fade" id="passwd_reset_message_dialog" style="width: 400px; margin-left: -200px;">
<div class="modal-header">
<h3><?php echo(get_string("dlg_forgotten_password")); ?></h3>
</div>
<div class="modal-body" style="padding: 0px;">
<div style="background-color: #E7F3FF; color: #428EE2; padding: 15px;"><?php echo(get_string("dlg_password_reset")); ?></div>
</div>
<div class="modal-footer">
<input type="button" class="btn btn-primary" value="<?php echo(get_string("button_continue")); ?>" data-dismiss="modal" aria-hidden="true">
</div>
</div>
