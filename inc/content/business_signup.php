<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div class="content formpage" dir="auto"><div class="container panel"><div class="form-box" style="position: relative;">
    <div id="hideLogin" class="browser_warning" style="position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; background: #FFF;">
        <?php echo get_string("dlg_login_unsupported_browser") ?>
        <a href="http://helpdesk.eliademy.com/knowledgebase/articles/236003"><?php echo get_string("button_learn_more") ?></a>
    </div>
    <script><!--
        if (document.addEventListener && window.navigator.userAgent.indexOf("MSIE ") == -1<?php if (!defined("IE11") || !IE11) { ?> && window.navigator.userAgent.indexOf("Trident/") == -1<?php } ?>) {
            document.getElementById("hideLogin").style.display = "none";
        }
    --></script>
    <div class="topblock">
        <h3><?php echo get_string("title_signup_business"); ?></h3>
        <div class="alert" style="text-align: left;">
        <?php echo str_replace(array("{", "}"), array("<a href=\"" . CFG_GOOGLE_APPS_LINK . "\" target=\"_blank\">", "</a>"), get_string("text_business_start_now_subtext")); ?></div>
    </div>
    <form id="business_signup_form" class="form-horizontal" method="post" action="#">
        <div class="control-group">
            <label class="control-label" for="orgname"><?php echo get_string("placeholder_organization"); ?></label>
            <div class="controls">
                <input required="required" type="text" name="orgname" id="orgname" value="<?php echo @$_GET["company"]; ?>">
                <span class="help-inline hide"></span>
            </div>
        </div>

        <div class="firstname control-group">
            <label class="control-label" for="firstname"><?php echo get_string("placeholder_firstname"); ?></label>
            <div class="controls">
                <input required="required" type="text" name="firstname" id="firstname">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="lastname control-group">
            <label class="control-label" for="lastname"><?php echo get_string("placeholder_lastname"); ?></label>
            <div class="controls">
                <input required="required" type="text" name="lastname" id="lastname">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="email control-group">
            <label class="control-label" for="email"><?php echo get_string("placeholder_email"); ?></label>
            <div class="controls">
                <input required="required" type="email" name="email" id="email">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="password control-group">
            <label class="control-label" for="password"><?php echo get_string("placeholder_password"); ?></label>
            <div class="controls">
                <input required="required" type="password" name="password" id="password">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="control-group">
            <div class="controls submit">
                <input type="submit" class="btn btn-success btn-large" value="<?php echo(get_string("dlg_sign_up")); ?>">
            </div>
        </div>
    </form>
    <div style="padding: 0px 20px 20px 20px;"><?php echo get_string("dlg_accept_terms"); ?></div>
</div></div></div>
