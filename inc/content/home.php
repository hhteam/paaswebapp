<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<!-- header -->
<!--<div style="background: #FFF url('/img/source1.jpg') no-repeat 50% 0px; background-size: cover; -webkit-background-size: cover; text-align: center; min-height: 360px;">
    <img src="<?php echo CFG_ROOT_PATH; ?>img/pace_logo.png" style="height: 100px; margin-top: 30px;">
    <h1 style="margin: 0px; padding: 20px 0px; color: #FFFFFF; text-shadow: 0 0 10px rgba(74, 69, 63, 0.7);"><?php echo(get_string("text_home_header")); ?></h1>
    <p class="subheader" style="color: #FFFFFF; text-shadow: 0 0 10px rgba(74, 69, 63, 0.7);"><?php echo(get_string("text_home_subheader")); ?></p>
    <a class="btn btn-large btn-success" href="signup" style="margin: 0px 0px 30px 0px;"><?php echo(get_string("button_signup_now")); ?></a>
</div> -->
<!-- paginator block
<div style="text-align: center; background: #FAFAFA; border-bottom: 1px solid #EEE; height: 50px;">
    <ul class="nav nav-pills" style="display: inline-block;">
        <li><a href="#features" data-scroll><?php echo get_string("label_features"); ?></a></li>
        <li><a href="#courses" data-scroll><?php echo get_string("label_courses"); ?></a></li>
        <li><a href="#instructors" data-scroll><?php echo get_string("label_instructors"); ?></a></li>
    </ul>
</div>
paginator block -->
<div class="container panel">
    <div class="panel-wrapper">

       <div class="content formpage" dir="auto"><div class="container panel"><div class="form-box" style="position: relative;">
    <div id="hideLogin" class="browser_warning" style="position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; background: #FFF;">
        <?php echo get_string("dlg_login_unsupported_browser") ?>
        <a href="http://helpdesk.eliademy.com/knowledgebase/articles/236003"><?php echo get_string("button_learn_more") ?></a>
    </div>
    <script><!--
        if (document.addEventListener && window.navigator.userAgent.indexOf("MSIE ") == -1<?php if (!defined("IE11") || !IE11) { ?> && window.navigator.userAgent.indexOf("Trident/") == -1<?php } ?>) {
            document.getElementById("hideLogin").style.display = "none";
        }
    --></script>
    <div class="topblock" style="display:none;">
        <h3><?php echo get_string("dlg_log_in"); ?></h3>
        <div class="row-fluid">
            <div class="span3" style="text-align: center;">
                <a href="https://www.facebook.com/dialog/oauth/?client_id=<?php echo(CFG_FB_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/facebook_redirect.php&amp;scope=email,user_birthday,user_location&amp;response_type=code" class="btn btn-facebook social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-facebook" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://www.linkedin.com/uas/oauth2/authorization?client_id=<?php echo(CFG_LIN_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/linkedin_redirect.php&amp;scope=r_emailaddress&amp;state=<?php echo(substr(sha1(rand()), 0, 30)); ?>&amp;response_type=code" class="btn btn-linkedin social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-linkedin" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://login.live.com/oauth20_authorize.srf?client_id=<?php echo(CFG_LIVE_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/live_redirect.php&amp;scope=wl.signin%20wl.basic%20wl.emails&amp;response_type=code" class="btn btn-githug social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-windows" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo(CFG_GOOGLE_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/google_redirect.php&amp;scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&amp;state=<?php echo(substr(sha1(rand()), 0, 30)); ?>&amp;response_type=code" class="btn btn-google-plus social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-google-plus" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
        </div>
        <?php if (isset($_GET["error"]) && !empty($_GET["error"])): ?>
        <div class="alert alert-error" style="margin-top: 15px;"><?php echo get_string("dlg_social_signin_error"); ?></div>
        <?php endif; ?>
    </div>
    <div style="text-align: center; margin-bottom: 15px;"><?php //echo(get_string("dlg_login_with_email")); ?></div>
    <form method="post" action="#" id="login_form" class="form-horizontal" style="padding-bottom: 5px;padding-top:5px;">
        <div class="control-group" style="margin-top:10px;">
            <label class="control-label" for="username"><?php echo(get_string("placeholder_email")); ?></label>
            <div class="controls">
                <?php if (isset($_GET["username"]) && !empty($_GET["username"])): ?>
                <input type="text" id="username" name="username" value="<?php echo($_GET["username"]); ?>">
                <?php else: ?>
                <input type="text" id="username" name="username">
                <?php endif; ?>
            </div>
        </div>
        <div class="control-group" id="login_controls">
            <label class="control-label" for="password"><?php echo(get_string("placeholder_password")); ?></label>
            <div class="controls">
                <input type="password" id="password" name="password">
                <span id="signinerror" class="help-inline hide"><?php echo(get_string("dlg_password_incorrect")); ?></span>
            </div>
        </div>
        <div style="float: left; padding: 5px 0px 0px 20px;">
            <a href="javascript:void(0)" onclick="$('#passwd_reset_dialog').modal('show')"><?php echo(get_string("dlg_forgot_password")); ?></a>
        </div>
        <div class="control-group">
            <div class="controls submit">
                <input type="submit" class="btn btn-primary" value="<?php echo(get_string("dlg_log_in")); ?>">
            </div>
        </div>
    </form>
</div>
</div>
</div>
        <!-- Social block -->

     
        <!-- Video block -->

       
    </div>
    </div> <!-- /container -->

