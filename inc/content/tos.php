<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div class="content"><div class="container panel" dir="auto"><div class="shadowbox"><div class="page-wrapper">
    <h3 class="underlined">Eliademy.com Service Terms Of Use</h3>
    <h3>1. APPLICATION OF USER AGREEMENT</h3>
    <p>1. These terms of use ("User Agreement") are applied to the use of Eliademy.com applications and services (together the "Services") made available by CBTec Oy (Ltd), a company incorporated and organized under the laws of Finland with its principal place of business at Oravantie 27 F 30, 93600 Kuusamo, Finland with business identification code 2457719-7 ("CBTec").</p>
    <p>2. Our Services are very diverse, so sometimes additional terms or product requirements (including age requirements) may apply. Additional terms will be available with the relevant Services, and those additional terms become part of your agreement with us if you use those Services.</p>
    <p>3. The User undertakes to abide by the rules set forth in this User Agreement, other rules and policies set by CBTec from time to time as well as all applicable laws, rules and regulations applicable to the use of the Service and otherwise.</p>
    <p>4. Use of the Services requires registration as a user of the Services. A registered user is referred to as "User". As part of the registration process, each User has to accept to be bound by the provisions of this User Agreement and any other terms and conditions and instructions posted in the Services, all as amended from time to time by CBTec (Eliademy.com service) and all of which are incorporated herein by reference. The currently applicable User Agreement may be obtained at any time by visiting www.eliademy.com/terms</p>
    <p>5. A User may choose several roles within Services. The User roles are defined below. Additional terms and conditions are applicable to each User role. </p>
    <ol type="a">
        <li> "Instructor" is a User who has an intention to teach or transfer knowledge to a "student" by uploading Content. </li>
        <li> "Student" is a User who receives knowledge from the "Instructor". </li>
        <li> "Administrator" is a User who has rights to access Administration Account(s) and to administer Other User Accounts for organizations with a private learning space.</li>
    </ol>
    <h3>2. USE OF THE SERVICE AND INFORMATION ON USERS</h3>
    <p>1. Registered Users of Eliademy.com have the right to use the Service according to this User Agreement and applicable laws, rules and regulations.</p>
    <p>2. User undertakes to provide Eliademy.com with accurate information requested upon registration or thereafter. Furthermore, User undertakes to keep such information up to date. User is required to provide a valid email address that will also be used as user ID.</p>
    <p>3. A User who is an Administrator undertakes to administer User Accounts. Administrator is responsible for:</p>
    <ol type="a">
        <li>maintaining the confidentiality of the password and Admin Account(s); </li>
        <li>designating those individuals who are authorized to access the Administration Account(s); and </li>
        <li>ensuring that all activities that occur in connection with the Administrator's Account(s) comply with this Agreement.</li>
    </ol>
    <p>4. Individuals have to be at least 16 years old to register as a User of the Services. If the individual is younger than 16 years old then the legal guardian must accept this User Agreement on behalf of the User.</p>
    <p>5. A legal entity may also register as a User, but it needs to be represented by a natural person with the authority to act on behalf of the legal entity. </p>
    <p>6. For account created via Eliademy.com, a legal entity will appear on the Services through private individuals, whose names also appear on the Service profiles. For account created via Eliademy.com/business, a legal entity will appear as the name of the entity in the administration page. A User acting on behalf of a legal entity hereby represent and warrant that the User has the power and the authority to act on behalf of the respective entity. </p>
    <p>7. CBTec may in its sole discretion decide whether any individual or legal entity is eligible for registration to the Services or not.</p>
    <p>8. CBTec has the right to use User Information for the purpose of maintaining the Services (especially emails related to upkeep of the site and its Services), informing about the activities and Services, development, its offerings and using any User Information for statistical or market research purposes of the Services. All User Information is stored in Eliademy.com User Database which may lie outside of the jurisdiction of the User. The User accepts this fact under this User Agreement.</p>
    <p>9. Services may display, transfer and share User information with other Users of the Services in order to provide and make available the Services, in the context of a virtual class room environment and limited only to that functionality. </p>
    <h3>3. USERS' RIGHTS AND RESPONSIBILITIES</h3>
    <p>1. The User is responsible for all content uploaded to the Services. The user must ensure that by uploading data into our Services, the uploaded data, do not violate 3rd party intellectual property. User has the responsibility to protect CBTec from 3rd party claims related to violation of 3rd party IP caused by the uploading actions of the User.</p>
    <p>2. The User undertakes to not upload, send, sell, transmit or save any such material or information on or to the Services that is unlawful nor will he or she solicit any third party to do so. The User also undertakes not to send, sell, transmit, forward or store any material that is protected by copyright, trademark, or other intellectual property rights, unless authorized by the rights' owner to do so.</p>
    <p>3. The User will use the Services at his or her own risk and CBTec is not responsible for any lose of user data or content. </p>
    <h3>4. INDEMNIFICATION</h3>
    <p>1. User hereby indemnify CBTec, its subsidiaries such as Eliademy.com, affiliates, directors, agents, licensors and licensees, transferors and assignees ("Indemnified Parties") against all losses, liabilities, costs and expenses reasonably suffered or incurred by the Indemnified Party, all damages awarded against us under any judgment by a court of competent jurisdiction and all settlements sums paid by us as a result of any settlement agreed by us arising our or in connection with: </p>
    <ol type="a">
        <li>any claim by any third party that the use of the Services by the User is defamatory, offensive or abusive, or is illegal or constitutes a breach of any applicable law, regulation, code of practice; </li>
        <li>any claim by any third party that the use of the Services by the User infringes that third party's copyright or other intellectual property rights of whatever nature; </li>
        <li>any fines or penalties imposed by any regulatory, advertising or trading body or authority in connection with the use of the Services by the User; and </li>
        <li>breach of any terms and conditions of this User Agreement by the User.</li>
    </ol>
    <h3>5. PRICING FOR PAID CONTENT VIA THE MARKETPLACE</h3>
    <p>1. Instructor will be solely responsible for determining the fees to be charged for Course Content, in accordance with the Instructor terms and conditions.</p>
    <p>2. As an instructor of a paid course, you agree that CBTec Oy will be retaining a 30% of the total price of your course as a commission fee. If, as an instructor, you have an active subscription of Eliademy Premium, CBTec Oy will be retaining 10% of the total price of your course as a commission fee.</p>
    <p>3. Revenue, generated by your course, will be transferred to your PayPal account within 45 days of the sale and once it reaches at least 100 euros.</p>
    <p>4. For customers in the EU, a 24% Finland VAT will be collected and reported to EU tax authorities. You are responsible for reporting income received to your local tax authorities. </p>
    <p>5. As a User, you agree to pay the fees for Content that You Purchase, and hereby authorize us to charge you via the payment system you select such as PayPal. We will charge your selected payment system instantly for all fees owed. </p>
    <p>6. Access to Course Content is guaranteed for the duration of 365 days counted from date of purchase, unless otherwise specified by the Course Instructor.</p>
    <h3>6. PRICING FOR ELIADEMY FOR BUSINESS</h3>
    <p>1. Eliademy for Business is priced as indicated on www.eliademy.com/business. Administrator agrees to pay for the licenses you purchase on behalf of your organization and hereby authorize us to charge Your Payment system such as PayPal for these amounts. If Eliademy for Business is purchased via one of our Resellers, the pricing Agreement between You and the Reseller shall apply.</p>
    <h3>7. REFUNDS OF PAYMENTS VIA THE MARKETPLACE</h3>
    <p>1. CBTec offers Users a money back guarantee, after access to Content, on Content that is purchased through Eliademy marketplace. If you, as a User, are unhappy with such Content and request a refund, we will provide you with a full refund of the amount you paid after proper investigation. To request a refund, please contact us via support@eliademy.com.</p>
    <p>2. Instructor, acknowledge and agree that User has the right to receive a refund as set forth in www.eliademy.com/instructor-terms. Neither Instructor nor CBTec shall receive any payments, fees or commissions for any transactions for which a refund has been granted. </p>
    <h3>8. SERVICE AVAILABILITY AND LIMITATIONS</h3>
    <p>1. CBTec pursues to keep the Services available 24 hours a day, 7 days a week. However, CBTec does not give express or implied warranties or representations with regards to non-infringement, availability or fitness for any particular purpose of the Services or for the fact that the Services would be free from errors or the information and</p>
    <p>2. CBTec will not be responsible for any interruptions in the Services, including without limitation to interruptions caused by electricity outage or shortage, disruption of Internet connections, natural disaster, war, mutiny, nuclear disaster, or other force majeure event. </p>
    <p>3. Furthermore, CBTec has the right, at its sole discretion, to make the Services or part of the Services temporarily unavailable in order to perform maintenance, installations, changes, or to address system overload or any other problem caused by requirements of maintenance, public order, safety or force majeure. Eliademy.com will endeavor to inform of such interruptions in the availability of the Services in advance where possible. </p>
    <h3>9. INTELLECTUAL PROPERTY RIGHTS, CONTENT, LICENCES, AND PERMISSIONS</h3>
    <p>1. All software, technology, designs, materials, information, communications, text, graphics, links, electronic art, animations, illustrations, artwork, audio clips, video clips, photos, images, reviews, ideas, and other data or copyrightable materials or content, including the selection and arrangements thereof is "Content." </p>
    <p>2. Content uploaded, transmitted or posted to the Services by a User or Instructor (Submitted Content) remains the proprietary property of the person or entity supplying it (or their affiliated and/or third party providers and suppliers) and is protected, without limitation, pursuant to copyright and other intellectual property laws. You hereby represent and warrant that You have all licenses, rights, consents, and permissions necessary to grant the rights set forth in these Terms to CBTec with respect to Your Submitted Content.</p>
    <p>3. CBTec hereby grants You (as a User) a limited, non-exclusive, non-transferable license to access and use Submitted Content and CBTec Content, for which You have free access to through the Services or where you have paid all required fees, solely for Your personal, non-commercial, educational purposes through the Services, in accordance with these Terms and any conditions or restrictions associated with particular Content.</p>
    <p>4. CBTec does not guarantee that any information or material regarding its user is correct or that the content submitted by its user is correct, accurate, and lawful or conforms to any standard, or that any user is authorized to perform any skill or duty under any law or regulation. If you believe that submitted content of yours violates any law or regulation or is inaccurate or poses any risk whatsoever to a third party, it is your responsibility to take such steps you deem necessary to correct the situation. If you believe that submitted content of a third party or any cbtec content violates any laws or regulations, including, without limitation, any copyright laws, you should report it to the CBTec by sending us a mail at support@eliademy.com.</p>
    <p>5. Eliademy.com will not be responsible for any indirect or consequential damages or losses caused by the use of services.</p>
    <p>6. CBTec has the right to remove any information or content from the Service that is in breach of applicable laws, rules of regulations, this User Agreement or any Eliademy.com policy of use, or that Eliademy.com will in its sole discretion deems inappropriate or otherwise unsuitable for the Services.</p>
    <p>7. The Services and the "Eliademy.com" or "Eliademy" sign are protected by national copyright and other intellectual property laws and international conventions. CBTec Oy and its licensors reserve all rights. User may not copy, transmit, store, publish, and make available to public online or otherwise the Services, any part of the Service or any information or material available on the Services without prior written permission of CBTec.</p>
    <p>8. CBTec has the right to remove (suspend) any User that is or has been in violation of this User Agreement, applicable regulations or law. User may be suspended on temporarily basis or indefinitely as deemed appropriate by CBTec. </p>
    <h3>10. CONTENT RESTRICTIONS</h3>
    <p>1. You may not upload, post, or transmit (collectively, "submit") any video, image, text, audio recording, or other work (collectively, "content") that:</p>
    <ol type="a">
        <li>Infringes any third party's copyrights or other rights (e.g., trademark, privacy rights, etc.);</li>
        <li>Contains sexually explicit content or pornography (provided, however, that non-sexual nudity is permitted);</li>
        <li>Contains hateful, defamatory, or discriminatory content or incites hatred against any individual or group;</li>
        <li>Exploits minors;</li>
        <li>Depicts unlawful acts or extreme violence;</li>
        <li>Depicts animal cruelty or extreme violence towards animals;</li>
        <li>Promotes fraudulent schemes, multi level marketing (MLM) schemes, get rich quick schemes, gambling, cash gifting, work from home businesses, or any other dubious money-making ventures; or</li>
        <li>Violates any law.</li>
    </ol>
    <p>2. If CBTec becomes aware of abovementione content, it will report to the appropriate authorities and delete the Eliademy Accounts of those involved with the distribution. </p>
    <h3>11. AMENDMENTS</h3>
    <p>1. CBTec may amend the terms and conditions of this Terms of Use.</p>
    <p>2. User may terminate this User Agreement and discontinue the use of the Services before the amendments enter into force.</p>
    <h3>12. TERM AND TERMINATION, SUSPENSION</h3>
    <p>1. This User Agreement enters into force after being accepted by the individual in Services and after CBTec has accepted the individual as a User of the Services by granting the User access to the Services. For sake of clarity, if CBTec does not accept the individual as a User of the Services, this User Agreement does not enter into force.</p>
    <p>2. This agreement shall be in force until terminated by either party. Each party may terminate this User Agreement with immediate effect either by email or via the Services.</p>
    <p>3. Both parties shall be free of fulfilling the obligations of this User Agreement for the time and scope dictated by circumstances outside of control of each party. Circumstances outside of control shall be considered as sudden, unexpected and unforeseen circumstances, which could not have been avoided by either party. At least the following types of incidents shall be counted as such: war, mutiny, and confiscation by authorities, interruption on electricity or internet traffic, strike or other labour action, fire, thunderstorm or another natural disaster, or any other unusual incident that has a major impact on either party's ability to fulfill the agreement, that is beyond control of the affected party.</p>
    <p>4. CBTec may choose to suspend the provision of the Services to any User(s) that is, has been or is suspected by CBTec to be or to have been in violation of this User Agreement or applicable laws, rules or regulations. </p>
    <h3>13. TRANSFER OF AGREEMENT</h3>
    <p>The User does not have right to transfer this Agreement or its rights and obligations under this Agreement without prior written approval of CBTec.</p>
    <h3>14. APPLICABLE LAW AND RESOLUTION OF DISPUTES</h3>
    <p>1. The laws of Finland, excluding its choice of law's provisions, shall govern this User Agreement and all use of the Services. </p>
    <p>2. Any disputes arising between User and CBTec shall be settled via negotiations, where possible. Any dispute arising out of or in connection with the Agreement or the Services shall be resolved in front of the District Court in the City of Helsinki, Finland. Both contracting parties submit to the exclusive jurisdiction of the Finnish Courts.</p>
    <p>3. If the User is a consumer domiciled in Finland, all disputes arising out of or in connection with this User Agreement or the Services may be resolved also in front of the District Court that has jurisdiction over the domicile of the User. If the User is a consumer who is not domiciled in Finland, all disputes arising out of or in connection with this Agreement or the Services shall be resolved in front of the District Court in the City of Helsinki, Finland and in Finnish language. </p>
</div></div></div></div>