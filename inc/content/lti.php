<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div dir="auto" class="container">
    <h3 class="underlined">Learning Tools Interoperability in Eliademy</h3>
    <div id="introduction">
        <h3>Introduction<a name="introduction">&nbsp;</a></h3>
        <p>Eliademy supports embedding external tools as course content attachments via <a href="http://www.imsglobal.org/lti/"
        target="_blank">Learning Tools Interoperability</a>&trade; (LTI) protocol. Some applications are pre-configured for the end user and additional apps can be added individualy.</p>
        <p>In order to get a general overview of how LTI works, please refer to <a href="http://developers.imsglobal.org/tutorials.html" target="_blank">IMS Global tutorial</a>. Official documentation fully covers all major concepts of LTI protocol. The next figure illustrates the basic of LTI.</p>
        <p dir="auto"><a href="http://www.imsglobal.org/lti/blti/bltiv1p0pd/LTI_BasicLTI_Implementation_Guide_v1p0p1pd.html"><img src="http://www.imsglobal.org/lti/blti/bltiv1p0pd/images/image002.jpg" class="img-polaroid" alt="IMS Overview of Basic LTI"></a></p>

        <p>If you are interested in developing LTI compatible applications, please follow the <a href="http://www.imsglobal.org/lti/blti/bltiv1p0pd/LTI_BasicLTI_Implementation_Guide_v1p0p1pd.html" target="_blank">official developer documentation</a>. For authentication purposes LTI relies on the <a href="http://www.OAuth.net/" target="_blank">OAuth</a> protocol. For an extensive description, please refer to <a
        href="http://www.imsglobal.org/lti/blti/bltiv1p0/ltiBLTIimgv1p0.html#_Toc261271976">Basic LTI Security Model</a> section in LTI 1.0 spec.</p>

    </div>
    <div id="configuration">
        <h3>User experience<a name="configuration">&nbsp;</a></h3>
        <p>Content from LTI compatible resources can be attached to any course in Eliademy.</p>
        <p dir="auto"><img src="<?php echo(CFG_ROOT_PATH); ?>img/lti/lti-add-popup.jpg" class="img-polaroid" alt="lti-add-popup"></a></p>
        <p>Some basic LTI compatible apps are available out of the box (Khan Academy, TED Ed, Wikipedia etc). No additional steps are required and those apps are available for all Eliademy users.</p>
        <p dir="auto"><img src="<?php echo(CFG_ROOT_PATH); ?>img/lti/lti-default-apps.jpg" class="img-polaroid" alt="lti-default-apps"></a></p>
        <p>Users can also configure additional apps, which will be exclusively linked to their personal account. Configuration needs to be done only once, and will remain on App selection screen unless removed or edited manually.</p>
        <p dir="auto"><img src="<?php echo(CFG_ROOT_PATH); ?>img/lti/lti-custom-app.jpg" class="img-polaroid" alt="lti-custom-app"></a></p>
        <p>LTI configuration details should be obtained from the third party service provider via their developer website or by email request. The following details are reqired to configre a custom app in Eliademy.</p>
        <ul>
            <li>Name - name for the App</li>
            <li>Base URL - link for connecting to third party app.</li>
            <li>Icon URL - link for app icon</li>
            <li>Customer Key - OAuth consumer key</li>
            <li>Customer Secret - OAuth consumer secret</li>
            <li>Share Users Username - enables sharing of course participants names with the third party app provider</li>
            <li>Share Users Email - enables sharing of course participants emails with the third party app provider</li>
        </ul>
        <p>Resources added this way (for both pre-configured and custom apps) will be accessible to all course participants, assuming that the added app does not require additional authentication settings. Course creators can rename and delete added resources in the same way as any file attachments. Clicking on the resource will open the linked resource in a new window.</p>
        <p dir="auto"><img src="<?php echo(CFG_ROOT_PATH); ?>img/lti/lti-added-resources.jpg" class="img-polaroid" alt="lti-added-resources"></a></p>
        <p>An extensive and up to date list of LTI compatible apps can be found on the community run <a href="www.edu-apps.org/index.html" target="_blank">Edu Apps</a> website. Most of them can be enabled in Eliademy by an educator. Whenever assistance is required, please get in touch with us via our <a href="http://eliademy.uservoice.com/" target="_blank">support page</a>.</p>
    </div>
    <div id="wip">
        <h3>Work in progress<a name="wip">&nbsp;</a></h3>
        <p>At this moment we are working on adding compliance with the LTI 1.1 spec. This will enable educators to link external self assesment tools to Eliademy and keep track of received scores directly on the course tasks page.</p>

    </div>
    <div style="margin: 1em 0px 0px 0px; text-align: right;">
        <div class="fb-like" style="margin-right: 10px; vertical-align: top;" data-href="https://www.facebook.com/Eliademy" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true"></div>
    </div>
</div>