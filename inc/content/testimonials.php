<div class="container">
    <div class="panel-wrapper">
        <!-- general container -->
        <!-- Free -->
        <h1 id="instructors"><?php echo get_string("text_over_15K_teachers"); ?></h1>
        <!-- Text on left  Pavel-->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_pavel"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="http://www.humanitas.cz/?menu=skola&submenu=1"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_pavel"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/pavel.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right Inge -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/inge.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_inge"); ?></p>
                <i class="fa fa-external-link" ></i> <a class="ignored_link" href="http://ignatiawebs.blogspot.fi/2013/03/eliademy-mobile-mooc-platform-resides.html"><?php echo get_string("button_learn_more"); ?></a>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_inge"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left  Ketil-->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_ketil"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="http://www.smi.se"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_ketil"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/ketil.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right  Kamp-->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/kamp.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_kamp"); ?></p>
                <i class="fa fa-external-link" ></i> <a class="ignored_link" href="http://nl.linkedin.com/in/hsjkamp"><?php echo get_string("button_learn_more"); ?></a>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_kamp"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left Priscilla -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_priscilla"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://eliademy.com/users/31956.html"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_priscilla"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/priscilla.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right Badanov -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/badanov.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_badanov"); ?></p>
                <i class="fa fa-external-link" ></i> <a class="ignored_link" href="http://badanovag.blogspot.fi/2013/03/eliademy.html"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_badanov"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left Mevlut -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_mevlut"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://eliademy.com/users/9709.html"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_mevlut"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/develi.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right Tiina -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/huhtala.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_tiina"); ?></p>
                <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/users/10798.html"><?php echo get_string("button_learn_more"); ?></a>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_tiina"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left Jules -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_jules"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://eliademy.com/users/19947.htm"><?php echo get_string("button_learn_more"); ?></a>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_jules"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/gourley.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right Mccall -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/default_185.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_mccall"); ?></p>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_mccall"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left Francis -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_francis"); ?></p>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_francis"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/default_185.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right hoffman -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/default_185.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_hoffman"); ?></p>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_hoffman"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left radomske -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_radomske"); ?></p>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_radomske"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/default_185.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- THE FOLLOWING 3 is repetiion from landing page -->
        <!-- Text on right susanna -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/susanna.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:25px;"><?php echo get_string("testimonial_text_susanna"); ?></p>
                <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/users/8436.html"><?php echo get_string("button_learn_more"); ?></a>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_susanna"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left Harria -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p><?php echo get_string("testimonial_text_harris"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://eliademy.com/users/89366.html"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_harris"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/harris.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right belskaya -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/belskaya.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_belskaya"); ?></p>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_belskaya"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left piccinini -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p><?php echo get_string("testimonial_text_piccinini"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://eliademy.com/users/86460.html"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_piccinini"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/piccinini.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right Aika -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/aikathiele.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_aikathiele"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://twitter.com/aikathiele/status/735831165018484736"><?php echo get_string("button_learn_more"); ?></a>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_aikathiele"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Text on left testimonial_syedhashmi -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span9" style="text-align: left;">
                <p><?php echo get_string("testimonial_text_syedhashmi"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://eliademy.com/blog/2016/05/26/case-study-illustrated-math-series/"><?php echo get_string("button_learn_more"); ?></a>
                <div >
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_syedhashmi"); ?></h3>
                </div>
            </div>
            <div class="span3 image right">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/syedhashmi.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
        </div>
        <!-- Text on right Barrero -->
        <div class="row-fluid" style="margin-top: 2em;">
            <div class="span3 image left">
                <img src="<?php echo(CFG_ROOT_PATH); ?>img/profiles/jorgebarrero.jpg" class="img-polaroid" style="border-radius:50%">
            </div>
            <div class="span9" style="text-align: right;">
                <p style="margin-top:50px;"><?php echo get_string("testimonial_text_jorgebarrero"); ?></p>
                <i class="fa fa-external-link"></i> <a class="ignored_link" href="https://twitter.com/JorgeBarreroR/status/738927531756294145"><?php echo get_string("button_learn_more"); ?></a>
                <div>
                    <h3 style="color:#3A3A3A"><?php echo get_string("testimonial_jorgebarrero"); ?></h3>
                </div>
            </div>
        </div>
        <!-- Sign up -->
        <div class="line-separator"></div>
        <h1><?php echo get_string("text_interested"); ?></h1>
        <div class="row-fluid">
            <div class="span12" style="text-align: center;">
                <a href="<?php echo CFG_ROOT_PATH . $lang; ?>/signup" class="btn btn-success btn-large"><?php echo get_string("button_get_started"); ?> <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
        <!-- general container -->
    </div>
</div>