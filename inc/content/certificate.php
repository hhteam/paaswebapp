<?php
    $cert_hash = $uri_parts[$uri_index + 1];
    if (strtolower(substr($cert_hash, -5)) == ".html")
    {
        $cert_hash = substr($cert_hash, 0, -5);
    }
    $info = null;
    try
    {
        $db = new PDO("mysql:dbname=" . DB_NAME . ";host=" . DB_HOST, DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $q = $db->prepare("SELECT mc.userid AS userid, mcd.code AS code, md1.value AS certmode, u.firstname AS firstname, u.lastname AS lastname, md2.value AS pichash, mc.state AS state " .
            "FROM " . DB_PREFIX . "monorail_certificate AS mc " .
                "INNER JOIN " . DB_PREFIX . "monorail_course_data AS mcd ON mcd.courseid=mc.courseid " .
                "INNER JOIN " . DB_PREFIX . "user AS u ON u.id=mc.userid " .
                "LEFT JOIN " . DB_PREFIX . "monorail_data AS md1 ON md1.itemid=mc.courseid AND md1.datakey='cert_enabled' " .
                "LEFT JOIN " . DB_PREFIX . "monorail_data AS md2 ON md2.itemid=mc.userid AND md2.datakey='pichash' " .
                    "WHERE mc.hashurl=?");
        $q->bindParam(1, $cert_hash);
        if ($q->execute())
        {
            $info = $q->fetch(PDO::FETCH_ASSOC);
        }
    }
    catch (Exception $ex)
    {
        error_log(var_export($ex, true));
        $info = null;
    }
    if ($info && $info["state"]) {
?>
<div class="container" dir="auto" style="padding-top: 30px;">
    <div class="row-fluid">
        <span class="span9">
        <div style="box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.3); border-radius: 3px; text-align: center; margin-bottom: 25px;">
            <img src="/cert/<?php echo $cert_hash ?>.jpg">
        </div>
        <?php if ($info["certmode"] == "2") { ?>
        <div class="cert-block-user-<?php echo $info["userid"] ?>" style="display: none;">
            <a href="/app/a/certificate/<?php echo($info["code"]); ?>" style="margin-left: 10px;" class="pull-right btn btn-success btn-large"><?php echo get_string("label_order_now"); ?></a>
            <h2><?php echo get_string("label_premium_cert_available"); ?></h2>
        </div>
        <div class="cert-block-no-user" style="display: none;">
            <a href="/app/a/certificate/<?php echo($info["code"]); ?>" style="margin-left: 10px;" class="pull-right btn btn-success btn-large"><?php echo get_string("dlg_log_in"); ?></a>
            <h2><?php echo str_replace("%NAME", $info["firstname"] . " " . $info["lastname"], get_string("label_premium_cert_available_2")); ?></h2>
        </div>
        <?php } ?>
        </span>
        <span class="span3">
        <div style="box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.3); border-radius: 3px; margin-bottom: 20px; padding: 10px 20px; display: none;" class="cert-block-user-<?php echo $info["userid"] ?>">
            <a href="#" style="color: #333;" onclick="addthis.update('share', 'url', window.location.href); addthis_sendto('more'); return false;" class="pull-right"><i class="fa fa-share"></i> <?php echo get_string("label_share"); ?></a>
            <a href="/cert/<?php echo $cert_hash ?>.pdf" style="color: #333;" target="_blank"><i class="fa fa-share"></i> <?php echo get_string("label_download"); ?></a>
        </div>
        <div style="box-shadow: 0 1px 2px 0 rgba(50, 50, 50, 0.3); border-radius: 3px; margin-bottom: 20px; padding: 10px 20px;">
            <h3 style="margin: 5px 0px 20px 0px; padding-bottom: 10px; border-bottom: 1px solid #BBB;"><?php echo get_string("label_recipient"); ?></h3>
            <div style="text-align: center;">
                <img class="thumbnail" style="display: inline; border-radius: 120px; max-width: 120px;" src="/app/public_images/user_picture/<?php echo ($info["pichash"] ? $info["pichash"] : "default"); ?>_185.jpg">
                <h3 style="color: #333; font-size: 16px;"><?php echo($info["firstname"] . " " . $info["lastname"]); ?></h3>
            </div>
        </div>
        <div id="course-cert-block"></div>
        <script>
        $.get("/catalog/featured.html?sku=<?php echo $info["code"]; ?>&type=list", function (data)
        {
        $("#course-cert-block").html(data);
        });
        </script>
        </span>
    </div>
</div>
<?php } else { ?>
<h1><?php echo get_string("label_cert_not_exist") ?></h1>
<?php } ?>