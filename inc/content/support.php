<div style="background: #202D5F; background: -moz-linear-gradient(top, #202D5F 0%, #553973 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1f2a44), color-stop(100%,#553973)); background: -webkit-linear-gradient(top, #1f2a44 0%,#553973 100%); background: -o-linear-gradient(top, #1f2a44 0%,#553973 100%); background: -ms-linear-gradient(top, #1f2a44 0%,#553973 100%); background: linear-gradient(to bottom, #1f2a44 0%,#553973 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f2a44', endColorstr='#553973',GradientType=0 ); text-align: center; min-height: 280px;">
    <img src="<?php echo CFG_ROOT_PATH; ?>img/logo-eliademy-tree-white.png" style="height: 100px; margin-top: 30px;">
    <h1 style="margin: 0px; padding: 20px 0px; color: #FFFFFF;"><?php echo(get_string("text_support_hero_title")); ?></h1>
    <p class="subheader" style="color: #FFFFFF;"><?php echo(get_string("text_support_hero_subtitle")); ?></p>
    <div style="text-align: center; padding-bottom: 30px;">
        <a class="btn btn-large btn-info" href="#donate" data-scroll><?php echo get_string("text_support_hero_action"); ?></a>
    </div>
</div>
<div class="container">
    <div class="panel-wrapper">
        <!-- general container -->
        <h2 style="text-align: center;margin-top:30px; color:#553973"><?php echo get_string("text_support_principles_header"); ?></h2>
        <div class="row-fluid">
            <div class="span4">
                <div style="text-align: center;">
                    <span style="border: 1px solid #553973; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
                    <span style="font-size: 28px; color: #553973; line-height: 50px;">1</span>
                    </span>
                    <h2 style="color:#553973"><?php echo get_string("text_support_principles_1"); ?></h2>
                </div>
                <p style="text-align: center;"><?php echo get_string("text_support_principles_1_description"); ?></p>
            </div>
            <div class="span4">
                <div style="text-align: center;">
                    <span style="border: 1px solid #553973; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
                    <span style="font-size: 28px; color: #553973; line-height: 50px;">2</span>
                    </span>
                    <h2 style="color:#553973"><?php echo get_string("text_support_principles_2"); ?></h2>
                </div>
                <p style="text-align: center;"><?php echo get_string("text_support_principles_2_description"); ?></p>
            </div>
            <div class="span4">
                <div style="text-align: center;">
                    <span style="border: 1px solid #553973; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
                    <span style="font-size: 28px; color: #553973; line-height: 50px;">3</span>
                    </span>
                    <h2 style="color:#553973"><?php echo get_string("text_support_principles_3"); ?></h2>
                </div>
                <p style="text-align: center;"><?php echo get_string("text_support_principles_3_description"); ?></p>
            </div>
        </div>
        <p style="text-align: center; margin-top:30px"><?php echo get_string("text_support_principles_paragraph"); ?></p>
        <div class="line-separator"></div>
        <!-- Donate block -->
        <div id="donate" style="text-align: center; margin-top: 2em;">
            <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
            <i class="fa fa-heartbeat" style="font-size: 35px; color: #4189DD; line-height: 50px;"></i>
            </span>
        </div>
        <h2 style="text-align: center"><?php echo get_string("text_support_donate_header"); ?></h2>
        <p style="margin-top: 20px; margin-bottom:20px; text-align: center;"><?php echo get_string("text_support_donate_paragraph"); ?></p>
        <!-- block with 3 courses -->
        <div id="featured-course-block" class="catalog-3cols"></div>
        <script>
                    $.get("/catalog/donate.html?type=list&limit=3", function (data)
                    {
                        $("#featured-course-block").html(data);
                    });
        </script>
        <p style="margin-top: 20px; margin-bottom:20px; text-align: center;"><?php echo str_replace(array("{", "}"), array("<a href=\"mailto:support@eliademy.com?Subject=I%20would%20like%20to%20support%20Eliademy\" target=\"_blank\">", "</a>"), get_string("text_support_donate_footer")); ?></p>
        <div class="line-separator"></div>
        <!-- Share block -->
        <div style="text-align: center; margin-top: 2em;">
            <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
            <i class="fa fa-share-alt" style="font-size: 35px; color: #4189DD; line-height: 50px;"></i>
            </span>
        </div>
        <h2 style="text-align: center"><?php echo get_string("text_support_share_header"); ?></h2>
        <!-- Social block goes here -->
        <div style="text-align: center;">
            <!-- Go to www.addthis.com/dashboard to generate a new set of sharing buttons -->
            <a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a>
            <a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a>
            <a href="https://api.addthis.com/oexchange/0.8/forward/google_plusone_share/offer?url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/google_plusone_share.png" border="0" alt="Google+"/></a>
            <a href="https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/linkedin.png" border="0" alt="LinkedIn"/></a>
            <a href="https://api.addthis.com/oexchange/0.8/forward/vk/offer?url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/vk.png" border="0" alt="VKontakte"/></a>
            <a href="https://api.addthis.com/oexchange/0.8/forward/tumblr/offer?url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/tumblr.png" border="0" alt="Tumblr"/></a>
            <a href="https://api.addthis.com/oexchange/0.8/forward/email/offer?url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/email.png" border="0" alt="Email"/></a>
            <a href="https://www.addthis.com/bookmark.php?source=tbx32nj-1.0&v=300&url=http%3A%2F%2Fwww.eliademy.com&pubid=ra-512e0dda5c857366&ct=1&title=Are%20you%20looking%20for%20a%20place%20to%20create%2C%20share%20and%20teach%20online%20courses%3F%20Then%20it%E2%80%99s%20time%20to%20try%20Eliademy.&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/addthis.png" border="0" alt="Addthis"/></a>
        </div>
        <p style="margin-top: 20px; text-align: center;"><?php echo get_string("text_support_share_paragraph"); ?></p>
        <div class="line-separator"></div>
        <!-- Localize block -->
        <div style="text-align: center; margin-top: 2em;">
            <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
            <i class="fa fa-language" style="font-size: 35px; color: #4189DD; line-height: 50px;"></i>
            </span>
        </div>
        <h2 style="text-align: center"><?php echo get_string("text_support_localize_header"); ?></h2>
        <p style="margin-top: 20px; text-align: center;"><?php echo get_string("text_support_localize_paragraph"); ?></p>
        <div style="tmargin-top: 20px; text-align: center"><a class="btn btn-large btn-primary" href="http://translate.eliademy.com/"><?php echo get_string("text_support_localize_action"); ?></a></div>
        <div class="line-separator"></div>
        <!-- footer -->
        <h2 style="text-align: center"><?php echo get_string("text_support_footer"); ?></h2>
        <h2 style="text-align: center"><?php echo get_string("text_support_footer_regards"); ?></h2>
        <h2 style="text-align: center"><a href="<?php echo CFG_ROOT_PATH . $lang; ?>/about"><?php echo get_string("text_support_footer_signature"); ?></a></h2>
        <a href="<?php echo CFG_ROOT_PATH . $lang; ?>/about">
            <img src="<?php echo(CFG_ROOT_PATH); ?>img/group-photo.jpg" alt="About Us">
        </a>
        <br>
        <!-- general container -->
    </div>
</div>