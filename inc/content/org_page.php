<?php
    $courses = json_decode($info["courses"]);
    $courseInfos = array();
    $possibleCountries = array();
    $currentCategory = null;
    $currentCountry = null;
    $searchQuery = null;

    if (!is_array($courses)) {
        $courses = array();
    }

    try {
        require_once __DIR__ . "/../../app/theme/monorail/vendor/autoload.php";
        $redis = new Predis\Client(array(
            'scheme' => 'tcp',
            'host'   => REDIS_HOST,
            'port'   => REDIS_PORT,
        ));
    } catch (Exception $err) {
        $redis = null;
    }

    $categories = json_decode($info["cats"]);

    if (!is_array($categories)) {
        $categories = array();
    }

    if (array_key_exists(@$_GET["cat"], $categories)) {
        $currentCategory = $categories[$_GET["cat"]];
        if ($currentCategory) {
            $courses = $currentCategory->courses;
        }
    }

    if( isset( $_GET['c'] ) )
        $currentCountry = strtoupper( $_GET['c'] );

    if (isset($_GET["s"])) {
        $searchQuery = $_GET["s"];
    }

    if ($redis) {
        foreach ($courses as $cid) {
            $data = $redis->get("course_info_" . $cid);

            if ($data) {
                $cdata = unserialize($data);

                if ($cdata->course_privacy != 1 || $cdata->course_access == 1) {
                    continue;
                }

                $countries = array();
                if( isset( $cdata->course_country ) )
                    $countries = array_fill_keys(
                                    explode( ',', $cdata->course_country ), 1 );
                $possibleCountries += $countries;
                if( $currentCountry && !isset( $countries[$currentCountry] ) )
                    continue;

                // Course summary not in redis yet... so searching only fullname
                if ($searchQuery && !searchMatch($searchQuery, $cdata->fullname)) {
                    continue;
                }

                $courseInfos[] = $cdata;
            }
        }
    }

    if (!empty($possibleCountries)) {
        // Taking english county names directly from moodle strings...
        require_once __DIR__ . "/../../app/lang/en/countries.php";

        foreach (array_keys($possibleCountries) as $ccode) {
            $possibleCountries[$ccode] = $string[$ccode];
        }
    }

    function courseCmp($a, $b) {
        return strcmp($a->fullname, $b->fullname);
    }

    uasort($courseInfos, "courseCmp");

    $haveAds = false;
    $ads1 = array();
    $ads2 = array();
    $ads3 = array();

    if ($info["ad"]) {
        $ad_info = json_decode($info["ad_info"]);

        foreach ($ad_info->ads as $ad) {
            if ($ad->country && $currentCountry != strtoupper($ad->country)) {
                continue;
            }

            switch ($ad->group) {
                case 1:
                    $ads1[] = $ad;
                    $haveAds = true;
                    break;

                case 2:
                    $ads2[] = $ad;
                    $haveAds = true;
                    break;

                case 3:
                    $ads3[] = $ad;
                    $haveAds = true;
                    break;
            }
        }
    }

?>
<?php if (file_exists(__DIR__ . "/../../app/public_images/org/cover" . $info["id"] . ".jpg")) { ?>
<div style="background: url(<?php echo CFG_MOODLE_PATH ?>public_images/org/cover<?php echo $info["id"] ?>.jpg) no-repeat center center; background-size: cover; width: 100%; height: 270px;"></div>
<?php } else { ?>
<div style="width: 100%; height: 270px; background: #202D5F; background: -moz-linear-gradient(top, #202D5F 0%, #553973 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1f2a44), color-stop(100%,#553973)); background: -webkit-linear-gradient(top, #1f2a44 0%,#553973 100%); background: -o-linear-gradient(top, #1f2a44 0%,#553973 100%); background: -ms-linear-gradient(top, #1f2a44 0%,#553973 100%); background: linear-gradient(to bottom, #1f2a44 0%,#553973 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f2a44', endColorstr='#553973',GradientType=0 );"></div>
<?php } ?>

<div class="catalog-container">

    <div class="row-fluid">
        <div class="span3">
            <div style="display: inline-block;">
            <?php if (file_exists(__DIR__ . "/../../app/public_images/org/logo" . $info["id"] . ".png")) { ?>
            <img class="img-polaroid" style="margin-top: -48px;" src="<?php echo CFG_MOODLE_PATH ?>public_images/org/logo<?php echo $info["id"] ?>.png">
            <?php } ?>

            <?php if ($details->link_web) { ?>
                <div style="text-align: center; margin-top: 12px;">
                <a href="<?php echo linkFix($details->link_web) ?>" target="_blank" style="font-size: 14px; color: #478FE1; text-decoration: none;">
                <i class="fa fa-external-link" style="color: #666;"></i> <?php echo $details->link_web ?>
                </a>
                </div>
            <?php } ?>
            </div>
        </div>
        <div class="span9" style="padding-top: 24px;">
            <div style="float: right; padding-top: 10px;">
                <?php if ($details->link_fb) { ?>
                    <a href="<?php echo linkFix($details->link_fb) ?>" target="_blank" class="btn btn-facebook social-signup" style="width: 25px; padding: 0px; border-radius: 50%; margin-right: 10px;">
                    <i class="fa fa-facebook" style="line-height: 25px; font-size: 16px;"></i>
                    </a>
                <?php } ?>
                <?php if ($details->link_li) { ?>
                    <a href="<?php echo linkFix($details->link_li) ?>" target="_blank" class="btn btn-linkedin social-signup" style="width: 25px; padding: 0px; border-radius: 50%; margin-right: 10px;">
                    <i class="fa fa-linkedin" style="line-height: 25px; font-size: 16px;"></i>
                    </a>
                <?php } ?>
                <?php if ($details->link_tw) { ?>
                    <a href="<?php echo linkFix($details->link_tw) ?>" target="_blank" class="btn btn-twitter social-signup" style="width: 25px; padding: 0px; border-radius: 50%; margin-right: 10px;">
                    <i class="fa fa-twitter" style="line-height: 25px; font-size: 16px;"></i>
                    </a>
                <?php } ?>
                <?php if ($details->link_gp) { ?>
                    <a href="<?php echo linkFix($details->link_gp) ?>" target="_blank" class="btn btn-google-plus social-signup" style="width: 25px; padding: 0px; border-radius: 50%; margin-right: 10px;">
                    <i class="fa fa-google-plus" style="line-height: 25px; font-size: 16px;"></i>
                    </a>
                <?php } ?>
            </div>
            <h1 style="margin: 0px 0px 24px 0px; color: #666; text-align: left;"><?php echo $info["name"] ?></h1>
            <div><?php echo str_replace("\n", "<br>", $details->about) ?></div>
        </div>
    </div>

    <?php if (!empty($courses)) { ?>

    <div class="row-fluid" style="margin-top: 24px;">

        <span class="span4">
<?php if (!empty($categories)) { ?>

<div class="dropdown">
  <a class="dropdown-toggle catalog-dropdown" data-toggle="dropdown" href="#"><?php

    if ($currentCategory) {
        // XXX: count($courseInfos) perhaps?
        echo $currentCategory->title . " (" . count($currentCategory->courses) . ")";
    } else {
        echo get_string("label_all_courses");
        // Or search results...?
    }

  ?> <i class="fa fa-chevron-down" style="font-size: 14px; vertical-align: middle;"></i></a>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
    <li><a href="<?php echo makeLink("cat", null, array("c")) ?>"><?php echo get_string("label_all_courses") ?></a></li>
<?php foreach ($categories as $key => $cat) { ?>
    <li><a href="<?php echo makeLink("cat", $key, array("c")) ?>"><?php echo $cat->title ?> (<?php echo count($cat->courses) ?>)</a></li>
<?php } ?>
  </ul>
</div>

<?php } else { ?>

    <div class="catalog-dropdown"><?php echo get_string("label_all_courses") ?> (<?php echo count($courseInfos) ?>)</div>

<?php } ?>
        </span>

        <span class="span8" style="text-align: right;">

<?php if (!empty($possibleCountries)) { ?>

<div class="dropdown" style="display: inline-block;">
  <a class="dropdown-toggle catalog-dropdown" data-toggle="dropdown" href="#"><?php

    if ($currentCountry) {
        echo $possibleCountries[$currentCountry];
    } else {
        echo get_string("label_country");
    }

  ?> <i class="fa fa-chevron-down" style="font-size: 14px; vertical-align: middle;"></i></a>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" style="text-align: left;">
    <li><a href="<?php echo makeLink("c", null) ?>"><?php echo get_string("label_all") ?></a></li>
<?php foreach ($possibleCountries as $key => $c) { ?>
    <li><a href="<?php echo makeLink("c", $key) ?>"><?php echo $c ?></a></li>
<?php } ?>
  </ul>
</div>

<?php } ?>

        <form method="get" action="?" style="display: inline-block; margin: 0px; vertical-align: top;">
        <div class="input-prepend" style="margin-left: 12px;">
          <span class="add-on"><i class="fa fa-search"></i></span>
          <input class="input" name="s" type="text" value="<?php echo @$_GET["s"] ?>" placeholder="<?php echo get_string("label_search_courses") ?>">
        </div>
        </form>

        </span>
    </div>

    <div style="clear: both; border-top: 1px solid #888; margin: 0px 0px 24px 0px;"></div>

<?php if ($haveAds) { ?>
    <div class="row-fluid">
    <div class="span9">

    <div class="catalog-3cols">
<?php } else { ?>
    <div class="catalog-4cols">
<?php } ?>

      <?php if (!empty($courseInfos)) { ?>
        <?php foreach ($courseInfos as $course) { ?>
        <div class="catalog-card" style="float: left; vertical-align: top; position: relative;">
            <a href="https://eliademy.com/<?php echo $course->code ?>" title="<?php echo $course->fullname ?>" style="text-decoration: none; color: #333;">
            <div class="catalog-card-image" style="position: relative;">
                <img src="<?php echo $course->coursebackgroundtn ?>" alt="<?php echo $course->fullname ?>">
            </div>
            <div class="catalog-card-name<?php if (strlen($course->fullname) > 40) echo " longtext"; ?>"><?php echo $course->fullname ?></div>
            <div class="catalog-vertical-fade"></div>
            <div class="catalog-card-teacher">
                <table>
                    <tr>
                        <td rowspan="2" style="min-width: 47px;">
                            <img src="<?php echo $course->teacherlogo ?>" alt="<?php echo $course->teachername ?>">
                        </td>
                        <td><?php echo $course->teachername ?></td>
                    </tr>
                    <tr><td><?php echo $course->teachertitle ?></td></tr>
                </table>
            </div>
            <div class="catalog-card-status">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 25%;"><i class="fa fa-users"></i>&nbsp;<?php echo $course->students ?></td>
                        <td style="width: 50%; text-align: center;">
                            <i class="fa fa-star<?php if ($course->stars < 1) echo "-o"; ?>" style="color: gold;"></i>
                            <i class="fa fa-star<?php if ($course->stars < 2) echo "-o"; ?>" style="color: gold;"></i>
                            <i class="fa fa-star<?php if ($course->stars < 3) echo "-o"; ?>" style="color: gold;"></i>
                            <i class="fa fa-star<?php if ($course->stars < 4) echo "-o"; ?>" style="color: gold;"></i>
                            <i class="fa fa-star<?php if ($course->stars < 5) echo "-o"; ?>" style="color: gold;"></i>
                        </td>
                        <td style="width: 25%; text-align: right;"><?php echo ($course->price > 0 ? "&euro;" . number_format($course->price, 2) : "Free") ?></td>
                    </tr>
                </table>
            </div>
            </a>
            <?php if ($course->cert_enabled) { ?>
            <div style="position: absolute; top: 10px; right: 10px;"><i class="fa fa-certificate" style="font-size: 26px; color: #4189DD;"></i></div>
            <?php } ?>
        </div>
        <?php } ?>
      <?php } else { ?>
        <h2 style="text-align: center;"><?php echo get_string("label_no_courses_found") ?></h2>
      <?php } ?>
    </div><?php /* /3 cols */ ?>

    <?php } else /* no courses at all */ { ?>

    <div style="clear: both; border-top: 1px solid #888; margin: 24px 0px;"></div>

<?php if ($haveAds) { ?>
    <div class="row-fluid">
    <div class="span9">
<?php } ?>
    <h2 style="text-align: center;"><?php echo get_string("label_no_public_courses_yet") ?></h2>

    <?php } ?>

    <div style="clear: both;"></div>

<?php if ((int) $info["ann"]) {
    $ann_info = json_decode($info["ann_info"]);
?>
    <h3 style="color: #666;"><?php echo get_string("label_announcements") ?></h3>
    <div style="margin: 24px 0px; border-top: 1px solid #888;"></div>
    <div class="catalog-panel">
<?php if ($ann_info->cover) { ?>
    <img class="img-polaroid" src="<?php echo $ann_info->cover; ?>" style="float: left; margin: 0px 34px 24px 0px; max-width: 30%;">
<?php } ?>
<?php if (@$ann_info->title) { ?>
    <div style="margin-top: 12px; font-family: 'Open Sans'; font-weight: 300; font-size: 18px;"><?php echo @$ann_info->title; ?></div>
<?php } ?>
    <div style="margin-top: 12px;"><?php echo $ann_info->description; ?></div>
<?php if ($ann_info->url) { ?>
    <div class="pagination" style="text-align: center;">
    <ul><li>
        <a href="<?php echo linkFix($ann_info->url) ?>" target="_blank"><?php echo get_string("button_read_more"); ?> <i class="fa fa-long-arrow-right"></i></a>
    </li></ul>
    </div>
<?php } ?>
    <div style="clear: both;"></div>
    </div>
<?php } ?>

    <?php if ($details->location) { ?>
    <h3 style="color: #666;"><?php echo get_string("text_our_address") ?></h3>
    <div style="margin: 24px 0px; border-top: 1px solid #888;"></div>
    <iframe width="100%" height="250" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode($details->location . ", " . $details->country); ?>&key=AIzaSyC2rCXz182NkhJ_KjC8au3nItIke2tTCWY"></iframe>
    <?php } ?>

<?php if ($haveAds) { ?>
  </div>
  <div class="span3">

<?php if (!empty($ads1)) { ?>
<div class="catalog-panel" style="padding: 0px 8px 8px 8px;">
<?php if ($ad_info->title1) { ?>
    <div class="catalog-panel-head"><?php echo $ad_info->title1 ?></div>
<?php } ?>
<?php foreach ($ads1 as $ad) { ?>
<?php if ($ad->url) { ?><a href="<?php echo linkFix($ad->url) ?>" target="_blank"><?php } ?>
        <img src="<?php echo $ad->cover; ?>" style="margin-top: 8px;">
<?php if ($ad->url) { ?></a><?php } ?>
<?php } ?>
</div>
<?php } ?>

<?php if (!empty($ads2)) { ?>
<div class="catalog-panel" style="padding: 0px 8px 8px 8px;">
<?php if ($ad_info->title2) { ?>
    <div class="catalog-panel-head"><?php echo $ad_info->title2 ?></div>
<?php } ?>
<?php foreach ($ads2 as $ad) { ?>
<?php if ($ad->url) { ?><a href="<?php echo linkFix($ad->url) ?>" target="_blank"><?php } ?>
        <img src="<?php echo $ad->cover; ?>" style="margin-top: 8px;">
<?php if ($ad->url) { ?></a><?php } ?>
<?php } ?>
</div>
<?php } ?>

<?php if (!empty($ads3)) { ?>
<div class="catalog-panel" style="padding: 0px 8px 8px 8px;">
<?php if ($ad_info->title3) { ?>
    <div class="catalog-panel-head"><?php echo $ad_info->title3 ?></div>
<?php } ?>
<?php foreach ($ads3 as $ad) { ?>
<?php if ($ad->url) { ?><a href="<?php echo linkFix($ad->url) ?>" target="_blank"><?php } ?>
        <img src="<?php echo $ad->cover; ?>" style="margin-top: 8px;">
<?php if ($ad->url) { ?></a><?php } ?>
<?php } ?>
</div>
<?php } ?>

  </div>
 </div>
<?php } ?>

</div>
<?php

function linkFix($link) {
    if (strtolower(substr($link, 0, 7)) != "http://" && strtolower(substr($link, 0, 8) != "https://")) {
        return "http://" . $link;
    } else {
        return $link;
    }
}

// Simple search: a AND b AND c...
function searchMatch($query, $text) {
    $parts = preg_split('/\s+/', $query);

    foreach ($parts as $part) {
        if (stristr($text, $part) === FALSE) {
            return false;
        }
    }

    return true;
}

function makeLink($key, $val, $clear = array()) {
    $saveKeys = ["cat", "c"];
    $link = "?";

    if ($val !== null) {
        $link .= $key . "=" . $val;
    }

    foreach ($saveKeys as $k) {
        if ($key != $k && !in_array($k, $clear) && isset($_GET[$k])) {
            if ($link != "?") {
                $link .= "&";
            }

            $link .= $k . "=" . $_GET[$k];
        }
    }

    return $link;
}
