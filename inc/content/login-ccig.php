<?php
/**
* Eliademy.com
*
* @package   login_page_ccig
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div class="content formpage" dir="auto"><div class="container panel"><div class="form-box" style="position: relative;">
    <div id="hideLogin" class="browser_warning" style="position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; background: #FFF;">
        <?php echo get_string("dlg_login_unsupported_browser") ?>
        <a href="http://helpdesk.eliademy.com/knowledgebase/articles/236003"><?php echo get_string("button_learn_more") ?></a>
    </div>
    <script><!--
        if (document.addEventListener && window.navigator.userAgent.indexOf("MSIE ") == -1<?php if (!defined("IE11") || !IE11) { ?> && window.navigator.userAgent.indexOf("Trident/") == -1<?php } ?>) {
            document.getElementById("hideLogin").style.display = "none";
        }
    --></script>
    <div class="topblock">
        <h3><?php echo get_string("dlg_log_in"); ?></h3>
        <div><img id="logo-ccig" class="pull-middle" style="margin-bottom: 10px;" src="http://ccig.pl/wp-content/themes/ccig/_/img/logo.jpg"></div>
        <div>
            <form action="https://snlogger.ccig.pl/ccig/auth/" method="post">
                <input type="hidden" name="data[client_id]" value="hfa345Fgyu856fhuv8988IJU897h79">
                <input type="hidden" name="data[redirect_uri]" value="<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/ccig_redirect.php">
                <button type="submit" class="btn btn-githug social-signup">
                <i class="fa fa-external-link"></i>  | Log in with CCIG account
                </button>
            </form>
        </div>
        <?php if (isset($_GET["error"]) && !empty($_GET["error"])): ?>
        <div class="alert alert-error" style="margin-top: 15px;"><?php echo get_string("dlg_social_signin_error"); ?></div>
        <?php endif; ?>
    </div>
    <div style="text-align: center; margin-bottom: 15px;"><?php echo(get_string("dlg_login_with_email")); ?>:</div>
    <form method="post" action="#" id="login_form" class="form-horizontal">
        <div class="control-group">
            <label class="control-label" for="username"><?php echo(get_string("placeholder_email")); ?></label>
            <div class="controls">
                <?php if (isset($_GET["username"]) && !empty($_GET["username"])): ?>
                <input type="text" id="username" name="username" value="<?php echo($_GET["username"]); ?>">
                <?php else: ?>
                <input type="text" id="username" name="username">
                <?php endif; ?>
            </div>
        </div>
        <div class="control-group" id="login_controls">
            <label class="control-label" for="password"><?php echo(get_string("placeholder_password")); ?></label>
            <div class="controls">
                <input type="password" id="password" name="password">
                <span id="signinerror" class="help-inline hide"><?php echo(get_string("dlg_password_incorrect")); ?></span>
            </div>
        </div>
        <div class="control-group">
            <div class="controls submit">
                <input type="submit" class="btn btn-primary" value="<?php echo(get_string("dlg_log_in")); ?>">
            </div>
        </div>
    </form>
    <div style="text-align: center; padding: 0px 20px 20px 20px;">
        <a href="javascript:void(0)" onclick="$('#passwd_reset_dialog').modal('show')"><?php echo(get_string("dlg_forgot_password")); ?></a>
    </div>
</div></div></div>
<div class="modal hide fade" id="passwd_reset_dialog">
    <form style="margin: 0px;" method="post" action="#" id="passwd_reset_form">
        <div class="modal-header">
            <h3><?php echo(get_string("dlg_forgotten_password")); ?></h3>
        </div>
        <div class="modal-body" style="padding: 0px;">
            <div style="background-color: #E7F3FF; color: #428EE2; padding: 15px;"><?php echo(get_string("dlg_to_reset_password")); ?></div>
            <div class="control-group" id="reset_controls" style="margin: 15px;">
                <input type="text" style="width: 96%;" name="email" placeholder="<?php echo(get_string("placeholder_email")); ?>">
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" value="<?php echo(get_string("button_submit")); ?>">
        </div>
    </form>
</div>
<div class="modal hide fade" id="passwd_reset_message_dialog" style="width: 400px; margin-left: -200px;">
    <div class="modal-header">
        <h3><?php echo(get_string("dlg_forgotten_password")); ?></h3>
    </div>
    <div class="modal-body" style="padding: 0px;">
        <div style="background-color: #E7F3FF; color: #428EE2; padding: 15px;"><?php echo(get_string("dlg_password_reset")); ?></div>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn btn-primary" value="<?php echo(get_string("button_continue")); ?>" data-dismiss="modal" aria-hidden="true">
    </div>
</div>
