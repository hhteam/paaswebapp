<?php
/**
 * Eliademy.com
 * 
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 


?>
<div class="content" dir="auto"><div class="container panel"><div class="shadowbox"><div class="page-wrapper">

<h3 class="underlined">Open Source</h3>

<h3>We Open Great Ideas to Everyone</h3>

<p dir="auto">Eliademy from CBTec, is a new
<a href="http://en.wikipedia.org/wiki/Learning_management_system"
target="_blank">learning management system</a> or simply Classrooms in the
Cloud based on open source technology. Eliademy allows anyone to create,
share and manage educational content.</p>

<p dir="auto">Eliademy is released under several licenses and we empower
anybody who wants to learn more, use the learning system or simply
customize it to do so. If you would like to subscribe to any of our
services, please visit our <a href="/">main website</a>.</p>

<h3>Try out Eliademy</h3>

<p dir="auto">Eliademy is free for instructors to use without any
installations. Simply <a href="/#signup">create an account</a>,
create a course and invite participants. If you have existing courses on
popular learning management systems like Moodle, you can import your
courses to Eliademy by following this
<a href="http://eliademy.uservoice.com/knowledgebase/articles/211727-how-to-import-a-course-in-moodle-format-to-eliadem" target="_blank">instruction</a>.</p>

<p dir="auto">If at a later stage you want to migrate your courses back to
your own customized Eliademy or other LMS, we will also support with a
simple export button.</p>

<h3>Our Open Source Affiliation</h3>

<p dir="auto">CBTec is affiliated with a number of open source projects in
Finland and all around the world. We are proud of our contribution to these
communities as we encourage an open development and distribution of
software:</p>

<div class="logos row-fluid">
    <div class="span4"><a href="http://getbootstrap.com/" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-bootstrap.png" alt=""></a></div>
    <div class="span4"><a href="http://ckeditor.com/" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-ckeditor.png" alt=""></a></div>
    <div class="span4"><a href="http://jquery.com" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-jquery.png" alt=""></a></div>
 </div>
 <div class="logos row-fluid">
    <div class="span4"><a href="http://backbonejs.org" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-backbone.png" alt=""></a></div>
    <div class="span4"><a href="http://requirejs.org" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-requirejs.png" alt=""></a></div>
    <div class="span4"><a href="http://underscorejs.org" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-underscore.png" alt=""></a></div>
 </div>

<p dir="auto">CBTec is part of the steering group of <a href="http://coss.fi/" target="_blank">Centre for Open Source Software</a>. An initiative from Finland to promote open source, open data, open interfaces and open
standards. </p>

<h3>Open Source Projects</h3>

<h4>Eliademy Android client</h4>

<p dir="auto">Eliademy mobile app is designed to provide backwards compatibility with generic Moodle installation. You can learn more from project
<a href="https://github.com/cbtec/eliademy-android" target="_blank">repository</a>
on GitHub.</p>

<h4>Moodle THRIFT</h4>

<p dir="auto">CBTec has contributed to Moodle, the open source LMS to
provide the tools required to use Moodle Web services with Apache Thrift.
For details, please check
<a href="https://bitbucket.org/hhteam/moodle_thrift_tools/wiki/Home" target="_blank">here</a>.</p>

<p dir="auto"><strong>If would like to learn more, please contact us at <a href="mailto:support@eliademy.com">support@eliademy.com</a>.</strong></p>

</div></div></div></div>
