<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div style="text-align: center;">
    <h2 style="color:#3A3A3A; margin-top: 30px; margin-bottom: 10px;">
    <?php echo(get_string("dlg_have_account")); ?><a href="<?php echo CFG_ROOT_PATH . $lang; ?>/login"> <?php echo get_string("dlg_log_in"); ?></a>.
    </h2>
</div>
<div class="content formpage" dir="auto"><div class="container panel"><div class="form-box" style="position: relative;">
    <div id="hideLogin" class="browser_warning" style="position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; background: #FFF;">
        <?php echo get_string("dlg_login_unsupported_browser") ?>
        <a href="http://helpdesk.eliademy.com/knowledgebase/articles/236003"><?php echo get_string("button_learn_more") ?></a>
    </div>
    <script><!--
        if (document.addEventListener && window.navigator.userAgent.indexOf("MSIE ") == -1<?php if (!defined("IE11") || !IE11) { ?> && window.navigator.userAgent.indexOf("Trident/") == -1<?php } ?>) {
            document.getElementById("hideLogin").style.display = "none";
        }
    --></script>
    <div class="topblock"  style="display:none;">
        <h3><?php echo get_string("dlg_sign_up"); ?></h3>
        <div class="row-fluid">
            <div class="span3" style="text-align: center;">
                <a href="https://www.facebook.com/dialog/oauth/?client_id=<?php echo(CFG_FB_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/facebook_redirect.php&amp;scope=email,user_birthday,user_location&amp;response_type=code" class="btn btn-facebook social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-facebook" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://www.linkedin.com/uas/oauth2/authorization?client_id=<?php echo(CFG_LIN_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/linkedin_redirect.php&amp;scope=r_emailaddress&amp;state=<?php echo(substr(sha1(rand()), 0, 30)); ?>&amp;response_type=code" class="btn btn-linkedin social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-linkedin" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://login.live.com/oauth20_authorize.srf?client_id=<?php echo(CFG_LIVE_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/live_redirect.php&amp;scope=wl.signin%20wl.basic%20wl.emails&amp;response_type=code" class="btn btn-github social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-windows" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
            <div class="span3" style="text-align: center;">
                <a href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo(CFG_GOOGLE_CLIENT_ID); ?>&amp;redirect_uri=<?php echo(CFG_FB_REDIRECT_URL); ?>auth/googleoauth2/google_redirect.php&amp;scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&amp;state=<?php echo(substr(sha1(rand()), 0, 30)); ?>&amp;response_type=code" class="btn btn-google-plus social-signup" style="width: 65px; padding: 0px; border-radius: 100px;">
                <i class="fa fa-google-plus" style="line-height: 65px; font-size: 30px;"></i>
                </a>
            </div>
        </div>
    </div>
    <div style="text-align: center; margin-bottom: 15px;"><?php //echo(get_string("dlg_login_with_email")); ?>:</div>
    <form method="post" action="#" id="signup_form" class="form-horizontal">
        <div class="control-group" id="signup_firstname">
            <label class="control-label" for="firstname"><?php echo(get_string("placeholder_firstname")); ?></label>
            <div class="controls">
                <input type="text" id="firstname" name="firstname">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="control-group" id="signup_lastname">
            <label class="control-label" for="lastname"><?php echo(get_string("placeholder_lastname")); ?></label>
            <div class="controls">
                <input type="text" id="lastname" name="lastname">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="control-group" id="signup_email">
            <label class="control-label" for="email"><?php echo(get_string("placeholder_email")); ?></label>
            <div class="controls">
                <input type="text" id="email" value="<?php echo @$_GET["email"] ?>" name="email">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="control-group" id="signup_password">
            <label class="control-label" for="password"><?php echo(get_string("placeholder_password")); ?></label>
            <div class="controls">
                <input type="password" name="password">
                <span class="help-inline hide"></span>
            </div>
        </div>
        <div class="control-group">
            <div class="controls submit">
                <input type="submit" class="btn btn-success" value="<?php echo(get_string("dlg_sign_up")); ?>">
            </div>
        </div>
    </form>
    <div style="padding: 0px 20px 20px 20px;"><?php echo(get_string("dlg_accept_terms")); ?></div>
</div></div></div>
