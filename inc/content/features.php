<div class="container"> <div class="panel-wrapper">
    <!-- general container -->
    <!-- Free -->
    <div id="donate" style="text-align: center; margin-top: 2em;">
        <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
        <i class="fa fa-heartbeat" style="font-size: 35px; color: #4189DD; line-height: 50px;"></i>
        </span>
    </div>
    <h2 style="text-align: center; margin-top:30px; color:#4189DD"><?php echo get_string("feature_free_label"); ?></h2>
    <div class="line-separator"></div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_editor"); ?></h2>
            </div>
            <p><?php echo get_string("feature_free_body_editor"); ?></p>
            <i class="fa fa-youtube-play" ></i> <a class="ignored_link" data-magnific-iframe="true" href="https://www.youtube.com/watch?v=DE7rghCsIls"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_lms"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_lms"); ?></p>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_forum"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_forum"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/08/08/better-and-faster-forums-now-available-in-all-eliademy-courses/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_qizzes"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_qizzes"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://blog.eliademy.com/2016/09/28/quizzes-in-a-live-learning-platform/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_grading"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_grading"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/04/11/feature-friday-new-course-navigation-model-and-certificates-powered-by-the-users/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_multimedia"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_multimedia"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://youtu.be/DE7rghCsIls"><?php echo get_string("button_learn_more"); ?></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_certificate"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_certificate"); ?></p>
            <i class="fa fa-youtube-play" ></i> <a class="ignored_link" data-magnific-iframe="true" href="https://www.youtube.com/watch?v=r3YyTYRb1Kc"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_linkedin"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_linkedin"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/01/10/share-your-academic-achievements-on-linkedin/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_marketplace"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_marketplace"); ?></p>
            <i class="fa fa-youtube-play" ></i> <a class="ignored_link" data-magnific-iframe="true" href="https://www.youtube.com/watch?v=va8eVh0ld4E"><?php echo get_string("button_learn_more"); ?></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_notifications"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_notifications"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2013/07/22/eliademy-rolls-out-new-visual-editor-email-notifications-and-more/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_calendar"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_calendar"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/08/22/new-calendar-to-help-you-with-busy-autumn/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_analytics"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_analytics"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/05/30/learners-and-organization-analytics-comes-to-eliademy/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_localization"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_localization"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="http://translate.eliademy.com"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_free_header_unlimcourses"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_free_body_unlimcourses"); ?></p>
        </div>
        <div class="span4" style="text-align: center;">
        </div>
    </div>
    </div></div> <!-- /container -->
    <div class="braker-block">
        <h2><?php echo get_string("text_interested"); ?></h2>
        <div class="row-fluid">
            <div class="span12" >
                <a href="<?php echo CFG_ROOT_PATH . $lang; ?>/signup" class="btn btn-success btn-large"><?php echo get_string("button_get_started"); ?> <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
    <div class="container"><div class="panel-wrapper">  <!-- general container -->
    <!-- Premium -->
    <div id="donate" style="text-align: center; margin-top: 2em;">
        <span style="border: 1px solid #4189DD; display: inline-block; padding: 10px; border-radius: 40px; min-width: 50px;">
        <i class="fa fa-unlock-alt" style="font-size: 35px; color: #4189DD; line-height: 50px;"></i>
        </span>
    </div>
    <h2 style="text-align: center;margin-top:30px; color:#4189DD"><?php echo get_string("feature_pro_label"); ?></h2>
    <div class="line-separator"></div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_private"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_private"); ?></p>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_userdirectory"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_userdirectory"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/04/04/feature-friday-improved-quizzes-groups-management-and-self-registration-for-e4b/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_enrollments"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_enrollments"); ?></p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_webinar"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_webinar"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/11/14/eliademy-launches-live-sessions-to-bring-online-tutoring-to-every-corner-of-the-world/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_orgcatalog"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_orgcatalog"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="http://helpdesk.eliademy.com/knowledgebase/articles/657655-organization-public-web-page"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_roles"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_roles"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2015/09/04/eliademy-premium-gets-better-team-management/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_videos"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_videos"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/05/23/upload-and-store-private-videos-on-eliademy/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_organal"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_organal"); ?></p>
            <i class="fa fa-external-link" ></i> <a class="ignored_link" href="https://eliademy.com/blog/2014/05/30/learners-and-organization-analytics-comes-to-eliademy/"><?php echo get_string("button_learn_more"); ?></a>
        </div>
        <div class="span4" style="text-align: center;">
            <div >
                <h2 style="color:#4189DD; margin-top:30px;"><?php echo get_string("feature_pro_header_support"); ?></h2>
            </div>
            <p ><?php echo get_string("feature_pro_body_support"); ?></p>
        </div>
    </div>
    <!-- Sign up -->
    <div class="line-separator"></div>
    <div >
        <h2 style="text-align: center;"><?php echo get_string("text_interested_pro"); ?></h2>
        <div class="row-fluid">
            <div class="span12" style="text-align: center;">
                <a href="<?php echo CFG_ROOT_PATH . $lang; ?>/premium" class="btn btn-success btn-large"><?php echo get_string("button_learn_more"); ?> <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
    <!-- general container -->
</div>
</div>