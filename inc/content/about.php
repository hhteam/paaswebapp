<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div dir="auto"><div class="container panel">
	<h3 class="underlined"><?php echo get_string("title_about_us") ?></h3>
	<img src="<?php echo(CFG_ROOT_PATH); ?>img/group-photo.jpg" alt="About Us">
    <br>
    <br>
    <p dir="auto"><?php echo(get_string("text_aboutus_para2")); ?> <?php echo(get_string("text_aboutus_para21")); ?></p>
    <p dir="auto"><?php echo(get_string("text_aboutus_para1")); ?></p>
    <p dir="auto"><?php echo(get_string("text_aboutus_para3")); ?></p>
    <p dir="auto"><?php echo(get_string("text_aboutus_para4")); ?></p>
    <div class="row-fluid">
        <div class="span6">
            <div style="text-align: center;">
                <!-- Alexa banner -->
                <a href="http://www.alexa.com/siteinfo/eliademy.com"><script type="text/javascript" src="http://xslt.alexa.com/site_stats/js/t/a?url=eliademy.com"></script></a>
            </div>
        </div>
        <div class="span6">
<!--             <div style="text-align: center;">
                <a href="https://mixpanel.com/f/partner" rel="nofollow"><img src="//cdn.mxpnl.com/site_media/images/partner/badge_light.png" alt="Mobile Analytics" /></a>
            </div> -->
        </div>
    </div>

    <!-- Adblock -->
    <div style="text-align: middle">
    </div>

</div></div>