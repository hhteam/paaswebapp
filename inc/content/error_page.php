<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div class="content"><div class="container panel"><div class="shadowbox"><div class="page-wrapper">
    <h2 style="text-align: center; margin: 40px 0px;"><?php echo($error_message); ?></h2>
</div></div></div></div>