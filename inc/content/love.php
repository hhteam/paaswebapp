<?php
/**
* Eliademy.com
*
* @package   login_page
* @copyright CBTec Oy
* @license   All rights reserved
*/
?>
<div class="content" dir="auto"><div class="container panel"><div class="shadowbox"><div class="page-wrapper">
    <p dir="auto">We dream of a world where everybody have access to education and this is our road to make it happen.</p>
    <h3>Our Progress 2014</h3>
    <p dir="auto">2014 has been a year filled with noble and inspiring work. Here are some examples about our current achievements:</p>
    <ul>
        <li>Special thanks to our wonderful course instructors, because of them, we have been able to deliver free education to over 60 000 people for free or at a very affordable price.</li>
        <li>With an estimated value of 1.4 Millions USD of investment, we give back to the community with our learning management tool, allowing to provide a free cloud-based classroom to 30 000 users.</li>
        <li>We financed <a href="http://www.haaga-helia.fi/en/events/happy-hacking-day-2014#.U_b-ZrySxy8" target="_blank">HappyHackingDays</a>, giving 600 students the opportunity to experiment with 3D printing and learn about coding.</li>
        <li>We estimate that with a 10 USD admission per online course, we have trained 5 000 people with MOOCs and the overall contribution of 50 000 USD to the surrounding society.</li>
        <li>We have signed the Finnish <a href="http://www.fibsry.fi/images/TIEDOSTOT/DC_sitoumus_FIBS_eng.pdf">Diversity Charter</a> and as a member engaged in developing our management and service practices through diversity principles of the Charter.</li>
    </ul>
    <h3>Our Goals for 2015</h3>
    <p dir="auto">A lot of good things to be grateful for were accomplished during 2014, and we want to keep up the good work for the upcoming year. Here is how we are going to do it:</p>
    <ul>
        <li>Educate 1 Million people all over the world for free;</li>
        <li>Invest 25% of our profits in a project that develops community;</li>
        <li>Receive environmental certification and improve our actions to become a green organization;</li>
        <li>Implement relevant training for all of our employees, partners and investors on the effects of our good work in order to keep them motivated and focused on our mission.</li>
    </ul>
    <h3>Some of our ongoing projects</h3>
    <ul>
        <li>We are working with <a href="https://eliademy.com/blog/2014/07/16/eminus-academy-brighter-and-sustainable-future-for-the-youth-of-africa-through-online-education/"
        target="_blank">Eminus Academy</a>, a project held by UN Habitat to help young adults in Africa make their way out of poverty by teaching them entrepreneurship. The project is in a stage of further development for the second run.</li>
        <li>We are collaborating with NGOs and NPOs as part of our CSR program by supporting and helping them with their campaigns. By supporting non-profit and educational organisations, we help them to raise awareness about their causes through online courses on Eliademy. Hence, Eliademy takes one step further to the fulfilment of our main aim as a company.</li>
        <li>We are starting relevant collaborations with several non-profit and educational organisations all over the world. Stay tuned for more information!</li>
    </ul>
    <h3>Our partners</h3>
    <div class="logos row-fluid">
        <div class="span4"><a href="http://www.fibs.fi/" target="_blank"><img style="height: 80px" src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-fibs.png" alt="FIBS"></a></div>
        <div class="span4"><a href="http://www.diversity-charter.com/diversity-charter-finland-charter.php" target="_blank"><img style="height: 80px" src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-divchapt.png" alt="Divercity Chapter"></a></div>
        <div class="span4"><a href="https://www.unglobalcompact.org/" target="_blank"><img style="height: 80px" src="<?php echo(CFG_ROOT_PATH); ?>img/logos/logo-un.png" alt="UN"></a></div>
    </div>
    <h3>Read our policies</h3>
    <ul>
        <li><a href="docs/cbtec-cop.pdf">COP</a></li>
        <li><a href="docs/cbtec-coc.pdf">Code of Conduct</a></li>
        <li><a href="docs/cbtec-envpolicy.pdf">Environmental Policy</a></li>
        <li><a href="https://eliademy.com/privacy">Privacy policy</a></li>
        <li><a href="http://www.slideshare.net/Eliademy/eliademcsr-proposition-for-ngo" target="_blank">Corporate Social Responsibility</a></li>
    </ul>
</div></div></div></div>