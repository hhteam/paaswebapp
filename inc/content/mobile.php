<div class="container">
    <div class="panel-wrapper">
        <!-- general container -->
        <!-- TGIO stuff -->
        <div class="row-fluid">
            <span class="span8 textpad" style="padding-top: 1em;">
            <h1 style="margin: 1em 2em 2em 0px; text-align: left;">TGIO – your app to study in peace</h1>
            <p>TGIO is a mobile application that takes you off the grid. It unplugs you from the internet and your mobile applications for any time you want. TGIO will help you to understand your level of internet dependency, while still allowing you to enjoy an e-learning course or e-book. TGIO is a free – mobile extension to Eliademy.</p>
            <a href="https://play.google.com/store/apps/details?id=com.tgioapp.tgio" alt="Google Play" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logo-google-play.png" class="image" style="margin: 2em 0px;"></a>
            <p><?php echo(get_string("text_mobile_features")); ?></p>
            <ul>
                <li>Allows you to enjoy an e-learning course or e-book</li>
                <li>Suppresses all Social Media notifications</li>
                <li>Set timings for your personal connectivity retreat</li>
                <?php /*         <li>Allows to do calls and SMS to pre-determined connections</li>      */ ?>
            </ul>
            <p>TGIO is only available on Android, and we work hard to make it available on other devices. For more information check <a href="http://www.tgioapp.com">TGIO app page</a>.</p>
            </span>
            <span class="span4" style="text-align: center;">
            <img src="<?php echo(CFG_ROOT_PATH); ?>img/mobile-tgio.png">
            </span>
        </div>
        <!-- TGIO stuff -->
        <div class="line-separator"></div>
        <div class="row-fluid">
            <span class="span4" style="text-align: center;">
            <span style="display: inline-block; background: url('<?php echo(CFG_ROOT_PATH); ?>img/phones0.png') no-repeat; background-size: contain;">
            <img id="phone-img-1" src="<?php echo(CFG_ROOT_PATH); ?>img/phones1.png" class="image" style="width: 250px; position: absolute; visibility: hidden;">
            <img id="phone-img-2" src="<?php echo(CFG_ROOT_PATH); ?>img/phones1.png" class="image" style="width: 250px;">
            </span>
            </span>
            <span class="span8 textpad" style="padding-top: 1em;">
            <h1 style="margin: 1em 2em 2em 0px; text-align: left;"><?php echo(get_string("text_mobile_header")); ?></h1>
            <p><?php echo(get_string("text_mobile_subheader")); ?></p>
            <a href="https://play.google.com/store/apps/details?id=com.cbtec.eliademy" alt="Google Play" target="_blank"><img src="<?php echo(CFG_ROOT_PATH); ?>img/logo-google-play.png" class="image" style="margin: 2em 0px;"></a>
            <p><?php echo(get_string("text_mobile_features")); ?></p>
            <ul>
                <li><?php echo(get_string("text_mobile_features_1")); ?></li>
                <li><?php echo(get_string("text_mobile_features_2")); ?></li>
                <li><?php echo(get_string("text_mobile_features_3")); ?></li>
                <li><?php echo(get_string("text_mobile_features_4")); ?></li>
                <li><?php echo(get_string("text_mobile_features_5")); ?></li>
            </ul>
            <p><?php echo(get_string("text_mobile_features_footer")); ?></p>
            </span>
        </div>
        <!-- general container -->
    </div>
</div>
<script>
$(function ()
{
    var img = [
$("<img>").attr("src", "<?php echo(CFG_ROOT_PATH); ?>img/phones1.png"),
$("<img>").attr("src", "<?php echo(CFG_ROOT_PATH); ?>img/phones2.png"),
$("<img>").attr("src", "<?php echo(CFG_ROOT_PATH); ?>img/phones3.png"),
$("<img>").attr("src", "<?php echo(CFG_ROOT_PATH); ?>img/phones4.png"),
$("<img>").attr("src", "<?php echo(CFG_ROOT_PATH); ?>img/phones5.png") ], idx = 0;
var switchPic = function ()
{
if (++idx > img.length - 1)
{
idx = 0;
}
var pos = $("#phone-img-2").position();
$("#phone-img-1").attr("src", $("#phone-img-2").attr("src")).css({ top: pos.top, left: pos.left, width: $("#phone-img-2").css("width"), visibility: "visible", opacity: 1 });
$("#phone-img-2").css({ opacity: 0 }).attr("src", img[idx].attr("src"));
$("#phone-img-2").animate({ opacity: 1 }, 600, "linear", function ()
{
$("#phone-img-1").css({ visibility: "hidden" });
setTimeout(switchPic, 8000);
});
};
setTimeout(switchPic, 8000);
});
</script>