<?php
/**
 * Eliademy.com
 *
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

// Local paths
define("CFG_ROOT_PATH",         "/");                           // Login page
define("CFG_MOODLE_PATH",       "/app/");                       // Moodle
define("CFG_MAGIC_PATH",        "/app/a/");                     // Magic UI

// Social media integration
define("CFG_FB_CLIENT_ID",      "485673934801504");             // Facebook
define("CFG_FB_REDIRECT_URL",   "https://salestraining.eliademy.com/app/");

define("CFG_LIN_CLIENT_ID",     "c97fxelamf5x");                // LinkedIn

define("CFG_GOOGLE_CLIENT_ID",  "807835848233.apps.googleusercontent.com"); // Google
define("CFG_GOOGLE_APPS_LINK",  "https://chrome.google.com/webstore/detail/eliademy/ffphlebjhkikoffimgohhmjpkalbcple");

define("CFG_LIVE_CLIENT_ID",    "000000004C0FF884");            // MS


// Data connections
define("DB_HOST",               "localhost");
define("DB_NAME",               "moodle");
define("DB_USER",               "moodle");
define("DB_PASSWORD",           "S1B2@moodle");
define("DB_PREFIX",             "mdl_");

define("REDIS_HOST",            "localhost");
define("REDIS_PORT",             6379);


// Enable/disable IE11 login.
define("IE11", false);


// Add supported languages here. Create string files <code>.php in i18n
// directory.
$LANGS = array(
    "default" => "English",

    "ar" => "عربي",
    "cs" => "Čeština",
    "de" => "Deutsch",
    "el" => "Ελληνικά",
    "es" => "Español - intl.",
    "et" => "Eesti",
    "fil" => "Filipino",
    "fi" => "Suomi",
    "fr" => "Français",
    "hi" => "हिन्दी",
    "hr" => "Hrvatski",
    "it" => "Italian",
    "kk" => "Қазақша",
    "ko" => "한국어",
    "la" => "Latin",
    "lt" => "Lietuviškai",
    "lv" => "Latviešu",
    "mn" => "Монгол",
    "nl" => "Nederlands",
    "no" => "Norwegian Bokmål",
    "pl" => "Polski",
    "ptbr" => "Português do Brasil",
    "pt" => "Português",
    "ru" => "Русский",
    "sl" => "Slovenščina",
    "srlt" => "Srpski",
    "sr" => "Српски",
    "sv" => "Svenska",
    "ta" => "தமிழ்",
    "te" => "తెలుగు",
    "th" => "Thai",
    "tr" => "Türkçe",
    "vi" => "Vietnamese",
    "zhcn" => "简体中文",
    //"zhtw" => "繁體中文"
);
