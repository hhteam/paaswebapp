<?php
/**
 * Eliademy.com
 * 
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */ 

?>


<div class="content"><div class="container">

<?php if (!$course["is_open"]) { ?>
<div class="alert alert-error" style="text-align: center;"><?php echo(get_string("text_enrollment_closed")); ?></div>
<?php } ?>

<div class="shadowbox"><div style="padding: 0px;">

<div class="row-fluid">
 <div class="span8" style="margin: 5px 0px 0px 5px; background: transparent url('<?php echo($course["background_url"]); ?>') 50% 50% no-repeat; background-size: auto; height: 170px;"></div>
 <div class="span4" style="margin-left: 0px; width: 33.3%;">

<div style="background-color: #F6F6F6; margin-top: 80px; height: 53px; padding: 7px 0px 0px 0px;">
<table style="font-size: 10pt;">
<tr>
<td rowspan="2" style="width: 60px; text-align: center;"><img style="width: 32px;" src="<?php echo($course["teacher_icon"]); ?>" alt="<?php echo($course["teacher"]); ?>"></td>
<td><strong><?php echo($course["teacher"]); ?></strong></td>
</tr>
<tr><td><?php echo(get_string("teacher")); ?><td></tr>
</table>
</div>

<div style="background: #E4F0FE url('<?php echo(CFG_ROOT_PATH); ?>img/participants.png') 20px 10px no-repeat; height: 35px;"></div>

 </div>
</div>

<div class="row-fluid" style="padding: 10px 0px;">

<div class="span8">
 <h1 class="course-title"><?php echo($course["fullname"]); ?></h1>
</div>

<div class="span4" style="text-align: center;">
<?php if ($course["is_open"]) { ?>
 <a href="<?php echo(CFG_MAGIC_PATH); ?>invitation/<?php echo($course["invitation_url"]); ?>" class="btn btn-primary btn-large"><?php echo(get_string("button_enroll")); ?></a>
<?php } else { ?>
 <span class="btn btn-primary btn-large disabled"><?php echo(get_string("button_enroll")); ?></span>
<?php } ?>
</div>

</div>

</div></div></div>

<div class="container"><div class="shadowbox"><div style="padding: 20px 15px;">
<?php echo($course["description"]); ?>
</div></div></div>

</div>
