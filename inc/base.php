<?php
/**
 * Eliademy.com
 *
 * @package   login_page
 * @copyright CBTec Oy
 * @license   All rights reserved
 */

define("VERSION", "1492587492.14");

?>
<!DOCTYPE html>
<html lang="<?php echo($lang); ?>"  dir="<?php echo($lang == "ar" ? "rtl" : "ltr"); ?>" >
<head>
 <meta charset="UTF-8">
 <title><?php echo(@$title); ?></title>
 <meta name="title" content=<?php echo json_encode(@$title); ?>>
 <meta name="description" content=<?php echo json_encode(@$description); ?>>
<?php if (@$meta_publisher) { ?>
 <meta name="publisher" content="<?php echo $meta_publisher; ?>">
<?php } ?>
 <meta property="fb:app_id" content="485673934801504">
 <meta property="og:site_name" content="Eliademy">
 <meta property="og:title" content=<?php echo json_encode(@$title); ?>>
 <meta property="og:type" content="website">
 <meta property="og:url" content="https://eliademy.com<?php echo $_SERVER["REQUEST_URI"]; ?>">
 <meta property="og:description" content=<?php echo json_encode(@$description); ?>>
 <meta property="og:image" content="<?php echo $page_image; ?>">
 <meta property="og:locale" content="en_US">
 <meta name="twitter:card" content="summary">
 <meta name="twitter:url" content="https://eliademy.com<?php echo $_SERVER["REQUEST_URI"]; ?>">
 <meta name="twitter:title" content=<?php echo json_encode(@$title); ?>>
 <meta name="twitter:description" content=<?php echo json_encode(@$description); ?>>
 <meta name="twitter:image" content="<?php echo $page_image; ?>">
 <meta name="twitter:site" content="@eliademy">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <style>
@-webkit-viewport{width:device-width;}
@-moz-viewport{width:device-width;}
@-ms-viewport{width:device-width;}
@-o-viewport{width:device-width;}
@viewport{width:device-width;}
 </style>
 <link href="//fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet">
 <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
 <link href="<?php echo(CFG_ROOT_PATH); ?>css/bootstrap.min.css?v=<?php echo(VERSION); ?>" rel="stylesheet">
 <link href="<?php echo(CFG_ROOT_PATH); ?>css/bootstrap-responsive.min.css?v=<?php echo(VERSION); ?>" rel="stylesheet">
 <link href="<?php echo(CFG_ROOT_PATH); ?>css/social-buttons.min.css?v=<?php echo(VERSION); ?>" rel="stylesheet">
 <link href="<?php echo(CFG_ROOT_PATH); ?>css/style.css?v=<?php echo(VERSION); ?>" rel="stylesheet">
 <link href="<?php echo(CFG_ROOT_PATH); ?>css/header.css?v=<?php echo(VERSION); ?>" rel="stylesheet">
 <link href="<?php echo(CFG_ROOT_PATH); ?>css/catalog.css?v=<?php echo(VERSION); ?>" rel="stylesheet">
 <link rel="stylesheet" href="<?php echo CFG_ROOT_PATH ?>app/a/lib/css/magnific-popup.css?v=<?php echo VERSION ?>">

 <?php foreach ($LANGS as $lk => $lt) { ?>
   <?php if (strcmp($lk,'default') == 0) { ?>
     <link rel="alternate" hreflang="en" href="<?php echo CFG_ROOT_PATH;?>"/>
   <?php } else { ?>
     <link rel="alternate" hreflang="<?php echo $lk;?>" href="<?php echo CFG_ROOT_PATH . $lk;?>"/>
   <?php }} ?>
 <!--[if lt IE 9]>
 <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
 <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
 <script src="//maxcdn.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
 <script src="<?php echo(CFG_ROOT_PATH); ?>js/jquery.cookie.min.js?v=<?php echo(VERSION); ?>"></script>
 <script src="<?php echo(CFG_ROOT_PATH); ?>js/spin.min.js?v=<?php echo(VERSION); ?>"></script>
 <script src="<?php echo(CFG_ROOT_PATH); ?>js/smooth-scroll.min.js?v=<?php echo(VERSION); ?>"></script>
 <script src="<?php echo(CFG_ROOT_PATH); ?>js/fastclick.min.js?v=<?php echo(VERSION); ?>"></script>
 <script src="<?php echo(CFG_ROOT_PATH); ?>app/a/lib/js/magnificpopup.min.js?v=<?php echo(VERSION); ?>"></script>
 <script src="<?php echo(CFG_ROOT_PATH); ?>js/jquery.cookiesdirective.js?v=<?php echo(VERSION); ?>"></script>
 <script src="<?php echo(CFG_ROOT_PATH); ?>js/script.js?v=<?php echo(VERSION); ?>"></script>
 <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "brand": "Eliademy",
  "logo": "<?php echo $page_image; ?>",
  "description": <?php echo json_encode(@$description) ?>,
  "url": "https://eliademy.com/"
}
 </script>
 <script>
    var paths = { m: "<?php echo(CFG_MOODLE_PATH); ?>", a: "<?php echo(CFG_MAGIC_PATH); ?>", r: "<?php echo(CFG_ROOT_PATH); ?>" };
    var genericerror = "<?php echo(get_string("dlg_password_incorrect")); ?>";
    var loginretryerror = "<?php echo(get_string("error_login_retry")); ?>";
    var wrongautherror = "<?php echo(get_string("error_wrong_auth")); ?>";
    var userunknownerror = "<?php echo(get_string("error_user_unknown")); ?>";
    var currentLang = "<?php echo($lang); ?>";
    var currentPage = "<?php echo @$pageName; ?>";
    window.addEventListener('load', function() {
        FastClick.attach(document.body);
    }, false);
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
        document.createTextNode(
            "@-ms-viewport{width:auto!important}"
        )
    );
    document.getElementsByTagName("head")[0].
        appendChild(msViewportStyle);
}
$(document).ready(function() {
        $.cookiesDirective({
            privacyPolicyUri: '<?php echo(CFG_ROOT_PATH); ?>privacy',
            linkColor: "#FFF"
        });
    });
 </script>
 <script src="<?php echo CFG_MOODLE_PATH ?>theme/monorail/ext/ajax_get_toolbar.js?lang=<?php echo $lang; ?>&page=<?php echo $page; ?>"></script>

 <link rel="shortcut icon" href="<?php echo(CFG_ROOT_PATH); ?>img/favicon/favicon.ico?v=<?php echo(VERSION); ?>">
 <link rel="apple-touch-icon" href="<?php echo(CFG_ROOT_PATH); ?>img/favicon/favicon-apple.png?v=<?php echo(VERSION); ?>" />
  
  <!-- ShareThis Social block, used on Support page --
  <script type="text/javascript">var switchTo5x=true;</script>
  <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
  <script type="text/javascript">stLight.options({publisher: "3b75784d-6d60-4224-adf9-f485e6cd105c", doNotHash: false, doNotCopy: false, hashAddressBar: true});</script>
  -- ShareThis Social block, used on Support page -->
  
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrapper" style="background-color: <?php echo $background_color ?>;">
 <div class="header-block mobile-friendly" id="header"><div class="container" id="header-box"></div></div>
 <div class="hidden-phone" style="height: 51px;"></div>
 <div class="visible-phone" style="height: 0px;"></div>

 <?php require_once $content; ?>

 <div class="hidden-phone" style="height: 20px;"></div>
</div>

<div id="footer" style="margin-top: -160px;"></div>
<script>
(function () {
    $("#footer").load("<?php echo(CFG_ROOT_PATH); ?>footer.php?lang=<?php echo $lang; ?>");
})();
</script>

<!-- Google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38321548-1']);
  _gaq.push(['_setDomainName', 'eliademy.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"3TVni1awA+008d", domain:"eliademy.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=3TVni1awA+008d" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<!-- start Mixpanel -->
<script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f)}})(document,window.mixpanel||[]);
mixpanel.init("833586887b5ff48e87224afa1507418f");</script>
<!-- end Mixpanel -->

<!-- AddThis Button BEGIN -->

<!-- AddThis Button END -->

<!-- Google+ share button BEGIN -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<!-- Google+ share button END -->

<script>
    smoothScroll.init({ offset: 60 });
</script>
<?php /*
<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/ *<![CDATA[* /window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/ * custom configuration goes here (www.olark.com/documentation) * /
olark.identify('8971-790-10-8029');/ *]]>* /</script><noscript><a href="https://www.olark.com/site/8971-790-10-8029/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
*/ ?>
</body>
</html>
