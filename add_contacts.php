<?php

try
{
    if ($_SERVER["REQUEST_METHOD"] != "POST" ||
        !isset($_POST["name"]) || !isset($_POST["organization"]) || !isset($_POST["role"]) ||
        !isset($_POST["email"]) || !isset($_POST["phone"]) || !isset($_POST["country"]))
    {
        throw new Exception("Invalid request!");
    }

    $db = new PDO("sqlite:" . dirname(__FILE__) . "/inc/data/contacts.db");

    $stmt1 = $db->prepare("SELECT id FROM contacts WHERE ip_entered = ? AND time_entered > datetime('now', '-10 second')");
    $stmt1->execute(array($_SERVER["REMOTE_ADDR"]));

    if ($stmt1->fetch(PDO::FETCH_ASSOC) !== FALSE)
    {
        throw new Exception("Too many calls from this IP!");
    }

    $stmt2 = $db->prepare("INSERT INTO contacts (name, organization, role, email, phone, country, ip_entered) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $stmt2->execute(array($_POST["name"], $_POST["organization"], $_POST["role"],
        $_POST["email"], $_POST["phone"], $_POST["country"], $_SERVER["REMOTE_ADDR"]));

    mail("info@cbtec.fi", "Information request form", var_export($_POST, true));
}
catch (Exception $e)
{
    header('HTTP/1.1 404 Not Found');
    echo "<h1>404 Not Found</h1>";
    echo "The page you requested could not be found.";
}
